<?php 
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\ORM\TableRegistry;
class CommanHelper extends Helper
{
    public function initialize(array $config)
    {
        
    }
    public function findcoursefees($bid = null, $cid = null) {

        $articles = TableRegistry::get('Coursefranchise'); 
      //  pr($articles->find('all')->where(['Coursefranchise.branch' => $bid, 'Coursefranchise.course_id' => $cid, 'Coursefranchise.status' => 'A'])->first());
       // die;
        return $articles->find('all')->where(['Coursefranchise.branch' => $bid, 'Coursefranchise.course_id' => $cid, 'Coursefranchise.status' => 'A'])->first();
		
	}


    function inst_details($id = '') {


        $articles = TableRegistry::get('InstDetail'); 
        return $articles->find('all')->where(['InstDetail.inst_id' => $id]);


	}
    public function getCourseStudentCount($courseId) {
	
        $articles = TableRegistry::get('StudentCourse'); 
        return $articles->find('all')->where(['course_id'=>$courseId])->count();
 
       
       
 ///       return ClassRegistry::init('StudentCourse')->find('count', array('conditions' => array('course_id'=>$courseId), 'recursive' => -1));
	}



    public function seoFind($courseId = '') {

        $articles = TableRegistry::get('Seo'); 
        return $articles->find('all')->where(['Seo.course_id'=>$courseId])->first();
 
     

		//return ClassRegistry::init('Seo')->find('first', array('conditions' => array('Seo.course_id' => $courseId)));

	}




    public function user()
    {
        $articles = TableRegistry::get('Users'); 
        return $articles->find('all')->where(['Users.id' => 1])->first();
    } 

     public function admin()
     {
         $articles = TableRegistry::get('Users'); 
         return $articles->find('all')->where(['Users.id' => 1])->first();
     }


     function batc($id = '') {
        $articles = TableRegistry::get('Batch'); 
        return $articles->find('all')->where(['Batch.b_id' => $id])->first();


	}

    function brnach($id = '') {
        $articles = TableRegistry::get('Branch'); 
        return $articles->find('all')->where(['Branch.id' => $id])->first();


	}
    function getcoursebygroup($group = '') {
        $articles = TableRegistry::get('Course'); 
                    
        return $articles->find('all')->select(['cname','icon','url'])->where(['Course.course_group' =>trim($group),'Course.feature' => 'Y'])->order(['Course.sort' =>'ASC'])->toarray();


	}

    public function has_duplicate($mob) {

    $articles = TableRegistry::get('Enquiry'); 
    return $articles->find('all')->where(['Enquiry.phone like' => '%' . $mob . '%'])->first();

	
	}



    public function findcoursegroup() {

$articles = TableRegistry::get('Course'); 

return $articles->find('list', [
    'keyField' => 'course_group',
    'valueField' => 'course_group',
  ])->where(['course_group <>' => ''])->group(['Course.course_group'])->order(['Course.sort' => 'ASC'])->toArray();

  }
    public function testi($id = '') {

	
        $articles = TableRegistry::get('Resume'); 
        return $articles->find('all')->where(['Resume.enroll' =>$id])->first();

	}


    public function meta($id = '') {

	
        $articles = TableRegistry::get('Seo'); 
        return $articles->find('all')->where(['Seo.location' =>$id])->first();

	}


    public function seosettingmeta(){

	
        $articles = TableRegistry::get('Seosettings'); 
        return $articles->find('all')->order(['Seosettings.id' =>'DESC'])->first();

	}
    public function getBranch($s_enroll) {

        $articles = TableRegistry::get('Students'); 
        return $articles->find('all')->where(['Students.s_enroll' =>$s_enroll])->first();


	}
    public function discountstat($sid = '') {
        $articles = TableRegistry::get('Reportdiscount'); 
        return $articles->find('all')->where(['Reportdiscount.s_id' => $sid])->first();

	

	}
    public function getsettings() {
        $articles = TableRegistry::get('Seosettings'); 
        return $articles->find('all')->order(['Seosettings.id' => 'DESC'])->first();

	

	}

    public function myexcoursechangeBranch($id = '',$branchId="") {


        $articles = TableRegistry::get('StudentCourse'); 
        return $articles->find('all')->contain(['Branch','Students','Course'])->where(['StudentCourse.s_id' => $id, 'StudentCourse.is_change' => 'N', 'StudentCourse.branch_id' => $branchId])->toarray();

	

	}


    function std_newcorid($id = '', $crsid = '', $bbid = '') {
	
        $articles = TableRegistry::get('StudentCourse'); 
        return $articles->find('all')->contain(['Course'])->where(['StudentCourse.s_id' => $id, 'StudentCourse.course_id' => $crsid, 'StudentCourse.branch_id' => $bbid])->first();


	}
    public function usr_dtl($id = '' ) {


        $articles = TableRegistry::get('Students'); 
        return $articles->find('all')->contain(['StudentCourse','Reportdiscount'])->where(['s_enroll' => $id])->first();

  

}

    public function all_paid($st_id, $inst_id) {

        $articles = TableRegistry::get('StudentInstallment'); 
        return $articles->find('all')->where(['s_id' => $st_id, 'inst_id' => $inst_id, 'pay_date' => '0000-00-00'])->count();

	
	}


    function absresult($id = '', $eid = '') {

   $articles = TableRegistry::get('ExamStudent'); 
      
        return $articles->find('all')->select(['respo' =>$articles->find('all')->func()->sum('response')])->where(['ExamStudent.s_id' => $id, 'ExamStudent.e_id' => $eid])->first();
	}
    function resultuser($id = '') {


        $articles = TableRegistry::get('Users'); 
        return $articles->find('all')->where(['Users.s_id' => $id])->first();


	}

    public function crs($id = '') {

        $articles = TableRegistry::get('Course'); 
        return $articles->find('all')->where(['Course.id' => $id])->first();
      
	}
    public function findstaticpage($id = '') {

        $articles = TableRegistry::get('Staticpages'); 
        return $articles->find('all')->where(['Staticpages.id' => $id])->first();
      
	}

    
   

    public function recording($mobile)
	{

        $articles = TableRegistry::get('EnquiryCalls'); 
        return $articles->find('all')->where(['EnquiryCalls.customer_number'=>$mobile])->order(['EnquiryCalls.call_date' => 'DESC'])->first();

	}
    public function fees($id = '') {


        $articles = TableRegistry::get('Students'); 
        return $articles->find('all')->where(['Students.s_id' => $id, 'Students.status >' => '0'])->first();
	

	}

    public function myexcoursechange($id = '') {


        $articles = TableRegistry::get('StudentCourse'); 
        return $articles->find('all')->contain(['Branch','Students','Course'])->where(['StudentCourse.s_id' => $id, 'StudentCourse.is_change' => 'N'])->toarray();


	}

    public function checkcoursedisc($sid = null, $cid = null) {

        $articles = TableRegistry::get('StudentCourse'); 
        return $articles->find('all')->where(['course_id' => $cid, 's_id' => $sid])->first();

	
	}

    public function discountamt($enrol = "", $c_id = "") {

        $articles = TableRegistry::get('Discount'); 
        return $articles->find('all')->where(['enrollment LIKE' =>$enrol . '%', 'course_id' => $c_id,'status'=>'A'])->first();


	}
    public function instl($id = '', $insid = '') {


        $articles = TableRegistry::get('StudentInstallment'); 
        return $articles->find('all')->where(['StudentInstallment.s_id' => $id, 'StudentInstallment.inst_id' => $insid])->order(['StudentInstallment.pro_date' => 'ASC'])->toarray();


	}


    public function new_branch($id = '') {

        $articles = TableRegistry::get('Branch'); 
        return $articles->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'])->where(['Branch.id !=' => $id,'Branch.status' => 'Y'])->order(['name' => 'ASC'])->toArray();
    
	

	}
    public function feedetails($id = null, $cid = null) {


        $articles = TableRegistry::get('StudentInstallment'); 
      
        $payment = $articles->find('all')->select(['total_sum' =>$articles->find('all')->func()->sum('amount')])->where(['s_id' => $id, 'recipt_no !=' => 0, 'course_id' => $cid,'amount !=' => 250])->first();

        $articles = TableRegistry::get('Cash'); 

        $new_payment = $articles->find('all')->select(['total_cash' =>$articles->find('all')->func()->sum('amount')])->where(['s_id' => $id, 'status' => 'Y', 'is_reg' => 'N', 'cid' => $cid,'receipt_no !='=>'0'])->first();


		$total_fees = 0;
		$total_fees = $payment['total_sum'] + $new_payment['total_cash'];
		return $total_fees;
	}
    

    public  function paydte($id = '') {
        $articles = TableRegistry::get('StudentInstallment'); 
        return $articles->find('all')->where(['s_id' =>$id])->first();


	}

    function stname($id = '') {


        $articles = TableRegistry::get('Students'); 
        return $articles->find('all')->where(['s_enroll' =>$id])->first();

		

	}

public function certficate($sid = '', $cid = ''){

     $articles = TableRegistry::get('Certificate'); 
        return $articles->find('all')->where(['Certificate.s_id' => $sid, 'Certificate.course_id' => $cid])->first();

}

public function detestd($ssid = '', $inssid = '') {

    $articles = TableRegistry::get('StudentInstallment'); 
        return $articles->find('all')->where(['StudentInstallment.s_id' => $ssid, 'StudentInstallment.inst_id' => $inssid, 'StudentInstallment.pay_date !=' => '0000-00-00'])->count();

  

}

public function refundstatus($sid = '', $cid = '') {

    $articles = TableRegistry::get('Refund'); 
    return $articles->find('all')->where(['s_id' => $sid, 'course_id' => $cid])->toarray();

}
   
}
