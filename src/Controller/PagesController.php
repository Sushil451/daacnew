<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;  
use Cake\Auth\DefaultPasswordHasher;
use Cake\Datasource\ConnectionManager;
use Razorpay\Api\Api;

include '../vendor/phpmailer/phpmailer/src/PHPMailer.php';
include '../vendor/phpmailer/phpmailer/src/Exception.php';
include '../vendor/phpmailer/phpmailer/src/SMTP.php';
include '../vendor/phpmailer/phpmailer/src/OAuth.php';
include '../vendor/autoload.php';
include '../vendor/tecnickcom/tcpdf/tcpdf.php';
include '../vendor/PHPExcel/Classes/PHPExcel.php';
include '../vendor/PHPExcel/Classes/PHPExcel/IOFactory.php';

class PagesController extends AppController
{
  public function beforeFilter(Event $e)
  {
    $this->Auth->allow(['home','course','about','privacy','ourteam','studentstestimonials','downloadapp','placementsdaac','faq','contact','sendEnquiryMail','sendemail']);
  }

  public function initialize(){ 
    $this->loadModel('Users');
   
    parent::initialize();
  }

  public function isauth(){
    return $this->redirect($this->referer());;
  }



   public function home(){
   
    $this->viewBuilder()->layout('homepage');
    $this->loadModel('Sliders');
    $this->loadModel('Homepopup');
    $this->loadModel('Resume');
    $this->loadModel('Successstory');
    $this->loadModel('Students');
    $this->loadModel('Users');
    $this->loadModel('Course');
    $this->loadModel('StudentPortfolio');
    $this->loadModel('Placement');
    $scc = $this->Successstory->find('all')->toarray();
    $resu = $this->Resume->find('all')->toarray();
    $ab = $this->Sliders->find('all')->where(['Sliders.status'=>'Y','Sliders.type'=>'web'])->order(['id' => 'DESC'])->toarray();
    $student = $this->Students->find('all')->order(['s_id' => 'DESC'])->count();
    $user = $this->Users->find('all')->where(['Users.id'=>'1'])->first();
    $hompop = $this->Homepopup->find('all')->first();
     $courses = $this->Course->find('all')->where(['Course.category'=>'3'])->order(['Course.sort_order'=>'ASC'])->limit('6')->toarray();
     $courses2 = $this->Course->find('all')->where(['Course.sort_order !='=>'0'])->order(['Course.sort_order'=>'ASC'])->limit('6')->toarray();
     $student1 = $this->Successstory->find('all')->toarray();
     $studentportfolio = $this->StudentPortfolio->find('all')->contain(['Users'])->order(['StudentPortfolio.updated'=>'Desc'])->limit('10')->toarray();

$this->set('destination', $studentportfolio);
$cid=array('5','6','7');


$mid=array('8','9','10');
$logo = $this->Placement->find('all')->where(['Placement.company !='=>'happy technoac','Placement.logo !='=>''])->toarray();

    $this->set('slide',$ab);	
  $this->set('success',$scc);	
              $this->set('res',$resu);	
              $this->set('stu',$student);
              $this->set('Use',$user);
              $this->set('course1',$courses);
              $this->set('course2',$courses2);
  $this->set('student_details',$student1);
  $this->set('company_logo',$logo);
  $this->set('hompop',$hompop);

}

public function course($url=null){
$this->viewBuilder()->layout('homepage');
$this->loadModel('Sliders');
$this->loadModel('Course');
$courses = $this->Course->find('all')->where(['Course.url' => $this->request->url])->order(['Course.id' => 'Desc'])->toarray();
$course_image = $this->Course->find('all')->select(['image','url'])->where(['Course.category' =>trim($courses[0]['category']), 'Course.status' => '1', 'Course.feature' => 'Y'])->order(['Course.id' => 'Desc'])->toarray();
$ab = $this->Sliders->find('all')->where(['Sliders.status'=>'Y','Sliders.type'=>'web'])->order(['id' => 'DESC'])->toarray();
$this->set('courses', $courses);
$this->set('cou_image', $course_image);
  }

	public function about(){
    $this->loadModel('Staticpages');
    $this->viewBuilder()->layout('homepage');
    $about = $this->Staticpages->find('all')->where(['Staticpages.id' =>1])->order(['Staticpages.id' => 'Desc'])->toarray();
    $this->set('destination',$about);
   
   }

	public function privacy(){
    $this->loadModel('Staticpages');
    $this->viewBuilder()->layout('homepage');
    $privacy = $this->Staticpages->find('all')->where(['Staticpages.id' =>4])->order(['Staticpages.id' => 'Desc'])->toarray();
    $this->set('destination',$privacy);
   
   }

   public function ourteam(){
    $this->viewBuilder()->layout('homepage');

  }
  public function studentstestimonials(){
	
    $this->viewBuilder()->layout('homepage');
    $this->loadModel('Placement');
  $this->loadModel('Successstory');

  $logo = $this->Placement->find('all')->select(['Placement.logo','Placement.company'])->where(['Placement.logo !='=>''])->toarray();
  $scc1 = $this->Successstory->find('all')->toarray();
  $this->set('success',$scc1);	
  $this->set('company_logo',$logo);
   
   
   
   }


   public function downloadapp(){
	
    $this->viewBuilder()->layout('homepage');
  
    $this->loadModel('Staticpages');
    $app = $this->Staticpages->find('all')->where(['Staticpages.id' =>3])->order(['Staticpages.id' => 'Desc'])->toarray();

  $this->set('apps',$app);
    
    }

    public function placementsdaac(){
    $this->loadModel('Placement');
    $this->viewBuilder()->layout('homepage');
    $this->loadModel('Resume');
  $placemnt = $this->Resume->find('all')->where(['Resume.type !='=>2])->order(['Resume.created'=>'desc'])->toarray();
  $logo1 = $this->Placement->find('all')->select(['Placement.logo','Placement.company'])->where(['Placement.logo !='=>''])->toarray();
  $testi = $this->Placement->find('all')->order(['Placement.id'=>'DESC'])->toarray();
  $this->set('testi',$testi);
  $this->set('plcemnt',$placemnt);
  $this->set('company_logo2',$logo1);	  
      }

  public function faq(){
    $this->loadModel('Faq');
    $this->viewBuilder()->layout('homepage');
    $ques = $this->Faq->find('all')->order(['Faq.id'=>'desc'])->toarray();
    $this->set('ques',$ques);
   }

  public function contact(){
  $this->loadModel('Staticpages'); 
  $this->viewBuilder()->layout('homepage');
  $contact = $this->Staticpages->find('all')->where(['Staticpages.id'=>'2'])->order(['Staticpages.id'=>'desc'])->toarray();
  $this->set('destination',$contact);
   }

   public function get_client_ip_env() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP')) {
			$ipaddress = getenv('HTTP_CLIENT_IP');
		} else if (getenv('HTTP_X_FORWARDED_FOR')) {
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		} else if (getenv('HTTP_X_FORWARDED')) {
			$ipaddress = getenv('HTTP_X_FORWARDED');
		} else if (getenv('HTTP_FORWARDED_FOR')) {
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		} else if (getenv('HTTP_FORWARDED')) {
			$ipaddress = getenv('HTTP_FORWARDED');
		} else if (getenv('REMOTE_ADDR')) {
			$ipaddress = getenv('REMOTE_ADDR');
		} else {
			$ipaddress = 'UNKNOWN';
		}

		return $ipaddress;
	}

   public function sendEnquiryMail() {
		

    $this->request->data['g-recaptcha-response']="text";
	//	if(!empty($this->request->data['g-recaptcha-response'])){
	//	$secret = '6LdUiWIaAAAAABGfdwy9lZlehiUi9c2fVf3qvNHq';	
	//	$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$this->request->data['g-recaptcha-response']);
	//	$responseData = json_decode($verifyResponse);
		
	//	if($responseData->success)  {
	//	$this->autoRender = false;
		$curdate = date('Y-m-d');
		$ip = $this->get_client_ip_env();
		$this->loadModel('BlockIp');

    $ip_count = $this->BlockIp->find('all')->where(['ip' => $ip])->order(['BlockIp.id'=>'desc'])->count();
  
		$this->loadModel('SpamWord');
		$this->loadModel('Course');
		$this->loadModel('Enquiry');
		$words = $this->SpamWord->find('all')->toarray();
		$spam_word_found = false;
		foreach ($words as $word) {
			if (strpos($this->request->data['comment'], $word['word'], 0)) {
				$spam_word_found = true;
				break;
			}
		}
		$this->request->data['name']=strip_tags($this->request->data['name']);
		$this->request->data['comment']=strip_tags($this->request->data['comment']);
		$this->request->data['comment']=preg_replace('/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '',$this->request->data['comment']);
		
		if (!preg_match('/^\d{10}$/', $this->request->data['phone'])) {
			$this->redirect(SITE_URL . "contact.html?d=Thank You");
		  } 
	
		if (!empty($this->request->data['g-recaptcha-response']) && ($ip_count == 0) && $spam_word_found == false) {

			if ($this->request->data['mode'] == 7) {
				$type = "Google Adwords";
			} else {
				$type = "Website";
			}
			$secret = "6Lf4B1oUAAAAAHuGwyJNUFJUXpkfJg4D-Aai_fy7";
			$ip = $_SERVER['REMOTE_ADDR'];
			$capcta = $this->request->data['g-recaptcha-response'];
			
			if (1) {

			

        $course = $this->Course->find('all')->select(['cname'])->where(['Course.id'=>$this->request->data['course']])->order(['Course.id'=>'desc'])->first(); 
				

				$MesageSubject = "Enquiry For course " . $course['cname'];
				$MessageBody = '<b>NAME: </b>' . $this->request->data['name'] . '<br/><b>PHONE: </b>' . $this->request->data['phone'] . '<br/><b>EMAIL: </b>' . $this->request->data['email'] . '<br/><b>DESCR: </b>' . $this->request->data['comment'] . '<br/><b>COURSE: </b>' . $course['cname'] . '<BR><b>Ip Address: </b>' . $_SERVER['REMOTE_ADDR'];

				$headers = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$to = 'vikas@doomshell.com';
				$headers .= 'From: <sales@doomshell.com>' . "\r\n";

        $conns = ConnectionManager::get('default');
        $query1 ="insert into tblenquiry set name='" . strip_tags($this->request->data['name']) . "',phone='" . $this->request->data['phone'] . "',email='" . $this->request->data['email'] . "',enquiry='" . strip_tags($this->request->data['comment']) . "',course_id='" . strip_tags($this->request->data['course']) . "',type='" . $type . "',status=1,branch_id=1,reference=5,c_id=1,add_date='" . $curdate . "',ip='" . $ip . "',security_code='" . $this->request->data['security_code1'] . "'";
        $conns->execute($query1);
			

			
				$conns = ConnectionManager::get('default');
				$enq_id = 'select enq_id from tblenquiry where phone like "%' . $this->request->data['phone'] . '%" order by enq_id desc limit 1';
        $conns->execute($enq_id);

        $enquiry = $this->Enquiry->find('all')->select(['enq_id'])->order(['Enquiry.enq_id'=>'desc'])->first(); 

        $conns = ConnectionManager::get('default');
				$follow_id = "insert into followup set enq_id='" . $enquiry['enq_id'] . "',f_responce='" . $this->request->data['comment'] . "',f_date='" . $curdate . "',add_date='" . $curdate . "',status=1";
        $conns->execute($follow_id);
        


				$this->sendemail($to, $MesageSubject, $MessageBody);


				$this->Flash->success(__('Enquiry has been saved.'));
				//$this->Session->setFlash(__('The Enquiry has been important'), 'flash/sucess');
				if ($this->request->data['mode'] == 7) {
					$this->redirect(SITE_URL . "thank.html");
				} else {
					$this->redirect(SITE_URL . "contact.html?d=Thank You");
				}

			}

		} else {
			$this->redirect(SITE_URL);
		}
	/*  }
	  else{
		$this->Session->setFlash(__("Please provide us a valid captcha"), 'default', array(), 'editprope');
		$this->redirect(SITE_URL);
	  }
    } */
   }


   
public function sendemail($to,$subject,$message){
  $mail = new \PHPMailer(True);
  $mail->isSMTP();
 //    $mail->SMTPDebug = \SMTP::DEBUG_SERVER;
  $mail->Host ='smtp.gmail.com';
  $mail->SMTPAuth = true;
  $mail->Username = 'sales@doomshell.com';
  $mail->Password = 'ttsjlriymmsagbjs';
  $mail->SMTPSecure = \PHPMailer::ENCRYPTION_STARTTLS; 
  //  $mail->tls = true;
  $mail->Port = 587; 
  $mail->From = 'sales@doomshell.com';
  $mail->FromName = 'DAAC';
  $mail->addAddress($to);
   // $mail->addAttachment(sys_get_temp_dir() . '/'.$filename,$filename);
  $mail->isHTML(true);
  $mail->Subject = $subject;
  $mail->Body    =  $message;
  if(!$mail->send()) {
   return 0;
 }
 return 1;
 }


}
