<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;


include '../vendor/tecnickcom/tcpdf/tcpdf.php';


class FeesController extends AppController
{ 
	public function initialize(){	
		
		parent::initialize();
	}



    public function pendiscount($id='',$enroll='',$course=''){ //pr($id); die;
		$this->loadModel('Discount');	
		$this->loadModel('Students');	
		$this->loadModel('Course');	
		$this->loadModel('StudentCourse');
		$this->loadModel('Installment');
		$this->loadModel('StudentInstallment');
		//$dis_count=$this->Discount->find('count',array('conditions'=>array('Discount.enrollment'=>$enroll)));
		//pr($dis_count);

        $stud = $this->Students->find('all')->contain(['Branch'])->where(['Students.s_enroll' =>$enroll])->order(['s_id' => 'DESC'])->first();

        $sid=$stud['s_id'];        
        $this->set('stu_det', $stud);
  
 $transtn = $this->Discount->find('all')->contain(['Branch'])->where(['Discount.id' =>$id])->order(['Discount.id' => 'DESC'])->first();
 $this->set('discount', $transtn);

	if($this->request->is('post') || $this->request->is('put'))
	{ 

     
		
		//pr($this->request->data); die;
        $discount_data=$this->request->data;
		if($this->request->data['submit']=='Approve'){
			$en=$this->request->data['enrollment'];
            $enroll = $this->Students->find('all')->contain(['Branch'])->where(['Students.s_enroll' =>$en])->order(['s_id' => 'DESC'])->first();

	//pr($enroll); die;
	$sid=$enroll['s_id'];
	$course=$this->request->data['course_id'];
	//echo $course; die;
	$ty='1'; 
    	
$conns = ConnectionManager::get('default');
$query1 ="update cms_student_courses set discount='".$ty."' where course_id='".$course."'AND s_id='".$sid."'";
$conns->execute($query1);
		
	
		$discount=$this->request->data['discount'];
		

        $result = $this->Discount->find('all')->contain(['Branch'])->where(['Discount.id' =>$this->request->data['id']])->order(['Discount.id' => 'DESC'])->first();

		$ry=explode(",",$result['enrollment']);
		//pr($ry);die;
			foreach($ry as $enrollen){
				
				
				
				$amount=0;

                $d = $this->Students->find('all')->contain(['Branch'])->where(['Students.s_enroll' =>$enrollen])->order(['s_id' => 'DESC'])->first();




     $installment_ids=$this->Installment->find('all')->where(['course_id'=>$this->request->data['course_id']])->order(['inst_id' => 'ASC'])->toarray();
		
		
			$iids_arr=array();
			foreach($installment_ids as $iids){
				$iids_arr[]=$iids['inst_id'];
			}
            $ds=$this->StudentInstallment->find('all')->where(['StudentInstallment.s_id'=>$d['s_id'],'StudentInstallment.inst_id IN'=>$iids_arr])->order(['sid_id' => 'DESC'])->toarray();


			
			
				foreach($ds as $studentenroll){
				$amount+=$studentenroll['amount'];
				
				
			}
			if($this->request->data['dtype']==2){
				$rt=$discount*($amount/100);
				
			}else{
				$rt=$discount;
			}
				
				
				
				
				
				
				
				foreach($ds as $studentenroll){
				
				if($rt<$studentenroll['amount']){
					
				$rt=$studentenroll['amount']-$rt;	
				
        $conns = ConnectionManager::get('default');
        $query1 ="update tbl_student_installment set amount='".$rt."' where sid_id ='".$studentenroll['sid_id']."'";
        $conns->execute($query1);
     	$rt=0;
			}else if($rt>=$studentenroll['amount']){
						
		$rt=$rt-$studentenroll['amount'];
						
        $conns = ConnectionManager::get('default');
         $query1 ="delete from tbl_student_installment where sid_id=".$studentenroll['sid_id'];
            $conns->execute($query1);				

				
						
						}
				 
				 }
				
                 $tu=$this->StudentInstallment->find('all')->where(['StudentInstallment.s_id'=>$d['s_id'],'StudentInstallment.inst_id IN'=>$iids_arr])->order(['sid_id' => 'DESC'])->toarray();
	
					
			foreach($tu as $a){
		    $amounts+=$a['amount'];
			
             }
				 
				 
            $conns = ConnectionManager::get('default');
            $query1 ="INSERT INTO `tbl_reportdiscount` (`s_id`, `branch`, `discount`,  `course_fee`, `afdiscount_fee`,`course_id`,`course_name`,`comment`) VALUES ('".$d['s_id']."',  '".$d['branch_id']."', '".$discount."', '".$amount."', '".$amounts."','".$discount_data['course_id']."','".$discount_data['course_name']."','".$this->request->data['comment']."')";
            $conns->execute($query1);	 
			$amounts=0;
			
		}
	
	
        $conns = ConnectionManager::get('default');
        $query1 ="update discounts set status='A', comment='".$this->request->data['comment']."' where id=".$this->request->data['id'];
        $conns->execute($query1);	 


        $this->Flash->flash("Discount approved successfully for selected student.", [
            "params" => [
            "type" => "success"
            ]
            ]);
          $this->redirect(array('action' => 'discount'));
		
		
		
		
		
		
		}else{
            $conns = ConnectionManager::get('default');
            $query1 ="update discounts set status='R', comment='".$this->request->data['comment']."' where id=".$this->request->data['id'];
            $conns->execute($query1);	


            $this->Flash->flash("Discount rejected successfully for selected student.", [
                "params" => [
                "type" => "error"
                ]
                ]);
              $this->redirect(array('action' => 'discount'));
			}





		
	
	}		
	
		

		
	}

    public function statuschange($id=null,$status=null)
    {
        $this->loadModel('Users');
        $this->set('id', $id);
        $this->set('status', $status);
        $role_id = $this->request->session()->read('Auth.User.role_id');
        if ($role_id == '1') {
            $userId = $this->request->session()->read('Auth.User.id');
            $mob = $this->request->session()->read('Auth.User.mobile');
            $myotp = rand(1111, 9999);
            $mesg = "OTP for fee approval/pending is " . $myotp;
            $simple_string = $myotp;
            $decryption_iv = '1234567891011121';
            $ciphering = "AES-128-CTR";
            $encryption_iv = '1234567891011121';
            $encryption_key = "GeeksforGeeks";
            $encryption = openssl_encrypt($simple_string, $ciphering,
                $encryption_key, $options, $encryption_iv);

                $conns = ConnectionManager::get('default');
            $query1 ="update users set otp='" . $encryption . "' where id=" . $userId;
            $conns->execute($query1);
           // $mob='9694027991';
            $sms = 'http://alerts.prioritysms.com/api/web2sms.php?workingkey=A2960bddf6f159a76d113973b6831bf79&to=' . trim($mob) . '&sender=DAACIN&message=' . urlencode($mesg);
            $result = $this->file_get_contents_curl($sms);
          
        }

      


    }



    public function updatestatus()
    {
        $this->loadModel('Users');
        $role_id = $this->request->session()->read('Auth.User.role_id');
      

        if ($this->request->is('post')) {
        $role_id = $this->request->session()->read('Auth.User.role_id');
        if ($role_id == '1') {
            $userId = $this->request->session()->read('Auth.User.id');

 $user=$this->Users->find('all')->where(['Users.id'=>$userId])->order(['id' => 'DESC'])->first();
  $encryption = $user["otp"];
            $decryption_iv = '1234567891011121';
            $ciphering = "AES-128-CTR";
            $options = 0;
            $decryption_key = "GeeksforGeeks";
            $decryption = openssl_decrypt($encryption, $ciphering,
                $decryption_key, $options, $decryption_iv);

              
            if ($decryption == $this->request->data['otp']) {
                $this->loadModel('Feesubmission');
               $status=$this->request->data['status'];
               $id=$this->request->data['id'];
                $ip = $_SERVER['REMOTE_ADDR'];

                $conns = ConnectionManager::get('default');
            $query1 ="update fee_submission set status ='" . $status . "',ip_address='" . $ip . "' where id=" . $id;
            $conns->execute($query1);

             
              
            $this->Flash->flash("Status Updated Successfully.", [
                "params" => [
                "type" => "success"
                ]
                ]);
                $this->redirect(array('action' => 'submit'));
            } else {
                $this->Flash->flash("Invalid OTP.", [
                    "params" => [
                    "type" => "success"
                    ]
                    ]);
                $this->redirect(array('action' => 'submit'));
            }
        }


    }


    }

    public function submit()
    {

        $this->loadModel('Feesubmission');
        $this->loadModel('Cash');
        $this->loadModel('Branch');
        $this->viewBuilder()->layout('admin');
        
        $id = $this->request->session()->read('Auth.User.francheeid');
        
        $br1 = $this->Branch->find('list', ['keyField' => 'id','valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();
			
        $this->set('brn',$br1);
        $date = date('m-Y');
        $date = explode('-', $date);
        $month = $date[0];
        $year = $date[1];
        $date1 = date('Y-m-d');
        $prev_date = date('m-Y', strtotime('-1 month', strtotime($date1)));

        if ($this->request->session()->read('Auth.User.role_id') == 1) {
            

            $studentsall = $this->Feesubmission->find('all')->contain(['Branch'])->where(['YEAR(Feesubmission.date)' => $year])->order(['Feesubmission.date' => 'DESC']);
       $destination=$this->paginate($studentsall)->toarray();
      
            $this->set('cashes', $destination);

           
        } else {


            $branch = $this->request->session()->read('Auth.User.branch');
            $articles = $this->Feesubmission; 

            $appr_amt = $this->Feesubmission->find('all')->select(['ctotal' => $articles->find('all')->func()->sum('Feesubmission.amount')])->where(['MONTH(Feesubmission.date)' => $month, 'YEAR(Feesubmission.date)' => $year, 'Feesubmission.status' => 'A', 'Feesubmission.branch_id' => $branch])->first();


            $pen_amt = $this->Feesubmission->find('all')->select(['ctotal' => $articles->find('all')->func()->sum('Feesubmission.amount')])->where(['MONTH(Feesubmission.date)' => $month, 'YEAR(Feesubmission.date)' => $year, 'Feesubmission.status' => 'P', 'Feesubmission.branch_id' => $branch])->first();



            $studentsall = $this->Feesubmission->find('all')->where(['Feesubmission.branch_id' => $branch, 'MONTH(Feesubmission.date)' => $month, 'YEAR(Feesubmission.date)' => $year])->order(['Feesubmission.id' => 'Desc']);

            $destination=$this->paginate($studentsall)->toarray();
   $this->set('cashes', $destination);
            $articless = $this->Cash;
            $total_fees = $this->Cash->find('all')->select(['ctotal' => $articless->find('all')->func()->sum('Cash.amount')])->where(['MONTH(Cash.pay_date)' => $month, 'YEAR(Cash.pay_date)' => $year, 'Cash.status' => 'Y', 'Cash.is_exec' => 'N', 'Cash.branch_id' => $branch, 'Cash.mode !=' => ''])->first();



            $total_fine = $this->Cash->find('all')->select(['ctotal' => $articless->find('all')->func()->sum('Cash.fine')])->where(['MONTH(Cash.pay_date)' => $month, 'YEAR(Cash.pay_date)' => $year, 'Cash.status' => 'Y', 'Cash.is_exec' => 'N', 'Cash.branch_id' => $branch, 'Cash.mode !=' => ''])->first();

          

// print_r($total_fine); die;


$total_cash = $this->Cash->find('all')->select(['ctotal' => $articless->find('all')->func()->sum('Cash.amount')])->where(['MONTH(Cash.pay_date)' => $month, 'YEAR(Cash.pay_date)' => $year, 'Cash.status' => 'Y', 'Cash.is_exec' => 'N', 'Cash.branch_id' => $branch, 'Cash.mode' => 'cash'])->first();


$total_bank = $this->Cash->find('all')->select(['ctotal' => $articless->find('all')->func()->sum('Cash.amount')])->where(['MONTH(Cash.pay_date)' => $month, 'YEAR(Cash.pay_date)' => $year, 'Cash.status' => 'Y', 'Cash.is_exec' => 'N', 'Cash.branch_id' => $branch, 'Cash.mode' => 'bank'])->first();




$total_paytm = $this->Cash->find('all')->select(['ctotal' => $articless->find('all')->func()->sum('Cash.amount')])->where(['MONTH(Cash.pay_date)' => $month, 'YEAR(Cash.pay_date)' => $year, 'Cash.status' => 'Y', 'Cash.is_exec' => 'N', 'Cash.branch_id' => $branch, 'Cash.mode' => 'paytm'])->first();


$presentDate = date('Y-m-1');
$p_mon_fees = $this->Cash->find('all')->select(['ctotal' => $articless->find('all')->func()->sum('Cash.amount')])->where(['Cash.pay_date <' => $presentDate, 'Cash.pay_date >=' => '2019-08-01', 'Cash.status' => 'Y', 'Cash.is_exec' => 'N', 'Cash.branch_id' => $branch, 'Cash.mode !=' => ''])->first();

$artiscless = $this->Feesubmission;
$p_appr_amt = $this->Feesubmission->find('all')->select(['ctotal' => $artiscless->find('all')->func()->sum('Feesubmission.amount')])->where(['Feesubmission.date <' => $presentDate, 'Feesubmission.date >=' => '2019-08-01', 'Feesubmission.status' => 'A', 'Feesubmission.branch_id' => $branch])->first();
           
          
          
          
            $prev_due = $p_mon_fees['ctotal'] - $p_appr_amt['ctotal'];
            $this->set(compact('total_fees', 'total_cash', 'total_bank', 'total_paytm', 'appr_amt', 'pen_amt', 'prev_due', 'total_fine'));
        }

        if ($this->request->is('post')) {
            $this->request->data['date'] = date('Y-m-d', strtotime($this->request->data['date']));
            // $this->request->data['date'] = date('Y-m-d');
            $this->request->data['created'] = date("Y-m-d H:i:s");
            $data['branch_id'] = $this->request->session()->read('Auth.User.branch');
            $this->request->data['branch_id'] = $this->request->session()->read('Auth.User.branch');

            $ip = $_SERVER['REMOTE_ADDR'];
            $prev_date = date('d-m-Y', strtotime('-1 month', strtotime($this->request->data['date'])));



            $artiscless = $this->Feesubmission;
            $appr_amt = $this->Feesubmission->find('all')->select(['ctotal' => $artiscless->find('all')->func()->sum('Feesubmission.amount')])->where(['MONTH(Feesubmission.date)' => $month, 'YEAR(Feesubmission.date)' => $year, 'Feesubmission.status' => 'A', 'Feesubmission.branch_id' => $data['branch_id']])->first();


            $appr_amt = $this->Feesubmission->find('all')->select(['ctotal' => $artiscless->find('all')->func()->sum('Feesubmission.amount')])->where(['MONTH(Feesubmission.date)' => $month, 'YEAR(Feesubmission.date)' => $year, 'Feesubmission.status' => 'P', 'Feesubmission.branch_id' => $data['branch_id']])->first();


          
            $appr_amt = $this->Feesubmission->find('all')->select(['ctotal' => $artiscless->find('all')->func()->sum('Feesubmission.amount')])->where(['MONTH(Feesubmission.date)' => $month, 'YEAR(Feesubmission.date)' => $year, 'Feesubmission.status' => 'P', 'Feesubmission.branch_id' => $data['branch_id']])->first();


          
            $artisclecss = $this->Cash;

            $total_fees = $this->Cash->find('all')->select(['ctotal' => $artisclecss->find('all')->func()->sum('Cash.amount')])->where(['MONTH(Cash.pay_date)' => $month, 'YEAR(Cash.pay_date)' => $year, 'Cash.status' => 'Y', 'Cash.is_exec' => 'N', 'Cash.branch_id' => $data['branch_id'], 'Cash.mode !=' => ''])->first();



            if (empty($total_fees['ctotal'])) {
                $total_fees['ctotal'] = 0;
            }
            if (empty($appr_amt['ctotal'])) {
                $appr_amt['ctotal'] = 0;
            }
            if (empty($pen_amt['ctotal'])) {
                $pen_amt['ctotal'] = 0;
            }
           
            $presentDate = ('Y-m-1');



            $p_mon_fees = $this->Cash->find('all')->select(['ctotal' => $artisclecss->find('all')->func()->sum('Cash.amount')])->where(['Cash.pay_date <' => $presentDate, 'Cash.pay_date >=' => '2019-08-01', 'Cash.status' => 'Y', 'Cash.is_exec' => 'N', 'Cash.branch_id' => $data['branch_id'], 'Cash.mode !=' => ''])->first();


            $p_appr_amt = $this->Feesubmission->find('all')->select(['ctotal' => $artiscless->find('all')->func()->sum('Feesubmission.amount')])->where(['Feesubmission.date <' => $presentDate, 'Feesubmission.date >=' => '2019-08-01', 'Feesubmission.status' => 'A', 'Feesubmission.branch_id' => $data['branch_id']])->first();


            $prev_due = $p_mon_fees['ctotal'] - $p_appr_amt['ctotal'];

            $bal = ($prev_due + $total_fees['ctotal']) - ($appr_amt['ctotal'] + $pen_amt['ctotal']);

            if ($this->request->data['amount'] > $bal) {

                
            $this->Flash->flash("Amount exceeded total collection.", [
                "params" => [
                "type" => "error"
                ]
                ]);
              
                $this->redirect(array('action' => 'submit'));
            }


            $studentcash = $this->Feesubmission->newEntity();


            $savepackCourse = $this->Feesubmission->patchEntity($studentcash, $this->request->data);

            
            $this->Feesubmission->save($savepackCourse);


            $mob = 9829732221;
            $mesg1 = "Rs." . $this->request->data['amount'] . " added by " . $br1[$this->request->data['branch_id']];
            $sms = 'http://alerts.prioritysms.com/api/web2sms.php?workingkey=A2960bddf6f159a76d113973b6831bf79&to=' . trim($mob) . '&sender=DAACIN&message=' . urlencode($mesg1);
            $result = $this->file_get_contents_curl($sms);
           
            $this->Flash->flash("Fee Submission is Added successfully.", [
                "params" => [
                "type" => "success"
                ]
                ]);

            $this->redirect(array('action' => 'submit'));
        }

        //print_r($this->paginate('Feesubmission'));    die;
      
    }

    public function approvdiscount($id='',$enroll='',$course=''){ //pr($id); die;
		$this->loadModel('Discount');	
		$this->loadModel('Students');	
		$this->loadModel('Course');	
		$this->loadModel('StudentCourse');
		//$dis_count=$this->Discount->find('count',array('conditions'=>array('Discount.enrollment'=>$enroll)));
		//pr($dis_count);

        $stud = $this->Students->find('all')->contain(['Branch'])->where(['Students.s_enroll' =>$enroll])->order(['s_id' => 'DESC'])->first();

        $sid=$stud['s_id'];        
        $this->set('stu_det', $stud);
  
 $transtn = $this->Discount->find('all')->contain(['Branch'])->where(['Discount.id' =>$id])->order(['Discount.id' => 'DESC'])->first();
 $this->set('discount', $transtn);


	
		

		
	}


    public function discountdelete($id = null) {
        $this->loadModel('Discount');
        $catdeleteer =$this->Discount->get($id);
          $this->Discount->delete($catdeleteer);
       
               $this->Flash->flash("Discount approved successfully for selected student.", [
                "params" => [
                "type" => "success"
                ]
                ]);
              $this->redirect(array('action' => 'discount'));
       
             
       
           }
	public function discount()
    {
		$this->viewBuilder()->layout('admin');
        $this->loadModel('Discount');
	    $this->loadModel('Users');
	    $this->loadModel('Branch');
	    $this->loadModel('Students');
	    
	    $br1 = $this->Branch->find('list', ['keyField' => 'name','valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();
			
        $this->set('brn',$br1);
	    $role=$this->request->session()->read('Auth.User.role_id'); //pr($role); die;
	    $this->set('role',$role); 
	    
	    $branch=$this->request->session()->read('Auth.User.branch'); //pr($branch); die;
	    
	if($role=='1'){


      $studentsall = $this->Discount->find('all')->contain(['Branch'])->order(['Discount.id' => 'DESC']);

      $destination=$studentsall->toarray();

      $this->set('rec', $destination);
  } else {

    $studentsall = $this->Discount->find('all')->contain(['Branch'])->where(['Discount.branch_id'=>$branch])->order(['Discount.id' => 'DESC']);

    $destination=$studentsall->toarray();

    $this->set('rec', $destination);


  }
		
 if($this->request->is('post')){ 
      


       $transtn = $this->Discount->newEntity();
       $this->request->data['created'] = date('Y-m-d');
       $this->request->data['comment'] = "";
       $this->request->data['branch_id'] =$this->request->session()->read('Auth.User.branch');
       $this->request->data['status'] ="P";
       $dasdasd = $this->Discount->patchEntity($transtn, $this->request->data);
    
      $re= $this->Discount->save($dasdasd);
  
  

  
            $this->Flash->flash("Selected Student Discount has been updated.", [
              "params" => [
                "type" => "success"
              ]
              ]);
            $this->redirect(array('action' => 'discount'));



   

}


	

    }
	

	public function index($id=null)
    {
		$this->viewBuilder()->layout('admin');
        $this->loadModel('StudentCourse');
        $this->loadModel('StudentInstallment');
        $this->loadModel('Cash');
        $this->loadModel('Students');
        $this->loadModel('Reportdiscount');
        $this->loadModel('Refund');
        $this->loadModel('Discount');
        $this->loadModel('Course');

		$studentsall = $this->StudentInstallment->find('all')->contain(['Students','Course'])->where(['StudentInstallment.s_id' =>$id])->order(['StudentInstallment.course_id' => 'ASC','StudentInstallment.pro_date' => 'ASC'])->limit(50);

		$destination=$this->paginate($studentsall)->toarray();
       
		
	$stu_det = $this->Students->find('all')->contain(['Branch','StudentCourse'])->where(['Students.s_id' =>$id])->first();
	
       
        $this->set(compact('stu_det'));

        $this->set('destination', $destination);

		$branch = $this->request->session()->read('Auth.User.branch');
		$role_id = $this->request->session()->read('Auth.User.role_id');
       
  
        $this->set('role_id', $role_id);
        $branchhead = $this->request->session()->read('Auth.User.branchhead');

		$payment = $this->StudentInstallment->find('all')->where(['StudentInstallment.s_id' =>$id,'StudentInstallment.recipt_no !=' => 0])->order(['StudentInstallment.pay_date' => 'ASC'])->toarray();

        $pay_his = array();
        if (!empty($payment)) {
            foreach ($payment as $pay_value) {
                $paid_data = array();
                $paid_data['sid_id'] = $pay_value['sid_id'];

                $paid_data['s_id'] = $pay_value['s_id'];
                $paid_data['fine'] = $pay_value['fine'];
                $paid_data['amount'] = $pay_value['amount'];
                $paid_data['receipt_no'] = $pay_value['recipt_no'];
                $paid_data['course_id'] = $pay_value['course_id'];
                $paid_data['pay_date'] = $pay_value['pay_date'];
                $pay_his[] = $paid_data;
            }
        }

		$new_payment = $this->Cash->find('all')->where(['Cash.s_id' =>$id,'Cash.status' => 'Y', 'Cash.receipt_no !=' => 0])->order(['Cash.pay_date' => 'DESC'])->toarray();

      

        if (!empty($new_payment)) {
            $paid_data = array();

            foreach ($new_payment as $npay_value) {
                $paid_data['is_reg'] = $npay_value['is_reg'];
                $paid_data['s_id'] = $npay_value['s_id'];
                $paid_data['amount'] = $npay_value['amount'];
                $paid_data['fine'] = $npay_value['fine'];
                $paid_data['receipt_no'] = $npay_value['receipt_no'];
                $paid_data['course_id'] = $npay_value['cid'];
                $paid_data['pay_date'] = $npay_value['pay_date'];
                $pay_his[] = $paid_data;
            }
        }

        //pr($pay_his); die;
        $this->set('pay_his', $pay_his);

		$refund_det = $this->Refund->find('all')->where(['Refund.s_id' =>$id])->toarray();

        $this->set('refund_det', $refund_det);
        
        $stu_branch = $stu_det['student_course'][0]['branch'];
        $this->set(compact('stu_branch'));
        $enrol = $stu_det['s_enroll'];

        $stu_course = array();
        $course_ids = array();

		
        foreach ($stu_det['student_course'] as $det) {


			$payment = $this->StudentInstallment->find('all')->where(['StudentInstallment.s_id' =>$id,'StudentInstallment.recipt_no !=' => 0])->order(['StudentInstallment.pay_date' => 'ASC'])->toarray();


			$articles = $this->StudentInstallment; 
			$total1_fees = $this->StudentInstallment->find('all')->select(['ctotal' =>$articles->find('all')->func()->sum('amount')])->where(['StudentInstallment.s_id' => $id, 'StudentInstallment.course_id' => $det['course_id'], 'StudentInstallment.amount !=' => '250'])->first();


         
            //dd($total_fees); die;

            // if ($det['is_change'] == 'N' && $det['drop_out'] != 1) {
            if ($det['is_change'] == 'N') {
                $course_ids[] = $det['course_id'];
                $course['fee_paid'] = 0;
                $course['course_id'] = $det['course_id'];


				$course_name = $this->Course->find('all')->where(['Course.id' =>$det['course_id']])->first();
				$discount = $this->Discount->find('all')->where(['Discount.enrollment LIKE' => $enrol . '%', 'Discount.course_id' => $det['course_id'], 'Discount.status' => 'A'])->first();

				
              
                if (!empty($discount)) {

                    if ($discount['dtype'] == 2) {
                        $course['dis'] = ($det['fee'] * $discount['discount']) / 100;
                    } else {
                        $course['dis'] = $discount['discount'];
                    }
                } else {
                    $course['dis'] = 0;
                }

                $course['name'] = $course_name['sname'];

                if (!empty($pay_his)) {
                    foreach ($pay_his as $pay) {
                        if ($pay['course_id'] == $det['course_id'] && $pay['is_reg'] != 1 && $pay['amount'] !== "250") {
                            $course['fee_paid'] += $pay['amount'];
                        }
                    }
                }

                // $course['fees']=$det['fee'];
                $course['fees'] = $det['fee'];
                $course['drop'] = $det['drop_out'];
                $stu_course[] = $course;
            }
        }
        $this->set('course_ids', $course_ids);
        $this->set('stu_course', $stu_course);
        $this->set('branch', $branch);
        //die;

        if ($this->request->is('post')) {

            $this->loadModel('Setting');
            $this->loadModel('Users');
            $this->loadModel('Cash');
            $this->loadModel('Transaction');
            $this->loadModel('Students');
            $data['s_id'] = $this->request->data['s_id'];
            $data['token'] = $this->request->data['token'];
          		
            $stu_det1 = $this->Students->find('all')->contain(['Branch','StudentCourse'])->where(['Students.s_id' =>$data['s_id']])->first();
	

            $cash_ext = $this->Cash->find('all')->where(['Cash.token' =>$data['token'],'Cash.s_id' =>$data['s_id']])->first();

            if (!empty($cash_ext)) {
                return $this->redirect(array('action' => 'index/' . $data['s_id']));
            }
            
            if (date('m') > 3) {
                $fromYear = date('Y') . "-04-01";
                $toYear = (date('Y') + 1) . "-03-31";
            } else {
                $fromYear = (date('Y') - 1) . "-04-01";
                $toYear = date('Y') . "-03-31";

            }

            $enroll = $this->Students->find('all')->select(['s_enroll','s_name'])->where(['s_id' =>$data['s_id']])->order(['s_id' => 'DESC'])->first();


            $data['enroll'] = $enroll['s_enroll'];
            $data['name'] = $enroll['s_name'];
            $data['branch_id'] = $this->request->data['branch'];

  
   $rec_no = $this->Cash->find('all')->select(['receipt_no'])->where(['branch_id' =>$this->request->data['branch'],'pay_date >=' => $fromYear, 'pay_date <=' => $toYear])->order(['receipt_no' => 'DESC'])->first();



            if (!empty($this->request->data['fine']) || $this->request->data['fine'] != "") {

                $fin = $this->request->data['fine'];
            } else {
                $fin = '0';
            }
            if (!empty($this->request->data['paytm_reference']) || $this->request->data['paytm_reference'] != "") {

                $paytm_reference = $this->request->data['paytm_reference'];
            } else {
                $paytm_reference = '';
            }

            $amt = $this->request->data['amount'];

            $gtot = $amt;


         
    $gst = $this->Setting->find('all')->select(['sgst', 'igst', 'cgst'])->first();

            $typ = '1';
            $cid1 = $this->request->data['cid'];
            $data['mode'] = $this->request->data['mode'];
            $data['cid'] = $this->request->data['cid'];
            $data['pay_date'] = date('Y-m-d');
            $data['fine'] = $fin;
            $data['paytm_reference'] = $paytm_reference;

                $sgst = '0';
                $cgst = '0';
                $igst = '0';
                $igs = 'NULL';
                $cgs = 'NULL';
                $sgs = 'NULL';
                $tot = $gtot;

                $gst_num = '0';
                $gst_inclu = '0';
            
            $data['amount'] = $tot;
            $data['sgst_amount'] = $sgst;
            $data['cgst_amount'] = $cgst;
            $data['igst_amount'] = $igst;
            $data['igst_percent'] = 0;
            $data['cgst_percent'] = 0;
            $data['sgst_percent'] = 0;
            $data['gst_inclu'] = $gst_inclu;
            $data['created']=date('Y-m-d H:i:s');

            $data['branch'] = $this->request->data['branch'];
            if ($this->request->session()->read('Auth.User.role_id')== 1) {
                $data['is_exec'] = $this->request->data['is_exec'];
            }





            $mode = $this->request->data['mode'];
            //dd($fees); die;
            if ($this->request->data['mode'] == 'bank') {
                $data['chequedetail'] = $this->request->data['cheque'];
                $data['bank'] = $this->request->data['bank'];

            } else if ($this->request->data['mode'] == 'paytm') {
                $data['chequedetail'] = $this->request->data['paytm'];
            } else {
                $data['chequedetail'] = 'NULL';
                $data['bank'] = 'NULL';

            }
        
            if (!empty($rec_no['receipt_no'])) {
                $data['receipt_no'] = $rec_no['receipt_no'] + 1;

            } else {
                $data['receipt_no'] = 1001;

            }
            $studentcash = $this->Cash->newEntity();


            $savepackCourse = $this->Cash->patchEntity($studentcash, $data);

            
            if ($this->Cash->save($savepackCourse)) {

                $message = "Dear " . $stu_det1['s_name'] . ", Thank You We have received an amount of Rs. " . $data['amount'] . ", DAAC";

                $sms = 'http://alerts.prioritysms.com/api/web2sms.php?workingkey=A2960bddf6f159a76d113973b6831bf79&to=' . trim($stu_det1['s_contact']) . '&sender=DAACIN&message=' . urlencode($message);

                $result = $this->file_get_contents_curl($sms);
            }
            $cashid = $this->Cash->find('all')->order(['Cash.id'=>'DESC'])->first();

			$cash_id = $cashid['id'];
            $b_data['tran_id'] = $cash_id;
            $b_data['credit'] = $data['amount'];
            $b_data['bank_id'] = 0;
            $b_data['transdate'] = date('Y-m-d 00:00:00');
            $b_data['checknumber'] = $data['chequedetail'];
            $b_data['branch_id'] = $data['branch_id'];
            $b_data['debit'] = 0;
            $b_data['details'] = "";
            $studenttransaction = $this->Transaction->newEntity();

            $savepackTransaction = $this->Transaction->patchEntity($studenttransaction, $b_data);

            
            $this->Transaction->save($savepackTransaction);
            //pr($data); die;

            echo ("<script>
window.open('" . ADMIN_URL . "fees/receiptpdf/" . $data['s_id'] . "/" . $data['receipt_no'] . "/" . $data['cid'] . "');
                       window.location.href='" . ADMIN_URL . "fees/index/" . $data['s_id'] . "';
    </script>");

        }

    }

    public function delete_receipt($id = "", $s_id = "")
    {
        $this->autoRender = false;
        $this->loadModel('Cash');
        $this->loadModel('Transaction');
        
        
   $cash_det = $this->Cash->find('all')->where(['Cash.receipt_no' => $id])->order(['Cash.receipt_no' => 'DESC'])->first();
   $led_det = $this->Transaction->find('all')->where(['Transaction.tran_id' =>$cash_det['id']])->first();

   $catdelete = $this->Transaction->get($led_det['id']);
   $this->Transaction->deleteAll(['Transaction.id' => $led_det['id']]); 
   

   $this->Transaction->delete($catdelete);

     

    $conns = ConnectionManager::get('default');
	$query1 ="update cms_cashes set status ='N' where receipt_no=" . $id;

 
	$conns->execute($query1);
          
     
        $this->Flash->success(__('Payment Receipt has been deleted successfully.'));
        $this->redirect(array('action' => 'index/' . $s_id));

    }
    public function receiptpdf($id = '', $rc_no = '', $cid = '')
    {

        // echo $cid;
        $this->loadModel('StudentInstallment');
        $this->loadModel('Cash');
    $new_receipt = $this->Cash->find('all')->where(['Cash.s_id' =>$id,'Cash.receipt_no' => $rc_no])->order(['Cash.id'=>'DESC'])->first();

    
        //print_r($new_receipt); die;;
        if (!empty($new_receipt)) {
            //pr($new_receipt);
            $status = "new";
            $student = $this->StudentInstallment->find('all')->contain(['Students','Course','Branch'])->where(['StudentInstallment.s_id' => $new_receipt['s_id'], 'StudentInstallment.course_id' => $cid])->order(['StudentInstallment.sid_id'=>'DESC'])->first();

            $this->set('student', $student);
            //print_r($student); die;
            $this->set('new_receipt', $new_receipt);
        } else {
            $status = "old";

            $registration = $this->StudentInstallment->find('all')->contain(['Students','Course','Branch'])->where(['StudentInstallment.sid_id' => $id])->order(['StudentInstallment.sid_id'=>'DESC'])->first();

        
            $this->set('student', $registration);

        }
        //echo $status; die;
        $this->set('status', $status);

       
    }


    public function file_get_contents_curl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
	public function add()
	{
		$this->viewBuilder()->layout('admin');
		$newpack = $this->Staticpages->newEntity();
		if ($this->request->is(['post', 'put'])) { 			
			$this->request->data['slug'] = $this->request->data['slug'];
			$savepack = $this->Staticpages->patchEntity($newpack, $this->request->data);
			$results=$this->Staticpages->save($savepack);

			if ($results){
				$this->Flash->success(__('Staticpage has been saved.'));
				return $this->redirect(['action' => 'index']);	
			}else{
				$this->Flash->error(__('Staticpage not saved.'));
				return $this->redirect(['action' => 'index']);	
			}
		}
	}

	public function status($id,$status){
		if(isset($id) && !empty($id)){
			$product = $this->Staticpages->get($id);
			$product->status = $status;
			if ($this->Staticpages->save($product)) {
				if($status=='Y'){
					$this->Flash->success(__('Staticpage status has been Activeted.'));
				}else{
					$this->Flash->success(__('Staticpage status has been Deactiveted.'));
				}
				return $this->redirect(['action' => 'index']);  
			}
		}
	}



    public function deletesubmit($id = "")
    {
        $this->loadModel('Feesubmission');
        $id = base64_decode($id);
        $conns = ConnectionManager::get('default');
        $query1 ="DELETE FROM fee_submission WHERE id=" . $id;
        $conns->execute($query1);
        $this->Flash->flash("Record deleted successfully.", [
            "params" => [
            "type" => "success"
            ]
            ]);
        $this->redirect(array('action' => 'submit'));

    }
	public function delete($id)
	{
		$staticpage = $this->Staticpages->get($id);
		if($staticpage){
			$this->Staticpages->deleteAll(['Ourteams.id' => $id]); 
			$this->Staticpages->delete($staticpage);

			$this->Flash->success(__('Staticpage has been deleted successfully.'));
			return $this->redirect(['action' => 'index']);
		}else{
			$this->Flash->error(__('Staticpage not  delete'));
			return $this->redirect(['action' => 'index']);
		}
	}

	public function edit($id)
	{
		$this->viewBuilder()->layout('admin');
		$newpack = $this->Staticpages->get($id);
		$this->set('newpack',$newpack);
		if ($this->request->is(['post', 'put'])) {
			if($this->request->data['slug']){
				$this->request->data['slug'] = $this->request->data['slug'];
			}

			$savepack = $this->Staticpages->patchEntity($newpack, $this->request->data);
			$results=$this->Staticpages->save($savepack);
			if ($results){
				$this->Flash->success(__('Staticpage has been updated.'));
				return $this->redirect(['action' => 'index']);	
			}else{
				$this->Flash->error(__('Staticpage not Updated.'));
				return $this->redirect(['action' => 'index']);	
			}		    
		}
	}
	
	public function isAuthorized($user)
{
  if (isset($user['role_id']) && ($user['role_id'] == 1 || $user['role_id'] == 4 )) {
    return true;
  }
  return false;
}
	

}