<?php

namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Datasource\ConnectionManager;
use \Datetime;
include '../vendor/tecnickcom/tcpdf/tcpdf.php';
class ExamSetupController extends AppController
{
	//$this->loadcomponent('Session');
	public function initialize(){	
		$this->loadModel('ExamSetup');
		$this->loadModel('Users');
		$this->loadModel('Batch');
		$this->loadModel('QuestionCategory');
		parent::initialize();
	}
	public function index(){ 	
		$this->viewBuilder()->layout('admin'); 

		$ac = $this->QuestionCategory->find('list', [
			'keyField' => 'qc_id',
			'valueField' => 'qc_name',
		  ])->order(['QuestionCategory.qc_name' => 'ASC'])->toArray();
	 $this->set('categ',$ac);


        $brnchuser=$this->Users->find('all')->where(['Users.id'=>'1'])->first();
		$this->set('brnchuser',$brnchuser);
		
	if($this->request->session()->read('Auth.User.role_id')==1){


	$exambatch=$this->ExamSetup->find('all')->select(['b_id'])->where(['ExamSetup.status'=>1])->toarray();

       foreach ($exambatch as  $value) {
        $batchid[]=$value['b_id'];
        }
		$btch = $this->Batch->find('list', [
			'keyField' => 'b_id',
			'valueField' => 'batch_code',
		  ])->where(['Batch.b_id IN' =>$batchid])->order(['Batch.b_id' => 'Desc'])->toArray();

       
      }
      else
      {

		$btch = $this->Batch->find('list', [
			'keyField' => 'b_id',
			'valueField' => 'batch_code',
		  ])->where(['Batch.branch'=>$this->request->session()->read('Auth.User.branch')])->order(['Batch.b_id' => 'Desc'])->toArray();



     }
	 $this->set('btch',$btch);
     $user=$this->request->session()->read('Auth.User.id');

	 $seo = $this->ExamSetup->find('all')->contain(['Batch','QuestionCategory'])->order(['ExamSetup.e_id'=>'DESC']);
	 $this->set('destination', $this->paginate($seo)->toarray());
}

public function get_exams_details(){
	$this->loadModel('Question');
	
  $quid=$this->request->data['email']; 
  $etype=$this->request->data['extype']; 
  $articles = $this->Question; 
  $data = $this->Question->find('all')->select(['q_marks' =>$articles->find('all')->func()->count('q_marks')])->where(['Question.qc_id'=>$quid,'Question.q_exam'=>$etype])->group(['Question.q_marks'])->toarray();
 
if(empty($data)){
$data[0]['q_marks']=0;
$data[1]['q_marks']=0;
$data[2]['q_marks']=0;
$data[3]['q_marks']=0;
}
if(empty($data[1]['q_marks'])) $data[1]['q_marks']=0;
if(empty($data[2]['q_marks'])) $data[1]['q_marks']=0;
if(empty($data[3]['q_marks'])) $data[1]['q_marks']=0;
  echo json_encode($data); die;
}	

public function batchstudents(){
	$this->loadModel('StudentBatch');
 
  $students=$this->StudentBatch->find('all')->contain(['Students','Batch'])->where(['StudentBatch.batch_id IN'=>$this->request->data['code']])->toarray();
  // dd($students); die;
  $this->set(compact('students'));
  }
public function batchsearch(){
	
	$batches=$this->Batch->find('all')->where(['Batch.q_cat'=>$this->request->data['code']])->toarray();


  foreach($batches as $batch)
  {
   
	 $data[$batch['b_id']]=$batch['batch_code'];
  }
  echo json_encode($data); die;
  }

  public function edit($id=null){
	$this->viewBuilder()->layout('admin'); 
	$this->loadModel('ExamSetup');

$exambatch=$this->ExamSetup->find('all')->select(['b_id'])->where(['ExamSetup.status'=>1])->toarray();

foreach ($exambatch as  $value) {
  $batchid[]=$value['b_id'];
}
    //  $ac1=$this->Batch->find('list',array('fields'=>'b_id,batch_code','conditions'=>array('Batch.b_id NOT'=>$batchid,'Batch.is_new'=>'Y')));


	 $ac1 = $this->Batch->find('list', [
		'keyField' => 'b_id',
		'valueField' => 'batch_code',
	  ])->where(['Batch.is_new' =>'Y'])->order(['Batch.b_id' => 'Desc'])->toArray();



     $this->set('categ1',$ac1);

	 $ac = $this->QuestionCategory->find('list', [
		'keyField' => 'qc_id',
		'valueField' => 'qc_name',
	  ])->order(['QuestionCategory.qc_name' => 'ASC'])->toArray();


   
     $this->set('categ',$ac);
	 

	 

     $newresponse = $this->ExamSetup->get($id);
	 $this->set('newresponse',$newresponse);

     if ($this->request->is(['post', 'put'])) {
	
$this->request->data['total_marks']= ($this->request->data['1mark']*1)+($this->request->data['2mark']*2)+($this->request->data['3mark']*3)+($this->request->data['5mark']*5);
      $this->request->data['add_date']=date("Y-m-d");

	  if($this->request->data['e_date']){
      $this->request->data['e_date']=date("Y-m-d",strtotime($this->request->data['e_date']));
	  }else{
	  $this->request->data['e_date']=$newresponse['e_date'];
	  }
       
	  $savepack = $this->ExamSetup->patchEntity($newresponse, $this->request->data);
	  $results=$this->ExamSetup->save($savepack);

		

      $this->Flash->flash("The Exam Setup page is Updated successfully.", [
	     "params" => [
	     "type" => "success"
	      ]]); 


      $this->redirect(array('action' => 'index'));
  }

}


public function add($id=null){
	$this->viewBuilder()->layout('admin'); 

	$exambatch=$this->ExamSetup->find('all')->select(['b_id'])->where(['ExamSetup.status'=>1])->toarray();

   
foreach ($exambatch as  $value) {
  $batchid[]=$value['b_id'];
}
    //  $ac1=$this->Batch->find('list',array('fields'=>'b_id,batch_code','conditions'=>array('Batch.b_id NOT'=>$batchid,'Batch.is_new'=>'Y')));


	 $ac1 = $this->Batch->find('list', [
		'keyField' => 'b_id',
		'valueField' => 'batch_code',
	  ])->where(['Batch.is_new' =>'Y'])->order(['Batch.b_id' => 'Desc'])->toArray();



     $this->set('categ1',$ac1);

	 $ac = $this->QuestionCategory->find('list', [
		'keyField' => 'qc_id',
		'valueField' => 'qc_name',
	  ])->order(['QuestionCategory.qc_name' => 'ASC'])->toArray();


   
     $this->set('categ',$ac);
	 

	 

     $newresponse = $this->ExamSetup->newEntity();
	 $this->set('newresponse',$newresponse);

     if($this->request->is('post')){


      $dt=  $this->request->data['e_date'];
      $dt1= strftime('%Y-%m-%d',strtotime($dt));

      $this->request->data['e_date']=$dt1;
      $this->request->data['total_marks']= ($this->request->data['1mark']*1)+($this->request->data['2mark']*2)+($this->request->data['3mark']*3)+($this->request->data['5mark']*5);
      $this->request->data['add_date']=date("Y-m-d");
      $this->request->data['e_date']=date("Y-m-d",strtotime($this->request->data['e_date']));
      $allowed=$this->request->data['allowed_Stu'];
      // dd($allowed); die;
      foreach($allowed as $batch_id=>$batch_allow){
// echo $batch_id; die;
        $stu_ids=array();
        foreach($batch_allow as $valueid){
          $stu_ids[]=$valueid;
        }
        $this->request->data['b_id']=$batch_id;
        $this->request->data['students']=implode(',',$stu_ids);
        $this->request->data['5mark']=0;
        $this->request->data['status']=0;
        $this->request->data['mod_date']="0000-00-00";
     
        $newpack = $this->ExamSetup->newEntity();

		$savepack = $this->ExamSetup->patchEntity($newpack, $this->request->data);
			$results=$this->ExamSetup->save($savepack);

		
       if($results){
          $eid=  $results->id;
         $this->loadModel('StudentCourse');
         $this->loadModel('StudentBatch');
         $this->loadModel('Students');
         $this->loadModel('Users');
        
		 $bbc = $this->Students->find('all')->where(['Students.s_id IN' =>$batch_allow])->order(['s_id' => 'DESC'])->toarray();

       
         // dd($bbc); die;
         foreach($bbc as $bb=>$value)
         { 
 
        $this->request->data['username']=$value['s_enroll'];
        $this->request->data['password'] =(new DefaultPasswordHasher)->hash($value['s_enroll']);
         
          $ii=$value['s_id'];
 
          $this->request->data['s_id']=$value['s_id'];
          $this->request->data['b_id']=$batch_id;
          $this->request->data['e_id']=$eid;
          $this->request->data['name']=$value['s_name'];
          $this->request->data['role_id']=3;
          $this->request->data['created']=date("Y-m-d");
          $this->request->data['mobile']='';
          $this->request->data['branchhead']='';
          $this->request->data['enrollcode']='';
 

		  $sii = $this->Users->find('all')->where(['Users.s_id' =>$value['s_id']])->order(['id' => 'DESC'])->first();

         
 
          if(empty($sii))
          {

			$newpack = $this->Users->newEntity();
			$savepack = $this->Users->patchEntity($newpack, $this->request->data);
			$this->Users->save($savepack);

         }else{
 
			$conns = ConnectionManager::get('default');
			$query12 ="update users set b_id='".$batch_id."' where s_id='".$value['s_id']."'";
			$conns->execute($query12);
			
 
         }
 
 
 
 
       }
 
 
     }


      }
 

	    
$this->Flash->flash("The ExamSetup page is Added successfully.", [
	"params" => [
	  "type" => "success"
	]
	]); 
     
      $this->redirect(array('action' => 'index'));
  }

}



public function viewpdf($id='',$batch='') {
	$this->loadModel('StudentBatch');
	$this->loadModel('Result');
	$this->loadModel('ExamSetup');
	$this->loadModel('StudentCourse');
	

$bt = $this->ExamSetup->find('all')->where(['ExamSetup.e_id'=>$id])->order(['e_id' => 'DESC'])->first();
$btch=$bt['b_id'];

$blist1 = $this->StudentCourse->find('list', [
	'keyField' => 's_id',
	'valueField' => 's_id',
  ])->where(['StudentCourse.b_id'=>$btch])->toArray();


$blist2 = $this->StudentBatch->find('list', [
	'keyField' => 's_id',
	'valueField' => 's_id',
  ])->where(['StudentBatch.batch_id'=>$btch])->toArray();

$blist=$blist1+$blist2;
$this->set('eid',$id);
  
  $result=array();
  foreach($blist as $list){

$result[$list] = $this->Result->find('all')->where(['Result.s_id'=>$list,'Result.e_id'=>$id])->order(['e_id' => 'DESC'])->first();

  }

  
  $this->set('result',$result);


  $tot = $this->ExamSetup->find('all')->where(['ExamSetup.e_id'=>$id])->order(['e_id' => 'DESC'])->first();
 

   if($tot['e_type']==1)
   {
	 $etype="Major Exam";
   }
   else if($tot['e_type']==0)
   {
	 $etype="Minor Exam";
   }
   else
   {
	 $etype="Special Exam";
   }
   $this->set('examtype',$etype);
   $this->set('examname',$tot['e_name']);
   $this->set('batch',$batch);
  
   $onem=$tot['1mark'];
   $twom=$tot['2mark'];
   $threem=$tot['3mark'];
   $fourm=$tot['5mark'];
   $dt=strftime('%d-%b-%Y',strtotime($tot['e_date']));
   $allquestion=$onem+$twom+$threem+$fourm;
   $totalmarks=$tot['total_marks'];
   $this->set('allq',$allquestion);
   $this->set('allm',$totalmarks);
   $this->set('examdate',$dt);
   $this->set('posts', $blist);
  
  
		
  }


public function view_examdetail($id='') {
	
	$this->loadModel('StudentCourse');
	$this->loadModel('StudentBatch');
	$this->loadModel('ExamSetup');
	

	$bt=$this->ExamSetup->find('all')->where(['ExamSetup.e_id' => $id])->toarray();

	$exam_det = [];
	foreach ($bt as $value) {
		$btch = $value['b_id'];

		$array=$this->StudentBatch->find('all')->contain(['Students','Batch','Branch','QuestionCategory'])->where(['StudentBatch.batch_id' => $btch])->toarray();
		
		if (!empty($array)) {
			foreach ($array as $stu) {
				$data = [];
				$data['exam_date'] = $value['add_date'];
				$data['exam_name'] = $value['e_name'];
				$data['stu_name'] = $stu['student']['s_name'];
				$data['stu_enroll'] = $stu['student']['s_enroll'];
				$data['stu_mobile'] = $stu['student']['s_contact'];
				$data['exam_subject'] = $stu['question_category']['qc_name'];
				$data['batch_code'] = $stu['batch']['batch_code'];
				$data['branch'] = $stu['branch']['name'];
	
				$exam_det[] = $data;
			}

		}

}
	$this->set('exam_det', $exam_det);
	

	$this->set('posts', $data);



}

	public function delete($id)
	{
	    $this->loadModel('ExamSetup');
	    $catdelete = $this->ExamSetup->get($id);
	    if($catdelete){
		  $this->ExamSetup->deleteAll(['ExamSetup.e_id' => $id]); 
		  $this->ExamSetup->delete($catdelete);
		  $this->Flash->success(__('ExamSetup has been deleted successfully.'));
		  return $this->redirect(['action' => 'index']);
		}else{
		  $this->Flash->error(__('ExamSetup not  delete'));
		  return $this->redirect(['action' => 'index']);
		}
	}





	public function search(){ 
		$this->loadModel('ExamSetup'); 
		
	
		$req_data = $this->request->data();

	
		 $e_cat = $req_data['e_cat'];
		 $from_date = $req_data['from_date'];
		 $to_date = $req_data['to_date'];
		 $cond = [];   
		 if(isset($e_cat) && $e_cat!='')
		 {
		 $cond['ExamSetup.e_cat']=$e_cat;	
		 }
		 if(isset($from_date) && $from_date!='')
		 {
			$from_date=date('Y-m-d',strtotime($from_date));
		 $cond['DATE(ExamSetup.e_date) >=']=$from_date;	
		 }
		 
		 if(isset($to_date) && $to_date!='')
		 {
			$to_date=date('Y-m-d',strtotime($to_date));
		 $cond['DATE(ExamSetup.e_date) <=']=$to_date;	
		 }

	
	  //die;
	  $seo = $this->ExamSetup->find('all')->contain(['Batch','QuestionCategory'])->where([$cond])->order(['ExamSetup.e_id'=>'DESC']);
	  $this->set('destination', $this->paginate($seo)->toarray());
	  
		}
	  
	  

	

public function download($filename){




$this->response->file("images"."/". $filename ,
array('download'=> true, 'name'=> $filename));

}



	
	 public function isAuthorized($user)
	 {
		if (isset($user['role_id']) && ($user['role_id'] == 1)) {
			return true;
		}
		return false;
	 }
        
}
