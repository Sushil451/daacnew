<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class StaticpagesController extends AppController

{ 
	public function initialize(){	
		$this->loadModel('Staticpages');
		$this->loadModel('Seo');
		parent::initialize();
	}

	public function index(){ 
		$this->viewBuilder()->layout('admin');
		$staticpage = $this->Staticpages->find('all')->order(['Staticpages.id'=>DESC]);
		$this->set('staticpage', $this->paginate($staticpage)->toarray());
	}

	public function add()
	{
		$this->viewBuilder()->layout('admin');
		$newpack = $this->Staticpages->newEntity();
		if ($this->request->is(['post', 'put'])) { 			
			$this->request->data['slug'] = $this->request->data['slug'];
			$this->request->data['orgid'] = 1;
			$this->request->data['titleid'] = 1;
			$this->request->data['image'] = '';
			$this->request->data['page'] = '';
			$this->request->data['created'] = date('Y-m-d');
			$savepack = $this->Staticpages->patchEntity($newpack, $this->request->data);
			$results=$this->Staticpages->save($savepack);


			$user = $results->id;
     
			$seoentry=$this->Seo->find('all')->where(['Seo.course_id' => $user,'Seo.orgid' =>1])->first();
		 if($seoentry){
		 
		   $seopack = $this->Seo->get($seoentry['id']);
		   
		 
		 $seo['course_id'] = $user;
		
			 
		 }else{
			  $seopack = $this->Seo->newEntity();
			  $seo['orgid']=1;
			  $seo['course_id'] = $user;
			  $seo['page'] = $this->request->data['title'];
			  $seo['title'] = "";
			  $seo['location'] = "";
			  $seo['keyword'] = "";
			  $seo['locality'] ="";
			  $seo['description'] = "";
			  $seo['created']=date('Y-m-d');
		 
		 }
			  $savepackseo = $this->Seo->patchEntity($seopack, $seo);
			  $result= $this->Seo->save($savepackseo);



			if ($results){
				$this->Flash->success(__('Staticpage has been saved.'));
				return $this->redirect(['action' => 'index']);	
			}else{
				$this->Flash->error(__('Staticpage not saved.'));
				return $this->redirect(['action' => 'index']);	
			}
		}
	}

	public function status($id,$status){
		if(isset($id) && !empty($id)){
			$product = $this->Staticpages->get($id);
			$product->status = $status;
			if ($this->Staticpages->save($product)) {
				if($status=='Y'){
					$this->Flash->success(__('Staticpage status has been Activeted.'));
				}else{
					$this->Flash->success(__('Staticpage status has been Deactiveted.'));
				}
				return $this->redirect(['action' => 'index']);  
			}
		}
	}

	public function delete($id)
	{
		$staticpage = $this->Staticpages->get($id);
		if($staticpage){
			$this->Staticpages->deleteAll(['Ourteams.id' => $id]); 
			$this->Staticpages->delete($staticpage);

			$this->Flash->success(__('Staticpage has been deleted successfully.'));
			return $this->redirect(['action' => 'index']);
		}else{
			$this->Flash->error(__('Staticpage not  delete'));
			return $this->redirect(['action' => 'index']);
		}
	}

	public function edit($id)
	{
		$this->viewBuilder()->layout('admin');
		$newpack = $this->Staticpages->get($id);
		$this->set('newpack',$newpack);
		if ($this->request->is(['post', 'put'])) {
			if($this->request->data['slug']){
				$this->request->data['slug'] = $this->request->data['slug'];
			}
			$this->request->data['orgid'] = 1;
			$this->request->data['titleid'] = 1;
			$this->request->data['image'] = '';
			$this->request->data['page'] = '';
			$savepack = $this->Staticpages->patchEntity($newpack, $this->request->data);
			$results=$this->Staticpages->save($savepack);


			$user = $results->id;
     
			$seoentry=$this->Seo->find('all')->where(['Seo.course_id' => $user,'Seo.orgid' =>1])->first();
		 if($seoentry){
		 
		   $seopack = $this->Seo->get($seoentry['id']);
		   
		 
		 $seo['course_id'] = $user;
		
			 
		 }else{
			  $seopack = $this->Seo->newEntity();
			  $seo['orgid']=1;
			  $seo['course_id'] = $user;
			  $seo['page'] = $this->request->data['title'];
			  $seo['title'] = "";
			  $seo['location'] = "";
			  $seo['keyword'] = "";
			  $seo['description'] = "";
			  $seo['locality'] ="";
			  $seo['created']=date('Y-m-d');
		 
		 }
			  $savepackseo = $this->Seo->patchEntity($seopack, $seo);
			  $result= $this->Seo->save($savepackseo);

			if ($results){
				$this->Flash->success(__('Staticpage has been updated.'));
				return $this->redirect(['action' => 'index']);	
			}else{
				$this->Flash->error(__('Staticpage not Updated.'));
				return $this->redirect(['action' => 'index']);	
			}		    
		}
	}
	
	public function isAuthorized($user)
{
  if (isset($user['role_id']) && ($user['role_id'] == 1)) {
    return true;
  }
  return false;
}
	

}