<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class PlacementController extends AppController

{ 
	public function index(){
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Placement');
	$this->loadModel('PlacementRequire');
		
	
 $seo = $this->Placement->find('all')->order(['Placement.id'=>'DESC']);
  $this->set('placement', $this->paginate($seo)->toarray());



}
	public function requirement(){
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Placement');
	$this->loadModel('PlacementRequire');
		
	
 $seo = $this->PlacementRequire->find('all')->contain(['Placement'])->order(['PlacementRequire.id'=>'DESC']);
  $this->set('placement', $this->paginate($seo)->toarray());



}





public function findcourseid(){

	$this->loadModel('Seo');
	$this->loadModel('Course');
	$orgid=$this->request->data['orgid'];
	if($this->request->data['orgid']==3){
		$orgid=0;
	}
	
	$this->set('orgid', $orgid);
	$maincoursesGroup = $this->Seo->find('list', [
		'keyField' => 'id',
		'valueField' => 'course_id',
	  ])->where(['Seo.orgid' =>$orgid])->order(['Seo.course_id' => 'ASC'])->toArray();
	$cio=[];
	  foreach($maincoursesGroup as $t=>$u){
	
		$cio[$t]=$u;
	  }
	  $coursesGroup = $this->Course->find('all')->where(['Course.id IN' =>$cio])->group(['Course.cname'])->order(['Course.cname' => 'ASC'])->toArray();
	  $this->set('coursesGroup', $coursesGroup);

}
	public function search(){ 
	
$this->loadModel('Placement');

//echo "test"; die;

$type = $this->request->data['type'];
$name = $this->request->data['company'];
$from_date = $this->request->data['from_date'];
$to_date = $this->request->data['to_date'];

$apk = array();

if (!empty($type)) {

	$apk['Placement.technology LIKE']='%'.$type.'%';
}

if (!empty($name)) {

	$apk['Placement.company LIKE']=$name.'%';
}

if(isset($from_date) && $from_date!='')
{
   $from_date=date('Y-m-d',strtotime($from_date));
$apk['DATE(Placement.created) >=']=$from_date;	
}

if(isset($to_date) && $to_date!='')
{
   $to_date=date('Y-m-d',strtotime($to_date));
$apk['DATE(Placement.created) <=']=$to_date;	
}


 $seo = $this->Placement->find('all')->where([$apk])->order(['Placement.id'=>'DESC']);
 $this->set('placement', $this->paginate($seo)->toarray());
}
public function reqsearch(){ 
	
$this->loadModel('PlacementRequire');

//echo "test"; die;


$name = $this->request->data['company'];
$from_date = $this->request->data['from_date'];
$to_date = $this->request->data['to_date'];

$apk = array();



if (!empty($name)) {

	$apk['Placement.company LIKE']=$name."%";
}

if(isset($from_date) && $from_date!='')
{
   $from_date=date('Y-m-d',strtotime($from_date));
$apk['DATE(PlacementRequire.created) >=']=$from_date;	
}

if(isset($to_date) && $to_date!='')
{
   $to_date=date('Y-m-d',strtotime($to_date));
$apk['DATE(PlacementRequire.created) <=']=$to_date;	
}


 $seo = $this->PlacementRequire->find('all')->contain(['Placement'])->where([$apk])->order(['Placement.id'=>'DESC']);
 $this->set('placement', $this->paginate($seo)->toarray());
}
	

	public function add($id=null)
	{
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Placement');
		if($id){
        	$newpack = $this->Placement->get($id);
		}else{
			$newpack = $this->Placement->newEntity();
		}
		
		$this->set('newpack',$newpack);
		if ($this->request->is(['post', 'put'])) { 	
			
			$filename=$this->data['logo']['name'];
$ext=  end(explode('.', $filename));
		   $name = md5(time($filename)); 
$exclname="file".$name.'.'.$ext;
if(move_uploaded_file($this->data['logo']['tmp_name'],"images/company/". $exclname)){

$this->request->data['logo']=$exclname; }
else
{
$courseall_id= $this->Placement->find('all')->where(['Placement.id' =>$this->request->data['id']])->first();
if($courseall_id){
	$this->request->data['logo']=$courseall_id['logo'];
}
$exclname=" ";
$this->request->data['logo']=$exclname;
}



$this->request->data['review']="";
$this->request->data['technology']=implode(',',$this->request->data['technology']);
$this->request->data['description']="";
$this->request->data['created']=date('Y-m-d');


			$savepack = $this->Placement->patchEntity($newpack, $this->request->data);
			$results=$this->Placement->save($savepack);

     if ($results){
				$this->Flash->success(__('Company has been saved.'));
				return $this->redirect(['action' => 'index']);	
			}else{
				$this->Flash->error(__('Company not saved please fill your all fields'));
			return $this->redirect(['action' => 'add']);
			}
		}
	}
	public function addreq($id=null)
	{
		$this->viewBuilder()->layout('admin');
		$this->loadModel('PlacementRequire');
		$this->loadModel('Placement');


		
		$companyList = $this->Placement->find('list', [
            'keyField' => 'id',
			'valueField' => 'company',
		  ])->where(['status' => 'Y'])->order(['Placement.company' => 'ASC'])->toArray();
		  $this->set(compact('companyList'));
		if($id){
        	$newpack = $this->PlacementRequire->get($id);
		}else{
			$newpack = $this->PlacementRequire->newEntity();
		}
		
		$this->set('newpack',$newpack);
		if ($this->request->is(['post', 'put'])) { 	
			
		
			$date=$this->request->data['created']; //pr($date); die;
  
			$date=date('Y-m-d', strtotime($date)); //die;
		   $this->request->data['created']=$date ;
		   
		   $loc=$this->request->data['location'];
		   $this->request->data['timestamp']=time(); 
		   if($loc==''){
			   
			   $this->request->data['location']='Jaipur';
			 
			   
		   }
		   $this->request->data['comment']='';


			$savepack = $this->PlacementRequire->patchEntity($newpack, $this->request->data);
			$results=$this->PlacementRequire->save($savepack);

     if ($results){
				$this->Flash->success(__('Company Requirement has been updated.'));
				return $this->redirect(['action' => 'requirement']);	
			}else{
				$this->Flash->error(__('Company Requirement not updated please fill your all fields'));
			return $this->redirect(['action' => 'addrfq']);
			}
		}
	}
	public function settings($id=null)
	{
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Seosettings');
if($id){
	$newpack = $this->Seosettings->get($id);

}else{
	$newpack = $this->Seosettings->newEntity();

}
		
		$this->set('newpack',$newpack);
		if ($this->request->is(['post', 'put'])) { 			
			$savepack = $this->Seosettings->patchEntity($newpack, $this->request->data);
			$results=$this->Seosettings->save($savepack);

     if ($results){
				$this->Flash->success(__('Seo Settings has been updated.'));
				return $this->redirect(['action' => 'index']);	
			}else{
				$this->Flash->error(__('Seo Settings  not saved please fill your all fields'));
			return $this->redirect(['action' => 'settings']);
			}
		}
	}

	public function status($id,$status){

		$this->loadModel('Seo');
		if(isset($id) && !empty($id)){
			$product = $this->Seo->get($id);
			$product->status = $status;
			if ($this->Seo->save($product)) {
				$this->Flash->success(__('Seo status has been updated.'));
				return $this->redirect(['action' => 'index']);  
			}
		}
	}
	public function deletereq($id='') {
		$this->autoRender=false;
   $this->loadModel('PlacementRequire');
   
   $seotdel = $this->PlacementRequire->get($id);
   if($seotdel){
	   $this->PlacementRequire->deleteAll(['PlacementRequire.id' => $id]); 
	   $this->PlacementRequire->delete($seotdel);

	   $this->Flash->success(__('Placement requirement deleted successfully.'));
		  

		   $this->redirect(array('action' => 'requirement'));

	   }


   }

	public function delete($id = null) {
		$this->loadModel('Placement');
		$this->loadModel('PlacementRequire');
			
	
	$pid_count= $this->PlacementRequire->find('all')->where(['PlacementRequire.p_id' =>$id])->count();
			
	if($pid_count==0){
		$seotdel = $this->Placement->get($id);
		if($seotdel){
			$this->Placement->deleteAll(['Placement.id' => $id]); 
			$this->Placement->delete($seotdel);
	
			
				$this->Flash->success(__('Company information is deleted successfully.'));
	
				$this->redirect(array('action' => 'index'));
	
			}
	}else{
    $this->Flash->success(__('Company information cant be deleted. Company already posted vacancies.'));
		
		$this->redirect(array('action' => 'index'));
		}
	
		}

	

	public function edit($id)
	{
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Seo');
		$this->loadModel('Course');

		$newpack = $this->Seo->get($id);
		$this->set('newpack',$newpack);
		if ($this->request->is(['post', 'put'])) {	

			$savepack = $this->Seo->patchEntity($newpack, $this->request->data);
			$results=$this->Seo->save($savepack);

			if($results){
			$orgid=$results->orgid;

			if ($orgid==0 || $orgid==2){
				$course_id=$results->course_id;
				$location=$results->location;
				$seoentry=$this->Course->find('all')->where(['Course.id' =>$course_id])->first();
				if($seoentry){
			    $coursepack = $this->Course->get($seoentry['id']);
				$coursee['url'] = $location;
				$savepackcourse = $this->Course->patchEntity($coursepack, $coursee);
				$result= $this->Course->save($savepackcourse);
					
				}
					 
			}
				$this->Flash->success(__('Seo details has been updated.'));
				return $this->redirect(['action' => 'index']);	
			}else{
				$this->Flash->error(__('Seo not updated'));
			return $this->redirect(['action' => 'index']);
			}		    
		}
	}
	
	public function viewdocument($id)
	{
	$this->loadModel('Seo');
		$popupdata = $this->Seo->find('all')->where(['Seo.id'=>$id])->order(['Seo.id'=>DESC]);
		$this->set('popupdata', $this->paginate($popupdata)->toarray());
	}

		public function viewkeywords($id)
	{
	$this->loadModel('Seo');
		$keyworddata = $this->Seo->find('all')->where(['Seo.id'=>$id])->order(['Seo.id'=>DESC]);
		$this->set('keyworddata', $this->paginate($keyworddata)->toarray());
	
	}

	
	public function isAuthorized($user)
	{
		if (isset($user['role_id']) && ($user['role_id'] == 1)) {
			return true;
		}
		return false;
	}
}