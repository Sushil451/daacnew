<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\View\Helper;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;


class PermissionmodulesController extends AppController
{
  //initialize component
  public function initialize(){
    parent::initialize();
    $this->loadModel('PermissionModules');
    $this->loadModel('Roles');
  }
  
  public function index($id=null)
  {
    $this->viewBuilder()->layout('admin');
    $employees= $this->Roles->find('all')->find('list', ['keyField'=>'id','valueField'=>'name'])->where(['Roles.id not in'=>['1']])->order(['Roles.name'=>'ASC'])->toarray();
    $this->set(compact('employees')); 

    if ($this->request->is(['post', 'put'])){   

      $modules=sizeof($this->request->data['module']);
      $user_id=$this->request->data['user_id'];
      $userTable = TableRegistry::get('PermissionModules');
      $exists = $userTable->exists(['user_id' =>$user_id]);
      if($exists){
        $conns = ConnectionManager::get('default');
        $quersy="DELETE FROM `permission_module` WHERE `user_id`='$user_id'";
        $conns->execute($quersy);
      }
      for($i=1;$i<=$modules;$i++){
        $menu=array();
        $mod=array();
        $mod=$this->request->data['module'.$i];
        $menu=$this->request->data['menu'.$i];
        foreach($menu as $kty=>$ddd){
          $ter=array();
          $ter=explode('^',$ddd);
          $conn = ConnectionManager::get('default');
          
          $query="INSERT INTO `permission_module`(`user_id`, `module`, `menu`, `controller`, `action`, `status`) VALUES ('$user_id','$mod[0]','$ter[0]','$ter[1]','$ter[2]','Y')";
          $results = $conn->execute($query);
        } 
      }
      $this->Flash->success(__('Rights Update to User sucessfully.'));
      return $this->redirect(['action' => 'index']);     
    }
  }


  public function calculatepermission(){
    if ($this->request->is(['post', 'put'])){ 
      $empid=$this->request->data['empid'];

      $userTable = TableRegistry::get('PermissionModules');
      $exists = $userTable->exists(['user_id' =>$empid]);
      if($exists){
        $employees= $this->PermissionModules->find('all')->find('all')->where(['PermissionModules.user_id'=>$empid])->order(['PermissionModules.id'=>'ASC'])->toarray();
        $module=array();
        $menu=array();
        foreach($employees as $k=>$ty){
          
          if(!in_array($ty['module'],$module)){
            $module[]=$ty['module'];
          }
          if(!in_array($ty['menu'],$menu)){
            $menu[]=$ty['menu'];
          } 
        }
        $this->set(compact('module','menu'));
      }else{
        $module=array();
        $menu=array();
        $this->set(compact('module','menu'));
      }
    }

  }

  public function isAuthorized($user)
  {
    if (isset($user['role_id']) && ($user['role_id'] == 1)) {
      return true;
    }
    return false;
  }
  


}
