<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

class SliderController extends AppController
{
  // for view or index page
  public function index(){ 
   $this->viewBuilder()->layout('admin');
   $this->loadModel('Sliders');  
   $sliders=$this->Sliders->find('all')->order(['Sliders.id' => 'Desc']);
   $this->set('sliders',$this->paginate($sliders)->toarray());
 }

 
public function add()
{
  $this->viewBuilder()->layout('admin');
  $this->loadModel('Sliders'); 
  $newpack = $this->Sliders->newEntity();
  if ($this->request->is(['post', 'put'])) { 
 //  pr($this->request->data); die; 
      if ($this->request->data['image']['name'] != '')
      {
        $k = $this->request->data['image'];
        $galls = $this->move_images($k,'images/sliders/');
         $this->FcCreateThumbnail("compress", "images/sliders", $galls[0], $galls[0], "1920", "767");
        $this->request->data['image'] = $galls[0];
        unlink('compress/' . $galls[0]);
      }  
      
      if ($this->request->data['mobile_slider']['name'] != '')
      {
        $k = $this->request->data['mobile_slider'];
        $galls = $this->move_images($k,'images/sliders/');
         $this->FcCreateThumbnail("compress", "images/sliders", $galls[0], $galls[0], "100", "100");
        $this->request->data['mobile_slider'] = $galls[0];
        unlink('compress/' . $galls[0]);
      }  

    $savepack = $this->Sliders->patchEntity($newpack, $this->request->data);
    $results=$this->Sliders->save($savepack);
    if ($results){
      $this->Flash->success(__('Sliders has been saved.'));
      return $this->redirect(['action' => 'index']);  
    }else{
      $this->Flash->error(__('Sliders not saved.'));
      return $this->redirect(['action' => 'index']);  
    }
  }
}


public function status($id,$status){

  $this->loadModel('Sliders');
  if(isset($id) && !empty($id)){
    $product = $this->Sliders->get($id);
    $product->status = $status;
    if ($this->Sliders->save($product)) {
      if($status=='1'){
        $this->Flash->success(__('Sliders status has been Activeted.'));
      }else{
        $this->Flash->success(__('Sliders status has been Deactiveted.'));
      }
      return $this->redirect(['action' => 'index']);  
    }
  }
}

public function delete($id)
{
  $this->loadModel('Sliders');
  $catdelete = $this->Sliders->get($id);
    if($catdelete){
      unlink('images/sliders/' . $catdelete['image']);
      $this->Sliders->deleteAll(['Sliders.id' => $id]); 
      $this->Sliders->delete($catdelete);

      $this->Flash->success(__('Sliders has been deleted successfully.'));
      return $this->redirect(['action' => 'index']);
    }
}

public function edit($id)
{
  $this->viewBuilder()->layout('admin');
  $this->loadModel('Sliders');

  $sliders = $this->Sliders->get($id);
  $this->set('slider',$sliders);
  if ($this->request->is(['post', 'put'])) {
  //  pr($this->request->data); die; 
      if ($this->request->data['image']['name'] != '')
      {
        $k = $this->request->data['image'];
        $galls = $this->move_images($k,'images/sliders/');
        $this->FcCreateThumbnail("compress", "images/sliders", $galls[0], $galls[0], "1920", "767");
        $this->request->data['image'] = $galls[0];
        unlink('compress/' . $galls[0]);
      }else{
        $this->request->data['image'] = $sliders['image'];
      }    

      if ($this->request->data['mobile_slider']['name'] != '')
      {
        $k = $this->request->data['mobile_slider'];
        $galls = $this->move_images($k,'images/sliders/');
        $this->FcCreateThumbnail("compress", "images/sliders", $galls[0], $galls[0], "78", "78");
        $this->request->data['mobile_slider'] = $galls[0];
        unlink('compress/' . $galls[0]);
      }else{
        $this->request->data['mobile_slider'] = $sliders['mobile_slider'];
      }    
    
    $savepack = $this->Sliders->patchEntity($sliders, $this->request->data);
    $results=$this->Sliders->save($savepack);
    if ($results){
      $this->Flash->success(__('Sliders has been updated.'));
      return $this->redirect(['action' => 'index']);  
    }else{
      $this->Flash->error(__('Sliders not Updated.'));
      return $this->redirect(['action' => 'index']);  
    }           
  }
}

public function isAuthorized($user)
{
  if (isset($user['role_id']) && ($user['role_id'] == 1)) {
    return true;
  }
  return false;
}

}
