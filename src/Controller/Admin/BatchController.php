<?php

namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use \Datetime;

class BatchController extends AppController
{
	//$this->loadcomponent('Session');
	public function initialize(){	
		$this->loadModel('Batch');
		$this->loadModel('StudentCourse');
		$this->loadModel('Users');
		$this->loadModel('Branch');
		parent::initialize();
	}



	
	public function deletestu($id = "", $sub_id = "") {
		$this->loadModel('StudentBatch');
		$conns = ConnectionManager::get('default');
$query12 ="DELETE FROM cms_studentbatch where id=" . $id . " AND sub_id= " . $sub_id;
$conns->execute($query12);

$this->Flash->flash("Batch has been updated Successfully.", [
	"params" => [
	  "type" => "success"
	]
	]);
		
		$this->redirect(array('action' => 'index'));

	}
	public function getbatchstudent($id){
		$this->loadModel('StudentBatch');
		if ($this->request->session()->read('Auth.User.role_id') == 1) {
			$stu_ids = $this->StudentBatch->find('all')->contain(['Students','Branch','QuestionCategory'])->where(['StudentBatch.batch_id' =>$id])->toarray();
		} else {
			$branch = $this->request->session()->read('Auth.User.branch');
			$stu_ids = $this->StudentBatch->find('all')->contain(['Students','Branch'])->where(['StudentBatch.batch_id' =>$id, 'StudentBatch.branch_id' => $branch])->toarray();
	}


		$this->set(compact('stu_ids'));
		$this->set('batch_id', $id);

	}



	public function markcomplete($id){

		$this->set(compact('id'));

		if ($this->request->is('post')) {


			$date = date('Y-m-d', strtotime($this->request->data['date']));
	$status=$this->request->data['status'];
	$id=$this->request->data['id'];
	
	$comment=$this->request->data['comment'];
			$conns = ConnectionManager::get('default');
			$query12 ="update tbl_batch set closing_comment='" . $comment . "',closing_date='" . $date . "',status='" . $status . "' where b_id=" . $id;
			$conns->execute($query12);
		
	
	
			$this->Flash->flash("Batch has been Mark Completed Successfully.", [
				"params" => [
				  "type" => "success"
				]
				]);
		
		
			$this->redirect(array('action' => 'index'));
		}
	}
	public function aftergetbatchstudent(){

		$id=$this->request->data['batchid'];
		$this->loadModel('StudentBatch');
		if ($this->request->session()->read('Auth.User.role_id') == 1) {
			$stu_ids = $this->StudentBatch->find('all')->contain(['Students','Branch','QuestionCategory'])->where(['StudentBatch.batch_id' =>$id])->toarray();
		} else {
			$branch = $this->request->session()->read('Auth.User.branch');
			$stu_ids = $this->StudentBatch->find('all')->contain(['Students','Branch'])->where(['StudentBatch.batch_id' =>$id, 'StudentBatch.branch_id' => $branch])->toarray();
	}


		$this->set(compact('stu_ids'));
		$this->set('batch_id', $this->request->data['batchid']);

	}
	public function addstudentbatch($id,$q_cat){
		$this->loadModel('StudentBatch');
		if ($this->request->session()->read('Auth.User.role_id') == 1) {
			$stu_ids = $this->StudentBatch->find('all')->contain(['Students','Branch','QuestionCategory'])->where(['StudentBatch.batch_id' =>$id])->toarray();
		} else {
			$branch = $this->request->session()->read('Auth.User.branch');
			$stu_ids = $this->StudentBatch->find('all')->contain(['Students','Branch'])->where(['StudentBatch.batch_id' =>$id, 'StudentBatch.branch_id' => $branch])->toarray();
	}


		$this->set(compact('stu_ids','id','q_cat'));
		$this->set('batch_id', $this->request->data['batchid']);

	}

	public function stuselect() {
		$this->loadModel('Student');
		$this->loadModel('StudentCourse');
		$enroll=$this->request->data['enroll']; 
		$type=$this->request->data['type']; 
		$batch_id=$this->request->data['batch_id']; 
		$sub_id=$this->request->data['sub_id']; 

		if ($type == 'name') {
			if ($this->request->session()->read('Auth.User.role_id') == 1) {
				$students = $this->StudentCourse->find('all')->contain(['Students','Branch'])->where(['StudentCourse.drop_out' => 0, 'StudentCourse.status' => 1, 'Students.s_name LIKE' => $name . '%'])->group(['StudentCourse.s_id'])->order(['StudentCourse.id' => 'DESC'])->toarray();

			} else {
				$branch = $this->request->session()->read("Auth.User.branch");

				$students = $this->StudentCourse->find('all')->contain(['Students','Branch'])->where(['StudentCourse.drop_out' => 0, 'StudentCourse.status' => 1, 'Students.s_name LIKE' => $name . '%','StudentCourse.branch' => $branch])->group(['StudentCourse.s_id'])->order(['StudentCourse.id' => 'DESC'])->toarray();
				
			}
			$this->set(compact('students','batch_id','sub_id'));

		}
		if ($type == 'enroll') {

	if ($this->request->session()->read('Auth.User.role_id') == 1) {
				$students = $this->StudentCourse->find('all')->contain(['Students','Branch'])->where(['StudentCourse.drop_out' => 0, 'StudentCourse.status' => 1, 'Students.s_enroll LIKE' => $enroll . '%'])->group(['StudentCourse.s_id'])->order(['StudentCourse.id' => 'DESC'])->toarray();

			} else {
				$branch = $this->request->session()->read("Auth.User.branch");

				$students = $this->StudentCourse->find('all')->contain(['Students','Branch'])->where(['StudentCourse.drop_out' => 0, 'StudentCourse.status' => 1, 'Students.s_enroll LIKE' => $enroll . '%','StudentCourse.branch' => $branch])->group(['StudentCourse.s_id'])->order(['StudentCourse.id' => 'DESC'])->toarray();
				
			}

			$this->set(compact('students','batch_id','sub_id'));
		}

	}

	public function dropstu($id,$subid,$sid){
		$this->loadModel('StudentBatch');
	
			$stu_ids = $this->StudentBatch->find('all')->contain(['Students','Branch','QuestionCategory'])->where(['StudentBatch.batch_id' =>$id,'StudentBatch.sub_id' =>$subid])->first();
			$this->set(compact('stu_ids','id','subid'));
			$this->loadModel('Students');
    $stud = $this->Students->find('all')->contain(['Branch'])->where(['Students.s_id' =>$sid])->order(['s_id' => 'DESC'])->first();
    $this->set('stu_det', $stud);


	if ($this->request->is('post')) {


		$date = date('Y-m-d', strtotime($this->request->data['date']));
$status=$this->request->data['status'];
$id=$this->request->data['id'];
$sub_id=$this->request->data['sub_id'];
$comment=$this->request->data['comment'];
		$conns = ConnectionManager::get('default');
		$query12 ="update cms_studentbatch set status='" . $status . "',drop_date='" . $date . "',drop_comment='" . $comment . "' where id=" . $id . " AND sub_id=" . $sub_id;
		$conns->execute($query12);


		$this->Flash->flash("Batch has been updated Successfully.", [
			"params" => [
			  "type" => "success"
			]
			]);
	
	
		$this->redirect(array('action' => 'index'));
	}


		
	

	}

	public function delete($id = null) {


		$conns = ConnectionManager::get('default');
		$query12 ="update tbl_batch set is_delete='Y' where b_id=" . $id;
		$conns->execute($query12);


		$conns = ConnectionManager::get('default');
		$query12 ="update cms_studentbatch set status='N' where batch_id=" . $id;
		$conns->execute($query12);
		
	
	
		$this->Flash->flash("Batch has been deleted Successfully.", [
			"params" => [
			  "type" => "success"
			]
			]);
		$this->redirect(array('action' => 'index'));

	}
	public function index(){	
	$this->viewBuilder()->layout('admin'); 
	
	
	$batch=$this->Batch->find('all')->contain(['Branch','QuestionCategory','StudentCourse'])->where(['Batch.is_new' => 'Y','Batch.is_delete'=>'N'])->order(['Batch.b_id' => 'DESC']);
    $destination=$this->paginate($batch)->toarray();

	$this->set('batch', $destination);

	$faculty = $this->Users->find('all')->where(['Users.role_id' =>13])->first();
	$this->set('faculty', $faculty);


    $br =$this->Branch->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();
	$this->set('branch', $br);


	}
	public function search(){	
	
	
	$this->loadmodel('Batch');
	$branch=$this->request->data['branch_id'];
	$type=$this->request->data['status'];
	$cond=[];
	if (!empty($branch)) {

		$cond['Batch.branch_id']=$branch;
		
	}
	if (!empty($type)) {
		if($type!='delete' && $type!="fac_assgn"){

			$cond['Batch.status']=$type;
			$cond['Batch.is_delete']='N';

	
		}else{

			
			$cond['Batch.is_delete']='Y';
		
		}
	
	}else{

		$cond['Batch.is_delete']='N';
		
	}

	$cond['Batch.is_new']='Y';
	
	$batch=$this->Batch->find('all')->contain(['Branch','QuestionCategory','StudentCourse'])->where([$cond])->order(['Batch.b_id' => 'DESC']);
    $destination=$this->paginate($batch)->toarray();

	$this->set('batch', $destination);



	}


	public function checkmail() {
		$this->loadmodel('Batch');


		$usrinfo = $this->Batch->find('all')->where(['batch_code' => $this->request->data['email']])->count();

		if ($usrinfo >= 1) {
			$response['chk'] = true;
		} else {
			$response['chk'] = false;
		}
		echo json_encode($response);
		exit;
	}
	public function add($id=null){ 	
		$this->viewBuilder()->layout('admin');
	if ($this->request->session()->read('Auth.User.role_id') != 1) {
		$this->redirect(array('action' => 'index'));
		}
		$this->loadModel('Course');
		$this->loadModel('Branch');
		$ac1 = $this->Course->find('list', [
			'keyField' => 'id',
			'valueField' => 'cname',
		  ])->where(['status' => 0])->order(['cname' => 'ASC'])->toArray();
		  
		$this->set('categ1', $ac1);

		$br1 = $this->Branch->find('list', [
			'keyField' => 'id',
			'valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();
		
			  $this->set('brn', $br1);


		$this->loadModel('QuestionCategory');

		$sub1 = $this->QuestionCategory->find('list', [
			'keyField' => 'qc_id',
			'valueField' => 'qc_name',
		  ])->where(['status'=>'Y','is_delete'=>'N'])->order(['qc_name' => 'ASC'])->toArray();
		
		$this->set('sub1', $sub1);
		$this->loadModel('Users');

		$faculty = $this->Users->find('list', [
			'keyField' => 'id',
			'valueField' => 'name',
		  ])->where(['role_id' => 13])->order(['name' => 'ASC'])->toArray();
		
		$this->set('faculty', $faculty);

		if($id){
       
			$newresponse = $this->Batch->get($id);
		  }else{
			$newresponse = $this->Batch->newEntity();
		  
		  }
		  $this->set('newresponse', $newresponse);
		if ($this->request->is('post')) {

			$this->request->data['start_date'] = date('Y-m-d', strtotime($this->request->data['start_date']));
			$this->request->data['end_date'] = date('Y-m-d', strtotime($this->request->data['end_date']));
			$this->request->data['is_new'] = 'Y';
			//print_r($this->request->data); die;
			
			$this->request->data['add_date']=date('Y-m-d');
			$this->request->data['mod_date']=date('Y-m-d');
			$this->request->data['course_id']=0;
			$this->request->data['endtime']='';
			$this->request->data['sub_id']='';

			
			$savepack = $this->Batch->patchEntity($newresponse, $this->request->data);
   
			if($this->Batch->save($savepack)) {
		
				$this->Flash->flash("Batch has been updated Successfully.", [
					"params" => [
					  "type" => "success"
					]
					]);
					$this->redirect(array('action' => 'index'));

			} else {

				$this->Flash->flash("Error Find For add Batch.", [
					"params" => [
					  "type" => "error"
					]
					]);
					$this->redirect(array('action' => 'index'));

			}

		}


	}


	public function studentadd() {
		
		$this->loadModel('StudentBatch');
		$this->loadModel('Students');

	

		$data['batch_id'] = $this->request->data['batch_id'];
		$data['sub_id'] = $this->request->data['sub_id'];

		$data['s_id'] = $this->request->data['s_id'];
		$data['branch_id'] = $this->request->data['branch_id'];
		$data['created'] = date("Y-m-d h:i:s");
		$data['s_performance'] = "";
		$data['drop_date'] = "";
		$data['drop_comment'] = "";
		$data['total_working'] = "";
		$data['present_days'] = "";


		$stu_ext = $this->StudentBatch->find('all')->where(['StudentBatch.s_id' => $data['s_id'], 'StudentBatch.batch_id' => $data['batch_id']])->toarray();

		if (!empty($stu_ext)) {
			echo "hellos"; die;
		}


		$newresponse = $this->StudentBatch->newEntity();
		
		$savepack = $this->StudentBatch->patchEntity($newresponse, $data);
   
		$this->StudentBatch->save($savepack);
	
		echo "hello"; die;
	}



	  
	  


	
	
	  public function edit($id)
 	 { 
	  $this->viewBuilder()->layout('admin');
	  $this->loadModel('Document');
	  $this->loadModel('CategoryDocument');
	  
	
	  $categorydocument = $this->CategoryDocument->find('list', [
		  'keyField' => 'id',
		  'valueField' => 'name',
		])->order(['name' => 'ASC'])->toArray();
		
	
		$this->set('categorydocument',$categorydocument);
	  $product = $this->Document->get($id);
	  $this->set('newresponse',$product);
	  
	  if ($this->request->is(['post', 'put'])) {
		//pr($this->request->data); die;
		  if ($this->request->data['file']['name'] != '')
		  {
			$k = $this->request->data['file'];
			$galls = $this->single_file($k,'images/');
			// $this->FcCreateThumbnail1("compress", "images/subcategories", $galls[0], $galls[0], "78", "78");
			$this->request->data['file'] = $galls[0];
		//	unlink('compress/' . $galls[0]);
		  }else{
			$this->request->data['file'] = $product['file'];
		  }    
		
		$savepack = $this->Document->patchEntity($product, $this->request->data);
		$results=$this->Document->save($savepack);
		if ($results){
		  $this->Flash->success(__('Document has been updated.'));
		  return $this->redirect(['action' => 'index']);  
		}else{
		  $this->Flash->error(__('Document not Updated.'));
		  return $this->redirect(['action' => 'index']);  
		}           
	  }
	}
	
	

public function download($filename){




$this->response->file("images"."/". $filename ,
array('download'=> true, 'name'=> $filename));

}



	
	 public function isAuthorized($user)
	 {
		if (isset($user['role_id']) && ($user['role_id'] == 1)) {
			return true;
		}
		return false;
	 }
        
}
