<?php

namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use \Datetime;
class CourseController extends AppController

{







    public function initialize(){	
		$this->loadModel('Course');
        $this->loadModel('Installment');
        $this->loadModel('InstDetail');
		
		

		parent::initialize();
	}

public function  index(){

    $this->viewBuilder()->layout('admin'); 
    
    $this->loadModel('Course');

    $courses=$this->Course->find('all')->where(['Course.locality IS'=>NULL])->order(['Course.sort_order' => 'ASC']);



    $this->set('rec',$this->paginate($courses)->toarray());

        



}

public function search(){ 
	
  $this->loadModel('Course');
  //echo "test"; die;
  $name = $this->request->data['name'];
  $apk = array();
  
  if (!empty($name)) {
    
    
    $apk['Course.sname LIKE']=$name.'%';
     
  }
  $apk['Course.locality IS']=NULL;
  
  
   $courses = $this->Course->find('all')->where([$apk])->order(['Course.sort_order'=>'ASC']);
  
    $this->set('rec',$this->paginate($courses)->toarray());
  }

  
public function feature( $id = null,$status = null)
{
    $this->loadModel('Course');
    $this->loadModel('Seo');
    if (!empty($status)) {

       


       $course = $this->Course->get($id);
       $course->feature=$status;
       
        if($this->Course->save($course)){
       
          if($status == 'Y'){
            $conns = ConnectionManager::get('default');
            $query1 ="update tblcourse set feature ='".$status."' where parent_id=" . $id;
            $conns->execute($query1);

            $seoentry=$this->Seo->find('all')->where(['Seo.course_id' => $id,'Seo.orgid' =>0])->first();
            if($seoentry){
            
              $seopack = $this->Seo->get($seoentry['id']);
              
            $seo['course_id'] = $id;
                
            }else{
                 $seopack = $this->Seo->newEntity();
                 $seo['orgid']=0;
                 $seo['course_id'] = $id;
                 $seo['page'] = $course->cname;
                 $seor['status'] = "N";
                 $seo['title'] = "";
                 $seo['location'] = $course->url;
                 $seo['keyword'] = "";
                 $seo['description'] = "";
                 $seo['created']=date('Y-m-d');
            
            }
                 $savepackseo = $this->Seo->patchEntity($seopack, $seo);
                 $result= $this->Seo->save($savepackseo);


                 $user = $course->id;
                 $url = $course->url;
                 $image = $course->image;
                 $icon = $course->icon;
            
                  $fee_op=array('vidyadharnagar'=>'Vidyadhar Nagar','c-scheme'=>'C-Scheme','vaishalinagar'=>'Vaishali Nagar','mansarover'=>'Mansarover','banipark'=>'Banipark','murlipura'=>'Murlipura','jhotwara'=>'Jhotwara');
                  $romm = sizeof($fee_op);
            
                 foreach($fee_op as $krt=>$vall){
            
                  $this->request->data['parent_id']=$user;
                  $this->request->data['locality']=$krt;
              
                  $cseoentry=$this->Course->find('all')->where(['Course.parent_id' =>$user,'Course.locality' =>$krt])->first();
              
                  if($cseoentry['id']){  
                  $newresponse = $this->Course->get($cseoentry['id']);
                  }else{
                  $newresponse = $this->Course->newEntity();
            
                  $this->request->data['url']=$krt."/".$url;
                  }
              
            
                    $this->request->data['icon']=$icon;
                    $this->request->data['image']=$image;
                    $this->request->data['cname']=$course->cname;
                    $this->request->data['sort']=$course->sort;
                    $this->request->data['sname']=$course->sname;
                    $this->request->data['fees']=$course->fees;
                    $this->request->data['duration']=$course->duration;
                    $this->request->data['description']=$course->description;
            
                  
                  $this->request->data['status'] = 0;
                  
                  $this->request->data['course_category'] = '';
                  $this->request->data['eligible'] = '';
                  $this->request->data['category'] = 2;
                  $this->request->data['timing'] = "1.5 hour a day, 5 days a week";
                  $this->request->data['career'] = "";
                  $this->request->data['fpat'] = 0;
                  $this->request->data['ftype'] = "m";
                  $this->request->data['seo_title'] = $this->request->data['cname'];
                  $this->request->data['seo_keyword'] = "";
                  $this->request->data['seo_desc'] = "";
                  $this->request->data['sort_order'] = $this->request->data['sort'];
                  $this->request->data['add_date'] = date('Y-m-d');
                  $this->request->data['mod_date'] = date('Y-m-d');
                  $savepack = $this->Course->patchEntity($newresponse, $this->request->data);
                  $results=$this->Course->save($savepack);
                 
                     
                 
                      
                }




$locality=$this->Course->find('all')->where(['Course.parent_id' =>$id])->order(['Course.id'=>'ASC'])->toarray();


foreach($locality as $krty=>$irty){

 
  $seoentrye=$this->Seo->find('all')->where(['Seo.course_id' =>$irty['id'],'Seo.orgid' =>2])->first();
  if($seoentrye){
  
    $seopackr = $this->Seo->get($seoentrye['id']);
    
  
  $seor['course_id'] = $irty['id'];
      
  }else{
       $seopackr = $this->Seo->newEntity();
       $seor['orgid']=2;
       $seor['course_id'] = $irty['id'];
       $seor['status'] = "N";
       $seor['page'] = $irty['cname']." ".$irty['locality'];
       $seor['title'] = "";
       $seor['location'] = $irty['url'];
       $seor['locality'] = $irty['locality'];
       $seor['keyword'] = "";
       $seor['description'] = "";
       $seor['created']=date('Y-m-d');
  
  }
       $savepackseor = $this->Seo->patchEntity($seopackr, $seor);
      $this->Seo->save($savepackseor);

}



          }
        
                  
        $this->Flash->success(__('Course featured successfully.'));



        }else{

          $this->Flash->error(__('Course information  not updated successfully.'));

        }
                }

    $this->redirect($this->referer());
}

public function admin_make_supportiv($status = null, $id = null)
    {
        $this->loadModel('Course');
        if (!empty($status)) {

            if ($status == 'active') {

                $atp = 0;
            }
            if ($status == 'inactive') {
                $atp = 1;
            }
           $course = $this->Course->get($id);
           $course->status=$atp;
           
            if($this->Course->save($course)){
           

              $conns = ConnectionManager::get('default');
              $query1 ="update tblcourse set status ='".$atp."' where parent_id=" . $id;
              $conns->execute($query1);
                      
            $this->Flash->success(__('Course information updated successfully.'));
            }else{

              $this->Flash->error(__('Course information  not updated successfully.'));

            }
                    }

        $this->redirect($this->referer());
    }



public function add(){

  $this->viewBuilder()->layout('admin'); 

  $this->loadModel('Course');
  
  $this->loadModel('Seo');
  $this->loadModel('QuestionCategory');
  
  
  $coursesGroup = $this->Course->find('list', [
    'keyField' => 'course_group',
    'valueField' => 'course_group',
  ])->where(['course_group <>' => ''])->group(['Course.course_group'])->order(['Course.course_group' => 'ASC'])->toArray();
  
  $subjectsList = $this->QuestionCategory->find('list', [

    'keyField' => 'qc_id',
    'valueField' => 'qc_name',
  ])->where(['is_delete' => 'N'])->order(['QuestionCategory.qc_name' => 'ASC'])->toArray();
  $this->set(compact('coursesGroup', 'subjectsList'));

    $newresponse = $this->Course->newEntity();
  

  $this->set(compact('newresponse'));

 
  if ($this->request->is(['post', 'put'])){

// Insert As Main Course

$this->request->data['slocality']=$this->request->data['locality'];
if($this->request->data['qc_id']){
  $qc_id=$this->request->data['qc_id'];
  $this->request->data['qc_id']=implode(",",$qc_id);
  }
  
  $this->request->data['locality']=NULL;
  $stripped = str_replace(' ', '', $this->request->data['cname']);
  $this->request->data['url']=strtolower($stripped);
  
  
  if ($this->request->data['icon']['name'] != '')
   {
    $this->request->data['licon']=$this->request->data['icon'];
     $k = $this->request->data['icon'];
     $gallsh = $this->single_file($k,'images/course/');
  
     $this->request->data['icon'] = $gallsh[0];
     
   }
   if ($this->request->data['image']['name'] != '')
   {
    $this->request->data['limage']=$this->request->data['image'];
     $k = $this->request->data['image'];
     $galls = $this->single_file($k,'images/course/');
  
     $this->request->data['image'] = $galls[0];
     
   }  
  
   
   
   $this->request->data['status'] = 1;
   $this->request->data['course_category'] = '';
   $this->request->data['eligible'] = '';
   $this->request->data['category'] = 2;
   $this->request->data['timing'] = "1.5 hour a day, 5 days a week";
   $this->request->data['career'] = "";
   $this->request->data['fpat'] = "";
   $this->request->data['ftype'] = "m";
   $this->request->data['seo_title'] = $this->request->data['cname'];
   $this->request->data['seo_keyword'] = "";
   $this->request->data['seo_desc'] = "";
   $this->request->data['sort_order'] = $this->request->data['sort'];
   $this->request->data['add_date'] = date('Y-m-d');
   $this->request->data['mod_date'] = date('Y-m-d');
   $savepack = $this->Course->patchEntity($newresponse, $this->request->data);
   $results=$this->Course->save($savepack);
  
       $users = $results->id;
       $icon = $results->icon;
       $image = $results->image;
  
       

// End Main Course


  //// End As Course Locality Wise
  
  $this->Flash->flash("Course is Added successfully.", [
    "params" => [
    "type" => "success"
    ]
    ]);
			return $this->redirect(['action' => 'index']);  
		  

    
  }
}

public function edit($id){

  $this->viewBuilder()->layout('admin'); 

  $this->loadModel('Course');
  
  $this->loadModel('Seo');
  $this->loadModel('QuestionCategory');
  
  
  $coursesGroup = $this->Course->find('list', [
    'keyField' => 'course_group',
    'valueField' => 'course_group',
  ])->where(['course_group <>' => ''])->group(['Course.course_group'])->order(['Course.course_group' => 'ASC'])->toArray();
  
  $subjectsList = $this->QuestionCategory->find('list', [

    'keyField' => 'qc_id',
    'valueField' => 'qc_name',
  ])->where(['is_delete' => 'N'])->order(['QuestionCategory.qc_name' => 'ASC'])->toArray();
  $this->set(compact('coursesGroup', 'subjectsList'));

  
    $newresponse = $this->Course->get($id);

  $this->set(compact('newresponse'));
if ($this->request->is(['post', 'put'])){
// Edit As  Course
if($this->request->data['qc_id']){
$qc_id=$this->request->data['qc_id'];
$this->request->data['qc_id']=implode(",",$qc_id);
}else{
  $this->request->data['qc_id']="";
}
  $romm = sizeof($this->request->data['locality']);
  $this->request->data['slocality']=$this->request->data['locality'];
  $seoentrys=$this->Course->find('all')->where(['Course.id' =>$this->request->data['sid']])->first();
  $this->request->data['locality']=NULL;

  
if ($this->request->data['icon']['name'] != '')
 {
   $k = $this->request->data['icon'];
   $gallsh = $this->single_file($k,'images/course/');

   $this->request->data['icon'] = $gallsh[0];
   
 }else{
  $this->request->data['icon']=$seoentrys['icon'];

 }
 if ($this->request->data['image']['name'] != '')
 {
   $k = $this->request->data['image'];
   $galls = $this->single_file($k,'images/course/');

   $this->request->data['image'] = $galls[0];
   
 }else{
  $this->request->data['image']=$seoentrys['image'];

 }  

 
 
 $this->request->data['status'] = 0;
 
 $this->request->data['course_category'] = '';
 $this->request->data['eligible'] = '';
 $this->request->data['category'] = 2;
 $this->request->data['timing'] = "1.5 hour a day, 5 days a week";
 $this->request->data['career'] = "";
 $this->request->data['fpat'] = 0;
 $this->request->data['ftype'] = "m";
 $this->request->data['seo_title'] = $this->request->data['cname'];
 $this->request->data['seo_keyword'] = "";
 $this->request->data['seo_desc'] = "";
 $this->request->data['sort_order'] = $this->request->data['sort'];
 $this->request->data['add_date'] = date('Y-m-d');
 $this->request->data['mod_date'] = date('Y-m-d');
 $savepack = $this->Course->patchEntity($newresponse, $this->request->data);
 $results=$this->Course->save($savepack);

    



 
     if ($results){
			$this->Flash->success(__('Course has been saved.'));
			return $this->redirect(['action' => 'index']);  
		  }else{
			$this->Flash->error(__('Course has not saved.'));
			return $this->redirect(['action' => 'index']);  
		  }


  }


}




 
  










public function delete($id){

  $this->loadModel('Course');
  $catdelete = $this->Course->get($id);
  if($catdelete){
  unlink('images/' . $catdelete['file']);
  $this->Course->deleteAll(['Course.id' => $id]); 
  $this->Course->delete($catdelete);

  $this->Flash->success(__('Course has been deleted successfully.'));
  return $this->redirect(['action' => 'index']);
}else{
  $this->Flash->error(__('Course not  delete'));
  return $this->redirect(['action' => 'index']);
}


}





public function isAuthorized($user)
{
  if (isset($user['role_id']) && ($user['role_id'] == 1) || ($user['role_id'] == 2)) {
    return true;
  }
  return false;
}

}
?>