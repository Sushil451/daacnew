<?php

namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use \Datetime;

class InstallmentController extends AppController
{
	//$this->loadcomponent('Session');
	public function initialize(){	
		$this->loadModel('Installment');
		
		

		parent::initialize();
	}
	public function index(){ 	
		$this->viewBuilder()->layout('admin'); 
		$this->loadModel('Course');
        $this->loadModel('Installment');

        

		$courses = $this->Course->find('list', [
			'keyField' => 'id',
			'valueField' => 'sname',
		  ])->toArray();
		  $this->set('courses', $courses);

		  $rec=$this->Installment->find('all')->order(['Installment.inst_id' => 'desc'])->contain(['Course'])->where(['Installment.branch' => $this->request->session()->read('Auth.User.branch')]);
     
		  $this->set('rec',$this->paginate($rec)->toarray());
		
		  

	}

	public function search(){


		$this->loadModel('Installment');

		$rec=$this->Installment->find('all')->order(['Installment.inst_id' => 'desc'])->contain(['Course'])->where(['Installment.course_id' => $this->request->data['course_id'],'Installment.branch' => $this->request->session()->read('Auth.User.branch')]);
       // $this->paginate = array('limit' => '50', 'order' => array('Installment.inst_id' => 'desc'), 'conditions' => array('Installment.course_id' => $this->request->data['Search']['course_id'], 'Installment.branch' => $this->Session->read("Auth.User.branch")));

	   $this->set('rec',$this->paginate($rec)->toarray());
	   
	}

	public function select_course()
    {
//echo "hello"; die;
        $this->loadModel('Course');
		$fees =$this->Course->find('all')->select(['id','fees'])->where(['Course.id' => $this->request->data['id']])->first();
    
	
        $this->set('fees', $fees);
    }
	
	public function add (){
		$conn = ConnectionManager::get('default');

		$this->viewBuilder()->layout('admin'); 
		$this->loadModel('Course');
        $this->loadModel('Installment');
        $this->loadModel('InstDetail');
        $courses =$this->Course->find('list', [
			'keyField' => 'id',
			'valueField' => 'sname',
		  ])->toArray();
        $this->set('courses', $courses);


		
		if ($this->request->is('post')) {
			pr($this->request->data);
						$inst_name = $this->request->data['inst_name'];
						$course_id = $this->request->data['course_id'];
						$total_ins = $this->request->data['total_ins'];
						$session = $this->request->session();
						$branch = $session->read("Auth.User.branch");
						$status = 1;
						$add_date = date('Y-m-d');
			
						
						
						$conn->execute("insert into tbl_ins (inst_name,course_id,branch,percentage,discount,mod_date,total_ins,add_date,status) value ('" . $inst_name . "',$course_id,$branch,0,0,'$add_date',$total_ins,'" . $add_date . "',$status)");
						
						$inst_id=$this->Installment->find('all')->select(['inst_id'])->order(['Installment.inst_id' => 'DESC'])->first();
						
						
						
			
						for ($i = 0; $i < count($this->request->data['ins']); $i++) {
							$inst_idd = $inst_id['inst_id'];
							$amt = $this->request->data['ins'][$i];
			

							$stmt = $conn->execute("insert into tbl_ins_detail (inst_id,amt) value ('".$inst_idd."','".$amt."')");

							
						}
			
						$this->Flash->success(__('Installment has been saved.'));
						$this->redirect(array('action' => 'index'));
					}

	}


	public function edit($id = null)
    {
		$conn = ConnectionManager::get('default');

		$this->viewBuilder()->layout('admin'); 
		$this->loadModel('Course');
        $this->loadModel('Installment');
        $this->loadModel('InstDetail');
        $courses =$this->Course->find('list', [
			'keyField' => 'id',
			'valueField' => 'sname',
		  ])->toArray();
        $this->set('courses', $courses);


        if ($this->request->is('post')) {

            $inst_id = $this->request->data['inst_id'];
            $inst_name = $this->request->data['inst_name'];
            $course_id = $this->request->data['course_id'];
            $total_ins = $this->request->data['total_ins'];
			$session = $this->request->session();
			$branch = $session->read("Auth.User.branch");
            $status = 1;
            $add_date = date('Y-m-d');


			$conn->execute("update tbl_ins set inst_name='" . $inst_name . "',course_id=" . $course_id . ",branch=" . $branch . ",total_ins=" . $total_ins . ",mod_date='" . $add_date . "',status=" . $status . " where inst_id=" . $inst_id);
			$conn->execute("delete from tbl_ins_detail where inst_id=" . $inst_id);			
		
          

            for ($i = 0; $i < count($this->request->data['ins']); $i++) {
                $inst_idd = $inst_id;
                $amt = $this->request->data['ins'][$i];
				
				$stmt = $conn->execute("insert into tbl_ins_detail (inst_id,amt) value ('".$inst_idd."','".$amt."')");
            }

            $this->Session->setFlash(__('The Installments updated successfully'), 'flash/success');
            $this->redirect(array('action' => 'installment'));
        } else {
			$installment= $this->Installment->find('all')->where(['Installment.inst_id' => $id])->first();
		
		
            $this->set('installment', $installment);

        }
    }

	public function isAuthorized($user)
	{
		if (isset($user['role_id']) && ($user['role_id'] == 1 || $user['role_id'] == 4 )) {
			return true;
		}
		return false;
	}
        
}
