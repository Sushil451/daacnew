<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class EmailtemplateController extends AppController
{ 
  public function index(){ 
    $this->viewBuilder()->layout('admin');
    $this->loadModel('Emailtemplates');
    $this->loadModel('Roles');

    $emailtemplate = $this->Emailtemplates->find('all')->contain(['Roles'])->order(['Emailtemplates.id'=>'DESC']);
    $this->set('emailtemplate', $this->paginate($emailtemplate)->toarray());
  }

  public function status($id,$status){
    $this->loadModel('Emailtemplates');
    if(isset($id) && !empty($id)){
      $product = $this->Emailtemplates->get($id);
      $product->status = $status;
      if ($this->Emailtemplates->save($product)) {
        $this->Flash->success(__('Emailtemplate status has been updated.'));
        return $this->redirect(['action' => 'index']);  
      }
    }
  }

  public function search(){ 
    $this->loadModel('Emailtemplates');
    $this->loadModel('Roles');
    $user=$this->request->session()->read('Auth.User'); 
    $role_id = $this->request->data['role_id'];
    $session = $this->request->session(); 
    $session->delete('cond'); 
    $cond = [];   
    if(isset($role_id) && $role_id!='')
    {
      $cond['Emailtemplates.role_id LIKE']=$role_id;  
    }
    $session = $this->request->session();
    $this->request->session()->write('cond',$cond);
    
    $emailtemplate = $this->Emailtemplates->find('all')->contain(['Roles'])->where([$cond])->order(['Emailtemplates.id' => 'DESC'])->toarray();
    $this->set('emailtemplate', $emailtemplate);
  }
  
  public function add(){
    $this->viewBuilder()->layout('admin');
    $this->loadModel('Emailtemplates');

    $newpack = $this->Emailtemplates->newEntity();
    if ($this->request->is(['post', 'put'])) {  

      $savepack = $this->Emailtemplates->patchEntity($newpack, $this->request->data);
      $results=$this->Emailtemplates->save($savepack);
      if ($results){
        $this->Flash->success(__('Emailtemplate has been updated.'));
        return $this->redirect(['action' => 'index']);  
      }else{
        $this->Flash->error(__('Emailtemplate not updated'));
        return $this->redirect(['action' => 'edit']);
      }       
    }
  }


  public function edit($id){
    $this->viewBuilder()->layout('admin');
    $this->loadModel('Emailtemplates');

    $newpack = $this->Emailtemplates->get($id);
    $this->set('newpack',$newpack);
    if ($this->request->is(['post', 'put'])) {  

      $savepack = $this->Emailtemplates->patchEntity($newpack, $this->request->data);
      $results=$this->Emailtemplates->save($savepack);
      if ($results){
        $this->Flash->success(__('Emailtemplate has been updated.'));
        return $this->redirect(['action' => 'index']);  
      }else{
        $this->Flash->error(__('Emailtemplate not updated'));
        return $this->redirect(['action' => 'edit']);
      }       
    }
  }
  
  public function viewtemplate($id){
    $this->loadModel('Emailtemplates');
    $popupdata = $this->Emailtemplates->find('all')->where(['Emailtemplates.id'=>$id])->order(['Emailtemplates.id'=>DESC])->first();
    $this->set('popupdata',$popupdata);
  }

  public function isAuthorized($user){
    if (isset($user['role_id']) && ($user['role_id'] == 1)) {
      return true;
    }
    return false;
  }

  
}