<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class SeoController extends AppController

{ 
	public function index(){ 
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Seo');
		$this->loadModel('Course');
//echo "test"; die;

 $seo = $this->Seo->find('all')->order(['Seo.id'=>'DESC']);
  $this->set('seo', $this->paginate($seo)->toarray());



}





public function findcourseid(){

	$this->loadModel('Seo');
	$this->loadModel('Course');
	$orgid=$this->request->data['orgid'];
	if($this->request->data['orgid']==3){
		$orgid=0;
	}
	
	$this->set('orgid', $orgid);
	$maincoursesGroup = $this->Seo->find('list', [
		'keyField' => 'id',
		'valueField' => 'course_id',
	  ])->where(['Seo.orgid' =>$orgid])->order(['Seo.course_id' => 'ASC'])->toArray();
	$cio=[];
	  foreach($maincoursesGroup as $t=>$u){
	
		$cio[$t]=$u;
	  }
	  $coursesGroup = $this->Course->find('all')->where(['Course.id IN' =>$cio])->group(['Course.cname'])->order(['Course.cname' => 'ASC'])->toArray();
	  $this->set('coursesGroup', $coursesGroup);

}
	public function search(){ 
	
$this->loadModel('Seo');
$this->loadModel('Course');
//echo "test"; die;

$type = $this->request->data['type'];
$name = $this->request->data['name'];
$keyword = $this->request->data['keyword'];
$course_id = $this->request->data['course_id'];
$status = $this->request->data['status'];

$apk = array();

if (!empty($status)) {

	$apk['Seo.status']=$status;
}

if (!empty($type)) {
if($type==1){

	$apk['Seo.orgid']=1;

}
if($type==3){

	$apk['Seo.orgid']=0;
	if($this->request->data['course_id']){
	$apk['Seo.course_id']=$course_id;
	}
}

if($type==2){

	$apk['Seo.orgid']=2;


	if($this->request->data['course_id']){
		$course_id= $this->Course->find('all')->where(['Course.id' =>$this->request->data['course_id']])->first();

		$courseall_id= $this->Course->find('all')->where(['Course.parent_id' =>$course_id['parent_id']])->toarray();

		$cio=[];
	  foreach($courseall_id as $t=>$u){
	
		$cio[$t]=$u['id'];
	  }


	  $apk['Seo.course_id IN']=$cio;

	}
	

}
	
  
}

if (!empty($name)) {
	$apk['Seo.orgid']=2;
	
	$apk['Seo.locality LIKE']=$name.'%';
   

	
}



if (!empty($keyword)) {
	
	
	$apk['Seo.keyword LIKE']='%'.$keyword.'%';
   
}

 $seo = $this->Seo->find('all')->where([$apk])->order(['Seo.id'=>'DESC']);
  $this->set('seo', $this->paginate($seo)->toarray());
}
	

	public function add()
	{
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Seo');
		$this->loadModel('Course');
		$newpack = $this->Seo->newEntity();
		$this->set('newpack',$newpack);
		if ($this->request->is(['post', 'put'])) { 			
			$savepack = $this->Seo->patchEntity($newpack, $this->request->data);
			$results=$this->Seo->save($savepack);

     if ($results){
				$this->Flash->success(__('Seo has been saved.'));
				return $this->redirect(['action' => 'index']);	
			}else{
				$this->Flash->error(__('Seo not saved please fill your all fields'));
			return $this->redirect(['action' => 'add']);
			}
		}
	}
	public function settings($id=null)
	{
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Seosettings');
if($id){
	$newpack = $this->Seosettings->get($id);

}else{
	$newpack = $this->Seosettings->newEntity();

}
		
		$this->set('newpack',$newpack);
		if ($this->request->is(['post', 'put'])) { 			
			$savepack = $this->Seosettings->patchEntity($newpack, $this->request->data);
			$results=$this->Seosettings->save($savepack);

     if ($results){
				$this->Flash->success(__('Seo Settings has been updated.'));
				return $this->redirect(['action' => 'index']);	
			}else{
				$this->Flash->error(__('Seo Settings  not saved please fill your all fields'));
			return $this->redirect(['action' => 'settings']);
			}
		}
	}

	public function status($id,$status){

		$this->loadModel('Seo');
		if(isset($id) && !empty($id)){
			$product = $this->Seo->get($id);
			$product->status = $status;
			if ($this->Seo->save($product)) {
				$this->Flash->success(__('Seo status has been updated.'));
				return $this->redirect(['action' => 'index']);  
			}
		}
	}

	public function delete($id)
	{
		$this->loadModel('Seo');
		$seotdel = $this->Seo->get($id);
		if($seotdel){
			$this->Seo->deleteAll(['Seo.id' => $id]); 
			$this->Seo->delete($seotdel);

			$this->Flash->success(__('Seo has been deleted successfully.'));
			return $this->redirect(['action' => 'index']);
		}else{
			$this->Flash->error(__('Seo not  delete'));
			return $this->redirect(['action' => 'index']);
		}
	}

	public function edit($id)
	{
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Seo');
		$this->loadModel('Course');

		$newpack = $this->Seo->get($id);
		$this->set('newpack',$newpack);
		if ($this->request->is(['post', 'put'])) {	

			$savepack = $this->Seo->patchEntity($newpack, $this->request->data);
			$results=$this->Seo->save($savepack);

			if($results){
			$orgid=$results->orgid;

			if ($orgid==0 || $orgid==2){
				$course_id=$results->course_id;
				$location=$results->location;
				$seoentry=$this->Course->find('all')->where(['Course.id' =>$course_id])->first();
				if($seoentry){
			    $coursepack = $this->Course->get($seoentry['id']);
				$coursee['url'] = $location;
				$savepackcourse = $this->Course->patchEntity($coursepack, $coursee);
				$result= $this->Course->save($savepackcourse);
					
				}
					 
			}
				$this->Flash->success(__('Seo details has been updated.'));
				return $this->redirect(['action' => 'index']);	
			}else{
				$this->Flash->error(__('Seo not updated'));
			return $this->redirect(['action' => 'index']);
			}		    
		}
	}
	
	public function viewdocument($id)
	{
	$this->loadModel('Seo');
		$popupdata = $this->Seo->find('all')->where(['Seo.id'=>$id])->order(['Seo.id'=>DESC]);
		$this->set('popupdata', $this->paginate($popupdata)->toarray());
	}

		public function viewkeywords($id)
	{
	$this->loadModel('Seo');
		$keyworddata = $this->Seo->find('all')->where(['Seo.id'=>$id])->order(['Seo.id'=>DESC]);
		$this->set('keyworddata', $this->paginate($keyworddata)->toarray());
	
	}

	
	public function isAuthorized($user)
	{
		if (isset($user['role_id']) && ($user['role_id'] == 1)) {
			return true;
		}
		return false;
	}
}