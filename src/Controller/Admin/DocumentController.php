<?php

namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use \Datetime;

class DocumentController extends AppController
{
	//$this->loadcomponent('Session');
	public function initialize(){	
		$this->loadModel('Document');
		$this->loadModel('CategoryDocument');

		
		

		parent::initialize();
	}
	public function index(){ 	
		$this->viewBuilder()->layout('admin'); 
		$products=$this->Document->find('all')->contain(['CategoryDocument']);
		$this->loadModel('CategoryDocument');
       
		$categorydocument = $this->CategoryDocument->find('list', [
			'keyField' => 'id',
			'valueField' => 'name',
		  ])->order(['name' => 'ASC'])->toArray();
		  
		 
		  
		  $this->set(compact('categorydocument'));
		//pr($this->paginate($products)->toarray());
		//die;
		$this->set('products',$this->paginate($products)->toarray());
	 


	}


	public function add(){ 	

		$this->loadModel('CategoryDocument');
        $this->loadModel('Document'); 
		$categorydocument = $this->CategoryDocument->find('list', [
			'keyField' => 'id',
			'valueField' => 'name',
		  ])->order(['name' => 'ASC'])->toArray();
		  
		 
		  
		  $this->set(compact('categorydocument'));











		$this->viewBuilder()->layout('admin'); 

		$newpack = $this->Document->newEntity();
		if ($this->request->is(['post', 'put'])) {  
		  //pr($this->request->data); die;
		  
		  
			if ($this->request->data['file']['name'] != '')
			{
			  $k = $this->request->data['file'];
			  $galls = $this->single_file($k,'images/');
			  // $this->FcCreateThumbnail1("compress", "images/subcategories", $galls[0], $galls[0], "78", "78");
			  $this->request->data['file'] = $galls[0];
			  //unlink('compress/' . $galls[0]);
			}  
			$this->request->data['created']=date('Y-m-d H:i:s');
			
			
		  $savepack = $this->Document->patchEntity($newpack, $this->request->data);
		  $results=$this->Document->save($savepack);
		  if ($results){
			$this->Flash->success(__('Document has been saved.'));
			return $this->redirect(['action' => 'index']);  
		  }else{
			$this->Flash->error(__('Document not saved.'));
			return $this->redirect(['action' => 'index']);  
		  }
		}


	}


	public function delete($id)
	{
	    $this->loadModel('Document');
	    $catdelete = $this->Document->get($id);
	    if($catdelete){
		  unlink('images/' . $catdelete['file']);
		  $this->Document->deleteAll(['Document.id' => $id]); 
		  $this->Document->delete($catdelete);
	
		  $this->Flash->success(__('Document has been deleted successfully.'));
		  return $this->redirect(['action' => 'index']);
		}else{
		  $this->Flash->error(__('Document not  delete'));
		  return $this->redirect(['action' => 'index']);
		}
	}





	public function search(){ 
		$this->loadModel('Document'); 
		
		$this->loadModel('CategoryDocument');
		$req_data = $this->request->data();

		
		
		 $name = $req_data['name'];
		 $category_id = $req_data['b_id'];
		 $cond = [];   
		 if(isset($name) && $name!='')
		 {
		 $cond['Document.title LIKE']='%'.trim($name).'%';	
		 }
		 if(isset($category_id) && $category_id!='')
		 {
		 $cond['Document.category']=$category_id;	
		 }
	
	  //die;
	   
	   $documents=$this->Document->find('all')->contain(['CategoryDocument'])->where([$cond]);
	

	   $this->set('documents',$this->paginate($documents)->toarray());
		
		}
	  
	  


	
	
	  public function edit($id)
 	 { 
	  $this->viewBuilder()->layout('admin');
	  $this->loadModel('Document');
	  $this->loadModel('CategoryDocument');
	  
	
	  $categorydocument = $this->CategoryDocument->find('list', [
		  'keyField' => 'id',
		  'valueField' => 'name',
		])->order(['name' => 'ASC'])->toArray();
		
	
		$this->set('categorydocument',$categorydocument);
	  $product = $this->Document->get($id);
	  $this->set('newresponse',$product);
	  
	  if ($this->request->is(['post', 'put'])) {
		//pr($this->request->data); die;
		  if ($this->request->data['file']['name'] != '')
		  {
			$k = $this->request->data['file'];
			$galls = $this->single_file($k,'images/');
			// $this->FcCreateThumbnail1("compress", "images/subcategories", $galls[0], $galls[0], "78", "78");
			$this->request->data['file'] = $galls[0];
		//	unlink('compress/' . $galls[0]);
		  }else{
			$this->request->data['file'] = $product['file'];
		  }    
		
		$savepack = $this->Document->patchEntity($product, $this->request->data);
		$results=$this->Document->save($savepack);
		if ($results){
		  $this->Flash->success(__('Document has been updated.'));
		  return $this->redirect(['action' => 'index']);  
		}else{
		  $this->Flash->error(__('Document not Updated.'));
		  return $this->redirect(['action' => 'index']);  
		}           
	  }
	}
	
	

public function download($filename){




$this->response->file("images"."/". $filename ,
array('download'=> true, 'name'=> $filename));

}



	
	 public function isAuthorized($user)
	 {
		if (isset($user['role_id']) && ($user['role_id'] == 1)) {
			return true;
		}
		return false;
	 }
        
}
