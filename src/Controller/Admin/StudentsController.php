<?php

namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Datasource\ConnectionManager;
use Cake\Auth\FallbackPasswordHasher;

include '../vendor/tecnickcom/tcpdf/tcpdf.php';
class StudentsController extends AppController
{
	
  public function search(){ 

    
 //echo "test"; die;
 $this->loadModel('Students');
 $this->loadModel('StudentCourse');
 $this->loadModel('Reportdiscount');
 $this->loadModel('Discount');
 $this->loadModel('StudentInstallment');
 $this->loadModel('Cash');
 $this->loadModel('Certificate');
 $this->loadModel('Refund');
 $this->loadModel('StudentBatch');
 $this->loadModel('Batch');
 $this->loadModel('Branch');
 $this->loadModel('Course');
    $req_data = $this->request->data();
    $name = $req_data['name'];
    $branch_id = $req_data['branch_id'];
    $b_id = $req_data['b_id'];
    $en = $req_data['enrolment'];
    $stdstatus = $req_data['status'];
    $course = $req_data['crse'];


    $degrecourseList = $this->Course->find('list', [
      'keyField' => 'id',
      'valueField' => 'id',
    ])->where(['status' => 0, 'course_group' => 'Degree Courses'])->order(['sname' => 'ASC'])->toArray();
    $session = $this->request->session(); 
    $this->request->session()->delete('cond'); 
    $cond = [];   


    if (!empty($b_id)) {

      $batch_stu = $this->StudentBatch->find('list', [
        'keyField' => 's_id',
        'valueField' => 's_id',
      ])->where(['batch_id' =>$b_id])->order(['id' => 'ASC'])->toArray();

    
      $s_i = array();
      foreach ($batch_stu as $valu) {
          $s_i[] = $valu;
      }

      $cond['StudentCourse.s_id IN']=$s_i; 

  }

  if (!empty($branch_id)) {

    $cond['StudentCourse.branch_id']=$branch_id; 
   
    $selectedBranch = $branch_id;
}
if (!empty($en)) {

  $cond['Students.s_enroll']=$en; 
 

}


if ($this->request->session()->read('Auth.User.id') != 1) {

  $cond['StudentCourse.branch_id']=$this->request->session()->read('Auth.User.branch'); 


  $selectedBranch = $this->request->session()->read('Auth.User.branch');
}

if (!empty($name)) {
  $cond['Students.s_name LIKE']= $name . '%'; 
 

}
if (!empty($course)) {
  $cond['StudentCourse.course_id']=$course; 


}


if ($stdstatus == 'placed') {
  $cond['Students.is_placed']='D'; 

} elseif ($stdstatus == 'not_placed') {

  $cond['Students.is_placed !=']='D'; 
  $cond['Students.drop_out']='0'; 

} elseif ($stdstatus == 'cert_pending') {
  $cond['Students.cid']='0'; 
 
} elseif ($stdstatus == 'cert_generated') {
  $cond['Students.cid']='1'; 
  
} elseif ($stdstatus == 'active') {

  $cond['Students.drop_out']='0'; 
  $cond['StudentCourse.drop_out']='0'; 
  $cond['StudentCourse.status !=']='2'; 

} elseif ($stdstatus == 'drop') {
  $cond['StudentCourse.drop_out']='1'; 
  
} elseif ($stdstatus == 'complete') {
  $cond['StudentCourse.status']='2'; 
  
}elseif ($stdstatus == 'transferred') {
  $cond['Students.drop_out']='0'; 
  $cond['StudentCourse.is_transfer']='1'; 

} elseif ($stdstatus == 'new_reg') {

  
  $cond['StudentCourse.course_id NOT IN']=$degrecourseList; 

} elseif ($stdstatus == 'new_deg') {

   
  $cond['StudentCourse.course_id IN']=$degrecourseList; 


} elseif ($stdstatus == 'referred') {

  $cond['StudentCourse.rname NOT IN']=[null,'']; 

 
}  elseif ($stdstatus == '0') {
  $cond['Students.drop_out']=0; 
  $cond['StudentCourse.drop_out']=0; 


}



  
    $session = $this->request->session();
    $this->request->session()->write('cond',$cond);



$studentsall = $this->StudentCourse->find('all')->contain(['Branch','Students','Course'])->where([$cond])->group(['StudentCourse.s_id'])->order(['Students.s_name' => 'ASC'])->limit(20);
 $destination=$this->paginate($studentsall)->toarray();

 $this->set('destination', $destination);
 
 $predate = date('Y-m-d');
 $batch_code = array();
 foreach ($destination as $dest_val){

  $batch_det = $this->StudentBatch->find('all')->contain('Batch')->where(['StudentBatch.s_id' =>$dest_val['student']['s_id']])->order(['id' => 'DESC'])->toarray();

    
     $bt = array();
     if (!empty($batch_det)) {
         foreach ($batch_det as $det_val) {
           if($det_val['batch']['status']=='N'){
             $bt[] = $det_val['batch']['batch_code'];
         }
        }
     }
     $batch_code[$dest_val['student']['s_id']] = implode(',', $bt);

 }


 $this->set(compact('batch_code'));








  }



  public function branch()
  {

    $this->loadModel('Students');
    $this->loadModel('Users');
    $this->loadModel('Branch');
      $userbranch = $this->request->data['branch'];

      $st = $this->Students->find('all')->select(['s_id','s_enroll'])->where(['branch_id' =>$userbranch])->order(['s_id' => 'DESC'])->first();
      $brnchuser = $this->Users->find('all')->select(['enrollcode','branchhead','branch'])->where(['branch' =>$userbranch])->first();
   
      if (!empty($st)) {
          $enrol = substr($st['s_enroll'], 2, 100);

      } else {
          $enrol = 1000;
          // echo  $enrol+1; //die;

      }

      $enroll = $brnchuser['enrollcode'] . ($enrol + 1);
      //die;
      $br_det = $this->Branch->find('all')->where(['id' =>$userbranch])->toarray();
   
      $this->set('br_det', $br_det);
      $this->set('enrollment', $enroll);

      $this->set('branchhead', $brnchuser['branchhead']);
      $this->set('branch', $brnchuser['branch']);

  }
  public function discount_calc($ins = null, $dis)
  {
      $k = array();

      $total = array_sum($ins);
      $discount = ($total * $dis) / 100;
      $discountTotal = $total - $discount;

      $arrayCnt = count($ins);

      for ($i = $arrayCnt - 1; $i >= 0; $i--) {
          $k[$i] = $ins[$i];
          if ($total >= $discountTotal) {
              if ($ins[$i] <= $discount) {
                  $discount = $discount - $ins[$i];
                  $k[$i] = 0;
                  $total = $total - $ins[$i];
              } else {
                  $k[$i] = $k[$i] - $discount;
                  $total = $total - $ins[$i];
              }

          }

      }
      return $k;

  }

  public function selectchk()
  {
    $this->loadModel('InstDetail');
    $this->loadModel('Installment');
      $id = $this->request->data['inst_id'];
      if ($id != '') {

        $drestl = $this->InstDetail->find('all')->where(['inst_id' =>$id])->order(['id_id' => 'DESC'])->toarray();
        $course_det = $this->Installment->find('all')->where(['inst_id' =>$id])->order(['inst_id' => 'DESC'])->toarray();


    

          $course_fees = $course_det[0]['fees'];
          $this->set('course_fees', $course_fees);

          if (!empty($drestl)) {
              $this->set('det', $drestl);

              foreach ($drestl as $dtAS) {
                  $myArray[] = $studentsall['amt'];
              }
              if (isset($this->request->data['discount'])) {
                  $disc = $this->request->data['discount'];
                  $retrn = $this->discount_calc($myArray, $disc);

                  $total = array_sum($myArray);
                  $discount = ($total * $disc) / 100;
                  $discountTotal = $total - $discount;

                  $this->set('return', $retrn);
                  $this->set('disc', $disc);
                  $this->set('total', $total);
                  $this->set('discountTotal', $discountTotal);
              }
          } else {
              $dtl = '';
              $this->set('det', $drestl);
          }
      } else {
          $dtl = '';
          $this->set('det', $dtl);
      }

      $this->set('reg_fees', $this->request->data['reg_fees']);
  }

  public function inst()
    {
        $this->loadModel('Coursefranchise');
        $this->loadModel('Installment');
        $date = date('Y-m-d');
        $val = '';
        $idd = $this->request->data['id12'];
        $branch = $this->request->data['branch'];

      

        $reg_fee = 250;
       
        $this->set('dis_val', $val);
        $this->set('reg_fee', $reg_fee);

        if ($branch != '0') {
     $ac1 = $this->Installment->find('list', [
            'keyField' => 'inst_id',
            'valueField' => 'inst_name',
          ])->where(['course_id ' => $idd, 'branch' => $branch, 'status' => 1])->order(['inst_name' => 'ASC'])->toArray();
    
     if (empty($ac1)) {
                echo 5;die;
            }
            //pr($ac1); die;

        } else {


          $ac1 = $this->Installment->find('list', [
            'keyField' => 'inst_id',
            'valueField' => 'inst_name',
          ])->where(['course_id ' => $idd, 'branch' => 0, 'status' => 1])->order(['inst_name' => 'ASC'])->toArray();
    
           
        }

        //pr($ac1);
        $this->set('inst', $ac1);

    }

  public function checkcoursefeeflexibility()
  {
      //echo "hello"; die;
      $this->loadModel('Coursefranchise');
      $this->loadModel('Branch');
      $id = $this->request->data['id12'];
      $userbranch = $this->request->session()->read('Auth.User.branch');
      //echo $userbranch; die;
   
      $branch = $this->Branch->find('all')->where(['Branch.id' =>$userbranch])->order(['id' => 'DESC'])->first();

      //pr($branch); die;
      $ft = $branch['fee_flex'];
      //echo $ft; die;
      if ($ft == '1') {
        $coure = $this->Coursefranchise->find('all')->where(['branch' => $userbranch, 'course_id' => $id, 'status' => 'A'])->order(['id' => 'DESC'])->first();
         
          //pr($coure); die;
          if (!empty($coure)) {
              $reg = $coure['reg_fees'];
              echo $reg;die;
          } else {
              echo 0;die;

          }

      } else {
          echo 1;die;

      }

  }


  public function ajx()
  { //pr($this->request->data);
      $this->loadModel('Students');
     
      $this->loadModel('Branch');
      


      
      $br1 = $this->Branch->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();
    
          $this->set('brn', $br1);
      

      $enrl = $this->request->data['enrl'];


      $ssid = $this->Students->find('all')->where(['Students.s_enroll' =>$enrl])->order(['s_id' => 'DESC'])->first();

  
      $sid = $ssid['s_id'];

      $sold = $this->Students->find('all')->where(['Students.s_id' =>$sid])->order(['s_id' => 'DESC'])->first();

      $this->set('newresponse', $sold);

//die;

  }
  public function findcrsname()
  { //pr($this->request->data);
  
 $this->loadModel('Students');
	    $this->loadModel('StudentCourse');
	    $this->loadModel('Discount');
	   // pr($this->data);
	    $enrl= $this->request->data['enrl'];;
	    //die;
	 
      $ssid = $this->Students->find('all')->where(['Students.s_enroll' =>$enrl])->order(['s_id' => 'DESC'])->first();
      $sid = $ssid['s_id'];
      $Stdid = $this->Students->find('all')->where(['Students.s_id' =>$sid])->order(['s_id' => 'DESC'])->first();
	     //pr($Stdid);
	     //die;
	    $sid=$Stdid['s_id'];
	   $this->set('name',$Stdid['s_name']);


     $Stcrsid = $this->StudentCourse->find('all')->contain(['Course'])->where(['StudentCourse.s_id'=>$sid,'StudentCourse.drop_out'=>'0'])->order(['s_id' => 'DESC'])->toarray();

     $s = $this->Discount->find('all')->select(['course_id'])->where(['Discount.enrollment'=> $enrol])->toarray();
  
         	//pr($s); die;
         	$cid=array();
         	foreach($s as $crsid){ //pr($crsid);
				
				$cid[]=$crsid['course_id'];
				
			}
			//pr($cid);
			// die;
			$this->set('cid',$cid);
			//pr($Stcrsid);die;
          $this->set('stcrs',$Stcrsid);

//die;

  }
  
  public function exist()
  {
      $this->loadModel('StudentCourse');
      $this->loadModel('StudentInstallment');
      $this->loadModel('Mode');
      $this->loadModel('Course');
      $this->loadModel('Students');
      $this->loadModel('Branch');
      $this->viewBuilder()->layout('admin');
      $mode = $this->Mode->find('list', [
        'keyField' => 'id',
        'valueField' => 'name',
      ])->order(['name' => 'ASC'])->toArray();
      
      $this->set(compact('mode'));
 
      $ac1 = $this->Course->find('list', [
        'keyField' => 'id',
        'valueField' => 'sname',
      ])->where(['status' => 0])->order(['sname' => 'ASC'])->toArray();
      
      $degrecourseList = $this->Course->find('list', [
        'keyField' => 'id',
        'valueField' => 'id',
      ])->where(['status' => 0, 'course_group' => 'Degree Courses'])->order(['sname' => 'ASC'])->toArray();

      $this->set('degrecourseList', $degrecourseList); //pr($ac1); die;
      $this->set('categ1', $ac1); //pr($ac1); die;


      
      $br1 = $this->Branch->find('list', [
    'keyField' => 'id',
    'valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();

      $this->set('brn', $br1);

      $userbranch =$this->request->session()->read("Auth.User.branch");
      if ($this->request->session()->read('Auth.User.role_id') != 1) {
         
      $br_det = $this->Branch->find('all')->where(['Branch.id' =>$userbranch])->order(['id' => 'DESC'])->first();

      $this->set('br_det', $br_det);
      }

      $st = $this->Students->find('all')->where(['Students.branch_id' =>$userbranch])->order(['s_id' => 'DESC'])->first();
      

      if (!empty($st)) {
          $enrol = substr($st['s_enroll'], 2, 100);

      } else {
          $enrol = 1000;

      }

      $this->set('rol', $enrol);

   
      
      
      
      
      $this->set('newresponse', $newresponse);
      if ($this->request->is(['post', 'put'])) {
         
        
        $this->loadModel('Course');
        $this->loadModel('InstDetail');
        $this->loadModel('Setting');
        $this->loadModel('Cash');
        $this->loadModel('Users');
        $this->loadModel('Transaction');
    $articles = $this->InstDetail; 
    $course_fee = $this->InstDetail->find('all')->select(['amt' =>$articles->find('all')->func()->sum('amt')])->where(['inst_id' =>$this->request->data['inst_id']])->order(['id_id' => 'DESC'])->first();
    $branch_detail = $this->Branch->find('all')->where(['id' =>$this->request->data['branch']])->first();
    $gst = $this->Setting->find('all')->select(['sgst', 'igst', 'cgst'])->first();

    
    $this->request->data['branch']=$branch_detail['id'];
    $branchhead = $this->Users->find('all')->select(['enrollcode','branchhead','branch'])->where(['branch' =>$this->request->data['branch']])->first();

   
        // pr($branch_detail); die;
        $bran_head = $branchhead['branchhead'];
        $sgst = $gst['sgst'];
        $igst = $gst['igst'];
        $cgst = $gst['cgst'];
        $wt = $cgst + $sgst;

        $add = $this->request->data['add_date'];
        $bd = $this->request->data['b_date'];
        $this->request->data['s_pass'] = (new FallbackPasswordHasher)->hash($this->request->data['s_enroll']);
        $this->request->data['add_date'] = date('Y-m-d');
        $this->request->data['b_date'] = date('Y-m-d', strtotime($bd));
        $this->request->data['cid'] = 0;
        $this->request->data['reg_fees'] = 0;
      

if($this->request->data['s_id']){
  $newresponse = $this->Students->get($this->request->data['s_id']);
}else{
 
  $this->Flash->error(__('Student Enrollment information not found.'));

  return $this->redirect(['action' => 'exist']);


  
}
  $this->request->data['branch_id']=$this->request->data['branch'];
 
  
 

        $savepack = $this->Students->patchEntity($newresponse, $this->request->data);
   
				if($this->Students->save($savepack)) {

     
$vcrt=0;


  
  $gtt=$this->request->data['s_id'];



$studentcourse=$this->StudentCourse->find('all')->where(['s_id' =>$gtt])->order(['StudentCourse.s_id'=>'DESC'])->first();

$course_id=$studentcourse['course_id'];
$sc_id =$studentcourse['id'];

  $vcrt=0;

	//$conns = ConnectionManager::get('default');
//	$query1 ='update cms_student_courses set is_change="Y" where id=' . $sc_id;
	//$conns->execute($query1);




$conns = ConnectionManager::get('default');
$query12 ='delete from tbl_student_installment where s_id=' . $gtt . ' and course_id=' . $this->request->data['course_id'] . ' and recipt_no =0';
$conns->execute($query12);



      

            // echo $gtt;die;
            $this->request->data['s_id'] = $gtt;
            $course_id = $this->request->data['course_id'];
            $ins_id = $this->request->data['inst_id'];
            $brh_id = $this->request->data['branch'];
            $ad_date = $this->request->data['add_date'];

            
            $studentcourse = $this->StudentCourse->newEntity();
           
            $lss['s_id'] = $gtt;
            $lss['cid'] = 0;
            $lss['b_id'] = 0;
            $lss['course_id'] = $course_id;
            $lss['inst_id'] = $ins_id;
            $lss['branch_id'] = $brh_id;
            $lss['fee'] = $course_fee['amt'];
            $lss['drop_out'] = 0;
            $lss['discount'] = 0;
            $lss['reg_fees'] = 0;
            $lss['drop_reasn'] = "";
            $lss['is_transfer'] = 0;
            $lss['drop_date'] = "0000-00-00";
            $lss['add_date'] = $ad_date;
            $lss['transfer_date'] = "0000-00-00";
            $lss['transfer_reason'] = "";
            $lss['completedate'] = "0000-00-00";
            $lss['certifiacte'] = 0;
            $lss['status'] = 0;
            if (in_array($course_id, $degrecourseList)) {
                $sessionDetail = explode('-', $this->request->data['session']);
                $year = $sessionDetail[1];
                $session = $sessionDetail[0];
                $lss['session'] = $session;
            }else{
                $lss['session'] = "";


            }

      
          //pr($this->request->data); die;
            //pr($course_id); die;
            $savepackCourse = $this->StudentCourse->patchEntity($studentcourse, $lss);

            $this->StudentCourse->save($savepackCourse);
            //    pr($this->request->data); die;
            $install = $this->request->data['installment'];
            $yr = date('Y');
            $mn = date('m');
            $dy = date('d');
            $mnth = 0;


            $course_det = $this->Course->find('all')->where(['id' =>$course_id])->first();

        
            $monthh = 0;
            $monthy = 0;
           // $reg = array_pop($install);

            //$gt = $this->InstDetail->find('all', array('conditions' => array('InstDetail.inst_id' => $idd)));

           // $install = array('reg' => $reg) + $install;
            //pr($install); die;

			$conns = ConnectionManager::get('default');
			$query1 ="update tbl_student set status='1' where s_id=" . $gtt;
			$result=$conns->execute($query1);


/* Update Query For cms_student_course Query */

$conns = ConnectionManager::get('default');
$query1s ="update cms_student_courses set status='1' where s_id=$gtt and course_id=$course_id";
$conns->execute($query1s);

$reg_status = "n";
 

            foreach ($install as $in_key => $in_value) {
                //pr($install)

                $studentinstall = $this->StudentInstallment->newEntity();
           
                $tn = array();
                if ($in_value == '' || $in_value == 0) {
                    $key_install =& $install;

                    unset($key_install[$in_key]);
                    //pr($install);die;

                    continue;
                }
                //pr($install); die;

               

                    if ($course_det['ftype'] == 'm') {
                        $f_date = date('Y-m-d');
                        if ($dy >= 15) {
                            $date3min = strtotime(date("Y-m-30", strtotime($f_date)) . "+" . $mnth . " Month");
                            $date = date('Y-m-d', $date3min);
                        } else {
                            $date3min = strtotime(date("Y-m-15", strtotime($f_date)) . "+" . $mnth . " Month");
                            $date = date('Y-m-d', $date3min);
                        }

                        $mn++;
                        $mnth++;

                    } elseif ($course_det['ftype'] == 'h' && in_array($course_det['id'], $degrecourseList)) {
                        $sessionDetail = explode('-', $this->request->data['session']);
                        $year = $sessionDetail[1];
                        $session = $sessionDetail[0];
                        if ($session == 'june') {
                            $f_date = $year . '-05-30';
                        } else {
                            $f_date = $year . '-11-30';
                        }
                        $date3min = strtotime(date("Y-m-d", strtotime($f_date)) . "+" . $monthh . " Month");
                        $date = date('Y-m-d', $date3min);
                        $monthh += 6;
                    } elseif ($course_det['ftype'] == 'y' && in_array($course_det['id'], $degrecourseList)) {
                        $sessionDetail = explode('-', $this->request->data['session']);
                        $year = $sessionDetail[1];
                        $session = $sessionDetail[0];
                        if ($session == 'june') {
                            $f_date = $year . '-05-30';
                        } else {
                            $f_date = $year . '-11-30';
                        }
                        $date3min = strtotime(date("Y-m-d", strtotime($f_date)) . "+" . $monthh . " Month");
                        $date = date('Y-m-d', $date3min);
                        $monthh += 12;
                    } elseif ($course_det['ftype'] == 'h') {
                        $f_date = date('Y-m-d');
                        $date3min = strtotime(date("Y-m-d", strtotime($f_date)) . "+" . $monthh . " Month");
                        $date = date('Y-m-d', $date3min);
                        $monthh += 6;
                    } else {

                        $f_date = date('Y-m-d');

                        $date3min = strtotime(date("Y-m-d", strtotime($f_date)) . "+" . $monthy . " Month");
                        $date = date('Y-m-d', $date3min);
                        $monthy += 12;
                    }

                    //pr($date); die;

                    $tn['s_id'] = $gtt;

                    $tn['course_id'] = $this->request->data['course_id'];
                    $tn['inst_id'] = $ins_id;
                    $tn['pro_date'] = $date;
                    $tn['pay_date'] = "0000-00-00";
                    $tn['fine'] = '0';
                    $tn['is_drop'] = '0';
                    $tn['mode'] = '0';
                    $tn['chequedetail'] = '';
                    $tn['bank'] = '';
                    if ($branch_detail['gstapli'] == '1') {

                        if ($branch_detail['gstinclu'] == '1') {
                            $x1 = $wt / 100;
                            $y1 = 1 + $x1;
                            $z1 = $install / $y1;
                            $sgs = ($z1 * $sgst) / 100;
                            $cgs = ($z1 * $cgst) / 100;

                            $tot = $z1;
                            $tn['amount'] = $in_value;
                            $tn['cgst_percent'] = $cgst;
                            $tn['sgst_percent'] = $sgst;
                            $tn['cgst_amount'] = $cgs;
                            $tn['sgst_amount'] = $sgs;
                            $tn['gst_num'] = $branch_detail['gsnum'];
                            $tn['gst_inclu'] = $branch_detail['gstinclu'];

                        } else {
                            $z1 = $install;
                            $sgs = ($z1 * $sgst) / 100;
                            $cgs = ($z1 * $cgst) / 100;
                            $tot = $z1;
                            $tn['amount'] = $in_value;
                            $tn['cgst_percent'] = $cgst;
                            $tn['sgst_percent'] = $sgst;
                            $tn['cgst_amount'] = $cgs;
                            $tn['sgst_amount'] = $sgs;
                            $tn['gst_num'] = $branch_detail['gsnum'];
                            $tn['gst_inclu'] = $branch_detail['gstinclu'];

                        }
                    } elseif ($branch_detail['gstapli'] == '2') {

                        if ($branch_detail['gstinclu'] == '1') {
                            $x1 = $igst / 100;
                            $y1 = 1 + $x1;
                            $z1 = $install / $y1;
                            $igs = ($z1 * $igst) / 100;
                            $tot = $z1;
                            $tn['amount'] = $in_value;
                            $tn['igst_percent'] = $igst;
                            $tn['igst_amount'] = $igs;
                            $tn['gst_num'] = $branch_detail['gsnum'];
                            $tn['gst_inclu'] = $branch_detail['gstinclu'];

                        } else {
                            $z1 = $install;
                            $igs = ($z1 * $igst) / 100;
                            $tot = $z1;
                            $tn['amount'] = $in_value;
                            $tn['igst_percent'] = $igst;
                            $tn['igst_amount'] = $igs;
                            $tn['gst_num'] = $branch_detail['gsnum'];
                            $tn['gst_inclu'] = $branch_detail['gstinclu'];

                        }
                    } else {
                        $sgs = '0';
                        $cgs = '0';
                        $igs = '0';
                        $tot = $install;
                        $tn['amount'] = $in_value;
                    }

                    $tn['branch_id'] = $brh_id;

                    $tn['branch_name'] = $branch_detail['name'];
                    $tn['branch_mobile'] = $branch_detail['phone'];
                    $tn['address'] = $branch_detail['address'];
                    $tn['branch_head'] = $bran_head;
                   
                    $tn['pay_date'] = "0000-00-00";
//pr($tn);die;

                    $amt = $in_value;

                    if ($amt != 0) {

                      $transtn = $this->StudentInstallment->newEntity();
                      $dasdasd = $this->StudentInstallment->patchEntity($transtn, $tn);
  
                      $re= $this->StudentInstallment->save($dasdasd);

                       
                    } else {
                        continue;
                    }

                
            }
  // $ghj=$this->StudentInstallment->getInsertID();
 // window.open('" . ADMIN_URL . "fee/mpdf/" . $gtt . "/" . $receipt . "/" . $course_id . "');
           

          

              $this->Flash->success(__('Existing Student Information has been Updated successfully.'));
              return $this->redirect(['action' => 'index/'. $gtt]);

                
            

        }

      }

  }

  public function droppops($cid, $sid, $insid)
  {
    $this->loadModel('Students');
    $stud = $this->Students->find('all')->contain(['Branch'])->where(['Students.s_id' =>$sid])->order(['s_id' => 'DESC'])->first();
    $this->set('stu_det', $stud);
      $this->set('cid', $cid);
      $this->set('sid', $sid);
      $this->set('insid', $insid);
    if($this->request->is(['post', 'put'])) {

      
       
            $id = $this->request->data['s_id'];
            $instid = $this->request->data['instid'];
            $corsid = $this->request->data['corsid'];
	$conns = ConnectionManager::get('default');
	$query1 ="update tbl_student_installment set is_drop=1 where s_id=$id and inst_id=$instid";
	$conns->execute($query1);
  $today = date('Y-m-d'); 
            $conns = ConnectionManager::get('default');
            $query1 ="update cms_student_courses set drop_out='1', drop_reasn='" . $this->request->data['reason'] . "',drop_date='" . $today . "' where s_id=$id and course_id=$corsid";
            $conns->execute($query1);
          
   
$this->Flash->flash("Selected Student is dropped out.", [
"params" => [
  "type" => "success"
]
]);
            $this->redirect(array('action' => 'index'));


    }

  }

  public function iscomplete($cid = '', $sid = '', $sts = '')
    {
        $today = date('Y-m-d');


        $conns = ConnectionManager::get('default');
        $query1 ="update cms_student_courses set status='" . $sts . "',completedate='" . $today . "' where s_id=$sid and course_id=$cid";
        $conns->execute($query1);
        /* Update Query For cms_student_course Query */

        $this->Flash->flash("Selected Student Status has been updated.", [
          "params" => [
            "type" => "success"
          ]
          ]);

        $this->redirect(array('action' => 'index'));

    }

  public function refund($s_id = "", $c_id = "")
  {

      $this->loadModel('StudentCourse');
      $this->loadModel('StudentInstallment');
      $this->loadModel('Cash');
      $this->loadModel('Students');
      $this->loadModel('Refund');
      $this->loadModel('Course');
      $this->set('cid', $c_id);
      $this->set('sid', $s_id);

      $stud = $this->Students->find('all')->contain(['Branch'])->where(['Students.s_id' =>$s_id])->order(['s_id' => 'DESC'])->first();
      $this->set('stu_det', $stud);


      $ac1 = $this->Course->find('list', [
        'keyField' => 'id',
        'valueField' => 'sname',
      ])->where(['status' => 0])->order(['sname' => 'ASC'])->toArray();

      $this->set('categ1', $ac1);

      $payment = $this->StudentInstallment->find('all')->where(['StudentInstallment.s_id' => $s_id, 'StudentInstallment.course_id' => $c_id, 'StudentInstallment.recipt_no !=' => 0, 'StudentInstallment.amount !=' => '250'])->order(['s_id' => 'DESC'])->toarray();

      $pay_his = array();
      $paid_data = array();

      if (!empty($payment)) {
          foreach ($payment as $pay_value) {

              $paid_data['amount'][] = $pay_value['amount'];
              $paid_data['receipt_no'][] = $pay_value['recipt_no'];

          }
      }

      $new_payment = $this->Cash->find('all')->where(['Cash.s_id' => $s_id, 'Cash.cid' => $c_id, 'Cash.status' => 'Y', 'Cash.is_reg' => 'N'])->order(['Cash.s_id' => 'DESC'])->toarray();
     

      if (!empty($new_payment)) {
          $paid_data = array();

          foreach ($new_payment as $npay_value) {

              $paid_data['amount'][] = $npay_value['amount'];
              $paid_data['receipt_no'][] = $npay_value['receipt_no'];

          }
      }
      $rec_no = implode(',', $paid_data['receipt_no']);
      if (!empty($paid_data['amount'])) {
          $paid_amt = array_sum($paid_data['amount']);
      } else {
          $paid_amt = 0;
      }

      $this->set('paid_amt', $paid_amt);

      if ($this->request->is('post')) {
          //pr($this->request->data); die;
      
          //pr($Refund); die;
          $id = $this->request->data['s_id'];
          $corsid = $this->request->data['course_id'];


          $conns = ConnectionManager::get('default');
          $query1 ="update tbl_student_installment set is_drop=1 where s_id=$id and course_id=$corsid";
          $conns->execute($query1);
          $this->request->data['date'] = date('Y-m-d');
          $this->request->data['created'] = date('Y-m-d');
          /* Drop Out Student Data Update in cms_student_courses Table */

          $conns = ConnectionManager::get('default');
          $query1 ="update cms_student_courses set drop_out='1', drop_reasn='" . $this->request->data['description'] . "',drop_date='" . $this->request->data['date'] . "' where s_id=$id and course_id=$corsid";
          $conns->execute($query1);

         
//pr($Refund); die;


    $transtn = $this->Refund->newEntity();
     $dasdasd = $this->Refund->patchEntity($transtn, $this->request->data);
  
    $re= $this->Refund->save($dasdasd);



          $this->Flash->flash("Selected Student Refund has been updated.", [
            "params" => [
              "type" => "success"
            ]
            ]);
          $this->redirect(array('action' => 'index'));
      }
  }

  public function idcards($sid, $cid)
  {
    $this->loadModel('Students');
    $this->loadModel('Course');

    $stud_det = $this->Students->find('all')->contain(['Branch'])->where(['Students.s_id' =>$sid])->order(['s_id' => 'DESC'])->first();
    $cor_det = $this->Course->find('all')->where(['Course.id' =>$cid])->order(['id' => 'DESC'])->first();
     $this->set('students', $stud_det);
      $this->set('cid', $cid);
      $this->set('sid', $sid);
      $this->set('cor_det', $cor_det);

  }

  public function mprint($id = "", $cid = "")
  {
    $this->set('ide', trim($id));
    $this->set('cide', trim($cid));
  }
  public function certppopups($cid = "", $sid = "")
  {

    $this->loadModel('Students');
    $this->loadModel('Course');
    $this->loadModel('Certificate');

    $ac1 = $this->Course->find('list', [
      'keyField' => 'id',
      'valueField' => 'sname'])->where(['status' => 0])->order(['sname' => 'ASC'])->toArray();
      $this->set('categ1', $ac1);

      $stud_det = $this->Students->find('all')->contain(['Branch'])->where(['Students.s_id' =>$sid])->order(['s_id' => 'DESC'])->first();
    
      //pr($stud_det); die;
      $this->set('stu_det', $stud_det);
      $this->set('cid', $cid);
      $this->set('sid', $sid);

      if ($this->request->is('post')) {
        //pr($this->request->data); die;
    
        //pr($Refund); die;
        $id = $this->request->data['sid'];
        $corsid = $this->request->data['course_id'];

			$this->request->data['add_date'] = date('Y-m-d');
			$this->request->data['publish'] = 0;
			$this->request->data['admin_type'] = 0;
			$this->request->data['admin_id'] = 0;
			$this->request->data['issue'] = "P";
			$this->request->data['delivery_date'] = "";
			$this->request->data['comment'] = "";
			$this->request->data['course_includes'] = "";
			$this->request->data['cdetail'] = "";
      $length = 4;

      $randomString1 = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
  
      $randomString2 = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
  
      $vcode = $randomString1 . '-' . $randomString2;
			$this->request->data['verify_code'] = $vcode;

			if ($this->request->data['sid'] != '') {

				$stid = $this->request->data['sid'];
				$course_id = $this->request->data['course_id'];

        $transtn = $this->Certificate->newEntity();
        $dasdasd = $this->Certificate->patchEntity($transtn, $this->request->data);
     
       $re= $this->Certificate->save($dasdasd);
    
       $conns = ConnectionManager::get('default');
       $query1 ="update tbl_student set cid='1' where s_id=" . $stid;
       $conns->execute($query1);

       $conns = ConnectionManager::get('default');
       $query1 ="update cms_student_courses set certifiacte='1' where s_id=" . $stid . " AND course_id=" . $course_id;
       $conns->execute($query1);

			 $this->Flash->flash("The Certificate page is Added successfully.We can Download it.", [
        "params" => [
          "type" => "success"
        ]
        ]);
		

					$this->redirect(array('action' => 'index'));

				
      } else {

        $transtn = $this->Certificate->newEntity();
        $dasdasd = $this->Certificate->patchEntity($transtn, $this->request->data);
     
       $re= $this->Certificate->save($dasdasd);

       $this->Flash->flash("The Certificate page is Added successfully..We can Download it.", [
        "params" => [
          "type" => "success"
        ]
        ]);


					$this->redirect(array('action' => 'index'));

				

			}
    }
     

  }
  public function transpops($cid, $sid, $insid, $bname, $bid)
  {

      $this->set('cid', $cid);
      $this->set('sid', $sid);
      $this->set('insid', $insid);
     
      $this->set('bid', $bid);

      $this->loadModel('Students');
      $this->loadModel('Branch');
      $stud = $this->Students->find('all')->contain(['Branch'])->where(['Students.s_id' =>$sid])->order(['s_id' => 'DESC'])->first();
      $this->set('stu_det', $stud);


      

        $branch = $this->Branch->find('all')->where(['Branch.id' =>$bid])->order(['id' => 'DESC'])->first();
      $this->set(compact('branch'));


      if($this->request->is(['post', 'put'])) {

      $this->loadModel('StudentCourse');
      //pr($this->request->data);die;
      $today = date('Y-m-d');
      $instid = $this->request->data['instid'];
      $corsid = $this->request->data['corsid'];
      $ssid = $this->request->data['sid'];

      $conns = ConnectionManager::get('default');
      $query1 ="update cms_student_courses set branch_id=" . $this->request->data['new_branch'] . ", is_transfer='" . $this->request->data['old_br'] . "',transfer_reason='" . $this->request->data['reason'] . "',transfer_date='" . $today . "' where s_id=$ssid and course_id=$corsid";
      $conns->execute($query1);

    

/* End of  Cms Student Courses Update Query*/
$conns = ConnectionManager::get('default');
$query1 ="update tbl_student_installment set branch_id=" . $this->request->data['new_branch'] . " where s_id=" . $this->request->data['sid'] . " AND pay_date='0000-00-00' and inst_id=$instid";
$conns->execute($query1);

       
$this->Flash->flash("Selected Student is transfered.", [
  "params" => [
    "type" => "success"
  ]
  ]);

 

      $this->redirect(array('action' => 'index'));
    }
  }
  public function add($id=null)
  {
      $this->loadModel('StudentCourse');
      $this->loadModel('StudentInstallment');
      $this->loadModel('Mode');
      $this->loadModel('Course');
      $this->loadModel('Students');
      $this->loadModel('Branch');
      $this->viewBuilder()->layout('admin');
      $mode = $this->Mode->find('list', [
        'keyField' => 'id',
        'valueField' => 'name',
      ])->order(['name' => 'ASC'])->toArray();
      
      $this->set(compact('mode'));
 
      $ac1 = $this->Course->find('list', [
        'keyField' => 'id',
        'valueField' => 'sname',
      ])->where(['status' => 0])->order(['sname' => 'ASC'])->toArray();
      
      $degrecourseList = $this->Course->find('list', [
        'keyField' => 'id',
        'valueField' => 'id',
      ])->where(['status' => 0, 'course_group' => 'Degree Courses'])->order(['sname' => 'ASC'])->toArray();

      $this->set('degrecourseList', $degrecourseList); //pr($ac1); die;
      $this->set('categ1', $ac1); //pr($ac1); die;


      
      $br1 = $this->Branch->find('list', [
    'keyField' => 'id',
    'valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();

      $this->set('brn', $br1);

      $userbranch =$this->request->session()->read("Auth.User.branch");
      if ($this->request->session()->read('Auth.User.role_id') != 1) {
         
          $br_det = $this->Branch->find('all')->where(['Branch.id' =>$userbranch])->order(['id' => 'DESC'])->first();

          $this->set('br_det', $br_det);
      }

      $st = $this->Students->find('all')->where(['Students.branch_id' =>$userbranch])->order(['s_id' => 'DESC'])->first();
      

      if (!empty($st)) {
          $enrol = substr($st['s_enroll'], 2, 100);

      } else {
          $enrol = 1000;

      }

      $this->set('rol', $enrol);

   
      if($id){
       
        $newresponse = $this->Students->get($id);
      }else{
        $newresponse = $this->Students->newEntity();
      
      }
      $this->set('newresponse', $newresponse);
      if ($this->request->is(['post', 'put'])) {
        
    
        $this->loadModel('Course');
        $this->loadModel('InstDetail');
        $this->loadModel('Setting');
        $this->loadModel('Cash');
        $this->loadModel('Users');
        $this->loadModel('Transaction');
    $articles = $this->InstDetail; 
    $course_fee = $this->InstDetail->find('all')->select(['amt' =>$articles->find('all')->func()->sum('amt')])->where(['inst_id' =>$this->request->data['inst_id']])->order(['id_id' => 'DESC'])->first();

   
    $branch_detail = $this->Branch->find('all')->where(['id' =>$this->request->data['branch']])->first();
    
    
    $gst = $this->Setting->find('all')->select(['sgst', 'igst', 'cgst'])->first();

    
    $this->request->data['branch']=$branch_detail['id'];
    $branchhead = $this->Users->find('all')->select(['enrollcode','branchhead','branch'])->where(['branch' =>$this->request->data['branch']])->first();

   
        // pr($branch_detail); die;
        $bran_head = $branchhead['branchhead'];
        $sgst = $gst['sgst'];
        $igst = $gst['igst'];
        $cgst = $gst['cgst'];
        $wt = $cgst + $sgst;

        $add = $this->request->data['add_date'];
        $bd = $this->request->data['b_date'];
        $this->request->data['s_pass'] = (new FallbackPasswordHasher)->hash($this->request->data['s_enroll']);
        $this->request->data['add_date'] = date('Y-m-d', strtotime($add));
        $this->request->data['b_date'] = date('Y-m-d', strtotime($bd));
        $this->request->data['cid'] = 0;
      

  $this->request->data['b_id']=0;
  $this->request->data['reg_fees']=0;
  $this->request->data['admin_type']=0;
  $this->request->data['admin_id']=0;
  $this->request->data['drop_out']=0;
  $this->request->data['status']=0;
  $this->request->data['discount']=0;
  $this->request->data['drop_reasn']='';
  $this->request->data['is_transfer']=0;
  $this->request->data['drop_date']="0000-00-00";
  $this->request->data['transfer_date']="0000-00-00";

  $this->request->data['transfer_reason']="";
  
  $this->request->data['pdf']="";
  $this->request->data['pdfcreated']="";
  if($this->request->data['type'] !=11){

    $this->request->data['rname']=""; 
    $this->request->data['rmobile']=""; 
    $this->request->data['ramount']=""; 
  }

  $this->request->data['branch_id']=$this->request->data['branch'];
  
  if ($this->request->data['image']['name'] != '')
  {
    $k = $this->request->data['image'];
    $galls = $this->single_file($k,'images/student/');
    // $this->FcCreateThumbnail1("compress", "images/subcategories", $galls[0], $galls[0], "78", "78");
    $this->request->data['image'] = $galls[0];
    //unlink('compress/' . $galls[0]);
  }else if($this->request->data['s_id']){


    $useida = $this->Students->find('all')->where(['s_id' =>$this->request->data['s_id']])->order(['Students.s_id'=>'DESC'])->first();
    $this->request->data['image'] = $useida['image'];

  } 

        $savepack = $this->Students->patchEntity($newresponse, $this->request->data);
   
				if($this->Students->save($savepack)) {

      $useida = $this->Students->find('all')->order(['Students.s_id'=>'DESC'])->first();

			$gtt = $useida['s_id'];
$vcrt=0;
if($this->request->data['s_id']){

  
  $gtt=$this->request->data['s_id'];
  $this->Flash->set(__('Student Information has been Updated successfully.'), ['key' => 'refer_fail']);
  return $this->redirect(['action' => 'index/'. $gtt]);


$studentcourse=$this->StudentCourse->find('all')->where(['s_id' =>$gtt])->order(['StudentCourse.s_id'=>'DESC'])->first();

$course_id=$studentcourse['course_id'];
$sc_id =$studentcourse['id'];
if($course_id!=$this->request->data['course_id']){
  $vcrt=0;

	$conns = ConnectionManager::get('default');
	$query1 ='update cms_student_courses set is_change="Y" where id=' . $sc_id;
	$conns->execute($query1);

}else{
  $vcrt=$sc_id;
}


$conns = ConnectionManager::get('default');
$query12 ='delete from tbl_student_installment where s_id=' . $gtt . ' and course_id=' . $this->request->data['course_id'] . ' and recipt_no =0';
$conns->execute($query12);


}
      

            // echo $gtt;die;
            $this->request->data['s_id'] = $gtt;
            $course_id = $this->request->data['course_id'];
            $ins_id = $this->request->data['inst_id'];
            $brh_id = $this->request->data['branch'];
            $ad_date = $this->request->data['add_date'];

            if($vcrt==0){
            $studentcourse = $this->StudentCourse->newEntity();
            }else{
            $studentcourse = $this->StudentCourse->get($vcrt);
            }
            $lss['s_id'] = $gtt;
            $lss['cid'] = 0;
            $lss['b_id'] = 0;
            $lss['course_id'] = $course_id;
            $lss['inst_id'] = $ins_id;
            $lss['branch_id'] = $brh_id;
            $lss['fee'] = $course_fee['amt'];
            $lss['drop_out'] = 0;
            $lss['discount'] = 0;
            $lss['reg_fees'] = 0;
            $lss['drop_reasn'] = "";
            $lss['is_transfer'] = 0;
            $lss['drop_date'] = "0000-00-00";
            $lss['add_date'] = $ad_date;
            $lss['transfer_date'] = "0000-00-00";
            $lss['transfer_reason'] = "";
            $lss['completedate'] = "0000-00-00";
            $lss['certifiacte'] = 0;
            $lss['status'] = 0;
            if (in_array($course_id, $degrecourseList)) {
                $sessionDetail = explode('-', $this->request->data['session']);
                $year = $sessionDetail[1];
                $session = $sessionDetail[0];
                $lss['session'] = $session;
            }else{
                $lss['session'] = "";


            }

      
          //pr($this->request->data); die;
            //pr($course_id); die;
            $savepackCourse = $this->StudentCourse->patchEntity($studentcourse, $lss);

            $this->StudentCourse->save($savepackCourse);
            //    pr($this->request->data); die;
            $install = $this->request->data['installment'];
            $yr = date('Y');
            $mn = date('m');
            $dy = date('d');
            $mnth = 0;


            $course_det = $this->Course->find('all')->where(['id' =>$course_id])->first();

        
            $monthh = 0;
            $monthy = 0;
           // $reg = array_pop($install);

            //$gt = $this->InstDetail->find('all', array('conditions' => array('InstDetail.inst_id' => $idd)));

           // $install = array('reg' => $reg) + $install;
            //pr($install); die;

			$conns = ConnectionManager::get('default');
			$query1 ="update tbl_student set status='1' where s_id=" . $gtt;
			$result=$conns->execute($query1);


/* Update Query For cms_student_course Query */

$conns = ConnectionManager::get('default');
$query1s ="update cms_student_courses set status='1' where s_id=$gtt and course_id=$course_id";
$conns->execute($query1s);

$reg_status = "n";
 

            foreach ($install as $in_key => $in_value) {
                //pr($install)

                $studentinstall = $this->StudentInstallment->newEntity();
           
                $tn = array();
                if ($in_value == '' || $in_value == 0) {
                    $key_install =& $install;

                    unset($key_install[$in_key]);
                    //pr($install);die;

                    continue;
                }
                //pr($install); die;

               

                    if ($course_det['ftype'] == 'm') {
                        $f_date = date('Y-m-d');
                        if ($dy >= 15) {
                            $date3min = strtotime(date("Y-m-30", strtotime($f_date)) . "+" . $mnth . " Month");
                            $date = date('Y-m-d', $date3min);
                        } else {
                            $date3min = strtotime(date("Y-m-15", strtotime($f_date)) . "+" . $mnth . " Month");
                            $date = date('Y-m-d', $date3min);
                        }

                        $mn++;
                        $mnth++;

                    } elseif ($course_det['ftype'] == 'h' && in_array($course_det['id'], $degrecourseList)) {
                        $sessionDetail = explode('-', $this->request->data['session']);
                        $year = $sessionDetail[1];
                        $session = $sessionDetail[0];
                        if ($session == 'june') {
                            $f_date = $year . '-05-30';
                        } else {
                            $f_date = $year . '-11-30';
                        }
                        $date3min = strtotime(date("Y-m-d", strtotime($f_date)) . "+" . $monthh . " Month");
                        $date = date('Y-m-d', $date3min);
                        $monthh += 6;
                    } elseif ($course_det['ftype'] == 'y' && in_array($course_det['id'], $degrecourseList)) {
                        $sessionDetail = explode('-', $this->request->data['session']);
                        $year = $sessionDetail[1];
                        $session = $sessionDetail[0];
                        if ($session == 'june') {
                            $f_date = $year . '-05-30';
                        } else {
                            $f_date = $year . '-11-30';
                        }
                        $date3min = strtotime(date("Y-m-d", strtotime($f_date)) . "+" . $monthh . " Month");
                        $date = date('Y-m-d', $date3min);
                        $monthh += 12;
                    } elseif ($course_det['ftype'] == 'h') {
                        $f_date = date('Y-m-d');
                        $date3min = strtotime(date("Y-m-d", strtotime($f_date)) . "+" . $monthh . " Month");
                        $date = date('Y-m-d', $date3min);
                        $monthh += 6;
                    } else {

                        $f_date = date('Y-m-d');

                        $date3min = strtotime(date("Y-m-d", strtotime($f_date)) . "+" . $monthy . " Month");
                        $date = date('Y-m-d', $date3min);
                        $monthy += 12;
                    }

                    //pr($date); die;

                    $tn['s_id'] = $gtt;

                    $tn['course_id'] = $this->request->data['course_id'];
                    $tn['inst_id'] = $ins_id;
                    $tn['pro_date'] = $date;
                    $tn['pay_date'] = "0000-00-00";
                    $tn['fine'] = '0';
                    $tn['is_drop'] = '0';
                    $tn['mode'] = '0';
                    $tn['chequedetail'] = '';
                    $tn['bank'] = '';
                    if ($branch_detail['gstapli'] == '1') {

                        if ($branch_detail['gstinclu'] == '1') {
                            $x1 = $wt / 100;
                            $y1 = 1 + $x1;
                            $z1 = $install / $y1;
                            $sgs = ($z1 * $sgst) / 100;
                            $cgs = ($z1 * $cgst) / 100;

                            $tot = $z1;
                            $tn['amount'] = $in_value;
                            $tn['cgst_percent'] = $cgst;
                            $tn['sgst_percent'] = $sgst;
                            $tn['cgst_amount'] = $cgs;
                            $tn['sgst_amount'] = $sgs;
                            $tn['gst_num'] = $branch_detail['gsnum'];
                            $tn['gst_inclu'] = $branch_detail['gstinclu'];

                        } else {
                            $z1 = $install;
                            $sgs = ($z1 * $sgst) / 100;
                            $cgs = ($z1 * $cgst) / 100;
                            $tot = $z1;
                            $tn['amount'] = $in_value;
                            $tn['cgst_percent'] = $cgst;
                            $tn['sgst_percent'] = $sgst;
                            $tn['cgst_amount'] = $cgs;
                            $tn['sgst_amount'] = $sgs;
                            $tn['gst_num'] = $branch_detail['gsnum'];
                            $tn['gst_inclu'] = $branch_detail['gstinclu'];

                        }
                    } elseif ($branch_detail['gstapli'] == '2') {

                        if ($branch_detail['gstinclu'] == '1') {
                            $x1 = $igst / 100;
                            $y1 = 1 + $x1;
                            $z1 = $install / $y1;
                            $igs = ($z1 * $igst) / 100;
                            $tot = $z1;
                            $tn['amount'] = $in_value;
                            $tn['igst_percent'] = $igst;
                            $tn['igst_amount'] = $igs;
                            $tn['gst_num'] = $branch_detail['gsnum'];
                            $tn['gst_inclu'] = $branch_detail['gstinclu'];

                        } else {
                            $z1 = $install;
                            $igs = ($z1 * $igst) / 100;
                            $tot = $z1;
                            $tn['amount'] = $in_value;
                            $tn['igst_percent'] = $igst;
                            $tn['igst_amount'] = $igs;
                            $tn['gst_num'] = $branch_detail['gsnum'];
                            $tn['gst_inclu'] = $branch_detail['gstinclu'];

                        }
                    } else {
                        $sgs = '0';
                        $cgs = '0';
                        $igs = '0';
                        $tot = $install;
                        $tn['amount'] = $in_value;
                    }

                    $tn['branch_id'] = $brh_id;

                    $tn['branch_name'] = $branch_detail['name'];
                    $tn['branch_mobile'] = $branch_detail['phone'];
                    $tn['address'] = $branch_detail['address'];
                    $tn['branch_head'] = $bran_head;
                   
                    $tn['pay_date'] = "0000-00-00";
//pr($tn);die;

                    $amt = $in_value;

                    if ($amt != 0) {

                      $transtn = $this->StudentInstallment->newEntity();
                      $dasdasd = $this->StudentInstallment->patchEntity($transtn, $tn);
  
                      $re= $this->StudentInstallment->save($dasdasd);

                       
                    } else {
                        continue;
                    }

                
            }
  // $ghj=$this->StudentInstallment->getInsertID();
 // window.open('" . ADMIN_URL . "fee/mpdf/" . $gtt . "/" . $receipt . "/" . $course_id . "');
            if ($reg_status == "y") {
   echo ("<script>

   window.location.href='" . ADMIN_URL . "fees/index/" . $gtt . "';
</script>"); 
            } else {

           
              $this->Flash->success(__('Student Information has been Updated successfully.'));
              return $this->redirect(['action' => 'index/'. $gtt]);

                
            }

        }

      }

  }


  
  public function editcourse($id=null,$stucourseid=null,$crseid=null)
  {
      $this->loadModel('StudentCourse');
      $this->loadModel('StudentInstallment');
      $this->loadModel('Mode');
      $this->loadModel('Course');
      $this->loadModel('Students');
      $this->loadModel('Branch');
      $this->viewBuilder()->layout('admin');
      $mode = $this->Mode->find('list', [
        'keyField' => 'id',
        'valueField' => 'name',
      ])->order(['name' => 'ASC'])->toArray();
      
      $this->set(compact('mode'));
 
      $ac1 = $this->Course->find('list', [
        'keyField' => 'id',
        'valueField' => 'sname',
      ])->where(['status' => 0])->order(['sname' => 'ASC'])->toArray();
      
      $degrecourseList = $this->Course->find('list', [
        'keyField' => 'id',
        'valueField' => 'id',
      ])->where(['status' => 0, 'course_group' => 'Degree Courses'])->order(['sname' => 'ASC'])->toArray();

      $this->set('degrecourseList', $degrecourseList); //pr($ac1); die;
      $this->set('categ1', $ac1); //pr($ac1); die;
      $this->set('crseid', $crseid); //pr($ac1); die;

  $studentInstallment = $this->StudentInstallment->find('all')->where(['StudentInstallment.s_id' =>$id,'StudentInstallment.course_id' =>$crseid])->order(['sid_id' => 'ASC'])->toarray();

          $this->set('studentinstallment', $studentInstallment);
      
      $br1 = $this->Branch->find('list', [
    'keyField' => 'id',
    'valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();

      $this->set('brn', $br1);

      $userbranch =$this->request->session()->read("Auth.User.branch");
      if ($this->request->session()->read('Auth.User.role_id') != 1) {
         
          $br_det = $this->Branch->find('all')->where(['Branch.id' =>$userbranch])->order(['id' => 'DESC'])->first();

          $this->set('br_det', $br_det);
      }

      $st = $this->Students->find('all')->where(['Students.branch_id' =>$userbranch])->order(['s_id' => 'DESC'])->first();
      

      

      if (!empty($st)) {
          $enrol = substr($st['s_enroll'], 2, 100);

      } else {
          $enrol = 1000;

      }

      $this->set('rol', $enrol);

   
      if($id){
       
        $newresponse = $this->Students->get($id);
      }else{
        $newresponse = $this->Students->newEntity();
      
      }
      $this->set('newresponse', $newresponse);
      if ($this->request->is(['post', 'put'])) {
        
    
        $this->loadModel('Course');
        $this->loadModel('InstDetail');
        $this->loadModel('Setting');
        $this->loadModel('Cash');
        $this->loadModel('Users');
        $this->loadModel('Transaction');
   

   
    $branch_detail = $this->Branch->find('all')->where(['id' =>$this->request->data['branch']])->first();
    
    
    $gst = $this->Setting->find('all')->select(['sgst', 'igst', 'cgst'])->first();

    
    $this->request->data['branch']=$branch_detail['id'];
    $branchhead = $this->Users->find('all')->select(['enrollcode','branchhead','branch'])->where(['branch' =>$this->request->data['branch']])->first();

   
        // pr($branch_detail); die;
        $bran_head = $branchhead['branchhead'];
        $sgst = $gst['sgst'];
        $igst = $gst['igst'];
        $cgst = $gst['cgst'];
        $wt = $cgst + $sgst;

        $add = $this->request->data['add_date'];
        $bd = $this->request->data['b_date'];
        $this->request->data['s_pass'] = (new FallbackPasswordHasher)->hash($this->request->data['s_enroll']);
        $this->request->data['add_date'] = date('Y-m-d');
        $this->request->data['b_date'] = date('Y-m-d', strtotime($bd));
        $this->request->data['cid'] = 0;
      

  $this->request->data['b_id']=0;
  $this->request->data['reg_fees']=0;
  $this->request->data['admin_type']=0;
  $this->request->data['admin_id']=0;
  $this->request->data['drop_out']=0;
  $this->request->data['status']=0;
  $this->request->data['discount']=0;
  $this->request->data['drop_reasn']='';
  $this->request->data['is_transfer']=0;
  $this->request->data['drop_date']="0000-00-00";
  $this->request->data['transfer_date']="0000-00-00";
  $this->request->data['image']="";
  $this->request->data['transfer_reason']="";
  
  $this->request->data['pdf']="";
  $this->request->data['pdfcreated']="";
  if($this->request->data['type'] !=11){

    $this->request->data['rname']=""; 
    $this->request->data['rmobile']=""; 
    $this->request->data['ramount']=""; 
  }

  $this->request->data['branch_id']=$this->request->data['branch'];

  $useida = $this->Students->find('all')->where(['Students.s_id'=>$this->request->data['s_id']])->first();
	$gtt = $useida['s_id'];
	$branch_id = $useida['branch_id'];
$vcrt=0;


$studentcourse=$this->StudentCourse->find('all')->where(['s_id' =>$gtt,'id'=>$stucourseid])->order(['StudentCourse.s_id'=>'DESC'])->first();

$course_id=$studentcourse['course_id'];
$sc_id =$studentcourse['id'];
$inst_ids =$studentcourse['inst_id'];
if($course_id!=$this->request->data['course_id']){
  $vcrt=0;
  $articles = $this->InstDetail; 
  $course_fee = $this->InstDetail->find('all')->select(['amt' =>$articles->find('all')->func()->sum('amt')])->where(['inst_id' =>$this->request->data['inst_id']])->order(['id_id' => 'DESC'])->first();
	$conns = ConnectionManager::get('default');
	$query1 ='update cms_student_courses set is_change="Y" where id=' . $sc_id;
	$conns->execute($query1);
  $ins_id = $this->request->data['inst_id'];
}else{

  $articles = $this->InstDetail; 
  $course_fee = $this->InstDetail->find('all')->select(['amt' =>$articles->find('all')->func()->sum('amt')])->where(['inst_id' =>$inst_ids])->order(['id_id' => 'DESC'])->first();
  $vcrt=$sc_id;
  $ins_id = $inst_ids;
  
}


$conns = ConnectionManager::get('default');
$query12 ='delete from tbl_student_installment where s_id=' . $gtt . ' and course_id=' . $this->request->data['course_id'] . ' and recipt_no =0';
$conns->execute($query12);



      

            // echo $gtt;die;
            $this->request->data['s_id'] = $gtt;
            $course_id = $this->request->data['course_id'];
           
            $brh_id = $branch_id;
            $ad_date = $this->request->data['add_date'];

            if($vcrt==0){
            $studentcourse = $this->StudentCourse->newEntity();
            }else{
            $studentcourse = $this->StudentCourse->get($vcrt);
            }
            $lss['s_id'] = $gtt;
            $lss['cid'] = 0;
            $lss['b_id'] = 0;
            $lss['course_id'] = $course_id;
            $lss['inst_id'] = $ins_id;
            $lss['branch_id'] = $branch_id;
            $lss['fee'] = $course_fee['amt'];
            $lss['drop_out'] = 0;
            $lss['discount'] = 0;
            $lss['reg_fees'] = 0;
            $lss['drop_reasn'] = "";
            $lss['is_transfer'] = 0;
            $lss['drop_date'] = "0000-00-00";
            $lss['add_date'] = $ad_date;
            $lss['transfer_date'] = "0000-00-00";
            $lss['transfer_reason'] = "";
            $lss['completedate'] = "0000-00-00";
            $lss['certifiacte'] = 0;
            $lss['status'] = 0;
            if (in_array($course_id, $degrecourseList)) {
                $sessionDetail = explode('-', $this->request->data['session']);
                $year = $sessionDetail[1];
                $session = $sessionDetail[0];
                $lss['session'] = $session;
            }else{
                $lss['session'] = "";


            }

     
          //pr($this->request->data); die;
            //pr($course_id); die;
            $savepackCourse = $this->StudentCourse->patchEntity($studentcourse, $lss);

            $this->StudentCourse->save($savepackCourse);
            //    pr($this->request->data); die;
            $install = $this->request->data['installment'];
            $yr = date('Y');
            $mn = date('m');
            $dy = date('d');
            $mnth = 0;


            $course_det = $this->Course->find('all')->where(['id' =>$course_id])->first();

        
            $monthh = 0;
            $monthy = 0;
           // $reg = array_pop($install);

            //$gt = $this->InstDetail->find('all', array('conditions' => array('InstDetail.inst_id' => $idd)));

           // $install = array('reg' => $reg) + $install;
            //pr($install); die;

			$conns = ConnectionManager::get('default');
			$query1 ="update tbl_student set status='1' where s_id=" . $gtt;
			$result=$conns->execute($query1);


/* Update Query For cms_student_course Query */

$conns = ConnectionManager::get('default');
$query1s ="update cms_student_courses set status='1' where s_id=$gtt and course_id=$course_id";
$conns->execute($query1s);

$reg_status = "n";
 

            foreach ($install as $in_key => $in_value) {
                //pr($install)

                $studentinstall = $this->StudentInstallment->newEntity();
           
                $tn = array();
                if ($in_value == '' || $in_value == 0) {
                    $key_install =& $install;

                    unset($key_install[$in_key]);
                    //pr($install);die;

                    continue;
                }
                //pr($install); die;

               

                    if ($course_det['ftype'] == 'm') {
                        $f_date = date('Y-m-d');
                        if ($dy >= 15) {
                            $date3min = strtotime(date("Y-m-30", strtotime($f_date)) . "+" . $mnth . " Month");
                            $date = date('Y-m-d', $date3min);
                        } else {
                            $date3min = strtotime(date("Y-m-15", strtotime($f_date)) . "+" . $mnth . " Month");
                            $date = date('Y-m-d', $date3min);
                        }

                        $mn++;
                        $mnth++;

                    } elseif ($course_det['ftype'] == 'h' && in_array($course_det['id'], $degrecourseList)) {
                        $sessionDetail = explode('-', $this->request->data['session']);
                        $year = $sessionDetail[1];
                        $session = $sessionDetail[0];
                        if ($session == 'june') {
                            $f_date = $year . '-05-30';
                        } else {
                            $f_date = $year . '-11-30';
                        }
                        $date3min = strtotime(date("Y-m-d", strtotime($f_date)) . "+" . $monthh . " Month");
                        $date = date('Y-m-d', $date3min);
                        $monthh += 6;
                    } elseif ($course_det['ftype'] == 'y' && in_array($course_det['id'], $degrecourseList)) {
                        $sessionDetail = explode('-', $this->request->data['session']);
                        $year = $sessionDetail[1];
                        $session = $sessionDetail[0];
                        if ($session == 'june') {
                            $f_date = $year . '-05-30';
                        } else {
                            $f_date = $year . '-11-30';
                        }
                        $date3min = strtotime(date("Y-m-d", strtotime($f_date)) . "+" . $monthh . " Month");
                        $date = date('Y-m-d', $date3min);
                        $monthh += 12;
                    } elseif ($course_det['ftype'] == 'h') {
                        $f_date = date('Y-m-d');
                        $date3min = strtotime(date("Y-m-d", strtotime($f_date)) . "+" . $monthh . " Month");
                        $date = date('Y-m-d', $date3min);
                        $monthh += 6;
                    } else {

                        $f_date = date('Y-m-d');

                        $date3min = strtotime(date("Y-m-d", strtotime($f_date)) . "+" . $monthy . " Month");
                        $date = date('Y-m-d', $date3min);
                        $monthy += 12;
                    }

                    //pr($date); die;

                    $tn['s_id'] = $gtt;

                    $tn['course_id'] = $this->request->data['course_id'];
                    $tn['inst_id'] = $ins_id;
                    $tn['pro_date'] = $date;
                    $tn['pay_date'] = "0000-00-00";
                    $tn['fine'] = '0';
                    $tn['is_drop'] = '0';
                    $tn['mode'] = '0';
                    $tn['chequedetail'] = '';
                    $tn['bank'] = '';
                    if ($branch_detail['gstapli'] == '1') {

                        if ($branch_detail['gstinclu'] == '1') {
                            $x1 = $wt / 100;
                            $y1 = 1 + $x1;
                            $z1 = $install / $y1;
                            $sgs = ($z1 * $sgst) / 100;
                            $cgs = ($z1 * $cgst) / 100;

                            $tot = $z1;
                            $tn['amount'] = $in_value;
                            $tn['cgst_percent'] = $cgst;
                            $tn['sgst_percent'] = $sgst;
                            $tn['cgst_amount'] = $cgs;
                            $tn['sgst_amount'] = $sgs;
                            $tn['gst_num'] = $branch_detail['gsnum'];
                            $tn['gst_inclu'] = $branch_detail['gstinclu'];

                        } else {
                            $z1 = $install;
                            $sgs = ($z1 * $sgst) / 100;
                            $cgs = ($z1 * $cgst) / 100;
                            $tot = $z1;
                            $tn['amount'] = $in_value;
                            $tn['cgst_percent'] = $cgst;
                            $tn['sgst_percent'] = $sgst;
                            $tn['cgst_amount'] = $cgs;
                            $tn['sgst_amount'] = $sgs;
                            $tn['gst_num'] = $branch_detail['gsnum'];
                            $tn['gst_inclu'] = $branch_detail['gstinclu'];

                        }
                    } elseif ($branch_detail['gstapli'] == '2') {

                        if ($branch_detail['gstinclu'] == '1') {
                            $x1 = $igst / 100;
                            $y1 = 1 + $x1;
                            $z1 = $install / $y1;
                            $igs = ($z1 * $igst) / 100;
                            $tot = $z1;
                            $tn['amount'] = $in_value;
                            $tn['igst_percent'] = $igst;
                            $tn['igst_amount'] = $igs;
                            $tn['gst_num'] = $branch_detail['gsnum'];
                            $tn['gst_inclu'] = $branch_detail['gstinclu'];

                        } else {
                            $z1 = $install;
                            $igs = ($z1 * $igst) / 100;
                            $tot = $z1;
                            $tn['amount'] = $in_value;
                            $tn['igst_percent'] = $igst;
                            $tn['igst_amount'] = $igs;
                            $tn['gst_num'] = $branch_detail['gsnum'];
                            $tn['gst_inclu'] = $branch_detail['gstinclu'];

                        }
                    } else {
                        $sgs = '0';
                        $cgs = '0';
                        $igs = '0';
                        $tot = $install;
                        $tn['amount'] = $in_value;
                    }

                    $tn['branch_id'] = $branch_id;

                    $tn['branch_name'] = $branch_detail['name'];
                    $tn['branch_mobile'] = $branch_detail['phone'];
                    $tn['address'] = $branch_detail['address'];
                    $tn['branch_head'] = $bran_head;
                   
                    $tn['pay_date'] = "0000-00-00";
//pr($tn);die;

                    $amt = $in_value;

                    if ($amt != 0) {

                      $transtn = $this->StudentInstallment->newEntity();
                      $dasdasd = $this->StudentInstallment->patchEntity($transtn, $tn);
  
                      $re= $this->StudentInstallment->save($dasdasd);

                       
                    } else {
                        continue;
                    }

                
            }
  // $ghj=$this->StudentInstallment->getInsertID();
 // window.open('" . ADMIN_URL . "fee/mpdf/" . $gtt . "/" . $receipt . "/" . $course_id . "');
            if ($reg_status == "y") {
   echo ("<script>

   window.location.href='" . ADMIN_URL . "fees/index/" . $gtt . "';
</script>"); 
            } else {


              $this->Flash->success(__('Student Information has been Updated successfully.'));
            
              return $this->redirect(['action' => 'index/'. $gtt]);

                
            }

        

      }

  }


	
	public function index($id = '', $rcp = '', $branchead = '', $stid = ''){ 

		$this->viewBuilder()->layout('admin');

 //echo "test"; die;
 $this->loadModel('Students');
 $this->loadModel('StudentCourse');
 $this->loadModel('Reportdiscount');
 $this->loadModel('Discount');
 $this->loadModel('StudentInstallment');
 $this->loadModel('Cash');
 $this->loadModel('Certificate');
 $this->loadModel('Refund');
 $this->loadModel('StudentBatch');
 $this->loadModel('Batch');
 $this->loadModel('Branch');
 $this->loadModel('Course');



 $course = $this->Course->find('list', [
  'keyField' => 'id',
  'valueField' => 'sname',
])->order(['sname' => 'ASC'])->toArray();
 $this->set('course', $course);

 $btch = $this->Batch->find('list', [
  'keyField' => 'b_id',
  'valueField' => 'batch_code',
])->where(['status' => 'N', 'is_new' => 'Y'])->order(['b_id' => 'DESC'])->toArray();
$this->set('btch', $btch);


$brn = $this->Branch->find('list', [
  'keyField' => 'id',
  'valueField' => 'name',
])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();
$this->set('brn', $brn);



 $user = $this->request->session()->read('Auth.User.id');
 $s_id_arr = array();

 if ($this->request->session()->read("Auth.User.role_id") == 1) {

	$s_id = $this->StudentCourse->find('all')->where(['drop_out' => 0, 'status !=' => '2'])->order(['StudentCourse.id' => 'DESC'])->toarray();

     foreach ($s_id as $id) {
         $s_id_arr[] = $id['s_id'];
     }
     $s_id_arr = array_unique($s_id_arr);

     $studentsall = $this->StudentCourse->find('all')->contain(['Students','Course','Branch'])->where(['StudentCourse.drop_out' => 0, 'StudentCourse.s_id IN' =>$s_id_arr])->group(['StudentCourse.s_id'])->order(['Students.s_name' => 'ASC'])->limit(20);

    
 }else{

  $s_id = $this->StudentCourse->find('all')->where(['drop_out' => 0, 'status !=' => '2','branch_id'=>$this->request->session()->read("Auth.User.branch")])->order(['StudentCourse.id' => 'DESC'])->toarray();

     foreach ($s_id as $id) {
         $s_id_arr[] = $id['s_id'];
     }
     $s_id_arr = array_unique($s_id_arr);

     $studentsall = $this->StudentCourse->find('all')->contain(['Students','Course','Branch'])->where(['StudentCourse.drop_out' => 0, 'StudentCourse.s_id IN' =>$s_id_arr])->group(['StudentCourse.s_id'])->order(['Students.s_name' => 'ASC'])->limit(20);

 }


 $destination=$this->paginate($studentsall)->toarray();
 $this->set('destination', $destination);
 
 $predate = date('Y-m-d');
 $batch_code = array();
 foreach ($destination as $dest_val){

  $batch_det = $this->StudentBatch->find('all')->contain('Batch')->where(['StudentBatch.s_id' =>$dest_val['student']['s_id']])->order(['id' => 'DESC'])->toarray();

    
     $bt = array();
     if (!empty($batch_det)) {
         foreach ($batch_det as $det_val) {
           if($det_val['batch']['status']=='N'){
             $bt[] = $det_val['batch']['batch_code'];
         }
        }
     }
     $batch_code[$dest_val['student']['s_id']] = implode(',', $bt);

 }


 $this->set(compact('batch_code'));





	}


public function useradd()
{
  $this->viewBuilder()->layout('admin');
  $this->loadModel('Users');  
  $this->loadModel('Stores');
  $store = $this->Stores->find('list')->where(['Stores.status' => '1'])->order(['Stores.id'=>'DESC'])->toarray();
  $this->set('store',$store); 

  $newpack = $this->Users->newEntity();
  if ($this->request->is(['post', 'put'])) { 
  // pr($this->request->data); die;  
   if ($this->request->data['image']['name'] != '')
   {
	 $k = $this->request->data['image'];
	 $galls = $this->move_images($k);
	 $this->FcCreateThumbnail1("compress", "images/user_images", $galls[0], $galls[0], "100", "100");
	 $this->request->data['image'] = $galls[0];
	 unlink('compress/' . $galls[0]);
   }    
   $this->request->data['role_id']='2';
   $this->request->data['password']=(new DefaultPasswordHasher)->hash($this->request->data['password']);

    $savepack = $this->Users->patchEntity($newpack, $this->request->data);
    $results=$this->Users->save($savepack);
    if ($results){
      $this->Flash->success(__('User has been saved.'));
      return $this->redirect(['action' => 'index']);  
    }else{
      $this->Flash->error(__('User not saved.'));
      return $this->redirect(['action' => 'index']);  
    }
  }
}





public function edit($id=null)
{
  $this->viewBuilder()->layout('admin');
  $this->loadModel('Students');  
 
  $users = $this->Students->get($id);
  $this->set('users',$users);
  if ($this->request->is(['post', 'put'])) { 
  // pr($this->request->data); die;  
   if ($this->request->data['image']['name'] != '')
   {
	 $k = $this->request->data['image'];
	 $galls = $this->move_images($k);
	 $this->FcCreateThumbnail1("compress", "images/user_images", $galls[0], $galls[0], "100", "100");
	 $this->request->data['image'] = $galls[0];
	 unlink('compress/' . $galls[0]);
   }else{
	$this->request->data['image'] = $users['image'];
   }  
   $this->request->data['role_id']='2';
   $this->request->data['password']=(new DefaultPasswordHasher)->hash($this->request->data['new_password']);

    $savepack = $this->Users->patchEntity($users, $this->request->data);
    $results=$this->Users->save($savepack);
    if ($results){
      $this->Flash->success(__('User has been updated.'));
      return $this->redirect(['action' => 'index']);  
    }else{
      $this->Flash->error(__('User not saved.'));
      return $this->redirect(['action' => 'index']);  
    }
  }
}

 public function pdfreportfeeplan($sid=null, $cid=null, $insid=null)
    {
        //echo "hello"; die;
        $this->loadModel('StudentCourse');
        $this->loadModel('Discount');
        $this->loadModel('StudentInstallment');
        $this->loadModel('Cash');
        $this->loadModel('Course');
        $this->loadModel('Students');
        $this->set('cid', $cid);
        $this->set('sid', $sid);
        $this->set('insid', $insid);

        //var_dump($sid); die;

        
        $studetail = $this->Students->find('all')->where(['Students.s_id' =>$sid])->order(['s_id' => 'DESC'])->first();

        $hjk = $this->Course->find('all')->where(['Course.id' =>$cid])->order(['id' => 'DESC'])->first();
      
        //dd($hjk); die;
        $coursename = $hjk['sname'];

        $totafees = $this->StudentCourse->find('all')->select(['fee','discount'])->where(['StudentCourse.s_id' => $sid, 'StudentCourse.course_id' => $cid])->first();

        $installment = $this->StudentInstallment->find('all')->where(['StudentInstallment.s_id' => $sid, 'StudentInstallment.course_id' => $cid, 'StudentInstallment.is_reg' => 'N'])->order(['pro_date' => 'ASC'])->toarray();
        $enrol = $studetail['s_enroll'];
        $discont_det = $this->Discount->find('all')->where(['Discount.enrollment LIKE' => $enrol . '%', 'Discount.course_id' => $cid, 'Discount.status' => 'A'])->first();

        //print_r($discont); die;
        if (!empty($discont_det)) {
            if ($discont_det['dtype'] == 2) {
                $discomt = ($totafees['fee'] * $discont_det['discount']) / 100;
            } else {
                $discomt = $discont_det['discount'];
            }
        } else {
            $discomt = 0;
        }

        $netfee = $totafees['fee'] - $discomt;
        $enroll = $studetail['s_enroll'];
        $mobile = $studetail['s_contact'];
        $name = $studetail['s_name'];
        $totalamount = $totafees['fee'];
        $this->set(compact('discomt', 'mobile', 'name', 'totalamount', 'netfee', 'coursename', 'installment', 'enroll'));


        $payment = $this->StudentInstallment->find('all')->where(['StudentInstallment.s_id' => $sid, 'StudentInstallment.course_id' => $cid, 'StudentInstallment.recipt_no !=' => 0, 'StudentInstallment.amount !=' => '250'])->toarray();

  
        //pr($payment); die;
        $pay_his = array();
        if (!empty($payment)) {
            foreach ($payment as $pay_value) {
                $paid_data = array();
                $paid_data['sid_id'] = $pay_value['sid_id'];

                $paid_data['s_id'] = $pay_value['s_id'];
                $paid_data['amount'] = $pay_value['amount'];
                $paid_data['receipt_no'] = $pay_value['recipt_no'];
                $paid_data['course_id'] = $pay_value['course_id'];
                $paid_data['pay_date'] = $pay_value['pay_date'];
                $pay_his[] = $paid_data;
            }
        }


        
        $new_payment = $this->Cash->find('all')->where(['Cash.s_id' => $sid, 'Cash.cid' => $cid, 'Cash.status' => 'Y', 'Cash.receipt_no !=' => 0])->toarray();

        //pr($new_payment); die;
        if (!empty($new_payment)) {
            $paid_data = array();

            foreach ($new_payment as $npay_value) {
                $paid_data['is_reg'] = $npay_value['is_reg'];
                $paid_data['s_id'] = $npay_value['s_id'];
                $paid_data['amount'] = $npay_value['amount'];
                $paid_data['fine'] = $npay_value['fine'];
                $paid_data['receipt_no'] = $npay_value['receipt_no'];
                $paid_data['course_id'] = $npay_value['cid'];
                $paid_data['pay_date'] = $npay_value['pay_date'];
                $pay_his[] = $paid_data;
            }
        }


        $this->set('pay_his', $pay_his);

   

    }

 public function pdffullreport($id,$stuCourseId,$cId)
    {

$this->loadModel('StudentCourse');
      $this->loadModel('StudentBatch');
      $this->loadModel('ExamSetup');
      $this->loadModel('Result');
  $this->loadModel('StudentInstallment');
  $this->loadModel('Cash');
  $this->loadModel('Students');
  $this->loadModel('Reportdiscount');
  $this->loadModel('Refund');
  $this->loadModel('Course');
  $this->loadModel('Discount');

  $studentsall = $this->StudentInstallment->find('all')->where(['StudentInstallment.s_id' => $id])->order(['StudentInstallment.course_id' => 'Asc', 'StudentInstallment.pro_date' => 'ASC']);

  $destination=$this->paginate($studentsall)->toarray();

  $this->set('destination', $destination);

  $stu_det = $this->Students->find('all')->where(['Students.s_id' =>$id])->order(['s_id' => 'DESC'])->first();

  $this->set(compact('stu_det'));

	$branch = $this->request->session()->read('Auth.User.branch');
		$role_id = $this->request->session()->read('Auth.User.role_id');

  $this->set('role_id', $role_id);
  $branchhead = $this->request->session()->read('Auth.User.branchhead');

  $refund_det = $this->Refund->find('all')->where(['Refund.s_id' =>$id])->order(['s_id' => 'DESC'])->toarray();

  $this->set('refund_det', $refund_det);

  $stu_course_det = $this->StudentCourse->find('all')->where(['StudentCourse.s_id' => $id,'StudentCourse.id'=>$stuCourseId])->order(['s_id' => 'DESC'])->first();

   
  $stu_branch = $stu_course_det['branch'];
  $this->set(compact('stu_branch'));
  $enrol = $stu_det['s_enroll'];

  $stu_course = array();
  $course_ids = array();

    //dd($total_fees); die;
foreach ($stu_course_det as $det) {
      $course=[];

      
  $course_det = $this->Course->find('all')->where(['Course.id' =>$det['course_id']])->first();

      
              $courseIds=[];
              if(!empty($course_det['qc_id'])){
              $courseIds=explode(',',$course_det['qc_id']);
              }
              
              $payment = $this->StudentInstallment->find('all')->where(['StudentInstallment.s_id' => $id, 'StudentInstallment.recipt_no !=' => 0,'StudentInstallment.course_id'=>$det['course_id']])->order(['StudentInstallment.pay_date' => 'ASC'])->toarray();


  //pr($this->paginate('StudentInstallment')); die;
  $pay_his = array();
  if (!empty($payment)) {
    foreach ($payment as $pay_value) {
      $paid_data = array();
      $paid_data['sid_id'] = $pay_value['sid_id'];

      $paid_data['s_id'] = $pay_value['s_id'];
      $paid_data['fine'] = $pay_value['fine'];
      $paid_data['amount'] = $pay_value['amount'];
      $paid_data['receipt_no'] = $pay_value['recipt_no'];
      $paid_data['course_id'] = $pay_value['course_id'];
      $paid_data['pay_date'] = $pay_value['pay_date'];
      $course['pay_his'][] = $paid_data;
    }
  }


  $new_payment = $this->Cash->find('all')->where(['Cash.s_id' => $id,'Cash.cid' => $det['course_id'], 'Cash.status' => 'Y','Cash.receipt_no !='=>0])->order(['Cash.pay_date' => 'ASC'])->toarray();


  if (!empty($new_payment)) {
    $paid_data = array();

    foreach ($new_payment as $npay_value) {
      $paid_data['is_reg'] = $npay_value['is_reg'];
      $paid_data['s_id'] = $npay_value['s_id'];
      $paid_data['amount'] = $npay_value['amount'];
      $paid_data['fine'] = $npay_value['fine'];
      $paid_data['receipt_no'] = $npay_value['receipt_no'];
      $paid_data['course_id'] = $npay_value['cid'];
      $paid_data['pay_date'] = $npay_value['pay_date'];
      $course['pay_his'][] = $paid_data;
    }
  }
    //dd($total_fees); die;

    // if ($det['is_change'] == 'N' && $det['drop_out'] != 1) {
    if ($det['is_change'] == 'N') {
      $course_ids[] = $det['course_id'];
      $course['fee_paid'] = 0;
      $course['course_id'] = $det['course_id'];

  

  $course_name = $this->Course->find('all')->where(['Course.id' =>$det['course_id']])->first();

  $discount = $this->Discount->find('all')->where(['Discount.enrollment LIKE' => $enrol . '%', 'Discount.course_id' =>$det['course_id'], 'Discount.status' => 'A'])->first();

      //print_r($enrol);   die;
      //$discount=$this->Reportdiscount->find('first',array('conditions'=>array('Reportdiscount.course_id'=>$det['course_id'],'Reportdiscount.s_id'=>$id)));
      //print_r($discount); die;
      if (!empty($discount)) {

        if ($discount['dtype'] == 2) {
          $course['dis'] = ($det['fee'] * $discount['discount']) / 100;
        } else {
          $course['dis'] = $discount['discount'];
        }
      } else {
        $course['dis'] = 0;
      }

      $course['name'] = $course_name['sname'];

      if (!empty($course['pay_his'])) {
        foreach ($course['pay_his'] as $pay) {
          if ($pay['course_id'] == $det['course_id'] && $pay['is_reg'] != 1 && $pay['amount'] !== "250") {
            $course['fee_paid'] += $pay['amount'];
          }
        }
      }

      // $course['fees']=$det['fee'];
      $course['fees'] = $det['fee'];
              $course['drop']=$det['drop_out'];
              $stu_batch=[];
              if(!empty($courseIds)){
                $stu_batch = $this->StudentBatch->find('all')->contain('Batch')->where(['StudentBatch.s_id' =>$id,'Batch.q_cat'=>$courseIds])->order(['id' => 'DESC'])->toarray();
 }
              $course['batchDet']=$stu_batch;
              $exam_result=[];
              foreach($stu_batch as $batch){

                $exam_setup = $this->ExamSetup->find('all')->contain('Batch')->where(['ExamSetup.b_id'=>$batch['b_id'],'FIND_IN_SET  (\''. $id .'\',ExamSetup.students)'])->toarray();

             if(!empty($exam_setup)){ 
          foreach($exam_setup as $setup){ 
               $data['e_name']=$setup['e_name'];
               $data['add_date']=$setup['add_date'];
               $data['batch_code']=$setup['batch']['batch_code'];
               if(!empty($setup['batch']['Subjects'])){
            $data['subject']=$setup['batch']['Subjects']['sub_name'];
             }else{
            $data['subject']=$setup['Batch']['QuestionCategory']['qc_name'];
             }
             $stu_result = $this->Result->find('first', array('conditions' => array('Result.s_id' => $id,'Result.e_id'=>$setup['ExamSetup']['e_id']),'recursive'=>-1));
             
             if(!empty($stu_result)){
              $data['marks']=$stu_result['Result']['marks_obtained'].'/'.$stu_result['Result']['total_marks'];
              $data['result']=$stu_result['Result']['result'];
             } else{
              $data['marks']='-';   
              $data['result']='Absent';
          }
          
                 $exam_result[]=$data;

          }
            }       
          }
              $course['exam_result']=$exam_result;
      $stu_courses[] = $course;
    }
  }
  $this->set('course_ids', $course_ids);
  $this->set('stu_courses', $stu_courses);
  $this->set('branch', $branch);
      $stu_batch = $this->StudentBatch->find('all', array('conditions' => array('StudentBatch.s_id' => $id)));
  $exam_result=[];
  foreach($stu_batch as $batch){
  $exam_setup=$this->ExamSetup->find('all',array('conditions'=>array('ExamSetup.b_id'=>$batch['Batch']['b_id'],'FIND_IN_SET(\''. $id .'\',ExamSetup.students)'),'recursive'=>3));
     if(!empty($exam_setup)){ 
  foreach($exam_setup as $setup){ 
        $data['e_name']=$setup['ExamSetup']['e_name'];
        $data['add_date']=$setup['ExamSetup']['add_date'];
        $data['batch_code']=$setup['Batch']['batch_code'];
        if(!empty($setup['Batch']['Subjects'])){
    $data['subject']=$setup['Batch']['Subjects']['sub_name'];
     }else{
    $data['subject']=$setup['Batch']['QuestionCategory']['qc_name'];
     }
     $stu_result = $this->Result->find('first', array('conditions' => array('Result.s_id' => $id,'Result.e_id'=>$setup['ExamSetup']['e_id']),'recursive'=>-1));
     
     if(!empty($stu_result)){
      $data['marks']=$stu_result['Result']['marks_obtained'].'/'.$stu_result['Result']['total_marks'];
      $data['result']=$stu_result['Result']['result'];
     } else{
      $data['marks']='-';   
      $data['result']='Absent';
  }
  
          $exam_result[]=$data;
       
  }
}
     
  }

    }


public function delete($id)
{
  $this->loadModel('Users');
  $catdelete = $this->Users->get($id);
    if($catdelete){
      unlink('images/user_images/' . $catdelete['image']);
      $this->Users->deleteAll(['Users.id' => $id]); 
      $this->Users->delete($catdelete);

      $this->Flash->success(__('User has been deleted successfully.'));
      return $this->redirect(['action' => 'index']);
    }else{
      $this->Flash->error(__('User not  delete'));
      return $this->redirect(['action' => 'index']);
    }
}
public function status($id,$status){

	$this->loadModel('Users');
	if(isset($id) && !empty($id)){
	  $product = $this->Users->get($id);
	  $product->status = $status;
	  if ($this->Users->save($product)) {
		if($status=='1'){
		  $this->Flash->success(__('User status has been Activeted.'));
		}else{
		  $this->Flash->success(__('User status has been Deactiveted.'));
		}
		return $this->redirect(['action' => 'index']);  
	  }
	}
  }
/*
public function add($id=null){ 
    $this->loadModel('Users');
    $this->loadModel('Company');
    $this->loadModel('Currencies');
    $this->loadModel('Timezones');
    $this->loadModel('Languages');
    
    $this->viewBuilder()->layout('admin');
    $this->set(compact('branch_count','branch_list','default_branch'));

    $currencies = $this->Currencies->find('list')->select(['name'])->where(['Currencies.status' => 1])->order(['Currencies.id'=>'DESC'])->toarray();
  $this->set('currencies',$currencies); 
  
  $timezones = $this->Timezones->find('list')->select(['name'])->where(['Timezones.status' => 1])->order(['Timezones.id'=>'DESC'])->toarray();
  $this->set('timezones',$timezones);
  
  $languages = $this->Languages->find('list')->select(['name'])->where(['Languages.status' => 1])->order(['Languages.id'=>'DESC'])->toarray();
  $this->set('languages',$languages);

    $ntypes = $this->Users->get($id);
    //pr($ntypes); die;
    if ($this->request->is(['post', 'put'])) {

      if ($this->request->data['brand_logo']['name'] != '')
      {
        $k = $this->request->data['brand_logo'];
        $galls = $this->move_images($k,'images/');
        $this->FcCreateThumbnail("compress", "images", $galls[0], $galls[0], "100", "100");
        $this->request->data['brand_logo'] = $galls[0];
        unlink('compress/' . $galls[0]);
      }else{
        $this->request->data['brand_logo'] = $ntypes['brand_logo'];
      } 

      if ($this->request->data['icon']['name'] != '')
      {
        $k = $this->request->data['icon'];
        $galls = $this->move_images($k,'images/');
        $this->FcCreateThumbnail("compress", "images", $galls[0], $galls[0], "100", "100");
        $this->request->data['icon'] = $galls[0];
        unlink('compress/' . $galls[0]);
      }else{
        $this->request->data['icon'] = $ntypes['icon'];
      } 

      if((isset($this->request->data['new_password']) && !empty($this->request->data['new_password'])) && (isset($this->request->data['confirm_passs']) && !empty($this->request->data['confirm_passs']))){
          if($this->request->data['new_password'] == $this->request->data['confirm_passs']){
          $this->request->data['password'] = (new DefaultPasswordHasher)->hash($this->request->data['new_password']);     //change password
          $this->request->data['confirm_pass']=$this->request->data['confirm_passs'];
    if ($this->request->data['image']['name'] != '')
      { 
        $this->request->data['image']=$this->request->data['image'];
      }
          $ntypes = $this->Users->patchEntity($ntypes, $this->request->data); 
          if ($this->Users->save($ntypes)) {
            $this->Flash->success(__('Your password is changed sucessfully, Please log in with new password'));
            return $this->redirect('/logout');  
          }
        }else{
          $this->Flash->error(__('Your new password and confirm password doesnot match, try again.'));
          
        }
    }
    
    $ntypes = $this->Users->patchEntity($ntypes, $this->request->data); 
    $this->Users->save($ntypes);
    $this->Flash->success(__('Your Profile has been updated.'));
    return $this->redirect(['action' =>'add/'.$id]);
    
  }
  $this->set('ntypes', $ntypes);
} */
   //  to change password
public function changepassword(){ 
	$this->viewBuilder()->layout('admin');
	$this->loadModel('Users');   
	$user =$this->Users->get($this->Auth->user('id')); 
	if ($this->request->is(['post', 'put'])) {
			//check old password and new password
		if((isset($this->request->data['new_password']) && !empty($this->request->data['new_password'])) && (isset($this->request->data['confirm_pass']) && !empty($this->request->data['confirm_pass']))){
			if($this->request->data['new_password'] == $this->request->data['confirm_pass']){
					$this->request->data['password'] = (new DefaultPasswordHasher)->hash($this->request->data['new_password']);			//change password
					$user = $this->Users->patchEntity($user, $this->request->data); 
					$this->Users->save($user);
					$this->Flash->success(__('Your password is successfully Changed.'));
					return $this->redirect(['controller'=>'visitors','action' => 'index']);
					
				}else{
					$this->Flash->error(__('Your new password and confirm password doesnot match, try again.'));
					return $this->redirect(['controller'=>'visitors','action' => 'index']);	
				}
			}	
		}
	}

  public function isAuthorized($user)
{
  if (isset($user['role_id']) && ($user['role_id'] == 1) || ($user['role_id'] == 4)) {
    return true;
  }
  return false;
}
  
}
