<?php

namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Datasource\ConnectionManager;

class EnquiryController extends AppController
{
	

	public function ajx()
    {
		$this->loadModel('Enquiry');
        $this->loadModel('ArchiveEnquiry');
        $phone = $this->request->data['phone'];


		$enq = $this->Enquiry->find('all')->contain(['Branch','Course'])->where(['Enquiry.phone'=>$this->request->data['phone']])->first();


        // dd($enq); die;
        if (empty($enq)) {

			$enq = $this->ArchiveEnquiry->find('all')->contain(['Branch','Course'])->where(['ArchiveEnquiry.phone'=>$this->request->data['phone']])->first();
			
            if (!empty($enq)) {
                $enq['closed'] = 1;
            }
        }
        $this->set('enquiry', $enq);

    }

	public function newcontact()
    {

       
        $return = '';

        //$this->loadModel('Resume');
        $this->loadModel('Enquiry');
        $this->loadModel('ArchiveEnquiry');

       //pr($this->request->data);
	$cm = $this->Enquiry->find('all')->where(['Enquiry.phone'=>$this->request->data['phone']])->first();


    if ($cm['enq_id']=="") {
	$cm = $this->ArchiveEnquiry->find('all')->where(['ArchiveEnquiry.phone'=>$this->request->data['phone']])->first();
		
	if ($cm['enq_id']=="") {
                    $return = 1;
                } else {
                    $return = 0;
                }
            } else {

                $return = 2;
            }

        
        echo $return; die;

    }
    public function get_client_ip_env()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } elseif (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } elseif (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } elseif (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }

    public function delete($id)
{
  $this->loadModel('Enquiry');
  $catdelete = $this->Enquiry->get($id);
    if($catdelete){
    
      $this->Enquiry->deleteAll(['Enquiry.enq_id' => $id]); 
      $this->Enquiry->delete($catdelete);

   

      $this->Flash->flash("Enquiry has been deleted successfully.", [
        "params" => [
          "type" => "success"
        ]
        ]);
    }else{
        $this->Flash->flash("Enquiry not deleted successfully.", [
            "params" => [
              "type" => "error"
            ]
            ]);
      
    }
    return $this->redirect(['action' => 'index']);
}
public function file_get_contents_curl($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

public function impo($id = '', $mobile, $branch)
{

    $conns = ConnectionManager::get('default');
    $query1 ="update tblenquiry set important='1' where enq_id=" . $id;
    $conns->execute($query1);

  
    $this->loadModel('Branch');
    if ($branch != 0) {
      
        $branch = $this->Branch->find('all')->where(['id' => $branch])->first();


       
    } else {
        $branch = $this->Branch->find('all')->where(['id' => 2])->first();
     
    }
    $mesg = "Thank You for showing interest in DAAC. Contact:" . $branch['phone'];
    $this->Session->setFlash(__('The Enquiry has been important'), 'flash/sucess');
    $result = $this->file_get_contents_curl('http://alerts.prioritysms.com/api/web2sms.php?workingkey=A861bd14a91290960179eb61cc4c9d2ca&to=' . $mobile . '&sender=DhWala&message=' . urlencode($mesg));

    $this->redirect($this->referer());

}
    public function openenquirydetails($id=null)
    {
        $this->loadModel('Mode');
        $this->loadModel('Enquiry');
        $this->loadModel('EnquiryCalls');
        $this->loadModel('Course');
        $this->loadModel('Branch');
        $this->loadModel('ArchiveEnquiry');
        $this->loadModel('Follow');
        $this->set(compact('id'));
        $det = $this->Enquiry->find('all')->contain(['Follow'])->where(['Enquiry.enq_id'=> $id])->first();


        $det['callDetails'] = $this->EnquiryCalls->find('all')->where(['EnquiryCalls.customer_number'=>$det['phone']])->order(['EnquiryCalls.call_date'=> 'DESC'])->toarray();
       

        
        $this->set(compact('det'));

        $mode_list = $this->Mode->find('list', [
			'keyField' => 'id',
			'valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();
		

        $this->set(compact('mode_list'));
        $ac1 = $this->Course->find('list', [
			'keyField' => 'id',
			'valueField' => 'sname'])->order(['sname' => 'ASC'])->toArray();
		
        $this->set('categ1', $ac1);

        $branch = $this->request->session()->read("Auth.User.branch");
        // echo $branch; die;

		$transbranch = $this->Branch->find('list', [
			'keyField' => 'id',
			'valueField' => 'name'])->where(['Branch.id !=' => $branch,'Branch.status' => 'Y'])->order(['name' => 'ASC'])->toArray();

        $this->set('transbranch', $transbranch);

        

        if ($this->request->is('post')) {
            // dd($this->request->data);die;
            if ($this->request->data['reference']) {

                $enq=$this->request->data['enq_id']; 
                $ref = $this->request->data['reference'];

                $conns = ConnectionManager::get('default');
                    $query1 ="update tblenquiry set reference=" . $ref . " where enq_id=" . $enq;
                     $conns->execute($query1);


              
            }

            if (!empty($this->request->data['follow_id'])) {
                $this->loadModel('Follow');

                $conns = ConnectionManager::get('default');
                $query1 ='update followup set called=0,status=1,new_response="' . $this->request->data['f_responce'] . '",created="' . date('Y-m-d H:i:s') . '" where f_id=' . $this->request->data['follow_id'];

                 $conns->execute($query1);

            }
            $this->request->data['called'] = 1;
            if (!empty($enq)) {
                $this->request->data['enq_id'] = $enq;
            } else {
                $enq = $this->request->data['enq_id'];
            }
            $this->request->data['add_date'] = date('Y-m-d');

            $fdate = $this->request->data['fdate'];
            $shift = $this->request->data['branch'];
            $st = $this->request->data['status'];
            $walkin = $this->request->data['is_walkin'];
            if (!empty($fdate)) {
                $fd = date('Y-m-d', strtotime($fdate));

            } else {
                $f_date = date('Y-m-d');
                $date3min = strtotime(date("Y-m-d", strtotime($f_date)) . "+3 Day");
                $fd = date('Y-m-d', $date3min);
            }

            if (!empty($shift)) {
                $fd = date('Y-m-d');
            }
            $branch = $this->request->session()->read("Auth.User.branch");
            $this->request->data['f_date'] = $fd;
            $this->request->data['branch'] = $branch;
            $this->request->data['created'] = date('Y-m-d H:i:s');

            $follow = $this->Follow->newEntity();
            $savepack = $this->Follow->patchEntity($follow,$this->request->data);

            
            if ($this->Follow->save($savepack)) {

                // dd($branch); die;
                if ($walkin == 'Y') {

                    $conns = ConnectionManager::get('default');
                    $query1 ="update tblenquiry set is_walkin='Y',follow='1',branch_id='" . $branch . "' where enq_id=" . $enq;
    
                     $conns->execute($query1);

                } else {

                    $conns = ConnectionManager::get('default');
                    $query1 ="update tblenquiry set follow='1',branch_id='" . $branch . "' where enq_id=" . $enq;
                     $conns->execute($query1);

                }
                if (!empty($shift)) {
                 
                    $conns = ConnectionManager::get('default');
                    $query1 ="update tblenquiry set follow='1',branch_id='" . $shift . "',c_id='" . $shift . "',shiftfrom='" . $branch . "',shift_date='" . date('Y-m-d') . "' where enq_id=" . $enq;
                     $conns->execute($query1);
                  
                    $this->redirect($this->referer());
                }

                if ($st != 1) {
                    $enquiry = $this->Enquiry->find('all')->where(['Enquiry.enq_id' => $enq])->first();
                    $follow_enq = $this->Follow->find('all')->where(['Follow.enq_id' => $enq])->toarray();
               
                    $enquiry['closing_date'] = date('Y-m-d');
                    $enquiry['status'] = $st;


                    $followee = $this->ArchiveEnquiry->newEntity();
                    $savepacks = $this->ArchiveEnquiry->patchEntity($followee,$enquiry);
        
                    
                    if ($this->ArchiveEnquiry->save($savepacks)) {


                   
                        foreach ($follow_enq as $follow_enqs) {
                            $archiveFollow = $this->ArchiveFollow->create();

                                $sasvepacsjjkFollow = $this->ArchiveFollow->patchEntity($archiveFollow, $follow_enqs);

                                    $this->ArchiveFollow->save($sasvepacsjjkFollow);
                                }

                                
                 $this->Follow->deleteAll(['Follow.enq_id' =>$enq]); 
                 $this->Enquiry->delete(['Enquiry.enq_id' =>$enq]);
                    }
                }

                $this->Flash->flash("The Follow up has been saved.", [
                    "params" => [
                      "type" => "success"
                    ]
                    ]);

                $this->redirect($this->referer());

            } else {


                $this->Flash->flash("The Follow up could not be saved. Please, try again.", [
                    "params" => [
                      "type" => "error"
                    ]
                    ]);
           
                    $this->redirect($this->referer());
            }

        }

    } 

    public function closedenquirydetails($id=null)
    {
    $this->loadModel('Mode');
    $this->loadModel('Enquiry');
    $this->loadModel('EnquiryCalls');
    $this->loadModel('Course');
    $this->loadModel('Branch');
    $this->loadModel('ArchiveEnquiry');
    $this->loadModel('ArchiveFollow');
    $this->set(compact('id'));
    $det = $this->ArchiveEnquiry->find('all')->contain(['ArchiveFollow'])->where(['ArchiveEnquiry.enq_id'=> $id])->first();

    $this->set(compact('det'));

    $mode_list = $this->Mode->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();
    

    $this->set(compact('mode_list'));
    $ac1 = $this->Course->find('list', [
        'keyField' => 'id',
        'valueField' => 'sname'])->order(['sname' => 'ASC'])->toArray();
    
    $this->set('categ1', $ac1);

    $branch = $this->request->session()->read("Auth.User.branch");
    // echo $branch; die;

    $transbranch = $this->Branch->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'])->where(['Branch.id !=' => $branch,'Branch.status' => 'Y'])->order(['name' => 'ASC'])->toArray();

    $this->set('transbranch', $transbranch);

    


} 


    public function search()
    {
          
        $this->loadModel('Enquiry');	
        $this->loadModel('Mode');
        $this->loadModel('Branch');
        $this->loadModel('Course');
        $this->loadModel('Follow');


		$mode_list = $this->Mode->find('list', [
			'keyField' => 'id',
			'valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();
		

        $this->set(compact('mode_list'));

        $this->request->session()->delete("fdate");
        $this->request->session()->delete("cont");


		$ac1 = $this->Course->find('list', [
			'keyField' => 'id',
			'valueField' => 'sname'])->order(['sname' => 'ASC'])->toArray();
		
        $this->set('categ1', $ac1);

		$mode = $this->Mode->find('list', [
			'keyField' => 'id',
			'valueField' => 'name'])->where(['id NOT IN' =>['11','1']])->order(['name' => 'ASC'])->toArray();

        $this->set('mode', $mode);


	 $branhces = $this->Branch->find('list', [
			'keyField' => 'name',
			'valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();

        $this->set('branhces', $branhces);

        $branch = $this->request->session()->read("Auth.User.branch");
        // echo $branch; die;

		$transbranch = $this->Branch->find('list', [
			'keyField' => 'name',
			'valueField' => 'name'])->where(['Branch.id !=' => $branch,'Branch.status' => 'Y'])->order(['name' => 'ASC'])->toArray();

        $this->set('transbranch', $transbranch);

       
        $course = $this->request->data['course_id'];
        $mode = $this->request->data['modes'];

        $br = $this->request->data['branch_id'];

        $mob = $this->request->data['mobile'];

        $name = $this->request->data['name'];
        $pg = $this->request->data['paper'];
        $fdate = $this->request->data['fdate'];
        $type = $this->request->data['type'];
      
        $pg = "followsearch";
        $this->set('pg', $pg);
       
        $apk = array();

        // if ($pg == 'followenq') {

        //     $sts = array('Enquiry.follow' => 'Asc', 'Enquiry.enq_id' => 'Desc');
        //     $newarray = $this->Session->read("cont");
        //     //$apk[]=array('Enquiry.enq_id' => $newarray);
        //     $apk[] = array('Enquiry.status' => '1');

        // } else {
        //     $sts = array('Enquiry.status' => 'Desc', 'Enquiry.enq_id' => 'Desc');
        // }

        $fc = date('Y-m-d');


        $conns=ConnectionManager::get('default');
        $query1="SELECT *,Max(f_id) as mf FROM `followup` group by enq_id having `f_date`<='" . $fc . "'";
        $foll=$conns->execute($query1)->fetchALL("assoc");
                $fids = array();
                foreach ($foll as $fl) {
                    $fids[] = $fl['mf'];
                }
        
                $cn = [];
        
                

        
                $apk['Follow.f_id IN']=$fids;



        if (!empty($course)) {

            $apk['Enquiry.course_id']=$course;
          
        }
        if (!empty($mode)) {

            $apk['Enquiry.reference']=$mode;
           
        }

        if (!empty($mob)) {

            $apk['Enquiry.phone']=$mob;

           
        }
        if (!empty($name)) {

            $apk['Enquiry.name LIKE %']=$name.'%';


           
        }

        if ($type == 'walkin') {
            $apk['Enquiry.is_walkin']='Y';
           
        } elseif ($type == "marketing") {
            $ref = [4, 7, 14];
            $apk['Enquiry.reference IN']=$ref;
            
        } elseif ($type == "important") {

            $apk['Enquiry.important']=1;
           
        }
        // dd($fids); die;

      /*  if(!empty($fdate)){
            if($type == "shifted_to" || $type == "shifted"){
               $apk[] = array('Enquiry.shift_date >=' => date('Y-m-d', strtotime($fdate)));
            }else{
               $apk[] = array('Enquiry.add_date >=' => date('Y-m-d', strtotime($fdate)));
            }
           }
           if(!empty($todate)){
               if($type == "shifted_to" || $type == "shifted"){
                   $apk[] = array('Enquiry.shift_date <=' => date('Y-m-d', strtotime($todate)));
                }else{
                   $apk[] = array('Enquiry.add_date <=' => date('Y-m-d', strtotime($todate)));
                }
           }
*/

       
        if ($this->request->session()->read("Auth.User.role_id") == 1) {
            if ($type == "viewall") {
              
            }
            if ($br) {

                $apk['Enquiry.branch_id']=$br;
       
            }
            if ($type == "shifted_to" || $type == "shifted") {

                $apk['Enquiry.shiftfrom !=']=0;
                
            }

            $studentsall = $this->Follow->find('all')->contain(['Enquiry'])->where([$apk])->order(['Follow.add_date' => 'DESC'])->limit(50);
			$destination=$this->paginate($studentsall)->toarray();

         
        } else { //pr($cn); die;
            $brn = $this->request->session()->read("Auth.User.branch");
            if ($type == "viewall") {
              
            } elseif ($type == "shifted_to") {
                $apk['Enquiry.branch_id !=']=$brn;
                $apk['Enquiry.shiftfrom']=$brn;

              
            } elseif ($type == "shifted") {

                $apk['Enquiry.shiftfrom !=']=0;
                $apk['Enquiry.branch_id']=$brn;

               
            } else {

                $apk['Enquiry.branch_id']=$brn;
               
            }

            // dd($apk); die;
        }
            
         
        $studentsall = $this->Follow->find('all')->contain(['Enquiry'])->where([$apk])->order(['Follow.add_date' => 'DESC'])->limit(50);
        $destination=$this->paginate($studentsall)->toarray();
         

       
        $this->set('enq', $destination);

        $this->set('pg', $pg);
       
        $this->loadModel('Contact');
        $this->loadModel('ArchiveEnquiry');
        $this->loadModel('ArchiveFollow');
    }
    public function closedsearch()
    {
           $this->loadModel('ArchiveEnquiry');	
           $this->loadModel('ArchiveFollow');	
		

        $this->loadModel('Mode');
        $this->loadModel('Branch');
        $this->loadModel('Course');
        $this->loadModel('Follow');


		$mode_list = $this->Mode->find('list', [
			'keyField' => 'id',
			'valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();
		

        $this->set(compact('mode_list'));

        $this->request->session()->delete("fdate");
        $this->request->session()->delete("cont");


		$ac1 = $this->Course->find('list', [
			'keyField' => 'id',
			'valueField' => 'sname'])->order(['sname' => 'ASC'])->toArray();
		
        $this->set('categ1', $ac1);

		$mode = $this->Mode->find('list', [
			'keyField' => 'id',
			'valueField' => 'name'])->where(['id NOT IN' =>['11','1']])->order(['name' => 'ASC'])->toArray();

        $this->set('mode', $mode);


	 $branhces = $this->Branch->find('list', [
			'keyField' => 'name',
			'valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();

        $this->set('branhces', $branhces);

        $branch = $this->request->session()->read("Auth.User.branch");
        // echo $branch; die;

		$transbranch = $this->Branch->find('list', [
			'keyField' => 'name',
			'valueField' => 'name'])->where(['Branch.id !=' => $branch,'Branch.status' => 'Y'])->order(['name' => 'ASC'])->toArray();

        $this->set('transbranch', $transbranch);

       
        $course = $this->request->data['course_id'];
        $mode = $this->request->data['modes'];

        $br = $this->request->data['branch_id'];

        $mob = $this->request->data['mobile'];

        $name = $this->request->data['name'];
        $pg = $this->request->data['paper'];
        $fdate = $this->request->data['fdate'];
        $type = $this->request->data['type'];
      
        $pg = "followsearch";
        $this->set('pg', $pg);
       
        $apk = array();

        // if ($pg == 'followenq') {

        //     $sts = array('Enquiry.follow' => 'Asc', 'Enquiry.enq_id' => 'Desc');
        //     $newarray = $this->Session->read("cont");
        //     //$apk[]=array('Enquiry.enq_id' => $newarray);
        //     $apk[] = array('Enquiry.status' => '1');

        // } else {
        //     $sts = array('Enquiry.status' => 'Desc', 'Enquiry.enq_id' => 'Desc');
        // }

        $fc = date('Y-m-d');


        $conns=ConnectionManager::get('default');
        $query1="SELECT *,Max(f_id) as mf FROM `archivefollowup` group by enq_id having `f_date`<='" . $fc . "' order by f_id DESC LIMIT 0,1000";
        $foll=$conns->execute($query1)->fetchALL("assoc");
                $fids = array();
                foreach ($foll as $fl) {
                    $fids[] = $fl['mf'];
                }
        
                $cn = [];
        
                

        
                $apk['ArchiveFollow.f_id IN']=$fids;



        if (!empty($course)) {

            $apk['ArchiveEnquiry.course_id']=$course;
          
        }
        if (!empty($mode)) {

            $apk['ArchiveEnquiry.reference']=$mode;
           
        }

        if (!empty($mob)) {

            $apk['ArchiveEnquiry.phone']=$mob;

           
        }
        if (!empty($name)) {

            $apk['ArchiveEnquiry.name LIKE %']=$name.'%';


           
        }

        if ($type == 'walkin') {
            $apk['ArchiveEnquiry.is_walkin']='Y';
           
        } elseif ($type == "marketing") {
            $ref = [4, 7, 14];
            $apk['ArchiveEnquiry.reference IN']=$ref;
            
        } elseif ($type == "important") {

            $apk['ArchiveEnquiry.important']=1;
           
        }
        // dd($fids); die;

      /*  if(!empty($fdate)){
            if($type == "shifted_to" || $type == "shifted"){
               $apk[] = array('Enquiry.shift_date >=' => date('Y-m-d', strtotime($fdate)));
            }else{
               $apk[] = array('Enquiry.add_date >=' => date('Y-m-d', strtotime($fdate)));
            }
           }
           if(!empty($todate)){
               if($type == "shifted_to" || $type == "shifted"){
                   $apk[] = array('Enquiry.shift_date <=' => date('Y-m-d', strtotime($todate)));
                }else{
                   $apk[] = array('Enquiry.add_date <=' => date('Y-m-d', strtotime($todate)));
                }
           }
*/

       
        if ($this->request->session()->read("Auth.User.role_id") == 1) {
            if ($type == "viewall") {
              
            }
            if ($br) {

                $apk['ArchiveEnquiry.branch_id']=$br;
       
            }
            if ($type == "shifted_to" || $type == "shifted") {

                $apk['ArchiveEnquiry.shiftfrom !=']=0;
                
            }

            $studentsall = $this->ArchiveFollow->find('all')->contain(['ArchiveEnquiry'])->where([$apk])->order(['ArchiveFollow.add_date' => 'DESC'])->limit(50);
			$destination=$this->paginate($studentsall)->toarray();

         
        } else { //pr($cn); die;
            $brn = $this->request->session()->read("Auth.User.branch");
            if ($type == "viewall") {
              
            } elseif ($type == "shifted_to") {
                $apk['ArchiveEnquiry.branch_id !=']=$brn;
                $apk['ArchiveEnquiry.shiftfrom']=$brn;

              
            } elseif ($type == "shifted") {

                $apk['ArchiveEnquiry.shiftfrom !=']=0;
                $apk['ArchiveEnquiry.branch_id']=$brn;

               
            } else {

                $apk['ArchiveEnquiry.branch_id']=$brn;
               
            }

            // dd($apk); die;
        }
            
         
        $studentsall = $this->ArchiveFollow->find('all')->contain(['ArchiveEnquiry'])->where([$apk])->order(['ArchiveFollow.add_date' => 'DESC'])->limit(50);
        $destination=$this->paginate($studentsall)->toarray();
         

       
        $this->set('enq', $destination);

        $this->set('pg', $pg);
       
    
    }
	public function index()
    {
		$this->loadModel('Enquiry');	
		$this->viewBuilder()->layout('admin');

        $this->loadModel('Mode');
        $this->loadModel('Branch');
        $this->loadModel('Course');
        $this->loadModel('Follow');


		$mode_list = $this->Mode->find('list', [
			'keyField' => 'id',
			'valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();
		

        $this->set(compact('mode_list'));

        $this->request->session()->delete("fdate");
        $this->request->session()->delete("cont");


		$ac1 = $this->Course->find('list', [
			'keyField' => 'id',
			'valueField' => 'sname'])->order(['sname' => 'ASC'])->toArray();
		
        $this->set('categ1', $ac1);

		$mode = $this->Mode->find('list', [
			'keyField' => 'id',
			'valueField' => 'name'])->where(['id NOT IN' =>['11','1']])->order(['name' => 'ASC'])->toArray();

        $this->set('mode', $mode);


	 $branhces = $this->Branch->find('list', [
			'keyField' => 'id',
			'valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();

        $this->set('branhces', $branhces);

        $branch = $this->request->session()->read("Auth.User.branch");
        // echo $branch; die;

		$transbranch = $this->Branch->find('list', [
			'keyField' => 'name',
			'valueField' => 'name'])->where(['Branch.id !=' => $branch,'Branch.status' => 'Y'])->order(['name' => 'ASC'])->toArray();

        $this->set('transbranch', $transbranch);

        $fc = date('Y-m-d');

$conns=ConnectionManager::get('default');
$query1="SELECT *,Max(f_id) as mf FROM `followup` group by enq_id having `f_date`<='" . $fc . "'";
$foll=$conns->execute($query1)->fetchALL("assoc");
        $fids = array();
        foreach ($foll as $fl) {
            $fids[] = $fl['mf'];
        }

        $cn = [];

		$cn['Enquiry.enq_id']=$fids;
        
        $this->request->session()->write("cont", $fids);

		$cn['Enquiry.enq_id']=$fids;

     
        $roleId =  $this->request->session()->read("Auth.User.role_id");
        $this->set(compact('roleId'));


        if ($this->request->session()->read("Auth.User.role_id") == 1) {

			$branch = $this->Branch->find('all')->select(['id'])->toarray();
            $b_id = array();
		
            foreach ($branch as $branches){
                $b_id[] = $branches['id'];
            }

    		$studentsall = $this->Follow->find('all')->contain(['Enquiry'])->where(['Follow.f_date <= ' =>$fc, 'Follow.f_id IN' => $fids, 'Enquiry.c_id IN' => $b_id])->order(['Follow.f_date' => 'DESC', 'Enquiry.type' => 'Desc'])->limit(50);
			$destination=$this->paginate($studentsall)->toarray();

//  dd($this->paginate('Follow')); die;
        } else {
            //pr($cn); die;
            $brn = array();
            $brn[] = $branch;
            //$brn[]=0;

			$studentsall = $this->Follow->find('all')->contain(['Enquiry'])->where(['Follow.f_date <= ' =>$fc, 'Follow.f_id IN' =>$fids, 'Enquiry.c_id IN' => $brn])->order(['Follow.f_date' => 'DESC', 'Enquiry.type' => 'Desc'])->limit(50);
			$destination=$this->paginate($studentsall)->toarray();



        }

        $this->set('enq', $destination);

        $this->set('pg', $pg);
       
        $this->loadModel('Contact');
        $this->loadModel('ArchiveEnquiry');
        $this->loadModel('ArchiveFollow');
//  dd($_REQUEST); die;
        if ($this->request->is('post')) {
            // dd($this->request->data); die;

            $has_duplicate = $this->Enquiry->find('all')->where(['Enquiry.phone'=> $this->request->data['phone']])->count();

            $has_duplicate1 = 0;
            if ($this->request->data['closed'] != 1) {

                $has_duplicate1 = $this->ArchiveEnquiry->find('all')->where(['ArchiveEnquiry.phone'=> $this->request->data['phone']])->count();
             
            }

            if ($has_duplicate == 0 && $has_duplicate1 == 0) {
                //  dd($this->request->data); die;
                if ($this->request->data['closed'] == 1) {
                 
        $array = $this->ArchiveEnquiry->find('all')->where(['ArchiveEnquiry.phone'=> $this->request->data['phone']])->order(['ArchiveEnquiry.enq_id'=>'ASC'])->first();
               


                   
                    if (!empty($array)) {

                        $studentcourse = $this->Enquiry->newEntity();

                        $details['enq_id'] =$array['enq_id'];
                        $details['name'] =$array['name'];
                        $details['email'] =$array['email'];
                        $details['course_id'] =$array['course_id'];
                        $details['enquiry'] =$array['enquiry'];
                        $details['title'] =$array['title'];
                        $details['add_date'] =$array['add_date'];
                        $details['phone'] =$array['phone'];
                        $details['type'] =$array['type'];
                        $details['reference'] =$array['reference'];
                        $details['duplicate'] =$array['duplicate'];
                        $details['important'] =$array['important'];
                        $details['add_by'] =$array['add_by'];
                        $details['status'] =$array['status'];
                        $details['follow'] =$array['follow'];
                        $details['branch_id'] =$array['branch_id'];
                        $details['shiftfrom'] =$array['shiftfrom'];
                        $details['shift_date'] =$array['shift_date'];
                        $details['ip'] =$array['ip'];
                        $details['created'] =$array['created'];
                        $details['security_code'] =$array['security_code'];
                        $details['sc_name'] =$array['sc_name'];
                        $details['is_walkin'] =$array['is_walkin'];
                        $details['closing_date'] =$array['closing_date'];
                        $details['branch_id'] = $this->request->session()->read('Auth.User.branch');
                        $details['c_id'] = $this->request->session()->read('Auth.User.branch');
                        $details['restore_date'] = date('Y-m-d');
                        $details['status'] = 0;
     
        $savepack = $this->Enquiry->patchEntity($studentcourse, $details);
        if($this->Enquiry->save($savepack)) {
         $follow_dets = $this->ArchiveFollow->find('all')->where(['ArchiveFollow.enq_id' => $details['enq_id']])->first();

                

                           
                            foreach ($follow_dets as $follow_det) {

                                $follow = $this->Follow->newEntity();
                          
                    if (empty($follow_det['f_date'])) {
                           $follow_det['f_date'] = date('Y-m-d');
                       } elseif (strtotime($follow_det['f_date']) > strtotime(date('Y-m-d'))) {
                         $follow_det['f_date'] = date('Y-m-d');
                         }
                         $savepack = $this->Follow->patchEntity($follow, $follow_det);
   
                         $this->Follow->save($savepack);

                        }
                  


                 $this->ArchiveFollow->deleteAll(['ArchiveFollow.enq_id' =>$details['enq_id']]); 
                 $this->ArchiveFollow->delete(['ArchiveFollow.enq_id' =>$details['enq_id']]);
                    

                        }

                   

                        $this->Flash->flash("The Enquiry has been saved.", [
                            "params" => [
                              "type" => "success"
                            ]
                            ]);

                        $this->redirect($this->referer());

                    } else {


                        $this->Flash->flash("Duplicate Enquiry.", [
                            "params" => [
                              "type" => "error"
                            ]
                            ]);
                    
                        $this->redirect($this->referer());

                    }} else {

                        $enquiryy = $this->Enquiry->newEntity();
                        $foolowyy = $this->Follow->newEntity();

                    if ($this->request->data['status'] == '0') {
                        //  dd($this->request->data); die;
                    $this->request->data['branch_id'] = $this->request->session()->read('Auth.User.branch');
                    $this->request->data['add_date'] = date('Y-m-d');
                    $fdate = $this->request->data['f_date'];
                    $this->request->data['ip'] = $this->get_client_ip_env();

            if($this->request->session()->read('Auth.User.role_id') == 15) {
            $this->request->data['c_id'] = $this->request->session()->read('Auth.User.id');
            } else {
            $this->request->data['c_id'] = $this->request->session()->read('Auth.User.branch');
            }    
                    $savepacsk = $this->Enquiry->patchEntity($enquiryy, $this->request->data);
   
                        $result=$this->Enquiry->save($savepacsk);
                      if($result){
                            $enq = $result->enq_id;

                            if (!empty($fdate)) {
                                $fd = date('Y-m-d', strtotime($fdate));
                                $msg = "Student Told to Call on : ";
                            } else {
                                $f_date = date('Y-m-d');
                                $date3min = strtotime(date("Y-m-d", strtotime($f_date)) . "+3 Day");
                                $fd = date('Y-m-d', $date3min);
                                $msg = "Call on : ";
                            }

                            $this->request->data['enq_id'] = $enq;
                            $this->request->data['called'] = 0;
                            $this->request->data['add_date'] = date('Y-m-d');
                            $this->request->data['f_date'] = $fd;
                            $this->request->data['status'] = 1;
                            $this->request->data['ctype'] = $this->request->data['Enquiry']['status'];
                            $this->request->data['f_responce'] = $this->request->data['Enquiry']['enquiry'];

                    $sasvepacsk = $this->Follow->patchEntity($foolowyy, $this->request->data);
   
                        $result=$this->Follow->save($sasvepacsk);


                            // $this->Follow->query("update followup set status=" . $st . " where enq_id=" . $enq);

                    $enquiry = $this->Enquiry->find('all')->where(['Enquiry.enq_id' => $enq])->first();

                    $follow_enq = $this->Follow->find('all')->where(['Follow.enq_id' => $enq])->toarray();

                        
                        
                            // dd($enquiry); die;
                            $enquiry['closing_date'] = date('Y-m-d');
                            $enquiry['status'] = $this->request->data['status'];
                            $archiveEnquiry = $this->ArchiveEnquiry->newEntity();

                            $sasvepacsjjk = $this->ArchiveEnquiry->patchEntity($archiveEnquiry, $enquiry);
   
                       


                            if ($this->ArchiveEnquiry->save($sasvepacsjjk)) {
                                foreach ($follow_enq as $follow_enqs) {
                                $archiveFollow = $this->ArchiveFollow->create();

                                $sasvepacsjjkFollow = $this->ArchiveFollow->patchEntity($archiveFollow, $follow_enqs);

                                    $this->ArchiveFollow->save($sasvepacsjjkFollow);
                                }

                                
                 $this->Follow->deleteAll(['Follow.enq_id' =>$enq]); 
                 $this->Enquiry->delete(['Enquiry.enq_id' =>$enq]);
                    
                 
                            
                            }
                            

                            $this->Flash->flash("The Enquiry has been saved in Closed Enquiry.", [
                                "params" => [
                                  "type" => "success"
                                ]
                                ]);

                        } else {

                            $this->Flash->flash("The Enquiry could not be saved. Please, try again.", [
                                "params" => [
                                  "type" => "error"
                                ]
                                ]);
                     

                        }
                    } else {
                        // echo "ok"; die;
                        $this->request->data['branch_id'] = $this->request->session()->read('Auth.User.branch');
                        $this->request->data['add_date'] = date('Y-m-d');
                        $fdate = $this->request->data['f_date'];
                        $this->request->data['ip'] = $this->get_client_ip_env();

                        if ($this->request->session()->read('Auth.User.role_id') == 15) {
                            $this->request->data['c_id'] = $this->request->session()->read('Auth.User.id');
                        } else {
                            $this->request->data['c_id'] = $this->request->session()->read('Auth.User.branch');
                        }


                    $sasvepacEnquiry= $this->Enquiry->patchEntity($enquiryy, $this->request->data);

                      
                    $results=$this->Enquiry->save($sasvepacEnquiry);
                      
                          
                            if($results){
                                  $enq = $results->enq_id;
                       

                            if (!empty($fdate)) {
                                $fd = date('Y-m-d', strtotime($fdate));
                                $msg = "Student Told to Call on : ";
                            } else {
                                $f_date = date('Y-m-d');
                                $date3min = strtotime(date("Y-m-d", strtotime($f_date)) . "+3 Day");
                                $fd = date('Y-m-d', $date3min);
                                $msg = "Call on : ";
                            }

                            $this->request->data['enq_id'] = $enq;
                            $this->request->data['called'] = 0;
                            $this->request->data['add_date'] = date('Y-m-d');
                            $this->request->data['f_date'] = $fd;
                            $this->request->data['status'] = 1;
                            $this->request->data['ctype'] = $this->request->data['status'];
                            $this->request->data['f_responce'] = $this->request->data['enquiry'];

                            $sasvepacsky = $this->Follow->patchEntity($foolowyy, $this->request->data);
   
                            $result=$this->Follow->save($sasvepacsky);

                            $conns = ConnectionManager::get('default');
                            $query1 ="update tblenquiry set follow='1' where enq_id=" . $enq;
                            $conns->execute($query1);

                            $this->Flash->flash("The Enquiry has been saved.", [
                                "params" => [
                                  "type" => "success"
                                ]
                                ]);
                           
                        } else {


                            $this->Flash->flash("The Enquiry could not be saved. Please, try again.", [
                                "params" => [
                                  "type" => "error"
                                ]
                                ]);
                           

                        }
                    }
                  
                   
                // $archiveEnquiry = $this->Contact->newEntity();
                 
                 //  $this->request->data['is_enquiry'] = 1;
                 //  $sasvepacsjjk = $this->Contact->patchEntity($archiveContact,$this->request->data);
                  //  $this->Contact->save($sasvepacsjjk);
                

                    if ($_GET['search']) {

                        $this->redirect(array('controller' => 'Application', 'action' => 'app_user_contact?search=daac'));

                    } else {

                        $this->redirect($this->referer());

                    }

                }
            } else {

                
                $this->Flash->flash("Duplicate Enquiry found,enquiry could not be saved. Please, try again.", [
                    "params" => [
                      "type" => "error"
                    ]
                    ]);
           
                $this->redirect($this->referer());
            }

        }




    

}



public function closed(){
    $this->loadModel('Enquiry');	
    $this->viewBuilder()->layout('admin');

    $this->loadModel('Mode');
    $this->loadModel('Branch');
    $this->loadModel('Course');
    $this->loadModel('Follow');
    $this->loadModel('ArchiveFollow');


    $mode_list = $this->Mode->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();
    

    $this->set(compact('mode_list'));

    $this->request->session()->delete("fdate");
    $this->request->session()->delete("cont");


    $ac1 = $this->Course->find('list', [
        'keyField' => 'id',
        'valueField' => 'sname'])->order(['sname' => 'ASC'])->toArray();
    
    $this->set('categ1', $ac1);

    $mode = $this->Mode->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'])->where(['id NOT IN' =>['11','1']])->order(['name' => 'ASC'])->toArray();

    $this->set('mode', $mode);


 $branhces = $this->Branch->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'])->where(['status' => 'Y'])->order(['name' => 'ASC'])->toArray();

    $this->set('branhces', $branhces);

    $branch = $this->request->session()->read("Auth.User.branch");
    // echo $branch; die;

    $transbranch = $this->Branch->find('list', [
        'keyField' => 'name',
        'valueField' => 'name'])->where(['Branch.id !=' => $branch,'Branch.status' => 'Y'])->order(['name' => 'ASC'])->toArray();

    $this->set('transbranch', $transbranch);

    $fc = date('Y-m-d');

$conns=ConnectionManager::get('default');
$query1="SELECT *,Max(f_id) as mf FROM `archivefollowup` group by enq_id having `f_date`<='" . $fc . "' order by f_id DESC LIMIT 0,1000";
$foll=$conns->execute($query1)->fetchALL("assoc");
    $fids = array();
    foreach ($foll as $fl) {
        $fids[] = $fl['mf'];
    }

   

 
    $roleId =  $this->request->session()->read("Auth.User.role_id");
    $this->set(compact('roleId'));


    if ($this->request->session()->read("Auth.User.role_id") == 1) {

        $branch = $this->Branch->find('all')->select(['id'])->toarray();
        $b_id = array();
    
        foreach ($branch as $branches){
            $b_id[] = $branches['id'];
        }

        $studentsall = $this->ArchiveFollow->find('all')->contain(['ArchiveEnquiry'])->where(['ArchiveFollow.f_id IN' => $fids])->order(['ArchiveFollow.f_date' => 'DESC'])->limit(50);
        $destination=$this->paginate($studentsall)->toarray();

//  dd($this->paginate('Follow')); die;
    } else {
        //pr($cn); die;
        $brn = array();
        $brn[] = $branch;
        //$brn[]=0;

     $studentsall = $this->ArchiveFollow->find('all')->contain(['ArchiveEnquiry'])->where(['ArchiveFollow.f_id IN' =>$fids, 'ArchiveEnquiry.c_id IN' => $brn])->order(['ArchiveFollow.f_date' => 'DESC', 'ArchiveEnquiry.type' => 'Desc'])->limit(50);
        $destination=$this->paginate($studentsall)->toarray();



    }

    $this->set('enq', $destination);

    $this->set('pg', $pg);
   




}






	public function isAuthorized($user)
	{
	  if (isset($user['role_id']) && ($user['role_id'] == 1 || $user['role_id'] == 4)) {
		return true;
	  }
	  return false;
	}
	

}