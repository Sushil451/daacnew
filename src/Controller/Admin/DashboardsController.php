<?php

namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use \Datetime;

class DashboardsController extends AppController
{
	//$this->loadcomponent('Session');
	public function initialize(){	
		$this->loadModel('Users');
		
		

		parent::initialize();
	}
	public function registration_box_branch($branch_id){
	    $this->loadModel('TargetIncome');
	    $this->loadModel('Students');
			/*---front side start-----*/
		$reg_this_month=$this->Students->regPresentMonth($branch_id);
        $reg_this_year=$this->Students->regYear($branch_id);
		$this->set(compact('reg_this_month'));
		$this->set(compact('reg_this_year'));
		/*---front side end -----*/
		
		/*---back side start----*/

		

		$conns = ConnectionManager::get('default');
$query12 ='select sum(m1)+sum(m2)+sum(m3)+sum(m4)+sum(m5)+sum(m6)+sum(m7)+sum(m8)+sum(m9)+sum(m10)+sum(m11)+sum(m12) as sigma from tbl_income_t where branch='.$branch_id.' and year(add_date)='.date('Y');
$count=$conns->execute($query12)->fetchALL("assoc");;

			$reg_year_target=$count[0][0]['sigma'];
			$this->set('reg_year_target',$reg_year_target);
		
		/*---back side end-------*/

		}
	public function expense_box_branch(){

		
		$branch_id=$this->request->session()->read('Auth.User.branch');	
		//$dt = DateTime::createFromFormat('!m', date('n'));
		$month_name=date('F');
		//$this->loadModel('Expense');
		$this->loadModel('ExpenseDetail');
		$this->loadModel('ExpenseDetailTarget');
	
		//done
		$expense=$this->ExpenseDetail->getExpense($branch_id);
		
		$this->set('this_month_expense',$expense['m']['trans_amnt']);
		
	//	$this->set('this_year_expense',$expense['y'][0]['sum(trans_amnt)']);
	   $target_exp_month=$this->ExpenseDetailTarget->month($branch_id);
		$this->set('target_exp_month',$target_exp_month);
		$this->set('exp_actual_year',$expense['z']['trans_amnt']);
		
		//done

		$articles = $this->ExpenseDetailTarget; 
		$exp_month_target_b1 = $this->ExpenseDetailTarget->find('all')->select(['sum' => $articles->find('all')->func()->sum($month_name)])->where(['comp_id'=>$branch_id,'YEAR(add_date)'=>date('Y')])->first();


		
		$this->set('exp_month_target',$exp_month_target_b1['sum']);
		
		//done


		$articles = $this->ExpenseDetailTarget; 
		$exp_month_target_b11 = $this->ExpenseDetailTarget->find('all')->select(['january' => $articles->find('all')->func()->sum('january')])->where(['comp_id'=>$branch_id,'YEAR(add_date)'=>date('Y')])->first();

		$exp_month_target_b2 = $this->ExpenseDetailTarget->find('all')->select(['february' => $articles->find('all')->func()->sum('february')])->where(['comp_id'=>$branch_id,'YEAR(add_date)'=>date('Y')])->first();

		$exp_month_target_b3 = $this->ExpenseDetailTarget->find('all')->select(['march' => $articles->find('all')->func()->sum('march')])->where(['comp_id'=>$branch_id,'YEAR(add_date)'=>date('Y')])->first();

		$exp_month_target_b4 = $this->ExpenseDetailTarget->find('all')->select(['april' => $articles->find('all')->func()->sum('april')])->where(['comp_id'=>$branch_id,'YEAR(add_date)'=>date('Y')])->first();


		$exp_month_target_b5 = $this->ExpenseDetailTarget->find('all')->select(['may' => $articles->find('all')->func()->sum('may')])->where(['comp_id'=>$branch_id,'YEAR(add_date)'=>date('Y')])->first();

		$exp_month_target_b6 = $this->ExpenseDetailTarget->find('all')->select(['june' => $articles->find('all')->func()->sum('june')])->where(['comp_id'=>$branch_id,'YEAR(add_date)'=>date('Y')])->first();

		$exp_month_target_b7 = $this->ExpenseDetailTarget->find('all')->select(['july' => $articles->find('all')->func()->sum('july')])->where(['comp_id'=>$branch_id,'YEAR(add_date)'=>date('Y')])->first();

		$exp_month_target_b8 = $this->ExpenseDetailTarget->find('all')->select(['august' => $articles->find('all')->func()->sum('august')])->where(['comp_id'=>$branch_id,'YEAR(add_date)'=>date('Y')])->first();

		$exp_month_target_b9 = $this->ExpenseDetailTarget->find('all')->select(['september' => $articles->find('all')->func()->sum('september')])->where(['comp_id'=>$branch_id,'YEAR(add_date)'=>date('Y')])->first();

		$exp_month_target_b10 = $this->ExpenseDetailTarget->find('all')->select(['october' => $articles->find('all')->func()->sum('october')])->where(['comp_id'=>$branch_id,'YEAR(add_date)'=>date('Y')])->first();

		$exp_month_target_b11 = $this->ExpenseDetailTarget->find('all')->select(['november' => $articles->find('all')->func()->sum('november')])->where(['comp_id'=>$branch_id,'YEAR(add_date)'=>date('Y')])->first();

		$exp_month_target_b12 = $this->ExpenseDetailTarget->find('all')->select(['december' => $articles->find('all')->func()->sum('december')])->where(['comp_id'=>$branch_id,'YEAR(add_date)'=>date('Y')])->first();


		$year_target_exp_b1=$exp_month_target_b11+$exp_month_target_b2+$exp_month_target_b3+$exp_month_target_b4+$exp_month_target_b5+$exp_month_target_b6+$exp_month_target_b7+$exp_month_target_b8+$exp_month_target_b9+$exp_month_target_b10+$exp_month_target_b11+$exp_month_target_b12;


		$this->set('year_target_exp',$year_target_exp_b1);
		}




		
public function dropout_box_branch($y,$branch_id){


	$this->loadModel('StudentCourse');
	$this->loadModel('StudentInstallment');
	$drop_outs_year=$this->StudentCourse->totaldropCount($branch_id);

	$this->set(compact('drop_outs_year'));


$drop_outs_total=$this->StudentInstallment->lossMonth($branch_id);
$drop_out_fee_total=$this->StudentCourse->feeTotalYear($branch_id);
$drop_outs_total=$drop_out_fee_total-$drop_outs_total;
$this->StudentInstallment->bindModel(
        array('belongsTo' => array(
                'StudentCourse' => array(
                    'className' => 'StudentCourse',
                    'foreignKey'=>'s_id'
                )
            )
        )
    );
    $this->StudentInstallment->unbindModel(
    array('belongsTo' => array('Student','Branch','Installment'))
);
$drop_outs=$this->StudentCourse->dropCount($branch_id);
$this->set(compact('drop_outs'));
$drop_out_total_amount=$this->StudentInstallment->loss($branch_id);
$drop_out_fee_total=$this->StudentCourse->feeTotalAll($branch_id);
$drop_out_total_amount=$drop_out_fee_total-$drop_out_total_amount;
		$this->set(compact('drop_outs_total'));
			$this->set(compact('drop_out_total_amount'));
}

	public function index() {
		//echo $this->Session->read('Auth.User.role_id'); die;
		
		$this->viewBuilder()->layout('admin'); 
		
		$this->loadModel('Resume');
		$this->loadModel('PlacementRequire');
		$this->loadModel('FeeTransaction');
		$this->loadModel('StudentInstallment');
		$this->loadModel('Students');
		$total_pending_b1=$this->Students->total(1);
		$total_pending_b2=$this->Students->total(2);
		$total_pending_b3=$this->Students->total(3);
		
		
		$this->set(compact('total_pending_b1'));
		$this->set(compact('total_pending_b2'));
		$this->set(compact('total_pending_b3'));
				
		$total_placement_b1=$this->Students->totalplaced(1);
		$total_placement_b2=$this->Students->totalplaced(2);
		$total_placement_b3=$this->Students->totalplaced(3);
		$total_placement_b4=$this->Students->totalplaced(4);
		$this->set(compact('total_placement_b1'));
		$this->set(compact('total_placement_b2'));
		$this->set(compact('total_placement_b3'));
		$this->set(compact('total_placement_b4'));


		$es = $this->Resume->find('all')->where(['enroll !='=>''])->order(['id' => 'DESC'])->toarray();

	
			
			$placed_enroll=array();
			foreach($es as $e){
				$placed_enroll[]=$e['enroll'];
			}


			$st_count = $this->Students->find('all')->where(['is_placed='=>'Y','branch'=>'1'])->order(['s_id' => 'DESC'])->count();

		   $this->set('place_pending1',$st_count);
		
		   $st_count2 = $this->Students->find('all')->where(['is_placed='=>'Y','branch'=>'2'])->order(['s_id' => 'DESC'])->count();
		   $this->set('place_pending2',$st_count2);
		
		   $st_coun3 = $this->Students->find('all')->where(['is_placed='=>'Y','branch'=>'3'])->order(['s_id' => 'DESC'])->count();
		   $this->set('place_pending3',$st_coun3);

		   $st_coun4 = $this->Students->find('all')->where(['is_placed='=>'Y','branch'=>'4'])->order(['s_id' => 'DESC'])->count();
		   $this->set('place_pending4',$st_coun4);
		
		 

		   $count = $this->PlacementRequire->find('all')->where(['Year(PlacementRequire.created)'=>date('Y')])->order(['id' => 'DESC'])->count();

	
		$this->set('require',$count);
			
		
		
				if(date('m')==1){
			$lm=12;
			$ly=date('Y')-1;
		}else{
			$lm=date('m')-1;
			$ly=date('Y');
		}	
		   // $this->admin_right();
		   
	
		
		$role=$this->request->session()->read('Auth.User.role_id');
		$user=$this->request->session()->read('Auth.User.id');
		
		if($role!=1)
		{
		  
	$recd = $this->Students->find('all')->where(['Students.branch_id'=>$this->request->session()->read('Auth.User.branch')])->order(['s_id' => 'DESC'])->first();


	$rec = $this->Students->find('all')->where(['Students.branch_id'=>$this->request->session()->read('Auth.User.branch')])->order(['s_id' => 'DESC'])->first();


	$rec = $this->Students->find('all')->where(['Students.branch_id'=>$this->request->session()->read('Auth.User.branch')])->order(['s_id' => 'DESC'])->first();


	$recr = $this->FeeTransaction->find('all')->where(['FeeTransaction.branch_id'=>$this->request->session()->read('Auth.User.branch')])->order(['rec_id' => 'DESC'])->first();
		
	
		
		
		
		}
		else
		{
		

			$recd = $this->Students->find('all')->order(['s_id' => 'DESC'])->first();
			$rec = $this->Students->find('all')->order(['s_id' => 'DESC'])->count();


	  
			$recr = $this->FeeTransaction->find('all')->order(['rec_id' => 'DESC'])->first();
		
	
		
		
		}
		
		
		
		$date=date('Y-m-d');
		
		
		$this->set('stud',$recd);
		
		$this->set('dcount',$rec);
		
		
		
		
		
		$idd=$recr['s_id'];
		$insdd=$recr['sid_id'];
		
		if($insdd!=0)
		{
			
	
		
		$in = $this->StudentInstallment->find('all')->where(['StudentInstallment.sid_id'=>$insdd])->order(['sid_id' => 'DESC'])->first();
		


		$gi=$in['s_id'];
		$amt=$in['amount'];
		$title="INSTALLMENT Fee";
		
		$feestudent = $this->Students->find('all')->where(['Students.s_id'=>$gi])->order(['s_id' => 'DESC'])->first();

		
		}
			else
			{

		$feestudent = $this->Students->find('all')->where(['Students.s_id'=>$idd])->order(['s_id' => 'DESC'])->first();


	
		$amt=$feestudent['reg_fees'];
		$title="REGISTRATION   Fee";
		
				}
			  
		$this->set('student',$feestudent);
		$this->loadModel('Cash');
		$this->loadModel('StudentCourse');
		$this->set('amont',$amt);
		$this->set('title',$title);
		$y=date('Y'); $m=date('m');
		$cd=date('Y-m-d');
		
		$branch_id=$this->request->session()->read('Auth.User.branch');
		
		if($branch_id!=0){
		
			//$branch_left=$this->StudentInstallment->LeftBox($branch_id);
			$this->expense_box_branch();
		
			$this->registration_box_branch($branch_id);
			//$this->income_box_branch($branch_id);
			$this->dropout_box_branch($y,$branch_id);
			$this->placement_box_branch($m,$y,$branch_id);
			$this->enquiry_box_branch($lm,$ly,$m,$y,$branch_id,$cd);
		
		$latest_5=$this->Student->find('all',array('conditions'=>array('Student.drop_out'=>0,'Student.branch'=>$branch_id),'limit'=>5,'order'=>'s_id desc'));
		
		
		$today_cash=$branch_left[0];
		$week_cash=$branch_left[1];
		$month_cash=$branch_left[2];
		$month3_cash=$branch_left[3];
		$year_cash=$branch_left[4];
		//$inc_graph=$this->StudentInstallment->IncomeGraph($branch_id);
		
		$this->expense_branch_graph();
		/*Graph Collection data start */
		$cash_collection=array();
		
		for($i=1;$i<=12;$i++) {
			$month_name=date('F', strtotime("2012-$i-01"));
		
			
			$cash_collection[]=array('label'=>$month_name,'y'=>intval($inc_graph[$i]));
		}
		/*Graph Collection data end  */
		
		/* Graph Registration date start */
		
		$data_reg=array();
		for($i=1;$i<=12;$i++) {
			$month_name=date('F', strtotime("2012-$i-01"));
			$monthly_reg=$this->Student->find('count',array('conditions'=>array('YEAR(Student.add_date)'=>$y,'MONTH(Student.add_date)'=>$i,'Student.branch'=>$branch_id)));
			$data_reg[]=array('label'=>$month_name,'y'=>intval($monthly_reg));
		}
		
		/* Graph Registration data end */
		
		/*Graph Enquires Data start */
		$data_enq=array();
		for($i=1;$i<=31;$i++) {
			
		   
			$enq_month=$this->Enquiry->find('count',array('conditions'=>array('MONTH(Enquiry.add_date)'=>$m,'YEAR(Enquiry.add_date)'=>$y,'DAY(Enquiry.add_date)'=>$i,'Enquiry.branch'=>$branch_id,'DAY(Enquiry.add_date)'=>$i)));
			$follow_month=$this->Follow->find('count',array('conditions'=>array('Follow.called'=>0,'MONTH(Follow.created)'=>$m,'YEAR(Follow.created)'=>$y,'DAY(Follow.created)'=>$i,'Enquiry.branch'=>$branch_id)));
			//echo $follow_month." ";
			$data_enq[]=array('label'=>$i,'y'=>(intval($enq_month)+intval($follow_month)));
		}
		
		/*Graph Enquires Data end */
		
		
		/* Placement Graph Data start */
		
		$data_placement=array();
		for($i=1;$i<=12;$i++) {
			//$dt = DateTime::createFromFormat('!m', $i);
		  $month_name=date('F', strtotime("2012-$i-01"));
			$placement_month_data=$this->Resume->find('count',array('conditions'=>array('MONTH(Resume.created)'=>$i,'YEAR(Resume.created)'=>$y,'Resume.type'=>1,'Resume.branch'=>$branch_id))); 
			$data_placement[]=array('label'=>$month_name,'y'=>intval($placement_month_data));
		}
		
		/* Placement Graph data end*/
		
		
		/* income branch wise */
		
		$lastinc_graph=$this->Cash->LastIncomeGraph($branch_id);
		
		for($i=1;$i<=12;$i++) {
		$lastincometotalbrach+=$lastinc_graph[$i];
		 $month_name=date('F', strtotime("2012-$i-01"));
		
			$lastyearcash_collection[]=array('label'=>$month_name,'y'=>intval($lastinc_graph[$i]));
		
		}
		
		
		$this->set('lastcash_collectionbranch',json_encode($lastyearcash_collection));
		$this->set(compact('lastincometotalbrach'));
		
		
		// current cash
		
		$inc_graph=$this->Cash->IncomeGraph($branch_id);
		
		
		for($i=1;$i<=12;$i++) {
		
		$cash_collectionbra+=$inc_graph[$i];
		 $month_name=date('F', strtotime("2012-$i-01"));
		
			$cash_collectionbrach[]=array('label'=>$month_name,'y'=>intval($inc_graph[$i]));
		
		}
		
		$this->set('cash_collectionbranch',json_encode($cash_collectionbrach));
		$this->set(compact('cash_collectionbra'));
		
		// target income
		
		
		$target_income_month=$this->TargetIncome->target_income($branch_id);
		
		
		
		
		$jan=0;
		$feb=0;
		$mar=0;
		$apr=0;
		$may=0;
		$jun=0;
		$july=0;
		$aug=0;
		$set=0;
		$oct=0;
		$nov=0;
		$dec=0;
		foreach($target_income_month as $value){
		
			$jan+=$value['TargetIncome']['jan'];
			$feb+=$value['TargetIncome']['feb'];
			$mar+=$value['TargetIncome']['march'];
			$apr+=$value['TargetIncome']['april'];
			$may+=$value['TargetIncome']['may'];
			$jun+=$value['TargetIncome']['june'];
			$july+=$value['TargetIncome']['july'];
			$aug+=$value['TargetIncome']['aug'];
			$set+=$value['TargetIncome']['sept'];
			$oct+=$value['TargetIncome']['oct'];
			$nov+=$value['TargetIncome']['nov'];
			$dec+=$value['TargetIncome']['dece'];
		}
		
		
		$target=array();
		
			$targetincometotalbranch=$jan+$feb+$mar+$apr+$may+$jun+$july+$aug+$set+$oct+$nov+$dec;
		   
			$target[]=array('label'=>'January','y'=>intval($jan));
			$target[]=array('label'=>'February','y'=>intval($feb));
			$target[]=array('label'=>'March','y'=>intval($mar));
			$target[]=array('label'=>'April','y'=>intval($apr));
			$target[]=array('label'=>'May','y'=>intval($may));
			$target[]=array('label'=>'June','y'=>intval($jun));
			$target[]=array('label'=>'July','y'=>intval($july));
			$target[]=array('label'=>'August','y'=>intval($aug));
			$target[]=array('label'=>'September','y'=>intval($set));
			$target[]=array('label'=>'October','y'=>intval($oct));
			$target[]=array('label'=>'November','y'=>intval($nov));
			$target[]=array('label'=>'December','y'=>intval($dec));
		
		
		$this->set('targetmonthbrach',json_encode($target));
		$this->set(compact('targetincometotalbranch'));
		
		
		
		// last year expense
		
		$lastexpen_graph=$this->ExpenseDetail->lastexenpGraph();
		
		for($i=1;$i<=12;$i++) {
		
		$lastexpentotal+=$lastexpen_graph[$i];
		 $month_name=date('F', strtotime("2012-$i-01"));
		$lastyearexpen_collection[]=array('label'=>$month_name,'y'=>intval($lastexpen_graph[$i]));
		
		}
		$this->set('lastcash_collection',json_encode($lastyearcash_collection));
			// current year expence 
		
		$expen_graph=$this->ExpenseDetail->exenpGraph();
		
		for($i=1;$i<=12;$i++) {
		
		$expentotal+=$expen_graph[$i];
		 $month_name=date('F', strtotime("2012-$i-01"));
		$yearexpen_collection[]=array('label'=>$month_name,'y'=>intval($expen_graph[$i]));
		
		}
		$this->set('expense',json_encode($yearexpen_collection));
		// current year expence target
		
		$target_expense_month=$this->ExpenseDetailTarget->target_expense($branch_id);
		// target expense
		
		$january=0;
		$february=0;
		$march=0;
		$april=0;
		$may=0;
		$june=0;
		$july=0;
		$august=0;
		$september=0;
		$october=0;
		$november=0;
		$december=0;
		foreach($target_expense_month as $value){
		
			$january+=$value['ExpenseDetailTarget']['january'];
			$february+=$value['ExpenseDetailTarget']['february'];
			$march+=$value['ExpenseDetailTarget']['march'];
			$april+=$value['ExpenseDetailTarget']['april'];
			$may+=$value['ExpenseDetailTarget']['may'];
			$junes+=$value['ExpenseDetailTarget']['june'];
			$julys+=$value['ExpenseDetailTarget']['july'];
			$augest+=$value['ExpenseDetailTarget']['august'];
			$september+=$value['ExpenseDetailTarget']['september'];
			$october+=$value['ExpenseDetailTarget']['october'];
			$november+=$value['ExpenseDetailTarget']['november'];
			$december+=$value['ExpenseDetailTarget']['december'];
		}
		$targetexp=array();
		
		
		   
			$targetexp[]=array('label'=>'January','y'=>intval($january));
			$targetexp[]=array('label'=>'February','y'=>intval($february));
			$targetexp[]=array('label'=>'March','y'=>intval($march));
			$targetexp[]=array('label'=>'April','y'=>intval($april));
			$targetexp[]=array('label'=>'May','y'=>intval($may));
			$targetexp[]=array('label'=>'June','y'=>intval($junes));
			$targetexp[]=array('label'=>'July','y'=>intval($julys));
			$targetexp[]=array('label'=>'August','y'=>intval($augest));
			$targetexp[]=array('label'=>'September','y'=>intval($september));
			$targetexp[]=array('label'=>'October','y'=>intval($october));
			$targetexp[]=array('label'=>'November','y'=>intval($november));
			$targetexp[]=array('label'=>'December','y'=>intval($december));
		
		
		
		$this->set('targetexp',json_encode($targetexp));
		
		
		
		$collection_month=$this->Cash->collectionMonth($this->Session->read('Auth.User.branch'));
		$this->set(compact('collection_month'));
		
		$pending_month=$this->StudentInstallment->pending_month($this->Session->read('Auth.User.branch'));
		$this->set(compact('pending_month')); 
		
		
		$target_income_year=$this->TargetIncome->yearIncome($this->Session->read('Auth.User.branch'));
		$this->set(compact('target_income_year'));
		
		$collection_year=$this->Cash->collectionYear($this->Session->read('Auth.User.branch'));
		$this->set(compact('collection_year'));
		
		$target_income_month_b1=$this->TargetIncome->monthIncome($this->Session->read('Auth.User.branch'));
		
		$this->set(compact('target_income_month_b1'));
		
		$branch_left=$this->Cash->LeftBox($this->Session->read('Auth.User.branch'));
		
		$today_cash=$branch_left[0];
		$week_cash=$branch_left[1];
		$month_cash=$branch_left[2];
		$month3_cash=$branch_left[3];
		$year_cash=$branch_left[4];
		
		// lastyear actual expen
		
		$lastexpen_graph=$this->ExpenseDetail->lastexenpGraph($this->Session->read('Auth.User.branch'));
		
		
		for($i=1;$i<=12;$i++) {
		
		$lastexpentotalbranch+=$lastexpen_graph[$i];
		 $month_name=date('F', strtotime("2012-$i-01"));
		$lastyearexpen_collectionbran[]=array('label'=>$month_name,'y'=>intval($lastexpen_graph[$i]));
		
		}
		
		$this->set('lastexpen_collectionbranch',json_encode($lastyearexpen_collectionbran));
		$this->set(compact('lastexpentotalbranch'));
		
		
		$currentexpen_graph=$this->ExpenseDetail->currentexenpGraph($this->Session->read('Auth.User.branch'));
		
		
		for($i=1;$i<=12;$i++) {
		
		$currentexpentotalbranch+=$currentexpen_graph[$i];
		 $month_name=date('F', strtotime("2012-$i-01"));
		$curentyearexpen_collectionbrancwish[]=array('label'=>$month_name,'y'=>intval($currentexpen_graph[$i]));
		
		}
		
		$this->set('currentexpen_collectionbranchdd',json_encode($curentyearexpen_collectionbrancwish));
		$this->set(compact('currentexpentotalbranch'));
		
		
		$target_expense_month=$this->ExpenseDetailTarget->currentexetargetnpGraph($this->Session->read('Auth.User.branch'));
		
		
		
		
		$jan=0;
		$feb=0;
		$mar=0;
		$apr=0;
		$may=0;
		$jun=0;
		$july=0;
		$aug=0;
		$set=0;
		$oct=0;
		$nov=0;
		$dec=0;
		foreach($target_expense_month as $value){
		
			 $jantargetexp+=$value['ExpenseDetailTarget']['january'];
			$febtargetexp+=$value['ExpenseDetailTarget']['february'];
			$martargetexp+=$value['ExpenseDetailTarget']['march'];
			$aprtargetexp+=$value['ExpenseDetailTarget']['april'];
			$maytargetexp+=$value['ExpenseDetailTarget']['may'];
			$juntargetexp+=$value['ExpenseDetailTarget']['june'];
			$julytargetexp+=$value['ExpenseDetailTarget']['july'];
			$augtargetexp+=$value['ExpenseDetailTarget']['august'];
			$settargetexp+=$value['ExpenseDetailTarget']['september'];
			$octtargetexp+=$value['ExpenseDetailTarget']['october'];
			$novtargetexp+=$value['ExpenseDetailTarget']['november'];
			$dectargetexp+=$value['ExpenseDetailTarget']['december'];
		}
		
		$targetexpen=array();
		
			$targeexpforbranchtotal=$jantargetexp+$febtargetexp+$martargetexp+$aprtargetexp+$maytargetexp+$juntargetexp+$julytargetexp+$augtargetexp+$settargetexp+$octtargetexp+$novtargetexp+$dectargetexp;
		   
			$targetexpen[]=array('label'=>'January','y'=>intval($jantargetexp));
			$targetexpen[]=array('label'=>'February','y'=>intval($febtargetexp));
			$targetexpen[]=array('label'=>'March','y'=>intval($martargetexp));
			$targetexpen[]=array('label'=>'April','y'=>intval($aprtargetexp));
			$targetexpen[]=array('label'=>'May','y'=>intval($maytargetexp));
			$targetexpen[]=array('label'=>'June','y'=>intval($juntargetexp));
			$targetexpen[]=array('label'=>'July','y'=>intval($julytargetexp));
			$targetexpen[]=array('label'=>'August','y'=>intval($augtargetexp));
			$targetexpen[]=array('label'=>'September','y'=>intval($settargetexp));
			$targetexpen[]=array('label'=>'October','y'=>intval($octtargetexp));
			$targetexpen[]=array('label'=>'November','y'=>intval($novtargetexp));
			$targetexpen[]=array('label'=>'December','y'=>intval($dectargetexp));
		
		
		$this->set('targetexpenmonthbrachwisedata',json_encode($targetexpen));
		$this->set(compact('targeexpforbranchtotal'));
		
		}
		else{
			
		
			$this->registration_box_admin();
		
			
				
			$this->expense_box_admin();
			$this->income_box_admin();
			 $this->expense_admin_graph(); 
			$this->dropout_box_admin();
				//echo "bfg"; die;
			$this->placement_box_admin($m,$y);
			$this->enquiry_box_admin($lm,$ly,$m,$y,$cd);
			
		$latest_5=$this->Student->find('all',array('conditions'=>array('Student.drop_out'=>0),'limit'=>5,'order'=>'s_id desc'));
		$branch_left=$this->Cash->LeftBox();
		//pr($branch_left); die;
		$today_cash=$branch_left[0];
		$week_cash=$branch_left[1];
		$month_cash=$branch_left[2];
		$month3_cash=$branch_left[3];
		$year_cash=$branch_left[4];
		$inc_graph=$this->Cash->IncomeGraph();
		
		$other_graph=$this->StudentInstallment->OtherGraph();
		$dtest=$other_graph[2];
		
		/*Graph Collection data start */
		$cash_collection=array();
		$data_reg=array();
		$data_drop=array();
		$data_placement=array();
		for($i=1;$i<=12;$i++) {
		$key = array_search($i, $dtest); 
		if($key != NULL)
		{
		$dropout=$other_graph[1][$i][total_dropout];
		}else
		{
		$dropout=0;
		}
		   $month_name=date('F', strtotime("2012-$i-01"));
		$placement_month_data=$this->Resume->find('count',array('conditions'=>array('MONTH(Resume.created)'=>$i,'YEAR(Resume.created)'=>$y,'Resume.type'=>1))); 
			$cash_collection[]=array('label'=>$month_name,'y'=>intval($inc_graph[$i]));
				
			  $data_reg[]=array('label'=>$month_name,'y'=>intval($other_graph[0][$i]));
			 $data_drop[]=array('label'=>$month_name,'y'=>intval($dropout));
		$data_placement[]=array('label'=>$month_name,'y'=>intval($placement_month_data));
		$currentincometotal+=$inc_graph[$i];
		}
		
		
		$data_enq=array();
		for($i=1;$i<=31;$i++) {
			
		   
			$enq_month=$this->Enquiry->find('count',array('conditions'=>array('MONTH(Enquiry.add_date)'=>$m,'YEAR(Enquiry.add_date)'=>$y,'DAY(Enquiry.add_date)'=>$i)));
		$follow=$this->Follow->find('count',array('conditions'=>array('MONTH(Follow.created)'=>$m,'YEAR(Follow.created)'=>$y,'DAY(Follow.created)'=>$i,'Follow.called'=>0)));
			$data_enq[]=array('label'=>$i,'y'=>(intval($enq_month)+intval($follow)));
		}
		
		}
		$lastinc_graph=$this->Cash->LastIncomeGraph();
		$expen_graph=$this->ExpenseDetail->exenpGraph();
		$lastexpen_graph=$this->ExpenseDetail->lastexenpGraph();
		
		//$lastyearcash_collection=array();
		for($i=1;$i<=12;$i++) {
		$lastincometotal+=$lastinc_graph[$i];
		 $month_name=date('F', strtotime("2012-$i-01"));
		
			$lastyearcash_collection[]=array('label'=>$month_name,'y'=>intval($lastinc_graph[$i]));
		
		}
		
		for($i=1;$i<=12;$i++) {
		
		$expentotal+=$expen_graph[$i];
		 $month_name=date('F', strtotime("2012-$i-01"));
		$yearexpen_collection[]=array('label'=>$month_name,'y'=>intval($expen_graph[$i]));
		
		}
		
		
		for($i=1;$i<=12;$i++) {
		
		$lastexpentotal+=$lastexpen_graph[$i];
		 $month_name=date('F', strtotime("2012-$i-01"));
		$lastyearexpen_collection[]=array('label'=>$month_name,'y'=>intval($lastexpen_graph[$i]));
		
		}
		$this->loadModel('Placement');
		$listed_companies=$this->Placement->find('count');
		
		
		
		$this->set(compact('listed_companies'));
		
		
		$this->set(compact('latest_5'));
		$this->set(compact('lastincometotal'));
		$this->set(compact('currentincometotal'));
		$this->set(compact('today_cash'));
		$this->set(compact('week_cash'));
		$this->set(compact('month_cash'));
		$this->set(compact('month3_cash'));
		$this->set(compact('year_cash'));
		
		$this->set('cash_collection',json_encode($cash_collection));
		$this->set('lastcash_collection',json_encode($lastyearcash_collection));
		$this->set('expense',json_encode($yearexpen_collection));
		$this->set('lastexpense',json_encode($lastyearexpen_collection));
		$this->set('data_reg',json_encode($data_reg));
		$this->set('data_enq',json_encode($data_enq));
		$this->set('data_drop',json_encode($data_drop));
		
		$data_placement=array();
		for($i=1;$i<=12;$i++) {
			//$dt = DateTime::createFromFormat('!m', $i);
		   $month_name=date('F', strtotime("2012-$i-01"));
			$placement_month_data=$this->Resume->find('count',array('conditions'=>array('MONTH(Resume.created)'=>$i,'YEAR(Resume.created)'=>$y,'Resume.type'=>1))); 
			$data_placement[]=array('label'=>$month_name,'y'=>intval($placement_month_data));
		}
		
		$this->set('data_placement',json_encode($data_placement));
		
		// lastyear actual expen
		
		$lastexpen_graph=$this->ExpenseDetail->lastexenpGraph(0);
		
		
		for($i=1;$i<=12;$i++) {
		
		$lastexpentotaladmin+=$lastexpen_graph[$i];
		 $month_name=date('F', strtotime("2012-$i-01"));
		$lastyearexpen_collectionddd[]=array('label'=>$month_name,'y'=>intval($lastexpen_graph[$i]));
		
		}
		
		$this->set('lastexpen_collection',json_encode($lastyearexpen_collectionddd));
		$this->set(compact('lastexpentotaladmin'));
		
		
		// current year
		
		$currentexpen_graph=$this->ExpenseDetail->currentexenpGraph(0);
		
		
		for($i=1;$i<=12;$i++) {
		
		$currentexpentotaladmingraph+=$currentexpen_graph[$i];
		 $month_name=date('F', strtotime("2012-$i-01"));
		$curentyearexpen_collectionddd[]=array('label'=>$month_name,'y'=>intval($currentexpen_graph[$i]));
		
		}
		
		$this->set('currentexpen_collection',json_encode($curentyearexpen_collectionddd));
		$this->set(compact('currentexpentotaladmingraph'));
		
		// target expendetails
		
		
		// target income
		
		
		$target_expense_month=$this->ExpenseDetailTarget->currentexetargetnpGraph();
		
		
		
		
		$jan=0;
		$feb=0;
		$mar=0;
		$apr=0;
		$may=0;
		$jun=0;
		$july=0;
		$aug=0;
		$set=0;
		$oct=0;
		$nov=0;
		$dec=0;
		foreach($target_expense_month as $value){
		
			$jantargetexp+=$value['ExpenseDetailTarget']['january'];
			$febtargetexp+=$value['ExpenseDetailTarget']['february'];
			$martargetexp+=$value['ExpenseDetailTarget']['march'];
			$aprtargetexp+=$value['ExpenseDetailTarget']['april'];
			$maytargetexp+=$value['ExpenseDetailTarget']['may'];
			$juntargetexp+=$value['ExpenseDetailTarget']['june'];
			$julytargetexp+=$value['ExpenseDetailTarget']['july'];
			$augtargetexp+=$value['ExpenseDetailTarget']['august'];
			$settargetexp+=$value['ExpenseDetailTarget']['september'];
			$octtargetexp+=$value['ExpenseDetailTarget']['october'];
			$novtargetexp+=$value['ExpenseDetailTarget']['november'];
			$dectargetexp+=$value['ExpenseDetailTarget']['december'];
		}
		$targetexpen=array();
		
			$targeexpforadmintotal=$jantargetexp+$febtargetexp+$martargetexp+$aprtargetexp+$maytargetexp+$juntargetexp+$julytargetexp+$augtargetexp+$settargetexp+$octtargetexp+$novtargetexp+$dectargetexp;
		   
			$targetexpen[]=array('label'=>'January','y'=>intval($jantargetexp));
			$targetexpen[]=array('label'=>'February','y'=>intval($febtargetexp));
			$targetexpen[]=array('label'=>'March','y'=>intval($martargetexp));
			$targetexpen[]=array('label'=>'April','y'=>intval($aprtargetexp));
			$targetexpen[]=array('label'=>'May','y'=>intval($maytargetexp));
			$targetexpen[]=array('label'=>'June','y'=>intval($juntargetexp));
			$targetexpen[]=array('label'=>'July','y'=>intval($julytargetexp));
			$targetexpen[]=array('label'=>'August','y'=>intval($augtargetexp));
			$targetexpen[]=array('label'=>'September','y'=>intval($settargetexp));
			$targetexpen[]=array('label'=>'October','y'=>intval($octtargetexp));
			$targetexpen[]=array('label'=>'November','y'=>intval($novtargetexp));
			$targetexpen[]=array('label'=>'December','y'=>intval($dectargetexp));
		
		
		$this->set('targetexpenmonthbrach',json_encode($targetexpen));
		$this->set(compact('targeexpforadmintotal'));
		
		
			}

	
	public function isAuthorized($user)
	{
		if (isset($user['role_id']) && ($user['role_id'] == 1 || $user['role_id'] == 4 )) {
			return true;
		}
		return false;
	}
        
}
