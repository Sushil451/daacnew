<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

class CertificateController extends AppController
{ 
	public function index(){
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Certificate');
		$from = date('M-Y');
		$to = "Jan-2017";
		while (strtotime($from) >= strtotime($to)) {
			$dates[date('m-Y', strtotime($from))] = date('M-Y', strtotime($from));
			$from = date('M-Y', strtotime('-1 month', strtotime($from)));
		}
		$this->set(compact('dates'));
 $certificate = $this->Certificate->find('all')->contain(['Course'])->order(['Certificate.ctitle'=>'ASC']);
  $this->set('destination', $this->paginate($certificate)->toarray());

}

public function ajx() {

	$this->loadModel('StudentCourse');
	$this->loadModel('Students');
	$this->loadModel('Course');
	$ac1 = $this->Course->find('list', [
		'keyField' => 'cname',
		'valueField' => 'cname',
	  ])->order(['Course.cname' => 'ASC'])->toArray();
	  


	$this->set('categ1', $ac1);

	$enrl = $this->request->data['s_enroll'];


$en=$this->Students->find('all')->contain(['Reportdiscount','StudentInstallment'])->where(['s_enroll' =>$enrl])->first();

	$stufee = 0;
	$depfee = 0;
	$valuedi = 0;

	foreach ($en['reportdiscount'] as $valuedis) {
		$valuedi += $valuedis['discount'];
	}

	foreach ($en['student_installment'] as $value) {

		$stufee += $value['amount'];
		if ($value['pay_date'] != '0000-00-00') {
			$depfee += $value['amount'];
		}

	}

	$this->set('stufee', $stufee);
	$this->set('depfee', $depfee);
	$this->set('valuedi', $valuedi); //pr($en); die;


	
$st_course=$this->StudentCourse->find('all')->contain(['Course'])->where(['StudentCourse.s_id' => $en['s_id']])->toarray();


$enc=$this->Certificate->find('all')->contain(['Course'])->where(['Certificate.s_enroll' => $enrl])->toarray();

	

	$course_arr = array();
	foreach ($enc as $cs) {
		$course_arr[] = $cs['course_id'];
	}

	$st_course=$this->StudentCourse->find('all')->contain(['Course'])->where(['StudentCourse.s_id' => $en['s_id']])->toarray();



	$this->set(compact('st_course'));
	if ($enc != null) {
		$this->set('enrc', $enc[0]['s_enroll']);
		$this->set('enrc_date', $enc[0]['add_date']); //pr($enc); die;
	} else {
		$this->set('enrc', null);
		$this->set('enrc_date', null);
	}



	
	$cert=$this->Students->find('all')->where(['Students.s_enroll' => $enrl])->first();

	$this->set('certi', $cert);
	
}
	public function requirement(){
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Placement');
	$this->loadModel('PlacementRequire');
		
	
 $seo = $this->PlacementRequire->find('all')->contain(['Placement'])->order(['PlacementRequire.id'=>'DESC']);
  $this->set('placement', $this->paginate($seo)->toarray());



}


public function chk_inst($s_id = '') {

	//echo $s_id; die;

	$this->loadModel('Installment');
	$this->loadModel('StudentInstallment');
	$this->loadModel('StudentCourse');
	$this->loadModel('Students');

	//dd($this->request->data); die;
	$course_id = $this->request->data['courseid'];
	$sdata=$this->Students->find('all')->where(['Students.s_id' => $s_id])->first();

	//$st_course=$this->StudentCourse->find('first',array('conditions'=>array('StudentCourse.s_id'=>$s_id,'StudentCourse.course_id'=>$course_id)));


	$st_course=$this->StudentInstallment->find('all')->where(['StudentInstallment.s_id' => $s_id, 'StudentInstallment.course_id' => $course_id])->toarray();

	
	foreach ($st_course as $valueinstal) {

		$stufee += $valueinstal['amount'];
		if ($valueinstal['pay_date'] != '0000-00-00') {
			$depfee += $valueinstal['amount'];
		}
	}

	$enrl = $valueinstal['s_enroll'];

	$en=$this->Students->find('all')->contain(['Reportdiscount'])->where(['Students.s_enroll' => $enrl])->first();

	foreach ($en['reportdiscount'] as $valuedis) {
		$valuedi += $valuedis['discount'];
	}

	$this->set('stufee', $stufee);
	$this->set('depfee', $depfee);
	$this->set('valuedi', $valuedi); //pr($en); die;



	$corse=$this->Installment->find('all')->where(['Installment.course_id' => $course_id])->toarray();


	//pr($corse); die;

	$corse_id = array();

	foreach ($corse as $c) {

		$corse_id[] = $c['inst_id'];

	} //pr($corse_id); die;


	
	$stdnt=$this->StudentInstallment->find('all')->where(['StudentInstallment.pay_date' => '0000-00-00', 'StudentInstallment.inst_id IN' => $corse_id, 'StudentInstallment.s_id' => $s_id])->count();


	//pr ($stdnt); die;

	$this->set('due_count', 0);
	$this->set('mobsdf', $sdata['s_contact']);

}


public function findcourseid(){

	$this->loadModel('Seo');
	$this->loadModel('Course');
	$orgid=$this->request->data['orgid'];
	if($this->request->data['orgid']==3){
		$orgid=0;
	}
	
	$this->set('orgid', $orgid);
	$maincoursesGroup = $this->Seo->find('list', [
		'keyField' => 'id',
		'valueField' => 'course_id',
	  ])->where(['Seo.orgid' =>$orgid])->order(['Seo.course_id' => 'ASC'])->toArray();
	$cio=[];
	  foreach($maincoursesGroup as $t=>$u){
	
		$cio[$t]=$u;
	  }
	  $coursesGroup = $this->Course->find('all')->where(['Course.id IN' =>$cio])->group(['Course.cname'])->order(['Course.cname' => 'ASC'])->toArray();
	  $this->set('coursesGroup', $coursesGroup);

}
	public function search(){ 
	
$this->loadModel('Certificate');

//echo "test"; die;

$name = $this->request->data['name'];
$enroll = $this->request->data['enroll'];
$c_issue = $this->request->data['issue'];
$yr = $this->request->data['year'];

$apk = array();

if (!empty($name)) {

	$apk['Certificate.ctitle LIKE']='%'.$name.'%';
}

if (!empty($enroll)) {

	$apk['Certificate.s_enroll LIKE']=$enroll.'%';
}

if (!empty($c_issue)) {

	$apk['Certificate.issue']=$c_issue;
}


if (!empty($yr)) {
	$dates = explode('-', $yr);
	$mon = $dates[0];
	$year = $dates[1];
	$apk['MONTH(Certificate.add_date)']=$mon;
	$apk['YEAR(Certificate.add_date)']=$year;
}

$certificate = $this->Certificate->find('all')->contain(['Course'])->where([$apk])->order(['Certificate.ctitle'=>'ASC']);
$this->set('destination', $this->paginate($certificate)->toarray());


}

	public function add($id=null)
	{
		$this->viewBuilder()->layout('admin');
		$this->loadModel('StudentCourse');
		$this->loadModel('Course');
		$this->loadModel('Certificate');


		$ac1 = $this->Course->find('list', [
			'keyField' => 'cname',
			'valueField' => 'cname',
		  ])->order(['Course.cname' => 'ASC'])->toArray();
		  
	
	
		$this->set('categ1', $ac1);


		$length = 4;

	$randomString1 = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

	$randomString2 = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

	$vcode = $randomString1 . '-' . $randomString2;

		if($id){
        	$newpack = $this->Certificate->get($id);
		}else{
			$newpack = $this->Certificate->newEntity();
		}
		
		$this->set('newpack',$newpack);
		if ($this->request->is(['post', 'put'])) { 	
			

			$certExist= $this->Certificate->find('all')->where(['Certificate.s_id' => $this->request->data['s_id'],'Certificate.course_id' => $this->request->data['course_id']])->first();

		    if($certExist){
			    $this->Flash->error(__('Certificate already Generated'));
				 return $this->redirect(['action' => 'index']);
			}
			$this->request->data['add_date'] = date('Y-m-d');

			$this->request->data['verify_code'] = $vcode;
			$this->request->data['cdetail'] = '';
			$this->request->data['publish'] = '0';
			$this->request->data['admin_type'] = '0';
			$this->request->data['admin_id'] = '0';
			$this->request->data['delivery_date'] = '';
			$this->request->data['comment'] = '';
			$this->request->data['course_includes'] = '';

			if ($this->request->data['s_id'] != '') {

				$stid = $this->request->data['s_id'];
				$course_id = $this->request->data['course_id'];


				$savepack = $this->Certificate->patchEntity($newpack, $this->request->data);
			$results=$this->Certificate->save($savepack);
				if ($results) {

					$conns = ConnectionManager::get('default');
					$query1 ="update tbl_student set cid='1' where s_id=" . $stid;
					$conns->execute($query1);



					$conns = ConnectionManager::get('default');
					$query1 ="update cms_student_courses set certifiacte='1' where s_id=" . $stid . " AND course_id=" . $course_id;
					$conns->execute($query1);

		


					$this->Flash->success(__('The Certificate page is Added successfully.'));
					return $this->redirect(['action' => 'index']);	
				

				}
			} else {
				$savepack = $this->Certificate->patchEntity($newpack, $this->request->data);
				$results=$this->Certificate->save($savepack);
				if ($results) {


					$this->Flash->success(__('The Certificate page is Added successfully.'));
					return $this->redirect(['action' => 'index']);	


				
				}

			}


		
		}
	}
	public function addreq($id=null)
	{
		$this->viewBuilder()->layout('admin');
		$this->loadModel('PlacementRequire');
		$this->loadModel('Placement');


		
		$companyList = $this->Placement->find('list', [
            'keyField' => 'id',
			'valueField' => 'company',
		  ])->where(['status' => 'Y'])->order(['Placement.company' => 'ASC'])->toArray();
		  $this->set(compact('companyList'));
		if($id){
        	$newpack = $this->PlacementRequire->get($id);
		}else{
			$newpack = $this->PlacementRequire->newEntity();
		}
		
		$this->set('newpack',$newpack);
		if ($this->request->is(['post', 'put'])) { 	
			
		
			$date=$this->request->data['created']; //pr($date); die;
  
			$date=date('Y-m-d', strtotime($date)); //die;
		   $this->request->data['created']=$date ;
		   
		   $loc=$this->request->data['location'];
		   $this->request->data['timestamp']=time(); 
		   if($loc==''){
			   
			   $this->request->data['location']='Jaipur';
			 
			   
		   }
		   $this->request->data['comment']='';


			$savepack = $this->PlacementRequire->patchEntity($newpack, $this->request->data);
			$results=$this->PlacementRequire->save($savepack);

     if ($results){
				$this->Flash->success(__('Company Requirement has been updated.'));
				return $this->redirect(['action' => 'requirement']);	
			}else{
				$this->Flash->error(__('Company Requirement not updated please fill your all fields'));
			return $this->redirect(['action' => 'addrfq']);
			}
		}
	}
	public function settings($id=null)
	{
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Seosettings');
if($id){
	$newpack = $this->Seosettings->get($id);

}else{
	$newpack = $this->Seosettings->newEntity();

}
		
		$this->set('newpack',$newpack);
		if ($this->request->is(['post', 'put'])) { 			
			$savepack = $this->Seosettings->patchEntity($newpack, $this->request->data);
			$results=$this->Seosettings->save($savepack);

     if ($results){
				$this->Flash->success(__('Seo Settings has been updated.'));
				return $this->redirect(['action' => 'index']);	
			}else{
				$this->Flash->error(__('Seo Settings  not saved please fill your all fields'));
			return $this->redirect(['action' => 'settings']);
			}
		}
	}

	public function status($id,$status){

		$this->loadModel('Seo');
		if(isset($id) && !empty($id)){
			$product = $this->Seo->get($id);
			$product->status = $status;
			if ($this->Seo->save($product)) {
				$this->Flash->success(__('Seo status has been updated.'));
				return $this->redirect(['action' => 'index']);  
			}
		}
	}
	public function delete($id = null, $stid = '') {
		$this->autoRender=false;
   $this->loadModel('Certificate');
   
   $seotdel = $this->Certificate->get($id);
   if($seotdel){
	   $this->Certificate->deleteAll(['PlacementRequire.id' => $id]); 
	   $this->Certificate->delete($seotdel);
	   $conns = ConnectionManager::get('default');
	   $query1 ="update tbl_student set cid='0' where s_id=" . $stid;
	   $conns->execute($query1);
	   $this->Flash->success(__('Certificate information deleted successfully.'));
		  

		   $this->redirect(array('action' => 'requirement'));

	   }


   }

	
	public function viewdocument($id)
	{
	$this->loadModel('Seo');
		$popupdata = $this->Seo->find('all')->where(['Seo.id'=>$id])->order(['Seo.id'=>DESC]);
		$this->set('popupdata', $this->paginate($popupdata)->toarray());
	}

		public function viewkeywords($id)
	{
	$this->loadModel('Seo');
		$keyworddata = $this->Seo->find('all')->where(['Seo.id'=>$id])->order(['Seo.id'=>DESC]);
		$this->set('keyworddata', $this->paginate($keyworddata)->toarray());
	
	}

	
	public function isAuthorized($user)
	{
		if (isset($user['role_id']) && ($user['role_id'] == 1)) {
			return true;
		}
		return false;
	}
}