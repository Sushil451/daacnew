<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class ContactTable extends Table {

	public function initialize(array $config)
	{
		$this->table('cms_contacts');
		$this->primaryKey('b_id');  
		$this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);     

		 $this->belongsTo('Appuser', [
            'foreignKey' => 'user_id',
        ]);  
	
	}
}
?>
