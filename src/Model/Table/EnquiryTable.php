<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class EnquiryTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tblenquiry');
		$this->primaryKey('enq_id');  

		$this->belongsTo('Course', [
            'foreignKey' => 'course_id'
        ]);  
		
		$this->belongsTo('Mode', [
            'foreignKey' => 'reference'
        ]);     

		$this->belongsTo('Branch', [
            'foreignKey' => 'branch_id'
        ]);     
     
		$this->belongsTo('User', [
            'foreignKey' => 'c_id'
        ]);     

		$this->hasMany('Follow', [
            'foreignKey' => 'enq_id'
        ]);
		
	}

	public function enquiryCount($month,$year,$branch=0){
		if($branch==0){

			return $this->find('all')->where(['MONTH(Enquiry.add_date)'=>$month,'YEAR(Enquiry.add_date)'=>$year])->count();


		}else{

			return $this->find('all')->where(['MONTH(Enquiry.add_date)'=>$month,'YEAR(Enquiry.add_date)'=>$year,'branch'=>$branch])->count();
		
		}
		
	}
	public function enquiryCountYear($branch=0){
		if($branch==0){
		   return  $this->find('count',array('conditions'=>array('YEAR(Enquiry.add_date)'=>date('Y')),'recursive'=>-1));

		   return $this->find('all')->where(['YEAR(Enquiry.add_date)'=>$year])->count();
		}else{

			return $this->find('all')->where(['YEAR(Enquiry.add_date)'=>$year,'branch'=>$branch])->count();


	
		}
		
	}
	public function todayCount($cd,$branch_id=0){
		  if($branch_id==0){

			return $this->find('all')->where(['Enquiry.add_date'=>$cd])->count();


		}else{

			return $this->find('all')->where(['Enquiry.add_date'=>$cd,'branch'=>$branch_id])->count();

		}
	}
}
?>
