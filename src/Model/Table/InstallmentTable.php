<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class InstallmentTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_ins');
		$this->primaryKey('inst_id');  
	

		 $this->belongsTo('Course', [
            'foreignKey' => 'course_id',
        ]);  
	
	}
}
?>
