<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class ExpenseDetailTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_exp_detail_a');
		$this->primaryKey('exd_id');  
	
		 $this->belongsTo('Expense', [
            'foreignKey' => 'ex_id',
        ]);  
		
	}



	function getExpense($branch_id) {

		$articles = $this->ExpenseDetail; 

		$m=$this->find('all')->contain(['Expense'])->select(['sum' => $articles->find('all')->func()->sum('ExpenseDetail.trans_amnt')])->where(['Expense.company'=>$branch_id,'YEAR(ExpenseDetail.trans_date)'=>date('Y'),'Month(trans_date)'=>date('m')])->first();


		$y=$this->find('all')->contain(['Expense'])->select(['sum' => $articles->find('all')->func()->sum('ExpenseDetail.trans_amnt')])->where(['Expense.company'=>$branch_id,'YEAR(ExpenseDetail.trans_date)'=>date('Y')])->first();

		$z=$this->find('all')->contain(['Expense'])->select(['sum' => $articles->find('all')->func()->sum('ExpenseDetail.trans_amnt')])->where(['Expense.company'=>$branch_id,'YEAR(ExpenseDetail.trans_date)'=>date('Y')])->first();

	 
	
		return array('m'=>$m,'y'=>$y,'z'=>$z);
	  }
  
  
	  public function exenpGraph($branch_id=0){
  
	if($branch_id==0)
	{
  
	
$totalInc=$this->find('all')->contain(['Expense'])->select(['sum' => $articles->find('all')->func()->sum('ExpenseDetail.trans_amnt')])->where(['YEAR(ExpenseDetail.trans_date)'=>date('Y')])->group(['Month(ExpenseDetail.trans_date)'])->first();



	
   
  
	}else
	{

		$totReg=$this->find('all')->contain(['Expense'])->select(['sum' => $articles->find('all')->func()->sum('ExpenseDetail.trans_amnt')])->where(['Expense.company'=>$branch_id,'YEAR(ExpenseDetail.trans_date)'=>date('Y')])->group(['Month(ExpenseDetail.trans_date)'])->first();


	$totReg=$User->find('all',array('conditions'=>array('YEAR(Student.add_date)'=>date('Y'),'branch'=>$branch_id),'fields'=>'sum(reg_fees)','group' => 'Month(Student.add_date)','recursive'=>-1));
	$totalInc_m=  $this->find('all',array('conditions'=>array('YEAR(StudentInstallment.pay_date)'=>date('Y'),'branch'=>$branch_id),'fields'=>'sum(amount)','group' => 'Month(StudentInstallment.pay_date)','recursive'=>-1));
  
	}
	for($i=1;$i<=12;$i++)
	{
	$cash[$i]=$totalInc[$i-1][0]['sum(trans_amnt)'];
	$fin[$i]=$cash[$i] ;
		 
	}
	  return $fin;
  }
	  
  
	   public function lastexenpGraph($branch_id=0){
  
	if($branch_id==0)
	{
  
	
  
	$totalInc=$this->find('all',array('conditions'=>array('YEAR(ExpenseDetail.trans_date)'=>date('Y',strtotime("-1 year"))),'fields'=>'sum(trans_amnt)','group' => 'Month(ExpenseDetail.trans_date)','recursive'=>-1));
  
	
   
  
	}else
	{
	 $totalInc=$this->find('all',array('fields'=>'sum(trans_amnt)','conditions'=>array('Expense.company'=>$branch_id,'YEAR(trans_date)'=>date('Y',strtotime("-1 year")),'Month(trans_date)'=>date('m')),'group' => 'Month(ExpenseDetail.trans_date)','recursive'=>1));
  
	}
	for($i=1;$i<=12;$i++)
	{
	$cash[$i]=$totalInc[$i-1][0]['sum(trans_amnt)'];
	$fin[$i]=$cash[$i] ;
		 
	}
	  return $fin;
  }
	  
  
	  public function currentexenpGraph($branch_id=0){
  
	if($branch_id==0)
	{
  
	
  
	$totalInc=$this->find('all',array('conditions'=>array('YEAR(ExpenseDetail.trans_date)'=>date('Y')),'fields'=>'sum(trans_amnt)','group' => 'Month(ExpenseDetail.trans_date)','recursive'=>-1));
   
	
   
  
	}else
	{
	 $totalInc=$this->find('all',array('fields'=>'sum(trans_amnt)','conditions'=>array('Expense.company'=>$branch_id,'YEAR(trans_date)'=>date('Y'),'Month(trans_date)'=>date('m')),'group' => 'Month(ExpenseDetail.trans_date)','recursive'=>1));
  
	}
	for($i=1;$i<=12;$i++)
	{
	$cash[$i]=$totalInc[$i-1][0]['sum(trans_amnt)'];
	$fin[$i]=$cash[$i] ;
		 
	}
	  return $fin;
  }
}
?>
