<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class AppuserTable extends Table {

	public function initialize(array $config)
	{
		$this->table('cms_users');
	
		$this->belongsTo('Branch', [
            'foreignKey' => 'branch'
        ]);     

	
	}
}
?>
