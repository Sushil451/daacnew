<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class ArchiveEnquiryTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_archive_enquiry');
		$this->primaryKey('enq_id');  
	
		$this->belongsTo('Course', [
            'foreignKey' => 'course_id'
        ]);  
		
		$this->belongsTo('Mode', [
            'foreignKey' => 'reference'
        ]);     

		$this->belongsTo('Branch', [
            'foreignKey' => 'branch_id'
        ]);     
     
		$this->belongsTo('User', [
            'foreignKey' => 'c_id'
        ]);     

		$this->hasMany('ArchiveFollow', [
            'foreignKey' => 'enq_id'
        ]);
		
		
	}
}
?>
