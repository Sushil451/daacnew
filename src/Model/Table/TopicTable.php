<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class TopicTable extends Table {

	public function initialize(array $config)
	{
		$this->table('cms_topics');
		$this->primaryKey('id');  
		
	}
}
?>