<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class BranchTable extends Table {

	public function initialize(array $config)
	{
		$this->table('branches');
		$this->primaryKey('id');         
	}
}
?>
