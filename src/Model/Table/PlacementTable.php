<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class PlacementTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_placement');
		$this->primaryKey('id');  
	
	}
}
?>