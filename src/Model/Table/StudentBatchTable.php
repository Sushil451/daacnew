<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class StudentBatchTable extends Table {

	public function initialize(array $config)
	{
		$this->table('cms_studentbatch');
		$this->primaryKey('id');     
		
		$this->belongsTo('Students', [
            'foreignKey' => 's_id',
        ]); 

		$this->belongsTo('QuestionCategory', [
            'foreignKey' => 'sub_id',
        ]); 
		$this->belongsTo('Branch', [
            'foreignKey' => 'branch_id',
        ]);  
		$this->belongsTo('Batch', [
            'foreignKey' => 'batch_id',
        ]);  
	
	}
}
?>
