<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class BatchTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_batch');
		$this->primaryKey('b_id');  
		$this->belongsTo('Branch', [
            'foreignKey' => 'branch_id',
            'joinType' => 'INNER',
        ]);     

		 $this->belongsTo('Course', [
            'foreignKey' => 'course_id',
        ]);  
		$this->belongsTo('Users', [
            'foreignKey' => 'faculty',
        ]);  
		$this->belongsTo('QuestionCategory', [
            'foreignKey' => 'q_cat',
        ]);  
		$this->hasMany('StudentCourse', [
            'foreignKey' => 'b_id',
        ]);  
	}
}
?>
