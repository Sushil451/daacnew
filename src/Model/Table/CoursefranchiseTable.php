<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class CoursefranchiseTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tblfranchise');
		$this->primaryKey('id');         
	}
}
?>
