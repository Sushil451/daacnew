<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class StudentCourseTable extends Table {

	public function initialize(array $config)
	{
		$this->table('cms_student_courses');
		$this->primaryKey('id');     
		
		$this->belongsTo('Students', [
            'foreignKey' => 's_id',
        ]); 

		$this->belongsTo('Course', [
            'foreignKey' => 'course_id',
        ]); 
		$this->belongsTo('Branch', [
            'foreignKey' => 'branch_id',
        ]);  
		$this->belongsTo('Batch', [
            'foreignKey' => 'b_id',
        ]);  
	
	}


	public function dropCount($branch_id=0){
		if($branch_id==0){

		    return $this->find('all')->where(['StudentCourse.drop_out'=>1,'YEAR(StudentCourse.drop_date)'=>date('Y')])->count();
			
		}else{

			return $this->find('all')->where(['StudentCourse.drop_out'=>1,'YEAR(StudentCourse.drop_date)'=>date('Y'),'StudentCourse.branch'=>$branch_id])->count();

		}
		
	}
	public function totaldropCount($branch_id=0){
		if($branch_id==0){

			return $this->find('all')->where(['StudentCourse.drop_out'=>1])->count();


		}else{

			return $this->find('all')->where(['StudentCourse.drop_out'=>1,'StudentCourse.branch'=>$branch_id])->count();

		}
		
	}
	public function feeTotalAll($branch_id=0){
		  if($branch_id==0){

			return $this->find('all')->where(['StudentCourse.drop_out'=>1,'StudentCourse.branch'=>$branch_id])->count();

			
  $sum=$this->find('first',array('fields'=>'sum(StudentCourse.fee)','conditions'=>array('StudentCourse.drop_out'=>1),'recursive'=>1));
}else{
 $sum= $this->find('first',array('fields'=>'sum(StudentCourse.fee)','conditions'=>array('StudentCourse.drop_out'=>1,'StudentCourse.branch'=>$branch_id),'recursive'=>1));
}
return $sum[0]['sum(`StudentCourse`.`fee`)'];

	}
	public function feeTotalYear($branch_id=0){
		  if($branch_id==0){
  $sum=$this->find('first',array('fields'=>'sum(StudentCourse.fee)','conditions'=>array('StudentCourse.drop_out'=>1,'YEAR(StudentCourse.drop_date)'=>date('Y')),'recursive'=>1));
}else{
 $sum= $this->find('first',array('fields'=>'sum(StudentCourse.fee)','conditions'=>array('StudentCourse.drop_out'=>1,'YEAR(StudentCourse.drop_date)'=>date('Y'),'StudentCourse.branch'=>$branch_id),'recursive'=>1));
}
return $sum[0]['sum(`StudentCourse`.`fee`)'];

	}
}
?>
