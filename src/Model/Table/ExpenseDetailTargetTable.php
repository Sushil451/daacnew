<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class ExpenseDetailTargetTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_exp_detail_t');
		$this->primaryKey('exd_id');  
	
		 $this->belongsTo('ExpenseCategory', [
            'foreignKey' => 'ec_id',
        ]);  
		
		
	}


	public function month($branch_id=0){
		if($branch_id!=0) {



		$articles = $this->ExpenseDetailTarget; 

		return $this->find('all')->select(['sum' => $articles->find('all')->func()->sum(date('F'))])->where(['comp_id'=>$branch_id,'YEAR(add_date)'=>date('Y')])->first();

	}else{


		$articles = $this->ExpenseDetailTarget; 

		return $this->find('all')->select(['sum' => $articles->find('all')->func()->sum(date('F'))])->where(['YEAR(add_date)'=>date('Y')])->first();



		
	
	}

	}

	public function target_expense($branch_id=0){
		if($branch_id==0){

			return $this->find('all')->where(['YEAR(add_date)'=>date('Y'),'MONTH(add_date)'=>date('m')])->toarray();
		
		}
		else{

			return $this->find('all')->where(['YEAR(add_date)'=>date('Y'),'MONTH(add_date)'=>date('m'),'comp_id'=>$branch_id])->toarray();

		
		}

	}


	 public function currentexetargetnpGraph($branch_id=0){

  if($branch_id==0){
  	
return $this->find('all')->where(['YEAR(add_date)'=>date('Y')])->toarray();
}else{
return $this->find('all')->where(['YEAR(add_date)'=>date('Y'),'comp_id'=>$branch_id])->toarray();
}
}
}
?>
