<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class ExpenseTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_expenses');
		$this->primaryKey('ex_id');  
	
		 $this->belongsTo('ExpenseCategory', [
            'foreignKey' => 'ec_id',
        ]);  
		 $this->belongsTo('Branch', [
            'foreignKey' => 'company',
        ]);  
		
	}
}
?>
