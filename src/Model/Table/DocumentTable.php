<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\FallbackPasswordHasher;

/**
 * Users Model
 *
 * @property \App\Model\Table\CountriesTable&\Cake\ORM\Association\BelongsTo $Countries
 * @property \App\Model\Table\RolesTable&\Cake\ORM\Association\BelongsTo $Roles
 * @property \App\Model\Table\CurrenciesTable&\Cake\ORM\Association\BelongsTo $Currencies
 * @property \App\Model\Table\TimezonesTable&\Cake\ORM\Association\BelongsTo $Timezones
 * @property \App\Model\Table\LanguagesTable&\Cake\ORM\Association\BelongsTo $Languages
 * @property \App\Model\Table\CartsTable&\Cake\ORM\Association\HasMany $Carts
 * @property \App\Model\Table\NotificationsTable&\Cake\ORM\Association\HasMany $Notifications
 * @property \App\Model\Table\OrderDetailsTable&\Cake\ORM\Association\HasMany $OrderDetails
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 * @property \App\Model\Table\PermissionModuleTable&\Cake\ORM\Association\HasMany $PermissionModule
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DocumentTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tbl_documents');
        $this->setPrimaryKey('id');

      //  $this->addBehavior('Timestamp');

        
        $this->belongsTo('CategoryDocument', [
            'foreignKey' => 'category',
            'joinType' => 'INNER',
        ]);
       
        // $this->hasMany('PermissionModule', [
        //     'foreignKey' => 'user_id',
        // ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    // public function validationDefault(Validator $validator)
    // {
    //     $validator
    //         ->integer('id')
    //         ->allowEmptyString('id', null, 'create');

    //     $validator
    //         ->scalar('name')
    //         ->maxLength('name', 191)
    //         ->requirePresence('name', 'create')
    //         ->notEmptyString('name');

    //     $validator
    //         ->scalar('mobile')
    //         ->maxLength('mobile', 250)
    //         ->requirePresence('mobile', 'create')
    //         ->notEmptyString('mobile');

    //     $validator
    //         ->scalar('image')
    //         ->maxLength('image', 191)
    //         ->allowEmptyFile('image');

    //     $validator
    //         ->email('email')
    //         ->allowEmptyString('email');

    //     $validator
    //         ->scalar('password')
    //         ->maxLength('password', 191)
    //         ->allowEmptyString('password');

    //     $validator
    //         ->scalar('conferm_pass')
    //         ->maxLength('conferm_pass', 250)
    //         ->allowEmptyString('conferm_pass');

    //     $validator
    //         ->boolean('status')
    //         ->notEmptyString('status');

    //     $validator
    //         ->scalar('facebook')
    //         ->maxLength('facebook', 250)
    //         ->allowEmptyString('facebook');

    //     $validator
    //         ->scalar('instagram')
    //         ->maxLength('instagram', 250)
    //         ->allowEmptyString('instagram');

    //     $validator
    //         ->scalar('youtube')
    //         ->maxLength('youtube', 250)
    //         ->allowEmptyString('youtube');

    //     $validator
    //         ->scalar('twitter')
    //         ->maxLength('twitter', 250)
    //         ->allowEmptyString('twitter');

    //     $validator
    //         ->scalar('address')
    //         ->maxLength('address', 250)
    //         ->allowEmptyString('address');

    //     $validator
    //         ->scalar('brand_logo')
    //         ->allowEmptyString('brand_logo');

    //     $validator
    //         ->scalar('icon')
    //         ->allowEmptyString('icon');

    //     $validator
    //         ->scalar('market_place_name')
    //         ->maxLength('market_place_name', 250)
    //         ->allowEmptyString('market_place_name');

    //     $validator
    //         ->scalar('slogan')
    //         ->maxLength('slogan', 250)
    //         ->allowEmptyString('slogan');

    //     $validator
    //         ->integer('worldwide_business_area')
    //         ->allowEmptyString('worldwide_business_area');

    //     $validator
    //         ->scalar('legal_name')
    //         ->maxLength('legal_name', 250)
    //         ->allowEmptyString('legal_name');

    //     return $validator;
    // }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    // public function buildRules(RulesChecker $rules)
    // {
    //     $rules->add($rules->isUnique(['email']));
    //     $rules->add($rules->existsIn(['country_id'], 'Countries'));
    //     $rules->add($rules->existsIn(['role_id'], 'Roles'));
    //     $rules->add($rules->existsIn(['currency_id'], 'Currencies'));
    //     $rules->add($rules->existsIn(['timezone_id'], 'Timezones'));
    //     $rules->add($rules->existsIn(['language_id'], 'Languages'));

    //     return $rules;
    // }

    public function identify($formData) {

        $passOk = false;

       
	$user = $this->find()->hydrate(false)->where(['username' => $formData['email'],'status' => 'Y'])->first();

  
	$checker = new FallbackPasswordHasher; 
        if(!is_null($passOk))
            $passOk = $checker->check($formData['password'], $user['password']); 

        return $passOk ? $user  : null;

    }

}
