<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Emailtemplate Model
 *
 * @property &\Cake\ORM\Association\BelongsTo $Roles
 *
 * @method \App\Model\Entity\Emailtemplate get($primaryKey, $options = [])
 * @method \App\Model\Entity\Emailtemplate newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Emailtemplate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Emailtemplate|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Emailtemplate saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Emailtemplate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Emailtemplate[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Emailtemplate findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class EmailtemplatesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('emailtemplates');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    // public function validationDefault(Validator $validator)
    // {
    //     $validator
    //         ->integer('id')
    //         ->allowEmptyString('id', null, 'create');

    //     $validator
    //         ->scalar('title')
    //         ->maxLength('title', 200)
    //         ->requirePresence('title', 'create')
    //         ->notEmptyString('title');

    //     $validator
    //         ->scalar('fromemail')
    //         ->maxLength('fromemail', 50)
    //         ->requirePresence('fromemail', 'create')
    //         ->notEmptyString('fromemail');

    //     $validator
    //         ->scalar('subject')
    //         ->maxLength('subject', 200)
    //         ->requirePresence('subject', 'create')
    //         ->notEmptyString('subject');

    //     $validator
    //         ->scalar('adminemail')
    //         ->maxLength('adminemail', 50)
    //         ->requirePresence('adminemail', 'create')
    //         ->notEmptyString('adminemail');

    //     $validator
    //         ->scalar('format')
    //         ->requirePresence('format', 'create')
    //         ->notEmptyString('format');

    //     $validator
    //         ->scalar('status')
    //         ->notEmptyString('status');

    //     return $validator;
    // }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
       // $rules->add($rules->existsIn(['role_id'], 'Roles'));

        return $rules;
    }
}
