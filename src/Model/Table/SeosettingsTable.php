<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class SeosettingsTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_seosettings');
		$this->primaryKey('id');         
	}
}
?>
