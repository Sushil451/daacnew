<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class DiscountTable extends Table {

	public function initialize(array $config)
	{
		$this->table('discounts');
		$this->primaryKey('id');  
		$this->belongsTo('Branch', [
            'foreignKey' => 'branch_id'
        ]);     

		
	}
}
?>
