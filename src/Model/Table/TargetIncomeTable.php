<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class TargetIncomeTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_income_t');
		$this->primaryKey('i_id');  
		$this->belongsTo('Course', [
			'foreignKey' => 'crs_id',
		]);  
		   
	}

	public function regTargetYear($branch_id){
		$count=$this->query('select sum(m1)+sum(m2)+sum(m3)+sum(m4)+sum(m5)+sum(m6)+sum(m7)+sum(m8)+sum(m9)+sum(m10)+sum(m11)+sum(m12) as sigma from tbl_income_t where branch='.$branch_id.' and year(add_date)='.date('Y'));
	    return $count[0][0]['sigma'];
	}
}
?>
