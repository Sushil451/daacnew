<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Customers Model
 *
 * @property \App\Model\Table\DropshipersTable&\Cake\ORM\Association\BelongsTo $Dropshipers
 * @property \App\Model\Table\OrderDetailsTable&\Cake\ORM\Association\HasMany $OrderDetails
 * @property \App\Model\Table\RatingsTable&\Cake\ORM\Association\HasMany $Ratings
 *
 * @method \App\Model\Entity\Customer get($primaryKey, $options = [])
 * @method \App\Model\Entity\Customer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Customer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Customer|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Customer saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Customer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Customer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Customer findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StudentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tbl_student');
        $this->setDisplayField('name');
        $this->setPrimaryKey('s_id');

       // $this->addBehavior('Timestamp');
       $this->belongsTo('Branch', [
        'foreignKey' => 'branch_id',
    ]);  
       
        $this->belongsTo('Course', [
            'foreignKey' => 'course_id',
        ]);
        $this->belongsTo('Batch', [
            'foreignKey' => 'b_id',
        ]);
        $this->hasMany('StudentInstallment', [
            'foreignKey' => 's_id',
        ]);

        $this->hasMany('Reportdiscount', [
            'foreignKey' => 's_id',
        ]);
        $this->hasMany('StudentCourse', [
            'foreignKey' => 's_id',
        ]);

        $this->hasMany('StudentBatch', [
            'foreignKey' => 's_id',
        ]); 

        
    }

    

    public function regPresentMonth($branch_id=0){
		if($branch_id!=0) {

            $count=$this->find('all')->where(['MONTH(Students.add_date)'=>date('m'),'YEAR(Students.add_date)'=>date('Y'),'Students.branch_id'=>$branch_id])->order(['Students.s_id' => 'DESC'])->count();

	
	}else {

        $count=$this->find('all')->where(['MONTH(Students.add_date)'=>date('m'),'YEAR(Students.add_date)'=>date('Y')])->order(['Students.s_id' => 'DESC'])->count();

	}
	return $count;
	}
	public function regYear($branch_id=0){
		if($branch_id!=0) {

            $count=$this->find('all')->where(['YEAR(Students.add_date)'=>date('Y'),'Students.branch_id'=>$branch_id])->order(['Students.s_id' => 'DESC'])->count();


		
	}else {

        $count=$this->find('all')->where(['YEAR(Students.add_date)'=>date('Y')])->order(['Students.s_id' => 'DESC'])->count();

		
	}
	return $count;
	}
	public function total($branch_id=0){
		 if($branch_id==0){



            return $this->find('all')->where(['Students.is_placed'=>'Y'])->order(['Students.s_id' => 'DESC'])->count();


			
		   }else{

     return $this->find('all')->where(['Students.is_placed'=>'Y','Students.branch_id'=>$branch_id])->order(['Students.s_id' => 'DESC'])->count();

		
		   }
	}


		public function totalplaced($branch_id=0){
		 if($branch_id==0){

            return $this->find('all')->where(['Students.is_placed'=>'D'])->order(['Students.s_id' => 'DESC'])->count();


			
		   }else{

            return $this->find('all')->where(['Students.is_placed'=>'D','Students.branch_id'=>$branch_id])->order(['Students.s_id' => 'DESC'])->count();


		   }
	}


	

	

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    // public function validationDefault(Validator $validator)
    // {
    //     $validator
    //         ->integer('id')
    //         ->allowEmptyString('id', null, 'create');

    //     $validator
    //         ->scalar('name')
    //         ->maxLength('name', 250)
    //         ->requirePresence('name', 'create')
    //         ->notEmptyString('name');

    //     $validator
    //         ->email('email')
    //         ->requirePresence('email', 'create')
    //         ->notEmptyString('email');

    //     $validator
    //         ->scalar('mobile')
    //         ->maxLength('mobile', 11)
    //         ->requirePresence('mobile', 'create')
    //         ->notEmptyString('mobile');

    //     $validator
    //         ->scalar('address')
    //         ->allowEmptyString('address');

    //     $validator
    //         ->notEmptyString('status');

    //     return $validator;
    // }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    // public function buildRules(RulesChecker $rules)
    // {
    //    $rules->add($rules->isUnique(['email']));
    //    $rules->add($rules->existsIn(['dropshiper_id'], 'Dropshipers'));

    //    return $rules;
    // }
}
