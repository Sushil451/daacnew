<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class ModeTable extends Table {

	public function initialize(array $config)
	{
		$this->table('modes');
		$this->primaryKey('id');  
		
	}
}
?>
