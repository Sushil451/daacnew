<?php

namespace App\Model\Table;
use Cake\ORM\Table;

class QuestionCategoryTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_question_category');
		$this->primaryKey('qc_id');  
		
	}
}
?>
