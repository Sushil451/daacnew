<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class SeoTable extends Table {

	public function initialize(array $config)
	{
		$this->table('cms_seos');
		$this->primaryKey('id');         
	}
}
?>
