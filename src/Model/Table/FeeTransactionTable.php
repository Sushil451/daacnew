<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class FeeTransactionTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_fee_transaction');
		$this->primaryKey('rec_id');  
	

		 $this->belongsTo('Students', [
            'foreignKey' => 's_id',
        ]);  
	
	}
}
?>
