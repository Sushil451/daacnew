<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class ReportdiscountTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_reportdiscount');
		$this->primaryKey('id');  
		$this->belongsTo('Students', [
            'foreignKey' => 's_id'
        ]);     
	}
}
?>
