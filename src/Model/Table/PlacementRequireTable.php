<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class PlacementRequireTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_placement_require');
		$this->primaryKey('id');  
		$this->belongsTo('Placement', [
            'foreignKey' => 'p_id'
        ]);     

		
	}
}
?>
