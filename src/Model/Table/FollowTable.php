<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class FollowTable extends Table {

	public function initialize(array $config)
	{
		$this->table('followup');
		$this->primaryKey('f_id');  

		$this->belongsTo('Enquiry', [
            'foreignKey' => 'enq_id'
        ]);  
		
		
	}


	

	  public function monthCount($m,$y,$branch_id=0){
		if($branch_id==0){

			return $this->find('all')->where(['MONTH(Follow.created)'=>$m,'YEAR(Follow.created)'=>$y,'Follow.called'=>0])->count();


		}else{

			return $this->find('all')->where(['MONTH(Follow.created)'=>$m,'YEAR(Follow.created)'=>$y,'Enquiry.branch'=>$branch_id,'Follow.called'=>0])->count();

	
		
		}
		
	}

	public function yearCount($branch_id=0){
		if($branch_id==0){

			return $this->find('all')->where(['YEAR(Follow.created)'=>$y,'Follow.called'=>0])->count();


		}else{

			return $this->find('all')->where(['YEAR(Follow.created)'=>$y,'Enquiry.branch'=>$branch_id,'Follow.called'=>0])->count();

	
		
		}
		
	}

	public function todayCount($cd,$branch_id=0){
		if($branch_id==0){

	return $this->find('all')->where(['DATE(Follow.created)'=>$cd,'Follow.called'=>0])->count();
	  }else{

	return $this->find('all')->where(['DATE(Follow.created)'=>$cd,'Enquiry.branch'=>$branch_id,'Follow.called'=>0])->count();

	
	  }
  }
	
}
?>
