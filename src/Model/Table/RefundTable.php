<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class RefundTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_refund');
		$this->primaryKey('id');  
		
	}
}
?>
