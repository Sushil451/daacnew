<?php

namespace App\Model\Table;
use Cake\ORM\Table;

class QuestionTable extends Table {

	public function initialize(array $config)
	{

	
		$this->table('tbl_question');
		$this->primaryKey('q_id');  
		
	}
}
?>
