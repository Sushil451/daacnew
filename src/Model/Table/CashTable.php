<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class CashTable extends Table {

	public function initialize(array $config)
	{
		$this->table('cms_cashes');
		$this->primaryKey('id');  
	
		$this->belongsTo('Branch', [
            'foreignKey' => 'branch_id'
        ]);     
		$this->belongsTo('Students', [
            'foreignKey' => 's_id'
        ]);
		 $this->belongsTo('Course', [
            'foreignKey' => 'cid',
        ]);  
		
	}
}
?>
