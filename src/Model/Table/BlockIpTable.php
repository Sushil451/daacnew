<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class BlockIpTable extends Table {

	public function initialize(array $config)
	{
		$this->table('block_ips');
		$this->primaryKey('id');  
	}
}
?>
