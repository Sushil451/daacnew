<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class CertificateTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tblcertificates');
		$this->primaryKey('cid');  
	
		 $this->belongsTo('Course', [
            'foreignKey' => 'course_id',
        ]);  
		
	}
}
?>
