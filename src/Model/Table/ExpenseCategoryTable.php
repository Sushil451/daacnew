<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class ExpenseCategoryTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_exp_cat');
		$this->primaryKey('ec_id');  
	
		 $this->hasMany('Expense', [
            'foreignKey' => 'ec_id',
        ]);  
		
		
	}
}
?>
