<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class StudentPortfolioTable extends Table {

	public function initialize(array $config)
	{
		$this->table('cms_student_portfolio');
		$this->belongsTo('Users', [
            'foreignKey' => 'u_id',
        ]);      
	}
}
?>
