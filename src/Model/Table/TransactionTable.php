<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class TransactionTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_ledger');
		$this->primaryKey('id');  
		$this->belongsTo('Bank', [
            'foreignKey' => 'bank_id',
        ]);

		$this->belongsTo('Cash', [
            'foreignKey' => 'tran_id',
        ]);

		$this->belongsTo('ExpenseDetail', [
            'foreignKey' => 'tran_id',
        ]);
	}
}
?>
