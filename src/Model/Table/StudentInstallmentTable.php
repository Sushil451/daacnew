<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class StudentInstallmentTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_student_installment');
		$this->primaryKey('sid_id');  
	

		 $this->belongsTo('Students', [
            'foreignKey' => 's_id',
        ]);  
		$this->belongsTo('Installment', [
            'foreignKey' => 'inst_id',
        ]);  
		$this->belongsTo('Course', [
            'foreignKey' => 'course_id',
        ]);  

		$this->belongsTo('Branch', [
            'foreignKey' => 'branch_id',
        ]);  
	
	}
}
?>
