<?php
namespace App\Model\Table;
use Cake\ORM\Table;

class ResultTable extends Table {

	public function initialize(array $config)
	{
		$this->table('tbl_exam_result');
		$this->primaryKey('id');  

		$this->belongsTo('ExamSetup', [
            'foreignKey' => 'e_id'
        ]);  
		
		
	}


	
}
?>
