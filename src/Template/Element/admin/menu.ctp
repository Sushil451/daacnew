<?php $role_id=$this->request->session()->read('Auth.User.role_id'); ?>
<?php $user = $this->request->session()->read('Auth.User');  ?> 

  <style type="text/css">
    .skin-blue .treeview-menu>li>a> svg path {fill: #fff;}
    .main-sidebar {width: 180px !important;}
    .treeview-menu, li.treeview span {width: 177px !important;}
    .content-wrapper, .right-side, .main-footer {margin-left: 177px;}
    .main-header .logo {width: 164px;}
  </style>
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
        </div>
        <div class="pull-left info">
          <p><?php echo ucfirst($this->request->session()->read('Auth.User.email'));?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li> 
        <li class="<?php if($this->request->params['controller'] == 'Dashboards'){ echo 'active';} ?> treeview">
          <a href="<?php echo $this->Url->build('/admin/dashboards/'); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>

        <li class="treeview">
         <a href="<?php echo ADMIN_URL;?>batch/index">
           <i class="fa fa-user-plus"></i> <span>Batch</span>
         </a>
       </li>
        <li class="treeview manage dropTreeVLi">
        <a href="#" class="dropTreeVa">
          <i class="fa fa-tags"></i> <span>
            Enquiry Manager</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu dropTreeVUl " >


     
       <li class="treeview">
       <a href="<?php echo ADMIN_URL;?>enquiry/index">
           <i class="fa fa-file-o"></i> <span>Follow Up</span>
         </a>
         </li>

        <li class="treeview">
        <a href="<?php echo ADMIN_URL;?>enquiry/closed">
           <i class="fa fa-book"></i> <span>Closed</span>
         </a>
       </li>

      
       </ul>
     </li>

     <li class="treeview">
         <a href="<?php echo ADMIN_URL;?>students/index">
           <i class="fa fa-user-plus"></i> <span>Students</span>
         </a>
       </li>
       <li class="treeview">
       <a href="<?php echo ADMIN_URL;?>fees/discount">
           <i class="fa fa-inr"></i> <span>Fee Discount Manager</span>
         </a>
       </li>

       <li class="treeview manage dropTreeVLi">
        <a href="#" class="dropTreeVa">
          <i class="fa fa-tags"></i> <span>Master Manager</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu dropTreeVUl " >


     
       <li class="treeview">
       <a href="<?php echo ADMIN_URL;?>document/index">
           <i class="fa fa-file-o"></i> <span>Document Manager</span>
         </a>
         </li>

        <li class="treeview">
        <a href="<?php echo ADMIN_URL;?>course/index">
           <i class="fa fa-book"></i> <span>Course Manager</span>
         </a>
       </li>

       <li class="treeview">
       <a href="<?php echo ADMIN_URL;?>installment/index">
           <i class="fa fa-exchange"></i> <span>Installment Manager</span>
         </a>
       </li>
       
       <li class="treeview">
       <a href="<?php echo ADMIN_URL;?>staticpages/index">
           <i class="fa fa-exchange"></i> <span>Static Manager</span>
         </a>
       </li>

       <li class="treeview">
       <a href="<?php echo ADMIN_URL;?>seo/index">
           <i class="fa fa-exchange"></i> <span>Seo Manager</span>
         </a>
       </li>
       </ul>
     </li>
</ul>
</section>
</aside>


<script>
$(document).ready(function(){
  $('.dropTreeVLi').click(function(e) {
      //e.preventDefault();
      if ($(this).siblings('li').find('ul.dropTreeVUl:visible').length) {
        $('ul.dropTreeVUl').slideUp('normal');
      }
      $(this).find('ul.dropTreeVUl').slideToggle('normal'); // show the respective one.
    });
 
});
</script>
