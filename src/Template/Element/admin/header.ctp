<style type="text/css">
  .skin-blue .treeview-menu>li>a> svg path {fill: #fff;}
  .navbar-nav {float: none;margin: 0;}
.form-horizontal .control-label {padding-top: 15px !important;}
header{-webkit-box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.18);
-moz-box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.18);
box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.18);}
</style>
<?php $user_id=$this->request->session()->read('Auth.User.id'); ?>
<?php $userdetail=$this->request->session()->read('Auth.User'); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="<?php echo SITE_URL;?>favicon.ico" type="image/x-icon">
  <link rel="icon" href="<?php echo SITE_URL;?>css/admin/css/all.min.css" type="image/x-icon">
  <title>DAAC</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
  <!-- Latest compiled and minified CSS -->
<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous"> -->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  
  <?= $this->Html->css('admin/dataTables.bootstrap.css') ?>
  <?= $this->Html->css('admin/AdminLTE.min.css') ?>
  <?= $this->Html->css('admin/skins/_all-skins.min.css') ?>
  <?= $this->Html->css('admin/style.css') ?>


  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
 <script src="<?php echo SITE_URL;?>js/datetimepicker_ra.js"></script>

  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


  


</head>
<style>.table .thead-dark th {
    color: #fff;
    background-color: #212529;
    border-color: #32383e;
}
  </style>
<!-- <script>
  var csrfToken = <?php //echo json_encode($this->request->params["_csrfToken"]); ?>;
</script> -->

  <body class="hold-transition skin-blue sidebar-mini ">
  <!-- sidebar-collapse -->
    <div class="wrapper">
      <header class="main-header">
        <div class="logo toggleLogoDv">
          <!-- <span class="logo-mini"> -->
          <a href="#" class="sidebar-toggle" >
            <span class="sr-only">Toggle navigation</span>
          </a>
          
 <!--   <span class="logo-lg">
   <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a> -->
  
        </div>
        <style>		 
.skin-blue .main-header .navbar .nav>li>a i {min-height:1px;margin-top: 13px;font-size: 23px;}
</style>	

        <nav class="navbar navbar-static-top d-flex justify-content-between" style="padding-left: 15px !important;"> 
        <img  src="<?php echo SITE_URL; ?>images/daacLogo.png"></span>


<ul class="list-inline mnMenuUlAdd">  
  <li class="list-inline-item">
    <a href="<?php echo SITE_URL; ?>admin/students/add">
    <img style="width: 128px;" src="<?php echo SITE_URL; ?>images/reg-stu.png"><br>Register
    </a>
  </li>

  <li class="list-inline-item">
    <a href="<?php echo SITE_URL; ?>admin/students/index">
    <img style="width: 128px;" src="<?php echo SITE_URL; ?>images/dep-fee.png"><br>Student
    </a>
  </li>

  <li class="list-inline-item">
    <a href="<?php echo SITE_URL; ?>admin/fees/discount">
    <img style="width: 128px;" src="<?php echo SITE_URL; ?>images/new-inc.png"><br>Fees
    </a>
  </li>

  <li class="list-inline-item">
    <a href="<?php echo SITE_URL; ?>admin/enquiry/index">
    <img style="width: 128px;" src="<?php echo SITE_URL; ?>images/followup.png"><br>Enquiry
    </a>
  </li>

  <li class="list-inline-item">
    <a href="javascript:void(0);">
    <img style="width: 128px;" src="<?php echo SITE_URL; ?>images/new-inc.png"><br>Reports
    </a>
  </li>

  <li class="list-inline-item">
    <a href="<?php echo SITE_URL; ?>admin/placement/index">
    <img style="width: 128px;" src="<?php echo SITE_URL; ?>images/lst-cmny.png"><br>Comp.
    </a>
  </li>

  <li class="list-inline-item">
    <a href="<?php echo SITE_URL; ?>admin/placement/requirement">
    <img style="width: 128px;" src="<?php echo SITE_URL; ?>images/pst-req.png"><br>Req.
    </a>
  </li>

  <li class="list-inline-item">
    <a href="javascript:void(0);">
    <img style="width: 128px;" src="<?php echo SITE_URL; ?>images/unnamed.png"><br>Document
    </a>
  </li>

  <li class="list-inline-item">
    <a href="<?php echo SITE_URL; ?>admin/fees/submit">
    <img style="width: 128px;" src="<?php echo SITE_URL; ?>images/fee_submit_director.png"><br>Payment
    </a>
  </li>
<!--
  <li class="list-inline-item">
    <a href="javascript:void(0);">
    <img style="width: 128px;" src="<?php echo SITE_URL; ?>images/sms_manager.png"><br>SMS
    </a>
  </li>-->

  <li class="list-inline-item">
    <a href="<?php echo ADMIN_URL;?>certificate/index">
    <img style="width: 128px;" src="<?php echo SITE_URL; ?>images/certificate.png"><br>Certificate
    </a>
  </li>

  <li class="list-inline-item">
    <a href="<?php echo SITE_URL; ?>admin/exam_setup">
    <img style="width: 128px;" src="<?php echo SITE_URL; ?>images/exam.png"><br>Exam
    </a>
  </li>

  <li class="list-inline-item">
    <a href="<?php echo SITE_URL; ?>admin/batch/index">
    <img style="width: 128px;" src="<?php echo SITE_URL; ?>images/batch.png"><br>Batch
    </a>
  </li>


  
</ul>

          <div class="navbar-custom-menu">



            <ul class="nav navbar-nav">



<?php /*

            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo SITE_URL;?>/img/user2-160x160.jpg" class="user-image" alt="User Image">
                <span class="hidden-xs">
                  <?php echo ucfirst($this->request->session()->read('Auth.User.name'));?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="user-header">
                    <img src="<?php echo SITE_URL;?>/img/user2-160x160.jpg" class="user-image" alt="User Image">
                    <p>
                      <?php echo ucfirst($this->request->session()->read('Auth.User.email'));?>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo $this->Url->build('/admin/users/add/'.$user_id); ?>" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo $this->Url->build('/logins/logout'); ?>" class="btn btn-default btn-flat" >Sign out</a>
                    </div>
                  </li>

                </ul>
              </li> */ ?>

      <?php /*        <li>
              <div class="headeUserDrop">
  <button class="btn dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Vikas Solnaki
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
    <li><a href="<?php echo $this->Url->build('/admin/users/add/'.$user_id); ?>">Profile</a></li>
   
  </ul>
</div>
              </li> */ ?>

<li><a href="<?php echo $this->Url->build('/admin/users/add/'.$user_id); ?>" class="adminusrProanchor">
<?php echo ucfirst($this->request->session()->read('Auth.User.name'));?>
</a></li>

<li>
  <a href="<?php echo $this->Url->build('/logins/logout'); ?>" class="adminLogout">
  <i class="fa fa-power-off" aria-hidden="true"></i>

  </a>
</li>


            </ul>
          </div>
        </nav>
      </header>
   



          <script>
$(document).ready(function(){
  $("header .sidebar-toggle").click(function(){
    $("aside.main-sidebar").toggleClass("open");
  });
});
</script>