<section id="google_app" class="position-relative pb-0">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-4 order-2 order-md-1"> <img src="<?php echo SITE_URL; ?>images/phone_app.webp"
                    alt="DAAC-Moblile_App" class="img-fluid"> </div>
            <div class="col-md-8 order-1 order-md-2">
                <div class="app_content position-relative">
                    <h3>Discover Our App</h3>
                    <p>Access your courses anywhere, anytime &amp; prepare with practice tests</p>
                    <a href="https://play.google.com/store/apps/details?id=in.daaconline.doomshell&hl=en"
                        target="_blank"><img src="<?php echo SITE_URL; ?>images/google-Play-Store.webp"
                            alt="DAAC-Moblile_App_play_store"></a>
                    <div id="animation_container"
                        style="background-color: rgb(255, 255, 255);width: 674px;height: 298px;position: absolute;top: -100px;right: -53px;background-color: transparent;z-index: 9999;">
                        <canvas id="canvas" width="674" height="298" style="width: 674px; height: 298px;"></canvas>
                        <div id="dom_overlay_container"
                            style="pointer-events: none; overflow: hidden; width: 674px; position: absolute; left: 0px; top: 0px; display: block; height: 298px;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer id="footer" class="position-relative pt-4">
    <div class="container">
        <div class="footbottom">
            <ul class="list-inline text-center">
                <li class="list-inline-item"> <a class="nav-link active text-light pl-0" href="<?php echo SITE_URL; ?>">
                        Home</a> </li>
                <li class=" list-inline-item list-inline-item"><a class="nav-link active text-light"
                        href="<?php echo SITE_URL; ?>about-us.html"> About</a></li>
                <li class="list-inline-item"> <a class="nav-link text-light"
                        href="<?php echo SITE_URL; ?>our-team.html">
                        Our Team</a> </li>
                <li class="list-inline-item list-inline-item"><a class="nav-link active text-light"
                        href="<?php echo SITE_URL; ?>alumni-speaks.html"> Alumni Speaks</a></li>
                <li class="list-inline-item"> <a class="nav-link text-light"
                        href="<?php echo SITE_URL; ?>placements-daac.html"> Placements</a> </li>
                <li class="list-inline-item list-inline-item"><a class="nav-link active text-light"
                        href="<?php echo SITE_URL; ?>contact.html"> Our Branches</a></li>
                <li class="list-inline-item list-inline-item"><a class="nav-link active text-light"
                        href="<?php echo SITE_URL; ?>faq.html"> FAQ</a></li>
                <li class="list-inline-item list-inline-item"><a class="nav-link active text-light"
                        href="<?php echo SITE_URL; ?>privacy.html"> Privacy</a></li>
                <li class="list-inline-item"> <a class="nav-link text-light" href="https://blog.daac.in/"> Blog</a>
                </li>
                <li class="list-inline-item"><a href="<?php echo SITE_URL;?>sitemap.xml" target="_blank"
                        rel="noopener noreferrer">Sitemap</a></li>
            </ul>
        </div>
    </div>
    <div class="footer_copyright text-center">
        <p class="text-center">2021 © <a href="#">Doomshell.com</a> , All Rights Reserved</p>
    </div>
</footer>

<div class="">
    <div id="request_modal" class="modal mylatest_modal fade" style="z-index: 99999">
        <div class="modal-dialog" style="width:735px;">
            <div class="modal-content">
                <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <div class="modal_img1">
                                <img alt="Doomshell" src="<?php echo SITE_URL; ?>/images/popup left.jpg"
                                    class="pop_mimg">
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <div class="modal_formsec">
                                <div class="modal_logo"><img src="<?php echo SITE_URL; ?>/images/logo.png"
                                        alt="DAAC-Advanced_Software_Development-Training_Institute_in_Jaipur"></div>
                                <div class="modal_divcontent">
                                    <h4>We Will Contact You, At a Time Which Suits You Best</h4>
                                    <form method="post" name="contcfrm"
                                        action="<?php echo SITE_URL; ?>pages/send_enquiry_mail"
                                        enctype="multipart/form-data" class="enquiry-form" onsubmit="return chk()">
                                        <input autocomplete="off" type="text" name="name" placeholder="Name"
                                            class="enq-input" required />
                                        <input type="email" name="email" id="email" placeholder="Email"
                                            autocomplete="off" class="enq-input" required />
                                        <input type="text" name="phone" id="phone" placeholder="Phone"
                                            autocomplete="off" class="enq-input" maxlength="10" required />
                                        <span id="phonevali" style="display: none; color: red; text-align: left;">
                                            Mobile Number is Not Valid</span>
                                        <select name="course" id="ddl_applyingfor" class="enq-select" required>
                                            <option value="56">Python</option>
                                            <option value="55">BigData</option>
                                            <option value="57">SalesForce</option>
                                            <option value="58">Oracle Apps</option>
                                            <option value="59">Block Chain</option>
                                            <option value="54">AWS</option>
                                            <option value="5">Web Designing</option>
                                            <option value="1">Graphic Designing</option>
                                            <option value="9">3D Animation</option>
                                            <option value="4">PHP</option>
                                            <option value="20">DOT NET</option>
                                            <option value="12">Linux</option>
                                            <option value="3">Dreamweaver</option>
                                            <option value="13">JAVA</option>
                                            <option value="14">SEO</option>
                                            <option value="15">SQT</option>
                                            <option value="24">C</option>
                                            <option value="26">C++</option>
                                            <option value="22">Android</option>
                                            <option value="16">Other</option>
                                        </select>
                                        <textarea placeholder="Message" name="comment"></textarea>
                                        <div class="cls"></div>
                                        <!-- <div class="g-recaptcha"
                                            data-sitekey="6LdUiWIaAAAAADuMFC6UIBe3Crn1tZJvJ59ZI87H"></div>-->
                                        <div class="cls"></div>
                                        <input type="submit" class="r-submit" name="sub" value="Submit" />
                                        <div class="cls"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
<?php 
 $msg=$this->Session->flash('editprope');
 if($msg){  ?>
$(document).ready(function() {
    $('#error_masg').modal('show');
    setTimeout(function() {
        $('#error_masg').modal('hide');
    }, 3000);
});
<?php } ?>
</script>
<div class="modal fade" id="error_masg" role="dialog" style="display:hide;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="alert alert-danger" role="alert">
            <?php  echo $msg;  ?>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $("header .c-menu__items li .submenu ").hide();
});
</script>
<script>
$(document).ready(function() {
    $(".dropdown1 .dropdown-toggle1").click(function() {
        $("header .dropdown1 .dropdown-menu1 ").slideToggle();
    });
    $(".dropdown2 .dropdown-toggle2").click(function() {
        $(".dropdown2 .dropdown-menu2 ").slideToggle();
    });
    $(".dropdown3 .dropdown-toggle3").click(function() {
        $(".dropdown3 .dropdown-menu3 ").slideToggle();
    });
    $(".dropdown4 .dropdown-toggle4").click(function() {
        $(".dropdown4 .dropdown-menu4 ").slideToggle();
    });
    $(".c-menu__item a").click(function() {
        $(this).toggleClass("active");
    });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    //called when key is pressed in textbox

    $("#phone").keypress(function(e) {
        //if the letter is not digit then display error and don't type anything

        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });
});
</script>
<script>
function testing2(form) {
    alert("Test");
    document.getElementById("entername").style.display = "none";
    document.getElementById("enterphone").style.display = "none";
    var mobnos = document.getElementById("phones");
    var phonenos = /(7|8|9)\d{9}/;
    if (form.names.value == "") {
        // alert("Enter Your Name!");
        document.getElementById("entername").style.display = "block";
        form.names.focus();
        return false;
    }
    if (!phonenos.test(phones.value)) {
        //alert('Please Provide a valid Phone No.');
        document.getElementById("enterphone").style.display = "block";
        document.getElementById("phone").value = "";
        form.phones.focus;
        return false;
    }
    if (grecaptcha.getResponse() == "") {
        alert("Captcha Fill Can't be empty");
        return false;
    }
    var chk = true;
    return chk;
}
</script>
<script>
function chk() {
    var mobile = document.getElementById("phone").value;
    var pattern = /(0|6|7|8|9)\d{9}/;
    var len = $('#phone').val().length;
    if (pattern.test(mobile) && len < 12) {
        $("#phonevali").css("display", "none");
    } else {
        $("#phonevali").css("display", "block");
        $("#phonevali").css("color", "red");
        return false;
    }
    if (grecaptcha.getResponse() == "") {
        alert("You can't proceed! Please Fill Captcha ");
        // return false;
    }
}
</script>
<script>
$('.count').each(function() {
    $(this).prop('Counter', 0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function(now) {
            $(this).text(Math.ceil(now));
        }
    });
});
</script>
<script>
$(document).ready(function() {
    $(".nav-pills a#pills-home-tab").click(function() {
        $(".tab-content .tab-pane.show").removeClass("show");
        $(".tab-content .tab-pane.active").removeClass("active");
        $(".tab-content .tab-pane#pills-home").addClass("show");
        $(".tab-content .tab-pane#pills-home").addClass("active");
    });
    $(".nav-pills a#pills-profile-tab").click(function() {
        $(".tab-content .tab-pane.show").removeClass("show");
        $(".tab-content .tab-pane.active").removeClass("active");
        $(".tab-content .tab-pane#pills-profile").addClass("show");
        $(".tab-content .tab-pane#pills-profile").addClass("active");
    });
    $(".nav-pills a#pills-mobile_app-tab").click(function() {
        $(".tab-content .tab-pane.show").removeClass("show");
        $(".tab-content .tab-pane.active").removeClass("active");
        $(".tab-content .tab-pane#mobile_app").addClass("show");
        $(".tab-content .tab-pane#mobile_app").addClass("active");
    });
    $(".nav-pills a#pills-other_tab-tab").click(function() {
        $(".tab-content .tab-pane.show").removeClass("show");
        $(".tab-content .tab-pane.active").removeClass("active");
        $(".tab-content .tab-pane#other_tab").addClass("show");
        $(".tab-content .tab-pane#other_tab").addClass("active");
    });
});
</script>
<script src="<?php echo SITE_URL; ?>frontend/js/owl.carousel.min.js" type="text/javascript"></script>
<script>
$('#otherc_crausal').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    autoplay: true,
    autoplaySpeed: 2000,
    autoplayTimeout: 2000,
    dots: true,
    nav: false,
    responsive: {
        0: {
            items: 1,
            nav: false
        },
        600: {
            items: 3,
            nav: false
        },
        1000: {
            items: 4,
            nav: false,
            loop: true
        }
    }
});

$('#recruter_crausal').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    autoplay: true,
    autoplaySpeed: 1000,

    dots: true,
    nav: false,
    responsive: {
        0: {
            items: 2,
            nav: false
        },
        600: {
            items: 4,
            nav: false
        },
        1000: {
            items: 6,
            nav: false,
            loop: true
        }
    }
});

$('#mapp_crausal').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    autoplay: true,
    autoplaySpeed: 2000,
    autoplayTimeout: 2000,
    dots: true,
    nav: false,
    responsive: {
        0: {
            items: 1,
            nav: false
        },
        600: {
            items: 3,
            nav: false
        },
        1000: {
            items: 4,
            nav: false,
            loop: true
        }
    }
});

$('#course_appcrausal').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    autoplay: true,
    autoplaySpeed: 2000,
    autoplayTimeout: 2000,
    dots: true,
    nav: false,
    responsive: {
        0: {
            items: 1,
            nav: false
        },
        600: {
            items: 3,
            nav: false
        },
        1000: {
            items: 4,
            nav: false,
            loop: true
        }
    }
});

$('#programing_crausal').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    autoplay: true,
    autoplaySpeed: 2000,
    autoplayTimeout: 2000,
    dots: true,
    nav: false,
    responsive: {
        0: {
            items: 1,
            nav: false
        },
        600: {
            items: 3,
            nav: false
        },
        1000: {
            items: 4,
            nav: false,
            loop: true
        }
    }
});

$('#designing_crausal').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    autoplay: true,
    autoplaySpeed: 2000,
    autoplayTimeout: 2000,
    nav: false,
    responsive: {
        0: {
            items: 1,
            nav: false,
            dots: false
        },
        600: {
            items: 3,
            nav: false,
            dots: false
        },
        1000: {
            items: 4,
            nav: false,
            dots: true,
            loop: true
        }
    }
});
</script>
<script type="text/javascript">
$(document).ready(function() {

    $(document).delegate('.open', 'click', function(event) {
        $(this).addClass('oppenned');
        event.stopPropagation();
    })

    $(document).delegate('.open.oppenned .cls', 'click', function(event) {
        $('.open').removeClass('oppenned');
        event.stopPropagation();
    });
});
</script>

<script type="text/javascript">
$(window).scrollTop(); // returns pixel value
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
});
</script>

<script src="<?php echo SITE_URL; ?>frontend/js/all_js.js" type="text/javascript"></script>
<script src="<?php echo SITE_URL; ?>frontend/js/lightbox.js" type="text/javascript"></script>
<link href="<?php echo SITE_URL ?>frontend/js/lightbox.min.css" rel="stylesheet">
<script src="<?php echo SITE_URL; ?>frontend/js/menu2.js" type="text/javascript"></script>
<script src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>
<script src="<?php echo SITE_URL; ?>frontend/js/svg_character22.js" type="text/javascript"></script>
<script src="<?php echo SITE_URL; ?>frontend/js/galleryjs.js" type="text/javascript"></script>


<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-36251023-1']);
_gaq.push(['_setDomainName', 'jqueryscript.net']);
_gaq.push(['_trackPageview']);

(function() {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') +
        '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
})();
</script>

<script>
var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;

function init() {
    canvas = document.getElementById("canvas");
    anim_container = document.getElementById("animation_container");
    dom_overlay_container = document.getElementById("dom_overlay_container");
    images = images || {};
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", handleFileLoad);
    loader.addEventListener("complete", handleComplete);
    loader.loadManifest(lib.properties.manifest);
}

function handleFileLoad(evt) {
    if (evt.item.type == "image") {
        images[evt.item.id] = evt.result;
    }
}

function handleComplete(evt) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for (i = 0; i < ssMetadata.length; i++) {
        ss[ssMetadata[i].name] = new createjs.SpriteSheet({
            "images": [queue.getResult(ssMetadata[i].name)],
            "frames": ssMetadata[i].frames
        })
    }
    exportRoot = new lib.svgorcanvas();
    stage = new createjs.Stage(canvas);
    stage.addChild(exportRoot);
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
        createjs.Ticker.setFPS(lib.properties.fps);
        createjs.Ticker.addEventListener("tick", stage);
    }
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
        var lastW, lastH, lastS = 1;
        window.addEventListener('resize', resizeCanvas);
        resizeCanvas();

        function resizeCanvas() {
            var w = lib.properties.width,
                h = lib.properties.height;
            var iw = window.innerWidth,
                ih = window.innerHeight;
            var pRatio = window.devicePixelRatio || 1,
                xRatio = iw / w,
                yRatio = ih / h,
                sRatio = 1;
            if (isResp) {
                if ((respDim == 'width' && lastW == iw) || (respDim == 'height' && lastH == ih)) {
                    sRatio = lastS;
                } else if (!isScale) {
                    if (iw < w || ih < h)
                        sRatio = Math.min(xRatio, yRatio);
                } else if (scaleType == 1) {
                    sRatio = Math.min(xRatio, yRatio);
                } else if (scaleType == 2) {
                    sRatio = Math.max(xRatio, yRatio);
                }
            }
            canvas.width = w * pRatio * sRatio;
            canvas.height = h * pRatio * sRatio;
            canvas.style.width = dom_overlay_container.style.width = anim_container.style.width = w * sRatio + 'px';
            canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h * sRatio + 'px';
            stage.scaleX = pRatio * sRatio;
            stage.scaleY = pRatio * sRatio;
            lastW = iw;
            lastH = ih;
            lastS = sRatio;
        }
    }
    makeResponsive(false, 'both', false, 1);
    fnStartAnimation();
}
</script>

<script>
var pushRight = new Menu({
    wrapper: '#o-wrapper',
    type: 'push-right',
    menuOpenerClass: '.c-button',
    maskId: '#c-mask'
});

var pushRightBtn = document.querySelector('#c-button--push-right');

pushRightBtn.addEventListener('click', function(e) {
    e.preventDefault;
    pushRight.open();
});
</script>


<script>
var pushRight = new Menu({
    wrapper: '#o-wrapper',
    type: 'push-right',
    menuOpenerClass: '.c-button',
    maskId: '#c-mask'
});
var pushRightBtn = document.querySelector('#c-button--push-right2');
pushRightBtn.addEventListener('click', function(e) {
    e.preventDefault;
    pushRight.open();
});
</script>



<script>
$(".drop_lilist1").mouseleave(function() {
    console.log("test1");
    $(".drop_ullist1").toggle();
});
$(".drop_lilist1").mouseenter(function() {
    $(".drop_ullist1").toggle();
});
$(".drop_lilist2").mouseleave(function() {
    console.log("test1");
    $(".drop_ullist2").toggle();
});
$(".drop_lilist2").mouseenter(function() {
    $(".drop_ullist2").toggle();
});
$(".drop_lilist3").mouseleave(function() {
    console.log("test1");
    $(".drop_ullist3").toggle();
});
$(".drop_lilist3").mouseenter(function() {
    $(".drop_ullist3").toggle();
});
$(".drop_lilist2 a").mouseleave(function() {
    console.log("test1");
});
$(".drop_lilist3 a").mouseleave(function() {
    console.log("test1");
});
</script>
<script>
$(document).ready(function() {
    var getNav = $(".onscroll_header");
    menu_height = $(".onload_header").outerHeight();
    actual_height = menu_height;

    var getOffset = actual_height;
    $(window).on("scroll", function() {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > getOffset) {
            getNav.addClass("sticked");
        } else {
            getNav.removeClass("sticked");
        }
    });
});
</script>
<script>
$('a[href^="#"]').on('click', function(e) {
    // e.preventDefault();

    var target = this.hash,
        $target = $(target);

    $('html, body').stop().animate({
        'scrollTop': $target.offset().top - 70
    }, 900, 'swing', function() {
        window.location.hash = target;
    });
});
</script>

<script>
var lFollowX = 0,
    lFollowY = 0,
    x = 0,
    y = 0,
    friction = 1 / 30;

function moveBackground() {
    x += (lFollowX - x) * friction;
    y += (lFollowY - y) * friction;
    translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';

    $('.course_bg').css({
        '-webit-transform': translate,
        '-moz-transform': translate,
        'transform': translate
    });
    window.requestAnimationFrame(moveBackground);
}
$(window).on('mousemove click', function(e) {
    var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
    var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
    lFollowX = (20 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
    lFollowY = (10 * lMouseY) / 100;
});
moveBackground(); <
!--Background Move js-- >
</script>

<script src="<?php echo SITE_URL; ?>frontend/js/classie2.js" type="text/javascript"></script>
<script src="<?php echo SITE_URL; ?>frontend/js/modalEffects2.js" type="text/javascript"></script>
<?php if(isset($_GET['d'])) { ?>
<div class="md-modal md-show" id="thank_you">
    <div class="md-content new_brnch_pop">
        <br><br>
        <h2 style="text-align:center;color:black;">Thank you for enquring DAAC We will get back to you soon.</h2>
        <a href="javascript:void(0)" class="md-close" onclick="$('#thank_you').removeClass('md-show');"></a>
        <br><br>
    </div>

</div>
<div class="md-overlay"></div>
<?php } ?>

<script type="text/javascript">
$('.branch-carousel').owlCarousel({
    loop: true,
    autoplay: true,
    smartSpeed: 400,
    responsiveClass: true,
    dots: false,
    nav: true,
    responsive: {
        0: {
            items: 1,
        },
    }
})
</script>

<script>
$("#myVideo")[0].autoplay = true;
// fallback: show controls if autoplay fails
// (needed for Samsung Internet for Android, as of v6.4)
let video = document.querySelector('video[muted][autoplay]');
try {
    await video.play();
} catch (err) {
    video.controls = true;
}
</script>

<script>
// Add url parameter or replace if already exists.
function addUrlParam(url, key, value) {
    var newParam = key + "=" + value;
    var result = url.replace(new RegExp("(&|\\?)" + key + "=[^\&|#]*"), '$1' + newParam);
    if (result === url) {
        result = (url.indexOf("?") != -1 ? url.split("?")[0] + "?" + newParam + "&" + url.split("?")[1] :
            (url.indexOf("#") != -1 ? url.split("#")[0] + "?" + newParam + "#" + url.split("#")[1] :
                url + '?' + newParam));
    }
    return result;
}

// Add and remove autoplay on videos opened in modals
$('.modal.videoModal').on('shown.bs.modal', function() {
    var $iframe = $(this).find('iframe');
    if ($iframe.length) {
        var src = $iframe.attr('src');
        var srcNew = addUrlParam(src, 'autoplay', '1');
        $iframe.attr('src', srcNew);
    }
});
$('.modal.videoModal').on('hide.bs.modal', function() {
    var $iframe = $(this).find('iframe');
    if ($iframe.length) {
        var src = $iframe.attr('src');
        var srcNew = addUrlParam(src, 'autoplay', '0');
        $iframe.attr('src', srcNew);
    }
});
</script>

<script>
$(document).ready(function() {
    $(".studentdropli").click(function() {
        $(".studentdropul").slideToggle();
    });
});
</script>
</body>

</html>