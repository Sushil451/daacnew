<!DOCTYPE HTML>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="authoring-tool" content="Adobe_Animate_CC">
      <!-- Chrome, Firefox OS and Opera -->
      <meta name="theme-color" content="#02abe3" />
      <!-- Windows Phone -->
      <meta name="msapplication-navbutton-color" content="#02abe3" />
      <!-- iOS Safari -->
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="#02abe3">
      <?php //$cl=Router::url($this->here, true );
         $urlseo = str_replace("/", "", $_SERVER['REQUEST_URI']);
         if($urlseo){
         $cl = str_replace("/", "", $_SERVER['REQUEST_URI']);
         }else{
         $cl=Router::url($this->here, true );
         }
       $met=$this->Comman->meta($cl);  
         if(!empty($met))
         { ?>
      <meta property="og:locale" content="en_US" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="<?php echo $met['title'];?>" />
      <meta property="og:description" name="description" content="<?php echo $met['description'];?>" />
      <meta property="og:url" content="<?php echo $met['url'];?>" />
      <meta property="og:site_name" content="DAAC" />
      <meta property="og:updated_time" content="<?php echo date('Y-m-d H:i:s'); ?>" />
      <meta property="og:image" content="<?php echo SITE_URL; ?>images/logo.png" />
      <meta property="og:image:secure_url" content="<?php echo SITE_URL; ?>images/logo.png" />
      <meta property="og:image:width" content="1024" />
      <meta property="og:image:height" content="383" />
      <meta property="og:image:alt" content="DAAC-Classroom With Corporate Professionals" />
      <meta property="og:image:type" content="image/png" />
      <meta name="google-site-verification" content="QrHJfM4Vor0wMNJc-mb9WIQ4aSWpjKV0uJLz5etp8hg" />
      <meta name="keywords" content="<?php echo $met['keyword'];?>" />
      <title><?php echo $met['title']; ?></title>
      <?php } ?>
   <!-- Canonical URL -->
      <?php $seosettings=$this->Comman->seosettingmeta(); ?>
      <link rel="canonical" href="<?php echo $seosettings['canonical_url']; ?>" hreflang="es-in" />
      <!-- Canonical URL -->
      <link rel="icon" href="<?php echo SITE_URL; ?>favicon.ico" type="image/ico">
      <!-- CSS Start -->
      <!---------bootstrap-------------------->
      <link href="<?php echo SITE_URL; ?>frontend/css/bootstrap2.min.css" rel="stylesheet" type="text/css" hreflang="es-in">
      
      <link href="<?php echo SITE_URL; ?>frontend/css/prettyPhoto2.css" rel="stylesheet" type="text/css" hreflang="es-in">
      <!------------font-awesome------------------>
      <link href="<?php echo SITE_URL; ?>frontend/css/font-awesome2.min.css" rel="stylesheet" type="text/css" hreflang="es-in">
      <link href="<?php echo SITE_URL; ?>frontend/css/owl.carousel2.min.css" rel="stylesheet" type="text/css" hreflang="es-in">
      <link href="<?php echo SITE_URL; ?>frontend/css/owl.theme2.default.css" rel="stylesheet" type="text/css" hreflang="es-in">
      <link href="<?php echo SITE_URL; ?>frontend/css/bootstrap-reboot2.min.css" rel="stylesheet" type="text/css" hreflang="es-in">
      <link href="<?php echo SITE_URL; ?>frontend/css/style-gallery.css" rel="stylesheet" type="text/css" hreflang="es-in">
      <link href="<?php echo SITE_URL; ?>frontend/css/toggle2.min.css" rel="stylesheet" type="text/css" hreflang="es-in">
      <link rel="stylesheet" href="<?php echo SITE_URL; ?>frontend/css/admin/bootstrap.offcanvas.min.css" hreflang="es-in" />
      <!---------------general css---------------------->
      <link href="<?php echo SITE_URL; ?>frontend/css/style2.css?12345" rel="stylesheet" type="text/css" hreflang="es-in">
      <link href="<?php echo SITE_URL; ?>frontend/css/headerStyle.css?12546" rel="stylesheet" type="text/css" hreflang="es-in">
      <link href="<?php echo SITE_URL; ?>frontend/css/component2.css" rel="stylesheet" type="text/css" hreflang="es-in">
      <link href="<?php echo SITE_URL; ?>frontend/css/branch_style2.css?0123" rel="stylesheet" type="text/css" hreflang="es-in">
      <link href="<?php echo SITE_URL; ?>frontend/css/responsive2.css" rel="stylesheet" type="text/css" hreflang="es-in">
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&display=swap" rel="stylesheet" type="text/css" hreflang="es-in">
      <!-- general JS -->
      <script src="<?php echo SITE_URL; ?>frontend/js/jquery-3.4.1.min.js" type="text/javascript"></script>
      <script src='https://www.google.com/recaptcha/api.js'></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" type="text/javascript"></script>
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-3229371-3" type="text/javascript"></script>
           <!-- Zopim Code -->
      <script type="text/javascript">
         window.$zopim || (function(d, s) {
         var z = $zopim = function(c) {
                 z._.push(c)
             },
             $ = z.s =
             d.createElement(s),
             e = d.getElementsByTagName(s)[0];
         z.set = function(o) {
             z.set.
             _.push(o)
         };
         z._ = [];
         z.set._ = [];
         $.async = !0;
         $.setAttribute('charset', 'utf-8');
         $.src = '//v2.zopim.com/?2Cxux1m3BoMR5fkr9UPq99hClv0gNlp1';
         z.t = +new Date;
         $.
         type = 'text/javascript';
         e.parentNode.insertBefore($, e)
         })(document, 'script');
      </script>
      <!-- End Zopim Chat -->
   </head>
   <body onload="init();" class="modal-open">
      <!-- Global site tag google analytics (gtag.js) - Google Analytics -->
      <?php echo $seosettings['g_tag_script']; ?>
      <!-- End google analytics Code -->
      <div id="page-wrapper">
      <section id="social_icons">
         <ul class="list-unstyled">
            <li><a href="https://www.facebook.com/DAACJAIPUR" class="face" target="_blank"><i
               class="fab fa-facebook-f"></i><span>Facebook</span>
               </a>
            </li>
            <li><a href="https://twitter.com/DaacAcademy" class="twit" target="_blank"><i
               class="fab fa-twitter"></i><span> Twitter</span>
               </a>
            </li>
            <li><a href="https://www.instagram.com/daacjaipur/" class="insta" target="_blank"><i
               class="fab fa-instagram"></i><span>Instagram</span>
               </a>
            </li>
            <li><a href="http://www.youtube.com/c/DAACJaipur" class="u_tube" target="_blank"><i
               class="fab fa-youtube"></i><span>Youtube</span>
               </a>
            </li>
            <li><a href="http://www.linkedin.com/company/daac-doomshell-academy-of-advance-computing-" class="linkd"
               target="_blank"><i class="fab fa-linkedin-in"></i><span>Linkdin</span>
               </a>
            </li>
            <li><a href="https://in.pinterest.com/DaacInstitute/" class="pin_trest" target="_blank"><i class="fab fa-pinterest-p" aria-hidden="true"></i>
               </a>
            </li>
         </ul>
      </section>
      
      <?php $pname=$_SERVER['REQUEST_URI']; 
      
         $control=$this->request->params['controller'];
         $action=$this->request->params['action'];
         ?>
      <header id="header">
         <div class="header_bottom_bar">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-4 col-5 align-self-center2 align-self-center">
                     <div class="logo"> 
                        <a href="<?php echo SITE_URL; ?>"> 
                        <img src="<?php echo SITE_URL; ?>images/logo.png" alt="DAAC-Advanced_Software_Development-Training_Institute_in_Jaipur" class="img-fluid">
                        </a> 
                     </div>
                  </div>
                  <div class="col-sm-4 col-2 align-self-center">
                     <div class="glorious_logo text-center"> 
                        <img src="<?php echo SITE_URL; ?>images/glorius13.png" alt="DAAC-14_glorius_years_of_experiance"
                           class="img-fluid glorious_logo"> 
                     </div>
                  </div>
                  <div class="col-sm-4 col-5 align-self-center">
                     <div class="head_contact_info text-right align-self-center">
                        <li class="nav-item request_call"> 
                           <p class="nav-link text-light pr-0 mb-3 mt-3"> 
                           <span class="d-none d-sm-inline-block">Call :</span> 
                           8764122221, 9414431944 
      </p>
                        </li>
                        <li>
                           <ul class="list-inline text-right head_top_social">
                              <li class="list-inline-item"><a href="https://www.facebook.com/DAACJAIPUR"
                                 class="face" target="_blank"><i class="fab fa-facebook-f"></i>
                                 </a>
                              </li>
                              <li class="list-inline-item"><a href="https://twitter.com/DaacAcademy"
                                 class="twit" target="_blank"><i class="fab fa-twitter"></i>
                                 </a>
                              </li>
                              <li class="list-inline-item"><a href="https://www.instagram.com/daacjaipur/"
                                 class="insta" target="_blank"><i class="fab fa-instagram"></i>
                                 </a>
                              </li>
                              <li class="list-inline-item"><a href="http://www.youtube.com/c/DAACJaipur"
                                 class="u_tube" target="_blank"><i class="fab fa-youtube"></i>
                                 </a>
                              </li>
                              <li class="list-inline-item"><a
                                 href="http://www.linkedin.com/company/daac-doomshell-academy-of-advance-computing-"
                                 class="linkd" target="_blank"><i class="fab fa-linkedin-in"></i>
                                 </a>
                              </li>
                              <li class="list-inline-item"><a href="https://in.pinterest.com/DaacInstitute/"
                                 class="pin_trest" target="_blank"><i class="fab fa-pinterest-p"></i>
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <!-- /scroller -->
                     </div>
                  </div>
                  <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog"
                     aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                     <div class="modal-dialog modal-xl">
                        <div class="modal-content rounded-0">
                           <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Our Courses</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                              </button>
                           </div>
                           <div class="modal-body">
                              <div class="courses_msecmenu">
                                 <div class="courses_msecmenu_inner">
                                    <!--<a href="javascript:void(0);" class="courses_msecmenu_close"><i class="fas fa-times"></i></a>-->
<div>
            <div class="row">
                                          <div class="col-md-3 col-sm-4 col-5">
                                             <div class="leftbg">
                       <div class="nav flex-column nav-pills" id="v-pills-tab"   role="tablist" aria-orientation="vertical">

<?php $getcourse=$this->Comman->findcoursegroup();
$cnt=1;
foreach($getcourse as $kry=>$item){ 
  if($item!="Students Work"){ 
    if($item=="Digital Marketing") {
?>
<a class="nav-link" href="<?php echo SITE_URL; ?>digital-marketing-course-in-jaipur.html">Digital Marketing</a>
<?php
}else if($item=="Advance Excel") {
  ?>
<a class="nav-link" href="<?php echo SITE_URL; ?>advance-excel-training.html">Advance Excel</a>
  <?php
  }else if($item=="Basic Computer") {
    ?>
  <a class="nav-link"href="<?php echo SITE_URL; ?>basic-computer-training-jaipur.html">Basic Computer</a>
    <?php
    }else{ ?>

 <a class="nav-link <?php if($cnt==2){ ?> active <?php } ?>" id="v-pills-settings-tab<?php echo $cnt; ?>" data-toggle="pill" href="#v-pills-course<?php echo $cnt; ?>"  role="tab" aria-controls="v-pills-course<?php echo $cnt; ?>" aria-selected="false"><?php echo $item; ?> </a> <?php } } $cnt++; } ?>
                
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-9 col-sm-8 col-7">
                                          <div class="tab-content" id="v-pills-tabContent">
                                          <?php  
$cnt=1;
foreach($getcourse as $kry=>$item){ 
  if($item=="Students Work" || $item=="Advance Excel" || $item=="Basic Computer" ){ 
   
    }else{ 
      
      $getcoursedetail=$this->Comman->getcoursebygroup($item);
      ?>

 
 <div class="tab-pane fade <?php if($cnt==2){ ?> show active <?php } ?> " id="v-pills-course<?php echo $cnt; ?>" role="tabpanel" aria-labelledby="v-pills-course<?php echo $cnt; ?>">
                       <ul class="list-unstyled">

<?php foreach($getcoursedetail as $krys=>$items){

  $path=LOCALCOURSE.$items['icon'];
 
  if(!empty($items['icon']) && file_exists($path)){
    $img=SITE_URL."images/course/".$items['icon'];
  }else{
    $img=SITE_URL."images/designing_1.png";
  }
  
   ?>

   <li>
     <a href="<?php echo $items['url']; ?>">
                    <div>
       <img src="<?php echo $img; ?>" alt="photoshop-institute-in-jaipur"></div>
        <span><?php echo $items['cname']; ?></span> </a>
                                                      </li>
                                            <?php } ?>         
                                                   </ul>
                                                </div>
 <?php } $cnt++; } ?>
</div></div> </div> </div></div></div></div> </div></div></div> </div></div>
         </div>
         <div class="header_top_bar">
            <div class="container-fluid">
               <ul class="nav d-flex ">
                  <li class="toggle_li d-block d-lg-none">
                     <div class="toogle_menu_velvet">
                        <div class="open">
                           <span class="cls"></span>
                           <span>
                              <div class="clos_icomenu"><i class="fas fa-times"></i></div>
                              <ul class="sub-menu ">
                                 <!-- <li class="nav-item"> <a class="nav-link active text-light pl-0" href="<?php echo SITE_URL; ?>"><-- <i class="fas fa-home"></i> -- Home</a> </li> -->
                                 <li class=" nav-item list-inline-item">
                                    <a class="nav-link active text-light"
                                       href="<?php echo SITE_URL; ?>about-us.html">
                                       <!-- <i class="fas fa-info-circle"></i> --> About
                                    </a>
                                 </li>
                                 <li class="nav-item">
                                    <a class="nav-link text-light"
                                       href="<?php echo SITE_URL; ?>our-team.html">
                                       <!-- <i class="fas fa-users"></i> --> Our Team
                                    </a>
                                 </li>
                                 <li class=" nav-item list-inline-item studentdropli">
                                    <a
                                       class="nav-link active text-light" href="javascript:void(0);">
                                       <!-- <i class="fas fa-info-circle"></i> --> Student Work <i
                                          class="fas fa-chevron-down"></i>
                                    </a>
                                    <ul class="studentdropul">
     <li><a href="<?php echo SITE_URL; ?>student-sketching-work.html">Sketching</a></li>
     <li><a href="<?php echo SITE_URL; ?>student-photoshop-work.html">Photoshop  </a></li>
     <li><a href="<?php echo SITE_URL; ?>student-html-work.html">HTML </a></li>
     <li><a href="<?php echo SITE_URL; ?>student-cake-php-work.html">CakePHP</a></li>
     <li><a href="<?php echo SITE_URL; ?>student-mobile-app-work.html">Mobile Apps </a></li>
     <li><a href="<?php echo SITE_URL; ?>student-digital-marketing-work.html">Digital  Marketing </a> </li> </ul></li>
                                
      <li class="nav-item list-inline-item"> <a class="nav-link active text-light"
                                       href="<?php echo SITE_URL; ?>alumni-speaks.html">
                                       Alumni Speaks
                                    </a>
                                 </li>
      <li class="nav-item"><a class="nav-link text-light"
         href="<?php echo SITE_URL; ?>placements-daac.html">
                                        Placements
                                    </a>
                                 </li>
     <li class="nav-item list-inline-item"><a class="nav-link active text-light"
                                       href="<?php echo SITE_URL; ?>contact.html">
                                        Our Branches
                                    </a>
                                 </li>
    <li class="nav-item list-inline-item"> 
       <a class="nav-link active text-light" href="<?php echo SITE_URL; ?>faq.html">FAQ
                                    </a>
        </li>
     <li class="nav-item"><a class="nav-link text-light" href="https://blog.daac.in/">Blog
                                    </a>
       </li>
                              </ul>
                           </span>
                           <span class="cls"></span>
                        </div>
                     </div>
                  </li>
                  <li class="nav-item all_courses d-inline-block d-lg-none"> 
                     <a href="#" class="nav-link text-light " data-toggle="modal" data-target=".bd-example-modal-xl">
                     <i class="fas fa-th"></i> Our Courses 
                     </a> 
                  </li>
                  <li class="d-none d-lg-inline-block">
                     <ul class="nav">
                        <li class=" nav-item list-inline-item">
                           <a class="nav-link active text-light"
                              href="<?php echo SITE_URL; ?>about-us.html">
                               About
                           </a>
                        </li>
                        <li class="nav-item all_courses">
                           <a href="#" class="nav-link text-light "
                              data-toggle="modal" data-target=".bd-example-modal-xl">
                              <!-- <i class="fas fa-th"></i> --> Our Courses <i
                                 class="fas fa-chevron-down"></i>
                           </a>
                        </li>
                     
                        <li class=" nav-item list-inline-item studentdropli">
                           <a class="nav-link active text-light" href="javascript:void(0);">
                              Student Work <i
                                 class="fas fa-chevron-down"></i>
                           </a>
                           <ul class="studentdropul">
                              <li><a href="<?php echo SITE_URL; ?>student-sketching-work.html">Sketching </a></li>
                              <li><a href="<?php echo SITE_URL; ?>student-photoshop-work.html">Photoshop </a></li>
                              <li><a href="<?php echo SITE_URL; ?>student-html-work.html">HTML </a></li>
                              <li><a href="<?php echo SITE_URL; ?>student-cake-php-work.html">CakePHP </a></li>
                              <li><a href="<?php echo SITE_URL; ?>student-mobile-app-work.html">Mobile Apps </a></li>
                              <li><a href="<?php echo SITE_URL; ?>student-digital-marketing-work.html">Digital
                                 Marketing </a>
                              </li>
                           </ul>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link text-light"
                              href="<?php echo SITE_URL; ?>our-team.html">
                              <!-- <i class="fas fa-users"></i> --> Our Team
                           </a>
                        </li>
                        <li class="nav-item list-inline-item">
                           <a class="nav-link active text-light"
                              href="<?php echo SITE_URL; ?>alumni-speaks.html">
                              Alumni Speaks
                           </a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link text-light"
                              href="<?php echo SITE_URL; ?>placements-daac.html">
                              <!-- <i class="fas fa-user-check"></i> --> Placements
                           </a>
                        </li>
                        <li class="nav-item list-inline-item">
                           <a class="nav-link active text-light"
                              href="<?php echo SITE_URL; ?>contact.html">
                              <!-- <i class="fas fa-map-marker-alt"></i> --> Our Branches
                           </a>
                        </li>
                        <li class="nav-item list-inline-item">
                           <a class="nav-link active text-light"
                              href="<?php echo SITE_URL; ?>faq.html">
                              <!-- <i class="far fa-question-circle"></i> --> FAQ
                           </a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link text-light" href="https://blog.daac.in/">
                              <!-- <i class="fab fa-blogger-b"></i> --> Blog
                           </a>
                        </li>
                     </ul>
                  </li>
                  <li class="nav-item ml-auto request_call"> 
                     <a href="#" class="nav-link text-light pr-0" data-toggle="modal" data-target="#request_modal"> 
                     <i class="fas fa-phone-volume"></i>
                     Request a Call Back 
                     </a> 
                  </li>
               </ul>
            </div>
         </div>
         <div class="whatsapp">
            <a href="https://api.whatsapp.com/send?phone=918764122221" class="pin_trest" target="_blank">
            <i class="fab fa-whatsapp"></i>
            </a>
         </div>
      </header>
     

