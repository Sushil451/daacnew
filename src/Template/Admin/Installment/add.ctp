<!-- <script>
   function checkextension() {
    var file = document.querySelector("#fUpload");
    if ( /\.(jpe?g|png|gif)$/i.test(file.files[0].name) === false ) 
      { alert("not an image please choose a image!");
    $('#fUpload').val('');
  }
  return false;
}
</script> -->


<style>
	.crsFeeDv .col-sm-3 { width:100% !important;}
	.installments{ display:flex; flex-wrap:wrap; margin:0px -8px; margin-top:30px;}
	.installments p{ margin:0px; width:16.666%; padding:0px 8px; margin-bottom:8px;}
	.installments p label{ display:block; text-align:left;}
	.installments p input{display: block;
    width: 100%;
    padding: .375rem .75rem; height:35px;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;}
</style>

<script>

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
        alert("Please Insert Only Numeric Characters!!");
        return false;
    }
    return true;

}
</script>

<script>
$(document).ready(function(){
$('#total_ins').change(function(){
	$('#err').css('display','none');
	var tot_ins=$(this).val();
	if(tot_ins > 12) {
		$('#total_ins').val('');
		$('#total_ins').focus();
		$('#total_ins').css('border-color','red');
		$('#err').css('display','block');
		 $('.installments').empty();
	}
	else {
			$('#err').css('display','none');
			$('#total_ins').css('border','1px solid #ddd');
	$('.installments').empty();
for(var i=1; i<=tot_ins; i++){	
  $('.installments').append(' <p>  <label>Installment '+i+'</label>    <span class="field"> <input type="text" required="required" class="longinput text inss" name="ins[]" onkeypress="return isNumber(event);">	</span>    </p>');

} 
$(".inss").each(function() {
 
            $(this).keyup(function(){
				
                calculateSum();
            });
        });
}
	
	
	
	
	});
	
	
	
 function calculateSum() {
 
        var sum = 0;
        //iterate through each textboxes and add the values
        $(".inss").each(function() {
 
            //add only if the value is number
            if(!isNaN(this.value) && this.value.length!=0) {
                sum += parseFloat(this.value);
            }
 
        });
        //.toFixed() method will roundoff the final sum to 2 decimal places

return sum;
  
       // $("#sum").html(sum.toFixed(2));
    }	
	
	$('.stdform').submit(function(){
		
	
	var tot_inst=calculateSum();	
	var fees = $('#fee_cou').val();


	if(tot_inst!=fees)
	{
	alert("Total installment amount and fees are not equal.");
		return false;
	}
	else
	{
		return true;
		}
	
		});

	
	
	});
</script>

 <script>
					$(document).ready(function(){
						$('#course').change(function(){
							var id=$(this).val();
							//alert(id);
                $.ajax({
type: "POST",
url: '<?php echo SITE_URL; ?>admin/installment/select_course',
data : { 'id' : id },
success: function(data){
if(data.trim()=='0'){
  //alert("hello");
$('#eror').html("Your Course fees not approved by Admin.");	
$('#klo').hide(); 
$('#fee_cou').val(''); 
 } else {
	 //alert("bye");
 $('#sk').html(data);
 $('#klo').show();
 $('#eror').html("");	
 }
}
});  
}); 
}); 
                
                
                </script>

<style type="text/css">
.box.box-info{ margin-bottom:30px;}
	.text{color:red; font-size: 12px;}
	.filename{font-size: 11px;color: #e02727;}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<i class="fa fa-plus-square" aria-hidden="true"></i>  
						<?php if(isset($newresponse['s_id'])){ ?>  Edit Student <?php } else { ?> 	 Add Installment <?php } ?>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo SITE_URL; ?>admin/students"><i class="fa fa-home"></i>Home</a></li>
			<li><a href="<?php echo SITE_URL; ?>admin/students">Manage Installment</a></li>
			<?php if(isset($newresponse['s_id'])){ ?>
				<li class="active"><a href="javascript:void(0)">Edit Installment</a></li>   
			<?php } else { ?>
				<li class="active"><a href="javascript:void(0)">Add Installment</a></li>
			<?php } ?>
		</ol>
	</section>
	<script>
function isNumber(evt) {

evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;
if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {

return false;
}
return true;
}

</script>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
			<?php echo $this->Form->create($newresponse, array(
						'class'=>'form-horizontal needs-validation',
						'type' => 'file',
						'validate','novalidate'
						
					));
				
					 ?>

				<div class="box box-info">
					<?php echo $this->Flash->render(); ?>
					<div class="box-header with-border">
						<h3 class="box-title">Installment Info</h3>
					</div>
			
					<div class="box-body addStdntsFrm">
						<div class="row">
							<div class="col-sm-4">
                           
								<label for="inputEmail3" class="control-label">Title <strong style="color:red;">*</strong></label>
								<?php echo $this->Form->input('inst_name', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Enter Title','autocomplete'=>'off','type'=>'text')); ?>   
							
                           
							</div>

                            <div class="col-sm-4">
                           
                           <label for="inputEmail3" class="control-label">Select Course<strong style="color:red;">*</strong></label>
                           <?php echo $this->Form->input('course_id', array('class' => 'form-control','required','label'=>false,'options' => $courses,'required','id'=>'course','empty'=>'Select Course')); ?>   
                       
                           
                       </div>
             <div class ="col-sm-4">
<div class="row">

			
			 <div id="sk" class="crsFeeDv col-md-6">
                
				</div>
				
				<div class="col-sm-6">
                           
						   <label for="inputEmail3" class="control-label">No of Installments<strong style="color:red;">*</strong></label>
						   <?php echo $this->Form->input('total_ins', array('class' => 'form-control','required','label'=>false,'id'=>'total_ins','onkeypress'=>'return isNumber(event);')); ?>   
					   
					  
					   </div>
			
			</div>
			<!-- sk -->
				</div>
<!-- col-md-6 -->
<div class = "col-sm-12">
			<div class="installments">


            </div>

				</div>

			

	
		<div id="fstbl" class="col-sm-12" >
             
			 </div>
			<!-- col-md-3 -->



						
						</div> 
<!-- row -->

<div class="box-footer" id="sub">
						<?php echo $this->Html->link('Back', ['action' => 'index'],['class'=>'btn btn-default']); ?>
						<?php if($newresponse['s_id']){ ?>
							<?php echo $this->Form->submit('Update',array('class' => 'btn btn-info pull-right', 'title' => 'Update')); ?>
							<?php }else{ ?>
							<?php echo $this->Form->submit('Save',array('class' => 'btn btn-info pull-right', 'title' => 'Save')); ?>
							<?php } ?>
						</div>

					
	




					</div> 
					<!--  -->

		</div>

<!-- box-end -->







<?php echo $this->Form->end(); ?>
				</div>
			</div>
		</section>
	</div> 
	<script>
$(document).ready(function(){
  var course_fees;
	$('#course_id').change(function(){
		$('.lds-facebook').show();
		//alert(userbranch);
		var id=$(this).val();
    var degreeCourses=<?php echo json_encode($degrecourseList) ?>;
    console.log(degreeCourses);
    if(Object.values(degreeCourses).includes(id)){
      $('.session').show();
    }else{
      $('.session').hide();
    }
		//alert(id);
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/checkcoursefeeflexibility',
		   type:'POST',
		   data:{'id12': id},
		   success:function(data){
			//alert(data);
			if(data.trim()!='0' && data.trim()!='1'){
       
		var userbranch='<?php echo $this->request->session()->read('Auth.User.branch'); ?>';   
		$('#rfee').val(data);
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/inst',
		   type:'POST',
		   data:{'id12': id, 'branch': userbranch},
		   success:function(data1){
			//alert(data1);
		
			if(data1.trim()=='5'){
				$('#sk').html('<div id="pk" style="color:red;"><label class=" text-right">Installment Type </label> <div class="input text"><span class="field" id="kl"> No Installment Type Added For This Course.</span></div></div>');
				
				$('#sub').hide();
			} else {
			$('#sk').html(data1);
			$('#sub').show();
		}
		   },

			});	   
		} 
		
		if(data.trim()=='1'){
			var userbranch='0';   
		
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/inst',
		   type:'POST',
		   data:{'id12': id, 'branch': userbranch},
		   success:function(data1){
			$('.lds-facebook').hide();
			$('#sk').html(data1);
			
		   },

			});
			
		} 
		
		if(data.trim()=='0'){
	

		$('#sub').hide();	
		$('#sk').html('<div id="pk" style="color:red;"><label class="text-right">Installment Type </label><div class="input text"><span class="field" id="kl"> Course is not approved by Admin.</span></div></div>');
				
				$('#sub').hide();
		} 
		   },

      });
     		
		});
	
	});
</script> 


<script>
$(function() {
$( "#datepicker" ).datepicker();

});
$(function() {
$( "#datepickerbrth" ).datepicker({minDate:null,changeYear:true,changeMonth:true,yearRange:"c-40:c"});

});



</script>


<script>

function getrefdata(data){
var type=data.value;
if(type==11){
  $('#updaterefdetails').css('display','block');
  $('#rname').attr('required',true);
  $('#rmobile').attr('required',true);
   $('#rmobile').val('<?php echo $this->request->data["rmobile"] ?>');

  $('#rname').val('<?php echo $this->request->data["rname"] ?>');
}
else{

  $('#rname').removeAttr('required');
  $('#rmobile').removeAttr('required');
  $('#rmobile').val('');

  $('#rname').val('');
    $('#updaterefdetails').css('display','none');

}
}
</script>
<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
    
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>