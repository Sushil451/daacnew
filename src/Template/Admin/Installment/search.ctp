
      <table  class="table table-bordered table-striped afTblStndRept" width="100%">
               
               <thead class="thead-dark">
               <tr>
                       <th style="width:1%;text-align:left;">S.No</th>
                       <th style="width: 18%;text-align:left;">Title</th>
                       <th style="width: 14%;text-align:left;">Course</th>
                       <th style="width: 14%;text-align:left;">Total Inst.</th>
                       <th style="width:8%;text-align:left;">Added On</th>
                       
<th style="text-align:left;">Actions</th>
                  
                   </tr>
               </thead>
               <tbody>
               <?php 
           $counter=($this->request->params['paging']['Installment']['page']-1) * $this->request->params['paging']['Document']['perPage']; 
           if(isset($rec) && !empty($rec)){ 
           foreach($rec as $product){//pr($product);die;?>
             <tr>
               <td><?php echo $counter+1;?></td>
               <td><b><?php echo $product['inst_name'] ?></b></td>
               <td><?php echo $product['course']['cname']; ?></td>
               <td><?php echo  $product['total_ins'] ?></td>
               <td><?php echo $product['add_date']; ?></td>
               <td>
              
               <?php

if($product['status']!=1)
{ ?>
<a href="<?php echo ADMIN_URL?>course/make_supportiv_installment/inactive/<?php echo $product['inst_id'];?>"> <img src="<?php echo SITE_URL?>admin_images/inactive.png"></a>
<?php }
else
{  ?>
<a href="<?php echo ADMIN_URL?>course/make_supportiv_installment/active/<?php echo $product['inst_id'];?>"> <img src="<?php echo SITE_URL?>admin_images/active.png"></a>
<?php }
?>





                <?php  echo $this->Html->link(__(''), ['action' => 'add', $product->id,],array('class'=>'fa fa-pencil-square-o fa-lg','title'=>'Edit','style'=>'font-size: 20px !important; margin-left: 12px;')) ?>
                

                <?php echo $this->Html->link('', ['action' => 'delete',$product->id],['title'=>'Delete','class'=> 'fa fa-trash','style'=>'color:#FF0000; margin-left: 13px; font-size: 19px !important;',"onClick"=>"javascript: return confirm('Are you sure do you want to delete this Course')"]); ?>
              </td>
            </tr>
            <?php $counter++;} } ?>  





                  
               </tbody>
           </table>
             <?php echo $this->element('admin/pagination'); ?> 