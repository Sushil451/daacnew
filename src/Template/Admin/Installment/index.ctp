<style type="text/css">
   th{
text-align: center;
   }
   td{
    text-align: center;

   }



   .action_icon a span {
    font-size: 14px !important;
}
 .action_icon a {
    font-size: 13px !important;
}
.action_icon b i {
    font-size: 13px !important;
}

.action_icon a {
    display: inline-block;
    width: 21px;
    text-align: center;
}
 </style>
 <div class="content-wrapper">
   <section class="content-header">
    <h1>
        Installment Detail
    </h1>
    <ol class="breadcrumb">
    <li><a href="<?php echo SITE_URL; ?>admin/dashboards"><i class="fa fa-home"></i>Home</a></li>
<li><a href="<?php echo SITE_URL; ?>admin/installment">Installment List</a></li>
    </ol> 
    </section> <!-- content header -->

    <!-- Main content -->
    <section class="content">
  <div class="row">
    <div class="col-xs-12">    
  <div class="box">
    <div class="box-header">
  <?php echo $this->Flash->render(); ?>


  <?php $role_id=$this->request->session()->read('Auth.User.role_id'); ?>
            <script>          
              $(document).ready(function () { 
                $("#Stsearch").bind("click", function (event) {
                //  $('.lds-facebook').show();
                  $.ajax({
                    async:true,
                    data:$("#Stsearch").serialize(),
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>installment/search",
                    success:function (data) {
                    //  $('.lds-facebook').hide();   
                      $("#Mycity").html(data); },
                    });
                  return false;
                });


                $("#Stsearch").bind("keyup", function (event) {
                //  $('.lds-facebook').show();
                  $.ajax({
                    async:true,
                    data:$("#Stsearch").serialize(),
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>installment/search",
                    success:function (data) {
                    //  $('.lds-facebook').hide();   
                      $("#Mycity").html(data); },
                    });
                  return false;
                });




              });

              $(document).on('click', '.pagination a', function(e) {
                var target = $(this).attr('href');
                var res = target.replace("/installment/search", "/installment");
                window.location = res;
                return false;
              });
            </script>

            
            <div class="form-group studentDtlFrmGroup d-flex justify-content-between" >
            <div class="col-md-6 align-self-center">
            <?php echo $this->Form->create('Stsearch',array('type'=>'post','inputDefaults'=>array('div'=>false,'label'=>false),'id'=>'Stsearch','class'=>'form-horizontal mb-0','method'=>'POST')); ?>
            
           
             <div class="d-flex row">
           
              <div class="col-md-6 ">
                
<?php echo $this->Form->input('course_id', array('class' =>'form-control','id'=>'course_id','label'=>false,'options'=>$courses,'empty'=>'Search By Couses','autofocus')); ?>
              </div>

            
            </div>
            
            <?php echo $this->Form->end(); ?>   
            </div>
  <div class="col-md-2">
    <ul class="list-inline w-75 m-auto text-center d-flex justify-content-around">
    <li class="list-inline-item">

              <a class="addnn" href="<?php echo ADMIN_URL; ?>installment/add"><i class="fa fa-user-plus" aria-hidden="true"></i>
     <br> New</a> 
            </li>

         


            </ul>
  <style>
        .addnn , .addnn:hover{ height:55px; width: 55px; border-radius: 50%; background-color: #2d95e3; line-height: 15px; font-size: 14px !important; text-align: center; color: #fff; display: inline-block; text-decoration: none;}
        .addnn i{ font-size:18px; color:#fff; margin-top:10px;}
        .studentDtlFrmGroup{ display:flex; flex-wrap:wrap;}
        .studentDtlFrmGroup>.col-md-10, .studentDtlFrmGroup>.col-md-2{ align-self:center !important;}
    </style>

            
              
                  <?php if($this->request->session()->read("Auth.User.role_id")==4)  {  
  $branchID = $this->request->session()->read("Auth.User.branch");
 echo $this->Form->input('branch_id', array('class' => 'smallselect branch','type' => 'hidden','value'=>$branchID));
}

 ?>
   <?php /*          
              <div class="col-sm-1">
                <label for="inputEmail3" class="control-label" style="color:white">Reset</label>
                <input type="reset" style="background-color:#d33c44;color:#fff;"  class="btn btn-success" value="Reset">       
              </div> 
              <div class="col-sm-1" style="margin-left: -40px;">
                <label for="inputEmail3" class="control-label" style="color:white">Search</label>
                <input type="submit" style="background-color:green;color:#fff;" id="Mysubscriptions" class="btn btn-success" value="Search">       
              </div> */ ?>
              </div>  
              
            
      
       

   
     </div><!-- /.box-header -->
      <div class="box-body" id="Mycity"> 
          
             

      <table  class="table table-bordered table-striped afTblStndRept" width="100%">
               
                    <thead class="thead-dark">
                    <tr>
                            <th>S.No</th>
                            <th>Title</th>
                            <th>Course</th>
                            <th>Total Inst.</th>
                            <th>Added On</th>
                            
<th>Actions</th>
                       
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                $counter=($this->request->params['paging']['Installment']['page']-1) * $this->request->params['paging']['Document']['perPage']; 
                if(isset($rec) && !empty($rec)){ 
                foreach($rec as $product){//pr($product);die;?>
                  <tr>
                    <td><?php echo $counter+1;?></td>
                    <td><b><?php echo $product['inst_name'] ?></b></td>
                    <td><?php echo $product['course']['cname']; ?></td>
                    <td><?php echo  $product['total_ins'] ?></td>
                    <td><?php echo $product['add_date']; ?></td>
                    <td>
                   
                    <?php

if($product['status']!=1)
{ ?>
<a href="<?php echo ADMIN_URL?>course/make_supportiv_installment/inactive/<?php echo $product['inst_id'];?>"> <img src="<?php echo SITE_URL?>admin_images/inactive.png"></a>
<?php }
else
{  ?>
   <a href="<?php echo ADMIN_URL?>course/make_supportiv_installment/active/<?php echo $product['inst_id'];?>"> <img src="<?php echo SITE_URL?>admin_images/active.png"></a>
<?php }
?>





                     <?php  echo $this->Html->link(__(''), ['action' => 'edit', $product->inst_id,],array('class'=>'fa fa-pencil-square-o fa-lg','title'=>'Edit','style'=>'font-size: 20px !important; margin-left: 12px;')) ?>
                     

                     <?php echo $this->Html->link('', ['action' => 'delete',$product->id],['title'=>'Delete','class'=> 'fa fa-trash','style'=>'color:#FF0000; margin-left: 13px; font-size: 19px !important;',"onClick"=>"javascript: return confirm('Are you sure do you want to delete this Course')"]); ?>
                   </td>
                 </tr>
                 <?php $counter++;} } ?>  





                       
                    </tbody>
                </table>
                <?php  //if(count($destination)>10){ //pr($events);?>
                    <?php echo $this->element('admin/pagination'); ?> 
            </div>
       
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
       </div>
      <!-- /.col -->  
    </div>
     <!-- /.row -->      
  </section>
    <!-- /.content -->  
       
 </div>     
     <!-- /.   content-wrapper -->  
     
     
