
      <table  class="table table-bordered table-striped afTblStndRept" id="example33" width="100%">
              <thead class="thead-dark">
              <tr>
              <th style="width:40px;" class="head0">S.No.</th>
              <th style="width:50px;" class="head0">Ref. No.</th>
              <th style="width:100px;" class="head0">Student Name</th>
              <th style="width:100px;" class="head0">Mobile</th>
              <th style="width:100px;" class="head0 ">Enrollment No.</th>
              <th style="width:140px;" class="head0 ">Course</th>
              <!--<th  style="width:300px;"  class="head0 ">Archive</th>-->
              <th class="head0" style="width:100px;">Add Date</th>
              <th style="width:100px;" class="head0 ">Certificate Issue</th>
              <?php if ($this->request->session()->read("Auth.User.role_id") == 1) { ?>
              <th style="width:100px;" class="head1">Actions</th>
              <?php } ?>
                </tr>
              </thead>
              <tbody>
                   <?php if (count($destination) > 0) { //pr(count($destination));?>
            <?php $cnt = 1;foreach ($destination as $key => $value) {
	$branch_id = $this->Comman->getBranch($value['s_enroll']);
	if ($this->request->session()->read("Auth.User.branch") != 0) {
		if ($branch_id['branch_id'] == $this->request->session()->read("Auth.User.branch")) {
			?>
            <tr class="gradeX">
              
              <td><?php echo $cnt;
			$cnt++; ?></td>
              <td><?php echo $value['cid']; ?></td>
              <td><?php echo ucwords($value['ctitle']); ?></td>
              <td><?php echo $value['mobile']; ?></td>
              <td><?php echo strtoupper($value['s_enroll']); ?></td>
              <td><?php echo ucwords($value['course']['sname']); ?></td>
              <!--<td>Move to Archive</td>-->
              <td><?php echo strftime('%d-%b-%Y', strtotime($value['add_date'])); ?></td>
              <td>
                <div class="deliver-buttton">
                  <div class="dlvr-optn">
                    <p><?php echo $value['delivery_date']; ?></p>
                    <p><?php echo $value['comment']; ?></p>
                  </div>
                  <?php if ($value['issue'] == 'P') {?>
                  <!--<a class='inline' href="#inline_contentdrop<?php //echo $value['cid']; ?>"></a>-->
                  <a href="#modal-i3-<?php echo $value['cid']; ?>" class="inline">Undelivered</a>
                  
                 <?php } else {?>
                  <?php echo "Delivered"; ?>
                  <?php }?>
                </div>
              </td>
              <?php if ($this->request->session()->read("Auth.User.role_id") == 1) { ?>
              <td>
                             
<?php 
			         	if ($value['issue'] != "D") { ?>
                <?php echo $this->Html->link(__(''), array('action' => 'delete', $value['cid'], $value['s_id']), array('class' => 'fa fa-trash', 'style'=>'font-size: 20px !important;color: indianred;','title' => 'Delete'), __('Are you sure you want to delete # %s?', $value['cid'])); } ?>
                

                <a target="_blank" href="<?php echo ADMIN_URL; ?>students/mprint/<?php echo $value['s_enroll']; ?>/<?php echo $value['cid']; ?>" class="fa fa-file-pdf-o" style="font-size: 20px !important;color: indianred;" title="Print PDF"></a>
         
              </td>
        <?php } ?>
            </tr>
            <?php }} else {?>
            <tr class="gradeX">
            
              <td><?php echo $cnt;
		$cnt++; ?></td>
              <td><?php echo $value['cid']; ?></td>
              <td><?php echo ucwords(strtolower($value['ctitle'])); ?></td>
              <td><?php echo $value['mobile']; ?></td>
              <td><?php echo strtoupper($value['s_enroll']); ?></td>
              <td><?php echo ucwords($value['course']['sname']); ?></td>
              <!--<td>Move to Archive</td>-->
              <td><?php echo strftime('%d-%b-%Y', strtotime($value['add_date'])); ?></td>
              <td>
                <div class="deliver-buttton">
                  <div class="dlvr-optn">
                    <p><?php echo $value['delivery_date']; ?></p>
                    <p><?php echo $value['comment']; ?></p>
                  </div>
                  <?php if ($value['issue'] == 'P') {?>
                  <!--<a class='inline' href="#inline_contentdrop<?php //echo $value['cid']; ?>">Pending</a>-->
                  <a href="#modal-i3-<?php echo $value['cid']; ?>" class="inline">Undelivered</a>
                  <?php } else if ($value['issue'] == 'D') {?>
                   <?php echo "delivered"; ?>
                  <?php } else {
			echo "Unprinted";
		}?>
                </div>
              </td>
              <?php if ($this->request->session()->read("Auth.User.role_id") == 1) { ?>
              <td>
            <?php     
			$entoll = str_replace(' ', '', $value['s_enroll']);
			if ($value['issue'] != 'D') {?>
                <?php echo $this->Html->link(__(''), array('action' => 'delete', $value['cid'], $value['s_id']), array('class' => 'fa fa-trash', 'title' => 'Delete','style'=>'font-size: 20px !important;color: indianred;'), __('Are you sure you want to delete # %s?', $value['cid']));} ?>

                <a target="_blank" href="<?php echo ADMIN_URL; ?>students/mprint/<?php echo $entoll; ?>/<?php echo $value['cid']; ?>" class="fa fa-file-pdf-o" style="font-size: 20px !important;color: indianred;" title="Print PDF"></a>
               
              </td>
      <?php } ?>
            </tr>
            <?php } } ?>
            <?php } else { ?>
            <tr>
              <td colspan="7" align="center">No data Available</td>
            </tr>
            <?php } ?>
                        
                    </tbody>
                </table>
                <?php  //if(count($destination)>10){ //pr($events);?>
                  <?php echo $this->element('admin/pagination'); ?> 
           