<style type="text/css">
   th{
text-align: center;
   }
   td{
    text-align: center;

   }


   .action_icon a span {
    font-size: 14px !important;
}
 .action_icon a {
    font-size: 13px !important;
}
.action_icon b i {
    font-size: 13px !important;
}

.action_icon a {
    display: inline-block;
    width: 21px;
    text-align: center;
}
 </style>
 <div class="content-wrapper">
   <section class="content-header">
    <h1>
    Certificate Manager
    </h1>
    <ol class="breadcrumb">
    <li><a href="<?php echo SITE_URL; ?>admin/dashboards"><i class="fa fa-home"></i>Home</a></li>
<li><a href="<?php echo SITE_URL; ?>admin/certificate">Certificate List</a></li>
    </ol> 
    </section> 

    <!-- Main content -->
    <section class="content">
  <div class="row">
    <div class="col-xs-12">    
  <div class="box">
    <div class="box-header">
  <?php echo $this->Flash->render(); ?>            

  <?php $role_id=$this->request->session()->read('Auth.User.role_id'); ?>
            <script>          
              $(document).ready(function () { 
                $("#Mysubscriptions").on("click", function (event) {
                 $('.lds-facebook').show();
                  $.ajax({
                    async:true,
                    data:$("#Stsearch").serialize(),
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>certificate/search",
                    success:function (data) {
                    $('.lds-facebook').hide();   
                      $("#Mycity").html(data); },
                    });
                  return false;
                });
              });


              $(document).ready(function () { 
                $(".branch").bind("change", function (event) {
               $('.lds-facebook').show();
                  $.ajax({
                    async:true,
                    data:$("#Stsearch").serialize(),
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>certificate/search",
                    success:function (data) {
                  $('.lds-facebook').hide();   
                      $("#Mycity").html(data); },
                    });
                  return false;
                });
              });

              $(document).on('click', '.pagination a', function(e) {
                var target = $(this).attr('href');
                var res = target.replace("/certificate/search", "/certificate");
                window.location = res;
                return false;
              });
            </script>

           
            
            <div class="form-group studentDtlFrmGroup" >
            
  <div class="row">
  <div class="col-md-10">
             <?php echo $this->Form->create('Stsearch',array('type'=>'get','inputDefaults'=>array('div'=>false,'label'=>false),'id'=>'Stsearch','class'=>'form-horizontal','method'=>'POST')); 
             
             $cissue = array('1' => 'Undelivered', '2' => 'Delivered');?>

             
             <div class="row">

             <div class="col-sm-2">  
            <?php echo $this->Form->input('name',array('class'=>'form-control branch','label' =>false,'placeholder'=>'Search Student Name..','autocomplete'=>'off')); ?>  
              </div> 
              <div class="col-sm-2">

<?php echo $this->Form->input('enroll',array('class'=>'form-control branch','label' =>false,'placeholder'=>'Search Enrollment Number..','autocomplete'=>'off')); ?>  
</div>
             <div class="col-sm-2">

            <?php echo $this->Form->input('issue', array('class' => 
             'form-control branch','id'=>'course_id','label'=>false,'options'=>$cissue,'empty'=>'--Select Certificate--','autofocus')); ?>
           </div>

           <div class="col-sm-2">

            <?php echo $this->Form->input('year', array('class' => 
             'form-control branch','id'=>'year','label'=>false,'options'=>$dates,'empty'=>'--Select Year--','autofocus')); ?>
           </div>
         
  
             

             
           
            </div>
        



<style>
        .addnn , .addnn:hover{ height:55px; width: 55px; border-radius: 50%; background-color: #2d95e3; line-height: 15px; font-size: 14px !important; text-align: center; color: #fff; display: inline-block; text-decoration: none;}
        .addnn i{ font-size:18px; color:#fff; margin-top:10px;}
        .studentDtlFrmGroup .row{ display:flex; flex-wrap:wrap; width:100%;}
        .studentDtlFrmGroup .row>.col-md-10, .studentDtlFrmGroup .row>.col-md-2{ align-self:center !important;}
        .studentDtlFrmGroup .row>.col-md-10 form{ margin-bottom:0px !important;}

        .btn-default:hover, .btn-default.focus, .btn-default:focus, .btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default.focus:active, .btn-default:active:focus, .btn-default:active:hover{ animation-name: none !important;}
    </style>

            
              
<?php if($this->request->session()->read("Auth.User.role_id")==4){  
  $branchID = $this->request->session()->read("Auth.User.branch");
 echo $this->Form->input('branch_id', array('class' => 'smallselect branch','type' => 'hidden','value'=>$branchID));
}

 ?>
   <?php /*          
              <div class="col-sm-1">
                <label for="inputEmail3" class="control-label" style="color:white">Reset</label>
                <input type="reset" style="background-color:#d33c44;color:#fff;"  class="btn btn-success" value="Reset">       
              </div> 
              <div class="col-sm-1" style="margin-left: -40px;">
                <label for="inputEmail3" class="control-label" style="color:white">Search</label>
                <input type="submit" style="background-color:green;color:#fff;" id="Mysubscriptions" class="btn btn-success" value="Search">       
              </div> */ ?>
              
              <?php echo $this->Form->end(); ?> 
</div>  
 
  <div class="col-md-2 pull-right">
    <ul class="list-inline w-75 m-auto text-center d-flex justify-content-around">
    <li class="list-inline-item ">

           <a class="addnn" href="<?php echo ADMIN_URL; ?>certificate/add" ><i class="fa fa-user-plus" aria-hidden="true"></i>
     <br> Add</a> 
            </li>

         


            </ul>
  <style>
        .addnn , .addnn:hover{ height:55px; width: 55px; border-radius: 50%; background-color: #2d95e3; line-height: 15px; font-size: 14px !important; text-align: center; color: #fff; display: inline-block; text-decoration: none;}
        .addnn i{ font-size:18px; color:#fff; margin-top:10px;}
        .studentDtlFrmGroup{ display:flex; flex-wrap:wrap;}
        .studentDtlFrmGroup>.col-md-10, .studentDtlFrmGroup>.col-md-2{ align-self:center !important;}
    </style>

            
              

   <?php /*          
              <div class="col-sm-1">
                <label for="inputEmail3" class="control-label" style="color:white">Reset</label>
                <input type="reset" style="background-color:#d33c44;color:#fff;"  class="btn btn-success" value="Reset">       
              </div> 
              <div class="col-sm-1" style="margin-left: -40px;">
                <label for="inputEmail3" class="control-label" style="color:white">Search</label>
                <input type="submit" style="background-color:green;color:#fff;" id="Mysubscriptions" class="btn btn-success" value="Search">       
              </div> */ ?>
              </div>  
              
            
      
       

   
     </div><!-- /.box-header -->
 <div class="box-body" id="Mycity" style="
    width: 100%;
"> 
          
             

      <table  class="table table-bordered table-striped afTblStndRept" id="example33" width="100%">
              <thead class="thead-dark">
              <tr>
              <th style="width:40px;" class="head0">S.No.</th>
              <th style="width:50px;" class="head0">Ref. No.</th>
              <th style="width:100px;" class="head0">Student Name</th>
              <th style="width:100px;" class="head0">Mobile</th>
              <th style="width:100px;" class="head0 ">Enrollment No.</th>
              <th style="width:140px;" class="head0 ">Course</th>
              <!--<th  style="width:300px;"  class="head0 ">Archive</th>-->
              <th class="head0" style="width:100px;">Add Date</th>
              <th style="width:100px;" class="head0 ">Certificate Issue</th>
              <?php if ($this->request->session()->read("Auth.User.role_id") == 1) { ?>
              <th style="width:100px;" class="head1">Actions</th>
              <?php } ?>
                </tr>
              </thead>
              <tbody>
                   <?php if (count($destination) > 0) { //pr(count($destination));?>
            <?php $cnt = 1;foreach ($destination as $key => $value) {
	$branch_id = $this->Comman->getBranch($value['s_enroll']);
	if ($this->request->session()->read("Auth.User.branch") != 0) {
		if ($branch_id['branch_id'] == $this->request->session()->read("Auth.User.branch")) {
			?>
            <tr class="gradeX">
              
              <td><?php echo $cnt;
			$cnt++; ?></td>
              <td><?php echo $value['cid']; ?></td>
              <td><?php echo ucwords($value['ctitle']); ?></td>
              <td><?php echo $value['mobile']; ?></td>
              <td><?php echo strtoupper($value['s_enroll']); ?></td>
              <td><?php echo ucwords($value['course']['sname']); ?></td>
              <!--<td>Move to Archive</td>-->
              <td><?php echo strftime('%d-%b-%Y', strtotime($value['add_date'])); ?></td>
              <td>
                <div class="deliver-buttton">
                  <div class="dlvr-optn">
                    <p><?php echo $value['delivery_date']; ?></p>
                    <p><?php echo $value['comment']; ?></p>
                  </div>
                  <?php if ($value['issue'] == 'P') {?>
                  <!--<a class='inline' href="#inline_contentdrop<?php //echo $value['cid']; ?>"></a>-->
                  <a href="#modal-i3-<?php echo $value['cid']; ?>" class="inline">Undelivered</a>
                  
                 <?php } else {?>
                  <?php echo "Delivered"; ?>
                  <?php }?>
                </div>
              </td>
              <?php if ($this->request->session()->read("Auth.User.role_id") == 1) { ?>
              <td>
                             
<?php 
			         	if ($value['issue'] != "D") { ?>
                <?php echo $this->Html->link(__(''), array('action' => 'delete', $value['cid'], $value['s_id']), array('class' => 'fa fa-trash', 'style'=>'font-size: 20px !important;color: indianred;','title' => 'Delete'), __('Are you sure you want to delete # %s?', $value['cid'])); } ?>
                

                <a target="_blank" href="<?php echo ADMIN_URL; ?>students/mprint/<?php echo $value['s_enroll']; ?>/<?php echo $value['cid']; ?>" class="fa fa-file-pdf-o" style="font-size: 20px !important;color: indianred;" title="Print PDF"></a>
         
              </td>
        <?php } ?>
            </tr>
            <?php }} else {?>
            <tr class="gradeX">
            
              <td><?php echo $cnt;
		$cnt++; ?></td>
              <td><?php echo $value['cid']; ?></td>
              <td><?php echo ucwords(strtolower($value['ctitle'])); ?></td>
              <td><?php echo $value['mobile']; ?></td>
              <td><?php echo strtoupper($value['s_enroll']); ?></td>
              <td><?php echo ucwords($value['course']['sname']); ?></td>
              <!--<td>Move to Archive</td>-->
              <td><?php echo strftime('%d-%b-%Y', strtotime($value['add_date'])); ?></td>
              <td>
                <div class="deliver-buttton">
                  <div class="dlvr-optn">
                    <p><?php echo $value['delivery_date']; ?></p>
                    <p><?php echo $value['comment']; ?></p>
                  </div>
                  <?php if ($value['issue'] == 'P') {?>
                  <!--<a class='inline' href="#inline_contentdrop<?php //echo $value['cid']; ?>">Pending</a>-->
                  <a href="#modal-i3-<?php echo $value['cid']; ?>" class="inline">Undelivered</a>
                  <?php } else if ($value['issue'] == 'D') {?>
                   <?php echo "delivered"; ?>
                  <?php } else {
			echo "Unprinted";
		}?>
                </div>
              </td>
              <?php if ($this->request->session()->read("Auth.User.role_id") == 1) { ?>
              <td>
            <?php     
			$entoll = str_replace(' ', '', $value['s_enroll']);
			if ($value['issue'] != 'D') {?>
                <?php echo $this->Html->link(__(''), array('action' => 'delete', $value['cid'], $value['s_id']), array('class' => 'fa fa-trash', 'title' => 'Delete','style'=>'font-size: 20px !important;color: indianred;'), __('Are you sure you want to delete # %s?', $value['cid']));} ?>

                <a target="_blank" href="<?php echo ADMIN_URL; ?>students/mprint/<?php echo $entoll; ?>/<?php echo $value['cid']; ?>" class="fa fa-file-pdf-o" style="font-size: 20px !important;color: indianred;" title="Print PDF"></a>
               
              </td>
      <?php } ?>
            </tr>
            <?php }}?>
            <?php } else {?>
            <tr>
              <td colspan="7" align="center">No data Available</td>
            </tr>
            <?php }?>
                        
                    </tbody>
                </table>
                <?php  //if(count($destination)>10){ //pr($events);?>
                  <?php echo $this->element('admin/pagination'); ?> 
            </div>
       
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
       </div>
      <!-- /.col -->  
    </div>
     <!-- /.row -->      
  </section>
    <!-- /.content -->  
       
 </div>     
