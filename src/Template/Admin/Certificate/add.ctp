<script type="text/javascript">
  function isLspecial(e) {
   var e = e || window.event;
   var k = e.which || e.keyCode;
   var s = String.fromCharCode(k);
   if(/^[\\\"\'\;\:\>\<\[\]\/\?\=\+\_\|~`!@#\$%^&*\(\)0-9]$/i.test(s)){
     $('#msg').css('display','block');
     return false;
   }
   $('#msg').hide();
 }
</script>

<script type="text/javascript">
  function isCspecial(e) {
   var e = e || window.event;
   var k = e.which || e.keyCode;
   var s = String.fromCharCode(k);
   if(/^[\\\"\'\;\:\>\<\[\]\-\.\,\/\?\=\+\_\|~`!@#\$%^&*\(\)0-9]$/i.test(s)){
    alert("Special characters not acceptable");
    return false;
  }
}
</script>

<style type="text/css">
	.text{color:red; font-size: 12px;}
	.filename{font-size: 11px;color: #e02727;}
	.fr-wrapper>div:first-child>a:first-child{display:none !important;}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
    Certificate Manager
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo SITE_URL; ?>admin/placement"><i class="fa fa-home"></i>Home</a></li>
			<li><a href="<?php echo SITE_URL; ?>admin/placement">Manage Certificate</a></li>
			<?php if(isset($newpack['id'])){ ?>
				<li class="active"><a href="javascript:void(0)">Edit Certificate</a></li>   
			<?php } else { ?>
				<li class="active"><a href="javascript:void(0)">Add Certificate</a></li>
			<?php } ?>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content addCrsMngPg">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<?php echo $this->Flash->render(); ?>
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-plus-square" aria-hidden="true"></i> 
	<?php if(isset($newpack['id'])){ ?> Edit Certificate <?php } else { ?> 	Add Certificate <?php } ?></h3>
					</div>
					<?php echo $this->Form->create($newpack, array(
						'enctype' => 'multipart/form-data',
						'class'=>'needs-validation',
						'novalidate'
						
					));
				
					 ?>
					<div class="box-body addStdntsFrm">

  <div class="row">
  <script>          
              $(document).ready(function () { 
                $("#cpy").on("change", function (event) {
              var instid= $(this).val();
             
                  $.ajax({
                    async:true,
                    data:{'s_enroll':instid},
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>certificate/ajx",
                    success:function (data) {
                    //  $('.lds-facebook').hide();   
                      $("#srrc").html(data);
                     },
                    });
                  return false;
                });
              });
              </script>
<div class="col-sm-3" >
   <label for="exampleInputEmail1">Enrollment No.</label>
                <?php echo $this->Form->input('s_enroll', array('class' => 
                'form-control','id'=>'cpy','placeholder'=>'Enter Enrollment No.','label'=>false,'autocomplete'=>'off','required')); ?>
                
    </div>
    </div>

    <div id="srrc" class="row">


    <div class="col-sm-3" >
     <label for="exampleInputEmail1">Student Name</label>
   <?php echo $this->Form->input('ctitle', array('class' => 
       'form-control','placeholder'=>'Enter Student Name','label'=>false,'autocomplete'=>'off','required')); ?>
    </div>

   

  
    <div class="col-sm-2" >
     <label for="exampleInputEmail1">Mobile</label>
   <?php echo $this->Form->input('mobile', array('class' => 
       'form-control','placeholder'=>'Enter Mobile','onkeypress'=>'return isNumber(event);','label'=>false,'autocomplete'=>'off','required')); ?>
    </div>

    <div class="col-sm-2" >
     <label for="exampleInputEmail1">Select Course</label>
  
     <?php echo $this->Form->input('course_id', array('class' => 
                'form-control','options' =>$categ1,'id'=>'cour','label'=>false,'autocomplete'=>'off','required')); ?>
    </div>
    <div class="col-sm-2" >
     <label for="exampleInputEmail1">Duration (From)</label>
     <div class="input-append date" id="datepickerbrth" data-date="<?php echo date('d-m-Y',strtotime($newpack['pre_date'])); ?>" data-date-format="dd-mm-yyyy"> 
							
              <input class="span2 form-control" size="16" placeholder="Job Posting Date" type="date" name="pre_date" value="<?php echo date('d-m-Y',strtotime($newpack['pre_date'])); ?>">  <span class="add-on"><i class="icon-th"></i></span>
            
            </div>
    </div>

    <div class="col-sm-2" >
     <label for="exampleInputEmail1">Duration (To)</label>
     <div class="input-append date" id="datepickerbrth" data-date="<?php echo date('d-m-Y',strtotime($newpack['end_date'])); ?>" data-date-format="dd-mm-yyyy"> 
			 <input class="span2 form-control" size="16" placeholder="Job Posting Date" type="date" name="end_date" value="<?php echo date('d-m-Y',strtotime($newpack['end_date'])); ?>">  <span class="add-on"><i class="icon-th"></i></span>
            
            </div>
    </div>


    <div class="box-footer">
      <?php
        if(isset($newpack['id'])){
        echo $this->Form->submit(
            'Update', 
            array('class' => 'btn btn-info pull-right', 'title' => 'Update')
        ); }else{ 
        echo $this->Form->submit(
            'Add', 
            array('class' => 'btn btn-info pull-right', 'title' => 'Add')
        );
        }
           ?>
          </div>

    </div>


    
   
  </div>
 
  </div>

          
          <!-- /.box-footer -->
 <?php echo $this->Form->end(); ?>
          </div>
      
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div> 

  <script>

function isNumber(evt) {
  evt = (evt) ? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
    alert("Please Insert Only Numeric Characters!!");
    return false;
  }
  return true;

}
</script>

  