<?php echo $this->Form->create('Student',array('type'=>'file','inputDefaults'=>array('div'=>false,'label'=>false),'class'=>'stdform dropupStudPop addStdntsFrm needs-validation')); ?> 
        <div class="feespopup" id='inline_contentinline_contentdrop<?php echo $cid; ?>' style=' background:#fff;'> <?php
  $ins=$this->Comman->instl($sid,$insid); ?>
  <div class="fplan">  

  <div class="row">
			<div class="col-md-12">
			

				<div class="box box-info boxInfoBx boxInfoBxStudentInfo">
  <div class="box-body addStdntsFrm">
                    <div class="row d-flex">
<div class="col-sm-4 align-self-center" ><?php 
$path=LOCALWEB.$stu_det['image'];
//echo $path; die;
if(!empty($stu_det['image']) && file_exists($path)){ ?>
<img src="<?php echo SITE_URL;?>images/student/<?php echo $stu_det['image']?>" alt="img" style="width:100px;height:100px"> <?php } else{ ?><img src="https://daac.in/images/student/noimage.jpg" alt="img" style="width:100px;height:100px"><?php } ?></div>
<!--  -->

               <div class="col-sm-8 align-self-center">
               <h3><?php echo ucwords($stu_det['s_name'])?></h3>
               <table style="width:100%">
               <tbody><tr style="width:100%"><td style="text-align:left; width:30%; padding-right:2%"><b>Enroll. No</b></td><td style="text-align:left ;"><b>:</b> <?php echo $stu_det['s_enroll']?></td></tr>
                           <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Father's Name</b></td><td style="text-align:left ;"><b>:</b>  <?php echo ucwords($stu_det['f_name'])?></td></tr>
                <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Mobile No</b></td><td style="text-align:left ;"><b>:</b>    <?php echo $stu_det['s_contact']?></td></tr>
                <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Branch</b></td><td style="text-align:left ;"><b>:</b>    <?php echo $stu_det['branch']['name']?></td></tr>
               </tbody></table>            
               </div>
              
               </div>
               
                </div></div>
              
              </div>
              
               </div>
<table class="table table-bordered table-striped afTblStndRept" width="100%">
               
               <thead class="thead-dark">
	<tr>

		<th style="text-align:center;   width:30px;"><strong>
      S.No.
    </strong></th>
   
    <th style="text-align:center; width:105px; "><strong>
      Fee Date
    </strong></th>
    <th style="text-align:center; "><strong>
      Pay Date
    </strong></th>
    <th style="text-align:center;  width:145px;"><strong>
      Amount
    </strong></th>
    <th style="text-align:center;"><strong>Receipt</strong></th>
    <th style="text-align:center;  width:145px;"><strong>
      Fine
    </strong></th>
   
    </tr>
    </thead>
    <tbody>
<?php if(count($ins)>0){  ?>

                    <?php $cnt=1;  foreach($ins as $key=>$vlv1) {  ?>
    <tr>

		<td style="text-align:center; width:30px;">
     <?php echo $cnt;?>
    </td>
    
    <td style="text-align:center; width:145px;">
     <?php echo date('d-M-Y',strtotime($vlv1['pro_date']));?>
    </td>
    <td style="text-align:center; width:145px;">
 <?php $cdf=date('d-m-Y',strtotime($vlv1['pay_date'])); 
 echo ($cdf!='01-01-1970') ? date('d-M-Y',strtotime($vlv1['pay_date'])) : "UNPAID";?>
    </td>
    <td style="text-align:center; width:145px;">
     <?php echo $vlv1['amount'];?>
    </td>
    <td style="text-align:center; width:145px;">
     <?php echo $vlv1['fine'];?>
    </td>
     <td style="text-align:center; width:145px;"> 
     <?php echo $vlv1['fine'];?>
    </td>
    </tr>
    <?php $cnt++; }
   } else {  ?>    
                    <tr><td colspan="7" align="center">No Meta Available</td></tr>
                    <?php } ?> 
    <tr>
    <td colspan=6 style="text-align:center; width:145px;color:red;font-size: 14px !important;">All unpaid installments will be deleted. Do you still want to drop ? </td>
    </tr>
    
     <!-- <tr>



    <td colspan="6" style="text-align:center; width:145px;">


    </td>
    
    </tr> -->
  	
    
</tbody>
</table>
  
<?php echo $this->Form->textarea('reason',array('class' => 'longinput','required','style' => 'width:100%','required','placeholder'=>'Mention Reason')); ?>
<?php echo $this->Form->hidden('s_id',array('class' => 'longinput','required','value'=>$sid)); ?>
<?php echo $this->Form->hidden('instid',array('class' => 'longinput','required','value'=>$insid)); ?>
<?php echo $this->Form->hidden('corsid',array('class' => 'longinput','required','value'=>$cid)); ?>


</div>
</div>
<div class="modal-footer box footer">
        <!-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> -->
        
        <?php echo $this->Form->submit('Drop',array('class' => 'btn btn-success','value'=>$sid)); ?>
      </div>
      <?php echo $this->Form->end();?> 
<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
    
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>