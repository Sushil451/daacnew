<!-- <script>
   function checkextension() {
    var file = document.querySelector("#fUpload");
    if ( /\.(jpe?g|png|gif)$/i.test(file.files[0].name) === false ) 
      { alert("not an image please choose a image!");
    $('#fUpload').val('');
  }
  return false;
}
</script> -->
<style type="text/css">
	.text{color:red; font-size: 12px;}
	.filename{font-size: 11px;color: #e02727;}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Product Manager
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo SITE_URL; ?>admin/dashboards"><i class="fa fa-home"></i>Home</a></li>
			<li><a href="<?php echo SITE_URL; ?>admin/product">Manage Product</a></li>
			<?php if(isset($product['id'])){ ?>
				<li class="active"><a href="javascript:void(0)">Edit Product</a></li>   
			<?php } else { ?>
				<li class="active"><a href="javascript:void(0)">Add Product</a></li>
			<?php } ?>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<?php echo $this->Flash->render(); ?>
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-plus-square" aria-hidden="true"></i>Edit Product</h3>
					</div>
					<?php echo $this->Form->create($product, array(
						'class'=>'form-horizontal',
						'enctype' => 'multipart/form-data',
						'validate'
					));
				
					 ?>
					<div class="box-body">
						<div class="form-group">
				<div class="col-sm-3">
		<label for="inputEmail3" class="control-label">Student Name</label>
       <input type="hidden" name="prev_image" value="<?php echo $this->request->data['image'];?>">
		<?php echo $this->Form->input('s_name', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Student Name','autofocus','autocomplete'=>'off')); ?>   
				
							</div>
							<div class="col-sm-3">
								<label for="inputEmail3" class="control-label">Mobile</label>
								<?php echo $this->Form->input('s_contact', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Mobile','autocomplete'=>'off','type'=>'text')); ?>   

                                <input type="hidden" name="image" id="image" value="<?php echo $this->request->data['image']; ?>">
							</div>
							<div class="col-sm-3">
								<label for="inputEmail3" class="control-label">Father's Name</label>
                                <?php echo $this->Form->input('f_name', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Fathers Name','autocomplete'=>'off','type'=>'text')); ?>
							</div>
							<div class="col-sm-3" id="subcat">
								<label for="inputEmail3" class="control-label">Alternate Mobile</label>

								<?php echo $this->Form->input('f_contact', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Alternate Mobile','autocomplete'=>'off','type'=>'number')); ?>


							
							</div>
							<div class="col-sm-3">
								<label for="inputEmail3" class="control-label">Father's Occupation</label>

								<?php echo $this->Form->input('f_occupation', array('class' => 'form-control','required','label'=>false,'placeholder'=>"Father's Occupation",'autocomplete'=>'off','type'=>'number')); ?>

								   
							</div>

							<div class="col-sm-3">
								<label for="inputEmail3" class="control-label">Course</label>
								<?php echo $this->Form->input('discounted_price', array('class' => 'form-control input2','required','label'=>false,'placeholder'=>'Discounted Price','autocomplete'=>'off','type'=>'number','min'=>'1','step'=>'.01')); ?>   
							</div>

							<div class="col-sm-3">
								<label for="inputEmail3" class="control-label">Select GST</label>
								<?php echo $this->Form->input('gst_id', array('class' => 
								'form-control','id'=>'gst','label'=>false,'options'=>$gst,'empty'=>'--Select GST--','required','autofocus')); ?>
							</div>

							<div class="col-sm-3">
								<label for="inputEmail3" class="control-label">Quantity</label>
								<?php echo $this->Form->input('total_quantity', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Quantity','autocomplete'=>'off','type'=>'number','min'=>0)); ?>   
							</div>

							<div class="col-sm-3">
								<label for="inputEmail3" class="control-label">Minimum Quantity</label>
								<?php echo $this->Form->input('minimum_quantity', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Minimum Quantity','autocomplete'=>'off','type'=>'number','min'=>0)); ?>   
							</div>

							<div class="col-sm-3">
								<label for="inputEmail3" class="control-label">Select Brand</label>
								<?php echo $this->Form->input('brand_id', array('class' => 
								'form-control','id'=>'cat','label'=>false,'options'=>$brandname,'empty'=>'--Select Brand--','autofocus')); ?>
							</div>
							<div class="col-sm-3">
								<label for="inputEmail3" class="control-label">Select Attribute Type</label>
								<?php echo $this->Form->input('attribute_id', array('class' => 
								'form-control','id'=>'attribute','label'=>false,'options'=>$attributes,'empty'=>'--Select Attribute--','autofocus')); ?>
							</div>

							<div class="col-sm-3" id="subattribute">
								<label for="inputEmail3" class="control-label">Select Attribute</label>
								<select name="sub_attribute_id[]" multiple style="width: -webkit-fill-available;">
								<?php foreach($subattributes as $value){ 
								$products_attribute=$this->Comman->ProductAttribute($value['id'],$product['id']); ?>
								<?php if($products_attribute>0) {?>
								<option value="<?php echo $value['id']; ?>" selected><?php echo $value['name']; ?>(<?php echo $value['description']; ?>)</option>
								<?php }else{ ?>
								<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?>(<?php echo $value['description']; ?>)</option>
								<?php } ?>
								<?php }?>
                               </select>
							</div>
							</div>
							<div class="form-group">
							<div class="col-sm-3">
              <label for="inputEmail3" class="control-label">Upload Featured Image</label>
              <?php if($product['featured_image']){ ?>
                <?php echo $this->Form->input('featured_image', array('class' => 
                'form-control','autocomplete' => 'off','label'=>false,'type'=>'file','id'=>'fUpload','onchange'=>'checkextension()')); ?>
                <img src="<?php echo SITE_URL;?>images/products/<?php echo $product['featured_image']; ?>" height="50px" width="50px" style="margin-top: 16px;">
              <?php }else{ ?>
               <?php echo $this->Form->input('featured_image', array('class' => 
               'form-control','autocomplete' => 'off','label'=>false,'type'=>'file','id'=>'fUpload','onchange'=>'checkextension()','required')); ?>
             <?php } ?>
           </div>
							<div class="col-sm-3">
								<label for="inputEmail3" class="control-label">Upload Image</label>
								<?php if($products_img){ ?>
									<?php echo $this->Form->input('image[]', array('class' => 
									'form-control','autocomplete' => 'off','label'=>false,'type'=>'file','id'=>'fUpload','onchange'=>'checkextension()','multiple')); ?>
									<?php foreach($products_img as $value) {?>
										<div class="col-sm-4" style="padding-top: 20px;">
											<img src="<?php echo SITE_URL;?>images/products/<?php echo $value['image']; ?>" height="50px" width="50px">
											<?php echo $this->Html->link('', ['action' => 'proimagedelete',$value->id],['title'=>'Delete','class'=> 'fa fa-trash','style'=>'color:#FF0000; margin-left: 52px; margin-top: -53px; font-size: 19px !important;',"onClick"=>"javascript: return confirm('Are you sure do you want to delete this Product image')"]); ?>
										</div>
									<?php } ?>
								<?php }else{ ?>
									<?php echo $this->Form->input('image[]', array('class' => 
									'form-control','autocomplete' => 'off','label'=>false,'type'=>'file','id'=>'fUpload','onchange'=>'checkextension()','multiple','required')); ?>
								<?php } ?>
							</div>
							<div class="col-sm-6">
								<label for="inputEmail3" class="control-label">Description</label>
								<?php echo $this->Form->input('description', array('class' => 'form-control','label'=>false,'placeholder'=>'Description','autocomplete'=>'off','required','type'=>'textarea','id'=>'summernote')); ?>  
							</div>
							</div>
						</div> 
						<div class="box-footer">
							<?php echo $this->Form->submit('Update',array('class' => 'btn btn-info pull-right', 'title' => 'Update')); ?>
							<?php echo $this->Html->link('Back', ['action' => 'index'],['class'=>'btn btn-default']); ?>
						</div>
						<?php echo $this->Form->end(); ?>
					</div> 
				</div>
			</div>
		</section>
	</div> 
