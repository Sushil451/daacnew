<label for="inputEmail3" class="control-label">Enrollment No.</label>
<?php
echo $this->Form->input('s_enroll', array('class' => 'form-control','value'=>$enrollment,'readonly','type'=>'text','label'=>false));
echo $this->Form->input('branchhead', array('type'=>'hidden','class' => 'form-control','value'=>$branchhead,'readonly','label'=>false));
echo $this->Form->input('branch', array('type'=>'hidden','class' => 'form-control','value'=>$branch,'readonly','label'=>false));
if($br_det[0]['is_reg']=='N'){ ?>
<script>
$('#reg_fee').hide();
$('#rfee').val(0);
$('#rfee').prop('required',false);		
</script>
<?php } else{ ?>
<script>
$('#reg_fee').show();
$('#rfee').val('<?php echo $br_det[0]['registration_fee']?>');
$('#rfee').prop('required',true);
</script>
<?php }
?>