<!-- <script>
   function checkextension() {
    var file = document.querySelector("#fUpload");
    if ( /\.(jpe?g|png|gif)$/i.test(file.files[0].name) === false ) 
      { alert("not an image please choose a image!");
    $('#fUpload').val('');
  }
  return false;
}
</script> -->
<style type="text/css">
.box.box-info{ margin-bottom:30px;}
	.text{color:red; font-size: 12px;}
	.filename{font-size: 11px;color: #e02727;}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<i class="fa fa-plus-square" aria-hidden="true"></i>  
							 Add Existing Student 
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo SITE_URL; ?>admin/students"><i class="fa fa-home"></i>Home</a></li>
			<li><a href="<?php echo SITE_URL; ?>admin/students">Manage Student</a></li>
			<?php if(isset($newresponse['s_id'])){ ?>
				<li class="active"><a href="javascript:void(0)">Edit Student</a></li>   
			<?php } else { ?>
				<li class="active"><a href="javascript:void(0)">Add Student</a></li>
			<?php } ?>
		</ol>
	</section>
	<script>
function isNumber(evt) {

evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;
if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {

return false;
}
return true;
}

</script>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
			<?php echo $this->Form->create($newresponse, array(
						'class'=>'form-horizontal needs-validation addStdntsFrm',
						'type' => 'file',
						'validate','novalidate'
						
					));
				
					 ?>

				<div class="box box-info">
					<?php echo $this->Flash->render(); ?>
					<div class="box-header with-border">
						<h3 class="box-title">Student Info</h3>
					</div>


	<script>          
              $(document).ready(function () { 
                $("#enrl").bind("change", function (event) {
              var instid= $(this).val();
			  $('.lds-facebook').show();   
                  $.ajax({
                    async:true,
                    data:{'enrl':instid},
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>students/ajx",
                    success:function (data) {
                      $('.lds-facebook').hide();   
					$("#updt1").html('');
                      $("#updt1").html(data); },
                    });
                  return false;
                });
              });
              </script>
					
					<div class="box-body addStdntsFrm">
						<div class="row">

						<div class="col-sm-4 bkExistSinglIn" id="bk">
         

<label for="inputEmail3" class="control-label">Enter Existing Student Enrollment No. <strong style="color:red;">*</strong></label>
<?php 	 

echo $this->Form->input('s_enroll',array('class' => 'form-control','required','label'=>false,'id'=>'enrl'));

echo $this->Form->input('branchhead', array('type'=>'hidden','class' => 'form-control','value'=>$this->request->session()->read("Auth.User.branchhead"),'readonly','label'=>false));

echo $this->Form->input('branch', array('type'=>'hidden','class' => 'form-control','value'=>$this->request->session()->read("Auth.User.branch"),'readonly','label'=>false));

?>
	   
	   </div>
	   <div id="updt1" class="updt1ExistFrm">

</div>
<!--  -->

</div>
<!-- row -->

<div class="row" <?php if($newresponse['s_id']){ ?> style="display:none;" <?php } ?>>
							



							<div class="col-sm-4">
			<label for="inputEmail3" class="control-label">Course <strong style="color:red;">*</strong></label>

			<?php if($newresponse['s_id']){ ?>

				<?php }else{ ?>
			<?php echo $this->Form->input('course_id', array('class' =>'form-control','id'=>'course_id','label'=>false,'options'=>$categ1,'empty'=>'--Select Course--','required','autofocus')); ?>  

			<?php } ?>
							</div>

							<div class="col-sm-4 session" style="display:none">
          <label class="inputEmail3">Select Session</label>
     
            <?php
            $session=[];
              $m=date('m');
              $year=date('Y');
              if($m<6){
                $session['june-'.($year-1)]='June-'.($year-1);
                $session['june-'.$year]='June-'.$year;
                $session['june-'.($year+1)]='June-'.($year+1);
                }else{
                $session['june-'.$year]='June-'.$year;
                $session['june-'.($year+1)]='June-'.($year+1);
                }
              if($m<12){
                $session['dec-'.($year-1)]='December-'.($year-1);
                $session['dec-'.$year]='December-'.$year;
                $session['dec-'.($year+1)]='December-'.($year+1);
                }else{
                $session['dec-'.$year]='December-'.$year;
                $session['dec-'.($year+1)]='December-'.($year+1);
                }
                

 echo $this->Form->input('session', array('class' => 'smallselect form-control','empty'=>'--Select Session--','options' =>$session,'id'=>'session')); 
 ?>
          
        </div>


<?php $br_det['is_reg'] == 'Y' ? $reg_feess = $br_det['registration_fee'] : $reg_feess = "";?>
<div class="col-sm-4" id="reg_fee" 
<?php if($this->request->session()->read('Auth.User.role_id') == 1 || $br_det['is_reg'] == 'N'){ ?> style="display:none" <?php } ?>>
         

		<label for="inputEmail3" class="control-label">Registration Fees</label>
			<?php echo $this->Form->input('installment[reg]', array('class' =>'form-control','id'=>'rfee','type'=>'text','label'=>false,'readonly')); ?>  
      </div>
   
        <div class="col-sm-4" id="sk">
		 <div id="pk">
         <label class="control-label">Installment Type <strong style="color:red;">*</strong></label>
		 <div class="input text">
    <span class="field" id="kl"> 
	<div class="form-check">
  <input class="form-check-input chk" type="radio" name="inst_id" value="" id="flexRadioDefault">
  <label class="form-check-label" for="flexRadioDefault">
 Choose Course First
  </label>
</div> </span>
		 </div>
         </div>
         
        </div>

		
						

	<script>          
              $(document).ready(function () { 
                $("#brnch").on("click", function (event) {
              var instid= $(this).val();
             
                  $.ajax({
                    async:true,
                    data:{'branch':instid},
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>students/branch",
                    success:function (data) {
                    //  $('.lds-facebook').hide();   
                      $("#bk").html(data); },
                    });
                  return false;
                });
              });
              </script>

 



		<div id="fstbl" class="col-sm-12" >
             
			 </div>
			<!-- col-md-3 -->



						
						</div> 
<!-- row -->
<div class="box-footer" id="sub">
						<?php echo $this->Html->link('Back', ['action' => 'index'],['class'=>'btn btn-default']); ?>
						<?php if($newresponse['s_id']){ ?>
							<?php echo $this->Form->submit('Update',array('class' => 'btn btn-info pull-right', 'title' => 'Update')); ?>
							<?php }else{ ?>
							<?php echo $this->Form->submit('Save',array('class' => 'btn btn-info pull-right', 'title' => 'Save')); ?>
							<?php } ?>


					</div> 
					<!--  -->

					
					</div> 
					<!--  -->
		</div>
<!-- box-end -->






			

		</div>



		<?php /*
<!-- box-open -->
<div class="box box-info" >
				
					<!-- <div class="box-header with-border">
						<h3 class="box-title">Course Info</h3>
					</div> -->
			
					<div class="box-body addStdntsFrm">
					
						</div>
				
					</div> 
					<!--  -->

<!-- box-end --> */ ?>



<?php echo $this->Form->end(); ?>
				</div>
			</div>
		</section>
	</div> 
	<script>
$(document).ready(function(){
  var course_fees;
	$('#course_id').change(function(){
		$('.lds-facebook').show();
		//alert(userbranch);
		var id=$(this).val();
    var degreeCourses=<?php echo json_encode($degrecourseList) ?>;
    console.log(degreeCourses);
    if(Object.values(degreeCourses).includes(id)){
      $('.session').show();
    }else{
      $('.session').hide();
    }
		//alert(id);
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/checkcoursefeeflexibility',
		   type:'POST',
		   data:{'id12': id},
		   success:function(data){
			//alert(data);
			if(data.trim()!='0' && data.trim()!='1'){
       
		var userbranch='<?php echo $this->request->session()->read('Auth.User.branch'); ?>';   
		$('#rfee').val(data);
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/inst',
		   type:'POST',
		   data:{'id12': id, 'branch': userbranch},
		   success:function(data1){
			//alert(data1);
		
			if(data1.trim()=='5'){
				$('#sk').html('<div id="pk" style="color:red;"><label class=" text-right">Installment Type </label> <div class="input text"><span class="field" id="kl"> No Installment Type Added For This Course.</span></div></div>');
				
				$('#sub').hide();
			} else {
			$('#sk').html(data1);
			$('#sub').show();
		}
		   },

			});	   
		} 
		
		if(data.trim()=='1'){
			var userbranch='0';   
		
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/inst',
		   type:'POST',
		   data:{'id12': id, 'branch': userbranch},
		   success:function(data1){
			$('.lds-facebook').hide();
			$('#sk').html(data1);
			
		   },

			});
			
		} 
		
		if(data.trim()=='0'){
	

		$('#sub').hide();	
		$('#sk').html('<div id="pk" style="color:red;"><label class="text-right">Installment Type </label><div class="input text"><span class="field" id="kl"> Course is not approved by Admin.</span></div></div>');
				
				$('#sub').hide();
		} 
		   },

      });
     		
		});
	
	});
</script> 


<script>
$(function() {
$( "#datepicker" ).datepicker();

});
$(function() {
$( "#datepickerbrth" ).datepicker({minDate:null,changeYear:true,changeMonth:true,yearRange:"c-40:c"});

});



</script>


<script>

function getrefdata(data){
var type=data.value;
if(type==11){
  $('#updaterefdetails').css('display','block');
  $('#rname').attr('required',true);
  $('#rmobile').attr('required',true);
   $('#rmobile').val('<?php echo $this->request->data["rmobile"] ?>');

  $('#rname').val('<?php echo $this->request->data["rname"] ?>');
}
else{

  $('#rname').removeAttr('required');
  $('#rmobile').removeAttr('required');
  $('#rmobile').val('');

  $('#rname').val('');
    $('#updaterefdetails').css('display','none');

}
}
</script>
<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
    
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>