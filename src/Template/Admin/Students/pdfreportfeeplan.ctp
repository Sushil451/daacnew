<link rel="icon" href="<?php echo SITE_URL;?>images/favicon_icon.png" type="image/x-icon">
<?php //pr($order); die;
class xtcpdf extends TCPDF {
}

$pdf = new TCPDF('P', 'mm', 'A5', true, 'UTF-8', false, true);

// set document information
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
$pdf->SetMargins(5, 0, 10, true);
$pdf->AddPage();

$pdf->SetFont('', '', 7, '', 'false');
//$pdf->SetTextColor(0, 0, 0);
//$pdf->SetFont('Arial', '', 12);

$pdf->SetAutoPageBreak(false, 0);
//$pdf->SetMargins(5, 0, 5, true);



$html.='
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title><link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">';

$html.='<style>*{padding:0px; margin:0px}</style><div class="fplan" >
  <table cellspacing="0" cellpadding="2" border="0" style="padding-top:0px; margin:0px;">
  <tr style="margin-bottom:5px;">
  <th align="center" colspan="3">
  <img src="https://daac.in/images/daac-logo.png" width="150px">
  </th>
  </tr>

  <tr>
    <td align="left" width="45%"><b>Student Name:-</b>&nbsp;&nbsp;' . ucwords($name) . '</td>
    <td width="21%"></td>
    <td align="left"><b>Total Fees:-</b>&nbsp;&nbsp;' . $totalamount . '</td>
  </tr>

    <tr>
	<td align="left"><b>Mobile:-</b>&nbsp;&nbsp;' . $mobile . '</td>
	<td></td>';
if ($discomt == '0') {
    $html .= '<td align="left"><b>Discount:-</b>&nbsp;&nbsp;N\A</td>';
} else {
    $html .= '<td align="left"><b>Discount:-</b>&nbsp;&nbsp;' . $discomt . '</td>';
}
$fee_paid = 0;
foreach ($pay_his as $his) {
    $fee_paid += $his['amount'];
}
$balance = $netfee - $fee_paid;
$html .= '</tr>

   <tr>
   <td align="left"><b>Course Name:-</b>&nbsp;&nbsp;' . $coursename . '</td>
   <td></td>
   <td align="left"><b>Fees Paid:-</b>&nbsp;&nbsp;' . $fee_paid . '</td>';
$html .= ' </tr>

 <tr>
   <td align="left"><b>Enrollment No:-</b>&nbsp;&nbsp;' . $enroll . '</td>
   <td></td>
   <td align="left"><b>Balance:-</b>&nbsp;&nbsp;' . $balance . '</td>
 </tr>


  </table>


  <table>
    <tr>
	  <td ';if (!empty($pay_his)) { $html .= 'width="49%"';} else { $html .= 'width="98%"';
}
$html .= '>
	    <h3 style="text-align:Center; margin-top:0px;">Fees Installment Details</h3>
	    <table cellspacing="0" cellpadding="2" border="1" >
         <thead>
          <tr>
          <th style="text-align:center;" width="20%">
		    <strong>S.No.</strong>
		  </th>
          <th style="text-align:center;" width="45%">
		    <strong>Fee Date</strong>
	      </th>
          <th style="text-align:center;" width="35%">
		    <strong>Amount</strong>
		  </th>
        </tr>
      </thead>

      <tbody>';

if (!empty($installment)) {
    $cnt = 1;foreach ($installment as $key => $vlv) { //pr($vlv); die;
        $tax = $vlv['igst_amount'] + $vlv['cgst_amount'] + $vlv['sgst_amount'];
        //dd($recnumber);
        $html .= ' <tr>
		<td style="text-align:center; " width="20%">' .
            $cnt . '
    </td>';
        $html .= '
    <td style="text-align:center;"  width="45%">' . date('d-M-Y', strtotime($vlv['pro_date'])) . '
    </td>';
        $html .= '<td style="text-align:center; " width="35%">' .
            $vlv['amount'] . '
    </td>
    </tr>';
        $cnt++;}} else {
    $html .= '<tr><td colspan="6" align="center">No Meta Available</td></tr>';
}
$html .= '</tbody>
 </table>
    </td>';
if (!empty($pay_his)) {

    $html .= '<td width="2%"></td>
	  <td  width="49%">
	   <h3 style="text-align:center; margin-top:0px;">Payment History</h3>

	   <table cellspacing="0" cellpadding="2" border="1" >
 <thead>
<tr>

    <th style="text-align:center;" width="25%">
	<strong>Receipt No</strong>
	</th>
    <th style="text-align:center;" width="30%">
	<strong>Payment Date</strong>
	</th>
    <th style="text-align:center;" width="17%">
	<strong>Amount</strong>
	</th>
    <th style="text-align:center;" width="13%">
	<strong>Fine</strong>
	</th>
    <th style="text-align:center;" width="15%">
	<strong>Total</strong>
	</th>
</tr>
 </thead>
 <tbody>';
    $i = 1;

    foreach ($pay_his as $his) {
        $paid += $his['amount'];
        if (empty($his['fine'])) {
            $his['fine'] = 0;
        }
        $pay_date = date('d-m-Y', strtotime($his['pay_date']));
        $paid_amount = $his['amount'] + $his['fine'];
        //pr($his); die;
        $html .= '<tr>

<td style="text-align:center; ">' . $his['receipt_no'] . '</td>
<td style="text-align:center; ">' . $pay_date . '</td>
<td style="text-align:center; ">' . $his['amount'] . '</td>
<td style="text-align:center; ">' . $his['fine'] . '</td>
<td style="text-align:center; ">' . $paid_amount . '</td>
</tr>';
        $i++;

    }

    $html .= '</tbody>
 </table>


    </td>';
}
$html .= '</tr>
  </table>
 <br>
 <br>';

$html .= '<table cellspacing="0" cellpadding="2" border="0">
  <tr>
  <td align="left" style="width:40%">Student\'s signature: _________________</td>
   <td style="width:30%"></td>
  <td align="left">Administator: ___________________</td>
  </tr>
  </table>
</div> 
  </body>';
      
$pdf->writeHTMLCell(0, 0, '', '', utf8_encode($html), 0, 1, 0, true, '', true);
ob_end_clean();
echo $pdf->Output('orderlist.pdf');
exit;
?>
  