<!-- <script>
   function checkextension() {
    var file = document.querySelector("#fUpload");
    if ( /\.(jpe?g|png|gif)$/i.test(file.files[0].name) === false ) 
      { alert("not an image please choose a image!");
    $('#fUpload').val('');
  }
  return false;
}
</script> -->
<style type="text/css">
.box.box-info{ margin-bottom:30px;}
	.text{color:red; font-size: 12px;}
	.filename{font-size: 11px;color: #e02727;}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<i class="fa fa-plus-square" aria-hidden="true"></i>  
						  Basic Info 
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo SITE_URL; ?>admin/students"><i class="fa fa-home"></i>Home</a></li>
			<li><a href="<?php echo SITE_URL; ?>admin/students">Manage Course</a></li>
				<li class="active"><a href="javascript:void(0)">Edit Course</a></li>   
			
		</ol>
	</section>
	<script>
function isNumber(evt) {

evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;
if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {

return false;
}
return true;
}

</script>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
			<div class="box box-info boxInfoBx boxInfoBxStudentInfo">
										<!-- <div class="box-header with-border">
						<h3 class="box-title">Student Info</h3>
					</div> -->
			
					<div class="box-body addStdntsFrm">
                    <div class="row d-flex">
<div class="col-sm-2 align-self-center" style="padding-top: 15px;padding-left:50px;"><?php 
$path=LOCALWEB.$newresponse['image'];
//echo $path; die;
if(!empty($newresponse['image']) && file_exists($path)){ ?>
<img src="<?php echo SITE_URL;?>images/student/<?php echo $newresponse['image']?>" alt="img" style="width:100px;height:100px"> <?php } else{ ?><img src="https://daac.in/images/student/noimage.jpg" alt="img" style="width:100px;height:100px"><?php } ?></div>
<!--  -->

               <div class="col-sm-4 align-self-center" style="padding:15px 0px 30px 0px;">
               
               <table style="width:100%">
               <tbody><tr style="width:100%"><td style="text-align:left; width:30%; padding-right:2%"><b>Enroll. No</b></td><td><b>:</b>   <?php echo $newresponse['s_enroll']; ?></td></tr>
			   <tr style="width:100%"><td style="text-align:left; width:30%; padding-right:2%"><b>Name</b></td><td><b>:</b>   <?php echo $newresponse['s_name']; ?></td></tr>
                           <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Father's Name</b></td><td><b>:</b>   <?php echo $newresponse['f_name']; ?> </td></tr>
                <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Mobile No</b></td><td><b>:</b>   <?php echo $newresponse['s_contact']; ?></td></tr>
                <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Branch</b></td><td><b>:</b>   
				<?php $brnach=$this->Comman->brnach($newresponse['branch_id']);
				 echo $brnach['name']; ?></td></tr>
               </tbody></table>            
               </div>
			  <!-- col-sm-4 -->



<!--  -->

<!-- <div class="box-body addStdntsFrm">
						<div class="row"> -->
							



							<div class="col-sm-3">
			<label for="inputEmail3" class="control-label">Course <strong style="color:red;">*</strong></label>
			<?php echo $this->Form->input('course_id', array('class' =>'form-control','id'=>'course_id','label'=>false,'options'=>$categ1,'default'=>$crseid,'empty'=>'--Select Course--','required','autofocus')); ?>  
							</div>

							<div class="col-sm-3 session" style="display:none">
          <label class="inputEmail3">Select Session</label>
     
            <?php
            $session=[];
              $m=date('m');
              $year=date('Y');
              if($m<6){
                $session['june-'.($year-1)]='June-'.($year-1);
                $session['june-'.$year]='June-'.$year;
                $session['june-'.($year+1)]='June-'.($year+1);
                }else{
                $session['june-'.$year]='June-'.$year;
                $session['june-'.($year+1)]='June-'.($year+1);
                }
              if($m<12){
                $session['dec-'.($year-1)]='December-'.($year-1);
                $session['dec-'.$year]='December-'.$year;
                $session['dec-'.($year+1)]='December-'.($year+1);
                }else{
                $session['dec-'.$year]='December-'.$year;
                $session['dec-'.($year+1)]='December-'.($year+1);
                }
                

 echo $this->Form->input('session', array('class' => 'smallselect form-control','empty'=>'--Select Session--','options' =>$session,'id'=>'session')); 
 ?>
          
        </div>


<?php $br_det['is_reg'] == 'Y' ? $reg_feess = $br_det['registration_fee'] : $reg_feess = "";?>
<div class="col-sm-3" id="reg_fee" 
<?php if($this->request->session()->read('Auth.User.role_id') == 1 || $br_det['is_reg'] == 'N'){ ?> style="display:none" <?php } ?>>
         

		<label for="inputEmail3" class="control-label">Registration Fees</label>
			<?php echo $this->Form->input('installment[reg]', array('class' =>'form-control','id'=>'rfee','type'=>'text','label'=>false,'readonly')); ?>  
      </div>
   
        <div class="col-sm-3" id="sk">
		 <div id="pk">
         <label class="control-label">Installment Type <strong style="color:red;">*</strong></label>
		 <div class="input text">
    <span class="field" id="kl"> 
	<div class="form-check">
  <input class="form-check-input chk" type="radio" name="inst_id" value="" id="flexRadioDefault">
  <label class="form-check-label" for="flexRadioDefault">
 Choose Course First
  </label>
</div> </span>
		 </div>
         </div>
         
        </div>

		
						

							<?php if($newresponse['s_id']){ ?>
							<script>          
              $(document).ready(function () { 
                
              var instid= '<?php echo $newresponse['branch_id']; ?>';
             
                  $.ajax({
                    async:true,
                    data:{'branch':instid},
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>students/branch",
                    success:function (data) {
                    //  $('.lds-facebook').hide();   
                      $("#bk").html(data); },
                    });
                  return false;
              
              });
              </script>
<?php }else{ ?>
	<script>          
              $(document).ready(function () { 
                $("#brnch").on("click", function (event) {
              var instid= $(this).val();
             
                  $.ajax({
                    async:true,
                    data:{'branch':instid},
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>students/branch",
                    success:function (data) {
                    //  $('.lds-facebook').hide();   
                      $("#bk").html(data); },
                    });
                  return false;
                });
              });
              </script>

 <?php } ?>


		<div id="fstbl" class="col-sm-12" >


		<label class="control-label"></label>
<table  class="table table-bordered table-striped" cellspacing="0" cellpadding="2" border="0" style="width:100%;">
<thead class="thead-dark">
	<tr>
		<th style="text-align:center;   width: 88px;"><strong>
      S.No.
    </strong></th>
   
    <th style="text-align:center;  width:145px;"><strong>
      Amount
    </strong></th>
    <!--<th style="text-align:center;  width:145px;"><strong>
      Dis. Amount
    </strong></th>-->
    </tr>
    </thead>
    <tbody>
<?php  if(count($studentinstallment)>0){
    $total_installments=0; ?>
  
      <?php $cnt=1;  foreach($studentinstallment as $key=>$value) {  ?>
    <tr>
		<td style="text-align:center; width:30px;">
     <?php echo $cnt; ?>
    </td>
    
    <td style="text-align:center; width:145px;">
    <?php  $total_installments+=$value['amount']; ?>
    <input type="text"  maxlength="6" onkeypress="return isNumber()" class="form-control install1 longinput" name="installment[]" value="<?php echo $value['amount'];?>" style="width:50%; margin:auto">
    </td>
    
    </tr>
    <?php $cnt++; } 
  ?>  <tr>
  <td style="text-align:center; width:30px;">Total <br> Fee(<?php echo $total_installments; ?>)</td>
  <td><input type="text" onkeypress="return isNumber()" value="<?php echo $total_installments; ?>" class="form-control" id="total" disabled style="width:50%; margin:auto"></td>
  </tr>
  <input type="hidden" id="course_fees" value="<?php echo $course_fees; ?>">
   <?php 
  } else {  ?>    
                    <tr><td colspan="7" align="center">No Meta Available</td></tr>
                    <?php } ?>
</tbody>
</table>

<script>
$(document).ready(function(){


$(document).change(".install1", function() {
 
    var sum = 0;
    $(".install1").each(function(){
        sum += +$(this).val();
    });
    $("#total").val(sum);
});

function validfees(){
  alert();
}

});
</script>
             
			 <!-- </div>
			



						
						</div>  -->

<!--  -->


               </div>
               
                </div>
			
				<div class="box-footer" id="sub">
						<?php echo $this->Html->link('Back', ['action' => 'index'],['class'=>'btn btn-default']); ?>
						<?php if($newresponse['s_id']){ ?>
							<?php echo $this->Form->submit('Update',array('class' => 'btn btn-info pull-right', 'title' => 'Update')); ?>
							<?php }else{ ?>
							<?php echo $this->Form->submit('Save',array('class' => 'btn btn-info pull-right', 'title' => 'Save')); ?>
							<?php } ?>
						</div>							
                      		
						
					</div>



			<?php echo $this->Form->create($newresponse, array(
						'class'=>'form-horizontal needs-validation addStdntsFrm',
						'type' => 'file',
						'validate','novalidate'
						
					));
				
					 ?>
					 

					 <?php if($newresponse['s_id']){
			 ?>
	  <input type="hidden" name="s_id" value="<?php echo $newresponse['s_id']; ?>">
	  <input type="hidden" name="branch" value="<?php echo $newresponse['branch_id']; ?>">
	  <?php } ?>





						</div>




<?php echo $this->Form->end(); ?>
				</div>
			</div>
		</section>
	</div> 
<?php if($newresponse['s_id']){ ?>
	<script>
$(document).ready(function(){
  var course_fees;

  $('.lds-facebook').show();
		//alert(userbranch);
		var id='<?php echo $crseid; ?>';

		$("#course_id option[value='<?php echo $crseid; ?>']").prop('selected', true);
    var degreeCourses=<?php echo json_encode($degrecourseList) ?>;
    console.log(degreeCourses);
    if(Object.values(degreeCourses).includes(id)){
      $('.session').show();
    }else{
      $('.session').hide();
    }
		//alert(id);
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/checkcoursefeeflexibility',
		   type:'POST',
		   data:{'id12': id},
		   success:function(data){
			//alert(data);
			if(data.trim()!='0' && data.trim()!='1'){
       
		var userbranch='<?php echo $this->request->session()->read('Auth.User.branch'); ?>';   
		$('#rfee').val(data);
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/inst',
		   type:'POST',
		   data:{'id12': id, 'branch': userbranch},
		   success:function(data1){
			//alert(data1);
		
			if(data1.trim()=='5'){
				$('#sk').html('<div id="pk" style="color:red;"><label class=" text-right">Installment Type </label> <div class="input text"><span class="field" id="kl"> No Installment Type Added For This Course.</span></div></div>');
				
				$('#sub').hide();
			} else {
			$('#sk').html(data1);
			$('#sub').show();
		}
		   },

			});	   
		} 
		
		if(data.trim()=='1'){
			var userbranch='0';   
		
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/inst',
		   type:'POST',
		   data:{'id12': id, 'branch': userbranch},
		   success:function(data1){
			//alert(data1);
			$('#sk').html(data1);
			
		   },

			});
			
		} 
		
		if(data.trim()=='0'){
	    $('#sub').hide();	
		$('#sk').html('<div id="pk" style="color:red;"><label class="text-right">Installment Type </label><div class="input text"><span class="field" id="kl"> Course is not approved by Admin.</span></div></div>');
				
				$('#sub').hide();
		} 
		   },

      });
     		
	
              var instid= '<?php echo $newresponse['inst_id']; ?>';
			

$('#flexRadioDefault'+instid).prop('checked', true);
              var discount= $("#discount").val();
              var reg_fees= $("#reg_fees").val();
                  $.ajax({
                    async:true,
                    data:{'inst_id':instid,'discount':discount,'reg_fees':reg_fees},
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>students/selectchk",
                    success:function (data) {
                      $('.lds-facebook').hide();   
                     // $("#fstbl").html(data); 
					  
					  },
                    });
                  return false;
              
	
	});
</script> 





<script>
$(document).ready(function(){
  var course_fees;
	$('#course_id').change(function(){
		$('.lds-facebook').show();
		$("#fstbl").html('');
		//alert(userbranch);
		var id=$(this).val();
    var degreeCourses=<?php echo json_encode($degrecourseList) ?>;
    console.log(degreeCourses);
    if(Object.values(degreeCourses).includes(id)){
      $('.session').show();
    }else{
      $('.session').hide();
    }
		//alert(id);
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/checkcoursefeeflexibility',
		   type:'POST',
		   data:{'id12': id},
		   success:function(data){
			//alert(data);
			if(data.trim()!='0' && data.trim()!='1'){
       
		var userbranch='<?php echo $this->request->session()->read('Auth.User.branch'); ?>';   
		$('#rfee').val(data);
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/inst',
		   type:'POST',
		   data:{'id12': id, 'branch': userbranch},
		   success:function(data1){
			//alert(data1);
		
			if(data1.trim()=='5'){
				$('#sk').html('<div id="pk" style="color:red;"><label class=" text-right">Installment Type </label> <div class="input text"><span class="field" id="kl"> No Installment Type Added For This Course.</span></div></div>');
				
				$('#sub').hide();
			} else {
			$('#sk').html(data1);
			$('#sub').show();
		}
		   },

			});	   
		} 
		
		if(data.trim()=='1'){
			var userbranch='0';   
		
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/inst',
		   type:'POST',
		   data:{'id12': id, 'branch': userbranch},
		   success:function(data1){
			$('.lds-facebook').hide();
			$('#sk').html(data1);
			
		   },

			});
			
		} 
		
		if(data.trim()=='0'){
	

		$('#sub').hide();	
		$('#sk').html('<div id="pk" style="color:red;"><label class="text-right">Installment Type </label><div class="input text"><span class="field" id="kl"> Course is not approved by Admin.</span></div></div>');
				
				$('#sub').hide();
		} 
		   },

      });
     		
		});
	
	});
</script> 


	<?php }else{ ?>
	<script>
$(document).ready(function(){
  var course_fees;
	$('#course_id').change(function(){
		$('.lds-facebook').show();
		//alert(userbranch);
		var id=$(this).val();
    var degreeCourses=<?php echo json_encode($degrecourseList) ?>;
    console.log(degreeCourses);
    if(Object.values(degreeCourses).includes(id)){
      $('.session').show();
    }else{
      $('.session').hide();
    }
		//alert(id);
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/checkcoursefeeflexibility',
		   type:'POST',
		   data:{'id12': id},
		   success:function(data){
			//alert(data);
			if(data.trim()!='0' && data.trim()!='1'){
       
		var userbranch='<?php echo $this->request->session()->read('Auth.User.branch'); ?>';   
		$('#rfee').val(data);
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/inst',
		   type:'POST',
		   data:{'id12': id, 'branch': userbranch},
		   success:function(data1){
			//alert(data1);
		
			if(data1.trim()=='5'){
				$('#sk').html('<div id="pk" style="color:red;"><label class=" text-right">Installment Type </label> <div class="input text"><span class="field" id="kl"> No Installment Type Added For This Course.</span></div></div>');
				
				$('#sub').hide();
			} else {
			$('#sk').html(data1);
			$('#sub').show();
		}
		   },

			});	   
		} 
		
		if(data.trim()=='1'){
			var userbranch='0';   
		
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/inst',
		   type:'POST',
		   data:{'id12': id, 'branch': userbranch},
		   success:function(data1){
			$('.lds-facebook').hide();
			$('#sk').html(data1);
			
		   },

			});
			
		} 
		
		if(data.trim()=='0'){
	

		$('#sub').hide();	
		$('#sk').html('<div id="pk" style="color:red;"><label class="text-right">Installment Type </label><div class="input text"><span class="field" id="kl"> Course is not approved by Admin.</span></div></div>');
				
				$('#sub').hide();
		} 
		   },

      });
     		
		});
	
	});
</script> 

<?php } ?>
<script>
$(function() {
$( "#datepicker" ).datepicker();

});
$(function() {
$( "#datepickerbrth" ).datepicker({minDate:null,changeYear:true,changeMonth:true,yearRange:"c-40:c"});

});



</script>


<script>

function getrefdata(data){
var type=data.value;
if(type==11){
  $('#updaterefdetails').css('display','block');
  $('#rname').attr('required',true);
  $('#rmobile').attr('required',true);
   $('#rmobile').val('<?php echo $this->request->data["rmobile"] ?>');

  $('#rname').val('<?php echo $this->request->data["rname"] ?>');
}
else{

  $('#rname').removeAttr('required');
  $('#rmobile').removeAttr('required');
  $('#rmobile').val('');

  $('#rname').val('');
    $('#updaterefdetails').css('display','none');

}
}
</script>
<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
    
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>