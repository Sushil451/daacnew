<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>
<style>
*{ padding:0px; margin:0px;}
.id_card_format{ border:1px solid #CCC; background:#fff; width:322px; height:512px; display:flex; flex-direction:column; justify-content:space-between; font-family: 'Open Sans', sans-serif; margin:auto}
.id_card_format .top_bar_red{ background:#eb3d3f; height:13px;}
.id_card_format .top_bar_blue{ background:#1d99d2; height:13px;}
.content_part{ text-align:center; padding-top:15px; flex:1; background:url(../../images/doomshell-staff-id-card-format_03.png) no-repeat right bottom;}
.content_part .logo{ margin-bottom:20px;}
.content_part .logo img{ width:230px;}
.student_detail h1{ font-size:22px; color:#333}
.student_detail p{ font-size:16px; color:#333}

.img_box{ width:150px; height:174px; margin-top:20px; margin:auto;overflow:hidden}
.img_box img{ width:100%;}
.footer_part{
    background: #1d99d2;
    display: flex;
    justify-content: space-between;
    height: 55px;
    padding: 5px;
    color: #fff;}
</style>

<div class="id_card_format" id="capture">
<div class="top_bars">
<div class="top_bar_red"></div>
<div class="top_bar_blue"></div>
</div>

<div class="content_part">
<div class="logo">
<img src="<?php echo SITE_URL; ?>images/daac-logo-id.png" alt="logo">
</div>

<div class="img_box">

<?php
$acadmicyear = date('Y') . "-" . date('y', strtotime('+1 years'));

$img_url=SITE_URL."images/student/".$students['image'];
if(!empty($students['image'])){
    //print_r($students); die;
    ?>
<img src="<?php echo SITE_URL; ?>images/student/<?php echo $students['image']; ?>" alt="image">
<?php  } else{ ?>
<img src="<?php echo SITE_URL; ?>images/no-image.jpg" alt="image">
<?php } ?>
</div>

<div class="student_detail">
<h1><b><?php echo ucwords($students['s_name'])?><b></h1>
<p style="margin-bottom:0px !important">Course: <?php echo ucwords($cor_det['sname']); ?></p>
<p style="margin-bottom:0px !important">Enroll No.: <?php echo $students['s_enroll']?></p>
<p style="margin-bottom:0px !important">Phone No.: <?php echo $students['s_contact'] ?>
</p>
<p><b>Year</b>: <?php echo $acadmicyear; ?></p>
</div>


</div>

<div class="footer_part">
<div class="website" style="font-size:18px;">www.daac.in</div>
<div class="website" style="font-size:18px;">8764122221</div>
</div>

</div>

<div><a href="javascript:void(0);" id="download">Clich Here to Download</a></div>

<script>
 $(document).ready(function(){
var dyidenroll='<?php echo $students['s_enroll']; ?>';
var element = $("#capture"); // global variable
var getCanvas; 

$('#download').click(function(){

 html2canvas(element, {
         onrendered: function (canvas) {
              //  $("#previewImage").append(canvas);
                getCanvas = canvas;
             }
         });
    var imgageData = getCanvas.toDataURL("image/jpeg");
    // Now browser starts downloading it instead of just showing it
    var newData = imgageData.replace(/^data:image\/jpeg/, "data:application/octet-stream");
    $("#download").attr("download", dyidenroll+".JPEG").attr("href", newData);
   
    
	});

});
</script>

