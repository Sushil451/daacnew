<?php echo $this->Form->create('Student',array('type'=>'file','inputDefaults'=>array('div'=>false,'label'=>false),'class'=>'stdform dropupStudPop addStdntsFrm needs-validation')); ?> 
        <div class="feespopup" id='inline_contentinline_contentdrop<?php echo $cid; ?>' style=' background:#fff;'> <?php
  $ins=$this->Comman->instl($sid,$insid); ?>
  <div class="fplan">  

  <div class="row">
			<div class="col-md-12">
			

				<div class="box box-info boxInfoBx boxInfoBxStudentInfo">
  <div class="box-body addStdntsFrm">
                    <div class="row d-flex">
<div class="col-sm-4 align-self-center" >
<?php 
$path=LOCALWEB.$stu_det['image'];
//echo $path; die;
if(!empty($stu_det['image']) && file_exists($path)){ ?>
<img src="<?php echo SITE_URL;?>images/student/<?php echo $stu_det['image']?>" alt="img" style="width:100px;height:100px"> <?php } else{ ?><img src="https://daac.in/images/student/noimage.jpg" alt="img" style="width:100px;height:100px"><?php } ?></div>
<!--  -->

               <div class="col-sm-8 align-self-center">
               <h3><?php echo ucwords($stu_det['s_name'])?></h3>
               <table style="width:100%">
               <tbody><tr style="width:100%"><td style="text-align:left; width:30%; padding-right:2%"><b>Enroll. No</b></td><td style="text-align:left ;"><b>:</b> <?php echo $stu_det['s_enroll']?></td></tr>
               <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Father's Name</b></td><td style="text-align:left ;"><b>:</b>  <?php echo ucwords($stu_det['f_name'])?></td></tr>
                <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Mobile No</b></td><td style="text-align:left ;"><b>:</b>    <?php echo $stu_det['s_contact']?></td></tr>
                <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Branch</b></td><td style="text-align:left ;"><b>:</b>    <?php echo $stu_det['branch']['name']?></td></tr>
               </tbody></table>            
               </div>
              
               </div>
               
                </div>
              </div>
              
              </div>
              
               </div>
<table class="table table-bordered table-striped afTblStndRept" width="100%">
               
              
    <tbody>
   <tr>
   <div class="row">
			<div class="col-md-12">
			

				<div class="box box-info boxInfoBx boxInfoBxStudentInfo boxInfoBxStudentInfoMB0">
  <div class="box-body addStdntsFrm">
                    <div class="row d-flex">
<div class="col-sm-6 align-self-center" >
<strong>Course :</strong>
  <?php echo $this->Form->input('course_id', array('class' => 'form-control','label'=>false,'empty'=>'Select Course','options' =>$categ1,'default'=>$cid,'value'=>$cid,'required','readonly')); 
 ?>
      <input type="hidden" name="s_id" value="<?php echo $sid;?>">
  </div>
<!--  -->

               <div class="col-sm-6 align-self-center">
               <strong>Amount Paid :</strong>
               <?php echo $this->Form->input('amount_paid', array('class' => 'form-control', 'readonly','value'=>$paid_amt,'label'=>false, 'readonly' )); ?>
               </div>
               
               </div>
              
              
               <div class="row d-flex">
<div class="col-sm-6 align-self-center" >
<strong>Refund Amount :</strong>
<?php echo $this->Form->input('amount', array('label'=>false,'class' => 'form-control','onkeypress' => 'return isNumber(event);')); ?>
<input type="hidden" name="s_id" value="<?php echo $sid;?>">
  </div>
<!--  -->

               <div class="col-sm-6 align-self-center">
               <strong>Description :</strong>
               <?php echo $this->Form->input('description', array('label'=>false,'class' => 'form-control')); ?>
               </div>
               
               </div>
              
              </div>
             
             </div>
             
              </div>

    </tr>
    
    
  	
    
</tbody>
</table>
  
</div>
</div>
<div class="modal-footer box footer">
        <!-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> -->
        <?php echo $this->Form->submit('Submit',array('class' => 'btn btn-success')); ?>
      </div>
      <?php echo $this->Form->end();?> 
<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
    
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
<script>
function isNumber(evt) {

evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;
if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {

return false;
}
return true;
}

</script>