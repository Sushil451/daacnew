<table class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th width="4%">S.No</th>                          
                  <th width="28%">Title</th>   
                  <th width="10%">Role</th>  
                  <th width="18%">Subject</th>   
                  <th width="18%">From Email</th>   
                  <th width="8%">Created</th>   
                  <th width="6%">Format</th>               
                  <th width="12%">Action</th>     
                  </tr>
                </thead>
                <tbody>
                <?php 
                $page = $this->request->params['paging'][$this->request->params['controller']]['page'];
                $limit = $this->request->params['paging'][$this->request->params['controller']]['perPage'];
                $counter = ($page * $limit) - $limit + 1;
                if(isset($emailtemplate) && !empty($emailtemplate)){ 
                foreach($emailtemplate as $value){ //pr($value); die;
                ?>
                <tr>
                  <td><?php echo  $counter; ?></td>
                  <td><?php echo $value['title']; ?></td>
                  <td><?php echo $value['role']['name']; ?></td>
                  <td><?php echo $value['subject']; ?></td>
                  <td><?php echo $value['fromemail']; ?></td>
                  <td><?php echo date('d-M-Y',strtotime($value['created'])); ?></td>
                  <td>
                   <a href="<?php  echo ADMIN_URL ?>emailtemplate/viewtemplate/<?php echo $value['id']; ?>" data-toggle="modal" class="documentcls badge badge-primary" title="View Email Template">view</a>
                  </td>

        <td>
      <?php if($value['status']=='Y'){ 
            echo $this->Html->link('', [
          'action' => 'status',
          $value->id,'N'
          ],['title'=>'Active','class'=>'fa fa-check-circle','style'=>'font-size: 21px !important; margin-left: 12px;     color: #36cb3c;']);
      
          }else{ 
        echo $this->Html->link('', [
          'action' => 'status',$value->id,'Y'
      ],['title'=>'Inactive','class'=>'fa fa-times-circle-o','style'=>'font-size: 21px !important; margin-left: 12px; color:#FF5722;']);
        
       } ?>

       <?php  echo $this->Html->link(__(''), ['action' => 'edit', $value->id,],array('class'=>'fa fa-pencil-square-o fa-lg','title'=>'Edit','style'=>'font-size: 20px !important; margin-left: 12px;')) ?>
      </td>
                  
                </tr>
    <?php $counter++;} } ?>  
                </tbody>
              </table>
              <?php echo $this->element('admin/pagination'); ?> 