<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">  
 <div class="content-wrapper">
   <section class="content-header">
    <h1>
         Email Template Manager
    </h1>
    <ol class="breadcrumb">
    <li><a href="<?php echo SITE_URL; ?>admin/dashboards"><i class="fa fa-home"></i>Home</a></li>
<li><a href="<?php echo SITE_URL; ?>admin/emailtemplate">Email Template</a></li>
    </ol> 
    </section> <!-- content header -->

    <!-- Main content -->
    <section class="content">
  <div class="row">
    <div class="col-xs-12"> 
    <div class="box">
          <div class="box-header">
            <?php echo $this->Flash->render(); ?>
            <?php $role_id=$this->request->session()->read('Auth.User.role_id'); ?>
            <script>          
              $(document).ready(function () { 
                $("#Mysubscriptions").bind("submit", function (event) {
                  $('.lds-facebook').show();
                  $.ajax({
                    async:true,
                    data:$("#Mysubscriptions").serialize(),
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>emailtemplate/search",
                    success:function (data) {
                      $('.lds-facebook').hide();   
                      $("#example2").html(data); },
                    });
                  return false;
                });
              });

            </script>

            <?php  echo $this->Form->create('Mysubscription',array('type'=>'file','inputDefaults'=>array('div'=>false,'label'=>false),'id'=>'Mysubscriptions','class'=>'form-horizontal')); ?>

            
            <div class="form-group" >
              <div class="col-sm-2">
                <label for="inputEmail3" class="control-label">Select Role</label>
              <?php $option = array('2' => 'Admin','3' => 'Dropshipper','4' => 'Customer'); ?>
          <?php echo $this->Form->input('role_id', array('class' => 'form-control','label'=>false,'options'=>$option,'empty'=>'Select Role')); ?> 
          </div> 
                  <div class="col-sm-1">
                  <label for="inputEmail3" class="control-label" style="color:white">Search</label>
                  <input type="submit" style="background-color:#00c0ef;" id="Mysubscriptions" class="btn btn4 btn_pdf myscl-btn date" value="Search">       
                </div> 
                <?php echo $this->Form->end(); ?> 
              </div>  
            </div> 
          </div>  
            
  <div class="box">
    <div class="box-header">
          <?php echo $this->Flash->render(); ?>
          <a href="<?php echo SITE_URL; ?>admin/emailtemplate/add">
            <button class="btn btn-success pull-right m-top10"><i class="fa fa-plus" aria-hidden="true"></i>
            Add Email </button></a>
  <?php echo $this->Flash->render(); ?>
     </div><!-- /.box-header -->
      <div class="box-body" id="example2">    
              <table class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th width="4%">S.No</th>                          
                  <th width="28%">Title</th>   
                  <th width="10%">Role</th>  
                  <th width="18%">Subject</th>   
                  <th width="18%">From Email</th>   
                  <th width="8%">Created</th>   
                  <th width="6%">Format</th>               
                  <th width="12%">Action</th>     
                  </tr>
                </thead>
                <tbody>
                <?php 
                $page = $this->request->params['paging'][$this->request->params['controller']]['page'];
                $limit = $this->request->params['paging'][$this->request->params['controller']]['perPage'];
                $counter = ($page * $limit) - $limit + 1;
                if(isset($emailtemplate) && !empty($emailtemplate)){ 
                foreach($emailtemplate as $value){ //pr($value); die;
                ?>
                <tr>
                  <td><?php echo  $counter; ?></td>
                  <td><?php echo $value['title']; ?></td>
                  <td><?php echo $value['role']['name']; ?></td>
                  <td><?php echo $value['subject']; ?></td>
                  <td><?php echo $value['fromemail']; ?></td>
                  <td><?php echo date('d-M-Y',strtotime($value['created'])); ?></td>
                  <td>
                   <a href="<?php  echo ADMIN_URL ?>emailtemplate/viewtemplate/<?php echo $value['id']; ?>" data-toggle="modal" class="documentcls" title="View Email Template"><i class="fa fa-eye" aria-hidden="true" style="font-size: 16px; margin-left: 12px;"></i></a>
                  </td>

        <td>
      <?php if($value['status']=='Y'){ 
            echo $this->Html->link('', [
          'action' => 'status',
          $value->id,'N'
          ],['title'=>'Active','class'=>'fa fa-check-circle','style'=>'font-size: 21px !important; margin-left: 12px;     color: #36cb3c;']);
      
          }else{ 
        echo $this->Html->link('', [
          'action' => 'status',$value->id,'Y'
      ],['title'=>'Inactive','class'=>'fa fa-times-circle-o','style'=>'font-size: 21px !important; margin-left: 12px; color:#FF5722;']);
        
       } ?>

       <?php  echo $this->Html->link(__(''), ['action' => 'edit', $value->id,],array('class'=>'fa fa-pencil-square-o fa-lg','title'=>'Edit','style'=>'font-size: 20px !important; margin-left: 12px;')) ?>
      </td>
                  
                </tr>
    <?php $counter++;} } ?>  
                </tbody>
              </table>
              <?php echo $this->element('admin/pagination'); ?> 
            </div>
       
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
       </div>
      <!-- /.col -->  
    </div>
     <!-- /.row -->      
  </section>
    <!-- /.content -->  
       
 </div>     
     <!-- /.   content-wrapper -->  
   
  <div class="modal fade" id="mymodel">
<div class="modal-dialog" style="max-width: 500px !important;">
  <div class="modal-content">
    <!-- Modal Header -->
    <div class="modal-header">
      <h4 class="modal-title">Email Template Format</h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <!-- Modal body -->
    <div class="modal-body">
    </div>
  </div>
</div>
</div>

<script>
$('.documentcls').click(function(e){ 
  e.preventDefault();
  $('#mymodel').modal('show').find('.modal-body').load($(this).attr('href'));
});
</script>
