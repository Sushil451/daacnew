<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
<script>
  $(document).ready(function() {
    $('#summernote').summernote();
  });</script>

  <style type="text/css">
  .form-group{
    width: 50%;
  }
</style>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Email Template Manager
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo SITE_URL; ?>admin/dashboards"><i class="fa fa-home"></i>Home</a></li>
      <li><a href="<?php echo SITE_URL; ?>admin/emailtemplate">Manage Email Template</a></li>
      <li class="active"><a href="<?php echo SITE_URL; ?>admin/emailtemplate/edit/<?php echo $newpack['id']; ?>">Edit Email Template</a></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <?php echo $this->Flash->render(); ?>
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-plus-square" aria-hidden="true"></i> Manage Email Template</h3>
          </div>
          <?php echo $this->Form->create($newpack,array(
           'class'=>'form-horizontal',
           'controller'=>'emailtemplate',
           'action'=>'edit',
           'enctype' => 'multipart/form-data',
           'validate' )); 
           ?>
           <div class="box-body">
            <div class="container-fluid">
              <div class="form-group">
                <div class="col-sm-12">
                  <label for="exampleInputEmail1">Title</label>
                  <?php echo $this->Form->input('title', array('class' => 
                  'form-control','id'=>'exampleInputEmail1','placeholder'=>'Title','label'=>false,'autocomplete'=>'off','required')); ?>
                </div>
              </div>

              <div class="form-group">
               <div class="col-sm-12">
                <label for="exampleInputEmail1">From Email</label>
                <?php echo $this->Form->input('fromemail', array('class' =>'form-control','id'=>'exampleInputEmail1','type'=>'email','placeholder'=>'From Email','label'=>false,'autocomplete'=>'off','required')); ?>  
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
                <label for="exampleInputEmail1">Subject</label>
                <?php echo $this->Form->input('subject', array('class' => 
                'form-control','id'=>'exampleInputEmail1','placeholder'=>'Subject','label'=>false,'autocomplete'=>'off','required')); ?>
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-12">
                <label for="exampleInputEmail1">Admin Email</label>
               <?php echo $this->Form->input('adminemail', array('class' =>'form-control','id'=>'exampleInputEmail1','type'=>'email','placeholder'=>'Admin Email','label'=>false,'autocomplete'=>'off')); ?>
             </div>
           </div>

           <div class="form-group">
              <div class="col-sm-12">
                <label for="exampleInputEmail1">Select Role</label>
                <?php $option = array('2' => 'Admin','3' => 'Dropshipper','4' => 'Customer'); ?>
          <?php echo $this->Form->input('role_id', array('class' => 'form-control','label'=>false,'options'=>$option,'empty'=>'Select Role')); ?> 
             </div>
           </div>

           <div class="form-group" style="width: 65%; margin-left: 1px;"> 
            <label for="inputEmail3" class="control-label">Format Description</label>
            <?php echo $this->Form->input('format', array('class' => 
            'form-control','placeholder'=>'Format Description','required','label'=>false,'type'=>textarea,'autocomplete'=>'off','id'=>'summernote')); ?>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <?php echo $this->Form->submit(
          'Update',array('class' => 'btn btn-info pull-right', 'title' => 'Update')); ?>
          <?php echo $this->Html->link('Back', ['action' => 'index'],['class'=>'btn btn-default']); ?>
        </div>
        <?php echo $this->Form->end(); ?>
      </div>
    </div>
  </div>
</section>
</div> 

