<div class="content-wrapper">
 <section class="content-header">
  <h1>
  Slider Manager
 </h1>
 <ol class="breadcrumb">
  <li><a href="<?php echo SITE_URL; ?>admin/dashboards"><i class="fa fa-home"></i>Home</a></li>
  <li><a href="<?php echo SITE_URL; ?>admin/slider">Slider</a></li>
</ol> 
</section> <!-- content header -->

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">    
      <div class="box">
        <div class="box-header">
          <?php echo $this->Flash->render(); ?>
          <a href="<?php echo SITE_URL; ?>admin/slider/add">
            <button class="btn btn-success pull-right m-top10"><i class="fa fa-plus" aria-hidden="true"></i>
            Add Slider </button></a>
          </div><!-- /.box-header -->
          <div class="box-body">    
            <table class="table table-bordered table-striped" width="100%">
              <thead>
                <tr>
                  <th width="5%">S.No</th>  
                  <th width="10%">Mobile</th>                          
                  <th width="15%">Title</th>                     
                  <th width="15%">Sub Title</th>   
                  <th width="10%">Slider</th> 
                  <th width="25%">Options</th>
                  <th width="8%">Created</th>               
                  <th width="10%">Action</th>     
                </tr>
              </thead>
              <tbody>
                <?php 
                $page = $this->request->params['paging'][$this->request->params['controller']]['page'];
                $limit = $this->request->params['paging'][$this->request->params['controller']]['perPage'];
                $counter = ($page * $limit) - $limit + 1;
                if(isset($sliders) && !empty($sliders)){ 
                foreach($sliders as $slider){//pr($slider);die;?>
                  <tr>
                    <td><?php echo $counter;?></td>
                    <td>
                      <img src="<?php echo SITE_URL; ?>images/sliders/<?php echo  $slider['mobile_slider']; ?>" width="80px" height="50px">
                    </td>
                    <td><?php echo $slider['title']; ?></td>
                    <td><?php echo $slider['sub_title']; ?></td>
                    <td>
                      <img src="<?php echo SITE_URL; ?>images/sliders/<?php echo  $slider['image']; ?>" width="130px" height="50px">
                    </td>
                   
                    <td>
                    Title text color: <strong><?php echo $slider['title_color']; ?></strong><br>
                    Sub-title text color: <strong><?php echo $slider['sub_title_color']; ?></strong><br>
                    Order: <strong><?php echo $slider['sort']; ?></strong><br>
                    Link: <strong><?php echo $slider['link']; ?></strong>
                    </td>
                    <td><?php echo date('d-M-Y',strtotime($slider['created'])); ?></td>

                    <td>
                      <?php if($slider['status']=='1'){ 
                        echo $this->Html->link('', [
                          'action' => 'status',
                          $slider->id,'0'
                        ],['title'=>'Active','class'=>'fa fa-check-circle','style'=>'font-size: 21px !important; margin-left: 12px;     color: #36cb3c;']);
                        
                      }else{ 
                        echo $this->Html->link('', [
                          'action' => 'status',$slider->id,'1'
                        ],['title'=>'Inactive','class'=>'fa fa-times-circle-o','style'=>'font-size: 21px !important; margin-left: 12px; color:#FF5722;']);
                        
                      } ?>
  
                     <?php  echo $this->Html->link(__(''), ['action' => 'edit', $slider->id,],array('class'=>'fa fa-pencil-square-o fa-lg','title'=>'Edit','style'=>'font-size: 20px !important; margin-left: 12px;')) ?>

                     <?php echo $this->Html->link('', ['action' => 'delete',$slider->id],['title'=>'Delete','class'=> 'fa fa-trash','style'=>'color:#FF0000; margin-left: 13px; font-size: 19px !important;',"onClick"=>"javascript: return confirm('Are you sure do you want to delete this Slider')"]); ?>
                   </td>
                 </tr>
                 <?php $counter++;} } ?>  
               </tbody>
             </table>
             <?php echo $this->element('admin/pagination'); ?> 
           </div>
           
           <!-- /.box-body -->
         </div>
         <!-- /.box -->
       </div>
       <!-- /.col -->  
     </div>
     <!-- /.row -->      
   </section>
   <!-- /.content -->  
   
 </div>     
 <!-- /.   content-wrapper -->  
 

