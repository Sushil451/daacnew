<script>
 function checkextension() {
  var file = document.querySelector("#fUpload");
  if ( /\.(jpe?g|png|gif)$/i.test(file.files[0].name) === false ) 
    { alert("not an image please choose a image!");
  $('#fUpload').val('');
}
return false;
}
</script>
<style type="text/css">
 .text{
  color:red; 
  font-size: 12px;
}
.colorr{
  width: 0.333333%; margin-left: -32px;
}
</style>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
    Slider Manager
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo SITE_URL; ?>admin/dashboards"><i class="fa fa-home"></i>Home</a></li>
      <li><a href="<?php echo SITE_URL; ?>admin/slider">Manage Slider</a></li>
      <?php if(isset($companies['id'])){ ?>
        <li class="active"><a href="javascript:void(0)">Edit Slider</a></li>   
      <?php } else { ?>
        <li class="active"><a href="javascript:void(0)">Add Slider</a></li>
      <?php } ?>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <?php echo $this->Flash->render(); ?>
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-plus-square" aria-hidden="true"></i> <?php if(isset($slider['id'])){ echo 'Edit Slider'; }else{ echo 'Create Slider';} ?></h3>
          </div>

          <?php echo $this->Form->create($slider, array(
           'class'=>'form-horizontal',
           'enctype' => 'multipart/form-data',
           'validate'
         )); ?>
         <div class="box-body">
          <div class="form-group">
            <div class="col-sm-6">
              <label for="inputEmail3" class="control-label">Title</label>
              <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="This line will be highlighted over the slider. Leave it blank if you don't want to show the title."></i>
              <?php echo $this->Form->input('title', array('class' => 'form-control','label'=>false,'placeholder'=>'Title','autofocus','autocomplete'=>'off')); ?>   
            </div>

            <div class="col-sm-3">
              <label for="inputEmail3" class="control-label">Title Color</label>
              <input name="title_color" class="form-control" data-jscolor="{position:'right'}" value="2CAFFE"> 
            </div>
            </div>

            <div class="form-group">
            <div class="col-sm-6">
              <label for="inputEmail3" class="control-label">Sub Title</label>
              <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="The second line of the title. Leave it blank if you don't want to show this."></i>
              <?php echo $this->Form->input('sub_title', array('class' => 'form-control','label'=>false,'placeholder'=>'Sub Title','autofocus','autocomplete'=>'off')); ?>   
            </div>

            <div class="col-sm-3">
              <label for="inputEmail3" class="control-label">Sub Title Color</label>
              <input name="sub_title_color" class="form-control" data-jscolor="{position:'right'}" value="b5b5b5">    
            </div>
            </div>

            <div class="form-group">
            <div class="col-sm-6">
              <label for="inputEmail3" class="control-label">Link</label>
              <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Users will redirect to this link." aria-describedby="tooltip401900"></i>
              <?php echo $this->Form->input('link', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Link','autofocus','autocomplete'=>'off')); ?>   
            </div>

            <div class="col-sm-3">
              <label for="inputEmail3" class="control-label">Order</label>
              <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="The slider will be  arranged by this order."></i>
              <?php echo $this->Form->input('sort', array('class' => 'form-control','label'=>false,'placeholder'=>'Order','autofocus','autocomplete'=>'off')); ?>   
            </div>
            </div>

            <div class="form-group">
            <div class="col-sm-6">
              <label for="inputEmail3" class="control-label">Slider Image</label>
              <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="The main image what will display as slider. Its required to generate the slider." aria-describedby="tooltip18807"></i>
              <?php echo $this->Form->input('image', array('class' =>'form-control','autocomplete' => 'off','label'=>false,'type'=>'file','id'=>'fUpload','onchange'=>'checkextension()','required')); ?>
            </div>

            <div class="col-sm-3">
              <label for="inputEmail3" class="control-label">Mobile Slider Image</label>
              <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="The slider image for mobile app. The system will hide this slider on mobile if not provided. Keep the ratio 2:1 in size, which means the width of the image should be double of its height." aria-describedby="tooltip129255"></i>
              <?php echo $this->Form->input('mobile_slider', array('class' =>'form-control','autocomplete' => 'off','label'=>false,'type'=>'file','id'=>'fUpload','onchange'=>'checkextension()')); ?>
            </div>
          </div> 
        </div>
        <div class="box-footer">
          <?php
          if(isset($category['id'])){
            echo $this->Form->submit(
              'Update', 
              array('class' => 'btn btn-info pull-right', 'title' => 'Update')
            ); }else{ 
              echo $this->Form->submit(
                'Add', 
                array('class' => 'btn btn-info pull-right', 'title' => 'Add')
              );
            }
            ?><?php
            echo $this->Html->link('Back', [
              'action' => 'index'
              
            ],['class'=>'btn btn-default']); ?>
          </div>
          <?php echo $this->Form->end(); ?>
        </div>
      </div>
    </div>
  </section>
</div> 

