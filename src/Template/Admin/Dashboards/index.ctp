

<div class="content-wrapper">
 <section class="content">

  <?php 

    if($this->request->session()->read('Auth.User.role_id')==1){
    
    $lastcash=$lastcash_collection;
    $currentcash=$cash_collection;
    $tarincome=$targetmonth;
    //legendText
    $lastincometotal=$lastincometotal;
    $currentincometotal=$currentincometotal;
    $targetincomeltotal=$targetincometotal;
    $targetexntotal=$targeexpforadmintotal;
     //lstyear exen
     $lastyearexpen=$lastexpen_collection;
$currentyearexp=$currentexpen_collection;

$targetexpengrap=$targetexpenmonthbrach;

     //legenttext

     $lastyearexptotal=$lastexpentotaladmin;
      $currentyearexptotal=$currentexpentotaladmingraph;
     
    }
    else{
    $lastcash=$lastcash_collectionbranch;
     $currentcash=$cash_collectionbranch;
     $tarincome=$targetmonthbrach;
      //legendText
    $lastincometotal=$lastincometotalbrach;
    $currentincometotal=$cash_collectionbra;
     $targetincomeltotal=$targetincometotalbranch;
$targetexntotal=$targeexpforbranchtotal;
     //lstyear exen
     $lastyearexpen=$lastexpen_collectionbranch;
$currentyearexp=$currentexpen_collectionbranchdd;
     //le

         $lastyearexptotal=$lastexpentotalbranch;
         $currentyearexptotal=$currentexpentotalbranch;
$targetexpengrap=$targetexpenmonthbrachwisedata;
    }
 
    
?>


<script type="text/javascript" src="https://www.daac.in/js/admin/canvasjs.min.js"></script>
<script type="text/javascript" src="https://www.daac.in/js/admin/canvasjs_graph.js"></script>
<style>
.back span{
	color:white !important;
	
}
.back .group,.back a{
	color:white !important;
}
</style>


<script type="text/javascript">
window.onload = function () {

//monthlysales
	var chart = new CanvasJS.Chart("monthincome",
	{
	animationEnabled: true,
	height:300,
		zoomEnabled:false,
		colorSet: "blue",
		title:{
			text: "",
			fontColor: "#000",
                        fontSize: 14,
                        padding: 10,
                        fontWeight: "600",
			horizontalAlign: "left",
			fontFamily: "'Open Sans', sans-serif"
		},
		axisY: {
                        title:"",
			labelFontSize: 12,
			labelFontColor: "#000",
			valueFormatString:  "#,##,##0.##",
			labelFontFamily:"'Open Sans', sans-serif",
			maximum: 550000,
			gridThickness: 1,
			lineThickness: 1,
			tickThickness: 1,
			interval: 40000
		},
		axisX: {
                        title:"",
			
			labelFontSize: 12,
			labelFontFamily:"'Open Sans', sans-serif",
			lineThickness: 1,
			labelFontColor: "#000",
			
			interval:1,
		},
		data: [
		{
			type: "column",
			showInLegend: true, 
			legendText: "<?php echo number_format($lastincometotal); ?>",
			name: "Last Year",
			color: "#f9d520",
			dataPoints: <?php echo $lastcash; ?>
			
		},
		{
			type: "column",
			showInLegend: true, 
			legendText: "<?php echo number_format($currentincometotal); ?>",
			color: "#42f459",
			name: "Current Year",
			dataPoints: <?php echo $currentcash; ?>
			
		},
		{
			type: "column",
			showInLegend: true, 
			
			legendText:"<?php echo number_format($targetincomeltotal); ?>",
			color: "#f92320",
			name: "Target",
			dataPoints: <?php echo $tarincome; ?>
			
		}
		
		]
	});

	chart.render();


	
//
//monthlyearnings
var chart1 = new CanvasJS.Chart("exp",
	{
		animationEnabled: true,
		height:300,
		width:850,
		
		zoomEnabled:!1,
		zoomType:"x",
		colorSet: "blue",
		title:{
			text: "",
			fontColor: "#000",
			fontSize: 14,
			padding: 10,
			fontWeight: "600",
			horizontalAlign: "left",
			fontFamily: "'Open Sans', sans-serif"
		},
		axisY: {title:"",
			labelFontSize: 12,
			labelFontColor: "#000",
			valueFormatString:  "#,##,##0.##",
			labelFontFamily:"'Open Sans', sans-serif",
			maximum: 550000,
			gridThickness: 1,
			lineThickness: 1,
			tickThickness: 1,
			interval: 40000
		},
		axisX: {title:"",
			//valueFormatString: "MMM-YYYY",
			labelFontSize: 12,
			labelFontFamily:"'Open Sans', sans-serif",
			lineThickness: 1,
			labelFontColor: "#000",
			interval: 1
		},
		data: [
		{
			type: "column",
			showInLegend: true, 
			legendText: "<?php echo number_format($lastyearexptotal); ?>",
			name: "Last Year",
			color: "#f9d520",
			dataPoints: <?php echo $lastyearexpen; ?>
			
		},
		{
			type: "column",
			showInLegend: true, 
			legendText: "<?php echo number_format($currentyearexptotal); ?>",
			color: "#42f459",
			name: "Current Year",
			dataPoints: <?php echo $currentyearexp; ?>
			
		},
		{
			type: "column",
			showInLegend: true, 
			
			legendText:"<?php echo number_format($targetexntotal); ?>",
			color: "#f92320",
			name: "Target",
			dataPoints: <?php echo $targetexpengrap; ?>
			
		}
		
		]
	});

	chart1.render();

}
</script>
<div class="maincontent">

<!-- START OF FLIP BOXES -->
<section class="flipboxes">
  <ul class="cellgrid row">
    <li>
      <div class="flipbox green">
        <div class="<?php if($this->request->session()->read('Auth.User.role_id')==1 || $this->request->session()->read('Auth.User.role_id')==2 || $this->request->session()->read('Auth.User.role_id')==3 ) { echo 'flipper'; } ?>">
          <div class="front">
            <div class="iconbox">
              <figure class="icon"><i class="fa fa-users fa-3x"></i></figure>
              <figure class="icon">Registration</figure>
              
                   <?php if($this->request->session()->read('Auth.User.role_id')==1){ ?>
              <div class="col-sm-12">
                <table width="100%" >
				 <tr>
					
					<td colspan=3>&nbsp;</td>
				
                </tr>
                <tr>
					
					<td>&nbsp;</td>
					<td align="center">Target</td>
					<td align="center">Actual</td>
				
                </tr>
                <tr>
					<td>M</td>
					<td align="center"><?php echo $reg_month_target_b1; ?></td>
					<td align="center"><?php echo $reg_this_month_b1; ?></td>
					
                </tr>
				     <tr>
					<td>V</td>
					<td align="center"><?php echo $reg_month_target_b2; ?></td>
					<td align="center"><?php echo $reg_this_month_b2; ?></td>
					
                </tr>
				 <tr>
					<td>C</td>
					<td align="center"><?php echo $reg_month_target_b3; ?></td>
					<td align="center"><?php echo $reg_this_month_b3; ?></td>
				
                </tr>
                <tr>
					<td>P</td>
					<td align="center"><?php echo $reg_month_target_b4; ?></td>
					<td align="center"><?php echo $reg_this_month_b4; ?></td>
				
                </tr>
                </table>
			
              <?php }else{ ?>
              	<div class="text-center bld_ttl">
             Target <br /><strong class="green"><?php echo $reg_year_target; ?></strong>
                               </div>
              <?php } ?>
            </div>
          </div>
          <div class="back">
            <div class="cell">
              <div class="group">
				  <?php if($this->request->session()->read('Auth.User.role_id')==2){ ?>
				
            
                 <div class="col-sm-12">
                <table width="100%">
                <tr>
					<td align="left">Target-Y</td>
					<td><?php echo $reg_year_target;?></td>
					 <tr>
					<tr>
					<td align="left">Actual-Y</td>
					<td><?php echo $reg_this_year; ?></td>
					 <tr>
					<tr >
					<td align="left">Diff:</td>
					<td><?php echo ($reg_year_target-$reg_this_year);?></td>
                </tr>
                </table>
                <?php } else { ?>
                 <div class="col-sm-12">
                <table width="100%">
                <tr><td></td>
					<td>Y(T)</td>
					<td>Y(A)</td>
					<td>Pending</td>
                </tr>
                <tr>
					<td>M</td>
					<td><?php echo $reg_year_target_b1; ?></td>
					<td><?php echo $reg_this_year_b1; ?></td>
					<td><?php echo ($reg_year_target_b1-$reg_this_year_b1); ?></td>
                </tr>
                 <tr>
					<td>V</td>
					<td><?php echo $reg_year_target_b2; ?></td>
					<td><?php echo $reg_this_year_b2; ?></td>
					<td><?php echo ($reg_year_target_b2-$reg_this_year_b2); ?></td>
                </tr>
                 <tr>
					<td>C</td>
					<td><?php echo $reg_year_target_b3; ?></td>
					<td><?php echo $reg_this_year_b3; ?></td>
					<td><?php echo ($reg_year_target_b3-$reg_this_year_b3); ?></td>
                </tr>
                <tr>
					<td>p</td>
					<td><?php echo $reg_year_target_b4; ?></td>
					<td><?php echo $reg_this_year_b4; ?></td>
					<td><?php echo ($reg_year_target_b4-$reg_this_year_b4); ?></td>
                </tr>
                </table>
					 </div>
                <?php } ?>
              </div>

          </div>
        </div>
      </div>
    </li>
    <li><!--collection box-->
      <div class="flipbox orange">
        <div class="<?php if($this->request->session()->read('Auth.User.role_id')==1 || $this->request->session()->read('Auth.User.role_id')==2 || $this->request->session()->read('Auth.User.role_id')==3 ) { echo 'flipper'; } ?>">
          <div class="front">
            <div class="iconbox">
              <figure class="icon"><i class="fa fa-tag fa-3x"></i></figure>
             
              <figure style="text-align:center ">Income</figure>
               
               
			      
					  <?php if($this->Session->read('Auth.User.branch')==0){ ?>
					 
					  <?php } else { ?>
					  	<div class="text-center bld_ttl">
             Target <br /><strong class="orange"><?php echo $target_income_month_b1; ?></strong>
                               </div>
					  <?php } ?>
                <?php if($this->request->session()->read('Auth.User.role_id')==1){ ?>
				 <div class="col-sm-12">
                <table width="100%" >
				 <tr>
					
					<td colspan=3>&nbsp;</td>
				
                </tr>
                <tr>
					
					<td>&nbsp;</td>
					<td align="center">Target</td>
					<td align="center">Actual</td>
				
                </tr>
                <tr>
					<td>M</td>
					<td align="center"><?php  echo $target_income_month_b1; ?></td>
					<td align="center"><?php echo $collection_month_b1; ?></td>
					
                </tr>
				     <tr>
					<td>V</td>
					<td align="center"><?php  echo $target_income_month_b2; ?></td>
					<td align="center"><?php echo $collection_month_b2; ?></td>
					
                </tr>
				    <tr>
					<td>C</td>
					<td align="center"><?php  echo $target_income_month_b3; ?></td>
					<td align="center"><?php echo $collection_month_b3; ?></td>
				
                </tr>
                  <tr>
					<td>P</td>
					<td align="center"><?php  echo $target_income_month_b4; ?></td>
					<td align="center"><?php echo $collection_month_b4; ?></td>
				
                </tr>
                </table>
				
				
			
              <?php } ?>
            </div>
          </div>
          <div class="back">
            <div class="cell">
              <div class="group">
				   <?php if($this->request->session()->read('Auth.User.role_id')==2){ ?>
				   <div class="col-sm-12">
				  <table width="100%">
				   <tr>
					<td align="left">Target-Y</td>
					<td align="right"><?php echo number_format($target_income_year);?></td>
					 <tr>
					<tr>
					<td align="left">Actual-Y</td>
					<td align="right"><?php echo number_format($collection_year);?></td>
					 <tr>
					<tr >
					<td align="left">Pending:</td>
					<td align="right"><?php echo number_format($pending_month[0]['sum(amount)']);?></td>
                </tr>
				</table>
				
              <?php } else { ?>
                 <div class="col-sm-12">
                <table width="100%">
                <tr>
					<td></td>
					<td>Y(T)</td>
					<td>Y(A)</td>
					
                </tr>
                <tr>
					<td>M</td>
					<td><?php echo $target_income_year_b1; ?></td>
					<td><?php echo $collection_year_b1; ?></td>
					
					
                </tr>
                 <tr>
					<td>V</td>
                                        <td><?php echo $target_income_year_b2; ?></td>
					<td><?php echo $collection_year_b2; ?></td>
					
                </tr>
                 <tr>
					<td>C</td>
					<td><?php echo $target_income_year_b3; ?></td>
					<td><?php echo $collection_year_b3; ?></td>
				
                </tr>
                  <tr>
					<td>P</td>
					<td><?php echo $target_income_year_b4; ?></td>
					<td><?php echo $collection_year_b4; ?></td>
				
                </tr>
                </table>
					 </div>
                <?php } ?>
              </div>
             <!-- <a class="themebtn btn-default btn-sm" href="<?php echo ADMIN_URL;?>report">View Summary</a> </div>-->
          </div>
        </div>
      </div>
    </li>
      <li>
      <div class="flipbox blue">
        <div class="<?php if($this->request->session()->read('Auth.User.role_id')==1 || $this->request->session()->read('Auth.User.role_id')==2 || $this->request->session()->read('Auth.User.role_id')==3 ) { echo 'flipper'; } ?>">
          <div class="front">
            <div class="iconbox">
              <figure class="icon"><i class="fa fa-inr" aria-hidden="true"></i></figure>
              <span class="value" style="margin-bottom: 11px"><figure style="text-align:center; "> Expenses</figure></span>
              
                 <?php if($this->Session->read('Auth.User.branch')==0){ ?>
					    
					  <?php } else { ?>
					  	<div class="text-center bld_ttl">
             Target <br /><strong class="blue"><?php echo $target_exp_month; ?></strong>
                               </div>
					  <?php } ?>
                <?php if($this->request->session()->read('Auth.User.role_id')==1){ ?>
				 <div class="col-sm-12">
                <table width="100%" >
				
                <tr>
					
					<td>&nbsp;</td>
					<td align="center">Target</td>
					<td align="center">Actual</td>
				
                </tr>
                <tr>
					<td>M</td>
					<td align="center"><?php echo $exp_month_target_b1; ?></td>
					<td align="center"><?php echo $exp_actual_month_b1; ?></td>
					
                </tr>
				     <tr>
					<td>V</td>
					<td align="center"><?php echo $exp_month_target_b2; ?></td>
					<td align="center"><?php echo $exp_actual_month_b2; ?></td>
					
                </tr>
				<tr>
					<td>C</td>
					<td align="center"><?php echo $exp_month_target_b3; ?></td>
					<td align="center"><?php echo $exp_actual_month_b3; ?></td>
				
                </tr>
                <tr>
					<td>P</td>
					<td align="center"><?php echo $exp_month_target_b4; ?></td>
					<td align="center"><?php echo $exp_actual_month_b4; ?></td>
				
                </tr>
                </table>
				
			
              <?php } ?>
            </div>
          </div>
          <div class="back">
            <div class="cell">
              <div class="group">
				  <?php if($this->request->session()->read('Auth.User.role_id')==2){ ?>
				  <div class="col-sm-12">
				  <table width="100%">
				   <tr>
					<td align="left">Target-Y</td>
					<td align="right"><?php echo $year_target_exp; ?></td>
					 <tr>
					<tr>
					<td align="left">Actual-Y</td>
					<td align="right"><?php echo $exp_actual_year; ?></td>
					 <tr>
					<tr >
					<td align="left">Diff:</td>
					<td align="right"><?php echo ($year_target_exp-$exp_actual_year); ?></td>
                </tr>
				</table>
                <?php } else { ?>
                  <div class="col-sm-12">
                <table width="100%">
                <tr>
					<td></td>
					<td>Y(T)</td>
					<td>Y(A)</td>
					
					
					
                </tr>
                <tr>
					<td>M</td>
					<td><?php echo $year_target_exp_b1; ?></td>
					<td><?php echo $exp_actual_year_b1; ?></td>
					
					
					
					
                </tr>
                 <tr>
					<td>V</td>
					<td><?php echo $year_target_exp_b2; ?></td>
					<td><?php echo $exp_actual_year_b2; ?></td>
					
			
					
                </tr>
                 <tr>
					<td>C</td>
						<td><?php echo $year_target_exp_b3; ?></td>
						<td><?php echo $exp_actual_year_b3; ?></td>
				
					
					
                </tr>
                 <tr>
					<td>P</td>
						<td><?php echo $year_target_exp_b4; ?></td>
						<td><?php echo $exp_actual_year_b4; ?></td>
				
					
					
                </tr>
                </table>
					 </div>
                <?php } ?>
              </div>
          </div>
        </div>
      </div>
    </li>
    <li>
      <div class="flipbox purple">
        <div class="<?php if($this->request->session()->read('Auth.User.role_id')==1 || $this->request->session()->read('Auth.User.role_id')==2 || $this->request->session()->read('Auth.User.role_id')==3 ) { echo 'flipper'; } ?>">
          <div class="front">
            <div class="iconbox">
               <figure class="icon"><i class="fa fa-tty fa-3x"></i></figure>
               <figure class="icon" style="text-align:center;margin-bottom: 10px ">Enquries</figure>
             
			<?php if($this->request->session()->read('Auth.User.role_id')!=1) { ?>
			  <span class="value"><span>Today's Enquries</span><?php echo $today_enq;?></span>
			  <span class="value"><span>Today's FollowUp</span><?php echo $today_follow;?></span>
            <?php } ?>
         <?php if($this->Session->read('Auth.User.branch')==0){ ?>
					    
					  <?php } else { ?>
					  	<div class="text-center bld_ttl">
             This Month <br /><strong class="purple"><?php echo $total_enq_month+$total_follow_month; ?></strong>
                               </div>
					  <?php } ?>
            <!--  <span class="value"><span>Follow ups</span><?php echo $today_follow;?></span>-->
                <?php if($this->request->session()->read('Auth.User.role_id')==1){ ?>
                <div class="col-sm-12">
                <table width="100%" >
				
                <tr>
					
					<td>&nbsp;</td>
					<td align="center">Enquiry</td>
					<td align="center">Followup</td>
				
                </tr>
                <tr>
					<td>M</td>
					<td align="center"><?php echo $today_enq_b1; ?></td>
					<td align="center"><?php echo $today_follow_b1; ?></td>
					
                </tr>
				     <tr>
					<td>V</td>
					<td align="center"><?php echo $today_enq_b2; ?></td>
					<td align="center"><?php echo $today_follow_b2; ?></td>
					
                </tr>
				    <tr>
					<td>C</td>
					<td align="center"><?php echo $today_enq_b3; ?></td>
					<td align="center"><?php echo $today_follow_b3; ?></td>
				
                </tr>
                 <tr>
					<td>P</td>
					<td align="center"><?php echo $today_enq_b4; ?></td>
					<td align="center"><?php echo $today_follow_b4; ?></td>
				
                </tr>
                </table>
				
              <?php } ?>
            </div>
          </div>
          <div class="back">
            <div class="cell">
              <div class="group">
             <?php if($this->request->session()->read('Auth.User.role_id')==2){ ?>
              <div class="col-sm-12">
                              <table width="100%">
                <tr>
					<td></td>
					<td>Current</td>
					<td>Last</td>
					<td>Year</td>
                </tr>
                <tr>
					<td>E</td>
					<td><?php echo $total_enq_month_fran; ?></td>
					<td><?php echo $total_last_enq_month_fran;?></td>
					<td><?php echo $total_enq_year_fran;?></td>
					
                </tr>
                <tr>
					<td>F</td>
					<td><?php echo $total_follow_month_fran; ?></td>
					<td><?php echo $total_last_follow_month_fran ;?></td>
					<td><?php echo $total_follow_year_fran ;?></td>
					
                </tr>
             </table></div>
              <?php } else { ?>
                 <div class="col-sm-12">
                <table width="100%">
                <tr>
					<td></td>
					<td>Current</td>
					<td>Last</td>
					<td>Year</td>
                </tr>
                <tr>
					<td>M</td>
					<td><?php echo $total_enq_month_b1+$total_follow_month_b1; ?></td>
					<td><?php echo $total_last_enq_month_b1+$total_last_follow_month_b1 ;?></td>
					<td><?php echo $total_enq_year_b1+$total_follow_year_b1 ;?></td>
					
                </tr>
                 <tr>
					<td>V</td>
						<td><?php echo $total_enq_month_b2+$total_follow_month_b2; ?></td>
					<td><?php echo $total_last_enq_month_b2+$total_last_follow_month_b2 ;?></td>
					<td><?php echo $total_enq_year_b2+$total_follow_year_b2 ;?></td>
                </tr>
                 <tr>
					<td>C</td>
					<td><?php echo $total_enq_month_b3+$total_follow_month_b3;?></td>
					<td><?php echo $total_last_enq_month_b3+$total_last_follow_month_b3 ;?></td>
					<td><?php echo $total_enq_year_b3+$total_follow_year_b3 ;?></td>
                </tr>
                  <tr>
					<td>P</td>
					<td><?php echo $total_enq_month_b4+$total_follow_month_b4;?></td>
					<td><?php echo $total_last_enq_month_b4+$total_last_follow_month_b4 ;?></td>
					<td><?php echo $total_enq_year_b4+$total_follow_year_b4 ;?></td>
                </tr>
                </table>
					 </div>
              <?php } ?>
              </div>
          </div>
        </div>
      </div>
    </li>
    <li>
      <div class="flipbox darkgreen">
        <div class="<?php if($this->request->session()->read('Auth.User.role_id')==1 || $this->request->session()->read('Auth.User.role_id')==2 || $this->request->session()->read('Auth.User.role_id')==3 ) { echo 'flipper'; } ?>">
          <div class="front">
            <div class="iconbox">
              <figure class="icon"><i class="fa fa-graduation-cap fa-3x"></i></figure>
   
                           <figure style="text-align: center; margin-bottom: 10px">Placement</figure>

                 				 <?php if($this->request->session()->read('Auth.User.role_id')!=1){ ?> <span class="value"><?php echo $require; ?></span> <?php } ?>
			
                            <?php if($this->request->session()->read('Auth.User.role_id')==1){ ?>
							
							<div class="col-sm-12">
                <table width="100%" >
				
                <tr>
					
					<td>&nbsp;</td>
					<td align="center">Placed</td>
					<td align="center">Due</td>
				
                </tr>
                <tr>
					<td>M</td>
					<td align="center"><?php echo $total_placement_b1; ?></td>
					<td align="center"><?php echo $place_pending1; ?></td>
					
                </tr>
				     <tr>
					<td>V</td>
					<td align="center"><?php echo $total_placement_b2; ?></td>
					<td align="center"><?php echo $place_pending2; ?></td>
					
                </tr>
				    <tr>
					<td>C</td>
					<td align="center"><?php echo $total_placement_b3; ?></td>
					<td align="center"><?php echo $place_pending3; ?></td>
				
                </tr>
                  <tr>
					<td>P</td>
					<td align="center" ><?php echo $total_placement_b4; ?></td>
					<td align="center"><?php echo $place_pending4; ?></td>
					
					
                </tr>
                </table>
             
              <?php } ?>
            </div>
          </div>
          <div class="back">
            <div class="cell">
              <div class="group">
				
                <div class="col-sm-12">
                <table width="100%">
                <tr>
					<td></td>
					<td>Pending</td>
					<td>Total</td>
					
                </tr>
                <tr>
					<td>M</td>
					<td><?php echo $place_pending1; ?></td>
					<td><?php echo $total_placement_b1; ?></td>
					
					
					
                </tr>
                 <tr>
					<td>V</td>
					<td><?php echo $place_pending2; ?></td>
					<td><?php echo $total_placement_b2; ?></td>
					
                </tr>
                 <tr>
					<td>C</td>
					<td><?php echo $place_pending3; ?></td>
					<td><?php echo $total_placement_b3; ?></td>
					
					
                </tr>
                <tr>
					<td>P</td>
					<td><?php echo $place_pending4; ?></td>
					<td><?php echo $total_placement_b4; ?></td>
					
					
                </tr>
               
                </table>
					 </div>
              
              </div>
         <!--     <a class="themebtn btn-default btn-sm" href="#">View Summary</a> </div>-->
          </div>
        </div>
      </div>
    </li>
    <li>
      <div class="flipbox yellow">
        <div class="<?php if($this->request->session()->read('Auth.User.role_id')==1 || $this->request->session()->read('Auth.User.role_id')==2 || $this->request->session()->read('Auth.User.role_id')==3 ) { echo 'flipper'; } ?>">
          <div class="front">
            <div class="iconbox">
              <figure class="icon"><i class="fa fa-ban fa-3x"></i></figure>
              <figure style="text-align:center;margin-bottom: 10px ">Dropouts</figure>
           
            <?php if($this->request->session()->read('Auth.User.role_id')!=1){ ?> <span class="value"><?php echo $drop_outs; ?></span> <?php } ?>
                <?php if($this->request->session()->read('Auth.User.role_id')==1){ ?>
				
							<div class="col-sm-12">
                <table width="100%" >

                <tr>
					
					<td>&nbsp;</td>
					<td align="center">Drop</td>
					<td align="center">Loss</td>
				
                </tr>
                <tr>
					<td>M</td>
					<td align="center"><?php echo $drop_outs_b1; ?></td>
					<td align="center"><?php echo number_format($drop_last_outs_b1); ?></td>
					
                </tr>
				     <tr>
					<td>V</td>
					<td align="center"><?php echo $drop_outs_b2; ?></td>
					<td align="center"><?php echo number_format($drop_last_outs_b2); ?></td>
					
                </tr>
				    <tr>
					<td>C</td>
					<td align="center"><?php echo $drop_outs_b3; ?></td>
					<td align="center"><?php echo number_format($drop_last_outs_b3); ?></td>
				
                </tr>
                 </tr>
				    <tr>
					<td>P</td>
					<td align="center"><?php echo $drop_outs_b4; ?></td>
					<td align="center"><?php echo number_format($drop_last_outs_b4); ?></td>
				
                </tr>
                </table>
			
              <?php } ?>
            </div>
          </div>
          <div class="back">
            <div class="cell">
              <div class="group">
				  <?php if($this->request->session()->read('Auth.User.role_id')==2){ ?>
                 <div class="col-sm-6"><span>Total</span><?php echo $drop_outs_year; ?></div>
                <div class="col-sm-6"><span>Fee</span><?php echo $drop_out_total_amount; ?></div>
                <?php } else { ?>
                  <div class="col-sm-12">
                <table width="100%">
                <tr>
					<td></td>
					<td>Total</td>
					<td>Fee</td>
					
                </tr>
                <tr>
					<td>M</td>
					<td><?php echo $drop_outs_year_b1; ?></td>
					<td><?php echo number_format($drop_year_outs_b1); ?></td>
					
					
                </tr>
                 <tr>
					<td>V</td>
					<td><?php echo $drop_outs_year_b2; ?></td>
					<td><?php echo number_format($drop_year_outs_b2); ?></td>

					
                </tr>
                 <tr>
					<td>C</td>
						<td><?php echo $drop_outs_year_b3; ?></td>
					<td><?php echo number_format($drop_year_outs_b3); ?></td>

					
                </tr>
                <tr>
					<td>P</td>
						<td><?php echo $drop_outs_year_b4; ?></td>
					<td><?php echo number_format($drop_year_outs_b4); ?></td>

					
                </tr>
                </table>
					 </div>
                <?php } ?>
              </div>
           <!--   <a class="themebtn btn-default btn-sm" href="#">View Summary</a> </div>-->
          </div>
        </div>
      </div>
    </li>
  
  </ul>
</section>  
<!-- END OF FLIP BOXES -->

<!-- START OF GRAPH PANEL -->
 <section class="graphPanel">
 <div class="row">
      <aside class="col-sm-9 col-graph-box">
        <hgroup>
          <h4><strong>Statistics</strong></h4>
        </hgroup>
        <div class="graphcontainer">
          <div class="tabs_nav_container responsive">
            <ul class="nav nav-tabs" role="tablist">
              <li class="active"><a href="#tabs_1" aria-controls="tabs_1" role="tab" data-toggle="tab">Income</a></li>
              <li><a href="#tabs_2" aria-controls="tabs_2" role="tab" data-toggle="tab">Expense</a></li>
     
            </ul>
            <div class="tab-content"> 
              
              <!--tab1 start here--> 
              <div class="tab-pane fade in active" id="tabs_1">

                <div id="monthincome" style="height: 270px; width: 100%;"></div>

                     
              </div>
              <!--tab1 end here--> 
              
              <!--tab2 start here--> 
              <div class="tab-pane fade" id="tabs_2">
            <div id="exp" style="height: 270px; width: 100%;"></div>
           
              </div>
              <!--tab2 end here--> 
              
              <!--tab3 start here--> 
              <div class="tab-pane fade" id="tabs_3">
                <div id="signups" style="height: 270px; width: 100%;"></div>
              </div>
              <!--tab3 end here--> 
              
              <!--tab4 start here--> 
              <div class="tab-pane fade" id="tabs_4">
                <div id="products_graph" style="height: 270px; width: 100%;"></div>
              </div>
              
              <!--tab4 end here--> 
              <div class="tab-pane fade" id="tabs_5">
                <div id="affiliate_graph" style="height: 270px; width: 100%;"></div>
              </div>
              
               <div class="tab-pane fade" id="tabs_6">
                <div id="actual_vs_target" style="height: 270px; width: 100%;"></div>
              </div>
            </div>
          </div>
        </div>
        
        <!--
                                <div class="paneltop"><h4><strong>Sales</strong> Statistics</h4></div>
                                <div class="graphcontainer" id="monthlysales"></div> --> 
      </aside>
      <aside class="col-sm-3 col-graph-content">
        <table class="table graphcontent">
          <thead>
            <tr>
              <th></th>
              <th>Total Collections</th>
             
            </tr>
          </thead>
          <tbody>
            <tr class="first">
              <td>Today</td>
              <td><i class="fa fa-inr" aria-hidden="true"></i><?php echo number_format($today_cash,2); ?></td>
             
            </tr>
            <tr>
              <td>This Week</td>
              <td><i class="fa fa-inr" aria-hidden="true"></i><?php echo number_format($week_cash); ?></td>
              
            </tr>
            <tr>
              <td>This Month</td>
                <td><i class="fa fa-inr" aria-hidden="true"></i><?php echo number_format($month_cash); ?></td>
             
            </tr>
            <tr>
              <td>Last 3 Months</td>
             <td><i class="fa fa-inr" aria-hidden="true"></i><?php echo number_format($month3_cash); ?></td>

            </tr>
            <tr>
              <td>Year Total</td>
            <td><i class="fa fa-inr" aria-hidden="true"></i><?php echo number_format($year_cash); ?></td>
            
            </tr>
          </tbody>
        </table>
      </aside>
    </div>  
    </section>
<!-- END OF GRAPH PANEL -->    



    <section class="latst_order">
      <hgroup>
        <h4 class="pull-left">Latest 5 Registration </h4>
        <!--<a href="" class="themebtn btn-default btn-sm">View All</a>-->
        <div class="view_drop pull-right">
         <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="viewdrop"><i class="fa fa-ellipsis-v fa-lg"></i></a>
            <div class="dropdown-menu" aria-labelledby="viewdrop">
              <ul class="linksvertical">
                <li><a href="#">View All</a></li>
              </ul>
            </div>
         </div>
      </hgroup>
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th width="2%">#</th>
                <th width="15%">Enroll</th>
                <th width="15%">Name</th>
                <th width="15%">Mobile</th>
                <th width="15%">Reg. Date</th>
                <th width="22%">Course</th>
               <!-- <th width="8%"></th>-->
              </tr>
            </thead>
            <tbody>
				<?php $count=0; foreach($latest_5 as $st){ ?>
              <tr>
                <td><?php echo ++$count; ?></td>
                <td><?php echo $st['Student']['s_enroll']; ?></td>
                <td><?php echo $st['Student']['s_name']; ?></td>
                  <td><?php echo $st['Student']['s_contact']; ?></td>
                <td><?php echo date('j, F Y',strtotime($st['Student']['add_date'])); ?></td>
                <td><?php $cs="";
                $courses=$this->Common->getcourses($st['Student']['s_id']);
                foreach($courses as $c){   $cs.=$c['Course']['sname']."<br>"; }  echo trim($cs,'<br>');
                  ?></td>
               <!-- <td><span class="label label-info"> <?php echo $st['Student']['s_contact']; ?></span></td>-->
                <!--<td><a title="View Order" href="#" class="fa fa-eye"></a></td>-->
              </tr>
              <?php } ?>
              
            </tbody>
          </table>
        </div>
    </section>

  
   



</section>
</div>



