<table  class="table table-bordered table-striped afTblStndRept" id="example33" width="100%">
               
               <thead class="thead-dark">
               <tr>
                       <th >No.</th>
                       <th style="text-align:left;">Course Name</th>
                      
                       <th style="text-align:left;">Short Name</th>
                       <th style="text-align:left;">Sort Order</th>
                       
                       <th style="text-align:left;">Fees</th>
                       <th style="text-align:left;">Added On</th>
                       <th style="text-align:left;">Student Count</th>


                       
<th style="text-align:left;">Action</th>
                  
                   </tr>
               </thead>
               <tbody>
               <?php 
           $counter=($this->request->params['paging']['Course']['page']-1) * $this->request->params['paging']['Course']['perPage']; 
           if(isset($rec) && !empty($rec)){ 
           foreach($rec as $product){
             
             $courseSeo=$this->Comman->seoFind($product['id']);
                       $stuCount=$this->Comman->getCourseStudentCount($product['id']);
             
             
             
             
             
             //pr($product);die;?>
             <tr>
               <td><?php echo $counter+1;?></td>
               <td style="text-align:left;"><b><?php echo $product['cname']; if(empty($courseSeo)){ echo " <span style='color:red'>*</span>"; } ?></b></td>
              <td style="text-align:left;"><?php echo $product['sname'] ?></td>
               <td style="text-align:left;"><?php if($product['sort_order']){ echo $product['sort_order']; }else{ echo $product['sort']; } ?></td>
               <td style="text-align:left;"><?php echo $product['fees']; ?></td>
               <td style="text-align:left;"><?php echo strftime('%d-%b-%Y',strtotime($product['add_date'])); ?></td>
               <td style="text-align:left;"><?php echo $stuCount; ?></td>

                             
               <td style="
    display: flex;
    align-items: center;
    justify-content: center;
">

   <?php if($product['feature']=="N"){ ?>
      <a href="<?php echo ADMIN_URL; ?>course/feature/<?php echo $product['id'];?>/Y" class="fa fa-circle" title="Featured" style="font-size: 20px !important; color:gray;margin-right: 10px;"></a>
<?php }else{ ?>
  <a href="<?php echo ADMIN_URL; ?>course/feature/<?php echo $product['id'];?>/N" class="fa fa-circle" title="Unfeatured" style="font-size: 20px !important; color:#48bd4c;margin-right: 10px;"></a>
<?php } ?>
               <?php if($product['status']==0){ ?>
                           <a
                               href="<?php echo ADMIN_URL?>course/admin_make_supportiv/inactive/<?php echo $product['id'];?>">
                               <img src="<?php echo SITE_URL?>admin_images/active.png"></a>
                           <?php } else { ?>
                           <a
                               href="<?php echo ADMIN_URL?>course/admin_make_supportiv/active/<?php echo $product['id'];?>">
                               <img src="<?php echo SITE_URL?>admin_images/inactive.png"></a>
                           <?php } ?>
               <?php  echo $this->Html->link(__(''), ['action' => 'edit', $product->id,],array('class'=>'fa fa-pencil-square-o fa-lg','title'=>'Edit','style'=>'font-size: 20px !important; margin-left: 12px;')) ?>
                

                <?php echo $this->Html->link('', ['action' => 'delete',$product->id],['title'=>'Delete','class'=> 'fa fa-trash','style'=>'color:#FF0000; margin-left: 13px; font-size: 19px !important;',"onClick"=>"javascript: return confirm('Are you sure do you want to delete this Course')"]); ?>
              </td>
            </tr>
            <?php $counter++;} } ?>  





                  
               </tbody>
           </table>
             <?php echo $this->element('admin/pagination'); ?> 