<link href="https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />

<style type="text/css">
	.text{color:red; font-size: 12px;}
	.filename{font-size: 11px;color: #e02727;}
	.fr-wrapper>div:first-child>a:first-child{display:none !important;}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Course Manager
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo SITE_URL; ?>admin/students"><i class="fa fa-home"></i>Home</a></li>
			<li><a href="<?php echo SITE_URL; ?>admin/course">Manage Course</a></li>
			<?php if(isset($newresponse['id'])){ ?>
				<li class="active"><a href="javascript:void(0)">Edit Course</a></li>   
			<?php } else { ?>
				<li class="active"><a href="javascript:void(0)">Add Course</a></li>
			<?php } ?>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content addCrsMngPg">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<?php echo $this->Flash->render(); ?>
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-plus-square" aria-hidden="true"></i> 
	<?php if(isset($newresponse['id'])){ ?> Edit Course <?php } else { ?> 	Add Course <?php } ?></h3>
					</div>
					<?php echo $this->Form->create($newresponse, array(
						'enctype' => 'multipart/form-data',
						'class'=>'needs-validation',
						'novalidate'
						
					));
				
					 ?>
					<div class="box-body addStdntsFrm">
						<div class="row">

						<div class="col-sm-3" >
				
					
					
				<label class="inputEmail3">Course Group</label>

		<?php
		
echo $this->Form->input('course_group', array('label'=> false,'class' => 'smallselect form-control','empty'=>'--Select Course Group--','options' =>$coursesGroup)); 
		?>

		</div>
				   <div class="col-sm-3">
		          <label for="inputEmail3" class="control-label">Full Course Name <strong style="color:red;">*</strong></label>
       		     <?php echo $this->Form->input('cname', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Full Course Name','required','autofocus','autocomplete'=>'off')); ?>   
				
				</div>
							

						
			
			
			
							<div class="col-sm-3">
	          	<label for="inputEmail3" class="control-label">Short Name <strong style="color:red;">*</strong></label>
       		      <?php echo $this->Form->input('sname', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Short Name','autofocus','autocomplete'=>'off')); ?>   
				
							</div>
			
			
						
							<div class="col-sm-3" style="display:none;">
		          <label for="inputEmail3" class="control-label">Locality <strong style="color:red;">*</strong></label>
		<?php $fee_op=array('vidyadharnagar'=>'Vidyadhar Nagar','c-scheme'=>'C-Scheme','vaishalinagar'=>'Vaishali Nagar','mansarover'=>'Mansarover','banipark'=>'Banipark','murlipura'=>'Murlipura','jhotwara'=>'Jhotwara');
		
		?>


<select name="locality[]" required class="smallselect form-select" multiple="multiple" id="locality">
			<option value="">--Select--</option>
			<?php  foreach($fee_op as $kry=>$item){ ?>
<option selected  value="<?php echo $kry; ?>"><?php echo $item; ?></option>
<?php } ?>
</select>


		</div>
				
								<div class="col-sm-1">
		                  <label for="inputEmail3" class="control-label">Sort Order <strong style="color:red;">*</strong></label>
       	     	<?php echo $this->Form->input('sort', array('class' => 'form-control','required','label'=>false,'type'=>'text','onkeypress'=>'return isNumber(event)','autofocus','autocomplete'=>'off')); ?>   
						</div>
						<div class="col-sm-2" >
				
					
					
				<label class="inputEmail3">Duration</label>

		<?php
		$time=array('1 Months'=>'1 Months','2 Months'=>'2 Months','3 Months'=>'3 Months','4 Months'=>'4 Months','5 Months'=>'5 Months','6 Months'=>'6 Months','7 Months'=>'7 Months','8 Months'=>'8 Months','9 Months'=>'9 Months','10 Months'=>'10 Months','11 Months'=>'11 Months','12 Months'=>'12 Months','14 Months'=>'14 Months','2 Years'=>'2 Years','3 Years'=>'3 Years');



		echo $this->Form->input('duration', array('label'=>false,'class' => 'smallselect form-control','required','options' =>$time,'id'=>'duration')); 
		?>

		</div>
							<div class="col-sm-1" >
         
			
			
							<label class="inputEmail3">Fee Type</label>
     
               <?php
               $fee=array('m'=>'Monthly','h'=>'Half Year','y'=>'Year');
              
                

		echo $this->Form->input('ftype', array('label'=>false,'class' => 'smallselect form-control','options' =>$fee,'required','id'=>'category')); 
		?>
				
				</div>


			<div class="col-sm-2">
				<label for="inputEmail3" class="control-label">Fees<strong style="color:red;">*</strong></label>
		<?php echo $this->Form->input('fees', array('class' => 'form-control','required','label'=>false,'onkeypress'=>'return isNumber(event)','autofocus','autocomplete'=>'off')); ?>   
						
									</div>	
									
												
									
				
		<div class="col-sm-3">

		<label class="inputEmail3">Subjects</label>

		<?php if($newresponse['id']){ 
			
			$rty=explode(',',$newresponse['qc_id']);
			?>
		<select name="qc_id[]" class="smallselect form-select" required multiple="multiple" id="duration">
			<option value="">--Select--</option>
			<?php  foreach($subjectsList as $kry=>$item){ ?>
<option <?php if(in_array($kry,$rty)){ ?>selected <?php } ?> value="<?php echo $kry; ?>"><?php echo $item; ?></option>
<?php } ?>
</select>

<?php }else{ ?>
<?php
echo $this->Form->input('qc_id[]', array('label'=>false,'class' => 'smallselect form-select','type' => 'select','multiple','options' =>$subjectsList,'id'=>'duration','required','default'=>$newresponse['qc_id'])); 
?>
<?php } ?>
				</div>
		<div class="col-sm-3">
				<label for="inputEmail3" class="control-label">Select Course Icon <strong style="color:red;">*</strong></label>

				<?php if($newresponse['icon']!=""){ ?>
					<?php echo $this->Form->input('icon', array('value'=> $newresponse['icon'],'class' => 'form-control',"label"=>false,'type'=>'file','autofocus','autocomplete'=>'off')); ?>  
					<?php }else{ ?>
						<?php echo $this->Form->input('icon', array('class' => 'form-control',"label"=>false,'type'=>'file','required','autofocus','autocomplete'=>'off')); ?> 
					
					<?php } ?> 
				<?php if($newresponse['icon']){ ?>
					<img src="<?php echo SITE_URL; ?>images/course/<?php echo $newresponse['icon']; ?>" height="100" width="100">	

					<?php } ?>
									</div>	
				
		<div class="col-sm-3">
				<label for="inputEmail3" class="control-label">Select Page Image <strong style="color:red;">*</strong></label>

				<?php if($newresponse['image']){ ?>
					<?php echo $this->Form->input('image', array('value'=> $newresponse['image'],'class' => 'form-control',"label"=>false,'type'=>'file','autofocus','autocomplete'=>'off')); ?>   
					<?php }else{ ?>
						<?php echo $this->Form->input('image', array('class' => 'form-control',"label"=>false,'type'=>'file','autofocus','autocomplete'=>'off','required')); ?>   
						<?php } ?>
				
				<?php if($newresponse['image']){ ?>
					<img src="<?php echo SITE_URL; ?>images/course/<?php echo $newresponse['image']; ?>" height="100" width="300">	

					<?php } ?>
									</div>
				
				
				
				<div class="col-sm-3" >
				<label for="inputEmail3" class="control-label">Enter Image Title <strong style="color:red;">*</strong></label>
					
				<?php echo $this->Form->input('imgtitle', array('class' => 'form-control','required','label'=>false,'type'=>'text','autofocus','autocomplete'=>'off')); ?>   
						
			

		</div>
				



	
		

		
			<div class="col-sm-12">
		
			<label for="inputEmail3" class="control-label">Description</label>
			<?php echo $this->Form->input('description', array('class' =>'form-control addCrsText','placeholder'=>'Content','required','label'=>false,'type'=>'textarea','autocomplete'=>'off','id'=>'edit')); ?>
			</div>


						</div>
	
	
				</div>

				
<!--  -->


				
	
	
	






<!-- testing -->


					


				
						<!-- row -->
						<div class="addStdntsFrm addStdntsFrmBtns ">
						<div class="box-footer" id="sub">
						<?php echo $this->Html->link('Back', ['action' => 'index'],['class'=>'btn btn-default']); ?>
							<?php echo $this->Form->submit('Save',array('class' => 'btn btn-info pull-right', 'title' => 'Save')); ?>
						
						</div>
				</div>
						<!--  -->
		</div>
				</div>

		
				</div>















				
		
						
						<?php echo $this->Form->end(); ?>
					</div> 
				</div>
			</div>
		</section>
	</div> 
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/js/froala_editor.pkgd.min.js"></script>

<!-- Initialize the editor. -->
 <script>
  new FroalaEditor('#edit', {
    // Set the file upload URL.
    imageUploadURL: '<?php echo SITE_URL; ?>upload_image.php',
    imageUploadParams: {
      id: 'my_editor'
    }
  })
</script>
	<script>
$(document).ready(function(){
  var course_fees;
	$('#course_id').change(function(){
		
		//alert(userbranch);
		var id=$(this).val();
    var degreeCourses=<?php echo json_encode($degrecourseList) ?>;
    console.log(degreeCourses);
    if(Object.values(degreeCourses).includes(id)){
      $('.session').show();
    }else{
      $('.session').hide();
    }
		//alert(id);
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/checkcoursefeeflexibility',
		   type:'POST',
		   data:{'id12': id},
		   success:function(data){
			//alert(data);
			if(data.trim()!='0' && data.trim()!='1'){
       
		var userbranch='<?php echo $this->request->session()->read('Auth.User.branch'); ?>';   
		$('#rfee').val(data);
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/inst',
		   type:'POST',
		   data:{'id12': id, 'branch': userbranch},
		   success:function(data1){
			//alert(data1);
		
			if(data1.trim()=='5'){
				$('#sk').html('<div id="pk" style="color:red;"><label class=" text-right">Installment Type </label> <div class="input text"><span class="field" id="kl"> No Installment Type Added For This Course.</span></div></div>');
				
				$('#sub').hide();
			} else {
			$('#sk').html(data1);
			$('#sub').show();
		}
		   },

			});	   
		} 
		
		if(data.trim()=='1'){
			var userbranch='0';   
		
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/inst',
		   type:'POST',
		   data:{'id12': id, 'branch': userbranch},
		   success:function(data1){
			//alert(data1);
			$('#sk').html(data1);
			
		   },

			});
			
		} 
		
		if(data.trim()=='0'){
	

		$('#sub').hide();	
		$('#sk').html('<div id="pk" style="color:red;"><label class="text-right">Installment Type </label><div class="input text"><span class="field" id="kl"> Course is not approved by Admin.</span></div></div>');
				
				$('#sub').hide();
		} 
		   },

      });
     		
		});
	
	});
</script> 
<script>
$(function() {
$( "#datepicker" ).datepicker();

});
$(function() {
$( "#datepickerbrth" ).datepicker({minDate:null,changeYear:true,changeMonth:true,yearRange:"c-40:c"});

});



</script>


<script>

function getrefdata(data){
var type=data.value;
if(type==11){
  $('#updaterefdetails').css('display','block');
  $('#rname').attr('required',true);
  $('#rmobile').attr('required',true);
   $('#rmobile').val('<?php echo $this->request->data["rmobile"] ?>');

  $('#rname').val('<?php echo $this->request->data["rname"] ?>');
}
else{

  $('#rname').removeAttr('required');
  $('#rmobile').removeAttr('required');
  $('#rmobile').val('');

  $('#rname').val('');
    $('#updaterefdetails').css('display','none');

}
}
</script>
<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
    
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>

<script>
function isNumber(evt) {

evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;
if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {

return false;
}
return true;
}

</script>