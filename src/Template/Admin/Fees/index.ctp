<!-- <script>
   function checkextension() {
    var file = document.querySelector("#fUpload");
    if ( /\.(jpe?g|png|gif)$/i.test(file.files[0].name) === false ) 
      { alert("not an image please choose a image!");
    $('#fUpload').val('');
  }
  return false;
}
</script> -->
<style type="text/css">
.box.box-info{ margin-bottom:15px;}
table td  img{ display:inline-block; width:20px;}
	.text{color:red; font-size: 12px;}
  .tableCrsDtl td{ text-align:center;}
	.filename{font-size: 11px;color: #e02727;}
  .stuInstalltbl tbody.stuInstalltbltbody{ justify-content:end;}
  .stuInstalltbl tbody.stuInstalltbltbody>tr:last-child>td:first-child{ margin-left:68%;}



</style>
<div class="content-wrapper studentFeePgW">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Fee Student 
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo SITE_URL; ?>admin/students"><i class="fa fa-home"></i>Home</a></li>
			<li><a href="<?php echo SITE_URL; ?>admin/students">Manage Student</a></li>
			<?php if(isset($newresponse['id'])){ ?>
				<li class="active"><a href="javascript:void(0)">Edit Student</a></li>   
			<?php } else { ?>
				<li class="active"><a href="javascript:void(0)">Add Student</a></li>
			<?php } ?>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
			

				<div class="box box-info boxInfoBx boxInfoBxStudentInfo">
					<?php echo $this->Flash->render(); ?>
					<!-- <div class="box-header with-border">
						<h3 class="box-title">Student Info</h3>
					</div> -->
			
					<div class="box-body addStdntsFrm"> 
                    <div class="row d-flex">
<div class="col-sm-2 align-self-center text-center" style="padding-top: 0px;padding-left:50px;"><?php 
$path=LOCALWEB.$destination[0]['student']['image'];
//echo $path; die;
if(!empty($destination[0]['student']['image']) && file_exists($path)){ ?>
<img src="<?php echo SITE_URL;?>images/student/<?php echo $destination[0]['student']['image']?>" alt="img" style="width:100px;height:100px"> <?php } else{ ?><img src="https://daac.in/images/student/noimage.jpg" alt="img" style="width:100px;height:100px"><?php } ?>
  <?php if($this->request->session()->read('Auth.User.role_id') != 1){ ?>  
<div class="text-center">
  <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#feeDepositMdl" class="depositFeeBtn">Deposit Fee</a>
</div>
<?php } ?>
</div>
<!--  -->

               <div class="col-sm-4 align-self-center"  >
               
               <table style="width:100%">
               <?php /*
               <tr style="width:100%"><td style="text-align:left; width:30%; padding-right:2%"><b>Student Name</b></td><td><b>:</b>   <?php echo ucwords($stu_det['s_name'])?></td></tr> */ ?>

               <tr style="width:100%"><td style="text-align:left; width:30%; padding-right:2%"><b>Enroll. No</b></td><td><b>:</b>   <?php echo $stu_det['s_enroll']?></td></tr>
              <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Name</b></td><td><b>:</b>   <?php echo ucwords($stu_det['s_name'])?></td></tr>
               <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Father's Name</b></td><td><b>:</b>   <?php echo ucwords($stu_det['f_name'])?></td></tr>
                <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Mobile No</b></td><td><b>:</b>   <?php echo $stu_det['s_contact']?></td></tr>
                <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Branch</b></td><td><b>:</b>   <?php echo $stu_det['branch']['name']?></td></tr>
               </table>            
               </div>
               <div class="col-sm-6 align-self-center" style="padding-left:0px">


               <table class="table table-bordered table-striped afTblStndRept tableCrsDtl" width="100%">
               
               <thead class="thead-dark">
                      <th class="head0" style="text-align:center">Sr. No.</th>
                      <th class="head0" style="text-align:center">Course Name</th>
                      <th class="head0" style="text-align:center">Total Fees</th>
                      <th class="head0" style="text-align:center">Discount</th>
                      <th class="head0" style="text-align:center">Fees To be Paid</th>
                      <th class="head0" style="text-align:center">Fees Paid</th>
                      <th class="head0" style="text-align:center">Balance</th>
                    </thead>
                    <tbody>
                     <?php $cnt=1;
                     $cor_opt=array(); 
                     foreach($stu_course as $course){ 
                       
                       ?>
                        <tr <?php if($course['drop']!=0){ ?> style="color:red"<?php } ?>>
                        <td><?php echo $cnt; ?></td> 
                        <td><?php echo $course['name']; ?></td>
                        <td><?php echo $course['fees']; ?></td>
                        <td><?php echo $course['dis']; ?></td>
                        <td><?php echo $course['fees']-$course['dis']; ?></td>
                        <td><?php echo $course['fee_paid']; ?></td>
                        <td><?php 
                        $fee_pending[$course['course_id']]=$course['fees']-$course['fee_paid']-$course['dis'];
                       
                        echo $fee_pending[$course['course_id']] ?><a href="<?php echo SITE_URL; ?>admin/students/pdfreportfeeplan/<?php echo $stu_det['s_id'];?>/<?php echo $course['course_id'];?>" target="_blank" title="Print Statement" style="color:red; font-size:14px;margin-right:0px"><i class="fa fa-file-pdf-o" aria-hidden="true" style="margin-left:5px"></i>
</a></td>
                        </tr>
                      <?php $cnt++;
                    if($fee_pending[$course['course_id']]>0){
                      $cor_opt[$course['course_id']]=$course['name'];
                    }
                    } ?>
                    </tbody>
                  </table>
               </div>
               </div>
               
                </div>
			
							
                      		
						
					</div> 
					<!--  -->

		</div>


    <div class="col-md-12">
      <div class="row">
<div class="col-sm-6" >
        <div class="box box-info studentInstlmntBx">
			
						
			
			

<h3 class="box-title">Students Installments</h3>
					<div class="box-body addStdntsFrm"> 
                    <div>

           
               <table class="table table-bordered  afTblStndRept stuInstalltbl" width="100%">
                 
            
               <thead class="thead-dark">
   <tr>
     <th class="head0" width="12%"  >Sr. No. <!--<span class="checkbox">
                              <input type="checkbox" class="checkall" /></span>--></th>

                              <th class="head0" width="34%">Course Name</th>
                              <th class="head0" width="34%">Propose Date</th>
                              
                              <th class="head0" width="20%">Amount</th>
                             

                            </tr>
                            </thead>
                
                         
                    <tbody class="stuInstalltbltbody">
                     <?php $cnt=1;
                    
                     $cnt=1; if(count($destination)>0){ 
                        $total_inst=0;
                         ?>
                      <?php foreach($destination as $key=>$value) { //pr($value);
                
                    if (!in_array($value['course_id'], $course_ids)) {
                    
                        if($value['is_reg']=='N'){
                    continue;
                      }
                    }

                   
$fee=$this->Comman->fees($value['s_id']);
$crs=$this->Comman->crs($value['course_id']);
?>

<tr class="gradeX" id="tyu">



<td class="center"><?php echo $cnt; ?> </td>

<td style="text-align:left"><?php echo $value['course']['sname']; ?></td>

<td><?php echo date("jS M, Y", strtotime($value['pro_date']));  ?></td>



<?php $tax=$value['igst_amount']+$value['cgst_amount']+$value['sgst_amount'];
$tota=$value['amount']+$tax+$value['fine']; ?>


<td class="amt"><?php echo round($value['amount']);  $total_inst+=$value['amount'];?></td>


</tr>

<?php $cnt++;
} ?>
<tr>
<td colspan="3" style="text-align:right"><b>Total Fees</b></td>
<td><b><?php echo $total_inst;?></b></td>
</tr>

<?php } else{  ?>    
<tr><td colspan="10" align="center">No Meta Available</td></tr>
<?php } ?>




                    </tbody>
                  </table>
             
               </div>
               
                </div>
                </div>
</div>
                <div class="col-sm-6" >
			
                <div class="box box-info paymentHistrStudntBx">
				
        <h3 class="box-title">Payment History</h3>
  
  
      <div class="box-body addStdntsFrm">
                <div class="row">

       
           <table class="table table-bordered table-striped afTblStndRept " width="100%">
           
           <thead class="thead-dark">
<tr>
 <th class="head0" style="width:100px;"  >Sr. No. 

                          <th class="head0" style="width:180px;">Payment Date	</th>
                          <th class="head0" style="width:180px;">Receipt No	</th>
                          
                          <th class="head0" style="width:180px;">Amount</th>
                          <th class="head0" style="width:180px;">Fine</th>
                          <th class="head0" style="width:180px;">Action</th>
                         

                        </tr>
                      </thead>
                <tbody>
                <?php $cnth=1; if(count($pay_his)>0){ 
            $total_inst=0; 
             ?>
                          <?php foreach($pay_his as $his_key=>$his_value) {
                   

$crs=$this->Comman->crs($his_value['course_id']);

?>

<tr class="gradeX" id="tyu">

<td class="center"><?php echo $cnth; ?>
</td>


<td><?php echo date('d-m-Y',strtotime($his_value['pay_date'])); ?></td>
<td><?php echo $his_value['receipt_no']; ?></td>
<td><?php echo $his_value['amount']; ?></td>
<td><?php echo $his_value['fine']; ?></td>
<?php if(empty($his_value['sid_id'])){ ?>
<td><a href="<?php echo SITE_URL?>admin/Fees/receiptpdf/<?php echo $his_value['s_id'];?>/<?php echo $his_value['receipt_no']; ?>/<?php echo $his_value['course_id']; ?>" target="_blank" title="Print Receipt"><img src="https://daac.in/images/dash_13.png" /> </a>
<?php if($role_id==1) { ?>
<a href="<?php echo SITE_URL?>admin/Fees/delete_receipt/<?php echo $his_value['receipt_no']; ?>/<?php echo $his_value['s_id']; ?>" onclick="return confirm('Are You Sure you want to delete')" ><img src="https://daac.in/images/dash_10.png" /></a>
<?php } ?>
</td>
<?php } else{ ?>
<td><a href="<?php echo SITE_URL?>admin/Fees/receiptpdf/<?php echo $his_value['sid_id'];?>" target="_blank"><img src="https://daac.in/images/dash_13.png" /></a></td>
<?php } ?>
<?php $tax=$value['igst_amount']+$value['cgst_amount']+$value['sgst_amount'];
$tota=$value['amount']+$tax+$value['fine']; ?>

</tr>

<?php $cnth++;
}  } else{  ?>    
<tr><td colspan="10" align="center">No Meta Available</td></tr>
<?php } ?>


                </tbody>
              </table>
         
           </div>
           
            </div>
  
          
                      
        
      </div> 
               
					</div> 

          
					<!--  -->

		</div>
</div>
<!-- ...... -->
</div>
</div>




<div class="col-sm-6">

<!--  -->



<!-- Modal -->
<div class="modal fade" id="feeDepositMdl" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Deposit Fees</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       <!-- feeDeposit-Start -->
       <?php if($this->request->session()->read('Auth.User.role_id') != 1){ ?>   
                
                 <div class="dataTables_wrapper" id="dyntable_wrapper">	
                 <?php echo $this->Form->create("Fee", array(
						'class'=>'form-horizontal',
						'type' => 'file','validate'
						
					));
				
					 ?>
      <input type="hidden" name="token" value="<?php echo md5(uniqid(rand(), true)); ?>">
    <input type="hidden" name="branch" value="<?Php echo $branch; ?>">
<input type="hidden" name="s_id" value="<?php echo $destination[0]['s_id'] ?>">
<input type="hidden" name="inst_id" value="0" id="inst_id">
                <div class="stdtable stdtablecb nkop" id="dyntablzze">

  <!-- <colgroup>
    <col class="con0" />
    <col class="con1" />
    <col class="con0" />
    <col class="con1" />
    <col class="con0" />
  </colgroup> -->
  <!-- <thead>
 
  <th class="head0" colspan="10" style="text-align:center"></th>
  
  </thead> -->
  <?php $tyu=array(); 
  
  foreach($destination as $key=>$value){
	  if($value['pay_date']=='0000-00-00' && $value['is_drop']=='0'){
	  $tyu[]=$value['course_id'];
  }
  } 
  
  $ry=array_unique($tyu); 
  
  $cn=count($ry); 

  if(array_sum($fee_pending)>0){  ?>
 
  <div> 
  

  <div class="row"> 
 <div class="col-md-6">
  <label>Select Course</label>

  <div>
 <select required class="cor_radio form-control" name="cid" >
 <option value="0">Select Course</option> 
  <?php
 
// $course_det=array_unique($course_det);

  foreach($cor_opt as $key1=>$value1){    
    ?>
    <option value="<?php echo $key1; ?>"><?php echo $value1; ?></option>
   <?php } ?></select></div> </div>


  <div class="col-md-6">

<label > Amount</label>
<div><input required type="text" class="form-control" maxlength="7" name="amount" placeholder="Amount" id="fees_add" onkeypress="return isNumber()"></div>

	</div>
  <!--  -->
<div class="col-md-6">

<label > Fine</label>
<div><input  type="text" name="fine" maxlength="4" class="form-control" placeholder="Fine Amount" id="fin" onkeypress="return isNumber()">
<input type="hidden" id="tes" value="0">
</div>

	</div>
<!--  -->
	<div class="col-md-6">

<label >Mode</label>

<br>
<div class="form-check form-check-inline">
  <input type="radio" class="rad form-check-input" id="modeRadio1" checked="checked" name="mode" value="cash">
  <label class="form-check-label" for="modeRadio1">Cash</label>
  </div>


  <div class="form-check form-check-inline">
    <input type="radio" class="rad form-check-input"   name="mode" id="modeRadio2" value="bank">
    <label class="form-check-label" for="modeRadio2">Bank</label>
    
  </div>

  <div class="form-check form-check-inline">
    <input type="radio" class="rad form-check-input"  name="mode" id="modeRadio3" value="paytm">
    <label class="form-check-label" for="modeRadio3">Paytm</label> 
  </div>
 

  
  <div>
<!--  -->

<!--  -->
<?php /*

<div class="btn-group" role="group" aria-label="Basic radio toggle button group">



  <input type="radio" class="rad btn-check"  id="modeRadio1" checked="checked" name="mode" value="cash" autocomplete="off">
  <label class="btn btn-outline-primary" for="modeRadio1">Cash</label>

  <!--  -->

 
    <input type="radio" class="rad btn-check"   name="mode" id="modeRadio2" value="bank" autocomplete="off">
    <label class="btn btn-outline-primary" for="modeRadio2">Bank</label>
    

  <!--  -->
 
    <input type="radio" class="rad btn-check"  name="mode" id="modeRadio3" value="paytm" autocomplete="off">
    <label class="btn btn-outline-primary" for="modeRadio3">Paytm</label> 
  
  <!--  -->

  </div>
  <!-- group -->
*/ ?>

  
  </div>

	</div>
	<!--  -->
	<div class="hj col-md-6" style="display:none;">

<label>Bank</label>
  <div><input  type="text" name="bank" class="sde form-control" placeholder="Bank"></div>


	</div>
	<!--  -->


	<div  class="hj col-md-6" style="display:none;">

<label >Cheque/Ref. No.</label>
  <div><input type="text" name="cheque" class="sde form-control" placeholder="Cheque NO." ></div>


	</div>
  <!--  -->
  <div class="paytm col-md-6" style="display:none">

  <label>Reference No.</label>
  <div><input type="text" name="paytm" class="paytm_ref form-control" placeholder="Reference No." ></d>
  </div>
	
	</div>
	<!--  -->
	<div class="col-md-12">


  <div colspan="2" align="center"><input type="submit" name="student" value="Fee Submit" onclick="return mydata();"></div>


	</div>

  </div>





  <?php } else{ ?>
<tbody>
<tr></tr>
<tr><td colspan="10" style="text-align:center"><h3>No Fees Due</h3></td></tr>
<tr></tr>
</tbody>
  <?php } ?>

</table>
<?php echo $this->Form->end(); ?>


<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
    
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
<script>
function mydata(){
  //alert($('.cor_radio').val());
  if($('.cor_radio').val()==0){
	alert("Please Select Course");
	return false;
	}

  var course_id=$('.cor_radio').val();
  var fees=$('#fees_add').val();
  if(!(fees>0))
  {
    alert('Please Enter Amount');
    return false;
  }
   var fee_pending=<?php echo json_encode($fee_pending);?>;
    if(fees > fee_pending[course_id])
  {
    	alert("Amount is greater than Pending fees");
      $('#fees_add').val(0);
	return false;
  }
  
}
</script>


<script>
$('.rad').change(function(){
	var fg=$(this).val();
  
	if(fg=='bank'){
    $('.paytm').hide();
		$('.hj').show();
		$('.sde').attr('required',true);
	}else if(fg=='paytm'){
    $('.hj').hide();
$('.paytm').show();
$('.paytm_ref').attr('required',true);
  }else {
		$('.paytm').hide();
		$('.hj').hide();
		$('.sde').removeAttr('required','required');
		$('.paytm_ref').removeAttr('required','required');
	}
	
	});


</script>


<script>
$('#fin').keyup(function(){

	var yu=$(this).val();
	//alert(yu);
	if(yu>500){
		alert('please enter fine amout below 500');
		$(this).val("");
		yu='0';
	}
	var sdf =$('#tes').val();
	//alert(sdf);
	if(yu==''){
		yu='0';
	}
	

		var ot=parseFloat(sdf)+parseFloat(yu);
		$('#tot').html(ot);
		$('#amout').val(ot);
		

	
	
	});


</script>
<script>
function isNumber(evt) {

evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;
if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {

return false;
}
return true;
}

</script>


</div>
                </div>
<?php  } ?>	
       <!-- feeDeposit-End -->
      </div>
    
    </div>
  </div>
</div>
<!--  -->


</div>
		<!-- col-md-6			 -->

		</div>





				</div>
</div>
			</div>
		</section>
	</div> 
