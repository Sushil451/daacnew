<link rel="icon" href="<?php echo SITE_URL;?>images/favicon_icon.png" type="image/x-icon">
<?php //pr($order); die;
class xtcpdf extends TCPDF {
}

$pdf = new TCPDF('L', 'mm', 'A5', true, 'UTF-8', false, true);

// set document information
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
$pdf->AddPage();

$pdf->SetY(-260);
//$pdf->SetTextColor(0, 0, 0);
//$pdf->SetFont('Arial', '', 12);

$pdf->SetAutoPageBreak(false, 0);
//$pdf->SetMargins(5, 0, 5, true);



$html.='
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title><link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">';

if($status=="old"){
    //pr($student); die;
  $html.='<body  style="color:#000;
      font-family: Arial;
      font-size:10px;">
      
  <div style="width:100%;margin-bottom:5px;" >
    <div style="border:1px solid black;width:100%; ><br>
      <table border="0" cellspacing="0" cellpadding="0" style="width:100%;">
        <tr>
          <td>
          <table  border="0" cellspacing="0" cellpadding="0" style="width:100%;">
            <tr>
              <td valign="top" style="width:25%;"><img src="https://daac.in/images/dacc.jpg" height="70" /></td>
              
          <td style="padding-left:0px; text-align:center; padding-top:0px; width:50%;">
          
            <span style=" text-transform:uppercase; margin:0px;font-size:10px">fee receipt</span><br>
            
      <span style="text-transform:uppercase; margin:0px; text-align:left;font-size:11px;font-weight:bold">'.$student["branch_head"].'</span>
   <br><span style=" text-transform:uppercase; font-size:10px;margin:0px;">'.trim($student["address"]).'</span><br>Phone: '.$student["phone"].'
  
      </td>
          <td valign="top" style="width:25%;text-align:right"><img src="https://daac.in/images/icon1.jpg" width="70" height="70"/></td>
        </tr>
        
        <tr>
          <td style="padding-top:10px;text-align:left;width:350px" colspan="2">';
           if(!empty($student["gst_num"]) && $student["gst_num"]!='NULL')
        {
          
          $html  .='<br>
            <span style="padding-left:0px;font-size:10px;"><b>GST No : '.$student["gst_num"].'</b> <br/> &nbsp; Enrollment No : '.$student["student"]["s_enroll"].' <br/> &nbsp; Student Name : '.ucwords($student["student"]["s_name"]).'</span><br/>';
            
        } else {
           $html  .='<br><br>
            <span style="padding-left:0px;font-size:10px;">Enrollment No : '.$student["student"]["s_enroll"].' <br/> &nbsp; Student Name : '.ucwords($student["student"]["s_name"]).'</span><br/>'; 
            
        }
        
        $prose=date('F-Y',strtotime($student["pro_date"]));
            
        $html  .=' </td>
          <td></td>
          <td style="padding-top:10px;width:170px;"><br><br>
            <span style="font-size:10px;">
              Receipt No : '.$student["recipt_no"].'
              <br/>
              &nbsp;  &nbsp; Date : '.strftime('%d-%b-%Y',strtotime($student["pay_date"])).'</span><br/>
          </td>
        </tr>
          </table>  </td>
        </tr>
        
        <tr>
          <td>
          
          <table border="0" cellspacing="0" cellpadding="4" style=" width:635px;">
            
            <tr>
              <td align="center" style=" width:50px; padding:10px;border-right:1px solid black;border-bottom:1px solid black;border-top:1px solid black;border-left:1px solid black;"><span style="font-size:10px;" >S No.</span></td>
        <td align="center" style="border-right:1px solid black;border-top:1px solid black; padding:10px;border-bottom:1px solid black;width:400px"><span style="font-size:10px;" >Particular</span></td>
        <td align="center" style="width:78px;padding:10px;border-bottom:1px solid black;border-top:1px solid black;border-right:1px solid black; "><span style="font-size:10px;" >Amount</span></td>
      </tr>
      
      
            <tr>
              <td align="center" valign="top" height="100" style=" padding:10px; border-right:1px solid black;border-bottom:1px solid black;border-left:1px solid black;"><span style="font-size:10px;" >1</span>';
               if($student["fine"]!='0'){
        $html .='<br><br>2';
    }
              
              $html .='</td>
        <td  valign="top" style="padding:10px; border-right:1px solid black;border-bottom:1px solid black;"><span style="font-size:10px;">'.$student["course"]["cname"].' upto <b>('.$prose.')</b></span>';
        
        if($student["fine"]!='0'){
        $html .='<br><br>Fine';
    }
        if(!empty($student["gst_num"] && $student["gst_num"]!='NULL'))
        {
       
        if(($student["cgst_percent"]!='0' || $student["sgst_percent"]!='0') && ($student["igst_percent"]=='0') ) { 
    
        $html .='<br><br><p style="text-align:right;font-size:10px;">CGST('.$student["cgst_percent"].'%)</p><p style="text-align:right;font-size:10px;">SGST('.$student["sgst_percent"].'%)</p>';
    
          $html .='</td>';
          
          if($student["gst_inclu"]=='1') {     
              
                 $gtotal=$student["fine"]+$student["amount"]+$student["cgst_amount"]+$student["sgst_amount"];
        $html .='<td align="right"  valign="top" style="  padding:10px;border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >'.round($student["amount"]).'</span>';
        if($student["fine"]!='0'){
        $html  .='<br><br>'.$student["fine"].'';
    }
         $html  .='<br><br><p style="text-align:right;font-size:10px;">'.round($student["cgst_amount"]).'</p><p style="text-align:right;font-size:10px;">'.round($student["sgst_amount"]).'</p>
         </td></tr>
            <tr>
              <td style=" padding:10px; border-bottom:1px solid black;border-left:1px solid black;">&nbsp;</td>
        <td style=" padding:10px; border-bottom:1px solid black; border-right:1px solid black;  text-align:right;"><span style="font-size:11px;font-weight:bold" >Total:</span></td>
        <td align="right" style=" padding:10px; border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >'.round($gtotal).'</span></td>';
         
         
    } else { 
         
        $html .='<td align="right"  valign="top" style="  padding:10px;border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >'.round($student["amount"]).'</span>';
         if($student["fine"]!='0'){
        $html  .='<br><br>'.$student["fine"].'';
    }
     $gtotal=$student["fine"]+$student["amount"]+$student["cgst_amount"]+$student["sgst_amount"];
         $html  .='<br><br><p style="text-align:right;font-size:10px;">'.round($student["cgst_amount"]).'</p><p style="text-align:right;font-size:10px;">'.round($student["sgst_amount"]).'</p>';
         $html  .='</td></tr>
            <tr>
              <td style=" padding:10px; border-bottom:1px solid black;border-left:1px solid black;">&nbsp;</td>
        <td style=" padding:10px; border-bottom:1px solid black;  border-right:1px solid black; text-align:right;"><span style="font-size:11px;font-weight:bold" >Total:</span></td>
        <td align="right" style=" padding:10px; border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >'.round($gtotal).'</span></td>';
     }
           
  } 
  
   else if(($student["cgst_percent"]=='0' || $student["sgst_percent"]=='0') && ($student["igst_percent"]!='0') ) { 
    
        $html .='<br><br><p style="text-align:right;font-size:10px;">IGST('.$student["igst_percent"].'%)</p>';
    
          $html .='</td>';
  
          if($student["gst_inclu"]=='1') {     
              
               
        $html .='<td align="right"  valign="top" style="  padding:10px;border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >'.round($student["amount"]).'</span>';
         if($student["fine"]!='0'){
        $html  .='<br><br>'.$student["fine"].'';
    }
    $gtotal=$student["fine"]+$student["amount"]+$student["igst_amount"];
         $html  .='<br><br><p style="text-align:right;font-size:10px;">'.round($student["igst_amount"]).'</p>
         </td></tr>
            <tr>
              <td style=" padding:10px; border-bottom:1px solid black;border-left:1px solid black;">&nbsp;</td>
        <td style=" padding:10px; border-bottom:1px solid black; border-right:1px solid black;  text-align:right;"><span style="font-size:11px;font-weight:bold" >Total:</span></td>
        <td align="right" style=" padding:10px; border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >'.round($gtotal).'</span></td>';
         
         
    } else { 
         
        $html .='<td align="right"  valign="top" style="  padding:10px;border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >'.round($student["amount"]).'</span>';
         if($student["fine"]!='0'){
        $html  .='<br><br>'.$student["fine"].'';
    }
      $gtotal=$student["fine"]+$student["amount"]+$student["igst_amount"];
         $html  .='<br><br><p style="text-align:right;font-size:10px;">'.round($student["igst_amount"]).'</p>';
         $html  .='</td></tr>
            <tr>
              <td style=" padding:10px; border-bottom:1px solid black;border-left:1px solid black;">&nbsp;</td>
        <td style=" padding:10px; border-bottom:1px solid black; border-right:1px solid black;  text-align:right;"><span style="font-size:11px;font-weight:bold" >Total:</span></td>
        <td align="right" style=" padding:10px; border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >'.round($gtotal).'</span></td>';
     }
  
  }
  }
  else { 
      
      $html .='</td><td align="right"  valign="top" style="  padding:10px;border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >'.round($student["amount"]).'</span>';
       if($student["fine"]!='0'){
        $html  .='<br><br>'.$student["fine"].'';
    }
    $gtotal=$student["fine"]+$student["amount"];
      $html  .='</td></tr>
      
        
            <tr>
              <td style=" padding:10px; border-bottom:1px solid black;border-left:1px solid black;">&nbsp;</td>
        <td style=" padding:10px; border-bottom:1px solid black; border-right:1px solid black;   text-align:right;"><span style="font-size:11px;font-weight:bold" >Total:</span></td>
        <td align="right" style=" padding:10px; border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >'.$gtotal.'</span></td>';
      
      
  }
      
      $html  .='</tr>
            <tr>
              <td valign="bottom" height="10" style="border-bottom:1px solid black;border-left:1px solid black;" colspan="2"><br><br><br><br><span style="font-size:10px;" >Payment Mode:'.ucfirst($student["mode"]);
              

              $html  .='</span></td>
        <td valign="bottom" style=" padding:10px;border-bottom:1px solid black;border-right:1px solid black;"><br><br><br><br><span style="font-size:10px;">Signature</span></td>
      </tr>
    </table>  
    </td>
        </tr>
        </table>
    </div>
  </body>';
      }
      else{
        $html.='<body  style="color:#000;
      font-family: Arial;
      font-size:10px;">
  
  <div style="width:600px;margin-bottom:5px;" >
    <div style="border:1px solid black;width:600px; height:300px;"><br>
      <table border="0" cellspacing="0" cellpadding="0" style="width:400px;">
        <tr>
          <td>
          <table  border="0" cellspacing="0" cellpadding="0" style="width:400px;">
            <tr>
              <td valign="top" style="width:150px;"><img src="https://daac.in/images/dacc.jpg" height="70" /></td>
  
          <td style="padding-left:0px; text-align:center; padding-top:0px; width:300px;">
  
            <span style=" text-transform:uppercase; margin:0px;font-size:10px">fee receipt</span><br>
  
      <span style="text-transform:uppercase; margin:0px; text-align:left;font-size:11px;font-weight:bold">' . $student["branch_head"] . '</span>
   <br><span style=" text-transform:uppercase; font-size:10px;margin:0px;">' . trim($student['branch']["address"]) . '</span><br>Phone: ' . $student['branch']["phone"] . '
      </td>

     <td valign="top" style="width:70px;text-align:right"><img src="https://daac.in/images/icon1.jpg" width="70" height="70"/></td>
        </tr>
  
        <tr>
          <td style="padding-top:10px;text-align:left;width:350px" colspan="2">';
  if (!empty($new_receipt["gst_num"]) && $new_receipt["gst_num"] != 'NULL') {
  
      $html .= '<br>
            <span style="padding-left:0px;font-size:10px;"><b>GST No : ' . $new_receipt["gst_num"] . '</b> <br/> &nbsp; Enrollment No : ' . $student["student"]["s_enroll"] . ' <br/> &nbsp; Student Name : ' . $student["student"]["s_name"] . '</span><br/>';
  
  } else {
      $html .= '<br><br>
            <span style="padding-left:0px;font-size:10px;">Enrollment No : ' . $student["student"]["s_enroll"] . ' <br/> &nbsp; Student Name : ' . ucwords($student["student"]["s_name"] ). '</span><br/>';
  
  }
  
  //$prose = date('F-Y', strtotime($new_receipt["pro_date"]));
  
  $html .= ' </td>
          <td></td>
          <td style="padding-top:10px;width:170px;"><br><br>
            <span style="font-size:10px;">
              Receipt No : ' . $new_receipt["receipt_no"] . '
              <br/>
              &nbsp;  &nbsp; Date : ' . strftime('%d-%b-%Y', strtotime($new_receipt["pay_date"])) . '</span><br/>
          </td>
        </tr>
          </table>  </td>
        </tr>
  
        <tr>
          <td>
  
          <table border="0" cellspacing="0" cellpadding="4" style=" width:600px;">
  
            <tr>
              <td align="center" style=" width:50px; padding:10px;border-right:1px solid black;border-bottom:1px solid black;border-top:1px solid black;border-left:1px solid black;"><span style="font-size:10px;" >S No.</span></td>
        <td align="center" style="border-right:1px solid black;border-top:1px solid black; padding:10px;border-bottom:1px solid black;width:400px"><span style="font-size:10px;" >Particular</span></td>
        <td align="center" style="width:78px;padding:10px;border-bottom:1px solid black;border-top:1px solid black;border-right:1px solid black; "><span style="font-size:10px;" >Amount</span></td>
      </tr>
  
  
            <tr>
              <td align="center" valign="top" height="100" style=" padding:10px; border-right:1px solid black;border-bottom:1px solid black;border-left:1px solid black;"><span style="font-size:10px;" >1</span>';
  if ($new_receipt["fine"] != '0') {
      $html .= '<br><br>2';
  }
  
  $html .= '</td>
        <td  valign="top" style="padding:10px; border-right:1px solid black;border-bottom:1px solid black;"><span style="font-size:10px;">' . $student["course"]["cname"] . ' </span>';
  
  if ($new_receipt["fine"] != '0') {
      $html .= '<br><br>Fine';
  }
  if (!empty($new_receipt["gst_num"] && $new_receipt["gst_num"] != 'NULL')) {
  
      if (($new_receipt["cgst_percent"] != '0' || $new_receipt["sgst_percent"] != '0') && ($new_receipt["igst_percent"] == '0')) {
  
          $html .= '<br><br><p style="text-align:right;font-size:10px;">CGST(' . $new_receipt["cgst_percent"] . '%)</p><p style="text-align:right;font-size:10px;">SGST(' . $new_receipt["sgst_percent"] . '%)</p>';
  
          $html .= '</td>';
  
          if ($new_receipt["gst_inclu"] == '1') {
  
              $gtotal = $new_receipt["fine"] + $new_receipt["amount"] + $new_receipt["cgst_amount"] + $new_receipt["sgst_amount"];
              $html .= '<td align="right"  valign="top" style="  padding:10px;border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >' . round($new_receipt["amount"]) . '</span>';
              if ($new_receipt["fine"] != '0') {
                  $html .= '<br><br>' . $new_receipt["fine"] . '';
              }
              $html .= '<br><br><p style="text-align:right;font-size:10px;">' . round($new_receipt["cgst_amount"]) . '</p><p style="text-align:right;font-size:10px;">' . round($new_receipt["sgst_amount"]) . '</p>
         </td></tr>
            <tr>
              <td style=" padding:10px; border-bottom:1px solid black;border-left:1px solid black;">&nbsp;</td>
        <td style=" padding:10px; border-bottom:1px solid black; border-right:1px solid black;  text-align:right;"><span style="font-size:11px;font-weight:bold" >Total:</span></td>
        <td align="right" style=" padding:10px; border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >' . round($gtotal) . '</span></td>';
  
          } else {
  
              $html .= '<td align="right"  valign="top" style="  padding:10px;border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >' . round($new_receipt["amount"]) . '</span>';
              if ($new_receipt["fine"] != '0') {
                  $html .= '<br><br>' .$new_receipt["fine"] . '';
              }
              $gtotal = $new_receipt["fine"] + $new_receipt["amount"] + $new_receipt["cgst_amount"] + $new_receipt["sgst_amount"];
              $html .= '<br><br><p style="text-align:right;font-size:10px;">' . round($new_receipt["cgst_amount"]) . '</p><p style="text-align:right;font-size:10px;">' . round($new_receipt["sgst_amount"]) . '</p>';
              $html .= '</td></tr>
            <tr>
              <td style=" padding:10px; border-bottom:1px solid black;border-left:1px solid black;">&nbsp;</td>
        <td style=" padding:10px; border-bottom:1px solid black;  border-right:1px solid black; text-align:right;"><span style="font-size:11px;font-weight:bold" >Total:</span></td>
        <td align="right" style=" padding:10px; border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >' . round($gtotal) . '</span></td>';
          }
  
      } else if (($new_receipt["cgst_percent"] == '0' || $new_receipt["sgst_percent"] == '0') && ($new_receipt["igst_percent"] != '0')) {
  
          $html .= '<br><br><p style="text-align:right;font-size:10px;">IGST(' . $new_receipt["igst_percent"] . '%)</p>';
  
          $html .= '</td>';
  
          if ($new_receipt["gst_inclu"] == '1') {
  
              $html .= '<td align="right"  valign="top" style="  padding:10px;border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >' . round($new_receipt["amount"]) . '</span>';
              if ($new_receipt["fine"] != '0') {
                  $html .= '<br><br>' . $new_receipt["fine"] . '';
              }
              $gtotal = $new_receipt["fine"] + $new_receipt["amount"] + $new_receipt["igst_amount"];
              $html .= '<br><br><p style="text-align:right;font-size:10px;">' . round($new_receipt["igst_amount"]) . '</p>
         </td></tr>
            <tr>
              <td style=" padding:10px; border-bottom:1px solid black;border-left:1px solid black;">&nbsp;</td>
        <td style=" padding:10px; border-bottom:1px solid black; border-right:1px solid black;  text-align:right;"><span style="font-size:11px;font-weight:bold" >Total:</span></td>
        <td align="right" style=" padding:10px; border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >' . round($gtotal) . '</span></td>';
  
          } else {
  
              $html .= '<td align="right"  valign="top" style="  padding:10px;border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >' . round($new_receipt["amount"]) . '</span>';
              if ($new_receipt["fine"] != '0') {
                  $html .= '<br><br>' . $new_receipt["fine"] . '';
              }
              $gtotal = $new_receipt["fine"] + $new_receipt["amount"] + $new_receipt["igst_amount"];
              $html .= '<br><br><p style="text-align:right;font-size:10px;">' . round($new_receipt["igst_amount"]) . '</p>';
              $html .= '</td></tr>
            <tr>
              <td style=" padding:10px; border-bottom:1px solid black;border-left:1px solid black;">&nbsp;</td>
        <td style=" padding:10px; border-bottom:1px solid black; border-right:1px solid black;  text-align:right;"><span style="font-size:11px;font-weight:bold" >Total:</span></td>
        <td align="right" style=" padding:10px; border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >' . round($gtotal) . '</span></td>';
          }
  
      }
  } else {
  
      $html .= '</td><td align="right"  valign="top" style="  padding:10px;border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >' . round($new_receipt["amount"]) . '</span>';
      if ($new_receipt["fine"] != '0') {
          $html .= '<br><br>' . $new_receipt["fine"] . '';
      }
      $gtotal = $new_receipt["fine"] + $new_receipt["amount"];
      $html .= '</td></tr>
  
  
            <tr>
              <td style=" padding:10px; border-bottom:1px solid black;border-left:1px solid black;">&nbsp;</td>
        <td style=" padding:10px; border-bottom:1px solid black; border-right:1px solid black;   text-align:right;"><span style="font-size:11px;font-weight:bold" >Total:</span></td>
        <td align="right" style=" padding:10px; border-bottom:1px solid black;border-right:1px solid black;"><span style="font-size:10px;" >' . $gtotal . '</span></td>';
  
  }
  
  $html .= '</tr>
            <tr>
              <td valign="bottom" height="10" style="border-bottom:1px solid black;border-left:1px solid black;" colspan="2"><br><br><br><br><span style="font-size:10px;" >Payment Mode:'.ucfirst($new_receipt["mode"]);
  
  // if ($new_receipt['Cash']["mode"] == 'Other') {
  //     $html .= 'Cheque';
  // } else {
  //     $html .= 'Cash';
  // }
  
  $html .= '</span></td>
  
        <td valign="bottom" style=" padding:10px;border-bottom:1px solid black;border-right:1px solid black;"><br><br><br><br><span style="font-size:10px;">Signature</span></td>
      </tr>
    </table>
  
    </td>
        </tr>
        </table>
  
    </div>
  
  </body>';
  
      }

      
$pdf->writeHTMLCell(0, 0, '', '', utf8_encode($html), 0, 1, 0, true, '', true);
ob_end_clean();
echo $pdf->Output('orderlist.pdf');
exit;
?>
  