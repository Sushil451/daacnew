<?php echo $this->Form->create('Student',array('type'=>'file','inputDefaults'=>array('div'=>false,'label'=>false),'class'=>'stdform addStdntsFrm needs-validation')); ?> 
        <div class="feespopup" id='inline_contentinline_contentdrop<?php echo $cid; ?>' style=' background:#fff;'> 
  <div class="fplan">  

  <div class="row">
			<div class="col-md-12">
			

				<div class="box box-info boxInfoBx boxInfoBxStudentInfo">
  <div class="box-body addStdntsFrm">
                    <div class="row d-flex">
<div class="col-sm-4 align-self-center" ><?php 
$path=LOCALWEB.$stu_det['image'];
//echo $path; die;
if(!empty($stu_det['image']) && file_exists($path)){ ?>
<img src="<?php echo SITE_URL;?>images/student/<?php echo $stu_det['image']?>" alt="img" style="width:100px;height:100px"> <?php } else{ ?><img src="https://daac.in/images/student/noimage.jpg" alt="img" style="width:100px;height:100px"><?php } ?></div>
<!--  -->

               <div class="col-sm-8 align-self-center">
               <h3><?php echo ucwords($stu_det['s_name'])?></h3>
               <table style="width:100%">
               <tbody><tr style="width:100%"><td style="text-align:left; width:30%; padding-right:2%"><b>Enroll. No</b></td><td style="text-align:left ;"><b>:</b> <?php echo $stu_det['s_enroll']?></td></tr>
                           <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Enrollment Date</b></td><td style="text-align:left ;"><b>:</b>  <?php echo date('d-m-Y',strtotime($stu_det['add_date'])); ?></td></tr>

                           <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Request Date</b></td><td style="text-align:left ;"><b>:</b>  <?php echo date('d-m-Y',strtotime($discount['created'])); ?></td></tr>

                           <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Branch</b></td><td style="text-align:left ;"><b>:</b>  <?php echo $discount['branch']['name']; ?></td></tr>

                           <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Discount</b></td><td style="text-align:left ;"><b>:</b>  <?php echo $discount['discount']; ?></td></tr>

                           <tr style="width:100%"><td style="text-align:left ;width:30%; padding-right:2%"><b>Discount Type</b></td><td style="text-align:left ;"><b>:</b>  <?php echo ($discount['dtype']==1)?"Fixed":"Percentage"; ?></td></tr>
               
               </tbody></table>            
               </div>
              
               </div>
               
                </div></div>
              
              </div>
              
               </div>
<table class="table table-bordered table-striped afTblStndRept" width="100%">
               
               <thead class="thead-dark">
	<tr>

		<th style="text-align:center; width:10px;"><strong>
      S.No.
    </strong></th>
    <th style="text-align:center; "><strong>
    Enrollment
    </strong></th>
    <th style="text-align:center; "><strong>
    Enrollment Date
    </strong></th>
    <th style="text-align:center; "><strong>
    Student Name
    </strong></th>
    <th style="text-align:center; "><strong>
    Fee Plan
    </strong></th>
    <th style="text-align:center; "><strong>
    Course
    </strong></th>
   
    </tr>
    </thead>
    <tbody> <?php    $rt=0;  ?>
                        <?php $s=explode(",",$discount['enrollment']); //pr($s); die;
                        
                        $count=1;
                        
                        $bid=$discount['branch']['id']; //pr($bid); die;
                        $corid=$discount['course_id'];
                     
                        foreach($s as $keys=>$enroll ){ //pr($values); die; //  pr($enroll);die;
                         $res=$this->Comman->usr_dtl($enroll); 
                         $fee_plan_status=0;
                         $course_fee=0;
                    foreach($res['student_course'] as $st_course){
						if($st_course['course_id']==$discount['course_id']){
							
							if($st_course['status']==1){
								$fee_plan_status=1;
								$course_fee=$st_course['fee'];
								break;
							}
						}
					}
						 $sids=$res['s_id'];
							
                    //  $c=$this->Common->std_newcorid($sids,$corid,$bid); 
                  $c=$this->Comman->crs($corid);  
                     //  pr($c); 
                     //
                      if(!empty($res))  { 
						  
						  ?>
                   
           <tr class="gradeX">
                
                <td> <?php echo $count; $count++; ?></td>
                <td> <?php echo $res['s_enroll']; ?></td>
                <td> <?php echo date('d-m-Y',strtotime($res['add_date'])); ?></td>
                <td> <?php echo $res['s_name']; ?></td>
                <td> <?php if($fee_plan_status=='1'){ echo "Started";   }else{ $rt++;  ?>
                <span colspan=5 style=color:red><?php echo "Doesn't Started"; ?></span> <?php  } ?></td>
                <td> <?php echo $c['sname']; ?></td>
                
           </tr>
              <?php } else { 
				  $rt++;
				  ?>     
            <tr class="gradeX">
                
                <td colspan=6 style=color:red><?php echo $enroll; ?> is not found !!</td>
               
                
           </tr>
                
			 
			 <?php } } ?>
    



    <td colspan="6" style="text-align:center; width:145px;">

<?php echo $this->Form->textarea('comment',array('class' => 'longinput','required','style' => 'width:100%','required','placeholder'=>'Enter Comment Here....')); ?>
<?php echo $this->Form->input('discount',array('type'=>'hidden','value'=>$discount['discount']));?> 

<?php echo $this->Form->input('course_id',array('type'=>'hidden','value'=>$discount['course_id']));?>
                     <?php echo $this->Form->input('enrollment',array('type'=>'hidden','value'=>$discount['enrollment']));?>
                      <?php echo $this->Form->input('dtype',array('type'=>'hidden','value'=>$discount['dtype']));?>
                       <?php echo $this->Form->input('fee',array('type'=>'hidden','value'=>$course_fee));?>
                        <?php echo $this->Form->input('course_name',array('type'=>'hidden','value'=>$c['sname']));?>

<?php echo $this->Form->input('id',array('type'=>'hidden','value'=>$discount['id']));?>
               
    </td>
    
    </tr>
  	
    
</tbody>
</table>
  
</div>
</div>
<div class="modal-footer box footer">

<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

<?php echo $this->Form->submit('Reject',array('name'=>'submit','class' => 'btn btn-danger','value'=>'Reject')); ?>
<?php echo $this->Form->submit('Approve',array('name'=>'submit','class' => 'btn btn-success','value'=>'Approve')); ?>
    
      </div>
      <?php echo $this->Form->end();?> 
<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
    
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>