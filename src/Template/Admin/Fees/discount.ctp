<style type="text/css">

.discountFePg .dataTables_filter > label input {
  display: block;
  width: 100%;
  padding: 0.375rem 0.75rem;
  font-size: 1rem;
  font-weight: 400;
  line-height: 1.5;
  color: #212529;
  background-color: #fff;
  background-clip: padding-box;
  border: 1px solid #ced4da;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  border-radius: 0.25rem;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  height: 35px;
}

   th{
text-align: center;
   }
   td{
    text-align: center;

   }


   .action_icon a span {
    font-size: 14px !important;
}
 .action_icon a {
    font-size: 13px !important;
}
.action_icon b i {
    font-size: 13px !important;
}

.action_icon a {
    display: inline-block;
    width: 21px;
    text-align: center;
}
 </style>
 <div class="content-wrapper">
   <section class="content-header">
    <h1>
         Fee Discount Manager
    </h1>
    <ol class="breadcrumb">
    <li><a href="<?php echo SITE_URL; ?>admin/dashboards"><i class="fa fa-home"></i>Home</a></li>
<li><a href="<?php echo SITE_URL; ?>admin/fees/discount">Fee Discount Manager</a></li>
    </ol> 
    </section> <!-- content header -->

    <!-- Main content -->
    <section class="content">
  <div class="row">
    <div class="col-xs-12">    
  <div class="box">
    <div class="box-header">
  <?php echo $this->Flash->render(); ?>


  <?php $role_id=$this->request->session()->read('Auth.User.role_id'); ?>
            <style>

.discountFePg .dataTables_filter {
  text-align: left;
}
.discountFePg .dataTables_filter > label {
  position: relative;
  z-index: 99;
 
}
            </style>
  <div class="form-group studentDtlFrmGroup d-flex justify-content-end discountPgAddDis" >
            
            <div class="row ">
           <div class="col-md-10">        
</div></div>
<?php if($this->request->session()->read('Auth.User.role_id') != 1){ ?>  
              <a  data-bs-toggle="modal" data-bs-target="#exampleModal5" style="margin-bottom:-70px;"  class="addnn"><i class="fa fa-user-plus" aria-hidden="true"></i>
     <br> Add Disount</a> 
           
<?php } ?>
<!-- Modal -->
<div class="modal fade" id="exampleModal5" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
<div class="modal-header" style="background-color: #3c8dbc;color: #fff;">
        <h5 class="modal-title" id="exampleModalLabel">Discount Request</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
<?php echo $this->Form->create('Student',array('type'=>'file','inputDefaults'=>array('div'=>false,'label'=>false),'class'=>' dropupStudPop  addStdntsFrm needs-validation')); ?> 
<div class="feespopup" id='inline_contentinline_contentdrop<?php echo $cid; ?>' style=' background:#fff;'> 
  <div class="fplan">  

   <div class="row">
			<div class="col-md-12">
			

				<div class="box box-info boxInfoBx boxInfoBxStudentInfo">
  <div class="box-body addStdntsFrm">
                    <div class="row d-flex">
<div class="col-sm-12 align-self-center typeRedioDv" >
<label class="d-block">Type :</label>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="dtype" id="inlineRadio1" value="1">
  <label class="form-check-label" for="inlineRadio1">Fixed</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="dtype" id="inlineRadio2" value="2">
  <label class="form-check-label" for="inlineRadio2">Percentage</label>
</div>
     
  </div>
<!--  -->

               <div class="col-sm-12 align-self-center">
               <strong> Enter Enrollment No :</strong>
               <?php 
                echo $this->Form->input('enrollment', array('class' => 'form-control','type'=>'text','id'=>'enrl','required','placeholder'=>'Enter Enroll','maxlength'=>6,'label'=>false)); 
               ?>
               </div>

	<script>          
              $(document).ready(function () {
              $("#enrl").bind("change", function (event) {
              var instid= $(this).val();
			  $('.lds-facebook').show();   
                  $.ajax({
                    async:true,
                    data:{'enrl':instid},
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>students/findcrsname",
                    success:function (data) {
                      $('.lds-facebook').hide();   
					$("#crk").html('');
                      $("#crk").html(data); },
                    });
                  return false;
                });
              });
              </script>
               
               </div>
               <?php  $branch=$this->request->session()->read('Auth.User.branch'); ?>
               <input type="hidden" name="branch_id" value="<?php echo $branch; ?>">
               
              
             
               <div class="row d-flex" id="crk">
<div class="col-sm-12 align-self-center" >
<strong>Discount :</strong>
<?php echo $this->Form->input('discount', array('label'=>false,'class' => 'form-control','onkeypress' => 'return isNumber(event);','required')); ?>

  </div>
<!--  -->
<div class="modal-footer box footer">
        <!-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> -->
        <?php echo $this->Form->submit('Submit',array('class' => 'btn btn-success')); ?>
      </div>
               
               
               </div>
              
              </div>
             
             </div>
             
              </div>
  
</div></div>
</div>

      <?php echo $this->Form->end();?> 
<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
    
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
<script>
function isNumber(evt) {

evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;
if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {

return false;
}
return true;
}

</script>
      </div>
    
    </div>
  </div>
</div>

  <style>
        .addnn , .addnn:hover{ height:55px; width: 55px; border-radius: 50%; background-color: #2d95e3; line-height: 15px; font-size: 14px !important; text-align: center; color: #fff; display: inline-block; text-decoration: none;}
        .addnn i{ font-size:18px; color:#fff; margin-top:10px;}
        .studentDtlFrmGroup{ display:flex; flex-wrap:wrap;}
        .studentDtlFrmGroup>.col-md-10, .studentDtlFrmGroup>.col-md-2{ align-self:center !important;}
    </style>

            
              

   <?php /*          
              <div class="col-sm-1">
                <label for="inputEmail3" class="control-label" style="color:white">Reset</label>
                <input type="reset" style="background-color:#d33c44;color:#fff;"  class="btn btn-success" value="Reset">       
              </div> 
              <div class="col-sm-1" style="margin-left: -40px;">
                <label for="inputEmail3" class="control-label" style="color:white">Search</label>
                <input type="submit" style="background-color:green;color:#fff;" id="Mysubscriptions" class="btn btn-success" value="Search">       
              </div> */ ?>
              </div>  
              
            
      
       

   
     </div><!-- /.box-header -->
      <div class="box-body discountFePg" id="Mycity"> 
      <table  class="table table-bordered table-striped afTblStndRept" id="example3" width="100%">
                <thead class="thead-dark">
                    <tr>
                            <th>No.</th>
                            <th>Request Date</th>
                     <?php $user=$this->request->session()->read('Auth.User.role_id'); ?>
                            <?php if($user ==1){  ?>
                            <th>Branch</th> 
                            <?php } ?>
                            <th>Enrollment</th>
                            <th>Discount</th>
                            <th>Course</th>
                            <th>Type</th>
                            <th>Status</th>

                       
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                $counter=($this->request->params['paging']['Discount']['page']-1) * $this->request->params['paging']['Discount']['perPage']; 
                if(isset($rec) && !empty($rec)){ 
                foreach($rec as $value){ ?>
                  <tr>
                    <td><?php echo $counter+1;?></td>
                   
                           <td><?php echo date('d-m-Y',strtotime($value['created']));  ?></td>
 
                          <?php if($user ==1){  ?>
                          <td><?php echo $value['branch']['name']; ?></td>
                          <?php } ?>
                       <td><?php //echo $value['Discount']['enrollment']; ?><br>
                      
                       <?php
                       $en_array=explode(',',$value['enrollment']);
                       
                       
                        $rt= $this->Comman->stname($value['enrollment']);  ?>
                       <?php  foreach($en_array as $en){ //pr($v); die; ?>
                        <?php 
                        $rt= $this->Comman->stname($en);
                       // $rt['enrollment']; ?>
                      <?php echo $rt['s_name']." (".$en."),"; ?>
				            <?php   } ?>
                       </td>
                       
                        <td><?php echo $value['discount'];?> 
                         
                         <?php if($value['dtype']==1){ 
						
							 }
                         else{
							  echo "%";
							 } ?></td>
							  <td><?php $c=$this->Comman->crs($value['course_id']);
                               echo $c['sname']; ?></td>
                         <td>  
                         <?php if($value['dtype']==1){ 
							 echo "Fixed";
							 }
                         else{
							  echo "Percentage";
							 } ?></td>
                      
            <td><?php if($value['status']=='A')
            { ?>
				<a class="approvv"  data-bs-toggle="modal" data-bs-target="#exampleModal6"  href="<?php echo ADMIN_URL;?>fees/approvdiscount/<?php echo $value['id']; ?>/<?php echo $value['enrollment']; ?>/<?php echo $value['course_id']; ?>"> <p style="color:green">Approved</p></a>
				
		
				<div id="appr"> <?php echo $value['comment']; ?> </div>
				<?php }
				elseif($value['status']=='R')
				
				{
					
					echo "<p style=color:red>Rejected</p>"; ?>
					<?php echo $value['comment']; ?>
					  <?php }
					   
			    else
                   {
					   if($user==1){ ?>
<a  class='transferpopupss' data-bs-toggle="modal" data-bs-target="#exampleModal12" href="<?php echo SITE_URL; ?>admin/fees/pendiscount/<?php echo $value['id']; ?>/<?php echo $value['enrollment']; ?>/<?php echo $value['course_id']; ?>" style="color:blue" title="">Pending for Approval </a>


					<?php   } else{
						   
						   echo "<p style=color:blue>Pending for Approval</p>";
						   }
					   
					   
					   } ?>
              
            
       
            
            
            
			
                   
                
	    
	 
                    
				
<?php if($value['status']!='A'){


echo $this->Html->link('', ['action' => 'discountdelete',$value->id],['title'=>'Delete','class'=> 'fa fa-trash','style'=>'color:#FF0000; margin-left: 13px; font-size: 19px !important;',"onClick"=>"javascript: return confirm('Are you sure do you want to delete this Discount ?')"]); 
  } ?>
							
                 </td>  
                 </tr>
                 <?php $counter++;} } ?>  





                       
                    </tbody>
                </table>
                <?php  //if(count($destination)>10){ //pr($events);?>
                    <?php echo $this->element('admin/pagination'); ?> 
            </div>
       
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
       </div>
      <!-- /.col -->  
    </div>
     <!-- /.row -->      
  </section>
    <!-- /.content -->  
       
 </div>     
     <!-- /.   content-wrapper -->  
     
     

      <script>
  $(document).on('click','.transferpopupss',function (e){
  e.preventDefault();
  $('#exampleModal12').modal('show').find('.modal-body').load($(this).attr('href'));
});

</script>

<script>
  $(document).on('click','.approvv',function (e){
  e.preventDefault();
  $('#exampleModal6').modal('show').find('.modal-body').load($(this).attr('href'));
});

</script>


<!-- Modal -->
<div class="modal fade" id="exampleModal12" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
<div class="modal-header" style="background-color: #3c8dbc;color: #fff;">
        <h5 class="modal-title" id="exampleModalLabel">Pending For Approval Discount</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       Please wait while we fetching data result....
      </div>
    
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModal6" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
<div class="modal-header" style="background-color: #3c8dbc;color: #fff;">
        <h5 class="modal-title" id="exampleModalLabel">Approved Student</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       Please wait while we fetching data result....
      </div>
    
    </div>
  </div>
</div>
