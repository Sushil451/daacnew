<style type="text/css">
   th{
text-align: center;
   }
   td{
    text-align: center;

   }


   .action_icon a span {
    font-size: 14px !important;
}
 .action_icon a {
    font-size: 13px !important;
}
.action_icon b i {
    font-size: 13px !important;
}

.action_icon a {
    display: inline-block;
    width: 21px;
    text-align: center;
}
 </style>
 <div class="content-wrapper">
   <section class="content-header">
    <h1>
    Payment Receipt List
    </h1>
    <ol class="breadcrumb">
    <li><a href="<?php echo SITE_URL; ?>admin/dashboards"><i class="fa fa-home"></i>Home</a></li>
<li><a href="<?php echo SITE_URL; ?>admin/fees/submit">Payment Receipt List</a></li>
    </ol> 
    </section> <!-- content header -->

    <!-- Main content -->
    <section class="content">
  <div class="row">
    <div class="col-xs-12">    
  <div class="box">
    <div class="box-header">
  <?php echo $this->Flash->render(); ?>


  <?php $role_id=$this->request->session()->read('Auth.User.role_id'); ?>
            
            
            
<?php if($this->request->session()->read('Auth.User.role_id') != 1){ ?>  
    <a  data-bs-toggle="modal" data-bs-target="#exampleModal5"  class="addnn"><i class="fa fa-user-plus" aria-hidden="true"></i>
      Add Payment</a> 
           
<?php } ?>
<!-- Modal -->
<div class="modal fade" id="exampleModal5" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
<div class="modal-header" style="background-color: #3c8dbc;color: #fff;">
        <h5 class="modal-title" id="exampleModalLabel">(H.O.) Payment Receipt</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
<?php echo $this->Form->create('Student',array('type'=>'file','inputDefaults'=>array('div'=>false,'label'=>false),'class'=>' dropupStudPop  addStdntsFrm needs-validation')); ?> 
<div class="feespopup" id='inline_contentinline_contentdrop<?php echo $cid; ?>' style=' background:#fff;'> 
  <div class="fplan">  

   <div class="row">
			<div class="col-md-12">


      <table class="table table-bordered table-striped afTblStndRept" width="100%">
              <tbody>
              <tr>
              
                  <div class="row mb-3 enq_form">
           
           
               
           
                          <div class="col-sm-12 align-self-center" >
                          <label>Date</label>
           <div class="input-append date" id="datepickerenquiry" data-date="<?php echo date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy"> 
             <input class="span2 form-control" size="16" placeholder="Next FollowUp Date" type="date" name="date" required >  <span class="add-on"><i class="icon-th"></i></span>
                     
                     </div>
                     <div class="row mb-3 enq_form">
                     <div class="col-sm-6 align-self-center enq_form">
               <label>Amount</label>
               <?php echo $this->Form->input('amount', array('class' => 'form-control', 'placeholder'=>'Enter Amount','label'=>false,'required','onkeypress'=>'return isNumber(event);')); ?>
                          </div>
           
           
             
                <div class="col-sm-6 align-self-center ">
                <label>Mode</label>
                 <?php $fee_op=array('cash'=>'Cash','bank'=>'Bank','paytm'=>'Paytm','cheque'=>'Cheque','other'=>'Other');
                 echo $this->Form->input('mode',array('type'=>'select','empty'=>'Select Mode','class' => 'form-control','label'=>false,'options'=>$fee_op)); ?>
                  </div> </div>
                  <div class="row enq_form">
                  <div class="col-sm-12 align-self-center ">
                   <label>Comments</label>
               <?php echo $this->Form->textarea('comment', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Comment','style'=>'height:100px;')); ?>
                         </div>
                         </div>
                        
           
                        
                          
              
               </tr>
               
               
               
               
           </tbody>
           </table>


<!--  -->
<div class="modal-footer box footer">
        <!-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> -->
        <?php echo $this->Form->submit('Submit',array('class' => 'btn btn-success')); ?>
      </div>
               
               
               </div>
              
              </div>
             
             </div>
             
              </div>
  
</div>
</div>

      <?php echo $this->Form->end();?> 
<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
    
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
<script>
function isNumber(evt) {

evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;
if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {

return false;
}
return true;
}

</script>
      </div>
    

  <style>
        .addnn , .addnn:hover{ height:55px; width: 55px; border-radius: 50%; background-color: #2d95e3; line-height: 15px; font-size: 14px !important; text-align: center; color: #fff; display: inline-block; text-decoration: none;}
        .addnn i{ font-size:18px; color:#fff; margin-top:10px;}
        .studentDtlFrmGroup{ display:flex; flex-wrap:wrap;}
        .studentDtlFrmGroup>.col-md-10, .studentDtlFrmGroup>.col-md-2{ align-self:center !important;}
    </style>

            
              

   <?php /*          
              <div class="col-sm-1">
                <label for="inputEmail3" class="control-label" style="color:white">Reset</label>
                <input type="reset" style="background-color:#d33c44;color:#fff;"  class="btn btn-success" value="Reset">       
              </div> 
              <div class="col-sm-1" style="margin-left: -40px;">
                <label for="inputEmail3" class="control-label" style="color:white">Search</label>
                <input type="submit" style="background-color:green;color:#fff;" id="Mysubscriptions" class="btn btn-success" value="Search">       
              </div> */ ?>
              </div>  
              
            
      
       

   
     </div><!-- /.box-header -->
      <div class="box-body discountFePg" id="Mycity"> 
      <table  class="table table-bordered table-striped afTblStndRept"  width="100%">
                <thead class="thead-dark">
                    <tr>
                            <th>No.</th>
                          
                     <?php $user=$this->request->session()->read('Auth.User.role_id'); ?>
                            <?php if($user ==1){  ?>
                            <th>Branch</th> 
                            <?php } ?>
                            <th>Date</th>
                            <th>Mode</th>
                            <th>Amount</th>
                            <th>Comments</th>
                            <th>Status</th>
                            <th>Action</th>

                       
                        </tr>

                        <thead>
                       
                    </thead>
                   
                    <tbody>
                    <?php
                    //print_r($cashes); die;
                    if(count($cashes)>0){ $sum="";?>
                    <?php $cnt=1; foreach($cashes as $key=>$value) {
            $sum+=$value['amount'];
             ?>
                        <tr class="gradeX">

                            <td> <?php echo $cnt; $cnt++;?></td>
                            <?php if($user ==1){  ?>
                             <td>  <?php echo $value['branch']['name']; ?></td>
                            <?php } ?>
                            <td><?php echo strftime('%d-%b-%Y',strtotime($value['date'])); ?></td>
                            <td style="text-align:left"><?php echo ucfirst($value['mode']); ?></td>
                            <td style="text-align:left"><?php echo $value['amount']; ?></td>
                            <td style="text-align:left;background-color:#fff"><?php echo $value['comment']; ?></td>
                            <td style="text-align:left;background-color:#fff"><?php if($value['status']!='A'){ ?><span style="color:red">Pending</span><?php } else{ ?><span style="color:green">Approved</span><?php } ?>
                            </td>
                            <td style="text-align:left;background-color:#fff">
                            <?php if($user ==1){   if($value['status']!='A'){ ?>  
                                <a href="<?php echo SITE_URL; ?>admin/fees/statuschange/<?php echo $value['id']; ?>/A" class="status-change"  data-bs-toggle="modal" data-bs-target="#exampleModal" title="Approve" ><img src="<?php echo  SITE_URL;?>admin_images/inactive.png" style="width:18px"></a>
                               <!-- <a href="<?php echo ADMIN_URL;?>Fee/submitstatus/<?php echo $value['id']; ?>/A" title="Approve"><img src="<?php echo  SITE_URL;?>admin_images/inactive.png"  style="width:18px"></a> --> 
                            <?php } else { ?>
                              <a href="<?php echo SITE_URL; ?>admin/fees/statuschange/<?php echo $value['id']; ?>/P" class="status-change"  data-bs-toggle="modal" data-bs-target="#exampleModal"  title="Dispprove" data-id="<?php echo $value['id']; ?>" data-status="P"><img src="<?php echo  SITE_URL;?>admin_images/active.png" style="width:18px"> </a>
                            <?php } ?>

<a href="<?php echo ADMIN_URL;?>Fees/deletesubmit/<?php echo base64_encode($value['id']); ?>" title="Delete" class="fa fa-trash" style="color:#FF0000; margin-left: 13px; font-size: 19px !important;" onclick="javascript: return confirm('Are you sure do you want to delete this Payment')"></a>
                                                         
<?php }else{
  
  if($value['status']!='A'){ ?>
<a href="<?php echo ADMIN_URL;?>Fees/deletesubmit/<?php echo base64_encode($value['id']); ?>" title="Delete" class="fa fa-trash" style="color:#FF0000; margin-left: 13px; font-size: 19px !important;" onclick="javascript: return confirm('Are you sure do you want to delete this Payment')"></a>
  <?php }  
                            }?>
                          <?php   ?>
                            </td>
                             
                        </tr>
                    <?php  } ?>
                   
                    <?php } else{  ?>    
                    <tr><td colspan="7" align="center">No Meta Available</td></tr>
                    <?php } ?>
                    </tbody>
                </table>
                
                 <?php echo $this->element('admin/pagination'); ?>
                
            </div>
       
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
       </div>
      <!-- /.col -->  
    </div>
     <!-- /.row -->      
  </section>
    <!-- /.content -->  
       
 </div>     
     <!-- /.   content-wrapper -->  
     
     
     <script>
  $(document).on('click','.status-change',function (e){
  e.preventDefault();
  $('#exampleModal').modal('show').find('.modal-body').load($(this).attr('href'));
});

</script>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
<div class="modal-header" style="background-color: #3c8dbc;color: #fff;">
        <h5 class="modal-title" id="exampleModalLabel">Payment</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       Please wait while we fetching data result....
      </div>
    
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModal6" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
<div class="modal-header" style="background-color: #3c8dbc;color: #fff;">
        <h5 class="modal-title" id="exampleModalLabel">OTP Verification</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       Please wait while we fetching data result....
      </div>
    
    </div>
  </div>
</div>
