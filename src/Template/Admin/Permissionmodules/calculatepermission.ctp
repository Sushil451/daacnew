 <script>

    $(".parenth").each(function(index){
        var group = $(this).data("group");
        var parent = $(this);

    parent.change(function(){  //"select all" change 
     $(group).prop('checked', parent.prop("checked"));
 });
    $(group).change(function(){ 
       // parent.prop('checked', false);
       parent.prop('checked', true);
       if ($(group+':checked').length == 0){
        parent.prop('checked', false);
    }
});
});

</script>
<div class="form-group">

 <div class="col-sm-6">
    <div class="title-div">
        <table width="80%">
            <tbody><tr>
                <td width="30%"><strong style="font-size: 18px;">Master Managers:</strong></td>
            </tr>
            <tr>
          <td width="30%"><strong>&nbsp;</strong></td>
        </tr>
            <tr style="background:#4993d7;">
                <td width="30%">
                 <input type="checkbox" name="module1[]" class="parenth" data-group=".group1" value="Category Manager" id="2_arrgruop" <?php if(in_array("Category Manager",$module)){ ?> checked="checked" <?php } ?>>
                 <input type="hidden" name="module[]" value="Category Manager"><strong style="color:#fff;"> Category Manager</strong></td> 
                 <td width="10%"></td>
             </tr>

             <tr><td width="30%"><input type="checkbox" name="menu1[]" class="group1" value="Add Category^category^add" id="2_arrgruop" <?php if(in_array("Add Category",$menu)){ ?> checked="checked" <?php } ?> >  Add Category</td></tr>

             <tr><td width="10%"><input type="checkbox" class="group1" name="menu1[]" id="2_V" value="Manage Category^category^edit" <?php if(in_array("Manage Category",$menu)){ ?> checked="checked" <?php } ?>>  Manage Category</td></tr>  

             <tr><td width="10%"><input type="checkbox" class="group1" name="menu1[]" id="2_V" value="View All Category^category^index" <?php if(in_array("View All Category",$menu)){ ?> checked="checked" <?php } ?>>  View All Category</td></tr>          
         </tbody></table>
     </div>
 </div>
 <div class="col-sm-6">
    <div class="title-div">
        <table width="80%">
            <tbody>
            <br>
          <tr>
            <td width="30%">&nbsp;</td>
          </tr>
            <tr style="background:#4993d7;">
                <td width="30%"><input type="checkbox" name="module2[]" <?php if(in_array("Attribute Manager",$module)){ ?> checked="checked" <?php } ?> class="parenth" data-group=".group2" value="Attribute Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Attribute Manager"><strong style="color:#fff;"> Attribute Manager</strong></td>
                <td width="10%"></td>
            </tr>

            <tr>	<td width="30%"><input type="checkbox" name="menu2[]" class="group2" value="Add Attribute^attribute^add" id="2_arrgruop" <?php if(in_array("Add Attribute",$menu)){ ?> checked="checked" <?php } ?>> Add Attribute</td></tr>
            
            <tr><td width="10%"><input type="checkbox" class="group2" name="menu2[]" id="2_V" value="Manage Attribute^attribute^edit" <?php if(in_array("Manage Attribute",$menu)){ ?> checked="checked" <?php } ?>> Manage Attribute</td></tr>
            
            <tr><td width="10%"><input type="checkbox" class="group2" name="menu2[]" id="2_V" value="View All Attribute^attribute^index" <?php if(in_array("View All Attribute",$menu)){ ?> checked="checked" <?php } ?>> View All Attribute</td></tr>
        </tbody></table>
    </div>
</div>
</div>   
<div class="form-group">
 <div class="col-sm-12"><br>
 </div>
</div>

<div class="form-group">
 <div class="col-sm-6">
    <div class="title-div">
        <table width="80%">
            <tbody><tr>
                <td width="30%"><strong>&nbsp;</strong></td> 
            </tr>
            <tr style="background:#4993d7;">
                <td width="30%"><input type="checkbox" name="module3[]" <?php if(in_array("Brand Manager",$module)){ ?> checked="checked" <?php } ?> class="parenth" data-group=".group3" value="Brand Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Brand Manager"><strong style="color:#fff;"> Brand Manager</strong></td> 
                <td width="10%"></td> 
            </tr>
            

            <tr>	<td width="30%"><input type="checkbox" name="menu3[]" class="group3" value="Add Brand^brand^add" id="2_arrgruop" <?php if(in_array("Add Brand",$menu)){ ?> checked="checked" <?php } ?>> Add Brand</td></tr>

            <tr><td width="10%"><input type="checkbox" class="group3" name="menu3[]" id="2_V" value="Manage Brand^brand^edit" <?php if(in_array("Manage Brand",$menu)){ ?> checked="checked" <?php } ?>>  Manage Brand</td></tr>
            
            <tr><td width="10%"><input type="checkbox" class="group3" name="menu3[]" id="2_V" value="View All Brand^brand^index" <?php if(in_array("View All Brand",$menu)){ ?> checked="checked" <?php } ?>>   View All Brand</td></tr>
            
        </tbody></table>
    </div>
</div>
<div class="col-sm-6">
    <div class="title-div">
        <table width="80%">
            <tbody><tr>
                <td width="30%">&nbsp;</td>  
            </tr>
            <tr style="background:#4993d7;">
                <td width="30%"><input type="checkbox" name="module4[]" <?php if(in_array("Coupon Manager",$module)){ ?> checked="checked" <?php } ?> class="parenth" data-group=".group4" value="Coupon Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Coupon Manager"><strong style="color:#fff;"> Coupon Manager</strong></td>
                <td width="10%"></td>
            </tr>   

            <tr><td width="10%"><input type="checkbox" class="group4"  name="menu4[]" id="2_V" value="Add Coupon^coupons^add" <?php if(in_array("Add Coupon",$menu)){ ?> checked="checked" <?php } ?>> Add Coupon</td></tr>

            <tr><td width="10%"><input type="checkbox" class="group4" name="menu4[]" id="2_V" value="Manage Coupon^coupons^edit" <?php if(in_array("Manage Coupon",$menu)){ ?> checked="checked" <?php } ?>>  Manage Coupon</td></tr>

            <tr><td width="10%"><input type="checkbox" class="group4" name="menu4[]" id="2_V" value="View All Coupon^coupons^index" <?php if(in_array("View All Coupon",$menu)){ ?> checked="checked" <?php } ?>> View All Coupon</td></tr>   
        </tbody></table>
    </div>
</div>


<div class="form-group">
 <div class="col-sm-12"><br> 
 </div>
</div>
</div>
<div class="form-group">
 <div class="col-sm-6">
    <div class="title-div">
        <table width="80%">
            <tbody><tr>
                <td width="30%"><strong>&nbsp;</strong></td>
            </tr>
            <tr style="background:#4993d7;">
                <td width="30%"><input type="checkbox" name="module5[]" <?php if(in_array("Tax Manager",$module)){ ?> checked="checked" <?php } ?> class="parenth" data-group=".group5" value="Tax Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Tax Manager"><strong style="color:#fff;"> Tax Manager</strong></td>
                <td width="10%"></td>
            </tr>
            
            
            <tr><td width="30%"><input type="checkbox" name="menu5[]" class="group5" value="Add Tax^gst^add"  <?php if(in_array("Add Tax",$menu)){ ?> checked="checked" <?php } ?>> Add Tax</td></tr> 
            <tr><td width="30%"><input type="checkbox" name="menu5[]" class="group5" value="Manage Tax^gst^edit"  <?php if(in_array("Add Tax",$menu)){ ?> checked="checked" <?php } ?>> Manage Tax</td></tr> 
            <tr><td width="30%"><input type="checkbox" name="menu5[]" class="group5" value="View Tax^gst^index"  <?php if(in_array("View Tax",$menu)){ ?> checked="checked" <?php } ?>> View Tax</td></tr>                   
        </tbody></table>
    </div>
</div>
<div class="col-sm-6">
    <div class="title-div">
        <table width="80%">
            <tbody>
               <tr>
                <td width="30%">&nbsp;</td>  
            </tr>
            <tr style="background:#4993d7;">
                <td width="30%"><input type="checkbox" name="module6[]" <?php if(in_array("Country",$module)){ ?> checked="checked" <?php } ?> class="parenth" data-group=".group6" value="Country" id="2_arrgruop"><input type="hidden" name="module[]" value="Country"><strong style="color:#fff;"> Country</strong></td>
                
                <td width="10%"></td>
            </tr>
            <tr><td width="10%"><input type="checkbox" class="group6" name="menu6[]" id="2_V" value="Add Country^country^add" <?php if(in_array("Add Country",$menu)){ ?> checked="checked" <?php } ?>> Add Country</td></tr>

            <tr><td width="10%"><input type="checkbox" class="group6" name="menu6[]" id="2_V" value="Manage Country^country^edit" <?php if(in_array("Manage Country",$menu)){ ?> checked="checked" <?php } ?>> Manage Country</td></tr>

            <tr><td width="10%"><input type="checkbox" class="group6" name="menu6[]" id="2_V" value="View Country^country^index" <?php if(in_array("View Country",$menu)){ ?> checked="checked" <?php } ?>> View Country</td></tr>
            
        </tbody></table>
    </div>
</div>

</div>
<div class="form-group">
 <div class="col-sm-12"><br>
 </div>
</div>
<div class="form-group">
 <div class="col-sm-6">
    <div class="title-div">
        <table width="80%">
            <tbody>
               <tr>
                  <td width="30%"><strong style="font-size: 18px;">Front End Managers:</strong></td>
              </tr>
              <tr>
                <td width="30%"><strong>&nbsp;</strong></td>
            </tr>
            <tr style="background:#4993d7;">
                <td width="30%"><input type="checkbox" <?php if(in_array("Product Manager",$module)){ ?> checked="checked" <?php } ?> name="module7[]" class="parenth" data-group=".group7" value="Product Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Product Manager"><strong style="color:#fff;">Product Manager</strong></td>
                <td width="10%"></td>
            </tr>

            <tr>    <td width="30%"><input type="checkbox" name="menu7[]" class="group7" value="Add Product^product^add" id="2_arrgruop" <?php if(in_array("Add Product",$menu)){ ?> checked="checked" <?php } ?>>  Add Product</td></tr>
            <tr>	<td width="30%"><input type="checkbox" name="menu7[]" class="group7" value="Manage Product^product^edit" id="2_arrgruop" <?php if(in_array("Manage Product",$menu)){ ?> checked="checked" <?php } ?>>  Manage Product</td></tr>
            <tr><td width="30%"><input type="checkbox" name="menu7[]" class="group7" value="View Product^product^index" id="2_arrgruop" <?php if(in_array("View Product",$menu)){ ?> checked="checked" <?php } ?>> View Product</td></tr>
        </tbody></table>
    </div>
</div>

<div class="col-sm-6">
    <div class="title-div">
        <table width="80%">
            <tbody>
            <br>
               <tr>
                <td width="30%">&nbsp;</td>
            </tr>
            <tr style="background:#4993d7;">
                <td width="30%"><input type="checkbox" name="module8[]" <?php if(in_array("Order Manager",$module)){ ?> checked="checked" <?php } ?> class="parenth" data-group=".group8" value="Order Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Order Manager" ><strong style="color:#fff;"> Order Manager</strong></td>
                <td width="10%"></td></tr>
                
                <tr>
                    <td width="10%">
                        <input type="checkbox" class="group8" value="Add Order^order^add" name="menu8[]" id="2_V" <?php if(in_array("Add Order",$menu)){ ?> checked="checked" <?php } ?>> Add Order
                    </td>
                </tr>
             
            </tbody></table>
        </div>
    </div>
    
</div>



<div class="form-group">
 <div class="col-sm-12"><br>
 </div>
</div>
<div class="form-group">
 <div class="col-sm-6">
    <div class="title-div">
        <table width="80%">
            <tbody><tr>
                <td width="30%"><strong>&nbsp;</strong></td>
            </tr>
            <tr style="background:#4993d7;">
                <td width="30%"><input type="checkbox" name="module9[]" <?php if(in_array("Slider Manager",$module)){ ?> checked="checked" <?php } ?> class="parenth" data-group=".group9" value="Slider Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Slider Manager"><strong style="color:#fff;">Slider Manager</strong></td>
                <td width="10%"></td>
            </tr>
            <tr>	<td width="30%"><input type="checkbox" name="menu9[]" class="group9" value="Add Slider^slider^add" id="2_arrgruop" <?php if(in_array("Add Slider",$menu)){ ?> checked="checked" <?php } ?>> Add Slider</td></tr>

            <tr><td width="30%"><input type="checkbox" name="menu9[]" class="group9" value="Manage Slider^slider^edit" id="2_arrgruop" <?php if(in_array("Manage Slider",$menu)){ ?> checked="checked" <?php } ?>> Manage Slider</td></tr>
            
            <tr><td width="30%"><input type="checkbox" name="menu9[]" class="group9" value="View Slider^slider^index" <?php if(in_array("View Slider",$menu)){ ?> checked="checked" <?php } ?> id="2_arrgruop"> View Slider</td></tr>
        </tbody></table>
    </div>
</div>
<div class="col-sm-6">
    <div class="title-div">
        <table width="80%">
            <tbody><tr>
                <td width="30%">&nbsp;</td>
            </tr>
            <tr style="background:#4993d7;">
                <td width="30%"><input type="checkbox" name="module10[]"  <?php if(in_array("Static Page Manager",$module)){ ?> checked="checked" <?php } ?> class="parenth" data-group=".group10" value="Static Page Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Static Page Manager"><strong style="color:#fff;"> Static Page Manager</strong></td>
                <td width="10%"></td>
            </tr>

            <tr><td width="10%"><input type="checkbox" class="group10" name="menu10[]" id="2_V" value="Add Static Page^staticpages^add" <?php if(in_array("Add Static Page",$menu)){ ?> checked="checked" <?php } ?>> Add Static Page</td></tr>
            
            <tr><td width="10%"><input type="checkbox" class="group10" name="menu10[]" id="2_V" value="Manage Static Page^staticpages^edit" <?php if(in_array("Manage Static Page",$menu)){ ?> checked="checked" <?php } ?>> Manage Static Page</td></tr>      
            
            <tr><td width="10%"><input type="checkbox" class="group10" name="menu10[]" id="2_V" value="View Static Page^staticpages^index" <?php if(in_array("View Static Page",$menu)){ ?> checked="checked" <?php } ?>> View Static Page</td></tr> 
            
        </tbody></table>
    </div>
</div>
</div>
<div class="form-group">
 <div class="col-sm-12"><br>
 </div>
</div>

<div class="form-group">
 <div class="col-sm-6">
    <div class="title-div">
        <table width="80%">
            <tbody><tr>
                <td width="30%"><strong>&nbsp;</strong></td> 
            </tr>
            <tr style="background:#4993d7;">
                <td width="30%"><input type="checkbox" name="module11[]" class="parenth" data-group=".group11" value="Drop Shipper Manager" <?php if(in_array("Drop Shipper Manager",$module)){ ?> checked="checked" <?php } ?> id="Drop Shipper Manager"><input type="hidden" name="module[]" value="Drop Shipper Manager"><strong style="color:#fff;"> Drop Shipper Manager</strong></td>
                <td width="10%"></td>
            </tr>

            <tr>
                <td width="30%"><input type="checkbox" name="menu11[]" class="group11" value="Add Drop Shipper^dropshipper^add" id="2_arrgruop" <?php if(in_array("Add Drop Shipper",$menu)){ ?> checked="checked" <?php } ?>> Add Drop Shipper</td></tr>
                <tr>
                    <td width="30%"><input type="checkbox" name="menu11[]" class="group11" value="Manage Drop Shipper^dropshipper^edit" id="2_arrgruop" <?php if(in_array("Manage Drop Shipper",$menu)){ ?> checked="checked" <?php } ?>>  Manage Drop Shipper</td></tr>
                    <tr>	<td width="30%"><input type="checkbox" name="menu11[]" class="group11" value="View Drop Shipper^dropshipper^index" id="2_arrgruop" <?php if(in_array("View Drop Shipper",$menu)){ ?> checked="checked" <?php } ?>>  View Drop Shipper</td></tr>
                </tbody></table>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="title-div">
                <table width="80%">
                    <tbody><tr>
                        <td width="30%">&nbsp;</td>
                    </tr>
                    <tr style="background:#4993d7;">
                        <td width="30%"><input type="checkbox" <?php if(in_array("Customer Manager",$module)){ ?> checked="checked" <?php } ?> name="module12[]" class="parenth" data-group=".group12" value="Customer Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Customer Manager"><strong style="color:#fff;"> Customer Manager</strong></td>
                        <td width="10%"></td>               
                    </tr>
                    <tr><td width="10%"><input type="checkbox" class="group12" name="menu12[]" id="2_V" value="Add Customer^customer^add" <?php if(in_array("Add Customer",$menu)){ ?> checked="checked" <?php } ?>> Add Customer</td></tr>

                    <tr><td width="10%"><input type="checkbox" class="group12" name="menu12[]" id="2_V" value="Manage Customer^customer^edit" <?php if(in_array("Manage Customer",$menu)){ ?> checked="checked" <?php } ?>> Manage Customer</td></tr>           

                    <tr><td width="10%"><input type="checkbox" class="group12" name="menu12[]" id="2_V" value="View Customer^customer^index" <?php if(in_array("View Customer",$menu)){ ?> checked="checked" <?php } ?>> View Customer</td></tr>  

                </tbody></table>
            </div>
        </div>
    </div>

    <div class="form-group">
     <div class="col-sm-6">
        <div class="title-div">
            <table width="80%">
                <tbody><tr>
                    <td width="30%"><strong>&nbsp;</strong></td>
                </tr> 	
                <tr style="background:#4993d7;">
                    <td width="30%"><input type="checkbox" <?php if(in_array("Email Manager",$module)){ ?> checked="checked" <?php } ?> name="module13[]" class="parenth" data-group=".group13" value="Email Manager" id="2_arrgruop"> 
                        <input type="hidden" name="module[]" value="Email Manager"><strong style="color:#fff;"> Email Manager</strong></td>

                        <td width="10%"></td> </tr>
                        <tr><td width="30%"><input type="checkbox" name="menu13[]" class="group13" value="Manage Email^emailtemplate^edit" id="2_arrgruop" <?php if(in_array("Manage Email",$menu)){ ?> checked="checked" <?php } ?>>  Manage Email</td></tr>
                        <tr><td width="30%"><input type="checkbox" name="menu13[]" class="group13" value="View Email^emailtemplate^index" id="2_arrgruop" <?php if(in_array("View Email",$menu)){ ?> checked="checked" <?php } ?>> View Email</td></tr>

                    </tbody></table>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="title-div">
                    <table width="80%">
                        <tbody><tr>
                            <td width="30%"><strong>&nbsp;</strong></td>
                        </tr> 	
                        <tr style="background:#4993d7;" >
                            <td width="30%"><input type="checkbox" <?php if(in_array("Seo Manager",$module)){ ?> checked="checked" <?php } ?> name="module14[]" class="parenth" data-group=".group14" value="Seo Manager" id="2_arrgruop"> 
                                <input type="hidden" name="module[]" value="Seo Manager" ><strong style="color:#fff;"> Seo Manager</strong></td>

                                <td width="10%"></td> </tr>
                                <tr>	<td width="30%"><input type="checkbox" name="menu14[]" class="group14" value="Add Seo^seo^add" <?php if(in_array("Add Seo",$menu)){ ?> checked="checked" <?php } ?> id="2_arrgruop">  Add Seo</td></tr>

                                <tr><td width="30%"><input type="checkbox" name="menu14[]" class="group14" <?php if(in_array("Manage Seo",$menu)){ ?> checked="checked" <?php } ?> value="Manage Seo^seo^edit" id="2_arrgruop">  Manage Seo</td></tr>

                                <tr><td width="30%"><input type="checkbox" name="menu14[]" class="group14" <?php if(in_array("View Seo",$menu)){ ?> checked="checked" <?php } ?> value="View Seo^seo^index" id="2_arrgruop">  View Seo</td></tr>
                            </tbody></table>
                        </div>
                    </div>
                </div>

                <!-- <div class="form-group">
                    <div class="col-sm-6"> 
                        <div class="title-div">
                           <table width="80%">
                            <tbody><tr>
                                <td width="30%"><strong>&nbsp;</strong></td>
                            </tr>                                                                      
                            <tr style="background:#4993d7;" >
                                <td width="30%"><input type="checkbox" <?php if (in_array("Enquiry Manager", $module)) {?> checked="checked" <?php } ?> name="module15[]" class="parenth" data-group=".group15" value="Enquiry Manager" id="2_arrgruop"> 
                                    <input type="hidden" name="module[]" value="Enquiry Manager" ><strong style="color:#fff;"> Enquiry Manager</strong></td>
                                    
                                    <td width="10%"></td> </tr>

                                    <tr>    <td width="30%"><input type="checkbox" name="menu15[]" class="group15" value="View Enquiry^enquiry^index"  <?php if (in_array("View Enquiry", $menu)) {?> checked="checked" <?php } ?> id="2_arrgruop"> View Enquiry</td></tr>
                                     
                                </tbody>
                            </table>                            
                        </div>
                    </div>    
          
                    <div class="col-sm-6"> 
                        <div class="title-div">
                           <table width="80%">
                            <tbody><tr>
                                <td width="30%"><strong>&nbsp;</strong></td>
                            </tr>                                                                      
                            <tr style="background:#4993d7;" >
                                <td width="30%"><input type="checkbox" <?php if (in_array("Testimonial Manager", $module)) {?> checked="checked" <?php } ?> name="module16[]" class="parenth" data-group=".group16" value="Testimonial Manager" id="2_arrgruop"> 
                                    <input type="hidden" name="module[]" value="Testimonial Manager" ><strong style="color:#fff;"> Testimonial Manager</strong></td>

                                    <td width="10%"></td> </tr>

                                    <tr>    <td width="30%"><input type="checkbox" name="menu16[]" class="group16" value="Add Testimonial^testimonials^add"  <?php if (in_array("Add Testimonial", $menu)) {?> checked="checked" <?php } ?> id="2_arrgruop"> Add Testimonial</td></tr>
                                    <tr>    <td width="30%"><input type="checkbox" name="menu16[]" class="group16" value="Manage Testimonial^testimonials^edit"  <?php if (in_array("Manage Testimonial", $menu)) {?> checked="checked" <?php } ?> id="2_arrgruop"> Manage Testimonial</td></tr>
                                    <tr>    <td width="30%"><input type="checkbox" name="menu16[]" class="group16" value="View Testimonial^testimonials^index"  <?php if (in_array("View Testimonial", $menu)) {?> checked="checked" <?php } ?> id="2_arrgruop"> View Testimonial</td></tr>
                                </tbody>
                            </table>                            
                        </div>
                    </div>    
                </div>  


                <div class="form-group">
                    <div class="col-sm-6"> 
                        <div class="title-div">
                           <table width="80%">
                            <tbody><tr>
                                <td width="30%"><strong>&nbsp;</strong></td>
                            </tr>                                                                      
                            <tr style="background:#4993d7;" >
                                <td width="30%"><input type="checkbox" <?php if (in_array("Seo Manager", $module)) {?> checked="checked" <?php } ?> name="module17[]" class="parenth" data-group=".group17" value="Seo Manager" id="2_arrgruop"> 
                                    <input type="hidden" name="module[]" value="Seo Manager" ><strong style="color:#fff;"> Seo Manager</strong></td>
                                    <td width="10%"></td> </tr>

                                    <tr>    <td width="30%"><input type="checkbox" name="menu17[]" class="group17" value="Add Seo^seo^add"  <?php if (in_array("Add Seo", $menu)) {?> checked="checked" <?php } ?> id="2_arrgruop"> Add Seo</td></tr>
                                    <tr>    <td width="30%"><input type="checkbox" name="menu17[]" class="group17" value="Manage Seo^seo^edit"  <?php if (in_array("Manage Seo", $menu)) {?> checked="checked" <?php } ?> id="2_arrgruop"> Manage Seo</td></tr>
                                    <tr>    <td width="30%"><input type="checkbox" name="menu17[]" class="group17" value="View Seo^seo^index"  <?php if (in_array("View Seo", $menu)) {?> checked="checked" <?php } ?> id="2_arrgruop"> View Seo</td></tr>
                                </tbody>
                            </table>                            
                        </div>
                    </div>    
                </div>  -->