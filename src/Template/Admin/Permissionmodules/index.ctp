<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Manage Permission Module </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo ADMIN_URL;?>PermissionModules"><i class="fa fa-home"></i>Home</a></li>
      <li><a href="<?php echo ADMIN_URL;?>PermissionModules/index">Manage Permission Module</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
             <i class="fa fa-search"></i> View Permission Modules List </h3>
             <?php echo $this->Flash->render(); ?>
           </div>
           <!-- /.box-header -->
           <?php echo $this->Form->create($classes, array(
             'class'=>'form-horizontal',
             'id' => 'sevice_sform',
             'enctype' => 'multipart/form-data',
             'validate'
           )); ?>
           <div class="box-body">
            <div class="manag-stu">
              <div style="float:center; width:100%">  
                <div class="title-field-group">
                  <div class="form-">
                   <div class="col-sm-3">
                    <label >User Role<span style="color:red;">*</span></label>
                    <?php  
                    echo $this->Form->input('user_id',array('class'=>'form-control','type'=>'select','id'=>'emp-type','empty'=>'--Select--','options'=>$employees,'label' =>false,'required')); ?>
                  </div>
                </div> <!-- .field-group -->
                <script>
                  $( document ).ready(function() {
                    $(".parenth").each(function(index){
                      var group = $(this).data("group");
                      var parent = $(this);

    parent.change(function(){  //"select all" change 
     $(group).prop('checked', parent.prop("checked"));
   });
    $(group).change(function(){ 
       // parent.prop('checked', false);
       parent.prop('checked', true);
       if ($(group+':checked').length == 0){
        parent.prop('checked', false);
      }
    });
  });
                  });
                </script>
                <script>

                  $(document).ready(function () {

  //--------------------------------------------

  $('#emp-type').on('change', function(){
    var emp = $('#emp-type').val();
    $.ajax({
      type: 'POST', 
      url: '<?php echo ADMIN_URL;?>permissionmodules/calculatepermission',
      data: {'empid':emp},
      success: function(data){ 
        $("#amountrt").html(data);
      }
    });  
  });
});

</script>

<div class="form-group">
 <div class="col-sm-6"><div class="submit">
   <input type="submit" class="btn btn-info pull-left" value="Update Rights" title="Update" style="margin-top: 24px;"></div></div><br>
 </div>
 <br><br>
 <div id="amountrt">
  <div class="form-group">
   <div class="col-sm-6">
    <div class="title-div">
      <table width="80%">
        <tbody><tr>
          <td width="30%"><strong style="font-size: 18px;">Master Managers:</strong></td>
        </tr>
        <tr>
          <td width="30%"><strong>&nbsp;</strong></td>
        </tr>
        <tr style="background:#4993d7;" >
          <td width="30%">
           <input type="checkbox" name="module1[]" class="parenth" data-group=".group1" value="Category  Manager" id="2_arrgruop">
           <input type="hidden" name="module[]" value="Category  Manager" ><strong style="color:#fff;"> Category Manager</strong></td>
           <td width="10%"></td>
         </tr>

         <tr><td width="30%"><input type="checkbox" name="menu1[]" class="group1" value="Add Category^category^add" id="2_arrgruop">  Add Category</td></tr>

         <tr><td width="10%"><input type="checkbox" class="group1" name="menu1[]" id="2_V" value="Manage Category^category^edit" >   Manage Category</td></tr>

         <tr><td width="10%"><input type="checkbox" class="group1" name="menu1[]" id="2_V" value="View All Category^category^index" >  View All Category</td></tr>         
       </tbody></table>
     </div>
   </div>
   <div class="col-sm-6">
    <div class="title-div">
      <table width="80%">
        <tbody>
        <br>
          <tr>
            <td width="30%">&nbsp;</td>
          </tr>
        <tr style="background:#4993d7;" >
          <td width="30%"><input type="checkbox" name="module2[]"  class="parenth" data-group=".group2" value="Attribute Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Attribute Manager" ><strong style="color:#fff;"> Attribute Manager</strong></td>
          <td width="10%"></td>
        </tr>

        <tr>  <td width="30%"><input type="checkbox" name="menu2[]" class="group2" value="Add Attribute^attribute^add" id="2_arrgruop"> Add Attribute</td></tr>

        <tr><td width="10%"><input type="checkbox" class="group2" name="menu2[]" id="2_V" value="Manage Attribute^attribute^edit" > Manage Attribute</td></tr>

        <tr><td width="10%"><input type="checkbox" class="group2" name="menu2[]" id="2_V" value="View All Attribute^attribute^index" > View All Attribute</td></tr>
      </tbody></table>
    </div>
  </div>

</div>   <div class="form-group">
 <div class="col-sm-12"><br>
 </div>
</div>
<div class="form-group">
 <div class="col-sm-6">
  <div class="title-div">
    <table width="80%">
      <tbody><tr>
        <td width="30%"><strong>&nbsp;</strong></td>
      </tr>
      <tr style="background:#4993d7;" >
        <td width="30%"><input type="checkbox" name="module3[]" class="parenth" data-group=".group3" value="Brand Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Brand Manager" ><strong style="color:#fff;"> Brand Manager</strong></td>
        <td width="10%"></td>
      </tr>

      <tr>  <td width="30%"><input type="checkbox" name="menu3[]" class="group3" value="Add Brand^brand^add" id="2_arrgruop"> Add Brand</td></tr>

      <tr><td width="10%"><input type="checkbox" class="group3" name="menu3[]" id="2_V" value="Manage Brand^brand^edit" >  Manage Brand</td></tr>

      <tr><td width="10%"><input type="checkbox" class="group3" name="menu3[]" id="2_V" value="View All Brand^brand^index" >  View All Brand</td></tr>
      
    </tbody></table>
  </div>
</div>
<div class="col-sm-6">
  <div class="title-div">
    <table width="80%">
      <tbody><tr>
        <td width="30%">&nbsp;</td>
      </tr>
      <tr style="background:#4993d7;" >
        <td width="30%"><input type="checkbox" name="module4[]" class="parenth" data-group=".group4" value="Coupon Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Coupon Manager" ><strong style="color:#fff;"> Coupon Manager</strong></td>
        <td width="10%"></td>
      </tr>

      <tr><td width="10%"><input type="checkbox" class="group4" name="menu4[]" id="2_V" value="Add Coupon^coupons^add" > Add Coupon</td></tr>
      <tr><td width="10%"><input type="checkbox" class="group4" name="menu4[]" id="2_V" value="Manage Coupon^coupons^edit" >  Manage Coupon</td></tr>
      <tr>  <td width="30%"><input type="checkbox" name="menu4[]" class="group4" value="View All Coupon^coupons^index" id="2_arrgruop"> View All Coupon</td></tr>
    </tbody></table>
  </div>
</div>

<div class="form-group">
 <div class="col-sm-12"><br>
 </div>
</div>
</div>
<div class="form-group">
 <div class="col-sm-6">
  <div class="title-div">
    <table width="80%">
      <tbody><tr>
        <td width="30%"><strong>&nbsp;</strong></td>
      </tr>
      <tr style="background:#4993d7;" >
        <td width="30%"><input type="checkbox" name="module5[]" class="parenth" data-group=".group5" value="Tax Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Tax Manager" ><strong style="color:#fff;"> Tax Manager</strong></td>
        <td width="10%"></td>
      </tr>

      <tr><td width="30%"><input type="checkbox" name="menu5[]" class="group5"  value="Add Tax^gst^add"> Add Tax</td></tr>

      <tr><td width="30%"><input type="checkbox" name="menu5[]" class="group5"  value="Manage Tax^gst^edit"> Manage Tax</td></tr>

      <tr><td width="30%"><input type="checkbox" name="menu5[]" class="group5"  value="View Tax^gst^index"> View Tax</td></tr>                  
    </tbody></table>
  </div>
</div>
<div class="col-sm-6">
  <div class="title-div">
    <table width="80%">
      <tbody>
       <tr>
        <td width="30%">&nbsp;</td>
      </tr>
      <tr style="background:#4993d7;" >
        <td width="30%"><input type="checkbox" name="module6[]" class="parenth" data-group=".group6" value="Country" id="2_arrgruop"><input type="hidden" name="module[]" value="Country" ><strong style="color:#fff;"> Country</strong></td>

        <td width="10%"></td>
      </tr>
      <tr><td width="10%"><input type="checkbox" class="group6"  name="menu6[]" id="2_V" value="Add Country^country^add" > Add Country</td></tr>

      <tr><td width="10%"><input type="checkbox" class="group6"  name="menu6[]" id="2_V" value="Manage Country^country^edit" > Manage Country</td></tr>

      <tr><td width="10%"><input type="checkbox" class="group6"  name="menu6[]" id="2_V" value="View Country^country^index" > View Country</td></tr>


    </tbody></table>
  </div>
</div>


</div>
<div class="form-group">
 <div class="col-sm-12"><br>
 </div>
</div>
<div class="form-group">
 <div class="col-sm-6">
  <div class="title-div">
    <table width="80%">
      <tbody>
        <tr>
          <td width="30%"><strong style="font-size: 18px;">Front End Managers:</strong></td>
        </tr>
        <tr>
          <td width="30%"><strong>&nbsp;</strong></td>
        </tr>
        <tr style="background:#4993d7;" >
          <td width="30%"><input type="checkbox" name="module7[]"  class="parenth" data-group=".group7" value="Product Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Product Manager" ><strong style="color:#fff;">Product Manager</strong></td>
          <td width="10%"></td>
        </tr>

        <tr>  <td width="30%"><input type="checkbox" name="menu7[]" class="group7" value="Add Product^product^add" id="2_arrgruop">  Add Product</td></tr>
        <tr>  <td width="30%"><input type="checkbox" name="menu7[]" class="group7" value="Manage Product^product^edit" id="2_arrgruop">  Manage Product</td></tr>
        <tr>  <td width="30%"><input type="checkbox" name="menu7[]" class="group7" value="View Product^product^index" id="2_arrgruop">  View Product</td></tr>
      </tbody></table>
    </div>
  </div>

  <div class="col-sm-6">
    <div class="title-div">
      <table width="80%">
        <tbody>
          <br>
          <tr>
            <td width="30%">&nbsp;</td>
          </tr>
          <tr style="background:#4993d7;" >
            <td width="30%"><input type="checkbox" name="module8[]" class="parenth" data-group=".group8" value="Order Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Order Manager" ><strong style="color:#fff;"> Order Manager</strong></td>
            <td width="10%"></td></tr>
            <tr>
              <td width="10%">
                <input type="checkbox" class="group8" value="View Order^order^index" name="menu8[]" id="2_V"> View Order
              </td>
            </tr>
          </tbody></table>
        </div>
      </div>
    </div>

    <div class="form-group">
     <div class="col-sm-12"><br>
     </div>
   </div>
   <div class="form-group">
     <div class="col-sm-6">
      <div class="title-div">
        <table width="80%">
          <tbody><tr>
            <td width="30%"><strong>&nbsp;</strong></td>
          </tr>
          <tr style="background:#4993d7;" >
            <td width="30%"><input type="checkbox" name="module9[]" class="parenth" data-group=".group9" value="Slider Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Slider Manager" ><strong style="color:#fff;">Slider Manager</strong></td>
            <td width="10%"></td>
          </tr>

          <tr>  <td width="30%"><input type="checkbox" name="menu9[]" class="group9" value="Add Slider^slider^add" id="2_arrgruop"> Add Slider</td></tr>
          <tr>  <td width="30%"><input type="checkbox" name="menu9[]" class="group9" value="Manage Slider^slider^edit" id="2_arrgruop"> Manage Slider</td></tr>
          <tr>  <td width="30%"><input type="checkbox" name="menu9[]" class="group9" value="View Slider^slider^index" id="2_arrgruop"> View Slider</td></tr>
        </tbody></table>
      </div>
    </div>

    <div class="col-sm-6">
      <div class="title-div">
        <table width="80%">
          <tbody><tr>
            <td width="30%">&nbsp;</td>
          </tr>
          <tr style="background:#4993d7;" >
            <td width="30%"><input type="checkbox" name="module10[]"  class="parenth" data-group=".group10" value="Static Page Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Static Page Manager" ><strong style="color:#fff;"> Static Page Manager</strong></td>
            <td width="10%"></td>
          </tr>

          <tr><td width="10%"><input type="checkbox" class="group10" name="menu10[]" id="2_V" value="Add Static Page^staticpages^add"> Add Static Page </td></tr>

          <tr><td width="10%"><input type="checkbox" class="group10" name="menu10[]" id="2_V" value="Manage Static Page^staticpages^edit"> Manage Static Page</td></tr>      

          <tr><td width="10%"><input type="checkbox" class="group10" name="menu10[]" id="2_V" value="View Static Page^staticpages^index" > View Static Page</td></tr> 
        </tbody></table>
      </div>
    </div>
  </div>
  <div class="form-group">
   <div class="col-sm-12"><br>
   </div>
 </div>

 <div class="form-group">
   <div class="col-sm-6">
    <div class="title-div">
      <table width="80%">
        <tbody><tr>
          <td width="30%"><strong>&nbsp;</strong></td>
        </tr>
        <tr style="background:#4993d7;" >
          <td width="30%"><input type="checkbox" name="module11[]" class="parenth" data-group=".group11" value="Drop Shipper Manager" id="Drop Shipper Manager"><input type="hidden" name="module[]" value="Drop Shipper Manager" ><strong style="color:#fff;"> Drop Shipper Manager</strong></td>
          <td width="10%"></td>
        </tr>

        <tr>
          <td width="30%"><input type="checkbox" name="menu11[]" class="group11" value="Add Drop Shipper^dropshipper^add" id="2_arrgruop"> Add Drop Shipper</td></tr>
          <tr>
            <td width="30%"><input type="checkbox" name="menu11[]" class="group11" value="Manage Drop Shipper^dropshipper^edit" id="2_arrgruop">  Manage Drop Shipper</td></tr>
            <tr>  <td width="30%"><input type="checkbox" name="menu11[]" class="group11" value="View Drop Shipper^dropshipper^index" id="2_arrgruop">  View Drop Shipper</td></tr>

          </tbody></table>
        </div>
      </div>

      <div class="col-sm-6">
        <div class="title-div">
          <table width="80%">
            <tbody><tr>
              <td width="30%">&nbsp;</td>
            </tr>
            <tr style="background:#4993d7;" >
              <td width="30%"><input type="checkbox" name="module12[]" class="parenth" data-group=".group12" value="Customer Manager" id="2_arrgruop"><input type="hidden" name="module[]" value="Customer Manager" ><strong style="color:#fff;"> Customer Manager</strong></td>
              <td width="10%"></td>
            </tr>

            <tr><td width="10%"><input type="checkbox" class="group12" name="menu12[]" id="2_V" value="Add Customer^customer^add" > Add Customer</td></tr>
            <tr><td width="10%"><input type="checkbox" class="group12" name="menu12[]" id="2_V" value="Manage Customer^customer^edit" > Manage Customer</td></tr>           
            <tr><td width="10%"><input type="checkbox" class="group12" name="menu12[]" id="2_V" value="View Customer^customer^index" > View Customer</td></tr>  

          </tbody></table>
        </div>
      </div>

    </div>



    <div class="form-group">
     <div class="col-sm-6">
      <div class="title-div">
        <table width="80%">
          <tbody><tr>
            <td width="30%"><strong>&nbsp;</strong></td>


          </tr>   
          <tr style="background:#4993d7;" >
            <td width="30%"><input type="checkbox" name="module13[]" class="parenth" data-group=".group13" value="Email Manager" id="2_arrgruop"> 
              <input type="hidden" name="module[]" value="Email Manager" ><strong style="color:#fff;"> Email Manager</strong></td>

              <tr><td width="30%"><input type="checkbox" name="menu13[]" class="group13" value="Manage Email^emailtemplate^edit" id="2_arrgruop"> Manage Email</td></tr>
              <tr>  <td width="30%"><input type="checkbox" name="menu13[]" class="group13" value="View Email^emailtemplate^index" id="2_arrgruop"> View Email</td></tr>
              
            </tbody></table>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="title-div">
           <table width="80%">
            <tbody><tr>
              <td width="30%"><strong>&nbsp;</strong></td>
            </tr>   
            <tr style="background:#4993d7;" >
              <td width="30%"><input type="checkbox" name="module14[]" class="parenth" data-group=".group14" value="Seo Manager" id="2_arrgruop"> 
                <input type="hidden" name="module[]" value="Seo Manager" ><strong style="color:#fff;"> Seo Manager</strong></td>

                <td width="10%"></td> </tr>
                <tr>  <td width="30%"><input type="checkbox" name="menu14[]" class="group14" value="Add Seo^seo^add" id="2_arrgruop">  Add Seo</td></tr>
                <tr>  <td width="30%"><input type="checkbox" name="menu14[]" class="group14" value="Manage Seo^seo^edit" id="2_arrgruop">  Manage Seo</td></tr>
                <tr>  <td width="30%"><input type="checkbox" name="menu14[]" class="group14" value="View Seo^seo^index" id="2_arrgruop">  View Seo</td></tr>
              </tbody></table>
            </div>
          </div>
        </div>

<!-- 
        <div class="form-group">
          <div class="col-sm-6"> 
            <div class="title-div">
             <table width="80%">
              <tbody><tr>
                <td width="30%"><strong>&nbsp;</strong></td>
              </tr>                                                             
              <tr style="background:#4993d7;" >
                <td width="30%"><input type="checkbox" name="module15[]" class="parenth" data-group=".group15" value="Enquiry Manager" id="2_arrgruop"> 
                  <input type="hidden" name="module[]" value="Enquiry Manager" ><strong style="color:#fff;"> Enquiry Manager</strong></td>

                  <td width="10%"></td> </tr>
                  <tr>  <td width="30%"><input type="checkbox" name="menu15[]" class="group15" value="View Enquiry^enquiry^index" id="2_arrgruop"> View Enquiry</td></tr>

                  </tbody></table>                            
                    </div>
                   </div>    
               
          <div class="col-sm-6"> 
            <div class="title-div">
             <table width="80%">
              <tbody><tr>
                <td width="30%"><strong>&nbsp;</strong></td>
              </tr>                                                             
              <tr style="background:#4993d7;" >
                <td width="30%"><input type="checkbox" name="module16[]" class="parenth" data-group=".group16" value="Testimonial Manager" id="2_arrgruop"> 
                  <input type="hidden" name="module[]" value="Testimonial Manager" ><strong style="color:#fff;"> Testimonial Manager</strong></td>

                  <td width="10%"></td> </tr>
                  <tr>  <td width="30%"><input type="checkbox" name="menu16[]" class="group16" value="Add Testimonial^testimonials^add" id="2_arrgruop"> Add Testimonial</td></tr>
                  <tr>  <td width="30%"><input type="checkbox" name="menu16[]" class="group16" value="Manage Testimonial^testimonials^edit" id="2_arrgruop"> Manage Testimonial</td></tr>
                  <tr>  <td width="30%"><input type="checkbox" name="menu16[]" class="group16" value="View Testimonial^testimonials^index" id="2_arrgruop"> View Testimonial</td></tr>

                  </tbody></table>                            
                    </div>
                   </div>    
                </div>

        <div class="form-group">
          <div class="col-sm-6"> 
            <div class="title-div">
             <table width="80%">
              <tbody><tr>
                <td width="30%"><strong>&nbsp;</strong></td>
              </tr>                                                             
              <tr style="background:#4993d7;" >
                <td width="30%"><input type="checkbox" name="module17[]" class="parenth" data-group=".group17" value="Seo Manager" id="2_arrgruop"> 
                  <input type="hidden" name="module[]" value="Seo Manager" ><strong style="color:#fff;"> Seo Manager</strong></td>

                  <td width="10%"></td> </tr>
                  <tr>  <td width="30%"><input type="checkbox" name="menu17[]" class="group17" value="Add Seo^seo^add" id="2_arrgruop"> Add Seo</td></tr>
                  <tr>  <td width="30%"><input type="checkbox" name="menu17[]" class="group17" value="Manage Seo^seo^edit" id="2_arrgruop"> Manage Seo</td></tr>
                  <tr>  <td width="30%"><input type="checkbox" name="menu17[]" class="group17" value="View Seo^seo^index" id="2_arrgruop"> View Seo</td></tr>

                  </tbody></table>                            
                    </div>
                   </div>    
                </div> -->
                
              </div>
            </div>
          </div>
      </div>
      <!-- /.box -->
    </form>
    </div>
    </div>
  </div>
 </section>

