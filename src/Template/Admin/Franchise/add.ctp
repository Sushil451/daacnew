
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
			<?php echo $this->Form->create($newresponse, array(
						'class'=>'form-horizontal needs-validation',
						'type' => 'file',
						'validate','novalidate'
						
					));
				
					 ?>

				<div class="box box-info">
					<?php echo $this->Flash->render(); ?>
					<div class="box-header with-border">
						<h3 class="box-title">Installment Info</h3>
					</div>
			
					<div class="box-body addStdntsFrm">
						<div class="row">
							<div class="col-sm-3">
                           
								<label for="inputEmail3" class="control-label">Branch Head Name<strong style="color:red;">*</strong></label>
								<?php echo $this->Form->input('head_name', array('class' => 'form-control','required','label'=>false,'autocomplete'=>'off','type'=>'text')); ?>   
							
                           
							</div>

                            <div class="col-sm-3">
                           
                           <label for="inputEmail3" class="control-label">Branch Head Mobile<strong style="color:red;">*</strong></label>
						   
                           <?php echo $this->Form->input('branch_mobile', array('class' => 'form-control','maxlength'=>'10','onkeypress'=>'return isNumber12(event);','required')); ?>   
                       
                           
                       </div>

					   <div class="col-sm-3">
                           
								<label for="inputEmail3" class="control-label">Branch Head User<strong style="color:red;">*</strong></label>
								<?php echo $this->Form->input('head_email', array('class' => 'form-control','required','label'=>false,'autocomplete'=>'off','type'=>'text')); ?>   
							
                           
							</div>

             
					   <div class="col-sm-3">
                           
						   <label for="inputEmail3" class="control-label">Password<strong style="color:red;">*</strong></label>
						   <?php echo $this->Form->input('head_password', array('class' => 'form-control','required','label'=>false,'autocomplete'=>'off','type'=>'password')); ?>   
					   
					  
					   </div>

					   
					   <div class="col-sm-3">
                           
								<label for="inputEmail3" class="control-label">Confirm Password<strong style="color:red;">*</strong></label>
								<?php echo $this->Form->input('head_confirm_password', array('class' => 'form-control','required','label'=>false,'autocomplete'=>'off','type'=>'password')); ?>   
							
                           
							</div>

							<div class="col-sm-3">
                           
								<label for="inputEmail3" class="control-label">Counsellor Name<strong style="color:red;">*</strong></label>
								<?php echo $this->Form->input('counsel_name', array('class' => 'form-control','required','label'=>false,'autocomplete'=>'off','type'=>'text')); ?>   
							
                           
							</div>

							<div class="col-sm-3">
                           
						   <label for="inputEmail3" class="control-label">Counsellor Mobile<strong style="color:red;">*</strong></label>
						   <?php echo $this->Form->input('counsel_mobile', array('class' => 'form-control','maxlength'=>'10','onkeypress'=>'return isNumber12(event);','required')); ?>   
					   
					  
					   </div>
					   <div class="col-sm-3">
                           
								<label for="inputEmail3" class="control-label">Counsellor User<strong style="color:red;">*</strong></label>
								<?php echo $this->Form->input('counsel_email', array('class' => 'form-control','required','label'=>false,'autocomplete'=>'off','type'=>'text')); ?>   
							
                           
							</div>
							
							<div class="col-sm-3">
                           
						   <label for="inputEmail3" class="control-label">Password<strong style="color:red;">*</strong></label>
						   <?php echo $this->Form->input('counsel_password', array('class' => 'form-control','required','label'=>false,'autocomplete'=>'off','type'=>'password')); ?>   
					   
					  
					   </div>

					   
					   <div class="col-sm-3">
                           
								<label for="inputEmail3" class="control-label">Confirm Password<strong style="color:red;">*</strong></label>
								<?php echo $this->Form->input('head_confirm_password', array('class' => 'form-control','required','label'=>false,'autocomplete'=>'off','type'=>'password')); ?>   
							
                           
							</div>



							<div class="col-sm-3">
                           
								<label for="inputEmail3" class="control-label">Branch Name<strong style="color:red;">*</strong></label>
								<?php echo $this->Form->input('branch_name', array('class' => 'form-control','required','label'=>false,'autocomplete'=>'off','type'=>'text')); ?>   
							
                           
							</div>

							<div class="col-sm-3">
                           
						   <label for="inputEmail3" class="control-label">Branch Address<strong style="color:red;">*</strong></label>
						   <?php echo $this->Form->input('branch_address', array('class' => 'form-control','required','label'=>false,'autocomplete'=>'off','type'=>'text')); ?>   
					   
					  
					   </div>
					   <div class="col-sm-3">
                           
						   <label for="inputEmail3" class="control-label">Branch Phone<strong style="color:red;">*</strong></label>
						   <?php echo $this->Form->input('branch_phone', array('class' => 'form-control','required','label'=>false,'autocomplete'=>'off','type'=>'text')); ?>   
					   
					  
					   </div>



					   <div class="col-sm-3">
                           
						   <label for="inputEmail3" class="control-label">Branch Head Location<strong style="color:red;">*</strong></label>
						   <?php echo $this->Form->input('branchhead', array('class' => 'form-control','required','label'=>false,'autocomplete'=>'off','type'=>'text')); ?>   
					   
					  
					   </div>

					   <div class="col-sm-3">


					   <label>Course Fee Flexibility</label>
                <span class="field">
                <input type="radio" class="ghj" name="data[User][fee_flex]" value="1" />&nbsp;Yes
                <input type="radio" class="ghj" name="data[User][fee_flex]" value="0" />&nbsp;No
              </span>




					   </div>

					   <div class="col-sm-3">
                           
						   <label for="inputEmail3" class="control-label">Enroll Code<strong style="color:red;">*</strong></label>
						   <?php echo $this->Form->input('enrollcode', array('class' => 'form-control','required','label'=>false,'autocomplete'=>'off','type'=>'text')); ?>   
					   
					  
					   </div>
					   <div class= "col-sm-3">
					   <label>GST Applicable</label>
                <span class="field">
                <input type="radio" class="gh" name="data[User][gstapli]" value="1" />&nbsp;GST
                <input type="radio" class="gh" name="data[User][gstapli]" value="2" />&nbsp;IGST
                <input type="radio" class="gh" name="data[User][gstapli]" checked value="0" />&nbsp;None
              </span>
				    
					
					
			  
					</div>


					<div class = "col-sm-3">


					<label>GST Included</label>
                <span class="field">
                <input type="radio" class="ghj" name="data[User][gstinclu]" value="1" />&nbsp;Yes
                <input type="radio" class="ghj" name="data[User][gstinclu]" value="0" />&nbsp;No
              </span>
				</div>



				<div class="col-sm-3">
                           
						   <label for="inputEmail3" class="control-label">GST Number<strong style="color:red;">*</strong></label>
						   <?php echo $this->Form->input('gstnum', array('class' => 'form-control','required','label'=>false,'autocomplete'=>'off','type'=>'text')); ?>   
					   
					  
					   </div>


      

				<div class="col-sm-3">
				<span class="field">
				<label>Registration Fee</label>
               <input type="radio" class="regf" name="data[User][is_reg]" value="Y" />&nbsp;Yes
                <input type="radio" class="regf" name="data[User][is_reg]" value="N" checked />&nbsp;No
					  
					   </div>

					   <div class="col-sm-3">
                           
						   <label for="inputEmail3" class="control-label">Reg Fees<strong style="color:red;">*</strong></label>
						   <?php echo $this->Form->input('registration_fee', array('class' => 'form-control','required','label'=>false,'autocomplete'=>'off','type'=>'text')); ?>   
					   
					  
					 
                 
					   </div>






					</div>



	
		<div id="fstbl" class="col-sm-6" >
             
			 </div>
			<!-- col-md-3 -->



						
						</div> 
<!-- row -->

<div class="box-footer" id="sub">
						<?php echo $this->Html->link('Back', ['action' => 'index'],['class'=>'btn btn-default']); ?>
						<?php if($newresponse['s_id']){ ?>
							<?php echo $this->Form->submit('Update',array('class' => 'btn btn-info pull-right', 'title' => 'Update')); ?>
							<?php }else{ ?>
							<?php echo $this->Form->submit('Save',array('class' => 'btn btn-info pull-right', 'title' => 'Save')); ?>
							<?php } ?>
						</div>

					
	




					</div> 
					<!--  -->

		</div>

<!-- box-end -->







<?php echo $this->Form->end(); ?>
				</div>
			</div>
		</section>
	</div> 
	<script>
$(document).ready(function(){
  var course_fees;
	$('#course_id').change(function(){
		$('.lds-facebook').show();
		//alert(userbranch);
		var id=$(this).val();
    var degreeCourses=<?php echo json_encode($degrecourseList) ?>;
    console.log(degreeCourses);
    if(Object.values(degreeCourses).includes(id)){
      $('.session').show();
    }else{
      $('.session').hide();
    }
		//alert(id);
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/checkcoursefeeflexibility',
		   type:'POST',
		   data:{'id12': id},
		   success:function(data){
			//alert(data);
			if(data.trim()!='0' && data.trim()!='1'){
       
		var userbranch='<?php echo $this->request->session()->read('Auth.User.branch'); ?>';   
		$('#rfee').val(data);
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/inst',
		   type:'POST',
		   data:{'id12': id, 'branch': userbranch},
		   success:function(data1){
			//alert(data1);
		
			if(data1.trim()=='5'){
				$('#sk').html('<div id="pk" style="color:red;"><label class=" text-right">Installment Type </label> <div class="input text"><span class="field" id="kl"> No Installment Type Added For This Course.</span></div></div>');
				
				$('#sub').hide();
			} else {
			$('#sk').html(data1);
			$('#sub').show();
		}
		   },

			});	   
		} 
		
		if(data.trim()=='1'){
			var userbranch='0';   
		
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/inst',
		   type:'POST',
		   data:{'id12': id, 'branch': userbranch},
		   success:function(data1){
			$('.lds-facebook').hide();
			$('#sk').html(data1);
			
		   },

			});
			
		} 
		
		if(data.trim()=='0'){
	

		$('#sub').hide();	
		$('#sk').html('<div id="pk" style="color:red;"><label class="text-right">Installment Type </label><div class="input text"><span class="field" id="kl"> Course is not approved by Admin.</span></div></div>');
				
				$('#sub').hide();
		} 
		   },

      });
     		
		});
	
	});
</script> 


<script>
$(function() {
$( "#datepicker" ).datepicker();

});
$(function() {
$( "#datepickerbrth" ).datepicker({minDate:null,changeYear:true,changeMonth:true,yearRange:"c-40:c"});

});



</script>


<script>

function getrefdata(data){
var type=data.value;
if(type==11){
  $('#updaterefdetails').css('display','block');
  $('#rname').attr('required',true);
  $('#rmobile').attr('required',true);
   $('#rmobile').val('<?php echo $this->request->data["rmobile"] ?>');

  $('#rname').val('<?php echo $this->request->data["rname"] ?>');
}
else{

  $('#rname').removeAttr('required');
  $('#rmobile').removeAttr('required');
  $('#rmobile').val('');

  $('#rname').val('');
    $('#updaterefdetails').css('display','none');

}
}
</script>
<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
    
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>