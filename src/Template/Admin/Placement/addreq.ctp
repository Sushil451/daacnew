<script type="text/javascript">
  function isLspecial(e) {
   var e = e || window.event;
   var k = e.which || e.keyCode;
   var s = String.fromCharCode(k);
   if(/^[\\\"\'\;\:\>\<\[\]\/\?\=\+\_\|~`!@#\$%^&*\(\)0-9]$/i.test(s)){
     $('#msg').css('display','block');
     return false;
   }
   $('#msg').hide();
 }
</script>

<script type="text/javascript">
  function isCspecial(e) {
   var e = e || window.event;
   var k = e.which || e.keyCode;
   var s = String.fromCharCode(k);
   if(/^[\\\"\'\;\:\>\<\[\]\-\.\,\/\?\=\+\_\|~`!@#\$%^&*\(\)0-9]$/i.test(s)){
    alert("Special characters not acceptable");
    return false;
  }
}
</script>

<style type="text/css">
	.text{color:red; font-size: 12px;}
	.filename{font-size: 11px;color: #e02727;}
	.fr-wrapper>div:first-child>a:first-child{display:none !important;}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
    Company Requirment
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo SITE_URL; ?>admin/placement"><i class="fa fa-home"></i>Home</a></li>
			<li><a href="<?php echo SITE_URL; ?>admin/requirement">Manage Requirment</a></li>
			<?php if(isset($newpack['id'])){ ?>
				<li class="active"><a href="javascript:void(0)">Edit Requirment</a></li>   
			<?php } else { ?>
				<li class="active"><a href="javascript:void(0)">Add Requirment</a></li>
			<?php } ?>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content addCrsMngPg">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<?php echo $this->Flash->render(); ?>
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-plus-square" aria-hidden="true"></i> 
	<?php if(isset($newpack['id'])){ ?> Edit Requirment <?php } else { ?> 	Add Requirment <?php } ?></h3>
					</div>
					<?php echo $this->Form->create($newpack, array(
						'enctype' => 'multipart/form-data',
						'class'=>'needs-validation',
						'novalidate'
						
					));
				
					 ?>
					<div class="box-body addStdntsFrm">

  <div class="row">
  <div class="col-sm-3" >
				
					
					
				<label class="inputEmail3">Company List</label>

		<?php
		
echo $this->Form->input('p_id', array('label'=> false,'class' => 'smallselect form-control','empty'=>'--Select Company--','options' =>$companyList)); 
		?>

		</div>
<div class="col-sm-3" >
   <label for="exampleInputEmail1">Job Posting Date</label>
              
						<div class="input-append date" id="datepickerbrth" data-date="<?php echo date('d-m-Y',strtotime($newpack['created'])); ?>" data-date-format="dd-mm-yyyy"> 
							
              <input class="span2 form-control" size="16" placeholder="Job Posting Date" type="date" name="created" value="<?php echo date('d-m-Y',strtotime($newpack['created'])); ?>">  <span class="add-on"><i class="icon-th"></i></span>
            
            </div>
                	<?php if(isset($newpack['id'])){ ?>
                    <input type="hidden" name="id" value="<?php echo $newpack['id']; ?>">
                   
                    <?php } ?>
    </div>

    </div>
    <div class="row">
 


    <div class="col-sm-6" >
      <label for="exampleInputEmail1">Vacancy</label>
      <?php echo $this->Form->textarea('description', array('class' => 
       'form-control','id'=>'exampleInputEmail3','placeholder'=>'Enter Vacancy','label'=>false,'autocomplete'=>'off','required','style'=>'height:80px;')); ?>
    </div>

   
  </div>
 
  </div>
  <!-- /.box-body -->
          <div class="box-footer">
      <?php
        if(isset($newpack['id'])){
        echo $this->Form->submit(
            'Update', 
            array('class' => 'btn btn-info pull-right', 'title' => 'Update')
        ); }else{ 
        echo $this->Form->submit(
            'Add', 
            array('class' => 'btn btn-info pull-right', 'title' => 'Add')
        );
        }
           ?><?php
      echo $this->Html->link('Back', [
          'action' => 'index'
         
      ],['class'=>'btn btn-default']); ?>
          </div>
          <!-- /.box-footer -->
 <?php echo $this->Form->end(); ?>
          </div>
      
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div> 


  