      <table  class="table table-bordered table-striped afTblStndRept" id="example33" width="100%">
              <thead class="thead-dark">
              <tr>
              <th class="head0" style="text-align:left;">S.No</th>
            <th class="head0" style="width:10%;text-align:left;">Date</th>
            <th class="head0" style="text-align:left;width: 14%;">Company</th>
            <th class="head0" style="text-align:left;width: 10%;">Contact Person</th>
            <th class="head0" style="text-align:left;width: 10%;">Mobile</th>
            <th class="head0" style="text-align:left;">Vacancy</th>
            <th class="head0" style="text-align:left;">Comment</th>
            <th class="head0" style="text-align:left;">Action</th>
                </tr>
                </tr>
              </thead>
              <tbody>
                        <?php if (count($placement) > 0) {?>
                        <?php $cnt = 1;foreach ($placement as $key => $value) {?>
                        <tr class="gradeX">
                          
                        <td  style="text-align:left;"><?php echo $cnt; $cnt++; ?></td>
            <td  style="text-align:left;"><?php echo date('d-m-Y',strtotime($value['created'])); ?> </td>
            <td  style="text-align:left;"><?php echo $value['placement']['company']; ?></td>
            <td  style="text-align:left;"><?php echo $value['placement']['name']; ?></td>
            <td  style="text-align:left;"><?php echo $value['placement']['mobile']; ?></td>
            <td  style="text-align:left;"><?php echo $value['description']; ?></td>
            <td  style="text-align:left;"><?php echo $value['comment']; ?></td>
            
            
			    <?php if($this->request->session()->read("Auth.User.role_id")==1){ ?>
			   <td  style="text-align:left;">
			   <?php echo $this->Html->link(__(''), array('action' => 'deletereq', $value['id']), array('class' => 'fa fa-trash','style'=>'font-size: 20px !important;', 'title' => 'Delete'), __('Are you sure you want to delete # %s?', $value['company'])); ?></td>
			   
			   <?php } ?>
			   
     
     
     
       </tr>
                        <?php }?>
                        <?php } else {?>
                        <tr>
                            <td colspan="7" align="center">No Meta Available</td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
                <?php  //if(count($destination)>10){ //pr($events);?>
                  <?php echo $this->element('admin/pagination'); ?> 
            