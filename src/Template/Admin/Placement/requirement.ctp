<style type="text/css">
   th{
text-align: center;
   }
   td{
    text-align: center;

   }


   .action_icon a span {
    font-size: 14px !important;
}
 .action_icon a {
    font-size: 13px !important;
}
.action_icon b i {
    font-size: 13px !important;
}

.action_icon a {
    display: inline-block;
    width: 21px;
    text-align: center;
}
 </style>
 <div class="content-wrapper">
   <section class="content-header">
<h1> Company Requirement Manager </h1>
    <ol class="breadcrumb">
    <li><a href="<?php echo SITE_URL; ?>admin/dashboards"><i class="fa fa-home"></i>Home</a></li>
<li><a href="<?php echo SITE_URL; ?>admin/requirement">Company Requirement List</a></li>
    </ol> 
    </section> 

    <!-- Main content -->
    <section class="content">
  <div class="row">
    <div class="col-xs-12">    
  <div class="box">
    <div class="box-header">
  <?php echo $this->Flash->render(); ?>            

  <?php $role_id=$this->request->session()->read('Auth.User.role_id'); ?>
            <script>          
              $(document).ready(function () { 
                $("#Mysubscriptions").on("click", function (event) {
                 $('.lds-facebook').show();
                  $.ajax({
                    async:true,
                    data:$("#Stsearch").serialize(),
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>placement/reqsearch",
                    success:function (data) {
                    $('.lds-facebook').hide();   
                      $("#Mycity").html(data); },
                    });
                  return false;
                });
              });


              $(document).ready(function () { 
                $(".branch").bind("change", function (event) {
               $('.lds-facebook').show();
                  $.ajax({
                    async:true,
                    data:$("#Stsearch").serialize(),
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>placement/reqsearch",
                    success:function (data) {
                  $('.lds-facebook').hide();   
                      $("#Mycity").html(data); },
                    });
                  return false;
                });
              });

              $(document).on('click', '.pagination a', function(e) {
                var target = $(this).attr('href');
                var res = target.replace("/placement/reqsearch", "/placement/requirement");
                window.location = res;
                return false;
              });
            </script>

           
            
            <div class="form-group studentDtlFrmGroup" >
            
  <div class="row">
  <div class="col-md-10">
             <?php echo $this->Form->create('Stsearch',array('type'=>'get','inputDefaults'=>array('div'=>false,'label'=>false),'id'=>'Stsearch','class'=>'form-horizontal','method'=>'POST')); 
             
             $tech=array('PHP'=>'PHP','Web Designing'=>'Web Designing','Android'=>'Android','Dot Net'=>'Dot Net','Iphone'=>'Iphone'); ?>

             
             <div class="row">

             <div class="col-sm-2">  
            <?php echo $this->Form->input('company',array('class'=>'form-control branch','label' =>false,'placeholder'=>'Search Company Name..','autocomplete'=>'off')); ?>  
              </div> 
               
           
           <div class="col-sm-2">
              
               <div class="input-append date" id="datepickerbrth" data-date="" data-date-format="dd-mm-yyyy"> 
							<input class="span2 form-control" size="16" placeholder="Exam Date From"  title="Exam Date From"  type="date" name="from_date" value="">  <span class="add-on"><i class="icon-th"></i></span>
						
						</div>
  
              </div> 

              <div class="col-sm-2">
              
              <div class="input-append date" id="datepickerbrth2" data-date="" data-date-format="dd-mm-yyyy"> 
             <input class="span2 form-control"  title="Exam Date To"  size="16" placeholder="Exam Date To" type="date" name="to_date" value="">  <span class="add-on"><i class="icon-th"></i></span>
           
           </div>
 
             </div> 
             <div class="col-sm-1" >
                
                <input type="submit" style="background-color:green;color: #fff;
    margin-top: 5px;
    padding: 6px;" id="Mysubscriptions" class="btn btn-success" value="Search">       
              </div>
            </div>
        



<style>
        .addnn , .addnn:hover{ height:55px; width: 55px; border-radius: 50%; background-color: #2d95e3; line-height: 15px; font-size: 14px !important; text-align: center; color: #fff; display: inline-block; text-decoration: none;}
        .addnn i{ font-size:18px; color:#fff; margin-top:10px;}
        .studentDtlFrmGroup .row{ display:flex; flex-wrap:wrap; width:100%;}
        .studentDtlFrmGroup .row>.col-md-10, .studentDtlFrmGroup .row>.col-md-2{ align-self:center !important;}
        .studentDtlFrmGroup .row>.col-md-10 form{ margin-bottom:0px !important;}

        .btn-default:hover, .btn-default.focus, .btn-default:focus, .btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default.focus:active, .btn-default:active:focus, .btn-default:active:hover{ animation-name: none !important;}
    </style>

            
              
<?php if($this->request->session()->read("Auth.User.role_id")==4){  
  $branchID = $this->request->session()->read("Auth.User.branch");
 echo $this->Form->input('branch_id', array('class' => 'smallselect branch','type' => 'hidden','value'=>$branchID));
}

 ?>
   <?php /*          
              <div class="col-sm-1">
                <label for="inputEmail3" class="control-label" style="color:white">Reset</label>
                <input type="reset" style="background-color:#d33c44;color:#fff;"  class="btn btn-success" value="Reset">       
              </div> 
              <div class="col-sm-1" style="margin-left: -40px;">
                <label for="inputEmail3" class="control-label" style="color:white">Search</label>
                <input type="submit" style="background-color:green;color:#fff;" id="Mysubscriptions" class="btn btn-success" value="Search">       
              </div> */ ?>
              
              <?php echo $this->Form->end(); ?> 
</div>  
 
  <div class="col-md-2 pull-right">
    <ul class="list-inline w-75 m-auto text-center d-flex justify-content-around">
    <li class="list-inline-item ">

           <a class="addnn" href="<?php echo ADMIN_URL; ?>placement/addreq" ><i class="fa fa-user-plus" aria-hidden="true"></i>
     <br> Add</a> 
            </li>

         


            </ul>
  <style>
        .addnn , .addnn:hover{ height:55px; width: 55px; border-radius: 50%; background-color: #2d95e3; line-height: 15px; font-size: 14px !important; text-align: center; color: #fff; display: inline-block; text-decoration: none;}
        .addnn i{ font-size:18px; color:#fff; margin-top:10px;}
        .studentDtlFrmGroup{ display:flex; flex-wrap:wrap;}
        .studentDtlFrmGroup>.col-md-10, .studentDtlFrmGroup>.col-md-2{ align-self:center !important;}
    </style>

            
              

   <?php /*          
              <div class="col-sm-1">
                <label for="inputEmail3" class="control-label" style="color:white">Reset</label>
                <input type="reset" style="background-color:#d33c44;color:#fff;"  class="btn btn-success" value="Reset">       
              </div> 
              <div class="col-sm-1" style="margin-left: -40px;">
                <label for="inputEmail3" class="control-label" style="color:white">Search</label>
                <input type="submit" style="background-color:green;color:#fff;" id="Mysubscriptions" class="btn btn-success" value="Search">       
              </div> */ ?>
              </div>  
              
            
      
       

   
     </div><!-- /.box-header -->
 <div class="box-body" id="Mycity" style="
    width: 100%;
"> 
          
             

      <table  class="table table-bordered table-striped afTblStndRept" id="example33" width="100%">
              <thead class="thead-dark">
              <tr>
              <th class="head0" style="text-align:left;">S.No</th>
            <th class="head0" style="width:10%;text-align:left;">Date</th>
            <th class="head0" style="text-align:left;width: 14%;">Company</th>
            <th class="head0" style="text-align:left;width: 10%;">Contact Person</th>
            <th class="head0" style="text-align:left;width: 10%;">Mobile</th>
            <th class="head0" style="text-align:left;">Vacancy</th>
            <th class="head0" style="text-align:left;">Comment</th>
            <th class="head0" style="text-align:left;">Action</th>
                </tr>
                </tr>
              </thead>
              <tbody>
                        <?php if (count($placement) > 0) {?>
                        <?php $cnt = 1;foreach ($placement as $key => $value) {?>
                        <tr class="gradeX">
                          
                        <td  style="text-align:left;"><?php echo $cnt; $cnt++; ?></td>
            <td  style="text-align:left;"><?php echo date('d-m-Y',strtotime($value['created'])); ?> </td>
            <td  style="text-align:left;"><?php echo $value['placement']['company']; ?></td>
            <td  style="text-align:left;"><?php echo $value['placement']['name']; ?></td>
            <td  style="text-align:left;"><?php echo $value['placement']['mobile']; ?></td>
            <td  style="text-align:left;"><?php echo $value['description']; ?></td>
            <td  style="text-align:left;"><?php echo $value['comment']; ?></td>
            
            
			    <?php if($this->request->session()->read("Auth.User.role_id")==1){ ?>
			   <td  style="text-align:left;">
			   <?php echo $this->Html->link(__(''), array('action' => 'deletereq', $value['id']), array('class' => 'fa fa-trash','style'=>'font-size: 20px !important;', 'title' => 'Delete'), __('Are you sure you want to delete # %s?', $value['company'])); ?></td>
			   
			   <?php } ?>
			   
     
     
     
       </tr>
                        <?php }?>
                        <?php } else {?>
                        <tr>
                            <td colspan="7" align="center">No Meta Available</td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
                <?php  //if(count($destination)>10){ //pr($events);?>
                  <?php echo $this->element('admin/pagination'); ?> 
            </div>
       
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
       </div>
      <!-- /.col -->  
    </div>
     <!-- /.row -->      
  </section>
    <!-- /.content -->  
       
 </div>     
