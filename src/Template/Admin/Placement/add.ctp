<script type="text/javascript">
  function isLspecial(e) {
   var e = e || window.event;
   var k = e.which || e.keyCode;
   var s = String.fromCharCode(k);
   if(/^[\\\"\'\;\:\>\<\[\]\/\?\=\+\_\|~`!@#\$%^&*\(\)0-9]$/i.test(s)){
     $('#msg').css('display','block');
     return false;
   }
   $('#msg').hide();
 }
</script>

<script type="text/javascript">
  function isCspecial(e) {
   var e = e || window.event;
   var k = e.which || e.keyCode;
   var s = String.fromCharCode(k);
   if(/^[\\\"\'\;\:\>\<\[\]\-\.\,\/\?\=\+\_\|~`!@#\$%^&*\(\)0-9]$/i.test(s)){
    alert("Special characters not acceptable");
    return false;
  }
}
</script>

<style type="text/css">
	.text{color:red; font-size: 12px;}
	.filename{font-size: 11px;color: #e02727;}
	.fr-wrapper>div:first-child>a:first-child{display:none !important;}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
    Company Manager
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo SITE_URL; ?>admin/placement"><i class="fa fa-home"></i>Home</a></li>
			<li><a href="<?php echo SITE_URL; ?>admin/placement">Manage Company</a></li>
			<?php if(isset($newpack['id'])){ ?>
				<li class="active"><a href="javascript:void(0)">Edit Company</a></li>   
			<?php } else { ?>
				<li class="active"><a href="javascript:void(0)">Add Company</a></li>
			<?php } ?>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content addCrsMngPg">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<?php echo $this->Flash->render(); ?>
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-plus-square" aria-hidden="true"></i> 
	<?php if(isset($newpack['id'])){ ?> Edit Company <?php } else { ?> 	Add Company <?php } ?></h3>
					</div>
					<?php echo $this->Form->create($newpack, array(
						'enctype' => 'multipart/form-data',
						'class'=>'needs-validation',
						'novalidate'
						
					));
				
					 ?>
					<div class="box-body addStdntsFrm">

  <div class="row">

<div class="col-sm-3" >
   <label for="exampleInputEmail1">Company Name</label>
                <?php echo $this->Form->input('company', array('class' => 
                'form-control','id'=>'exampleInputEmail1','placeholder'=>'Company Name','label'=>false,'autocomplete'=>'off','required')); ?>
                	<?php if(isset($newpack['id'])){ ?>
                    <input type="hidden" name="id" value="<?php echo $newpack['id']; ?>">
                    <?php } ?>
    </div>




    <div class="col-sm-3" >
     <label for="exampleInputEmail1">Number of Employees</label>
     <?php $empl=array('<5'=>'<5','<10'=>'<10','<20'=>'<20','<50'=>'<50','>50'=>'>50'); ?>
                <?php echo $this->Form->input('noe', array('class' => 
                'form-control','id'=>'exampleInputEmail2','options'=>$empl,'placeholder'=>'Page Location','label'=>false,'autocomplete'=>'off','required')); ?>
    </div>


    <div class="col-sm-3" >
      <label for="exampleInputEmail1">Contact Person</label>
                <?php echo $this->Form->input('name', array('class' => 
                'form-control','id'=>'exampleInputEmail3','placeholder'=>'Contact Person','label'=>false,'autocomplete'=>'off','required')); ?>
    </div>


    <div class="col-sm-3" >
        <label for="exampleInputPassword1">Mobile</label>
      <?php echo $this->Form->input('mobile', array('class' => 
                'form-control','placeholder'=>'Enter Mobile','onkeypress'=>'return isNumber()','label'=>false,'type'=>'text','autocomplete'=>'off','required')); ?>
                <h5 id="msg" style="display:none;" class="text">**Special characters not acceptable</h5>
    </div>
    <script>
function isNumber(evt) {

evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;
if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {

return false;
}
return true;
}

</script>
    <div class="col-sm-3" >
        <label for="exampleInputPassword1">Landline</label>
       <?php echo $this->Form->input('landline', array('class' => 
                'form-control','placeholder'=>'Enter Landline','onkeypress'=>'return isNumber()','label'=>false,'type'=>'text','autocomplete'=>'off','required')); ?>
              </div>
   

    <div class="col-sm-3" >
                <label for="exampleInputPassword1">Email</label>
                <?php echo $this->Form->input('email', array('class' => 
                'form-control','placeholder'=>'Enter Email','label'=>false,'type'=>'email','autocomplete'=>'off','required')); ?>
              </div>

    <div class="col-sm-3" >
                <label for="exampleInputPassword1">Technology</label>

                <?php $tech=array('PHP'=>'PHP','Web Designing'=>'Web Designing','Android'=>'Android','Dot Net'=>'Dot Net','Iphone'=>'Iphone');  ?>

                <?php echo $this->Form->input('technology[]', array('class' => 
                'form-select','placeholder'=>'Enter Email','label'=>false,'autocomplete'=>'off','options'=>$tech,'multiple')); ?>
              </div>

              <div class="col-sm-3" >
                <label for="exampleInputPassword1">Address</label>
                <?php echo $this->Form->input('address', array('class' => 
                'form-control','placeholder'=>'Enter Address','label'=>false,'type'=>'textarea','autocomplete'=>'off','required')); ?>
              </div>

              <div class="col-sm-3" >
                <label for="exampleInputPassword1">Special Instruction</label>
                <?php echo $this->Form->input('instr', array('class' => 
                'form-control','placeholder'=>'Enter Special Instruction','label'=>false,'type'=>'textarea','autocomplete'=>'off','required')); ?>
              </div>


              <div class="col-sm-3" >
                <label for="exampleInputPassword1">Company Logo</label>
                <?php echo $this->Form->input('logo', array('class' => 'form-control','required','type'=>'file','label'=>false)); ?></span>
                <span target="_blank" style="color: red; font-size:11px;" align="center"><?php echo " Logo size should be (width:100, height:50)";?></span>
              </div>
   
  </div>
 
  </div>
  <!-- /.box-body -->
          <div class="box-footer">
      <?php
        if(isset($newpack['id'])){
        echo $this->Form->submit(
            'Update', 
            array('class' => 'btn btn-info pull-right', 'title' => 'Update')
        ); }else{ 
        echo $this->Form->submit(
            'Add', 
            array('class' => 'btn btn-info pull-right', 'title' => 'Add')
        );
        }
           ?><?php
      echo $this->Html->link('Back', [
          'action' => 'index'
         
      ],['class'=>'btn btn-default']); ?>
          </div>
          <!-- /.box-footer -->
 <?php echo $this->Form->end(); ?>
          </div>
      
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div> 


  