 <table  class="table table-bordered table-striped afTblStndRept" id="example33" width="100%">
              <thead class="thead-dark">
              <tr>
              <th style="width:3%; text-align:left;">S.No.</th>
              <th style="text-align:left;width: 14%;">Company Name</th>
              <th style="text-align:left;">Contact Person	</th>
                <th style="text-align:left;">Contact 	</th>
               
                <th style="text-align:left;">Technology</th>
                <th style="text-align:left;width: 20%;">Address</th>
                <th style="text-align:left;">Added Date	</th>
                
                
                <th style="text-align:left;"><?= __('Actions') ?></th>
                </tr>
              </thead>
              <tbody>
                        <?php if (count($placement) > 0) {?>
                        <?php $cnt = 1;foreach ($placement as $key => $value) {?>
                        <tr class="gradeX">
                          
             <td><?php echo $cnt; $cnt++; ?></td>
             <td style="text-align:left"><?php echo $value['company']; ?><br>

                                <div class="deliver-buttton">
                                    <div class="dlvr-optn">
                                        <p><?php echo $value['review']; ?></p>
                                        <p><?php echo $value['description']; ?></p>
                                    </div>
                                    <?php  /* if ($value['review'] == '') {?>
                                    <!--<a class='inline' href="#inline_contentdrop<?php //echo $value['Certificate']['cid']; ?>">Pending</a>-->
                                    <a href="#modal-i3-<?php echo $value['id']; ?>" class="inline"
                                        style="color: blue;">Add Testimonial</a>
                                  
                                   

                                    <?php echo "View Testimonial"; ?>
                                    <?php } */ ?>
                                </div>

                            </td>
                            <td style="text-align:left"><?php echo $value['name']; ?></td>
                            <td style="text-align:left"><?php echo $value['mobile']; ?></td>
                            <td style="text-align:left"><?php echo $value['technology']; ?></td>
                            <td style="text-align:left"><?php echo $value['address']; ?></td>
                            <td style="text-align:left">
                                <?php echo date('d-M-Y', strtotime($value['created'])); ?></td>
                            <td>


            <?php  if ($this->request->session()->read('Auth.User.role_id') == 2) {    
              if($product['status']=="Y"){ ?>
      <a href="<?php echo ADMIN_URL; ?>placement/make_supportiv/inactive/<?php echo $product['id'];?>" class="fa fa-circle" title="Active" style="font-size: 20px !important; color:gray;margin-right: 10px;"></a>
<?php }else{ ?>
  <a href="<?php echo ADMIN_URL; ?>placement/make_supportiv/active/<?php echo $product['id'];?>" class="fa fa-circle" title="In Active" style="font-size: 20px !important; color:#48bd4c;margin-right: 10px;"></a>
<?php } } ?>
                                <?php echo $this->Html->link(__(''), array('action' => 'add', $value['id']), array('class' => 'fa fa-pencil-square-o fa-lg', 'title' => 'Edit','style'=>'font-size: 20px !important;')); ?>
                          <?php if($this->request->session()->read('Auth.User.role_id')==1){ ?>
                                <?php echo $this->Html->link(__(''), array('action' => 'delete', $value['id']), array('class' => 'fa fa-trash','style'=>'font-size: 20px !important;', 'title' => 'Delete'), __('Are you sure you want to delete # %s?', $value['company'])); ?><?php }?>
                                
                            </td>

                        </tr>
                        <?php }?>
                        <?php } else {?>
                        <tr>
                            <td colspan="8" align="center">No Meta Available</td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
                
                  <?php echo $this->element('admin/pagination'); ?> 
          