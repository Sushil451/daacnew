<style type="text/css">
  .videoo{margin-left: 65%;margin-top: -26px;color: red;}
</style>

<div class="content-wrapper">
  <section class="content-header">
    <h1>General Settings</h1>
</section>

<?php echo $this->Flash->render();
$user=$this->request->session()->read('Auth.User'); 
?>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-plus-square" aria-hidden="true"></i> <?php if(isset($ntypes['id'])){ echo 'Edit General Settings'; }else{ echo 'Create User';} ?></h3>
        </div>
        <?php echo $this->Form->create($ntypes, array(
          'onsubmit'=>'return registration();',
          'class'=>'form-horizontal',
          'enctype' => 'multipart/form-data',
          'validate'
        )); ?>
        <div class="box-body">

          <?php if($user['role_id']=='2') {?>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Name<span style="color:red;">*</span></label>
              <div class="col-sm-8">
                <?php echo $this->Form->input('name', array('class' => 'form-control','label'=>false,'readonly')); ?>
              </div>
            </div>      

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Mobile<span style="color:red;">*</span></label>
              <div class="col-sm-8">
                <?php echo $this->Form->input('mobile', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Enter Mobile Number','id'=>'phone','onkeypress'=> 'return isNumber(event);','required')); ?>
                <span id="phonemessage" style="color:red; display:none">Mobile Number is already exist !</span> 
              </div>
            </div>      

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Username<span style="color:red;">*</span></label>
              <div class="col-sm-8">
                <?php echo $this->Form->input('email', array('type'=>'text','class' =>'form-control','required','label'=>false,'placeholder'=>'Username','id'=>'myemail','required')); ?>
                <span id="emailar" style="color:red; display:none">Username is already exist !</span>                
              </div>
            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Certificate</label>
              <div class="col-sm-8">
                <?php if($ntypes['image']) {?>
                  <img src="<?php echo SITE_URL; ?>images/certificate/<?php echo $ntypes['image']; ?>" width="90px" height="60px">
                  <a target="_blank" href="<?php echo SITE_URL; ?>images/certificate/<?php echo $ntypes['image']; ?>"><span>Download Certificate</span></a>
                <?php }else{ ?>
                  <img src="<?php echo SITE_URL; ?>img/noimage.jpeg" width="90px" height="60px">
                <?php } ?>           
              </div>
            </div>


          <?php }else {?>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Market Place Name<span style="color:red;">*</span></label>
			  <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="left" title="" data-original-title="The name of the marketplace name. Visitors will see this name." style="margin-top: 11px;"></i>
              <div class="col-sm-8">
                <?php echo $this->Form->input('market_place_name', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Market Place Name','required')); ?>
              </div>
            </div>      

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Slogan</label>
			  <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="left" title="" data-original-title="The tagline that describe your marketplace most" style="margin-top: 11px;"></i>
              <div class="col-sm-8">
                <?php echo $this->Form->input('slogan', array('class' => 'form-control','label'=>false,'placeholder'=>'A nice and meaningful tagline')); ?>
              </div>
            </div>  

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Legal Name<span style="color:red;">*</span></label>
			  <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="left" title="" data-original-title="The legal name of the business" style="margin-top: 11px;"></i>
              <div class="col-sm-8">
                <?php echo $this->Form->input('legal_name', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Legal Name','required')); ?>
              </div>
            </div> 

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Username<span style="color:red;">*</span></label>
              <div class="col-sm-8">
                <?php echo $this->Form->input('email', array('type'=>'text','class' =>'form-control','required','label'=>false,'placeholder'=>'Username','id'=>'myemail','required')); ?>
                <span id="emailar" style="color:red; display:none">Username is already exist !</span>                
              </div>
            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Business Area<span style="color:red;">*</span></label>
			  <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="left" title="" data-original-title="The marketplace business area" style="margin-top: 11px;"></i>
              <div class="col-sm-8">
                <?php $duration = array('1'=>'Worldwide','0'=>'Active business area only'); ?>
                <?php echo $this->Form->input('worldwide_business_area', array('class' => 
                'form-control','id'=>'cat','placeholder'=>'Business Area','label'=>false,'options'=>$duration,'empty'=>'Select','required')); ?>
              </div>
            </div> 

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Time Zone<span style="color:red;">*</span></label>
			  <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="left" title="" data-original-title="This system will use this timezone to operate." style="margin-top: 11px;"></i>
              <div class="col-sm-8">
                <?php echo $this->Form->input('timezone_id', array('class' => 
                'form-control','placeholder'=>'Select','label'=>false,'options'=>$timezones,'empty'=>'--Select--','required')); ?>
              </div>
            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Default Language<span style="color:red;">*</span></label>
			  <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="left" title="" data-original-title="System default language" style="margin-top: 11px;"></i>
              <div class="col-sm-8">
                <?php echo $this->Form->input('language_id', array('class' => 
                'form-control','placeholder'=>'Select','label'=>false,'options'=>$languages,'empty'=>'--Select--','required')); ?>
              </div>
            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Currency<span style="color:red;">*</span></label>
			  <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="left" title="" data-original-title="The marketplace currency" style="margin-top: 11px;"></i>
              <div class="col-sm-8">
                <?php echo $this->Form->input('currency_id', array('class' => 
                'form-control','placeholder'=>'Select','label'=>false,'options'=>$currencies,'empty'=>'--Select--','required')); ?>
              </div>
            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Mobile<span style="color:red;">*</span></label>
              <div class="col-sm-8">
                <?php echo $this->Form->input('mobile', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Enter Mobile Number','id'=>'phone','onkeypress'=> 'return isNumber(event);','required')); ?>
                <span id="phonemessage" style="color:red; display:none">Mobile Number is already exist !</span> 
              </div>
            </div>    

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Instagram<span style="color:red;">*</span></label>
              <div class="col-sm-8">
                <?php echo $this->Form->input('instagram', array('type'=>'text','class' =>'form-control','required','label'=>false,'placeholder'=>'Instagram','required')); ?>             
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Facebook<span style="color:red;">*</span></label>
              <div class="col-sm-8">
                <?php echo $this->Form->input('facebook', array('type'=>'text','class' =>'form-control','required','label'=>false,'placeholder'=>'Facebook','required')); ?>             
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Twitter<span style="color:red;">*</span></label>
              <div class="col-sm-8">
                <?php echo $this->Form->input('twitter', array('type'=>'text','class' =>'form-control','required','label'=>false,'placeholder'=>'Twitter','required')); ?>             
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">YouTube<span style="color:red;">*</span></label>
              <div class="col-sm-8">
                <?php echo $this->Form->input('youtube', array('type'=>'text','class' =>'form-control','required','label'=>false,'placeholder'=>'YouTube','required')); ?>             
              </div>
            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Brand Logo<span style="color:red;">*</span></label>
              <div class="col-sm-8">
                <?php if($ntypes['brand_logo']){ ?>
                  <?php echo $this->Form->input('brand_logo', array('class' =>'form-control','autocomplete' => 'off','label'=>false,'type'=>'file','id'=>'fUpload','onchange'=>'checkextension()')); ?>
                  <img src="<?php echo SITE_URL;?>images/<?php echo $ntypes['brand_logo']; ?>" height="100px" width="100px">
                <?php }else{ ?>
                  <?php echo $this->Form->input('brand_logo', array('class' =>'form-control','autocomplete' => 'off','label'=>false,'type'=>'file','id'=>'fUpload','onchange'=>'checkextension()','required')); ?>
                <?php } ?>           
              </div>
            </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Icon</label>
              <div class="col-sm-8">
                <?php if($ntypes['icon']){ ?>
                  <?php echo $this->Form->input('icon', array('class' =>'form-control','autocomplete' => 'off','label'=>false,'type'=>'file','id'=>'fUpload','onchange'=>'checkextension()')); ?>
                  <img src="<?php echo SITE_URL;?>images/<?php echo $ntypes['icon']; ?>" height="100px" width="100px">
                <?php }else{ ?>
                  <?php echo $this->Form->input('icon', array('class' =>'form-control','autocomplete' => 'off','label'=>false,'type'=>'file','id'=>'fUpload','onchange'=>'checkextension()')); ?>
                <?php } ?>           
              </div>
            </div>

          </div>

        <?php } ?>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Address<span style="color:red;">*</span></label>
          <div class="col-sm-8">
            <?php echo $this->Form->input('address', array('type'=>'text','class' =>'form-control','required','label'=>false,'placeholder'=>'Address','required')); ?>             
          </div>
        </div>


        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label"></label>  
          <div class="col-sm-10">
            <a href="javascript:void(0)" class="chngpassword"  >Do you want to change password ?</a>
          </div>  
        </div>  

        <div class="passdata" style="display:none;">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">New Password<span style="color:red;">*</span></label>
            <div class="col-sm-10">
              <?php echo $this->Form->input('new_password',array('class'=>'form-control input2','placeholder'=>'New Password', 'id'=>'password-field','label' =>false,'autocomplete'=>'off')); ?>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Confirm Password<span style="color:red;">*</span></label>
            <div class="col-sm-10">
              <?php echo $this->Form->input('confirm_passs',array('class'=>'form-control input3','placeholder'=>'Confirm Password', 'id'=>'confirm_pass','label' =>false,'autocomplete'=>'off')); ?>              
            </div>
          </div>  
        </div> 
              <div class="box-footer">
        <?php
        if(isset($ntypes['id'])){
          echo $this->Form->submit(
            'Update', 
            array('class' => 'btn btn-info pull-right formsubmit', 'title' => 'Update')
          ); }else{ 
            echo $this->Form->submit(
              'Add', 
              array('class' => 'btn btn-info pull-right', 'title' => 'Add')
            );
          }
          ?>
        </div>
      </div>
        <?php echo $this->Form->end(); ?>
      </div>

    </div>
</section>
</div> 

<script type="text/javascript">
  $(document).ready(function() {
    $(".formsubmit").click(function(){
      var oldpass = $('.input1').val();
      var newpass = $('.input2').val();
      var confrmpass = $('.input3').val();
      if(newpass && confrmpass){
        if(newpass!=confrmpass){
          alert('New password and confirm password Does Not Match');
          return false;
        }
      }
    });
  });
</script>  

<script>  
  $(document).ready(function(){
    $(".chngpassword").click(function(){
      $(".passdata").toggle();
      var test=$(this).find('.passdata');
      if(!$('#password-field').is(':visible'))
      {
        $('#password-field').prop('required',false);
        $('#confirm_pass').prop('required',false);
        $('#old_password').prop('required',false);
      }
      else{
        $('#password-field').prop('required',true);
        $('#confirm_pass').prop('required',true);
        $('#old_password').prop('required',true);
      }
    });
  });

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
      alert("Please Enter Only Numeric Characters!!!!");
      return false;
    }
    return true;

  }
</script>


