<script>
   function checkextension() {
    var file = document.querySelector("#fUpload");
    if ( /\.(jpe?g|png|gif)$/i.test(file.files[0].name) === false ) 
      { alert("not an image please choose a image!");
    $('#fUpload').val('');
  }
  return false;
}
</script>
<style type="text/css">
 .text{
  color:red; 
  font-size: 12px;
}
</style>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Users Manager
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo SITE_URL; ?>admin/dashboards"><i class="fa fa-home"></i>Home</a></li>
      <li><a href="<?php echo SITE_URL; ?>admin/users">Manage Users</a></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <?php echo $this->Flash->render(); ?>
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-plus-square" aria-hidden="true"></i>Create User</h3>
          </div>

          <?php echo $this->Form->create('usersadd', array(
           'class'=>'form-horizontal',
           'enctype' => 'multipart/form-data',
           'validate'
         )); ?>
         <div class="box-body">
          <div class="form-group">
          <div class="col-sm-3">
          <label for="inputEmail3" class="control-label">Select Branch</label>
          <?php echo $this->Form->input('store_id', array('class' => 
              'form-control','id'=>'exampleInputEmail1','label'=>false,'options'=>$store,'empty'=>'--Select Branch--','required')); ?>
            </div>
            <div class="col-sm-3">
              <label for="inputEmail3" class="control-label">Name</label>
              <?php echo $this->Form->input('name', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Name','autofocus','autocomplete'=>'off')); ?>   
            </div>
            <div class="col-sm-3">
            <label for="inputEmail3" class="control-label">Email</label>
              <?php echo $this->Form->input('email', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Email','autofocus','autocomplete'=>'off')); ?>   
            </div>
            <div class="col-sm-3">
            <label for="inputEmail3" class="control-label">Mobile</label>
              <?php echo $this->Form->input('mobile', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Mobile','autofocus','autocomplete'=>'off','type'=>'number')); ?>   
            </div>
            </div>
            <div class="form-group">
            <div class="col-sm-3">
            <label for="inputEmail3" class="control-label">Password</label>
              <?php echo $this->Form->input('password', array('class' => 'form-control input1','required','label'=>false,'autofocus','autocomplete'=>'off','placeholder'=>'Password')); ?>   
            </div> 

            <div class="col-sm-3">
            <label for="inputEmail3" class="control-label">Confirm Password</label>
              <?php echo $this->Form->input('cpassword', array('class' => 'form-control input2','required','label'=>false,'placeholder'=>'Confirm Password','autofocus','autocomplete'=>'off')); ?>   
            </div>
            <!-- <div class="col-sm-3">
              <label for="inputEmail3" class="control-label">Upload Image</label>
              <?php //echo $this->Form->input('image', array('class' =>'form-control','autocomplete' => 'off','label'=>false,'type'=>'file','id'=>'fUpload','onchange'=>'checkextension()','required')); ?>
            </div> -->
            </div>
          </div> 
        </div>
        <div class="box-footer">
          <?php echo $this->Form->submit('Add', 
        array('class' => 'btn btn-info pull-right formsubmit', 'title' => 'Add')); ?>
        
        <?php
            echo $this->Html->link('Back', [
              'action' => 'index'
              
            ],['class'=>'btn btn-default']); ?>
          </div>
          <?php echo $this->Form->end(); ?>
        </div>
      </div>
    </div>
  </section>
</div> 

<script type="text/javascript">
 $(document).ready(function() {
  $(".formsubmit").click(function(){
  var newpass = $('.input1').val();
  var confrmpass = $('.input2').val();
  if(newpass && confrmpass){
if(newpass!=confrmpass){
    alert('New password and confirm password Does Not Match');
    return false;
  }
}
 });
});
</script> 