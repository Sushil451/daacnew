<script type="text/javascript">
  function isLspecial(e) {
   var e = e || window.event;
   var k = e.which || e.keyCode;
   var s = String.fromCharCode(k);
   if(/^[\\\"\'\;\:\>\<\[\]\/\?\=\+\_\|~`!@#\$%^&*\(\)0-9]$/i.test(s)){
     $('#msg').css('display','block');
     return false;
   }
   $('#msg').hide();
 }
</script>

<script type="text/javascript">
  function isCspecial(e) {
   var e = e || window.event;
   var k = e.which || e.keyCode;
   var s = String.fromCharCode(k);
   if(/^[\\\"\'\;\:\>\<\[\]\-\.\,\/\?\=\+\_\|~`!@#\$%^&*\(\)0-9]$/i.test(s)){
    alert("Special characters not acceptable");
    return false;
  }
}
</script>

<style type="text/css">
	.text{color:red; font-size: 12px;}
	.filename{font-size: 11px;color: #e02727;}
	.fr-wrapper>div:first-child>a:first-child{display:none !important;}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Seo Manager
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo SITE_URL; ?>admin/seo"><i class="fa fa-home"></i>Home</a></li>
			<li><a href="<?php echo SITE_URL; ?>admin/seo">Manage Seo</a></li>
			<?php if(isset($newpack['id'])){ ?>
				<li class="active"><a href="javascript:void(0)">Edit Seo</a></li>   
			<?php } else { ?>
				<li class="active"><a href="javascript:void(0)">Add Seo</a></li>
			<?php } ?>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content addCrsMngPg">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<?php echo $this->Flash->render(); ?>
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-plus-square" aria-hidden="true"></i> 
	<?php if(isset($newpack['id'])){ ?> Edit Seo <?php } else { ?> 	Add Seo <?php } ?></h3>
					</div>
					<?php echo $this->Form->create($newpack, array(
						'enctype' => 'multipart/form-data',
						'class'=>'needs-validation',
						'novalidate'
						
					));
				
					 ?>
					<div class="box-body addStdntsFrm">

  <div class="row">

<div class="col-sm-3" >
   <label for="exampleInputEmail1">Page Name</label>
                <?php echo $this->Form->input('page', array('class' => 
                'form-control','id'=>'exampleInputEmail1','placeholder'=>'Page','label'=>false,'autocomplete'=>'off','onkeypress'=>'return isCspecial()','required')); ?>
    </div>




    <div class="col-sm-3" >
     <label for="exampleInputEmail1">Page Location</label>
                <?php echo $this->Form->input('location', array('class' => 
                'form-control','id'=>'exampleInputEmail1','type'=>'url','placeholder'=>'Page Location','label'=>false,'autocomplete'=>'off','required')); ?>
    </div>


    <div class="col-sm-3" >
      <label for="exampleInputEmail1">Title</label>
                <?php echo $this->Form->input('title', array('class' => 
                'form-control','id'=>'exampleInputEmail1','placeholder'=>'Title','label'=>false,'autocomplete'=>'off','onkeypress'=>'return isCspecial()','required')); ?>
    </div>


    <div class="col-sm-6" >
        <label for="exampleInputPassword1">Keywords</label>
                <?php echo $this->Form->input('keyword', array('class' => 
                'form-control','placeholder'=>'Keywords','label'=>false,'type'=>textarea,'autocomplete'=>'off','onkeypress'=>'return isLspecial()','required','style'=>'height:60')); ?>
                <h5 id="msg" style="display:none;" class="text">**Special characters not acceptable</h5>
    </div>

    <div class="col-sm-6" >
                <label for="exampleInputPassword1">Description</label>
                <?php echo $this->Form->input('description', array('class' => 
                'form-control','placeholder'=>'Description','label'=>false,'type'=>textarea,'autocomplete'=>'off','required','style'=>'height:60')); ?>
              </div>
   

    <div class="col-sm-6" >
                <label for="exampleInputPassword1">Page Schema</label>
                <?php echo $this->Form->input('page_schema', array('class' => 
                'form-control','placeholder'=>'Page Schema','label'=>false,'type'=>'textarea','autocomplete'=>'off','required','style'=>'height:60')); ?>
              </div>
   
  </div>
 
  </div>
  <!-- /.box-body -->
          <div class="box-footer">
      <?php
        if(isset($companies['id'])){
        echo $this->Form->submit(
            'Update', 
            array('class' => 'btn btn-info pull-right', 'title' => 'Update')
        ); }else{ 
        echo $this->Form->submit(
            'Add', 
            array('class' => 'btn btn-info pull-right', 'title' => 'Add')
        );
        }
           ?><?php
      echo $this->Html->link('Back', [
          'action' => 'index'
         
      ],['class'=>'btn btn-default']); ?>
          </div>
          <!-- /.box-footer -->
 <?php echo $this->Form->end(); ?>
          </div>
      
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div> 


  