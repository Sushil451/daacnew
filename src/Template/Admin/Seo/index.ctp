<style type="text/css">
   th{
text-align: center;
   }
   td{
    text-align: center;

   }


   .action_icon a span {
    font-size: 14px !important;
}
 .action_icon a {
    font-size: 13px !important;
}
.action_icon b i {
    font-size: 13px !important;
}

.action_icon a {
    display: inline-block;
    width: 21px;
    text-align: center;
}
 </style>
 <div class="content-wrapper">
   <section class="content-header">
    <h1>
         SEO Manager
    </h1>
    <ol class="breadcrumb">
    <li><a href="<?php echo SITE_URL; ?>admin/dashboards"><i class="fa fa-home"></i>Home</a></li>
<li><a href="<?php echo SITE_URL; ?>admin/seo">SEO List</a></li>
    </ol> 
    </section> <!-- content header -->

    <!-- Main content -->
    <section class="content">
  <div class="row">
    <div class="col-xs-12">    
  <div class="box">
    <div class="box-header">
  <?php echo $this->Flash->render(); ?>            

  <?php $role_id=$this->request->session()->read('Auth.User.role_id'); ?>
            <script>          
              $(document).ready(function () { 
                $(".Stsearchs").bind("change", function (event) {
                 $('.lds-facebook').show();
                  $.ajax({
                    async:true,
                    data:$("#Stsearch").serialize(),
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>seo/search",
                    success:function (data) {
                    $('.lds-facebook').hide();   
                      $("#Mycity").html(data); },
                    });
                  return false;
                });
              });


              $(document).ready(function () { 
                $(".branch").bind("change", function (event) {
               $('.lds-facebook').show();
                  $.ajax({
                    async:true,
                    data:$("#Stsearch").serialize(),
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>seo/search",
                    success:function (data) {
                  $('.lds-facebook').hide();   
                      $("#Mycity").html(data); },
                    });
                  return false;
                });
              });

              $(document).on('click', '.pagination a', function(e) {
                var target = $(this).attr('href');
                var res = target.replace("/seo/search", "/seo");
                window.location = res;
                return false;
              });
            </script>

           
            
            <div class="form-group studentDtlFrmGroup" >
            
            <div class="row">
  <div class="col-md-10">
             <?php echo $this->Form->create('Stsearch',array('type'=>'get','inputDefaults'=>array('div'=>false,'label'=>false),'id'=>'Stsearch','class'=>'form-horizontal','method'=>'POST')); 
             
             $types=['1'=>'Static Pages','3'=>'Course Pages','2'=>'Locality Pages']; ?>

             
             <div class="row">
             <div class="col-sm-2">

            <?php echo $this->Form->input('type', array('class' => 
             'form-control branch','id'=>'course_id','label'=>false,'options'=>$types,'empty'=>'--Select Type--','autofocus')); ?>
           </div>
           <script>
$(document).ready(function(){
  var course_fees;
	$('#course_id').change(function(){
    $('#course_idupdate').hide();
    $('#course_idupdate').html('');
		//alert(userbranch);
		var id=$(this).val();
    if(id!=1){
		//alert(id);
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Seo/findcourseid',
		   type:'POST',
		   data:{'orgid': id},
		   success:function(data){
        $('#course_idupdate').show();
        $('#course_idupdate').html(data);
			
		   },

      });

      }
     		
		});
	
	});
</script> 
           <div class="col-sm-2" id="course_idupdate" style="display:none;">
            
         
           </div>
            

               <div class="col-sm-2">
               <?php  $fee_op=array('vidyadharnagar'=>'Vidyadhar Nagar','c-scheme'=>'C-Scheme','vaishalinagar'=>'Vaishali Nagar','mansarover'=>'Mansarover','banipark'=>'Banipark','murlipura'=>'Murlipura','jhotwara'=>'Jhotwara');
		
		?>


            <?php echo $this->Form->input('name',array('class'=>'form-control branch','label' =>false,'options'=>$fee_op,'empty'=>'--Select Locality--','title'=>'Search Locality Here..','autocomplete'=>'off')); ?>  
              </div> 


              <div class="col-sm-2">  
            <?php echo $this->Form->input('keyword',array('class'=>'form-control branch','label' =>false,'placeholder'=>'Search Keyword..','autocomplete'=>'off')); ?>  
              </div> 
              


              <div class="col-sm-2">
      <?php $seostatus=array('N'=>'Pending','Y'=>'Completed'); 	?>

  <?php echo $this->Form->input('status',array('class'=>'form-control branch','label' =>false,'options'=>$seostatus,'empty'=>'--Select Status--','title'=>'Search Status Here..','autocomplete'=>'off')); ?>  
              </div> 
            </div>
        



<style>
        .addnn , .addnn:hover{ height:55px; width: 55px; border-radius: 50%; background-color: #2d95e3; line-height: 15px; font-size: 14px !important; text-align: center; color: #fff; display: inline-block; text-decoration: none;}
        .addnn i{ font-size:18px; color:#fff; margin-top:10px;}
        .studentDtlFrmGroup .row{ display:flex; flex-wrap:wrap; width:100%;}
        .studentDtlFrmGroup .row>.col-md-10, .studentDtlFrmGroup .row>.col-md-2{ align-self:center !important;}
        .studentDtlFrmGroup .row>.col-md-10 form{ margin-bottom:0px !important;}

        .btn-default:hover, .btn-default.focus, .btn-default:focus, .btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default.focus:active, .btn-default:active:focus, .btn-default:active:hover{ animation-name: none !important;}
    </style>

            
              
<?php if($this->request->session()->read("Auth.User.role_id")==4){  
  $branchID = $this->request->session()->read("Auth.User.branch");
 echo $this->Form->input('branch_id', array('class' => 'smallselect branch','type' => 'hidden','value'=>$branchID));
}

 ?>
   <?php /*          
              <div class="col-sm-1">
                <label for="inputEmail3" class="control-label" style="color:white">Reset</label>
                <input type="reset" style="background-color:#d33c44;color:#fff;"  class="btn btn-success" value="Reset">       
              </div> 
              <div class="col-sm-1" style="margin-left: -40px;">
                <label for="inputEmail3" class="control-label" style="color:white">Search</label>
                <input type="submit" style="background-color:green;color:#fff;" id="Mysubscriptions" class="btn btn-success" value="Search">       
              </div> */ ?>
              
              <?php echo $this->Form->end(); ?> 
</div>  
 
  <div class="col-md-2 pull-right">
    <ul class="list-inline w-75 m-auto text-center d-flex justify-content-around">
    <li class="list-inline-item ">

         <?php $getsettings=$this->Comman->getsettings(); ?>   <a class="addnn" <?php if($getsettings['id']) {  ?> href="<?php echo ADMIN_URL; ?>seo/settings/<?php echo $getsettings['id']; ?>" <?php }else{ ?> href="<?php echo ADMIN_URL; ?>seo/settings"  <?php } ?> ><i class="fa fa-user-plus" aria-hidden="true"></i>
     <br> Settings</a> 
            </li>

         


            </ul>
  <style>
        .addnn , .addnn:hover{ height:55px; width: 55px; border-radius: 50%; background-color: #2d95e3; line-height: 15px; font-size: 14px !important; text-align: center; color: #fff; display: inline-block; text-decoration: none;}
        .addnn i{ font-size:18px; color:#fff; margin-top:10px;}
        .studentDtlFrmGroup{ display:flex; flex-wrap:wrap;}
        .studentDtlFrmGroup>.col-md-10, .studentDtlFrmGroup>.col-md-2{ align-self:center !important;}
    </style>

            
              

   <?php /*          
              <div class="col-sm-1">
                <label for="inputEmail3" class="control-label" style="color:white">Reset</label>
                <input type="reset" style="background-color:#d33c44;color:#fff;"  class="btn btn-success" value="Reset">       
              </div> 
              <div class="col-sm-1" style="margin-left: -40px;">
                <label for="inputEmail3" class="control-label" style="color:white">Search</label>
                <input type="submit" style="background-color:green;color:#fff;" id="Mysubscriptions" class="btn btn-success" value="Search">       
              </div> */ ?>
              </div>  
              
            
      
       

   
     </div><!-- /.box-header -->
 <div class="box-body" id="Mycity" style="
    width: 100%;
"> 
          
             

      <table  class="table table-bordered table-striped afTblStndRept" id="example33" width="100%">
              <thead class="thead-dark">
              <tr>
              <th style="width:3%; text-align:left;">S.No.</th>
              <th style="text-align:left;">Course / Page Name</th>
              <th style="width: 17%; text-align:left;">Page Title</th>
                <th>Page URL</th>
               
                <th style="width: 23%; text-align:left;">Keywords</th>
                <th style="width: 8%; text-align:left;">Locality</th>
                
                
                <th style="width: 9%; text-align:left;"><?= __('Actions') ?></th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $page = $this->request->params['paging'][$this->request->params['controller']]['page'];
                $limit = $this->request->params['paging'][$this->request->params['controller']]['perPage'];
                $counter = ($page * $limit) - $limit + 1;
                if(isset($seo) && !empty($seo)){ 
                foreach($seo as $value){//pr($value);die;
                  ?>
                  <tr>
                  <td  style="text-align:left;"><?php echo  $counter; ?></td>
                  <td  style="text-align:left;">
                  <?php if($value['orgid']!=1){ $crs=$this->Comman->crs($value['course_id']);
                      
                      if($crs['id']){
echo $crs['cname'];

                      }else{ ?>
                      --
                   <?php } }
                   
                   if($value['orgid']==1){
$crs=$this->Comman->findstaticpage($value['course_id']);
if($crs['id']){
  echo $crs['title'];
    }else{ ?>
                        --
                     <?php }

                      } ?>
                  </td><td  style="text-align:left;"><?php echo $value['title']; ?></td>
                  <td  style="text-align:left;"><a target="_blank" href="<?php echo SITE_URL; ?><?php echo $value['location']; ?>"><?php echo strtolower($value['location']); ?></a></td>
            
                  
                    <td  style="text-align:left;">
                  <?php echo $value['keyword']; ?>
                  </td>

                  <td  style="text-align:left;">
                    <?php 
                    
if($value['orgid']==2 && $value['locality']){

echo $value['locality'];

 }else{ ?> -- <?php } ?>
                
                  </td>
                  

                  
                  <td  style="text-align:left;">
                   <?php if($value['status']=='Y'){  ?>
                        <?php  echo $this->Html->link(__(''), ['action' => 'status', $value->id,'N'],array('class'=>'fa fa-circle','title'=>'Active','style'=>'font-size: 20px !important; color:#48bd4c;')) ?>

                     <?php  }else { ?>
                        <?php  echo $this->Html->link(__(''), ['action' => 'status', $value->id,'Y'],array('class'=>'fa fa-circle','title'=>'In Active','style'=>'font-size: 20px !important; color:#c02d2e;')) ?>
                     <?php }  ?>


       <?php  echo $this->Html->link(__(''), ['action' => 'edit', $value->id],array('class'=>'fa fa-pencil-square-o fa-lg','title'=>'Edit','style'=>'font-size: 20px !important; margin-left: 12px;')) ?>
     
                  </td>
                </tr>
                <?php $counter++;} } ?>  
                </tbody>
                </table>
                <?php  //if(count($destination)>10){ //pr($events);?>
                  <?php echo $this->element('admin/pagination'); ?> 
            </div>
       
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
       </div>
      <!-- /.col -->  
    </div>
     <!-- /.row -->      
  </section>
    <!-- /.content -->  
       
 </div>     
