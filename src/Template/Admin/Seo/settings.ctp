<script type="text/javascript">
  function isLspecial(e) {
   var e = e || window.event;
   var k = e.which || e.keyCode;
   var s = String.fromCharCode(k);
   if(/^[\\\"\'\;\:\>\<\[\]\/\?\=\+\_\|~`!@#\$%^&*\(\)0-9]$/i.test(s)){
     $('#msg').css('display','block');
     return false;
   }
   $('#msg').hide();
 }
</script>

<script type="text/javascript">
  function isCspecial(e) {
   var e = e || window.event;
   var k = e.which || e.keyCode;
   var s = String.fromCharCode(k);
   if(/^[\\\"\'\;\:\>\<\[\]\-\.\,\/\?\=\+\_\|~`!@#\$%^&*\(\)0-9]$/i.test(s)){
    alert("Special characters not acceptable");
    return false;
  }
}
</script>

<style type="text/css">
	.text{color:red; font-size: 12px;}
	.filename{font-size: 11px;color: #e02727;}
	.fr-wrapper>div:first-child>a:first-child{display:none !important;}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			SEO Settings Manager
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo SITE_URL; ?>admin/seo"><i class="fa fa-home"></i>Home</a></li>
			<li><a href="<?php echo SITE_URL; ?>admin/seo/settings">Manage SEO Settings</a></li>
			<?php if(isset($newpack['id'])){ ?>
				<li class="active"><a href="javascript:void(0)">Edit SEO Settings</a></li>   
			<?php } else { ?>
				<li class="active"><a href="javascript:void(0)">Add SEO Settings</a></li>
			<?php } ?>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content addCrsMngPg">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<?php echo $this->Flash->render(); ?>
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-plus-square" aria-hidden="true"></i> 
	<?php if(isset($newpack['id'])){ ?> Edit SEO Setting <?php } else { ?> 	Add SEO Setting <?php } ?></h3>
					</div>
					<?php echo $this->Form->create($newpack, array(
						'enctype' => 'multipart/form-data',
						'class'=>'needs-validation',
						'novalidate'
						
					));
				
					 ?>
<div class="box-body addStdntsFrm">
<div class="row mb-3">
<div class="col-sm-2">
  
<?php 
$path=LOCALSEO."sitemap.xml";

if(file_exists($path)){ 
$rtt=1;
}else{

$rtt=0;
}
$path2=LOCALSEO."robots.txt";


if(file_exists($path2)){ 
  $rtts=1;
  }else{
  
  $rtts=0;
  }
//echo $path; die;
 ?>
<label for="exampleInputEmail1">Is Site Map Exist ? <?php if($rtt==1){ ?>
  <a href="<?php echo SITE_URL; ?>sitemap.xml" target="_blank">View XML </a>
  <?php } ?></label>
<div class="col-sm-12 align-self-center">
<div class="form-check form-check-inline">


  <input class="form-check-input" type="radio" name="status" <?php if($rtt==1){ ?>
    checked="checked"
    <?php } ?>  id="inlineRadio1" value="Y">
  <label class="form-check-label" for="inlineRadio1">Yes</label>
  
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="status" <?php if($rtt==0){ ?>
    checked="checked"
    <?php } ?>  id="inlineRadio2" value="N">
  <label class="form-check-label" for="inlineRadio2">No</label>
</div>
  </div>
  </div>
  <div class="col-sm-2" >
      <label for="exampleInputEmail1">Is Robot TXT Exist ? <?php if($rtts==1){ ?><a href="<?php echo SITE_URL; ?>robot.txt" target="_blank">View TXT</a><?php } ?></label>
      <div class="col-sm-12 align-self-center">
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="robot_txt"  <?php if($rtts==1){ ?>
    checked="checked"
    <?php } ?> id="inlineRadio1" value="Y">
    

  <label class="form-check-label" for="inlineRadio1">Yes</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="robot_txt" <?php if($rtts==0){ ?>
    checked="checked"
    <?php } ?> id="inlineRadio2" value="N">
  <label class="form-check-label" for="inlineRadio2">No</label>
</div>
  </div>
  </div>

  <div class="col-sm-2" >
      <label for="exampleInputEmail1">Google Translate Plugin </label>
      <div class="col-sm-12 align-self-center">
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="is_google_translate"  id="inlineRadio1" value="Y">
  <label class="form-check-label" for="inlineRadio1">ON</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="is_google_translate" checked="" id="inlineRadio2" value="N">
  <label class="form-check-label" for="inlineRadio2">OFF</label>
</div>

  </div>
    </div>
    </div>
    
  <div class="row">

<div class="col-sm-6" >
   <label for="exampleInputEmail1">Canonical URL</label>
                <?php echo $this->Form->input('canonical_url', array('class' => 
                'form-control','id'=>'exampleInputEmail1','type'=>'url','placeholder'=>'Enter Canonical URL','label'=>false,'autocomplete'=>'off','required',)); ?>
    </div>


    <div class="col-sm-6" >
     <label for="exampleInputEmail1">Google Analytics</label>
                <?php echo $this->Form->textarea('google_analytics', array('class' => 
                'form-control','id'=>'exampleInputEmail1','placeholder'=>'Enter Google Analytics','label'=>false,'autocomplete'=>'off','required','style'=>'height:100px;')); ?>
    </div>
 
  </div>


  <div class="row">

<div class="col-sm-6" >
   <label for="exampleInputEmail1">FB Pixel</label>
                <?php echo $this->Form->textarea('fb_pixel', array('class' => 
                'form-control','id'=>'exampleInputEmail1','type'=>'url','placeholder'=>'Enter FB Pixel','label'=>false,'autocomplete'=>'off','required','style'=>'height:100px;')); ?>
    </div>

    <div class="col-sm-6" >
     <label for="exampleInputEmail1">Google Tag Script</label>
                <?php echo $this->Form->textarea('g_tag_script', array('class' => 
                'form-control','id'=>'exampleInputEmail1','placeholder'=>'Enter Google Tag Script','label'=>false,'autocomplete'=>'off','required','style'=>'height:100px;')); ?>
    </div>

</div>
 
  </div>
  <!-- /.box-body -->
          <div class="box-footer">
      <?php
        if(isset($newpack['id'])){
        echo $this->Form->submit(
            'Update', 
            array('class' => 'btn btn-info pull-right', 'title' => 'Update')
        ); }else{ 
        echo $this->Form->submit(
            'Add', 
            array('class' => 'btn btn-info pull-right', 'title' => 'Add')
        );
        }
           ?><?php
      echo $this->Html->link('Back', [
          'action' => 'index'
         
      ],['class'=>'btn btn-default']); ?>
          </div>
          <!-- /.box-footer -->
 <?php echo $this->Form->end(); ?>
          </div>
      
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div> 


  