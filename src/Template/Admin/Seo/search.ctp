<table  class="table table-bordered table-striped afTblStndRept" id="example33" width="100%">
              <thead class="thead-dark">
              <tr>
              <th style="width:3%; text-align:left;">S.No.</th>
              <th style="text-align:left;">Course / Page Name</th>
              <th style="width: 17%; text-align:left;">Page Title</th>
                <th>Page URL</th>
               
                <th style="width: 23%; text-align:left;">Keywords</th>
                <th style="width: 8%; text-align:left;">Locality</th>
                
                
                <th style="width: 9%; text-align:left;"><?= __('Actions') ?></th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $page = $this->request->params['paging'][$this->request->params['controller']]['page'];
                $limit = $this->request->params['paging'][$this->request->params['controller']]['perPage'];
                $counter = ($page * $limit) - $limit + 1;
                if(isset($seo) && !empty($seo)){ 
                foreach($seo as $value){//pr($value);die;
                  ?>
                  <tr>
                  <td  style="text-align:left;"><?php echo  $counter; ?></td>
                  <td  style="text-align:left;">
                  <?php if($value['orgid']!=1){ $crs=$this->Comman->crs($value['course_id']);
                      
                      if($crs['id']){
echo $crs['cname'];

                      }else{ ?>
                      --
                   <?php } }
                   
                   if($value['orgid']==1){
$crs=$this->Comman->findstaticpage($value['course_id']);
if($crs['id']){
  echo $crs['title'];
    }else{ ?>
                        --
                     <?php }

                      } ?>
                  </td><td  style="text-align:left;"><?php echo $value['title']; ?></td>
                  <td  style="text-align:left;"><a target="_blank" href="<?php echo SITE_URL; ?><?php echo $value['location']; ?>"><?php echo strtolower($value['location']); ?></a></td>
            
                  
                    <td  style="text-align:left;">
                  <?php echo $value['keyword']; ?>
                  </td>

                  <td  style="text-align:left;">
                    <?php 
                    
if($value['orgid']==2 && $value['locality']){

echo $value['locality'];

 }else{ ?> -- <?php } ?>
                
                  </td>
                  

                  
                  <td  style="text-align:left;">
                   <?php if($value['status']=='Y'){  ?>
                        <?php  echo $this->Html->link(__(''), ['action' => 'status', $value->id,'N'],array('class'=>'fa fa-circle','title'=>'Active','style'=>'font-size: 20px !important; color:#48bd4c;')) ?>

                     <?php  }else { ?>
                        <?php  echo $this->Html->link(__(''), ['action' => 'status', $value->id,'Y'],array('class'=>'fa fa-circle','title'=>'In Active','style'=>'font-size: 20px !important; color:#c02d2e;')) ?>
                     <?php }  ?>


       <?php  echo $this->Html->link(__(''), ['action' => 'edit', $value->id],array('class'=>'fa fa-pencil-square-o fa-lg','title'=>'Edit','style'=>'font-size: 20px !important; margin-left: 12px;')) ?>
     
                  </td>
                </tr>
                <?php $counter++;} } ?>  
                </tbody>
                </table>