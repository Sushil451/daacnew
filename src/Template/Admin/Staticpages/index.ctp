<style type="text/css">
   th{
text-align: center;
   }
   td{
    text-align: center;

   }


   .action_icon a span {
    font-size: 14px !important;
}
 .action_icon a {
    font-size: 13px !important;
}
.action_icon b i {
    font-size: 13px !important;
}

.action_icon a {
    display: inline-block;
    width: 21px;
    text-align: center;
}
 </style>
 <div class="content-wrapper">
   <section class="content-header">
    <h1>
         Static Page Detail
    </h1>
    <ol class="breadcrumb">
    <li><a href="<?php echo SITE_URL; ?>admin/dashboards"><i class="fa fa-home"></i>Home</a></li>
<li><a href="<?php echo SITE_URL; ?>admin/staticpages">Static Page List</a></li>
    </ol> 
    </section> <!-- content header -->

    <!-- Main content -->
    <section class="content">
  <div class="row">
    <div class="col-xs-12">    
  <div class="box">
    <div class="box-header">
  <?php echo $this->Flash->render(); ?>


  <?php $role_id=$this->request->session()->read('Auth.User.role_id'); ?>
            
              
 
  <div class="col-md-2 pull-right">
    <ul class="list-inline w-75 m-auto text-center d-flex justify-content-around">
    <li class="list-inline-item ">

              <a class="addnn" href="<?php echo ADMIN_URL; ?>staticpages/add"><i class="fa fa-user-plus" aria-hidden="true"></i>
     <br> New</a> 
            </li>

         


            </ul>
  <style>
        .addnn , .addnn:hover{ height:55px; width: 55px; border-radius: 50%; background-color: #2d95e3; line-height: 15px; font-size: 14px !important; text-align: center; color: #fff; display: inline-block; text-decoration: none;}
        .addnn i{ font-size:18px; color:#fff; margin-top:10px;}
        .studentDtlFrmGroup{ display:flex; flex-wrap:wrap;}
        .studentDtlFrmGroup>.col-md-10, .studentDtlFrmGroup>.col-md-2{ align-self:center !important;}
    </style>

            
              


   
     </div><!-- /.box-header -->
      <div class="box-body" id="Mycity"> 
             
              <table  class="table table-bordered table-striped afTblStndRept" width="100%">
               
               <thead class="thead-dark">
                <tr>
                  <th>S.No</th>                          
                  <th>Title</th>   
                   <th>Description</th>      
                  <th>Created</th>    
                  <th>Status</th>    
                  <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                $page = $this->request->params['paging'][$this->request->params['controller']]['page'];
                $limit = $this->request->params['paging'][$this->request->params['controller']]['perPage'];
                $counter = ($page * $limit) - $limit + 1;
                if(isset($staticpage) && !empty($staticpage)){ 
                foreach($staticpage as $value){//pr($value);die;
                ?>
                <tr>
                <td><?php echo $counter;?></td>
                <td><?php echo $value['title'];  ?></td>
          <td> 
            <?php $string = strip_tags($value['content']);?>
<p>
  <span id="more<?php echo $value['id']; ?>" style="display: none;"><?php echo $string; ?>
</span>
<a href="javascript:void(0);" id="myBtn<?php echo $value['id']; ?>">View</a>
<a style="display: none;" href="javascript:void(0);" id="less<?php echo $value['id']; ?>">Close</a>
</p></td>

<script type="text/javascript">
$(document).ready(function(){
$('#myBtn<?php echo $value['id']; ?>').click(function(){

     $("#dots<?php echo $value['id']; ?>").css({ display: "none" });  
       $("#more<?php echo $value['id']; ?>").css({ display: "block" });
       $("#myBtn<?php echo $value['id']; ?>").css({ display: "none" });
       $("#less<?php echo $value['id']; ?>").css({ display: "block" });

});
});
</script>

<script type="text/javascript">
$(document).ready(function(){
$('#less<?php echo $value['id']; ?>').click(function(){

     $("#dots<?php echo $value['id']; ?>").css({ display: "block" });
       $("#more<?php echo $value['id']; ?>").css({ display: "none" });
       $("#myBtn<?php echo $value['id']; ?>").css({ display: "block" });
       $("#less<?php echo $value['id']; ?>").css({ display: "none" });

});
});
</script>

                 
                <td><?php echo date('d-M-Y',strtotime($value['created'])); ?></td>
            <td>
              <?php if($value['status']=='Y'){  ?>
                        <?php  echo $this->Html->link(__(''), ['action' => 'status', $value->id,'N'],array('class'=>'fa fa-circle','title'=>'Active','style'=>'font-size: 20px !important; color:#48bd4c;')) ?>

                     <?php  }else { ?>
                        <?php  echo $this->Html->link(__(''), ['action' => 'status', $value->id,'Y'],array('class'=>'fa fa-circle','title'=>'In Active','style'=>'font-size: 20px !important; color:#c02d2e;')) ?>
                        <?php }  ?>
    </td> 
    <td>
     <?php  echo $this->Html->link(__(''), ['action' => 'edit', $value->id,],array('class'=>'fa fa-pencil-square-o fa-lg','title'=>'Edit','style'=>'font-size: 20px !important; margin-left: 12px;')) ?>

                     <?php
                    echo $this->Html->link('', ['action' => 'delete',$value->id],['title'=>'Delete','class'=> 'fa fa-trash','style'=>'color:#FF0000; margin-left: 13px; font-size: 19px !important;',"onClick"=>"javascript: return confirm('Are you sure do you want to delete this Static Page')"]); ?>
      </td>
                </tr>
    <?php $counter++;} } ?>  
    </tbody>
                </table>
                <?php  //if(count($destination)>10){ //pr($events);?>
                  <?php echo $this->element('admin/pagination'); ?> 
            </div>
       
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
       </div>
      <!-- /.col -->  
    </div>
     <!-- /.row -->      
  </section>
    <!-- /.content -->  
       
 </div>      
     <!-- /.   content-wrapper -->  
     
     

