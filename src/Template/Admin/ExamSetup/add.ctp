<link href="https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
<script>
	var SITE_URL='<?php echo SITE_URL; ?>';
$(function() {
$( "#datepickered" ).datepicker();

});

function valid()
{
  if(document.getElementById('ExamSetupBId').value=="")
  {
      alert("Select Batch Code");
      return false;
      
  }
  if(document.getElementById('ExamSetupEName').value=="")
  {
      alert("Enter Exam Name");
      return false;
      
  }    
   if(document.getElementById('ExamSetupECat').value=="")
  {
      alert("Select Category");
      return false;
      
  } 
     if(document.getElementById('cat').value=="")
  {
      alert("Select Exam Type");
      return false;
      
  } 
       if(document.getElementById('datepickered').value=="")
  {
      alert("Select Exam Date");
      return false;
      
  } 
  var onevalue=parseInt($('#one').text());
    if(document.getElementById('1m').value>onevalue)
  {
      alert("Please Add Marks less than avaiable marks");
$('#1m').focus();
      return false;
      
  }
  
    
var onevalue=parseInt($('#two').text());
    if(document.getElementById('2m').value>onevalue)
  {
      alert("Please Add Marks less than avaiable marks");
$('#2m').focus();
      return false;
      
  }  
  
    
var onevalue=parseInt($('#three').text());
    if(document.getElementById('3m').value>onevalue)
  {
          alert("Please Add Marks less than avaiable marks");
	$('#3m').focus();
      return false;
      
  }
  
   
var onevalue=parseInt($('#four').text());
    if(document.getElementById('4m').value>onevalue)
  {
           alert("Please Add Marks less than avaiable marks");
$('#4m').focus();
      return false;
      
  }
	
	
  
  
}
function check()
                {
					var ctype=$('.q_category').val();
					var extype= $('#cat').val();
				    var data = {
       'email': ctype,'extype':extype,
    };
    $.post("<?php echo ADMIN_URL; ?>ExamSetup/get_exams_details", data, function(data) {
        var response = jQuery.parseJSON(data);
        if(response.len===0){
       response['0']['q_mark']=0 ;
      response['1']['q_marks']=0 ;
       response['2']['q_marks']=0 ;
      response['3']['q_marks']=0 ;
        }

		$('#one').html(response['0']['q_marks']);
		$('#two').html(response['1']['q_marks']);
		$('#three').html(response['2']['q_marks']);
		$('#four').html(response['3']['q_marks']);
        
        
          
        
    });
   
              var abc=document.getElementById("cat").value;
               document.getElementById("show").style.display="flex";
              if(abc==0){
                  
				  document.getElementById("edur").value="30";
              }
              else
              {
                document.getElementById("edur").value="60";  
              }
              
            
                }

function searchquestion()
                {
        var code=$('.q_category').val();
				$('#cat').val('');
        $.ajax({
        async: true,
        data:{ code:code},
        dataType: "html",
        type: "POST",
        url: "<?php echo ADMIN_URL; ?>ExamSetup/batchsearch",
        success: function(data) {
          var options=JSON.parse(data);
          $('.batch_code').empty();
          $('.batch_code').attr('disabled',false);
          $('.batch_code').append('<option>Select batch</option>');
          for (var key in options) {
          $('.batch_code').append('<option value='+key+'>'+options[key]+'</option>');
          }
        },


      });
                
      }
</script>


<script>
 $(document).ready(function(){
$('.batch_code').change(function(){
  var code=$(this).val();
  //alert(code);

  $.ajax({
        async: true,
        data:{ code:code},
        dataType: "html",
        type: "POST",


        url: "<?php echo ADMIN_URL; ?>ExamSetup/batchstudents",
        success: function(data) {
          
        if(data){
          $("#show_studentssss").show();

          $("#show_studentssss").html(data);
        }else{
          $("#show_studentssss").hide();

        }
        },


      });
});
 });
 </script>
<style type="text/css">
	.text{color:red; font-size: 12px;}
	.filename{font-size: 11px;color: #e02727;}
	.fr-wrapper>div:first-child>a:first-child{display:none !important;}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1> 
			ExamSetup Manager
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo SITE_URL; ?>admin/students"><i class="fa fa-home"></i>Home</a></li>
			<li><a href="<?php echo SITE_URL; ?>admin/ExamSetup">Manage ExamSetup</a></li>
			<?php if(isset($newresponse['id'])){ ?>
				<li class="active"><a href="javascript:void(0)">Edit ExamSetup</a></li>   
			<?php } else { ?>
				<li class="active"><a href="javascript:void(0)">Add ExamSetup</a></li>
			<?php } ?>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content addCrsMngPg">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<?php echo $this->Flash->render(); ?>
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-plus-square" aria-hidden="true"></i> 
	<?php if(isset($newresponse['id'])){ ?> Edit Exam Setup <?php } else { ?> Add Exam Setup <?php } ?></h3>
					</div>
					<?php echo $this->Form->create($newresponse, array(
						'enctype' => 'multipart/form-data',
						'class'=>'needs-validation',
						'novalidate'
						
					));
				
					 ?>
					<div class="box-body addStdntsFrm">
						<div class="row">

						<div class="col-sm-3" >
				
					
					
				<label class="inputEmail3">Category</label>

	
		 <?php echo $this->Form->input('e_cat', array('class' => 'q_category form-control','empty'=>'Choose Question Category','label'=>false,'options' => $categ,'onchange'=>'return searchquestion();')); ?>

		</div>
				   <div class="col-sm-3">
		          <label for="inputEmail3" class="control-label">Batch Code<strong style="color:red;">*</strong></label>

				  <?php echo $this->Form->input('b_id[]', array('class' => 'smallselect batch_code form-select','label'=>false,'empty'=>'Choose Batch Code','options' => $categ1,'multiple','disabled')); ?>
       		      
				
				</div>
							

						
			
			
			
							<div class="col-sm-3">
	          	<label for="inputEmail3" class="control-label">Exam Name<strong style="color:red;">*</strong></label>
       		      <?php echo $this->Form->input('e_name', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Exam Name','autofocus','autocomplete'=>'off')); ?>   
				
							</div>
			
				<div class="col-sm-3" >
		          <label for="inputEmail3" class="control-label">Exam Type <strong style="color:red;">*</strong></label>
				  <?php echo $this->Form->input('e_type', array('class' => 'smallselect form-control','label'=>false,'empty'=>'Choose Exam Type','options' => array('0'=>'Minor Exam','1'=>'Major Exam','2'=>'Special Exam','3'=>'Android'),'onchange'=>'return check();','id'=>'cat')); ?>


		</div>
		<div id="show_studentssss" style="width:100%;display:none">

                  
                 
                </div>
				
				<div id="show" class="row" style="display: none;">
				<div class="col-sm-3" >
		          <label for="inputEmail3" class="control-label d-flex justify-content-between">1 Marks <strong style="color:red;flex:1">*</strong> 
	<span id="one" style="color: #19a6db !important;font-weight: bold;
    font-size: 14px !important;">0</span> </label>
				  <?php echo $this->Form->input('1mark', array('class' =>'form-control marks_cat','id'=>'1m','type'=>'text','label'=>false,'required')); ?>


		</div>	

		<div class="col-sm-3" >
		          <label for="inputEmail3" class="control-label d-flex justify-content-between">2 Marks <strong style="color:red;flex:1">*</strong> 
	<span id="two" style="color: #19a6db !important;font-weight: bold;
    font-size: 14px !important;">0</span> </label>
				  <?php echo $this->Form->input('2mark', array('class' => 'form-control marks_cat','id'=>'2m','label'=>false,'type'=>'text','required')); ?>	


		</div>

		<div class="col-sm-3" >
		          <label for="inputEmail3" class="control-label  d-flex justify-content-between">3 Marks <strong style="color:red;flex:1">*</strong> 
	<span id="three" style="color: #19a6db !important;font-weight: bold;
    font-size: 14px !important;">0</span> </label>
				  <?php echo $this->Form->input('3mark', array('class' => 'form-control marks_cat','id'=>'3m','type'=>'text','label'=>false,'required')); ?>	


		</div>

		<div class="col-sm-3" >
		          <label for="inputEmail3" class="control-label  d-flex justify-content-between">4 Marks <strong style="color:red;flex:1">*</strong> 
	<span id="four" style="color: #19a6db !important;font-weight: bold;
    font-size: 14px !important;">0</span></label>
				  <?php echo $this->Form->input('4mark', array('class' => 'form-control marks_cat','id'=>'4m','type'=>'text','label'=>false,'required')); ?>
		</div>

						</div>


						<div class="col-sm-3" >
		          <label for="inputEmail3" class="control-label">Exam Date <strong style="color:red;">*</strong></label>

				  <div class="input-append date" id="datepickerbrth" data-date="<?php echo date('d-m-Y',strtotime($newresponse['e_date'])); ?>" data-date-format="dd-mm-yyyy"> 
							<input class="span2 form-control" size="16" placeholder="Birthdate Date" type="date" name="e_date" value="<?php echo date('d-m-Y',strtotime($newresponse['e_date'])); ?>">  <span class="add-on"><i class="icon-th"></i></span>
						
						</div>
				


		</div>	

		<div class="col-sm-3" >
		          <label for="inputEmail3" class="control-label">Exam Duration <strong style="color:red;">*</strong></label>
				  <?php echo $this->Form->input('e_duration', array('class' =>'form-control','type'=>'text','id'=>'edur','label'=>false,'required')); ?>


		</div>	

		<div class="col-sm-3" >
		          <label for="inputEmail3" class="control-label">Pass Percentage <strong style="color:red;">*</strong></label>
				  <?php echo $this->Form->input('pass_perc', array('class' =>'form-control','type'=>'text','id'=>'pass_perc','label'=>false,'required')); ?>


		</div>	
						</div>
	
	
				</div>

				<!-- row -->
						<div class="addStdntsFrm addStdntsFrmBtns ">
						<div class="box-footer" id="sub">
						<?php echo $this->Html->link('Back', ['action' => 'index'],['class'=>'btn btn-default']); ?>
							<?php echo $this->Form->submit('Save',array('class' => 'btn btn-info pull-right', 'title' => 'Save')); ?>
						
						</div>
				</div>
						<!--  -->
		</div>
				</div>

		
				</div>















				
		
						
						<?php echo $this->Form->end(); ?>
					</div> 
				</div>
			</div>
		</section>
	</div> 
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/js/froala_editor.pkgd.min.js"></script>

<!-- Initialize the editor. -->
 <script>
  new FroalaEditor('#edit', {
    // Set the file upload URL.
    imageUploadURL: '<?php echo SITE_URL; ?>upload_image.php',
    imageUploadParams: {
      id: 'my_editor'
    }
  })
</script>
	<script>
$(document).ready(function(){
  var course_fees;
	$('#course_id').change(function(){
		
		//alert(userbranch);
		var id=$(this).val();
    var degreeCourses=<?php echo json_encode($degrecourseList) ?>;
    console.log(degreeCourses);
    if(Object.values(degreeCourses).includes(id)){
      $('.session').show();
    }else{
      $('.session').hide();
    }
		//alert(id);
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/checkcoursefeeflexibility',
		   type:'POST',
		   data:{'id12': id},
		   success:function(data){
			//alert(data);
			if(data.trim()!='0' && data.trim()!='1'){
       
		var userbranch='<?php echo $this->request->session()->read('Auth.User.branch'); ?>';   
		$('#rfee').val(data);
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/inst',
		   type:'POST',
		   data:{'id12': id, 'branch': userbranch},
		   success:function(data1){
			//alert(data1);
		
			if(data1.trim()=='5'){
				$('#sk').html('<div id="pk" style="color:red;"><label class=" text-right">Installment Type </label> <div class="input text"><span class="field" id="kl"> No Installment Type Added For This Course.</span></div></div>');
				
				$('#sub').hide();
			} else {
			$('#sk').html(data1);
			$('#sub').show();
		}
		   },

			});	   
		} 
		
		if(data.trim()=='1'){
			var userbranch='0';   
		
		$.ajax({
		   url:'<?php echo SITE_URL; ?>admin/Students/inst',
		   type:'POST',
		   data:{'id12': id, 'branch': userbranch},
		   success:function(data1){
			//alert(data1);
			$('#sk').html(data1);
			
		   },

			});
			
		} 
		
		if(data.trim()=='0'){
	

		$('#sub').hide();	
		$('#sk').html('<div id="pk" style="color:red;"><label class="text-right">Installment Type </label><div class="input text"><span class="field" id="kl"> Course is not approved by Admin.</span></div></div>');
				
				$('#sub').hide();
		} 
		   },

      });
     		
		});
	
	});
</script> 
<script>
$(function() {
$( "#datepicker" ).datepicker();

});
$(function() {
$( "#datepickerbrth" ).datepicker({minDate:null,changeYear:true,changeMonth:true,yearRange:"c-40:c"});

});



</script>


<script>

function getrefdata(data){
var type=data.value;
if(type==11){
  $('#updaterefdetails').css('display','block');
  $('#rname').attr('required',true);
  $('#rmobile').attr('required',true);
   $('#rmobile').val('<?php echo $this->request->data["rmobile"] ?>');

  $('#rname').val('<?php echo $this->request->data["rname"] ?>');
}
else{

  $('#rname').removeAttr('required');
  $('#rmobile').removeAttr('required');
  $('#rmobile').val('');

  $('#rname').val('');
    $('#updaterefdetails').css('display','none');

}
}
</script>
<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
    
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>

<script>
function isNumber(evt) {

evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;
if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {

return false;
}
return true;
}

</script>