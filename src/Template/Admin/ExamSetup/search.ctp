
          
             

      <table  class="table table-bordered table-striped afTblStndRept" id="example33" width="100%">
               
                    <thead class="thead-dark">
                    <tr>
                            <th >No.</th>
                            <th style="text-align:left;">Batch Code</th>
                            <th style="text-align:left;">Course Category</th>
                            <th style="text-align:left;">Exam Name</th>
                            <th style="text-align:left;">Exam Type</th>
                            
                            <th style="text-align:left;">Exam Date</th>
                            <th style="text-align:left;">Question (1 Mark)</th>
                            <th style="text-align:left;">Question (2 Mark)</th>
                            <th style="text-align:left;">Question (3 Mark)</th>
                            <th style="text-align:left;">Question (4 Mark)</th>
                            <th style="text-align:left;">Details</th>
                                                    
                            <th style="text-align:left;">Action</th>
                       
                        </tr>
                    </thead>
                    <tbody>
                  


                 <?php if(count($destination)>0){ $count=1;?>
                        <?php foreach($destination as $key=>$value) {
                      
                            $atz=$value['1mark'];
                            $atz1=$value['2mark'];
                            $atz2=$value['3mark'];
                            $atz3=$value['5mark'];
                            $tot=($atz+$atz1+$atz2+$atz3);
                            $tm=$atz*1+$atz1*2+$atz2*3+$atz3*4;

                            ?>

                           
                            <tr class="gradeX">
            <td style="text-align:left;"><?php echo $count++; ?></td>
                  <td style="text-align:left;"><?php echo $value['batch']['batch_code']; ?></td>
       <td style="text-align:left;"><?php echo $value['question_category']['qc_name']; ?></td>
                   <td style="text-align:left;"><a href="<?php echo SITE_URL; ?>admin/ExamSetup/view_examdetail/<?php echo $value['e_id']; ?>" target="_blank"><?php echo $value['e_name']; ?></a></td>
                               <td style="text-align:left;"><?php

                               if($value['e_type']==1)
                               {
                                echo("Major Exam");
                            }
                            else if($value['e_type']==0)
                            {
                               echo("Minor Exam");
                           }
                           else
                           {
                               echo("Special Exam");
                           }	

                           ?></td>

                           <td style="text-align:left;"><?php echo date("jS F, Y", strtotime($value['e_date']));  ?></td>

                           <td style="text-align:left;"><?php echo $value['1mark'];  ?></td>
                           <td style="text-align:left;"><?php echo $value['2mark'];  ?></td>
                           <td style="text-align:left;"><?php echo $value['3mark'];  ?></td>
                           <td style="text-align:left;"><?php if($value['4mark']){ echo $value['4mark']; }else{ echo 0; }  ?></td>
                           <td style="text-align:left;"><?php echo ("<b>Exam Duration :-</b>").$value['e_duration'].("<br> <b>Total Questions:-</b>").$tot.("<br> <b>Total Marks:-</b>").$tm ; ?></td>
                           <!-- <td> <a href="">Result </a> </td>-->
                           <td style="text-align:left;">


                            <?php
                            if($value['status']!=1)
                            {
                                echo("Exam Not Yet Held<br>"); 
                                if($this->request->session()->read('Auth.User.role_id')==1)  {
                                 
       echo $this->Html->link(__(''), array('action' => 'edit', $value['e_id']), array('class'=>'fa fa-pencil-square-o fa-lg','title'=>'Edit','style'=>'font-size: 20px !important; margin-left: 12px;'));


     
     echo $this->Html->link('', ['action' => 'delete',$value->e_id],['title'=>'Delete','class'=> 'fa fa-trash','style'=>'color:#FF0000; margin-left: 13px; font-size: 19px !important;',"onClick"=>"javascript: return confirm('Are you sure do you want to delete this Exam Setup')"]); 
                                }
                            }
                            else
                                { ?>

                                  <a target="_blank" href="<?php echo ADMIN_URL;?>ExamSetup/viewpdf/<?php echo $value['e_id'] ?>/<?php echo $value['batch']['batch_code']; ?>">View Result<br></a> 

                                <?php  } ?>

                          </td>

                      </tr>
                  <?php } ?>
              <?php } else{  ?>    
                <tr><td colspan="12" align="center">No Meta Available</td></tr>
            <?php } ?>
                       
                    </tbody>
                </table>
                <?php  //if(count($destination)>10){ //pr($events);?>
                  <?php echo $this->element('admin/pagination'); ?> 
           