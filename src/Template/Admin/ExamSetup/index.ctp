<style type="text/css">
   th{
text-align: center;
   }
   td{
    text-align: center;

   }


   .action_icon a span {
    font-size: 14px !important;
}
 .action_icon a {
    font-size: 13px !important;
}
.action_icon b i {
    font-size: 13px !important;
}

.action_icon a {
    display: inline-block;
    width: 21px;
    text-align: center;
}
 </style>
 <div class="content-wrapper">
   <section class="content-header">
    <h1>
    Examination Setup 
    </h1>
    <ol class="breadcrumb">
    <li><a href="<?php echo SITE_URL; ?>admin/dashboards"><i class="fa fa-home"></i>Home</a></li>
<li><a href="<?php echo SITE_URL; ?>admin/exam">Exam List</a></li>
    </ol> 
    </section> <!-- content header -->

    <section class="content">
  <div class="row">
    <div class="col-xs-12">    
  <div class="box">
    <div class="box-header">
  <?php echo $this->Flash->render(); ?>

  <?php $role_id=$this->request->session()->read('Auth.User.role_id'); ?>
            <script>          
              $(document).ready(function () { 
                $("#Mysubscriptions").on("click", function (event) {
                 $('.lds-facebook').show();
                  $.ajax({
                    async:true,
                    data:$("#Stsearch").serialize(),
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>ExamSetup/search",
                    success:function (data) {
                    $('.lds-facebook').hide();   
                      $("#Mycity").html(data); },
                    });
                  return false;
                });
              });


              $(document).ready(function () { 
                $(".branch").bind("change", function (event) {
               $('.lds-facebook').show();
                  $.ajax({
                    async:true,
                    data:$("#Stsearch").serialize(),
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>ExamSetup/search",
                    success:function (data) {
                  $('.lds-facebook').hide();   
                      $("#Mycity").html(data); },
                    });
                  return false;
                });
              });

              $(document).on('click', '.pagination a', function(e) {
                var target = $(this).attr('href');
                var res = target.replace("/course/search", "/course");
                window.location = res;
                return false;
              });
            </script>

           
            
            <div class="form-group studentDtlFrmGroup" >
            
            <div class="row">
            <div class="col-md-10">
             <?php echo $this->Form->create('Stsearch',array('type'=>'get','inputDefaults'=>array('div'=>false,'label'=>false),'id'=>'Stsearch','class'=>'form-horizontal','method'=>'POST')); 
              ?>

             
             <div class="row">
            
            

               <div class="col-sm-3">
              
               <?php echo $this->Form->input('e_cat', array('class' => 'form-control branch','empty'=>'Choose Question Category','label'=>false,'options' => $categ)); ?>
  
              </div> 
               <div class="col-sm-2">
              
               <div class="input-append date" id="datepickerbrth" data-date="" data-date-format="dd-mm-yyyy"> 
							<input class="span2 form-control" size="16" placeholder="Exam Date From"  title="Exam Date From"  type="date" name="from_date" value="">  <span class="add-on"><i class="icon-th"></i></span>
						
						</div>
  
              </div> 

              <div class="col-sm-2">
              
              <div class="input-append date" id="datepickerbrth2" data-date="" data-date-format="dd-mm-yyyy"> 
             <input class="span2 form-control"  title="Exam Date To"  size="16" placeholder="Exam Date To" type="date" name="to_date" value="">  <span class="add-on"><i class="icon-th"></i></span>
           
           </div>
 
             </div> 

            
             <div class="col-sm-1" >
                
                <input type="submit" style="background-color:green;color: #fff;
    margin-top: 5px;
    padding: 6px;" id="Mysubscriptions" class="btn btn-success" value="Search">       
              </div>

            </div>
           

            <script>

$(function() {
$( "#datepickerbrth" ).datepicker({minDate:null,changeYear:true,changeMonth:true,yearRange:"c-40:c"});

});
$(function() {
$( "#datepickerbrth2" ).datepicker({minDate:null,changeYear:true,changeMonth:true,yearRange:"c-40:c"});

});


</script>

<style>
        .addnn , .addnn:hover{ height:55px; width: 55px; border-radius: 50%; background-color: #2d95e3; line-height: 15px; font-size: 14px !important; text-align: center; color: #fff; display: inline-block; text-decoration: none;}
        .addnn i{ font-size:18px; color:#fff; margin-top:10px;}
        .studentDtlFrmGroup .row{ display:flex; flex-wrap:wrap; width:100%;}
        .studentDtlFrmGroup .row>.col-md-10, .studentDtlFrmGroup .row>.col-md-2{ align-self:center !important;}
        .studentDtlFrmGroup .row>.col-md-10 form{ margin-bottom:0px !important;}

        .btn-default:hover, .btn-default.focus, .btn-default:focus, .btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default.focus:active, .btn-default:active:focus, .btn-default:active:hover{ animation-name: none !important;}
    </style>

            
   
             
              
              <?php echo $this->Form->end(); ?> 
</div>  
              
 
  <div class="col-md-2 pull-right">
    <ul class="list-inline w-75 m-auto text-center d-flex justify-content-around">
    <li class="list-inline-item ">

              <a class="addnn" href="<?php echo ADMIN_URL; ?>ExamSetup/add"><i class="fa fa-user-plus" aria-hidden="true"></i>
     <br> New</a> 
            </li>

         


            </ul>
  <style>
        .addnn , .addnn:hover{ height:55px; width: 55px; border-radius: 50%; background-color: #2d95e3; line-height: 15px; font-size: 14px !important; text-align: center; color: #fff; display: inline-block; text-decoration: none;}
        .addnn i{ font-size:18px; color:#fff; margin-top:10px;}
        .studentDtlFrmGroup{ display:flex; flex-wrap:wrap;}
        .studentDtlFrmGroup>.col-md-10, .studentDtlFrmGroup>.col-md-2{ align-self:center !important;}
    </style>

            
              

   <?php /*          
              <div class="col-sm-1">
                <label for="inputEmail3" class="control-label" style="color:white">Reset</label>
                <input type="reset" style="background-color:#d33c44;color:#fff;"  class="btn btn-success" value="Reset">       
              </div> 
              <div class="col-sm-1" style="margin-left: -40px;">
                <label for="inputEmail3" class="control-label" style="color:white">Search</label>
                <input type="submit" style="background-color:green;color:#fff;" id="Mysubscriptions" class="btn btn-success" value="Search">       
              </div> */ ?>
              </div>  
              
            
      
       
              </div>
   
     </div><!-- /.box-header -->
      <div class="box-body" id="Mycity"> 
          
             

      <table  class="table table-bordered table-striped afTblStndRept" id="example33" width="100%">
               
                    <thead class="thead-dark">
                    <tr>
                            <th >No.</th>
                            <th style="text-align:left;">Batch Code</th>
                            <th style="text-align:left;">Course Category</th>
                            <th style="text-align:left;">Exam Name</th>
                            <th style="text-align:left;">Exam Type</th>
                            
                            <th style="text-align:left;">Exam Date</th>
                            <th style="text-align:left;">Question (1 Mark)</th>
                            <th style="text-align:left;">Question (2 Mark)</th>
                            <th style="text-align:left;">Question (3 Mark)</th>
                            <th style="text-align:left;">Question (4 Mark)</th>
                            <th style="text-align:left;">Details</th>
                                                    
                            <th style="text-align:left;">Action</th>
                       
                        </tr>
                    </thead>
                    <tbody>
                  


                 <?php if(count($destination)>0){ $count=1;?>
                        <?php foreach($destination as $key=>$value) {
                      
                            $atz=$value['1mark'];
                            $atz1=$value['2mark'];
                            $atz2=$value['3mark'];
                            $atz3=$value['5mark'];
                            $tot=($atz+$atz1+$atz2+$atz3);
                            $tm=$atz*1+$atz1*2+$atz2*3+$atz3*4;

                            ?>

                           
                            <tr class="gradeX">
            <td style="text-align:left;"><?php echo $count++; ?></td>
                  <td style="text-align:left;"><?php echo $value['batch']['batch_code']; ?></td>
       <td style="text-align:left;"><?php echo $value['question_category']['qc_name']; ?></td>
                   <td style="text-align:left;"><a href="<?php echo SITE_URL; ?>admin/ExamSetup/view_examdetail/<?php echo $value['e_id']; ?>" target="_blank"><?php echo $value['e_name']; ?></a></td>
                               <td style="text-align:left;"><?php

                               if($value['e_type']==1)
                               {
                                echo("Major Exam");
                            }
                            else if($value['e_type']==0)
                            {
                               echo("Minor Exam");
                           }
                           else
                           {
                               echo("Special Exam");
                           }	

                           ?></td>

                           <td style="text-align:left;"><?php echo date("jS F, Y", strtotime($value['e_date']));  ?></td>

                           <td style="text-align:left;"><?php echo $value['1mark'];  ?></td>
                           <td style="text-align:left;"><?php echo $value['2mark'];  ?></td>
                           <td style="text-align:left;"><?php echo $value['3mark'];  ?></td>
                           <td style="text-align:left;"><?php if($value['4mark']){ echo $value['4mark']; }else{ echo 0; }  ?></td>
                           <td style="text-align:left;"><?php echo ("<b>Exam Duration :-</b>").$value['e_duration'].("<br> <b>Total Questions:-</b>").$tot.("<br> <b>Total Marks:-</b>").$tm ; ?></td>
                           <!-- <td> <a href="">Result </a> </td>-->
                           <td style="text-align:left;">


                            <?php
                            if($value['status']!=1)
                            {
                                echo("Exam Not Yet Held<br>"); 
                                if($this->request->session()->read('Auth.User.role_id')==1)  {
                                 
       echo $this->Html->link(__(''), array('action' => 'edit', $value['e_id']), array('class'=>'fa fa-pencil-square-o fa-lg','title'=>'Edit','style'=>'font-size: 20px !important; margin-left: 12px;'));


     
     echo $this->Html->link('', ['action' => 'delete',$value->e_id],['title'=>'Delete','class'=> 'fa fa-trash','style'=>'color:#FF0000; margin-left: 13px; font-size: 19px !important;',"onClick"=>"javascript: return confirm('Are you sure do you want to delete this Exam Setup')"]); 
                                }
                            }
                            else
                                { ?>

                                  <a target="_blank" href="<?php echo ADMIN_URL;?>ExamSetup/viewpdf/<?php echo $value['e_id'] ?>/<?php echo $value['batch']['batch_code']; ?>">View Result<br></a> 

                                <?php  } ?>

                          </td>

                      </tr>
                  <?php } ?>
              <?php } else{  ?>    
                <tr><td colspan="7" align="center">No Meta Available</td></tr>
            <?php } ?>
                       
                    </tbody>
                </table>
                <?php  //if(count($destination)>10){ //pr($events);?>
                  <?php echo $this->element('admin/pagination'); ?> 
            </div>
       
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
       </div>
      <!-- /.col -->  
    </div>
     <!-- /.row -->      
  </section>
    <!-- /.content -->  
       
 </div>     
     <!-- /.   content-wrapper -->  
     
     
