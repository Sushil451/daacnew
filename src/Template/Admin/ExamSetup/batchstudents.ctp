<?php if(!empty($students)){ ?>
<p><input type="checkbox" class="checkall" checked>Select All</p>
<ul style="display: flex;
    list-style: none;
    /* border: 1px solid #ccc; */
    /* border-radius: 3px; */
    padding: 12px 0px;
    margin: 13px 0px;
    flex-wrap: wrap;">
    <?php foreach($students as $student){ ?>
                    <li style="width:25%">
                      <ul style="display:flex; list-style:none; align-items:center;">
                        <li style="width:20px"><input type="checkbox" name="allowed_Stu[<?php echo $student['batch']['b_id'];?>][]" value="<?php echo $student['student']['s_id'];?>" class="checks" checked></li>
                        <li><label><?php echo $student['student']['s_name'];?></label></li>
                      </ul>
                    </li>
    <?php } ?>
                  </ul>
                  <script>
                  $('.checkall').change(function(){
                  if($(this).attr("checked") == true){
                    $('.checks').attr('checked',true);
                  }else{
                    $('.checks').attr('checked',false);
                  }
                  });
                  </script>
    <?php } ?>
    