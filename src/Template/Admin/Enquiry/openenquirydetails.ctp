<?php echo $this->Form->create('Student',array('type'=>'file','inputDefaults'=>array('div'=>false,'label'=>false),'class'=>'stdform dropupStudPop addStdntsFrm needs-validation')); ?> 
<div class="feespopup" id='inline_contentinline_contentdrop' style="background:#fff;"> 

<?php echo $this->Form->input('enq_id', array('class' => 'longinput','type'=>'hidden','value'=>$id)); ?>
  <div class="fplan">  
  
  <div class="row">
			<div class="col-md-12">
			

			<span style="float:right"><b>Enquiry Type :</b>
            <?php echo $mode_list[$det['reference']]; if($det['type']=="Contest"){ ?>(<b><?php echo $det['type'];?></b>)</br>
 <?php if($det['sc_name']){ ?>Detail :<b><?php echo $det['sc_name'];?></b><? } ?><? } ?></span>	
              
              </div>
              
               </div>
<table class="table table-bordered table-striped afTblStndRept" width="100%">
               
              
    <tbody>
   <tr>
   
  

          

               <div class="row mb-3 enq_form">
<div class="col-sm-6 align-self-center" >


<label>Course</label>
<select name="course_id" label="false" class="form-control" value="<?php echo $det['course_id']; ?>" required>
        <option value="">Select Course</option>
              <?php foreach($categ1 as $key=>$value){ ?>
              <option <?php if($det['course_id']==$key){ ?>selected <?php } ?> value= "<?php echo $key;?>"><?php echo $value;?></option>
              <?php } ?>
            </select>
  </div>


    <div class="col-sm-6 align-self-center enq_form">
    <label>Contact No.</label>
    <?php echo $this->Form->input('phone', array('class' => 'form-control','type'=>'text','id'=>'cntct','required','value'=>$det['phone'],'onkeypress'=>'return isNumber();','placeholder'=>'Enter Mobile No.','label'=>false,'maxlength'=>12)); ?>
               </div>

               
               </div>

               <div class="row mb-3 enq_form">


    <div class="col-sm-6 align-self-center enq_form">
    <label>Student Name</label>
    <?php echo $this->Form->input('name', array('class' => 'form-control', 'placeholder'=>'Enter Student Name','value'=>$det['name'],'label'=>false,'required')); ?>
               </div>

               <div class="col-sm-6 align-self-center" >
               <label>Next FollowUp Date</label>
<div class="input-append date" id="datepickerenquiry" data-date="<?php echo date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy"> 
	<input class="span2 form-control" size="16" placeholder="Next FollowUp Date" type="date" name="fdate" required >  <span class="add-on"><i class="icon-th"></i></span>
					
					</div>


  </div>
               
               </div>
               <div class="col-sm-6 align-self-center mb-3 ">
               <label>Shift To</label>
               <?php echo $this->Form->input('branch',array('type'=>'select','empty'=>'Shift To','class' => 'form-control','label'=>false,'options'=>$transbranch)); ?>
               </div>
               <div class="col-sm-12 align-self-center ">
               <label>Comment</label>
    <?php echo $this->Form->textarea('f_responce', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Comment','style'=>'height:100px;')); ?>
              </div>

               <div class="row mb-3 enq_form">
<div class="col-sm-12 align-self-center" >
<strong>Type :</strong>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="status" checked id="inlineRadio1" value="1">
  <label class="form-check-label" for="inlineRadio1">Open Enquiry</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="0">
  <label class="form-check-label" for="inlineRadio2">Close Enquiry</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="status" id="inlineRadio3" value="-1">
  <label class="form-check-label" for="inlineRadio3">Registered</label>
</div>

<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" <?php if($det['is_walkin']=="Y"){ ?> checked <?php } ?> name="is_walkin" id="inlineRadio4" value="Y">
  <label class="form-check-label" for="inlineRadio4">Walk In</label>
</div>
  </div>


  <?php $follow_count=count($det['follow']); //pr($value['Follow']); ?>
                  <input type="hidden" name="follow_id" id="follow_idss" value="<?php echo (isset($det['follow']['f_id']))?$det['follow']['f_id']:""; ?>">

               
               </div>
               
   
    </tr>
    
    
  	
    
</tbody>
</table>

<table class="table table-bordered table-striped"   id="followdetails">
                    <thead class="thead-dark">
                      <tr>
                        <th style="font-size:12px;width:5%;">S.No.</th>
                        <th style="font-size:12px;width:15%;">Date</th>
                        <th style="font-size:12px;">Remark</th>
                        <th style="font-size:12px;width:15%;">Next FollowUp</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                    <?php if(count($det['follow'])>0){  
        $cnt=1;  ?>

    <?php foreach($det['follow'] as $key=>$value) {  ?>

<tr>
<td><?php echo $cnt++; ?></td>
<td><?php echo strftime('%b %d, %Y',strtotime($value['add_date'])); ?></td>
<td><?php echo $value['f_responce']; ?></td>
<td><?php echo strftime('%b %d, %Y',strtotime($value['f_date'])); ?></td>
    </tr>
        <?php }  }else{ ?>
            <tr>
                            <td colspan="4" align="center">No Meta Available</td>
                        </tr>
            <?php } ?>
                      </tbody>
                    
                  </table>
                  <table class="table table-bordered table-striped"  id="calldetails">
                    <thead class="thead-dark">
                      <tr>
                        <th style="font-size:12px;width:5%;">S.No.</th>
                        <th style="font-size:12px;width:15%;">Date</th>
                        <th style="font-size:12px;">Status</th>
                        <th style="font-size:12px;width:15%;">Recording</th>
                        <th>&nbsp;</th>
                      </tr>
                    </thead>
                    <tbody>
                         <?php if(count($det['callDetails'])>0){  
        $cnt=1;  ?>

    <?php foreach($det['callDetails'] as $key=>$value) { 
        if($value['recording_url']){
            $link='<a target="_blank" href="'.$value['recording_url'].'">Download</a>';
        }else{
            $link="--";
        } ?>
<tr>
<td><?php echo $cnt++; ?></td>
<td><?php echo strftime('%b %d, %Y',strtotime($value['call_date'])); ?></td>
<td><?php echo $value['customer_status']; ?></td>
<td><?php echo $link; ?></td>
    </tr>
        <?php }  }else{ ?>
            <tr>
                            <td colspan="4" align="center">No Meta Available</td>
                        </tr>
            <?php } ?>
                
                      </tbody>
                    
                  </table>
  
</div>
</div>
<div class="modal-footer box footer">
        <!-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> -->
        <?php echo $this->Form->submit('Submit',array('class' => 'btn btn-success')); ?>
      </div>
      <?php echo $this->Form->end();?> 
<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
    
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
<script>
function isNumber(evt) {

evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;
if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {

return false;
}
return true;
}

</script>