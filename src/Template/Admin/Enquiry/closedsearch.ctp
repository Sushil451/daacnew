
      <table class="table table-bordered table-striped afTblStndRept" width="100%">
               
               <thead class="thead-dark">
                   <tr> 
         
                     
                     <th class="head0" style="width:4%">S.No.</th>
                     <th class="head0" style="width:15%">Name</th>
                     <th class="head0" style="width:10%">Mobile</th>
                     <th class="head0" style="width:13%">Course</th>
                     <th class="head0" style="width:10%">Mode</th>
                     <th class="head0" style="width:10%">Follow Up Date</th>
                     <th class="head0" style="width:10%">Enquiry Date</th>
                     <th class="head0" style="width:21%">Remark</th>
                     <th class="head0" style="width:7%">Action</th>
         
                     
                   </tr>
                 </thead>
                             <tbody>
             <?php if(count($enq)>0){ 
                 $cnt=1;  ?>
         
             <?php foreach($enq as $key=>$value) {  ?>
            
             <tr class="gradeX"> 
                   
                   
             <td><?php echo $cnt; 
                   if($value['archive_enquiry']['important']==1){ 
                                    echo(" *");
                                       /*<img src="<?php echo $this->webroot;?>admin_images/online.png">*/
                                     }
                                    $cnt++; ?></td>
                     <td  style="text-align: left">
                   <?php //echo $value['Enquiry']['name']?>
         
                  
                  <a  class='followupps' data-bs-toggle="modal" data-bs-target="#exampleModalfollow" href="<?php echo SITE_URL; ?>admin/enquiry/closedenquirydetails/<?php echo $value['archive_enquiry']['enq_id']; ?>"  
                  <?php  $dup_cnt=$this->Comman->has_duplicate($value['archive_enquiry']['phone']);
                   if(count($dup_cnt)>1){ echo "style='color:red;'"; }  ?>
                   ><?php echo $value['archive_enquiry']['name']." \x20\x20"; if($this->request->session()->read("Auth.User.role_id")==1){
                     $branch=$this->Comman->brnach($value['archive_enquiry']['branch_id']);
                     ?>
                    <span style="color:red; font-size:12px;">(<?php echo $branch['enroll_code'];?>) </span>
                     <?php }else{ ?>
                       <a href="javascript:void(0)" data-id="<?php echo $value['archive_enquiry']['enq_id']; ?>" class="call-enquiry"><i class="fa fa-phone change-icon<?php echo $value['archive_enquiry']['enq_id']; ?>" aria-hidden="true"></i></a> 
                     <?php }  //var_dump($value['Enquiry']['phone']);
           if($value['archive_enquiry']['ip']!='' && $this->request->session()->read("Auth.User.role_id")=='1'){
                echo "<br>".$value['archive_enquiry']['ip'];
              }
                   ?></a>
                  
                  
                     </td>
                   <td style="text-align: left"><?php echo $value['archive_enquiry']['phone']?></td>
                   <?php //echo $value['archive_enquiry']['email']?>
                   
                   
                   <td style="text-align: left"><?php  if($value['archive_enquiry']['course_id']){ 
                $crsname=$this->Comman->crs($value['archive_enquiry']['course_id']);
                      echo $crsname['sname'];
               }else{
                          echo "Other";
                          }  ?>
                       
                       </td>
                    <td style="text-align: left"><?php echo $mode_list[$value['archive_enquiry']['reference']]; if($value['archive_enquiry']['type']=="Contest"){ ?>(<b><?php echo $value['archive_enquiry']['type'];?></b>) </br>
          <?php if($value['archive_enquiry']['sc_name']){ ?>Detail :<b><?php echo $value['archive_enquiry']['sc_name'];?></b><? } ?><? } ?></td>
                     
                   <td style="text-align: left"><?php
             if($value['archive_enquiry']['shiftfrom']!="0" && $value['archive_enquiry']['shiftfrom']!=null)
         {
               
            
              echo("<b>Shifted from ".$branhces[$value['archive_enquiry']['shiftfrom']]."</b><br>");
             
         }    ?>
                     
                     <?php  if($value['f_date']>0){
                      echo strftime('%b %d, %Y',strtotime($value['f_date'])); }else{
                          
                          echo strftime('%b %d, %Y',strtotime($value['archive_enquiry']['add_date']));
                          }?></td>
                     <td><?php echo strftime('%b %d, %Y',strtotime($value['archive_enquiry']['add_date']));?></td>
                   <td style="text-align: left"><?php if($value['f_responce']) {
            echo $value['f_responce']; 
             } 
         
         else { 
           
           echo("No remark"); 
               } ?>
             <?php 
             $recording=$this->Comman->recording($value['archive_enquiry']['phone']);
             if($recording['recording_url']){
               echo '<a target="_blank" style="font-size:14px;float:right" href="'.$recording['recording_url'].'"><i class="fa fa-play-circle-o" aria-hidden="true"></i>
               </a>';
             }
             ?>  
             </td>
                   <td>
                   
                      <?php if($this->request->session()->read("Auth.User.role_id")==1) {
         echo $this->Html->link(__(''), array('action' => 'delete', $value['archive_enquiry']['enq_id']), array('class'=>'fa fa-trash','title'=>'Delete','style'=>'font-size: 15px !important;'), __('Are you sure you want to delete # %s?', $value['archive_enquiry']['enq_id'])); }
          ?> &nbsp;   <?php
         if($value['archive_enquiry']['important']!=1){
         
            echo $this->Html->link(__(''), array('action' => 'impo', $value['archive_enquiry']['enq_id'],$value['archive_enquiry']['phone'],$value['archive_enquiry']['branch_id']), array('class'=>'fa fa-info','title'=>'Important','style'=>'font-size: 15px !important;'));
         }
         ?>
         <?php /* <a class='inline btn btn4 btn_edit_a' href="#inline_content<?php echo $value['Enquiry']['enq_id'];?>"></a>
                     if($value['Enquiry']['status']==0)
         {
                                        echo $this->Html->link(__(''), array('action' => 'activ', $value['Enquiry']['enq_id']), array('class'=>'btn btn4 btn_act_a','title'=>'Activate'));
         }        
         
         
         */
         ?></td>
                 </tr>
         
                                                
                                 <?php $count++; }  ?>
                                 <?php } else{  ?>
                                 <tr>
                                     <td colspan="7" align="center">No Meta Available</td>
                                 </tr>
                                 <?php } ?>
                             </tbody>
                         </table>
                       
                             <?php echo $this->element('admin/pagination'); ?> 