<style type="text/css">
   th{
text-align: center;
   }
   td{
    text-align: center;

   }


   .action_icon a span {
    font-size: 14px !important;
}
 .action_icon a {
    font-size: 13px !important;
}
.action_icon b i {
    font-size: 13px !important;
}

.action_icon a {
    display: inline-block;
    width: 21px;
    text-align: center;
}
 </style>
 <div class="content-wrapper">
   <section class="content-header">
    <h1>
         Enquiry Detail
    </h1>
    <ol class="breadcrumb">
    <li><a href="<?php echo SITE_URL; ?>admin/dashboards"><i class="fa fa-home"></i>Home</a></li>
<li><a href="<?php echo SITE_URL; ?>admin/student">Enquiry List</a></li>
    </ol> 
    </section> <!-- content header -->

    <!-- Main content -->
    <section class="content">
  <div class="row">
    <div class="col-xs-12">    
  <div class="box">
    <div class="box-header">
  <?php echo $this->Flash->render(); ?>


  <?php $role_id=$this->request->session()->read('Auth.User.role_id'); ?>
            <script>          
              $(document).ready(function () { 
                $(".Stsearchs").bind("change", function (event) {
                 $('.lds-facebook').show();
                  $.ajax({
                    async:true,
                    data:$("#Stsearch").serialize(),
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>enquiry/search",
                    success:function (data) {
                    $('.lds-facebook').hide();   
                      $("#Mycity").html(data); },
                    });
                  return false;
                });
              });


              $(document).ready(function () { 
                $(".branch").bind("change", function (event) {
               $('.lds-facebook').show();
                  $.ajax({
                    async:true,
                    data:$("#Stsearch").serialize(),
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>enquiry/search",
                    success:function (data) {
                  $('.lds-facebook').hide();   
                      $("#Mycity").html(data); },
                    });
                  return false;
                });
              });

              $(document).on('click', '.pagination a', function(e) {
                var target = $(this).attr('href');
                var res = target.replace("/enquiry/search", "/enquiry");
                window.location = res;
                return false;
              });
            </script>

           
            
            <div class="form-group studentDtlFrmGroup" >
            
            <div class="row">
           <div class="col-md-10">
             <?php echo $this->Form->create('Stsearch',array('type'=>'get','inputDefaults'=>array('div'=>false,'label'=>false),'id'=>'Stsearch','class'=>'form-horizontal','method'=>'POST')); ?>

             
             <div class="row">
             <div class="col-sm-2">
             
             <?php echo $this->Form->input('course_id', array('class' => 
             'form-control Stsearchs','id'=>'course_id','label'=>false,'options'=>$categ1,'empty'=>'--All Course--','autofocus')); ?>
           </div>
             <div class="col-sm-2">
             
             <?php echo $this->Form->input('modes', array('class' => 
             'form-control Stsearchs','id'=>'etype','label'=>false,'options'=>$mode,'empty'=>'--All Enquiry Mode--','autofocus')); ?>
           </div>


            <?php if($this->request->session()->read("Auth.User.role_id")==1){  ?>
              <div class="col-sm-2">
             
                <?php echo $this->Form->input('branch_id', array('class' => 
                'form-control Stsearchs','id'=>'branch_id','label'=>false,'options'=>$branhces,'empty'=>'--Select Branch--','autofocus')); ?>
              </div>
   <?php }else{
     
  echo $this->Form->input('branch_id',array('type'=>'hidden','value'=>$this->request->session()->read("Auth.User.branch")));   
 }  
 $types=['viewall'=>'All','active'=>'Active','important'=>'Interested','marketing'=>'Marketing','walkin'=>'Walkin','shifted_to'=>'Shifted By Us','shifted'=>'Shifted to us','enquiry'=>'Enquiry Detailed','monthly_summary'=>'Monthly Summary'];?>
              <div class="col-sm-2">
                
<?php echo $this->Form->input('type', array('class' =>'form-control Stsearchs','id'=>'b_id','label'=>false,'options'=>$types,'empty'=>'--Enquiry Type--','autofocus')); ?>
              </div>

               <div class="col-sm-2">
              
                <?php echo $this->Form->input('name',array('class'=>'form-control branch','label' =>false,'placeholder'=>'Name','autocomplete'=>'off')); ?>  
              </div> 
              <div class="col-sm-2">
                
                <?php echo $this->Form->input('mobile',array('class'=>'form-control branch','label' =>false,'placeholder'=>'Mobile','autocomplete'=>'off'));
                
                echo $this->Form->input('paper',array('value'=>$pg,'type'=>'hidden')); ?>  
              </div> 

            </div>
        



<style>
        .addnn , .addnn:hover{ height:55px; width: 55px; border-radius: 50%; background-color: #2d95e3; line-height: 15px; font-size: 14px !important; text-align: center; color: #fff; display: inline-block; text-decoration: none;}
        .addnn i{ font-size:18px; color:#fff; margin-top:10px;}
        .studentDtlFrmGroup .row{ display:flex; flex-wrap:wrap; width:100%;}
        .studentDtlFrmGroup .row>.col-md-10, .studentDtlFrmGroup .row>.col-md-2{ align-self:center !important;}
        .studentDtlFrmGroup .row>.col-md-10 form{ margin-bottom:0px !important;}

        .btn-default:hover, .btn-default.focus, .btn-default:focus, .btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default.focus:active, .btn-default:active:focus, .btn-default:active:hover{ animation-name: none !important;}
    </style>

            
              
<?php if($this->request->session()->read("Auth.User.role_id")==4){  
  $branchID = $this->request->session()->read("Auth.User.branch");
 echo $this->Form->input('branch_id', array('class' => 'smallselect branch','type' => 'hidden','value'=>$branchID));
}

 ?>
   <?php /*          
              <div class="col-sm-1">
                <label for="inputEmail3" class="control-label" style="color:white">Reset</label>
                <input type="reset" style="background-color:#d33c44;color:#fff;"  class="btn btn-success" value="Reset">       
              </div> 
              <div class="col-sm-1" style="margin-left: -40px;">
                <label for="inputEmail3" class="control-label" style="color:white">Search</label>
                <input type="submit" style="background-color:green;color:#fff;" id="Mysubscriptions" class="btn btn-success" value="Search">       
              </div> */ ?>
              
              <?php echo $this->Form->end(); ?> 
</div>
              <div class="col-md-2">
  
              <ul class="list-inline w-75 m-auto text-center d-flex justify-content-around">
    <li class="list-inline-item">

    <a  data-bs-toggle="modal" data-bs-target="#exampleModal5"  class="addnn addnnBg"><i class="fa fa-user-plus" aria-hidden="true"></i>
     <br> Add Enquiry Form</a> 



     
<!-----  Script for Contact No. Check  ------------>
<script> 
function mobileval() {

var mobile = document.getElementById("cntct").value; 
var pattern = /(0|6|7|8|9)\d{9}/; 
var len=$('#cntct').val().length;
if (pattern.test(mobile) && len<12) {

return true;
}
else{  
  $('#cntct').val('');
alert("Insert a valid mobile numbers");
return false;
}
}
     $(document).ready(function(){ 
         var site_url='<?php echo ADMIN_URL;?>';

         var vaal=$("#cntct").val();
               $('#cntct').change(function(e){ //alert();
           e.preventDefault();
                $.ajax({
           type: "POST",
           url: site_url+'enquiry/newcontact',
           data: $("#cntct").serialize(), // serializes the form's elements.
          
           success: function(data)
           { 
            // alert(data);
            if(data==0){
          
          //  $("#cntct").val('');
            $("#add_enq_save").attr('disabled',false);
            $("#inline_contentadd Form input").attr('required',false);
            $('#EnquiryEmail').attr('required',false);
            $("#inline_contentadd Form input[type=checkbox]").attr('required',false);
            $("#inline_contentadd Form select").attr('required',false);
            $("#inline_contentadd Form textarea").attr('required',false);
            $("input[type='hidden'][name='_method']").val('POST');
            $("#updrcrd").show();
            $(".enq_form").hide();
      $.ajax({
      type:"POST",
       data:$("#cntct").serialize(),
       url: site_url+'enquiry/ajx',
        dataType:"html",
         success:function (data, textStatus){
           $("#updrcrd").html(data);
           }, 
           });


            $("#restore_btn").show();
            $("#cntct").attr('required',true);
           
         }else if(data==2){
           
       
            $("#add_enq_save").attr('disabled',true);
            $("#updrcrd").show();

     $.ajax({
      type:"POST",
       data:$("#cntct").serialize(),
       url: site_url+'enquiry/ajx',
        dataType:"html",
         success:function (data, textStatus){
           $("#updrcrd").html(data);
           }, 
           });
        
            $(".enq_form").show();
            $("#restore_btn").hide();
            $("input[type='hidden'][name='_method']").val('POST');
            $("#cntct").val('');
         }else{
           $("#ncntct").css("display", "none");
           $("#updrcrd").hide();
           $(".enq_form").show();
           $("#restore_btn").hide();
         $("#add_enq_save").attr('disabled',false);
         $("#inline_contentadd Form input").attr('required',true); 
         $("#inline_contentadd Form input[type=checkbox]").attr('required',false);
         $('#EnquiryEmail').attr('required',false);
         $("input[type='hidden'][name='_method']").val('POST');
         }



           }
         });
           
                  });
   });

   </script>
   <script>




   $(function() {
    var today = new Date(date.getDate(), date.getMonth(), date.getFullYear());
$( "#datepickerenquiry" ).datepicker({minDate:today,startDate:today,changeYear:false,changeMonth:false});

});
     </script>
<!-- Modal -->
<div class="modal fade" id="exampleModal5" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
<div class="modal-header" style="background-color: #3c8dbc;color: #fff;">
        <h5 class="modal-title" id="exampleModalLabel">Add New Enquiry</h5>
   <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" id="inline_contentadd">
<?php echo $this->Form->create('Student',array('type'=>'file','inputDefaults'=>array('div'=>false,'label'=>false),'class'=>'form-horizontal dropupStudPop addStdntsFrm needs-validation','novalidate','onsubmit'=>'return mobileval();')); ?> 
<div class="feespopup" id='inline_contentinline_contentdrop<?php echo $cid; ?>' style=' background:#fff;'> 
  <div class="fplan">  

   <div class="row">
			<div class="col-md-12">
			

				<div class="box box-info boxInfoBx boxInfoBxStudentInfo">
  <div class="box-body addStdntsFrm">
                    <div class="row  mb-3">
<div class="col-sm-6 align-self-center">
<?php echo $this->Form->input('phone', array('class' => 'form-control','type'=>'text','id'=>'cntct','required','onkeypress'=>'return isNumber();','placeholder'=>'Enter Mobile No.','label'=>false,'maxlength'=>12)); ?>
     
  </div>


    <div class="col-sm-6 align-self-center enq_form">
    
    <?php echo $this->Form->input('name', array('class' => 'form-control', 'placeholder'=>'Enter Student Name','label'=>false,'required')); ?>
               </div>

               
               </div>

               <div id="updrcrd" class="row mb-3" style="display:none"></div>

               <div class="row mb-3 enq_form">
<div class="col-sm-6 align-self-center" >

<?php $cr=$categ1;  ?>

<select name="course_id" label="false" class="form-control" required>
        <option value="">Select Course</option>
              <?php foreach($cr as $key=>$value){ ?>
              <option value= "<?php echo $key;?>"><?php echo $value;?></option>
              <?php } ?>
            </select>
  </div>


    <div class="col-sm-6 align-self-center enq_form">
     
     <?php echo $this->Form->email('email', array('class' => 'form-control','placeholder'=>'Enter Vaid Email ID','label'=>false)); ?>
               </div>

               
               </div>

               <div class="row mb-3 enq_form">
<div class="col-sm-6 align-self-center" >

<?php $cr=$categ1;  ?>


<div class="input-append date" id="datepickerenquiry" data-date="<?php echo date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy"> 
	<input class="span2 form-control" size="16" placeholder="Next FollowUp Date" type="date" name="f_date" required  value="<?php echo date('d-m-Y'); ?>">  <span class="add-on"><i class="icon-th"></i></span>
					
					</div>


  </div>


    <div class="col-sm-6 align-self-center enq_form">
    
     <select name="reference" label="false" class="form-control" required>
      <option value="">Select Enquiry Type</option>
      
            <?php foreach($mode as $key=>$value) { 
             if($value!="Website"){
                        ?>
            <option type="radio"  value="<?php echo $key; ?>" <?php echo ($key==10)?"selected":""; ?>><?php echo $value; ?></option>
            <?php echo"<span>".$value."</span>"; } } ?>
            </select> 
               </div>

               
               </div>
              

               <div class="row mb-3 enq_form">
<div class="col-sm-12 align-self-center enq_formCheck" >
<label class="d-block">Type :</label>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="status" id="inlineRadio1" value="0">
  <label class="form-check-label" for="inlineRadio1">Mark Closed</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="important" id="inlineRadio2" value="1">
  <label class="form-check-label" for="inlineRadio2">Mark Important</label>
</div>

<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="is_walkin" id="inlineRadio2" value="Y">
  <label class="form-check-label" for="inlineRadio2">Walk In</label>
</div>
  </div>


    <div class="col-sm-12 align-self-center ">
    
     <?php echo $this->Form->textarea('enquiry', array('class' => 'form-control','required','label'=>false,'placeholder'=>'Enter Enquiry','style'=>'height:100px;')); ?>
               </div>

               
               </div>
               
              
             
               <div class="row d-flex" id="crk">
<div class="col-sm-12 align-self-center" >


  </div>

<div class="modal-footer box footer">
        <!-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> -->


        <?php echo $this->Form->submit('Submit',array('class' => 'btn btn-success enq_form','id'=>'add_enq_save')); ?>

        <div id="restore_btn" style="display:none">
        <?php echo $this->Form->submit('Restore',array('class' => 'btn btn-success','id'=>'add_enq_save')); ?> </div>
      </div>
               
               
               </div>
              
              </div>
             
             </div>
             
              </div>
  
</div></div>
</div>

      <?php echo $this->Form->end();?> 
<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
    
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
<script>
function isNumber(evt) {

evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;
if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {

return false;
}
return true;
}

</script>
      </div>
    
    </div>
  </div>
</div>



            </ul>
            </div>  
</div> 

   
     </div>





     
      <div class="box-body" id="Mycity"> 
          
             

      <table class="table table-bordered table-striped afTblStndRept" width="100%">
               
      <thead class="thead-dark">
          <tr> 

            
            <th class="head0" style="width:4%">S.No.</th>
            <th class="head0" style="width:15%">Name</th>
            <th class="head0" style="width:10%">Mobile</th>
            <th class="head0" style="width:13%">Course</th>
            <th class="head0" style="width:10%">Mode</th>
            <th class="head0" style="width:10%">Follow Up Date</th>
            <th class="head0" style="width:10%">Enquiry Date</th>
            <th class="head0" style="width:21%">Remark</th>
            <th class="head0" style="width:7%">Action</th>

            
          </tr>
        </thead>
                    <tbody>
    <?php if(count($enq)>0){  
        $cnt=1;  ?>

    <?php foreach($enq as $key=>$value) {  ?>
   
    <tr class="gradeX"> 
          
          
    <td><?php echo $cnt; 
          if($value['enquiry']['important']==1){ 
                           echo(" *");
                              /*<img src="<?php echo $this->webroot;?>admin_images/online.png">*/
                            }
                           $cnt++; ?></td>
            <td  style="text-align: left">
          <?php //echo $value['Enquiry']['name']?>

         
         <a  class='followupps' data-bs-toggle="modal" data-bs-target="#exampleModalfollow" href="<?php echo SITE_URL; ?>admin/enquiry/openenquirydetails/<?php echo $value['enquiry']['enq_id']; ?>"  
         <?php  $dup_cnt=$this->Comman->has_duplicate($value['enquiry']['phone']);
          if(count($dup_cnt)>1){ echo "style='color:red;'"; }  ?>
          ><?php echo $value['enquiry']['name']." \x20\x20"; if($this->request->session()->read("Auth.User.role_id")==1){
            $branch=$this->Comman->brnach($value['enquiry']['branch_id']);
            ?>
           <span style="color:red; font-size:12px;">(<?php echo $branch['enroll_code'];?>) </span>
            <?php }else{ ?>
              <a href="javascript:void(0)" data-id="<?php echo $value['enquiry']['enq_id']; ?>" class="call-enquiry"><i class="fa fa-phone change-icon<?php echo $value['enquiry']['enq_id']; ?>" aria-hidden="true"></i></a> 
            <?php }  //var_dump($value['Enquiry']['phone']);
  if($value['enquiry']['ip']!='' && $this->request->session()->read("Auth.User.role_id")=='1'){
       echo "<br>".$value['enquiry']['ip'];
     }
          ?></a>
         
         
            </td>
          <td style="text-align: left"><?php echo $value['enquiry']['phone']?></td>
          <?php //echo $value['Enquiry']['email']?>
          
          
          <td style="text-align: left"><?php  if($value['enquiry']['course_id']){ 
       $crsname=$this->Comman->crs($value['enquiry']['course_id']);
			 echo $crsname['sname'];
      }else{
				 echo "Other";
				 }  ?>
			  
			  </td>
           <td style="text-align: left"><?php echo $mode_list[$value['enquiry']['reference']]; if($value['enquiry']['type']=="Contest"){ ?>(<b><?php echo $value['enquiry']['type'];?></b>) </br>
 <?php if($value['enquiry']['sc_name']){ ?>Detail :<b><?php echo $value['enquiry']['sc_name'];?></b><? } ?><? } ?></td>
            
          <td style="text-align: left"><?php
    if($value['enquiry']['shiftfrom']!="0" && $value['enquiry']['shiftfrom']!=null)
{
      
   
     echo("<b>Shifted from ".$branhces[$value['enquiry']['shiftfrom']]."</b><br>");
    
}    ?>
            
            <?php  if($value['f_date']>0){
             echo strftime('%b %d, %Y',strtotime($value['f_date'])); }else{
				 
				 echo strftime('%b %d, %Y',strtotime($value['enquiry']['add_date']));
				 }?></td>
            <td><?php echo strftime('%b %d, %Y',strtotime($value['enquiry']['add_date']));?></td>
          <td style="text-align: left"><?php if($value['f_responce']) {
   echo $value['f_responce']; 
    } 

else { 
  
  echo("No remark"); 
      } ?>
    <?php 
    $recording=$this->Comman->recording($value['enquiry']['phone']);
    if($recording['recording_url']){
      echo '<a target="_blank" style="font-size:14px;float:right" href="'.$recording['recording_url'].'"><i class="fa fa-play-circle-o" aria-hidden="true"></i>
      </a>';
    }
    ?>  
    </td>
          <td>
          
             <?php if($this->request->session()->read("Auth.User.role_id")==1) {
echo $this->Html->link(__(''), array('action' => 'delete', $value['enquiry']['enq_id']), array('class'=>'fa fa-trash','title'=>'Delete','style'=>'font-size: 15px !important;'), __('Are you sure you want to delete # %s?', $value['enquiry']['enq_id'])); }
 ?> &nbsp;   <?php
if($value['enquiry']['important']!=1){

   echo $this->Html->link(__(''), array('action' => 'impo', $value['enquiry']['enq_id'],$value['enquiry']['phone'],$value['enquiry']['branch_id']), array('class'=>'fa fa-info','title'=>'Important','style'=>'font-size: 15px !important;'));
}
?>
<?php /* <a class='inline btn btn4 btn_edit_a' href="#inline_content<?php echo $value['Enquiry']['enq_id'];?>"></a>
            if($value['Enquiry']['status']==0)
{
                               echo $this->Html->link(__(''), array('action' => 'activ', $value['Enquiry']['enq_id']), array('class'=>'btn btn4 btn_act_a','title'=>'Activate'));
}        


*/
?></td>
        </tr>

                                       
                        <?php $count++; }  ?>
                        <?php } else{  ?>
                        <tr>
                            <td colspan="7" align="center">No Meta Available</td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
              
                    <?php echo $this->element('admin/pagination'); ?> 
              </div>
       
          </div>
        
       </div>
     
    </div>
    
  </section>
    
       
 </div>    

 <script>
  $(document).on('click','.followupps',function (e){
  e.preventDefault();
  $('#exampleModalfollow').modal('show').find('.modal-body').load($(this).attr('href'));
});

</script>


<!-- Modal -->
<div class="modal fade" id="exampleModalfollow" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
<div class="modal-header" style="background-color: #3c8dbc;color: #fff;">
        <h5 class="modal-title" id="exampleModalLabel">Enquiry Followup Form</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       Please wait while we fetching data result....
      </div>
    
    </div>
  </div>
</div>
