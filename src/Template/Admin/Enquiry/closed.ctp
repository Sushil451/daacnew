<style type="text/css">
   th{
text-align: center;
   }
   td{
    text-align: center;

   }


   .action_icon a span {
    font-size: 14px !important;
}
 .action_icon a {
    font-size: 13px !important;
}
.action_icon b i {
    font-size: 13px !important;
}

.action_icon a {
    display: inline-block;
    width: 21px;
    text-align: center;
}
 </style>
 <div class="content-wrapper">
   <section class="content-header">
    <h1>
         Closed Enquiry Detail
    </h1>
    <ol class="breadcrumb">
    <li><a href="<?php echo SITE_URL; ?>admin/dashboards"><i class="fa fa-home"></i>Home</a></li>
<li><a href="<?php echo SITE_URL; ?>admin/student">Enquiry List</a></li>
    </ol> 
    </section> <!-- content header -->

    <!-- Main content -->
    <section class="content">
  <div class="row">
    <div class="col-xs-12">    
  <div class="box">
    <div class="box-header">
  <?php echo $this->Flash->render(); ?>


  <?php $role_id=$this->request->session()->read('Auth.User.role_id'); ?>
            <script>          
              $(document).ready(function () { 
                $(".Stsearchs").bind("change", function (event) {
                 $('.lds-facebook').show();
                  $.ajax({
                    async:true,
                    data:$("#Stsearch").serialize(),
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>enquiry/closedsearch",
                    success:function (data) {
                    $('.lds-facebook').hide();   
                      $("#Mycity").html(data); },
                    });
                  return false;
                });
              });


              $(document).ready(function () { 
                $(".branch").bind("change", function (event) {
               $('.lds-facebook').show();
                  $.ajax({
                    async:true,
                    data:$("#Stsearch").serialize(),
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>enquiry/closedsearch",
                    success:function (data) {
                  $('.lds-facebook').hide();   
                      $("#Mycity").html(data); },
                    });
                  return false;
                });
              });

              $(document).on('click', '.pagination a', function(e) {
                var target = $(this).attr('href');
                var res = target.replace("/enquiry/closedsearch", "/enquiry/closed");
                window.location = res;
                return false;
              });
            </script>

           
            
            <div class="form-group studentDtlFrmGroup" >
            
            <div class="row">
           <div class="col-md-10">
             <?php echo $this->Form->create('Stsearch',array('type'=>'get','inputDefaults'=>array('div'=>false,'label'=>false),'id'=>'Stsearch','class'=>'form-horizontal','method'=>'POST')); ?>

             
             <div class="row">
             <div class="col-sm-2">
             
             <?php echo $this->Form->input('course_id', array('class' => 
             'form-control Stsearchs','id'=>'course_id','label'=>false,'options'=>$categ1,'empty'=>'--All Course--','autofocus')); ?>
           </div>
             <div class="col-sm-2">
             
             <?php echo $this->Form->input('modes', array('class' => 
             'form-control Stsearchs','id'=>'etype','label'=>false,'options'=>$mode,'empty'=>'--All Enquiry Mode--','autofocus')); ?>
           </div>


            <?php if($this->request->session()->read("Auth.User.role_id")==1){  ?>
              <div class="col-sm-2">
             
                <?php echo $this->Form->input('branch_id', array('class' => 
                'form-control Stsearchs','id'=>'branch_id','label'=>false,'options'=>$branhces,'empty'=>'--Select Branch--','autofocus')); ?>
              </div>
              <?php }else
 {
  echo $this->Form->input('branch_id',array('type'=>'hidden','value'=>$this->request->session()->read("Auth.User.branch")));   
 }  
 $types=['viewall'=>'All','active'=>'Active','important'=>'Interested','marketing'=>'Marketing','walkin'=>'Walkin','shifted_to'=>'Shifted By Us','shifted'=>'Shifted to us','enquiry'=>'Enquiry Detailed','monthly_summary'=>'Monthly Summary'];?>
              <div class="col-sm-2">
                
<?php echo $this->Form->input('type', array('class' =>'form-control Stsearchs','id'=>'b_id','label'=>false,'options'=>$types,'empty'=>'--Enquiry Type--','autofocus')); ?>
              </div>

               <div class="col-sm-2">
              
                <?php echo $this->Form->input('name',array('class'=>'form-control branch','label' =>false,'placeholder'=>'Name','autocomplete'=>'off')); ?>  
              </div> 
              <div class="col-sm-2">
                
                <?php echo $this->Form->input('mobile',array('class'=>'form-control branch','label' =>false,'placeholder'=>'Mobile','autocomplete'=>'off'));
                
                echo $this->Form->input('paper',array('value'=>$pg,'type'=>'hidden')); ?>  
              </div> 

            </div>
        



<style>
        .addnn , .addnn:hover{ height:55px; width: 55px; border-radius: 50%; background-color: #2d95e3; line-height: 15px; font-size: 14px !important; text-align: center; color: #fff; display: inline-block; text-decoration: none;}
        .addnn i{ font-size:18px; color:#fff; margin-top:10px;}
        .studentDtlFrmGroup .row{ display:flex; flex-wrap:wrap; width:100%;}
        .studentDtlFrmGroup .row>.col-md-10, .studentDtlFrmGroup .row>.col-md-2{ align-self:center !important;}
        .studentDtlFrmGroup .row>.col-md-10 form{ margin-bottom:0px !important;}

        .btn-default:hover, .btn-default.focus, .btn-default:focus, .btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default.focus:active, .btn-default:active:focus, .btn-default:active:hover{ animation-name: none !important;}
    </style>

            
              
<?php if($this->request->session()->read("Auth.User.role_id")==4){  
  $branchID = $this->request->session()->read("Auth.User.branch");
 echo $this->Form->input('branch_id', array('class' => 'smallselect branch','type' => 'hidden','value'=>$branchID));
}

 ?>
   <?php /*          
              <div class="col-sm-1">
                <label for="inputEmail3" class="control-label" style="color:white">Reset</label>
                <input type="reset" style="background-color:#d33c44;color:#fff;"  class="btn btn-success" value="Reset">       
              </div> 
              <div class="col-sm-1" style="margin-left: -40px;">
                <label for="inputEmail3" class="control-label" style="color:white">Search</label>
                <input type="submit" style="background-color:green;color:#fff;" id="Mysubscriptions" class="btn btn-success" value="Search">       
              </div> */ ?>
              
              <?php echo $this->Form->end(); ?> 
</div>
             
</div> 

   
     </div>





     
      <div class="box-body" id="Mycity"> 
          
             

      <table class="table table-bordered table-striped afTblStndRept" width="100%">
               
      <thead class="thead-dark">
          <tr> 

            
            <th class="head0" style="width:4%">S.No.</th>
            <th class="head0" style="width:15%">Name</th>
            <th class="head0" style="width:10%">Mobile</th>
            <th class="head0" style="width:13%">Course</th>
            <th class="head0" style="width:10%">Mode</th>
            <th class="head0" style="width:10%">Follow Up Date</th>
            <th class="head0" style="width:10%">Enquiry Date</th>
            <th class="head0" style="width:21%">Remark</th>
            <th class="head0" style="width:7%">Action</th>

            
          </tr>
        </thead>
                    <tbody>
    <?php if(count($enq)>0){ 
        $cnt=1;  ?>

    <?php foreach($enq as $key=>$value) {  ?>
   
    <tr class="gradeX"> 
          
          
    <td><?php echo $cnt; 
          if($value['archive_enquiry']['important']==1){ 
                           echo(" *");
                              /*<img src="<?php echo $this->webroot;?>admin_images/online.png">*/
                            }
                           $cnt++; ?></td>
            <td  style="text-align: left">
          <?php //echo $value['Enquiry']['name']?>

         
         <a  class='followupps' data-bs-toggle="modal" data-bs-target="#exampleModalfollow" href="<?php echo SITE_URL; ?>admin/enquiry/closedenquirydetails/<?php echo $value['archive_enquiry']['enq_id']; ?>"  
         <?php  $dup_cnt=$this->Comman->has_duplicate($value['archive_enquiry']['phone']);
          if(count($dup_cnt)>1){ echo "style='color:red;'"; }  ?>
          ><?php echo $value['archive_enquiry']['name']." \x20\x20"; if($this->request->session()->read("Auth.User.role_id")==1){
            $branch=$this->Comman->brnach($value['archive_enquiry']['branch_id']);
            ?>
           <span style="color:red; font-size:12px;">(<?php echo $branch['enroll_code'];?>) </span>
            <?php }else{ ?>
              <a href="javascript:void(0)" data-id="<?php echo $value['archive_enquiry']['enq_id']; ?>" class="call-enquiry"><i class="fa fa-phone change-icon<?php echo $value['archive_enquiry']['enq_id']; ?>" aria-hidden="true"></i></a> 
            <?php }  //var_dump($value['Enquiry']['phone']);
  if($value['archive_enquiry']['ip']!='' && $this->request->session()->read("Auth.User.role_id")=='1'){
       echo "<br>".$value['archive_enquiry']['ip'];
     }
          ?></a>
         
         
            </td>
          <td style="text-align: left"><?php echo $value['archive_enquiry']['phone']?></td>
          <?php //echo $value['archive_enquiry']['email']?>
          
          
          <td style="text-align: left"><?php  if($value['archive_enquiry']['course_id']){ 
       $crsname=$this->Comman->crs($value['archive_enquiry']['course_id']);
			 echo $crsname['sname'];
      }else{
				 echo "Other";
				 }  ?>
			  
			  </td>
           <td style="text-align: left"><?php echo $mode_list[$value['archive_enquiry']['reference']]; if($value['archive_enquiry']['type']=="Contest"){ ?>(<b><?php echo $value['archive_enquiry']['type'];?></b>) </br>
 <?php if($value['archive_enquiry']['sc_name']){ ?>Detail :<b><?php echo $value['archive_enquiry']['sc_name'];?></b><? } ?><? } ?></td>
            
          <td style="text-align: left"><?php
    if($value['archive_enquiry']['shiftfrom']!="0" && $value['archive_enquiry']['shiftfrom']!=null)
{
      
   
     echo("<b>Shifted from ".$branhces[$value['archive_enquiry']['shiftfrom']]."</b><br>");
    
}    ?>
            
            <?php  if($value['f_date']>0){
             echo strftime('%b %d, %Y',strtotime($value['f_date'])); }else{
				 
				 echo strftime('%b %d, %Y',strtotime($value['archive_enquiry']['add_date']));
				 }?></td>
            <td><?php echo strftime('%b %d, %Y',strtotime($value['archive_enquiry']['add_date']));?></td>
          <td style="text-align: left"><?php if($value['f_responce']) {
   echo $value['f_responce']; 
    } 

else { 
  
  echo("No remark"); 
      } ?>
    <?php 
    $recording=$this->Comman->recording($value['archive_enquiry']['phone']);
    if($recording['recording_url']){
      echo '<a target="_blank" style="font-size:14px;float:right" href="'.$recording['recording_url'].'"><i class="fa fa-play-circle-o" aria-hidden="true"></i>
      </a>';
    }
    ?>  
    </td>
          <td>
          
             <?php if($this->request->session()->read("Auth.User.role_id")==1) {
echo $this->Html->link(__(''), array('action' => 'delete', $value['archive_enquiry']['enq_id']), array('class'=>'fa fa-trash','title'=>'Delete','style'=>'font-size: 15px !important;'), __('Are you sure you want to delete # %s?', $value['archive_enquiry']['enq_id'])); }
 ?> &nbsp;   <?php
if($value['archive_enquiry']['important']!=1){

   echo $this->Html->link(__(''), array('action' => 'impo', $value['archive_enquiry']['enq_id'],$value['archive_enquiry']['phone'],$value['archive_enquiry']['branch_id']), array('class'=>'fa fa-info','title'=>'Important','style'=>'font-size: 15px !important;'));
}
?>
<?php /* <a class='inline btn btn4 btn_edit_a' href="#inline_content<?php echo $value['Enquiry']['enq_id'];?>"></a>
            if($value['Enquiry']['status']==0)
{
                               echo $this->Html->link(__(''), array('action' => 'activ', $value['Enquiry']['enq_id']), array('class'=>'btn btn4 btn_act_a','title'=>'Activate'));
}        


*/
?></td>
        </tr>

                                       
                        <?php $count++; }  ?>
                        <?php } else{  ?>
                        <tr>
                            <td colspan="7" align="center">No Meta Available</td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
              
                    <?php echo $this->element('admin/pagination'); ?> 
              </div>
       
          </div>
        
       </div>
     
    </div>
    
  </section>
    
       
 </div>    

 <script>
  $(document).on('click','.followupps',function (e){
  e.preventDefault();
  $('#exampleModalfollow').modal('show').find('.modal-body').load($(this).attr('href'));
});

</script>


<!-- Modal -->
<div class="modal fade" id="exampleModalfollow" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
<div class="modal-header" style="background-color: #3c8dbc;color: #fff;">
        <h5 class="modal-title" id="exampleModalLabel">Enquiry Followup Form</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       Please wait while we fetching data result....
      </div>
    
    </div>
  </div>
</div>
