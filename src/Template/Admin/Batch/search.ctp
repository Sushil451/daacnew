
      <table  class="table table-bordered table-striped afTblStndRept" id="example3" width="100%">
                <thead class="thead-dark">
                    <tr>
                    <th class="head0">S.No.</th>
                            <th class="head0">Batch Code</th>
                            <th class="head0">Batch Type</th>
                            <th class="head0">Branch</th>
                            <th class="head1">Start Date</th>
                            <th class="head1">End Date</th>
                            <th class="head1">Course</th>
                            <th class="head1">Time</th>
                            <th class="head1">Action</th>

                       
                        </tr>
                    </thead>
                    <tbody>
                    <?php $cnt = 1; foreach ($batch as $key => $value) {	?>
                        <tr class="gradeX">
  <td><?php echo $cnt; $cnt++; ?></td>
                            <td style="text-align:left">
                                <!-- POP-UP --->
                                <a href="<?php echo SITE_URL; ?>admin/batch/getbatchstudent/<?php echo $value['b_id']; ?>" class="batch_code"  data-bs-toggle="modal" data-bs-target="#exampleModal"   
                                    style="color:red; font-size:14px;margin-right:0px"><i class="fa fa-file-pdf-o" aria-hidden="true" style="margin-left:5px"></i>
                                </a>
                                <?php echo strtoupper($value['batch_code']); ?>
                                <a href="#" data-id="<?php echo $value['b_id']; ?>" title="Add Assignment"
                                    class="add_assign pull-right" style="color:red; font-size:14px;margin-left:0px"><i
                                        class="fa fa-plus" aria-hidden="true" style="margin-left:5px"></i>
                                </a>
                            </td>
                            <td><?php echo $value['batch_type']; ?></td>


                            <td><?php echo $value['branch']['name']; ?></td>
                      <td><?php echo strftime('%d-%b-%Y', strtotime($value['start_date'])); ?></td>

                            <td><?php echo strftime('%d-%b-%Y', strtotime($value['end_date'])); ?></td>
                            <td>

                                <?php
if ($value['q_cat'] != 0) {
		echo $value['question_category']['qc_name'];
	} else {
		$coursename = $this->Comman->crs($value['course_id']);

		echo $coursename['sname'];?>


                                <?php } ?>
                            </td>
                            <td><?php echo $value['starttime']?></td>
                            <td style="display: flex;justify-content: space-between;align-items: center;">

                                <?php if ($value['status'] == 'N') {?>
            </div>

            <?php $users = $this->request->session()->read('Auth.User.role_id');

		if ($users == 1) { ?>
            <a href="<?php echo SITE_URL; ?>admin/batch/addstudentbatch/<?php echo $value['b_id']; ?>/<?php echo $value['q_cat']; ?>" class="add_student"  data-bs-toggle="modal" data-bs-target="#exampleModal34"   title="Add Student" ><i class="fa fa-user-plus" aria-hidden="true"></i>
            </a>
            <?php
			echo $this->Html->link(__(''), array('action' => 'add', $value['b_id']), array('class' => 'fa fa-pencil-square-o', 'title' => 'Edit','style'=>'margin-top:3px'));

			echo $this->Html->link(__(''), array('action' => 'delete', $value['b_id']), array('class' => 'fa fa-trash', 'title' => 'Delete'), __('Are you sure you want to delete # %s?', $value['b_id']));

			?>
            <a href="<?php echo SITE_URL; ?>admin/batch/markcomplete/<?php echo $value['b_id']; ?>/<?php echo $value['q_cat']; ?>"  data-bs-toggle="modal" data-bs-target="#exampleModal35"   class="markcomplete" data-id="<?php echo $value['b_id']; ?>" title="Mark as Complete"><i
                    class="fa fa-check" aria-hidden="true"></i></a>

            <?php }} else {?>



            <?php $users = $users = $this->request->session()->read('Auth.User.role_id');?>
            <?php if ($users == 1) {?>

            <a href="<?php echo ADMIN_URL; ?>batch/batchstatus/<?php echo $value['b_id']; ?>"
                title="Mark Incomplete"><i class="fa fa-times" aria-hidden="true"></i></a>
            <a href="#" class="updatecomment" data-id="<?php echo $value['b_id']; ?>" title="Completed"><i
                    class="fa fa-check-circle" aria-hidden="true"></i></a>

            <?php } } ?>
            <?php if ($users == 1) {?>
            <a href="#send_msg-<?php echo $value['b_id']; ?>" class="inline" title="Send SMS"><i
                    class="fa fa-envelope" aria-hidden="true"></i>
            </a>
            <?php } ?>
            <div style='display:none'>
                <div class="studnt-my-pop" id="send_msg-<?php echo $value['b_id']; ?>"
                    style='padding:10px; background:#fff;'>

                    <div class="contform">
                        <?php echo $this->Form->create('SMS', array('url' => array('controller' => 'Batch', 'action' => 'send_msg'), 'inputDefaults' => array('div' => false, 'label' => false), 'class' => 'stdform')); ?>

                        <p><label>Type</label>
                            <span class="field">
                                <input type="radio" class="loginput" name="type"
                                    value="1">Closed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" class="loginput"
                                    name="type" value="2">Others
                            </span>
                        </p>
                        <p><label>Reason</label>
                            <span class="field">
                                <?php echo $this->Form->input('reason', array('class' => 'loginput', 'type' => 'textarea', 'id' => 'reason', 'onkeyup' => 'show_msg(this);')); ?>
                            </span>
                            <input type="hidden" name="branch" value="<?php echo $value['branch']['phone']; ?>">
                            <input type="hidden" name="batch_id" value="<?php echo $value['b_id']; ?>">
                        </p>
                        <p id="message"></p>

                        <p>
                            <label>&nbsp;</label>
                            <span class="field">
                                <?php echo $this->Form->submit('Send Message'); ?></span>
                        </p>
                        </form>
                        <h5>Batch Student Detail</h5>
                    </div>

                    <table cellpadding="0" cellspacing="0" border="0" class="stdtable stdtablecb" id="dyntablzze">
                        <thead>
                            <tr>

                                <th class="head0">S.No.</th>
                                <th class="head0 ">Name</th>
                                <th class="head0 ">Enrollment</th>
                                <th class="head0 ">Mobile</th>
                                <th class="head0 ">Course</th>


                            </tr>
                        </thead>
                        <tbody>

                            <?php $count = 1;foreach ($value['student_course'] as $val) { //pr($val);

		?>
                            <tr class="gradeX">

                                <td><?php echo $count;
		$count++; ?></td>

                                <td><?php echo $val['student']['s_name']; ?></td>
                                <td><?php echo $val['student']['s_enroll']; ?></td>
                                <td><?php echo $val['student']['s_contact']; ?></td>
                                <td><?php echo $val['course']['sname']; ?></td>


                            </tr>
                            <?php }?>

                        </tbody>

                    </table>

                </div>
            </div>
        </div>
        </td>

        </tr>

        <?php }?>
        </tbody>
        </table>
                <?php  //if(count($destination)>10){ //pr($events);?>
                    <?php echo $this->element('admin/pagination'); ?> 
           