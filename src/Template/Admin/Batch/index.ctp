<style type="text/css">
   th{
text-align: center;
   }
   td{
    text-align: center;

   }


   .action_icon a span {
    font-size: 14px !important;
}
 .action_icon a {
    font-size: 13px !important;
}
.action_icon b i {
    font-size: 13px !important;
}

.action_icon a {
    display: inline-block;
    width: 21px;
    text-align: center;
}
 </style>
 <div class="content-wrapper">
   <section class="content-header">
    <h1>
         Batch Manager
    </h1>
    <ol class="breadcrumb">
    <li><a href="<?php echo SITE_URL; ?>admin/dashboards"><i class="fa fa-home"></i>Home</a></li>
<li><a href="<?php echo SITE_URL; ?>admin/batch/discount">Batch Manager</a></li>
    </ol> 
    </section> <!-- content header -->

    <!-- Main content -->
    <section class="content">
  <div class="row">
    <div class="col-xs-12">    
  <div class="box">
  <div class="box-header">
  <?php echo $this->Flash->render(); ?>


  <?php $role_id=$this->request->session()->read('Auth.User.role_id'); ?>
            <script>          
              $(document).ready(function () { 
                $("#Stsearch").bind("click", function (event) {
                 $('.lds-facebook').show();
                  $.ajax({
                    async:true,
                    data:$("#Stsearch").serialize(),
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>batch/search",
                    success:function (data) {
                      $('.lds-facebook').hide();   
                      $("#Mycity").html(data); },
                    });
                  return false;
                });


                $("#Stsearch").bind("keyup", function (event) {
                $('.lds-facebook').show();
                  $.ajax({
                    async:true,
                    data:$("#Stsearch").serialize(),
                    dataType:"html",
                    type:"POST",
                    url:"<?php echo ADMIN_URL ;?>batch/search",
                    success:function (data) {
                     $('.lds-facebook').hide();   
                      $("#Mycity").html(data); },
                    });
                  return false;
                });




              });

              $(document).on('click', '.pagination a', function(e) {
                var target = $(this).attr('href');
                var res = target.replace("/batch/search", "/batch");
                window.location = res;
                return false;
              });
            </script>

            
            <div class="form-group studentDtlFrmGroup d-flex justify-content-between" >
            <div class="col-md-6 align-self-center">
            <?php echo $this->Form->create('Stsearch',array('type'=>'post','inputDefaults'=>array('div'=>false,'label'=>false),'id'=>'Stsearch','class'=>'form-horizontal mb-0','method'=>'POST')); ?>
            
           
             <div class="d-flex row">
           
              
                
<?php if ($this->request->session()->read('Auth.User.role_id') == 1) {  ?>

  <div class="col-md-6 ">

  <?php
  
  echo $this->Form->input('branch_id', array('class' =>'form-control','id'=>'b_id','label'=>false,'options'=>$branch,'empty'=>'--Select Branch--','autofocus'));
?>

</div>

<?php 
 }else{
    
    echo $this->Form->input('branch_id', array('class' =>'form-control','id'=>'b_id','label'=>false,'value'=>$this->request->session()->read('Auth.User.branch'),'type'=>'hidden','autofocus'));  } ?>
              

               <div class="col-md-6"> 
              
                <?php 
                $type = array('N' => 'Running', 'Y' => 'Completed','delete'=>'Deleted');
                

  echo $this->Form->input('status', array('class' =>'form-control','id'=>'b_id','label'=>false,'options'=>$type,'empty'=>'--Select type--','autofocus'));
?>
                
            
            </div> 
             
            </div>
            
            <?php echo $this->Form->end(); ?>   
            </div>
  <div class="col-md-2">
    <ul class="list-inline w-75 m-auto text-center d-flex justify-content-around">
    <li class="list-inline-item">

   <a class="addnn" href="<?php echo ADMIN_URL; ?>batch/add"><i class="fa fa-user-plus" aria-hidden="true"></i>
     <br> New</a> 
            </li>

         


            </ul>
  <style>
        .addnn , .addnn:hover{ height:55px; width: 55px; border-radius: 50%; background-color: #2d95e3; line-height: 15px; font-size: 14px !important; text-align: center; color: #fff; display: inline-block; text-decoration: none;}
        .addnn i{ font-size:18px; color:#fff; margin-top:10px;}
        .studentDtlFrmGroup{ display:flex; flex-wrap:wrap;}
        .studentDtlFrmGroup>.col-md-10, .studentDtlFrmGroup>.col-md-2{ align-self:center !important;}
    </style>

            
              
                  <?php if($this->request->session()->read("Auth.User.role_id")==4)  {  
  $branchID = $this->request->session()->read("Auth.User.branch");
 echo $this->Form->input('branch_id', array('class' => 'smallselect branch','type' => 'hidden','value'=>$branchID));
}

 ?>
   <?php /*          
              <div class="col-sm-1">
                <label for="inputEmail3" class="control-label" style="color:white">Reset</label>
                <input type="reset" style="background-color:#d33c44;color:#fff;"  class="btn btn-success" value="Reset">       
              </div> 
              <div class="col-sm-1" style="margin-left: -40px;">
                <label for="inputEmail3" class="control-label" style="color:white">Search</label>
                <input type="submit" style="background-color:green;color:#fff;" id="Mysubscriptions" class="btn btn-success" value="Search">       
              </div> */ ?>
              </div>  
              
            
      
       

   
     </div><!-- /.box-header -->
      <div class="box-body discountFePg" id="Mycity"> 
      <table  class="table table-bordered table-striped afTblStndRept" id="example3" width="100%">
                <thead class="thead-dark">
                    <tr>
                    <th class="head0">S.No.</th>
                            <th class="head0">Batch Code</th>
                            <th class="head0">Batch Type</th>
                            <th class="head0">Branch</th>
                            <th class="head1">Start Date</th>
                            <th class="head1">End Date</th>
                            <th class="head1">Course</th>
                            <th class="head1">Time</th>
                            <th class="head1">Action</th>

                       
                        </tr>
                    </thead>
                    <tbody>
                    <?php $cnt = 1; foreach ($batch as $key => $value) {	?>
                        <tr class="gradeX">
  <td><?php echo $cnt; $cnt++; ?></td>
                            <td style="text-align:left">
                                <!-- POP-UP --->
                                <a href="<?php echo SITE_URL; ?>admin/batch/getbatchstudent/<?php echo $value['b_id']; ?>" class="batch_code"  data-bs-toggle="modal" data-bs-target="#exampleModal"   
                                    style="color:red; font-size:14px;margin-right:0px"><i class="fa fa-file-pdf-o" aria-hidden="true" style="margin-left:5px"></i>
                                </a>
                                <?php echo strtoupper($value['batch_code']); ?>
                                <a href="#" data-id="<?php echo $value['b_id']; ?>" title="Add Assignment"
                                    class="add_assign pull-right" style="color:red; font-size:14px;margin-left:0px"><i
                                        class="fa fa-plus" aria-hidden="true" style="margin-left:5px"></i>
                                </a>
                            </td>
                            <td><?php echo $value['batch_type']; ?></td>


                            <td><?php echo $value['branch']['name']; ?></td>
                      <td><?php echo strftime('%d-%b-%Y', strtotime($value['start_date'])); ?></td>

                            <td><?php echo strftime('%d-%b-%Y', strtotime($value['end_date'])); ?></td>
                            <td>

                                <?php
if ($value['q_cat'] != 0) {
		echo $value['question_category']['qc_name'];
	} else {
		$coursename = $this->Comman->crs($value['course_id']);

		echo $coursename['sname'];?>


                                <?php } ?>
                            </td>
                            <td><?php echo $value['starttime']?></td>
                            <td style="display: flex;justify-content: space-between;align-items: center;">

                                <?php if ($value['status'] == 'N') {?>
            </div>

            <?php $users = $this->request->session()->read('Auth.User.role_id');

		if ($users == 1) { ?>
            <a href="<?php echo SITE_URL; ?>admin/batch/addstudentbatch/<?php echo $value['b_id']; ?>/<?php echo $value['q_cat']; ?>" class="add_student"  data-bs-toggle="modal" data-bs-target="#exampleModal34"   title="Add Student" ><i class="fa fa-user-plus" aria-hidden="true"></i>
            </a>
            <?php
			echo $this->Html->link(__(''), array('action' => 'add', $value['b_id']), array('class' => 'fa fa-pencil-square-o', 'title' => 'Edit','style'=>'margin-top:3px'));

			echo $this->Html->link(__(''), array('action' => 'delete', $value['b_id']), array('class' => 'fa fa-trash', 'title' => 'Delete'), __('Are you sure you want to delete # %s?', $value['b_id']));

			?>
            <a href="<?php echo SITE_URL; ?>admin/batch/markcomplete/<?php echo $value['b_id']; ?>/<?php echo $value['q_cat']; ?>"  data-bs-toggle="modal" data-bs-target="#exampleModal35"   class="markcomplete" data-id="<?php echo $value['b_id']; ?>" title="Mark as Complete"><i
                    class="fa fa-check" aria-hidden="true"></i></a>

            <?php }} else {?>



            <?php $users = $users = $this->request->session()->read('Auth.User.role_id');?>
            <?php if ($users == 1) {?>

            <a href="<?php echo ADMIN_URL; ?>batch/batchstatus/<?php echo $value['b_id']; ?>"
                title="Mark Incomplete"><i class="fa fa-times" aria-hidden="true"></i></a>
            <a href="#" class="updatecomment" data-id="<?php echo $value['b_id']; ?>" title="Completed"><i
                    class="fa fa-check-circle" aria-hidden="true"></i></a>

            <?php } } ?>
            <?php if ($users == 1) {?>
            <a href="#send_msg-<?php echo $value['b_id']; ?>" class="inline" title="Send SMS"><i
                    class="fa fa-envelope" aria-hidden="true"></i>
            </a>
            <?php } ?>
            <div style='display:none'>
                <div class="studnt-my-pop" id="send_msg-<?php echo $value['b_id']; ?>"
                    style='padding:10px; background:#fff;'>

                    <div class="contform">
                        <?php echo $this->Form->create('SMS', array('url' => array('controller' => 'Batch', 'action' => 'send_msg'), 'inputDefaults' => array('div' => false, 'label' => false), 'class' => 'stdform')); ?>

                        <p><label>Type</label>
                            <span class="field">
                                <input type="radio" class="loginput" name="type"
                                    value="1">Closed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" class="loginput"
                                    name="type" value="2">Others
                            </span>
                        </p>
                        <p><label>Reason</label>
                            <span class="field">
                                <?php echo $this->Form->input('reason', array('class' => 'loginput', 'type' => 'textarea', 'id' => 'reason', 'onkeyup' => 'show_msg(this);')); ?>
                            </span>
                            <input type="hidden" name="branch" value="<?php echo $value['branch']['phone']; ?>">
                            <input type="hidden" name="batch_id" value="<?php echo $value['b_id']; ?>">
                        </p>
                        <p id="message"></p>

                        <p>
                            <label>&nbsp;</label>
                            <span class="field">
                                <?php echo $this->Form->submit('Send Message'); ?></span>
                        </p>
                        </form>
                        <h5>Batch Student Detail</h5>
                    </div>

                    <table cellpadding="0" cellspacing="0" border="0" class="stdtable stdtablecb" id="dyntablzze">
                        <thead>
                            <tr>

                                <th class="head0">S.No.</th>
                                <th class="head0 ">Name</th>
                                <th class="head0 ">Enrollment</th>
                                <th class="head0 ">Mobile</th>
                                <th class="head0 ">Course</th>


                            </tr>
                        </thead>
                        <tbody>

                            <?php $count = 1;foreach ($value['student_course'] as $val) { //pr($val);

		?>
                            <tr class="gradeX">

                                <td><?php echo $count;
		$count++; ?></td>

                                <td><?php echo $val['student']['s_name']; ?></td>
                                <td><?php echo $val['student']['s_enroll']; ?></td>
                                <td><?php echo $val['student']['s_contact']; ?></td>
                                <td><?php echo $val['course']['sname']; ?></td>


                            </tr>
                            <?php }?>

                        </tbody>

                    </table>

                </div>
            </div>
        </div>
        </td>

        </tr>

        <?php }?>
        </tbody>
        </table>
                <?php  //if(count($destination)>10){ //pr($events);?>
                    <?php echo $this->element('admin/pagination'); ?> 
            </div>
       
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
       </div>
      <!-- /.col -->  
    </div>
     <!-- /.row -->      
  </section>
    <!-- /.content -->  
       
 </div>     
     <!-- /.   content-wrapper -->  
     
     

      <script>
  $(document).on('click','.batch_code',function (e){
  e.preventDefault();
  $('#exampleModal').modal('show').find('.modal-body').load($(this).attr('href'));
});

</script>
      <script>
  $(document).on('click','.markcomplete',function (e){
  e.preventDefault();
  $('#exampleModal35').modal('show').find('.modal-body').load($(this).attr('href'));
});

</script>

<script>
  $(document).on('click','.drop_click',function (e){
  e.preventDefault();
  $('#exampleModal6').modal('show').find('.modal-body').load($(this).attr('href'));
});

</script>

<script>
  $(document).on('click','.add_student',function (e){
  e.preventDefault();
  $('#exampleModal34').modal('show').find('.modal-body').load($(this).attr('href'));
});

</script>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
<div class="modal-header" style="background-color: #3c8dbc;color: #fff;">
        <h5 class="modal-title" id="exampleModalLabel">Batch Student Detail</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       Please wait while we fetching data result....
      </div>
    
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal35" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
<div class="modal-header" style="background-color: #3c8dbc;color: #fff;">
        <h5 class="modal-title" id="exampleModalLabel">Mark Complete Detail</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       Please wait while we fetching data result....
      </div>
    
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModal6" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
<div class="modal-header" style="background-color: #3c8dbc;color: #fff;">
        <h5 class="modal-title" id="exampleModalLabel">Drop Student</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       Please wait while we fetching data result....
      </div>
    
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal34" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
<div class="modal-header" style="background-color: #3c8dbc;color: #fff;">
        <h5 class="modal-title" id="exampleModalLabel">Add Student</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       Please wait while we fetching data result....
      </div>
    
    </div>
  </div>
</div>

