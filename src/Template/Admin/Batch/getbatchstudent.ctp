
<div class="feespopup" id='inline_contentinline_contentdrop' style="background:#fff;"> 


  <div class="fplan">  
  


<table class="table table-bordered table-striped" id="followdetails">
                    <thead class="thead-dark">
                    <tr>

<th class="head0">S.No.</th>
<th class="head0">Branch</th>
<th class="head0 ">Name</th>
<th class="head0 ">Enrollment</th>
<th class="head0 ">Image</th>
<th class="head0 ">Mobile</th>
<th class="head0 ">Course</th>
<?php if ($this->request->session()->read('Auth.User.role_id') == 1) {?>
<th class="head0 action">Action</th>
<?php } ?>


</tr>
                    </thead>
                    <tbody>
                    <?php $count = 1;if (!empty($stu_ids)) {
    foreach ($stu_ids as $val) { //dd($val); die;
        if ($val['student']['drop_out'] == 1) {
            continue;
        }
        ?>
        <tr <?php if ($val['status'] == 'N') {?> style="color:red" <?php }?>>

            <td><?php echo $count;
        $count++; ?></td>
            <td><?php echo $val['branch']['name'] . '<br>'; ?>
                <?php if ($val['status'] == 'N') {?>Drop
                Reason:<?php echo $val['drop_comment']; ?><?php }?>
            </td>
            <td><?php echo ucwords($val['student']['s_name']); ?><?php if ($val['status'] == 'N') {?>
                <br>Drop date:<?php echo date('d-m-Y', strtotime($val['drop_date']));} ?>
            </td>
            <td><?php echo $val['student']['s_enroll']; ?></td>
            <td><?php
$path = LOCALWEB. $val['student']['image'];
        if (!empty($val['student']['image']) && file_exists($path)) {?><img
                    src="<?php echo SITE_URL; ?>images/student/<?php echo $val['student']['image']; ?>"
                    style="width:75px; height:75px"><?php } else {?> <img
                    src="<?php echo SITE_URL; ?>images/student/noimage.jpg" style="width:75px; height:75px"> <?php }?>
            </td>
            <td><?php echo $val['student']['s_contact']; ?></td>
            <td><?php echo $val['question_category']['qc_name']; ?></td>
            <?php if ($this->request->session()->read('Auth.User.role_id') == 1) {?>
            <td class="delete_stu">
              <a href="<?php echo SITE_URL; ?>admin/batch/deletestu/<?php echo $val['id']; ?>/<?php echo $val['sub_id']; ?>">Delete</a>
                <?php if ($val['status'] != 'N') {?>
                <a href="<?php echo SITE_URL; ?>admin/batch/dropstu/<?php echo $val['id']; ?>/<?php echo $val['sub_id']; ?>/<?php echo $val['student']['s_id']; ?>" class="drop_click"  data-bs-toggle="modal" data-bs-target="#exampleModal6" >Drop</a> <?php }?>
            </td> <?php }?>


        </tr>
        <?php }} else {?>
        <tr class="gradeX">
            <td colspan="8" style="text-align:center">No Data Available</td>
        </tr>
        <?php }?>
                      </tbody>
                    
                  </table>
                  
  
</div>
</div>
<div class="modal-footer box footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> 
      
      </div>
     