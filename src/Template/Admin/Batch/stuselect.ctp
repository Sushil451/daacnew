
          
<table class="table table-bordered table-striped" >
                    <thead class="thead-dark">
                        <tr>

                         <th    class="head0">S.No.</th>
                         <th    class="head0">Branch</th>
                         <th    class="head0 ">Name</th>
                         <th    class="head0 ">Enrollment</th>
                         <th    class="head0 ">Action</th>
                                     
                       </tr>
                    </thead>
           <tbody id="stu_listbody">

<?php $count=1; if(!empty($students)){
  $s_id=array();
  $br_id=array();
  foreach($students as $val) {  
            $s_id[]=$val['student']['s_id'];
            $br_id[]=$val['branch']['id'];
           
            ?>
                <tr>
             
          <td><?php echo $count; ?></td>
                  <td><?php echo $val['branch']['name'];  ?></td>
                  <td><?php echo $val['student']['s_name'];  ?></td>
                  <td><?php echo $val['student']['s_enroll']; ?></td>
                  <td><button name="add" value="<?php echo $count; ?>" class="add_stud btn btn-success">Add</button></td>  
                </tr>
                   <?php  $count++; } }
                   else{?>
                   <tr class="gradeX">
                   <td colspan="8" style="text-align:center">No Data Available</td>
                   </tr>
                   <?php }?>
                   </tbody>
                   </table>

                   <script>
                       $(document).ready(function(){
                    $('.add_stud').click(function(e){
                      e.preventDefault();
                     var i= $(this).val()-1;
                     var s_id=<?php echo json_encode($s_id);?>;
                     var br_id=<?php echo json_encode($br_id);?>;
                     var sub_id="<?php echo $sub_id;?>"; 
                     var batch_id="<?php echo $batch_id;?>";
                     $.ajax({
                async: true,
                data: {
                  s_id:s_id[i],
                  branch_id:br_id[i],
                  batch_id:batch_id,
                  sub_id:sub_id
                },
                dataType: "html",
                type: "POST",
                beforeSend: function() {
                    $('.lds-facebook').show();
                },

                url: "<?php echo ADMIN_URL; ?>batch/studentadd",
                success: function(data) {
                    $('.name_ser').val('');
                    $('.enroll_ser').val('');
                    $("#update_stu").html('');
                    

                    $.ajax({
          type: 'POST',
          url: "<?php echo ADMIN_URL; ?>batch/aftergetbatchstudent",
          data: {batchid:batch_id},
        
          beforeSend: function() {
           
  
          },
          success: function(data) { 
            $('.lds-facebook').hide();
          $('#stu_batches').html(data);
          
          },
          complete: function() {
            
       
  
          },
          error: function(data) {
              alert(JSON.stringify(data));
  
          }
      });
                },

            });
                });
            });  
                   </script>