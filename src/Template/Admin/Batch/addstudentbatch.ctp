
<div class="feespopup" id='inline_contentinline_contentdrop' style="background:#fff;"> 


  <div class="fplan">  
  <?php echo $this->Form->create('Student',array('type'=>'file','inputDefaults'=>array('div'=>false,'label'=>false),'class'=>'stdform dropupStudPop addStdntsFrm needs-validation')); ?> 
        <div class="feespopup"  style=' background:#fff;'>

        <table class="table table-bordered table-striped afTblStndRept" width="100%">
               
              
               <tbody>
              <tr>
              <div class="row">
                       <div class="col-md-12">
                     
        <input type="hidden" name="subject_id" id="sub_ids" value="<?php echo $q_cat; ?>">
        <input type="hidden" name="batch_id" id="bat_id"  value="<?php echo $id; ?>">
           
             <div class="box box-info boxInfoBx boxInfoBxStudentInfo boxInfoBxStudentInfoMB0">
             <div class="box-body addStdntsFrm">
                             
                         
                          <div class="row d-flex">
           <div class="col-sm-6 align-self-center" >
           <strong>Name:</strong>
           
           <input class="longinput form-control name_ser" style="margin-bottom:20px"
                            id="name_ser" type="text">
             </div>
           
           
           
            <div class="col-sm-6 align-self-center">
                          <strong>Enroll: </strong>
                          <input class="longinput form-control enroll_ser" style="margin-bottom:20px"
                            id="enroll_ser" type="text">
                          </div>
                          
                          </div>
                         
                         </div>
                        
                        </div>
                        
                         </div>
           
               </tr>
               
               
                 
               
           </tbody>
           </table>
<div id="update_stu"></div>
<table class="table table-bordered table-striped" id="stu_batches">
                    <thead class="thead-dark">
                    <tr>

<th class="head0">S.No.</th>
<th class="head0">Branch</th>
<th class="head0 ">Name</th>
<th class="head0 ">Enrollment</th>
<th class="head0 ">Image</th>
<th class="head0 ">Mobile</th>
<th class="head0 ">Course</th>


</tr>
                    </thead>
                    <tbody>
                    <?php $count = 1;if (!empty($stu_ids)) {
    foreach ($stu_ids as $val) { //dd($val); die;
        if ($val['student']['drop_out'] == 1) {
            continue;
        }
        ?>
        <tr <?php if ($val['status'] == 'N') {?> style="color:red" <?php }?>>

            <td><?php echo $count;
        $count++; ?></td>
            <td><?php echo $val['branch']['name'] . '<br>'; ?>
                <?php if ($val['status'] == 'N') {?>Drop
                Reason:<?php echo $val['drop_comment']; ?><?php }?>
            </td>
            <td><?php echo ucwords($val['student']['s_name']); ?><?php if ($val['status'] == 'N') {?>
                <br>Drop date:<?php echo date('d-m-Y', strtotime($val['drop_date']));} ?>
            </td>
            <td><?php echo $val['student']['s_enroll']; ?></td>
            <td><?php
$path = LOCALWEB. $val['student']['image'];
        if (!empty($val['student']['image']) && file_exists($path)) {?><img
                    src="<?php echo SITE_URL; ?>images/student/<?php echo $val['student']['image']; ?>"
                    style="width:75px; height:75px"><?php } else {?> <img
                    src="<?php echo SITE_URL; ?>images/student/noimage.jpg" style="width:75px; height:75px"> <?php }?>
            </td>
            <td><?php echo $val['student']['s_contact']; ?></td>
            <td><?php echo $val['question_category']['qc_name']; ?></td>
           


        </tr>
        <?php }} else {?>
        <tr class="gradeX">
            <td colspan="7" style="text-align:center">No Data Available</td>
        </tr>
        <?php }?>
                      </tbody>
                    
                  </table>
                  
  
</div>
</div>
<div class="modal-footer box footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> 
      
      </div>
     <script>
$(document).ready(function(){
      $('.name_ser').keyup(function() {
        var batch_id = $('#bat_id').val();
        var sub_id = $('#sub_ids').val();
        var name = $(this).val();
        var type = "name";

        if (name != "") {
            $.ajax({
                async: true,
                data: {
                    name: name,
                    type: type,
                    batch_id: batch_id,
                    sub_id: sub_id
                },
                dataType: "html",
                type: "POST",
                beforeSend: function() {
                    $('.lds-facebook').show();
                },

                url: "<?php echo ADMIN_URL; ?>batch/stuselect",
                success: function(data) {

                    $("#update_stu").html(data);
                    $('.lds-facebook').hide();
                },

            });
            return false;
        }
    });
    });

    $(document).ready(function(){
    $('.enroll_ser').keyup(function() {
        var batch_id = $('#bat_id').val();
        var sub_id = $('#sub_ids').val();
        var enroll = $(this).val();
        var type = "enroll";

        if (enroll != "") {
            $.ajax({
                async: true,
                data: {
                    enroll: enroll,
                    type: type,
                    batch_id: batch_id,
                    sub_id: sub_id
                },
                dataType: "html",
                type: "POST",
                beforeSend: function() {
                    $('.lds-facebook').show();
                },

                url: "<?php echo ADMIN_URL; ?>batch/stuselect",
                success: function(data) {

                    $("#update_stu").html(data);
                    $('.lds-facebook').hide();
                },

            });
            return false;
        }
    });

});


</script>