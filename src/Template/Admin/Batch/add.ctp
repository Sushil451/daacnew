<!-- <script>
   function checkextension() {
    var file = document.querySelector("#fUpload");
    if ( /\.(jpe?g|png|gif)$/i.test(file.files[0].name) === false ) 
      { alert("not an image please choose a image!");
    $('#fUpload').val('');
  }
  return false;
}
</script> -->
<style type="text/css">
.box.box-info{ margin-bottom:30px;}
	.text{color:red; font-size: 12px;}
	.filename{font-size: 11px;color: #e02727;}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<i class="fa fa-plus-square" aria-hidden="true"></i>  
						<?php if(isset($newresponse['b_id'])){ ?>  Edit Batch <?php } else { ?> 	 Add Batch <?php } ?>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo SITE_URL; ?>admin/batch"><i class="fa fa-home"></i>Home</a></li>
			<li><a href="<?php echo SITE_URL; ?>admin/batch">Manage Batch</a></li>
			<?php if(isset($newresponse['b_id'])){ ?>
				<li class="active"><a href="javascript:void(0)">Edit Batch</a></li>   
			<?php } else { ?>
				<li class="active"><a href="javascript:void(0)">Add Batch</a></li>
			<?php } ?>
		</ol>
	</section>
	<script>
function isNumber(evt) {

evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;
if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {

return false;
}
return true;
}


function mobileval(id) {
var tty=id.id;
var mobile = document.getElementById(tty).value; 
var pattern = /(0|6|7|8|9)\d{9}/; 
var len=$('#'+tty).val().length;
if (pattern.test(mobile) && len<12) {

return true;
}
else{  
  $('#'+tty).val('');
alert("Insert a valid mobile numbers");
return false;
}
}
</script>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
			<?php echo $this->Form->create($newresponse, array(
						'class'=>'form-horizontal needs-validation',
						'type' => 'file',
						'enctype'=>'multipart/form-data',
						'validate','novalidate'
						
					));
				
					 ?>

				<div class="box box-info">
					<?php echo $this->Flash->render(); ?>
					<div class="box-header with-border">
						<h3 class="box-title">Batch Info</h3>
					</div>
			
					<div class="box-body addStdntsFrm">
						<div class="row">
				<div class="col-sm-3">
		<label for="inputEmail3" class="control-label">Batch Code <strong style="color:red;">*</strong></label>

		<?php if($newresponse['b_id']){
			 ?>
	  <input type="hidden" name="b_id" value="<?php echo $newresponse['b_id']; ?>">

	  <?php } ?>
<?php echo $this->Form->input('batch_code', array('class' => 'form-control','required','label'=>false,'id'=>'b_code','placeholder'=>'Batch Code','autofocus','autocomplete'=>'off','onchange'=>'chkexits()')); ?>   
		<span style="color:red;display:none;" id="batch_error">Batch Code Already Exits </span>
							</div>
							<div class="col-sm-2">
								<label for="inputEmail3" class="control-label">Select Course <strong style="color:red;">*</strong></label>
								<?php echo $this->Form->input('q_cat', array('class' => 'form-control','empty'=>'Select Course','type'=>'select','required','options' =>$sub1,'id'=>'cour','label'=>false)); ?>   
							
                              
							</div>
							<div class="col-sm-2" id="subcat">
								<label for="inputEmail3" class="control-label">Select Faculty <strong style="color:red;">*</strong></label>

								<?php echo $this->Form->input('faculty', array('class' => 'form-control','empty'=>'Select Faculty','required','options' =>$faculty,'id'=>'cour','label'=>false)); ?> 

							
							</div>
							<div class="col-sm-2">
								<label for="inputEmail3" class="control-label">Batch Conducted From <strong style="color:red;">*</strong></label>
								
 <?php echo $this->Form->input('branch_id', array('class' => 'form-control','required','empty'=>'Select Branch','options' =>$brn,'id'=>'brnch','default'=>2,'label'=>false)); 

?>
							</div>
							</div>

							<div class="row">
							<div class="col-sm-3">
							
        
						<label for="inputEmail3" class="control-label">Course Type <strong style="color:red;">*</strong></label><br><br>
						
						<span class="field" style="padding:7px 0px;">
<input type="radio" name="batch_type" id="Online" class="smallselect coursetype" empty="Select Course" required="required" value="online" <?php if($newresponse['batch_type']=="online"){
			 ?> checked="checked" <?php } ?> >&nbsp;Online


<input type="radio" name="batch_type" id="Offline" class="smallselect coursetype" empty="Select Course" required="required" value="offline" <?php if($newresponse['batch_type']=="offline"){
			 ?> checked="checked" <?php } ?>>&nbsp;Offline </span>
				
							</div>
						

<div class="col-sm-2">
		<label for="inputEmail3" class="control-label">Start Date<strong style="color:red;">*</strong> </label>

						

						<div class="input-append date" id="datepickerbrth" data-date="<?php echo date('d-m-Y',strtotime($newresponse['start_date'])); ?>" data-date-format="dd-mm-yyyy"> 
							
						<input class="span2 form-control" size="16" placeholder="Birthdate Date" type="date" name="start_date" value="<?php echo date('d-m-Y',strtotime($newresponse['start_date'])); ?>">   <span class="add-on"><i class="icon-th"></i></span>
						</div>
		
							</div>
							

							<div class="col-sm-2">
								<label for="inputEmail3" class="control-label">End Date <strong style="color:red;">*</strong></label>

								<div class="input-append date" id="datepickerbrth" data-date="<?php echo date('d/m/Y',strtotime($newresponse['end_date'])); ?>" data-date-format="dd-mm-yyyy"> 
							
							<input class="span2 form-control" size="16" placeholder="Birthdate Date" type="date" name="end_date" value="<?php echo date('d/m/Y',strtotime($newresponse['end_date'])); ?>">  <span class="add-on"><i class="icon-th"></i></span>
							</div>
							
							</div>
							<div class="col-sm-2">
								
							
							<label for="inputEmail3" class="control-label">Time<strong style="color:red;">*</strong></label>
							<input type="text" class="form-control"  
							<?php if($newresponse['starttime']){  ?> value="<?php echo $newresponse['starttime']; ?>" <?php } ?> name="starttime" required>
							<span style="color:green">4:00PM-5:00PM</span>
								   
							</div>
							

							
        </div>

		
							
		<div id="fstbl" class="col-sm-6" >
             
			 </div>
			<!-- col-md-3 -->



						
						</div> 
<!-- row -->

<div class="box-footer" id="sub">
						<?php echo $this->Html->link('Back', ['action' => 'index'],['class'=>'btn btn-default']); ?>
						<?php if($newresponse['s_id']){ ?>
							<?php echo $this->Form->submit('Update',array('class' => 'btn btn-info pull-right', 'title' => 'Update')); ?>
							<?php }else{ ?>
							<?php echo $this->Form->submit('Save',array('class' => 'btn btn-info pull-right', 'title' => 'Save')); ?>
							<?php } ?>
						</div>

					
	




					</div> 
					<!--  -->

		</div>

<!-- box-end -->







<?php echo $this->Form->end(); ?>
				</div>
			</div>
		</section>
	</div> 
<script type="text/javascript">
  

  function chkexits() {
    var email = $('#b_code').val();
  //  alert(email);
    var data = {
        'email': email
    };
    $.post("<?php echo SITE_URL; ?>/admin/batch/checkmail", data, function(data) {
        var response = jQuery.parseJSON(data);
        if (response.chk == true) {
            $('#b_code').val("");
            $('#batch_error').css('display','block');

            check = false;
        } else {
            $('#batch_error').css('display','none');

            check = true;
        }

    });
    return check;
}
</script>

<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
    
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>