<?php echo $this->Form->create('Student',array('type'=>'file','inputDefaults'=>array('div'=>false,'label'=>false),'class'=>'stdform dropupStudPop addStdntsFrm needs-validation')); ?> 

<input type="hidden" name="id" id="statusid" value="<?php echo $id; ?>">
<input type="hidden" name="status" value="Y">
<div class="feespopup" id='inline_contentinline_contentdrop' style="background:#fff;"> 
  <div class="fplan">  
 

 
        <div class="feespopup"  style='background:#fff;'>

        <table class="table table-bordered table-striped afTblStndRept" width="100%">
               
              
               <tbody>
              <tr>
              <div class="row">
                       <div class="col-md-12">
                     
    
           
             <div class="box box-info boxInfoBx boxInfoBxStudentInfo boxInfoBxStudentInfoMB0">
             <div class="box-body addStdntsFrm">
                             
                         
             <div class="row d-flex">
<div class="col-sm-6 align-self-center" >
<strong>Completion Date:</strong>
<div class="input-append date" id="datepickerbrth" data-date="<?php echo date('d/m/Y'); ?>" data-date-format="dd-mm-yyyy"> 
							
						<input class="span2 form-control" size="16" required placeholder="Drop Date" type="date" name="date" value="<?php echo date('d/m/Y'); ?>">  <span class="add-on"><i class="icon-th"></i></span>
					
					</div>
  </div>



 <div class="col-sm-6 align-self-center">
               <strong>Comments: </strong>
               <?php echo $this->Form->input('comment', array('label'=>false,'class' => 'form-control','required')); ?>
               </div>
               
               </div>
                          
                          </div>
                         
                         </div>
                        
                        </div>
                        
                         </div>
           
               </tr>
               
               
                 
               
           </tbody>
           </table>
  
</div>
</div>
<div class="modal-footer box footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> 
        <?php echo $this->Form->submit('Submit',array('class' => 'btn btn-success')); ?>
      </div>
    
      </div>  <?php echo $this->Form->end();?> 