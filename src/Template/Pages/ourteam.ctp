<style type="text/css">
  
/*#our_team{}
#our_team #top_banner{ background:url(../images/teamimg/our_team_banner.jpg) no-repeat center top; background-size:cover; height: 300px; padding:0px;}*/
#team_members{ padding:30px 0px; }
#guest_team_members{ display:none;}
#team_members .member_detail, #guest_team_members .member_detail{ margin-top:30px;}
#team_members .member_detail .member_img, #guest_team_members .member_detail .member_img{ position:relative;}
#team_members .member_detail .member_img img, #guest_team_members .member_detail .member_img img{ width:100%}
#team_members .member_detail .member_img .personaldtl, #guest_team_members .member_detail .member_img .personaldtl{ background:rgba(0, 167, 230, .7); width:auto; position:absolute; bottom:0px; left:0px; right:0px; margin:auto;}
#team_members .member_detail .member_img .personaldtl h3, #guest_team_members .member_detail .member_img .personaldtl h3{ font-size:22px; font-weight:600; color:#fff;}
#team_members .member_detail .member_img .personaldtl h3 span, #guest_team_members .member_detail .member_img .personaldtl h3 span{ display:block; font-size:16px; font-weight:400; color:#fff;  margin-top:10px;}
#team_members .member_detail .mmbr_dtl, #guest_team_members .member_detail .mmbr_dtl{background:#049cd6; padding:5px; min-height: 156px;}
#team_members .member_detail .mmbr_dtl h4, #guest_team_members .member_detail .mmbr_dtl h4{ color:#fff; margin:0px; font-weight:600; margin-bottom: 10px;}
#team_members .member_detail .mmbr_dtl li, #guest_team_members .member_detail .mmbr_dtl li{    background: #0086b9;
    color: #fff;
    display: inline-block;
    min-width: 49%;
    /* margin: 5px; */
    margin-bottom: 10px;
    padding: 5px;
    font-size: 13px;}
#team_members .member_detail .mber_dtl_2, #guest_team_members .member_detail .mber_dtl_2{}
#guest_team_members{ background:#f1f2f2;  padding:80px 0px;}
#address .box{ background-color:transparent;}
#address .box p, #address .box p span {
    font-size: 18px;
    color: #000;
  background:none; font-weight:600;}
</style>
 <div id="our_team">
    <div id="top_banner">
    <img src="http://daac.in/images/teamimg/our_team_banner.jpg" alt="">
    </div>
    <!--top_banner-->
    <div id="team_members">
      <div class="container">
    
        <hgroup>
<h2>Our Team</h2>
<p>Are You Looking For</p>
<h6>Team</h6>
</hgroup>
        <div class="row">
          <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="member_detail">
              <div class="member_img"> <img src="<?php echo SITE_URL; ?>images/teamimg/vikas_sir.png" alt="member_image">
                <div class="personaldtl">
                  <h3>Vikas Solanki <span>(15 Years Exp.)</span> </h3>
                </div>
              </div>
              <div class="mber_dtl_2">
                <div class="mmbr_dtl">
                  <h4>Area of Expertise</h4>
                  <ul>
                    <li>Amazon AWS</li>
                    <li>Ionic</li>
                    <li>SalesForce</li>
                    <li>DBMS</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="member_detail">
              <div class="member_img"> <img src="<?php echo SITE_URL; ?>images/teamimg/pradhuman_sir.png" alt="member_image">
                <div class="personaldtl">
                  <h3>Pradhuman Naruka <span>(15 Years Exp.)</span> </h3>
                </div>
              </div>
              <div class="mber_dtl_2">
                <div class="mmbr_dtl">
                  <h4>Area of Expertise</h4>
                  <ul>
                    <li>Advance Photoshop Skills</li>
                    <li>Digital Painting</li>
                    <li>Illustrator</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="member_detail">
              <div class="member_img"> <img src="<?php echo SITE_URL; ?>images/teamimg/priyanka_mam.png" alt="member_image">
                <div class="personaldtl">
                  <h3>Priyanka Solanki <span>(7 Years Exp.)</span> </h3>
                </div>
              </div>
              <div class="mber_dtl_2">
                <div class="mmbr_dtl">
                  <h4>Area of Expertise</h4>
                  <ul>
                    <li>HTML 5</li>
                    <li>Bootstrap</li>
                    <li>CSS 3</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="member_detail">
              <div class="member_img"> <img src="<?php echo SITE_URL; ?>images/teamimg/lokesh_sir.png" alt="member_image">
                <div class="personaldtl">
                  <h3>Lokesh Saini <span>(5 Years Exp.)</span> </h3>
                </div>
              </div>
              <div class="mber_dtl_2">
                <div class="mmbr_dtl">
                  <h4>Area of Expertise</h4>
                  <ul>
                    <li>Cakephp</li>
                    <li>Magento</li>
                    <li>OpenCart</li>
                    <li>Wordpress</li>
                    <li>MySQL</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="member_detail">
              <div class="member_img"> <img src="<?php echo SITE_URL; ?>images/teamimg/vishnu_sir.png" alt="member_image">
                <div class="personaldtl">
                  <h3>Vishnu <span>(5 Years Exp.)</span> </h3>
                </div>
              </div>
              <div class="mber_dtl_2">
                <div class="mmbr_dtl">
                  <h4>Area of Expertise</h4>
                  <ul>
                    <li>Cakephp</li>
                    <li>MySQL</li>
                    <li>Wordpress</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="member_detail">
              <div class="member_img"> <img src="<?php echo SITE_URL; ?>images/teamimg/Deepak_sir.png" alt="member_image">
                <div class="personaldtl">
                  <h3>Deepak <span>(5 Years Exp.)</span> </h3>
                </div>
              </div>
              <div class="mber_dtl_2">
                <div class="mmbr_dtl">
                  <h4>Area of Expertise</h4>
                  <ul>
                    <li>Photoshop</li>
                    <li>HTML 5</li>
                    <li>Bootstrap</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="member_detail">
              <div class="member_img"> <img src="<?php echo SITE_URL; ?>images/teamimg/Ritesh.png" alt="member_image">
                <div class="personaldtl">
                  <h3>Ritesh Chhipa <span>(5 Years Exp.)</span> </h3>
                </div>
              </div>
              <div class="mber_dtl_2">
                <div class="mmbr_dtl">
                  <h4>Area of Expertise</h4>
                  <ul>
                    <li>Photoshop</li>
                    <li>HTML 5</li>
                    <li>Bootstrap</li>
                    <li>Illustrator</li>
                    <li>CorelDraw</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="member_detail">
              <div class="member_img"> <img src="<?php echo SITE_URL; ?>images/teamimg/anup_sir.png" alt="member_image">
                <div class="personaldtl">
                  <h3>Anup Saini <span>(5 Years Exp.)</span> </h3>
                </div>
              </div>
              <div class="mber_dtl_2">
                <div class="mmbr_dtl">
                  <h4>Area of Expertise</h4>
                  <ul>
                    <li>Photoshop</li>
                    <li>HTML 5</li>
                    <li>Bootstrap</li>
                    <li>Illustrator</li>
                    <li>CorelDraw</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="member_detail">
              <div class="member_img"> <img src="<?php echo SITE_URL; ?>images/teamimg/aditya_sir.png" alt="member_image">
                <div class="personaldtl">
                  <h3>Aditya Sharma <span>(3 Years Exp.)</span> </h3>
                </div>
              </div>
              <div class="mber_dtl_2">
                <div class="mmbr_dtl">
                  <h4>Area of Expertise</h4>
                  <ul>
                    <li>Photoshop</li>
                    <li>HTML 5</li>
                    <li>Bootstrap</li>
                    <li>Illustrator</li>
                    <li>CorelDraw</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="guest_team_members">
      <div class="container">
        <hgroup>
          <h3 class="heading">Guest <span>Faculties</span></h3>
        </hgroup>
        <div class="row">
          <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="member_detail">
              <div class="member_img"> <img src="<?php echo SITE_URL; ?>images/teamimg/Guest_faculties.png" alt="member_image">
                <div class="personaldtl">
                  <h3>Guest Faculties <span>(10 Years Exp.)</span> </h3>
                </div>
              </div>
              <div class="mber_dtl_2">
                <div class="mmbr_dtl">
                  <h4>Area of Expertise</h4>
                  <ul>
                    <li>Amazon AWS</li>
                    <li>Ionic</li>
                    <li>SalesForce</li>
                    <li>DBMS</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="member_detail">
              <div class="member_img"> <img src="<?php echo SITE_URL; ?>images/teamimg/Guest_faculties.png" alt="member_image">
                <div class="personaldtl">
                  <h3>Guest Faculties <span>(10 Years Exp.)</span> </h3>
                </div>
              </div>
              <div class="mber_dtl_2">
                <div class="mmbr_dtl">
                  <h4>Area of Expertise</h4>
                  <ul>
                    <li>Amazon AWS</li>
                    <li>Ionic</li>
                    <li>SalesForce</li>
                    <li>DBMS</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="member_detail">
              <div class="member_img"> <img src="<?php echo SITE_URL; ?>images/teamimg/Guest_faculties.png" alt="member_image">
                <div class="personaldtl">
                  <h3>Guest Faculties <span>(10 Years Exp.)</span> </h3>
                </div>
              </div>
              <div class="mber_dtl_2">
                <div class="mmbr_dtl">
                  <h4>Area of Expertise</h4>
                  <ul>
                    <li>Amazon AWS</li>
                    <li>Ionic</li>
                    <li>SalesForce</li>
                    <li>DBMS</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="member_detail">
              <div class="member_img"> <img src="<?php echo SITE_URL; ?>images/teamimg/Guest_faculties.png" alt="member_image">
                <div class="personaldtl">
                  <h3>Guest Faculties <span>(10 Years Exp.)</span> </h3>
                </div>
              </div>
              <div class="mber_dtl_2">
                <div class="mmbr_dtl">
                  <h4>Area of Expertise</h4>
                  <ul>
                    <li>Amazon AWS</li>
                    <li>Ionic</li>
                    <li>SalesForce</li>
                    <li>DBMS</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>

      </div>
    </div>
  </div>