<div id="faq_page">
<section id="top_banner">
<img src="http://daac.in/images/FAQ.jpg" alt="">
</section><!--top_banner-->


<section id="faq_sec">
<div class="container">
<div class="row">
<div class="col-xl-3 col-lg-3 col-md-4"><div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
  <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all" role="tab" aria-controls="v-pills-all" aria-selected="true">All</a>
  <a class="nav-link" id="v-pills-design-tab" data-toggle="pill" href="#v-pills-design" role="tab" aria-controls="v-pills-design" aria-selected="false">Web Designing</a>
  <a class="nav-link" id="v-pills-cl-tab" data-toggle="pill" href="#v-pills-cl" role="tab" aria-controls="v-pills-cl" aria-selected="false">C, C++</a>
  <a class="nav-link" id="v-pills-java-tab" data-toggle="pill" href="#v-pills-java" role="tab" aria-controls="v-pills-java" aria-selected="false">Java</a>


  
   <a class="nav-link" id="v-pills-dmarketing-tab" data-toggle="pill" href="#v-pills-dmarketing" role="tab" aria-controls="v-pills-dmarketing" aria-selected="false">SEO</a>
   
<!--     <a class="nav-link" id="v-pills-cjava-tab" data-toggle="pill" href="#v-pills-cjava" role="tab" aria-controls="v-pills-cjava" aria-selected="false">Android</a>
    
    <a class="nav-link" id="v-pills-bmca-tab" data-toggle="pill" href="#v-pills-bmca" role="tab" aria-controls="v-pills-bmca" aria-selected="false">PHP</a> -->
</div></div>
<div class="col-xl-9 col-lg-9 col-md-8"><div class="tab-content" id="v-pills-tabContent">
  <div class="tab-pane fade show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab"><div id="accordion" role="tablist" aria-multiselectable="true">
  	<?php $i=0; foreach($ques as $key=>$value) {
  	 // print_r($value);?>
  <div class="card_position clearfix">
	  <div class="card" >
    <div class="card-header" role="tab" id="heading<?php echo $value['id']; ?>">
      <h5 class="mb-0">
        <a <?php if($i==0){ ?>aria-expanded="false" <? } else{ ?> class="collapsed" aria-expanded="true" <? } ?> data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $value['id']; ?>"  aria-controls="collapse<?php echo $value['id']; ?>">
          <?php echo $value['question']; ?>
        </a>
      </h5>
    </div>

    <div id="collapse<?php echo $value['id']; ?>" class="collapse <?php if($i==0){ ?>show<? } ?>" role="tabpanel" aria-labelledby="heading<?php echo $value['id']; ?>">
      <div class="card-block">
      <p><?php echo $value['answer']; ?></p>
      </div>
    </div>
  </div></div>
  <?php $i++; } ?>
  
  
</div></div>



  <div class="tab-pane fade" id="v-pills-design" role="tabpanel" aria-labelledby="v-pills-design-tab">
	  <div id="accordion1" role="tablist" aria-multiselectable="true">
  	<?php  $i=0;foreach($ques as $key=>$value) {
  	 if($value['category']=='6') { ?>
  <div class="card_position clearfix"><div class="card" >
    <div class="card-header" role="tab" id="heading<?php echo $value['id']; ?>">
      <h5 class="mb-0">
        <a data-toggle="collapse" data-parent="#accordion1" href="#collapsei<?php echo $value['id']; ?>" <?php if($i==0){ ?>aria-expanded="false" <? } else{ ?> class="collapsed" aria-expanded="true" <? } ?> aria-controls="collapse<?php echo $value['id']; ?>">
          <?php echo $value['question']; ?>
        </a>
      </h5>
    </div>

    <div id="collapsei<?php echo $value['id']; ?>" class="collapse <?php if($i==0){ ?>show<? } ?>" role="tabpanel" aria-labelledby="heading<?php echo $value['id']; ?>">
      <div class="card-block">
      <p><?php echo $value['answer']; ?></p>
      </div>
    </div>
  </div></div>
  <?php $i++; } }?></div></div>
  
  
  
  <div class="tab-pane fade" id="v-pills-cl" role="tabpanel" aria-labelledby="v-pills-cl-tab"><div id="accordion2" role="tablist" aria-multiselectable="true">
  	<?php $i=0; foreach($ques as $key=>$value) {
  	 if($value['category']=='2') { ?>
  <div class="card_position clearfix"><div class="card" >
    <div class="card-header" role="tab" id="heading<?php echo $value['id']; ?>">
      <h5 class="mb-0">
        <a data-toggle="collapse" data-parent="#accordion2" href="#collapsej<?php echo $value['id']; ?>" <?php if($i==0){ ?>aria-expanded="false" <? } else{ ?> class="collapsed" aria-expanded="true" <? } ?> aria-controls="collapse<?php echo $value['id']; ?>">
          <?php echo $value['question']; ?>
        </a>
      </h5>
    </div>

    <div id="collapsej<?php echo $value['id']; ?>" class="collapse <?php if($i==0){ ?>show<? } ?>" role="tabpanel" aria-labelledby="heading<?php echo $value['id']; ?>">
      <div class="card-block">
      <p><?php echo $value['answer']; ?></p>
      </div>
    </div>
  </div></div>
  <?php   $i++; } }?></div>
  </div>
  <div class="tab-pane fade" id="v-pills-java" role="tabpanel" aria-labelledby="v-pills-java-tab"><div id="accordion3" role="tablist" aria-multiselectable="true">
    <?php $i=0; foreach($ques as $key=>$value) {
     if($value['category']=='4') { ?>
  <div class="card_position clearfix"><div class="card" >
    <div class="card-header" role="tab" id="heading<?php echo $value['id']; ?>">
      <h5 class="mb-0">
        <a data-toggle="collapse" data-parent="#accordion3" href="#collapsek<?php echo $value['id']; ?>" <?php if($i==0){ ?>aria-expanded="false" <? } else{ ?> class="collapsed" aria-expanded="true" <? } ?>  aria-controls="collapse<?php echo $value['id']; ?>">
          <?php echo $value['question']; ?>
        </a>
      </h5>
    </div>

    <div id="collapsek<?php echo $value['id']; ?>" class="collapse <?php if($i==0){ ?>show<? } ?>" role="tabpanel" aria-labelledby="heading<?php echo $value['id']; ?>">
      <div class="card-block">
      <p><?php echo $value['answer']; ?></p>
      </div>
    </div>
  </div></div>
  <?php $i++;  } }?></div></div>
  
  <div class="tab-pane fade" id="v-pills-dmarketing" role="tabpanel" aria-labelledby="v-pills-dmarketing-tab"><div id="accordion4" role="tablist" aria-multiselectable="true">
    <?php $i=0;  foreach($ques as $key=>$value) {
     if($value['category']=='5') { ?>
  <div class="card_position clearfix"><div class="card" >
    <div class="card-header" role="tab" id="heading<?php echo $value['id']; ?>">
      <h5 class="mb-0">
        <a data-toggle="collapse" data-parent="#accordion4" href="#collapsel<?php echo $value['id']; ?>" <?php if($i==0){ ?>aria-expanded="false" <? } else{ ?> class="collapsed" aria-expanded="true" <? } ?> aria-controls="collapse<?php echo $value['id']; ?>">
          <?php echo $value['question']; ?>
        </a>
      </h5>
    </div>

    <div id="collapsel<?php echo $value['id']; ?>" class="collapse <?php if($i==0){ ?>show<? } ?>" role="tabpanel" aria-labelledby="heading<?php echo $value['id']; ?>">
      <div class="card-block">
      <p><?php echo $value['answer']; ?></p>
      </div>
    </div>
  </div></div>
  <?php $i++;  } }?></div></div>
  
  <div class="tab-pane fade" id="v-pills-cjava" role="tabpanel" aria-labelledby="v-pills-cjava-tab"> <div id="accordion5" role="tablist" aria-multiselectable="true">
    <?php  $i=0;  foreach($ques as $key=>$value) {
     if($value['category']=='3') { ?>
  <div class="card_position clearfix"><div class="card" >
    <div class="card-header" role="tab" id="heading<?php echo $value['id']; ?>">
      <h5 class="mb-0">
        <a data-toggle="collapse" data-parent="#accordion5" href="#collapseo<?php echo $value['id']; ?>" <?php if($i==0){ ?>aria-expanded="false" <? } else{ ?> class="collapsed" aria-expanded="true" <? } ?> aria-controls="collapse<?php echo $value['id']; ?>">
          <?php echo $value['question']; ?>
        </a>
      </h5>
    </div>

    <div id="collapseo<?php echo $value['id']; ?>" class="collapse <?php if($i==0){ ?>show<? } ?>" role="tabpanel" aria-labelledby="heading<?php echo $value['id']; ?>">
      <div class="card-block">
      <p><?php echo $value['answer']; ?></p>
      </div>
    </div>
  </div></div>
  <?php $i++;  } }?></div></div>
  
  <div class="tab-pane fade" id="v-pills-bmca" role="tabpanel" aria-labelledby="v-pills-bmca-tab"><div id="accordion6" role="tablist" aria-multiselectable="true">
   <?php   $i=0;foreach($ques as $key=>$value) {
     if($value['category']=='1') { ?>
  <div class="card_position clearfix"><div class="card" >
    <div class="card-header" role="tab" id="heading<?php echo $value['id']; ?>">
      <h5 class="mb-0">
        <a data-toggle="collapsep" data-parent="#accordion6" href="#collapse<?php echo $value['id']; ?>" <?php if($i==0){ ?>aria-expanded="false" <? } else{ ?> class="collapsed" aria-expanded="true" <? } ?> aria-controls="collapsep<?php echo $value['id']; ?>">
          <?php echo $value['question']; ?>
        </a>
      </h5>
    </div>

    <div id="collapsep<?php echo $value['id']; ?>" class="collapse <?php if($i==0){ ?>show<? } ?>" role="tabpanel" aria-labelledby="heading<?php echo $value['id']; ?>">
      <div class="card-block">
      <p><?php echo $value['answer']; ?></p>
      </div>
    </div>
  </div></div>
  <?php $i++;  } }?></div>
</div>
</div>
</div><!--container-->
</section><!--faq_sec-->


<section id="call_action">
<div class="container">
<div class="row justify-content-center">
<div class="col-md-6">
<p>We Will Contact You, At a Time Which Suits You Best</p>
</div>
<div class="col-md-4"><a href="javascript:void(0);" data-toggle="modal" data-target="#request_modal"><i class="fas fa-phone-volume"></i> Request a Call Back</a></div>
</div>
</div>
</section></div>


<script>
$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
</script>

<script>
$(document).ready(function(){
    $(".nav-pills a#pills-all").click(function(){
		$(".tab-content .tab-pane.show").removeClass("show");
        $(".tab-content .tab-pane#pills-home").addClass("show");
		 
    });
	
	    $(".nav-pills a#pills-design").click(function(){
		$(".tab-content .tab-pane.show").removeClass("show");
        $(".tab-content .tab-pane#pills-profile").addClass("show");
		 
    });
	
	
		    $(".nav-pills a#pills-mobile_app-tab").click(function(){
		$(".tab-content .tab-pane.show").removeClass("show");
        $(".tab-content .tab-pane#mobile_app").addClass("show");
		 
    });
	
	
		
		    $(".nav-pills a#pills-other_tab-tab").click(function(){
		$(".tab-content .tab-pane.show").removeClass("show");
        $(".tab-content .tab-pane#other_tab").addClass("show");
		 
    });
});
</script>
<script src="js/owl.carousel.min.js" type="text/javascript"></script>
<script>
$('#otherc_crausal').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
	autoplay:true,
	autoplaySpeed:2000,
	autoplayTimeout:2000,
	dots:true,
	nav:false,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:3,
            nav:false,
            loop:true
        }
    }
})
</script>

<script>

$('#mapp_crausal').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
	autoplay:true,
	autoplaySpeed:2000,
	autoplayTimeout:2000,
	dots:true,
	nav:false,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:3,
            nav:false,
            loop:true
        }
    }
})
</script>

<script>

$('#programing_crausal').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
	autoplay:true,
	autoplaySpeed:2000,
	autoplayTimeout:2000,
	dots:true,
	nav:false,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:3,
            nav:false,
            loop:true
        }
    }
})

</script>

<script>
$('#designing_crausal').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
	autoplay:true,
	autoplaySpeed:2000,
	autoplayTimeout:2000,
	dots:true,
	nav:false,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:3,
            nav:false,
            loop:true
        }
    }
})

</script>

<script src="js/all_js.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>


<script src="js/menu.js" type="text/javascript"></script>
<script>





  /**
   * Push right instantiation and action.
   */
  var pushRight = new Menu({
    wrapper: '#o-wrapper',
    type: 'push-right',
    menuOpenerClass: '.c-button',
    maskId: '#c-mask'
  });

  var pushRightBtn = document.querySelector('#c-button--push-right');
  
  pushRightBtn.addEventListener('click', function(e) {
    e.preventDefault;
    pushRight.open();
  });






</script>
