

<style type="text/css">
/*--
Author: W3layouts
Author URL: https://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
--*/
/* reset */

/*Pricing table and price blocks*/
.price-head h1 {
text-align: center;
margin-top: 2em;
font-size: 3em;
color: #fff;
}
.price-head h3 {
	color: #404042;
	font-size: 3em;
	text-decoration: none;
	font-weight: 700;
}
.pricing-grids {
margin: 12% 0;
}
/*----*/
.pricing-grid1,.pricing-grid2,.pricing-grid3 {
width: 31.5%;
float: left;
text-align: center;
margin-right: 2%;
transition: 0.5s all;
-webkit-transition: 0.5s all;
-moz-transition: 0.5s all;
-o-transition: 0.5s all;
padding: 0;
border: none;
border-radius: 0.7em;
-webkit-border-radius: 0.7em;
-o-border-radius: 0.7em;
-moz-border-radius: 0.7em;
}
 .pricing-grid3{
 	margin-right: 0;
 }
.pricing-grid1:hover,.pricing-grid2:hover,.pricing-grid3:hover {
	transform: scale(1.05);
-webkit-transform: scale(1.05);
-moz-transform: scale(1.05);
-o-transform: scale(1.05);
-ms-transform: scale(1.05);
	z-index: 1;

}
.pricing-grid1:nth-child(3){
	margin-right:0;
}
.price-value h2 a,.price-value.two h3 a,.price-value.three h4 a{
	font-size: 30px;
	color:#fff;
}
.price-value h2,.price-value.two h3,.price-value.three h3{ margin:8px 0px;}
.price-value,.price-value.two,.price-value.three {
	background: #512884;
	padding: 12px;
	border-bottom:2px solid#ffd500;
	border-top-left-radius:0.7em;
	-webkit-border-top-left-radius:0.7em;
	-o-border-top-left-radius: 0.7em;
	-moz-border-top-left-radius:0.7em;
	border-top-right-radius:0.7em;
	-webkit-border-top-right-radius:0.7em;
	-o-border-top-right-radius:0.7em;
	-moz-border-top-left-radius: 0.7em;
	position: relative;
}

.price-value.three {
	background: #04dbdd;
	border-bottom:2px solid#028f87;
}
.price-value ul,.pricing-grid1 ul,.pricing-grid2 ul,.pricing-grid3 ul{
	padding: 0;
}

 .pricing-grid1{ border:2px solid #512884; border-top:none;}
 .pricing-grid2{ border:2px solid #2b2b33; border-top:none;}
 .pricing-grid3{ border:2px solid #04dbdd; border-top:none;}
 .pricing-grid1, .pricing-grid2 , .pricing-grid3{ min-height:493px;}
.price-value ul li,.pricing-grid1,.pricing-grid2 ul li,.pricing-grid3 ul li {
	list-style: none;
}
.price-value ul li{
	list-style: none;
}
.price-value  h5 span{
color: #fbd707;
font-size: 19px;
}
.price-value lable{
color: #000;
font-size: 17px;
}
.price-value .title_shop{ color:#fbd707;}
.price-value.two .title_shop{ color:#fe6d72;}
.price-value.three .title_shop{ color:#018f90;}
.price-value.two h5 span{
	color:#fe6d72;
}
.price-value.two h5 lable{
	color:#8c8c94;
}
.price-value.three h5 span{
	color: #018f90;
}
.price-value.three h5 lable{
	color:#9CF7F8;
}

.pricing-grid1 ul li a,.pricing-grid2 ul li a,.pricing-grid3 ul li a{
	color: #C7C4C4;
	font-size: 15px;
	text-align: center;
	display: block;
	padding: 6px 0;
	text-decoration: none;
	font-weight: 400;
}
.pricing-grid1 ul li.whyt a,.pricing-grid2 ul li.whyt a,.pricing-grid3 ul li.whyt a{
	background:#f4f4f4;
}
.pricing-grid1:hover div.price-bg ul li a,.pricing-grid1:hover div.price-value h3 a{
 color:#512884;
}
.pricing-grid2:hover div.price-bg ul li a,.pricing-grid2:hover div.price-value h3 a{
 color:#fa6e6f;
}
.pricing-grid3:hover div.price-bg ul li a,.pricing-grid3:hover div.price-value h3 a{
	color:#04dbdd;
}
.price-bg {
	background: #fff;
}
.price-bg ul {
	padding: 0;
}
.price-bg ul li{
	list-style: none;
}

.detail_pg_nw{ padding: 40px 0px 20px; }
  .main_grouphead h2{ color: #211d1e; font-size: 30px; text-align: left; font-weight: bold; position: relative; z-index: 9;}
  .main_grouphead{ position: relative; }
  .main_grouphead h5{ color: #eeeef0; position: absolute; top: -43px; font-size: 65px; font-weight: bold; }
  .main_grouphead{ margin-bottom: 20px; }
  .intro_video2{ width: 90%; position:relative;
  -webkit-box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, .5), 10px 10px 2px 0px rgba(2,171,227,1);
-moz-box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, .5), 10px 10px 2px 0px rgba(2,171,227,1);
box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, .5), 10px 10px 2px 0px rgba(2,171,227,1); border-radius: 5px;}
.intro_video2 a{     position: absolute;
    top: 0;
    width: 100%;
    background: rgba(0, 0, 0, .1);
    display: block;
    height: 100%;
    border-radius: 5px;
    display: flex;
    justify-content: center;
    align-items: center;}
.intro_video2 a i{ position: absolute; font-size:50px; color:#db0326;}
 .intro_video2 img{ border-radius:5px;}
  .intro_video2 iframe{ margin-bottom: -8px; }
    .intro_video2 .ytp-gradient-top{ display: none !important; }
    #carrer_coursdur strong{ position: relative;}
    #carrer_coursdur strong:after{ content: '-'; position: absolute; right: 62px; top: -2px; color: #000; font-size: 18px; display: block; }
    #carrer_coursdur h3{ font-weight: bold; }
    #carrer_coursdur .inner_div{ padding: 0px !important; }
	.detail_pg_nw .modal-dialog{ max-width:700px;}
	.video_close{
	    position: absolute;
    right: -21px;
    top: -20px;
    background: rgba(0, 0, 0, .5);
    width: 40px;
    height: 40px;
    border-radius: 50%;
    color: #fff;
    opacity: 1;
	line-height: 29px;
	font-size:28px;
	}

	#modual_sec .modual_div ul li h3{ font-size: 15px; }
/*-----*/
/**

/**
 * Fade-zoom animation for first dialog
 */
/*--------------*/
@media(max-width:1366px){
	.pricing-grid1 ul li a, .pricing-grid2 ul li a, .pricing-grid3 ul li a {
	font-size: 17px;
	}
}
@media(max-width:1024px){
	.pricing-grid1 ul li a, .pricing-grid2 ul li a, .pricing-grid3 ul li a {
	font-size: 15px;
	}
	.price-value, .price-value.two, .price-value.three {
	padding: 2em 0 1em 0;
	}
	.price-value h2 a, .price-value.two h3 a, .price-value.three h4 a {
	font-size: 30px;
	}
	.price-head h1 {
	margin-top: 1em;
	font-size: 2.6em;
	}
	.pricing-grids {
	margin: 8% 0;
	}
}
@media(max-width:768px){
	.pricing-grid1 h3 a, .pricing-grid2 h3 a, .pricing-grid3 h3 a {
	padding: 0.4em 1em;
	font-size: 0.7em;
	}
	.pricing-grid1, .pricing-grid2, .pricing-grid3 {
	width: 55%;
	float: none;
	text-align: center;
	margin: 1em auto;
	}
	.price-head h1 {
	margin-top: 1em;
	font-size: 2.4em;
	}
	.cart1, .cart2, .cart3 {
	padding: 2em 0em 2em;
	}
}
@media(max-width:640px){
	.pricing-grid1 ul li a, .pricing-grid2 ul li a, .pricing-grid3 ul li a {
	font-size: 15px;
	}
	.pricing-grid1, .pricing-grid2, .pricing-grid3 {
	width: 65%;
	float: none;
	text-align: center;
	margin: 1em auto;
	}
	.pricing-grid1 ul li a, .pricing-grid2 ul li a, .pricing-grid3 ul li a {
	font-size: 15px;
	padding: 13px 0;
	}
	.payment-online-form-left input[type="text"] {
	padding: 1em 1em;
	width: 93%;
	}
	.payment-sendbtns {
	float: none;
	margin: 2em 0 1.5em;
	}
}
@media(max-width:480px){
	.pricing-grid1, .pricing-grid2, .pricing-grid3 {
	width: 81%;
	}
	.price-head h1 {
	margin-top: 1em;
	font-size: 2.1em;
	}
	.payment-online-form-left input[type="text"] {
	padding: 1em 1em;
	width: 91%;
	}
	.pop_up {
	border: 7px solid#485460;
	}
}
@media(max-width:320px){
	.pricing-grid1, .pricing-grid2, .pricing-grid3 {
	width: 100%;
	margin-right: 0;
	}
	.price-value h2 a, .price-value.two h3 a, .price-value.three h4 a {
	font-size: 30px;
	}
	.price-value h5 span,.price-value lable {
	font-size: 16px;
	}
	.price-head h1 {
	margin-top: 1em;
	font-size: 1.5em;
	}
	.payment-online-form-left input[type="text"] {
	padding: 0.8em 0.8em;
	width: 86%;
	font-size: 13px;
	margin: 3px;
	}
	.pop_up {
	border: 4px solid#485460;
	}
	.payment-online-form-left h4 {
	font-size: 1.4em;
	}
	.payment {
	background: url(https://preview.w3layouts.com/demos/flat_pricing_tables_design/web/images/icon.png) no-repeat -152px    -21px;
	}
	.footer p {
	font-size: 0.9em;
	line-height: 1.5em;
	}
	ul.payment-sendbtns li {
	margin-top: 1em;
	}

}


.price-value.two h5 lable{ color:#000 !important;}
.price-value.two .title_shop{ color: #000 !important; }
 .pricing-grid2:hover div.price-value h3 a{ color: #fff; }
</style>

<section id="top_banner">
<?php foreach ($courses as $key => $iteam) { //print_r($iteam);
    //echo     $iteam['cname'];
    $image = $iteam['image'];
    //echo $image;

    $img = SITE_URL . 'images/course/' . $image;?>
	<img src="<?php echo $img; ?>">
<?php }?>
<div class="banner_image">


<div class="container">
<h4>Start Your 7 - Days Free Trial Today.</h4>
</div><!--container-->
</div>
</section><!--top_banner-->
<?php foreach ($courses as $key => $iteam) {
    //print_r($iteam); die;
    ?>

	<div class="detail_pg_nw">
<?php if ($iteam['course_group'] == "Designing") {?>
	<div class="modal" id="myModal">
                       <div class="modal-dialog">
                         <div class="modal-content">
                           <button type="button" class="video_close" data-dismiss="modal">&times;</button>
						   <iframe width="100%" height="387" src="https://www.youtube.com/embed/R_t7iY4linw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                         </div>
					   </div>
					 </div>
							 <?php } else if ($iteam['course_group'] == "Mobile Development") {?>
								<div class="modal" id="myModal">
								<div class="modal-dialog">
                         <div class="modal-content">
                           <button type="button" class="video_close" data-dismiss="modal">&times;</button>
						   <iframe width="100%" height="387" src="https://www.youtube.com/embed/I0ycKVZUOBM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                         </div>
					   </div>
					 </div>
							 <?php }?>
							 <script>
							 $(document).ready(function () {
    // Easy Youtube Auto Play Script when visible
    // Author: Vitalii Rizo 2017
    "use strict";
    var iframe = document.getElementById("autoplay-video"),
        disableAutoPlay = false;
    function isScrolledIntoView(el) {
        var elemTop = el.getBoundingClientRect().top,
            elemBottom = el.getBoundingClientRect().bottom,
            isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
        return isVisible;
    }
    $(window).scroll(function () {
        if (!disableAutoPlay) {
            if (isScrolledIntoView(iframe)) {
                iframe.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
            } else {
                iframe.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
            }
        }
    });
    $(iframe).on("mouseleave", function () {
        disableAutoPlay = true;
    });
});
							 </script>
		<div class="container">


<?php if ($this->request->url == "student-cake-php-work.html") {?>


	<ul class="ckaep_ul">
		<li><a href="https://daac.in/student-sketching-work.html">Sketching </a></li>
		<li><a href="https://daac.in/student-photoshop-work.html">Photoshop </a></li>
		<li><a href="https://daac.in/student-html-work.html">HTML </a></li>
		<li><a href="https://daac.in/student-cake-php-work.html">CakePHP </a></li>
		<li><a href="https://daac.in/student-mobile-app-work.html">Mobile Apps </a></li>
		<li><a href="https://daac.in/student-digital-marketing-work.html">Digital Marketing </a></li>
	</ul>

	<?php }?>

	<?php if ($this->request->url == "student-photoshop-work.html") {?>


	<ul class="ckaep_ul">
		<li><a href="https://daac.in/student-sketching-work.html">Sketching </a></li>
		<li><a href="https://daac.in/student-photoshop-work.html">Photoshop </a></li>
		<li><a href="https://daac.in/student-html-work.html">HTML </a></li>
		<li><a href="https://daac.in/student-cake-php-work.html">CakePHP </a></li>
		<li><a href="https://daac.in/student-mobile-app-work.html">Mobile Apps </a></li>
		<li><a href="https://daac.in/student-digital-marketing-work.html">Digital Marketing </a></li>
	</ul>

	<?php }?>

		<?php if ($this->request->url == "student-sketching-work.html") {?>


	<ul class="ckaep_ul">
		<li><a href="https://daac.in/student-sketching-work.html">Sketching </a></li>
		<li><a href="https://daac.in/student-photoshop-work.html">Photoshop </a></li>
		<li><a href="https://daac.in/student-html-work.html">HTML </a></li>
		<li><a href="https://daac.in/student-cake-php-work.html">CakePHP </a></li>
		<li><a href="https://daac.in/student-mobile-app-work.html">Mobile Apps </a></li>
		<li><a href="https://daac.in/student-digital-marketing-work.html">Digital Marketing </a></li>
	</ul>

	<?php }?>

		<?php if ($this->request->url == "student-html-work.html") {?>


	<ul class="ckaep_ul">
		<li><a href="https://daac.in/student-sketching-work.html">Sketching </a></li>
		<li><a href="https://daac.in/student-photoshop-work.html">Photoshop </a></li>
		<li><a href="https://daac.in/student-html-work.html">HTML </a></li>
		<li><a href="https://daac.in/student-cake-php-work.html">CakePHP </a></li>
		<li><a href="https://daac.in/student-mobile-app-work.html">Mobile Apps </a></li>
		<li><a href="https://daac.in/student-digital-marketing-work.html">Digital Marketing </a></li>
	</ul>

	<?php }?>

			<?php if ($this->request->url == "student-mobile-app-work.html") {?>


	<ul class="ckaep_ul">
		<li><a href="https://daac.in/student-sketching-work.html">Sketching </a></li>
		<li><a href="https://daac.in/student-photoshop-work.html">Photoshop </a></li>
		<li><a href="https://daac.in/student-html-work.html">HTML </a></li>
		<li><a href="https://daac.in/student-cake-php-work.html">CakePHP </a></li>
		<li><a href="https://daac.in/student-mobile-app-work.html">Mobile Apps </a></li>
		<li><a href="https://daac.in/student-digital-marketing-work.html">Digital Marketing </a></li>
	</ul>

	<?php }?>

		<?php if ($this->request->url == "student-digital-marketing-work.html") {?>


	<ul class="ckaep_ul">
		<li><a href="https://daac.in/student-sketching-work.html">Sketching </a></li>
		<li><a href="https://daac.in/student-photoshop-work.html">Photoshop </a></li>
		<li><a href="https://daac.in/student-html-work.html">HTML </a></li>
		<li><a href="https://daac.in/student-cake-php-work.html">CakePHP </a></li>
		<li><a href="https://daac.in/student-mobile-app-work.html">Mobile Apps </a></li>
		<li><a href="https://daac.in/student-digital-marketing-work.html">Digital Marketing </a></li>
	</ul>

	<?php }?>



			
			<hgroup class="main_grouphead">
				<h2><?php echo $iteam['sname']; ?>
					<?php if ($this->request->url == "student-cake-php-work.html") {?>
<span>(CakePHP)</span>
							<?php }?>

							<?php if ($this->request->url == "student-photoshop-work.html") {?>
<span>(Photoshop)</span>
							<?php }?>

								<?php if ($this->request->url == "student-sketching-work.html") {?>
<span>(Sketching)</span>
							<?php }?>

							<?php if ($this->request->url == "student-html-work.html") {?>
<span>(HTML)</span>
							<?php }?>

							<?php if ($this->request->url == "student-mobile-app-work.html") {?>
<span>(Mobile App)</span>
							<?php }?>

							<?php if ($this->request->url == "student-digital-marketing-work.html") {?>
<span>(Digital Marketing)</span>
							<?php }?>

				</h2>
				<h5><?php echo $iteam['sname']; ?></h5>
			</hgroup>





		<div class="row">



			<div class="col-md-6 align-self-center">
				<?php if ($iteam['course_group'] == "Designing" || $iteam['course_group'] == "Mobile Development") {?>
				<div class="intro_video">
					<div class="intro_video2">
					<a href="#" data-toggle="modal" data-target="#myModal"><i class="fab fa-youtube"></i></a>
					<?php if ($iteam['course_group'] == "Designing") {?>
                    <img src="<?php echo SITE_URL; ?>images/UX_UI_Designer_image.jpg">
										<?php } else if ($iteam['course_group'] == "Mobile Development") {?>
											<img src="<?php echo SITE_URL; ?>images/react_video_screen.jpg">
										<?php }?>
				</div>
</div><?php } ?>
			</div>
<?php if ($this->request->url == "web-designing-course-in-jaipur.html") {?>
			<div class="col-md-6">

<p>Join Our Web Design Training Courses in Jaipur and become a professional in designing web and mobile pages using HTML/CSS programming language and graphics. Website designing is the first step in making your small or large business presence online. All over the world, people are become experts and improving their skills via learning online. </p>
<p>
Finding a good web designing training institute is a big challenge for everyone. But <strong>DAAC</strong> is the end of your search because in this web designing training course you will learn HTML/CSS and Graphics from professional web designers who are experts in the design of new websites for every type of industry. </p>
<p>
In <strong>DAAC</strong> you will learn and earn all knowledge which is helpful to create a website according to your client's needs and ideas. Web design online training programs give you a chance to learn important and interesting skills that are helpful to join a big IT company.
</p>

					</div>	<?php }?>

					<?php if ($this->request->url == "android-training-in-Jaipur.html") {?>
			<div class="col-md-6 align-self-center">

<div>
<div class="container">
<!-- <h3>Advance Mobile App Development Training and Certification&nbsp;(PACAD)</h3> -->

<div>
<p><strong>DAAC</strong> provides <strong>Android training in Jaipur</strong>, <strong>DAAC</strong> has industry best trainer who provides training to students in a professional manner according to industry requirements. Android is a Linux-based open-source operating system for mobile devices such as smartphones and tablet computers, which has been adapted by many Smartphone companies for their mobile devices operating the system. Android app development course details are:</p>
</div>
</div>
</div>

					</div>	<?php }?>

<?php if ($this->request->url == "advance-mobile-development.html") {?>
						<div class="col-md-6 align-self-center">

<p><strong>DAAC</strong> Provides best advanced mobile app development training courses in jaipur. We transform learners to experts during training programs. Our mobile app development trainers provide end to end skills that will help you to build a mobile application for android or ios platform. In the Learning part of mobile app development, you will get knowledge related application lifecycle, architecture, layout manager, testing part and at last the responsive app uploading part.
</p>
						</div><?php }?>


						<?php if ($this->request->url == "coreldraw-training-institute-jaipur.html") {?>
						<div class="col-md-6 align-self-center">

<p>CorelDRAW is an effective vector graphics editor software for graphic designers.CorelDraw training in <strong>DAAC</strong> provides the General details of CorelDraw with Hands-on exercises familiarize delegates with the ideas and operations.</p>

<p><strong>DAAC</strong> will teach you Coreldraw software tools, options used to create works such as posters, flyers, brochures, magazines, newspapers, and books. CorelDraw (styled CorelDRAW) is a vector graphics editor developed. It is also the name of Corel&rsquo;s Graphics Suite, which bundles CorelDraw with bitmap-image editor Corel Photo-Paint.</p>

						</div><?php }?>

									<?php if ($this->request->url == "illustrator-training-institute-jaipur.html") {?>
						<div class="col-md-6 align-self-center">

<p>Illustrator Course&nbsp; is a vector graphics editor developed and marketed by Adobe Systems.&nbsp;</p>

<p>In this Illustrator&nbsp; Training Course, you will learn step-by-step instructions and in-depth explanations of the features of Illustrator Training Course in Jaipur.&nbsp; You will first learn how to get started with adobe illustrator. Knowing the differences of Raster and vector graphics. colour models&nbsp; for web and print media (RGB VS CMYK ). Creating documents for web, print, film, video etc. Using Selection tools, drawing tools, painting tools, synbols, distortion tools, 3d objects, masks, blends , envelopes, compound objects, graphics,animations and Hyperlinks and many more interesting features in Adobe illustrator.</p>

<!-- <p><strong>LEARNING OUTCOME</strong></p>

<p>Art works for web, print and mobile devices Designing brochures, flyers, business cards, icons, leaplets, t shirt designs, mock ups etc. e-mailers, news letters Brand identity designs Integrate external files from other adobe products like Photoshop, indesign and flash etc.</p> -->

						</div><?php }?>

							<?php if ($this->request->url == "graphic-designing-institute-in-jaipur.html") {?>
						<div class="col-md-6 align-self-center">

<p><strong>DAAC</strong> is the best graphic design training institute in jaipur which provides professional training for candidates. Students learn creative and  innovative ways to design or create logos, posters and many more things via photoshop and Illustrator. <p>
<p>
<strong>DAAC</strong> will help you to Become a certified professional graphic designer. Our expert will guide and increase your skills in web design, mobile app design, multimedia design and computer graphics.
</p>

<!-- <p><strong>LEARNING OUTCOME</strong></p>

<p>Art works for web, print and mobile devices Designing brochures, flyers, business cards, icons, leaplets, t shirt designs, mock ups etc. e-mailers, news letters Brand identity designs Integrate external files from other adobe products like Photoshop, indesign and flash etc.</p> -->

						</div><?php }?>

										<?php if ($this->request->url == "photoshop-institute-in-jaipur.html") {?>
						<div class="col-md-6 align-self-center">

<p><strong>DAAC</strong> offers the best photoshop courses for beginners that give you basic knowledge of tools so you can design great graphics and images. They will teach you how to edit images by correcting colors, retouching portraits, adjusting brightness and many more.</p>
<p>
Students will learn how to use stunning text style to make your design more creative. They will understand how to use creative effects to design characters, business cards, icons, covers formatting text and many more.</p>


						</div><?php }?>



											<?php if ($this->request->url == "dreamweaver-training-in-jaipur.html") {?>
						<div class="col-md-6 align-self-center">

<p><strong>DAAC</strong> is best Dreamweaver Coaching Classes in Jaipur which offer best training courses and learn about html, css and many more. Students will learn how to use Dreamweaver to make new and creative designs. They will understand how to use dreamweaver to build characters, business cards, icons, covers formatting text and many more.</p>


						</div><?php }?>

			<?php if ($this->request->url == "3d-animation-in-jaipur.html") {?>
						<div class="col-md-6 align-self-center">

<p>3D Animation training course aim to guide students the important concepts of drawing, sketching and arts in creative ways. <strong>DAAC</strong> will help candidates to explore their drawing and ideas as an art form. </p>
<p>
Students learn how to develop stunning advanced level design through illustrations and adobe photoshop. In the animation industry you will become a successful 3d animation professional via <strong>DAAC</strong> which is the best animation training institute in jaipur.
</p>


						</div><?php }?>

						<?php if ($this->request->url == "sketching-training-institute-Jaipur.html") {?>
						<div class="col-md-6 align-self-center">
									<h3 style="text-align: left;"><strong>Design Sketching Course</strong></h3>
<p>Sketching course helps students improve their drawing skills, and they also typically cover general design principles. Read on to explore profiles of some common sketching courses.</p>

						</div><?php }?>





						<?php if ($this->request->url == "react-native-training-in-jaipur.html") {?>
						<div class="col-md-6 align-self-center">

<p>React Native is a popular, high-performance JavaScript library for building rapidly responsive user interfaces. Developers use React native to quickly and efficiently build large cross platform mobile applications for iOS and Android. It automatically manages all UI updates when the underlying data changes. It is used by many of the Internet’s most popular apps including Skype, Instagram, Netflix, Airbnb, Swiggy, Uber and many others
In <strong>DAAC</strong> you will learn to manage your application with redux and know how to apply this to react-native.

</p>
						</div><?php }?>

<?php if ($this->request->url == "full-stack-web-developer-training.html") {?>
						<div class="col-md-12">

<p>A Full Stack Developer Course which helps to become an expert who works on the front end and back end both. The person should be acquainted with the 3-tier model for excellent execution of procedures. He or she should be well aware of the concepts of user interface which constitutes the front end of any application. A Full Stack Developer is also aware of the data support part of back end. <strong>DAAC</strong> is one of the Best Full Stack Development Course Training Institute in Jaipur.
</p>

					</div><?php }?>


					<?php if ($this->request->url == "magento-training-in-Jaipur.html") {?>
						<div class="col-md-12">

<p>Magento is an open-source e-commerce platform written in PHP. It is one of the most popular open e-commerce systems in the network. This software is created using the Zend Framework. Magento source code is distributed under Open Software License (OSL) v3.0, an Open Source Initiative (OSI)-approved license, which is similar to the AGPL but not GPL compliant.</p>
<p>
The software was originally developed by Varien, Inc, a US private company headquartered in Culver City, California, with assistance from volunteers.</p>
<p>
More than 100,000 online stores have been created on this platform. The platform code has been downloaded more than 2.5 million times, and $155 billion worth of goods have been sold through Magento-based systems in 2019.[3] Two years ago, Magento accounted for about 30% of the total market share.[4].
</p>

					</div><?php }?>



					<?php if ($this->request->url == "laravel-training-in-jaipur.html") {?>
						<div class="col-md-12">

<p>Laravel is a free, open-source[3] PHP web framework, created by Taylor Otwell and intended for the development of web applications following the model–view–controller (MVC) architectural pattern and based on Symfony. Some of the features of Laravel are a modular packaging system with a dedicated dependency manager, different ways for accessing relational databases, utilities that aid in application deployment and maintenance, and its orientation toward syntactic sugar.
</p>

					</div><?php }?>

						<?php if ($this->request->url == "wordpress-training-jaipur.html") {?>
						<div class="col-md-12">

<p>WordPress (WordPress.org) is a free and open-source content management system (CMS) written in PHP[4] and paired with a MySQL or MariaDB database. Features include a plugin architecture and a template system, referred to within WordPress as Themes. WordPress was originally created as a blog-publishing system but has evolved to support other types of web content including more traditional mailing lists and forums, media galleries, membership sites, learning management systems (LMS) and online stores. WordPress is used by more than 60 million websites,[5] including 33.6% of the top 10 million websites as of April 2019,[6][7] WordPress is one of the most popular content management system solutions in use.[8] WordPress has also been used for other application domains such as pervasive display systems (PDS).[9].
</p>

					</div><?php }?>



						<?php if ($this->request->url == "drupal-training-in-jaipur.html") {?>
						<div class="col-md-12">

<p>Drupal is powerful, flexible software and is ideal for sites with many features and users.

Many government agencies use Drupal, including WhiteHouse.gov, and the national sites for countries such as india and France.

Many famous companies use Drupal including Pfizer, Sony and the New York Stock Exchange.
Because Drupal is in demand with large organizations, the Drupal job market is very strong.
If you know Drupal well, you have a great chance of finding work.
</p>

					</div><?php }?>



						<?php if ($this->request->url == "angular-JS-training-in-jaipur.html") {?>
						<div class="col-md-12">

<p><strong>DAAC</strong> Angular training course will help you master this JavaScript framework for building web applications. As part of the training, you will learn features of Angular and Angular Component Libraries, need for TypeScript, life cycle of Angular components, how to create responsive web designs and more through real-world projects and case studies. Angular allows developers to easily build dynamic, responsive single-page web applications that dynamically rewrite portions of the current page rather than having to generate a new page in response to every request.
</p>

					</div><?php }?>


						<?php if ($this->request->url == "java-training-jaipur.html") {?>
						<div class="col-md-12">

<p>Advance Java is the portion of java which is used for making enterprise level application and works as tool for backend web-dev
and future of java in growing industry is nothing but advance java .
Technically it is called J2EE .Advance java doesnt means something which is very very tough or
a new type of java that will blow your mind .
Actually this type of terminology is given by some people who are not familiar with technical jargon.
If you want to learn it without wasting a second enroll at <strong>DAAC</strong> and get trained by the professionals.
</p>

					</div><?php }?>


						<?php if ($this->request->url == "mySQL-training-in-jaipur.html") {?>
						<div class="col-md-12">

<p>MySQL® is the open source community's most popular Relational Database Management System (RDBMS) offering, and is a key part of LAMP – Linux™, Apache™, MySQL®, PHP/Perl/Python®. Many Fortune 500 companies adopt MySQL to reap the benefits of an open source, platform-independent RDMS, such as simplifying conversion from other platforms and lowering database Total Cost of Ownership by 90%. This class encourages the student to explore database fundamentals, as well as MySQL features. Students learn the basics of MySQL use and the programming of stored routines and triggers. Students also participate in database design discussions, perform administrative functions, learn about optimization and performance tuning, and explore various APIs. This course covers MySQL 5.6.
</p>

					</div><?php }?>
<?php if ($this->request->url == "mean-stack-web-development-training.html") {?>
							<div class="col-md-12">

<p>Join Mean stack web development courses and take your career to the next level. Become a front and back end mean stack web developer with latest javascript technology. Learn bottom to top knowledge of creating web applications. You will also gain expertise and skills on the most popular MEAN(MongoDB, Express, Angular and Node.js) stack. <strong>DAAC</strong> is best MEAN Stack Development Training institute in Jaipur which gives advanced level of mean stack development training on live projects.

</p></div>
	<?php }?>

								<?php if ($this->request->url == "core-java-training-in-jaipur.html") {?><div class="col-md-12">

<p> Java is a platform independent language. It is an object-oriented programming language developed by Sun Microsystems, a company best known for its high-end Unix workstations. Modeled after C++, the Java language was designed to be small, simple, and portable across platforms and operating systems.</p>
<p>The demand of the market is growing for the strong Java developers to work within a global team . <strong>DAAC</strong> is one of the best institutes for core java in JAIPUR.  When we enroll the student we take full responsible for developing, enhancing, modifying and/or maintaining the student’s intellectual knowledge with the core java and other important technical terms.

</p></div>
	<?php }?>

									<?php if ($this->request->url == "dotnet-training-jaipur.html") {?><div class="col-md-12">

<p> .NET Framework is a development framework that provides a new programming interface to
Windows services and APIs, and integrates a number of technologies that emerged from Microsoft during
the late 1990s.	.NET having an great scope so enroll you in DOTNET  			 Training in <strong>DAAC</strong>  having an well experienced IT professionals providing real time training with Live Project, with 100% placement
with top indusries.

</p></div>
	<?php }?>


									<?php if ($this->request->url == "web-development-course-in-jaipur.html") {?>		<div class="col-md-12">

<p>Become a web developer and work on real projects. Get training on live projects and learn from experts which will help you to grow your technical skills. You will understand and know how to build a website and web application. <strong>DAAC</strong> web development training course provides up to date with latest technology and you will become a professional web developer. </p>
<p>
web development is the trending and most demanding programming language at this time. Our main aim is to provide a web development course to equip students with the latest and unique skills so they can easily build web applications and backed APIs.


</p></div>
	<?php }?>




									<?php if ($this->request->url == "codeigniter-training-in-jaipur.html") {?>		<div class="col-md-12">

<p>CodeIgniter is a PHP MVC framework for developing applications rapidly. CodeIgniter provides out of the box libraries for connecting to the database and performing various operations. Like sending emails, uploading files, managing sessions, etc.</p>
<p>
CodeIgniter Features
Let's see some of the features that make CodeIgniter great. The following list is not exhaustive but gives you an idea of what to expect when working with CodeIgniter.


</p></div>
	<?php }?>


										<?php if ($this->request->url == "advance-react-native-training-in-jaipur.html") {?>		<div class="col-md-6">

<p>Start your career as a react native developer. <strong>DAAC</strong> started a trending course which is a react native course for beginners. Our react native professionals are given the whole knowledge of building react native programs for pc to mobile phone. Students learn how to work with nosql and RNative databases.</p>
<p>
In <strong>DAAC</strong> react native training course, you will learn how to create a user interface for android and ios with react native, and for the web with react. You will learn to manage your application with redux work and know how to apply this to react-native.


</p></div>
	<?php }?>


											<?php if ($this->request->url == "basic-computer-training-jaipur.html") {?>		<div class="col-md-12">

<p>In today's era each and every person should be computer literate so that he can easily handle the basic computerized stuff. For that purpose Sourcekode technologies started basic computer & IT training courses for housewives & old-age people. <strong>DAAC</strong> is one of the  Best Computer  Institute in Jaipur.


</p></div>
	<?php }?>

									<?php if ($this->request->url == "ethical-hacking-in-Jaipur.html") {?>		<div class="col-md-12">

<p>Get trained by Authorized EC Council Training Partner for the Ethical Hacking Training Course in  Jaipur.The world’s most advanced ethical hacking course with 20 of the most current security domains an ethical hacker will want to know when planning to beef up the information security posture of their organization. In 20 comprehensive modules, the course covers over 270 attack technologies, commonly used by hackers.</p>
<p>
You walk out the door with ethical hacking skills that are highly in demand, as well as the globally recognized Certified Ethical Hacker certification!


</p></div>
	<?php }?>



							<?php if ($this->request->url == "autocad-training-in-jaipur.html") {?>		<div class="col-md-12">

<p><strong>DAAC</strong>  offers AutoCAD Mechanical Training Course which is an Autodesk Authorised Training & Certification program. In this course we’ll be taking you through all of the AutoCAD Mechanical software and getting you to become an efficient and effective user of that AutoCAD Mechanical software. The course covers the basics of the AutoCAD Mechanical user interface and leads you step-by-step through producing precise, measured mechanical engineering drawings and designs.


</p></div>
	<?php }?>


								<?php if ($this->request->url == "selenium-course-in-Jaipur.html") {?>		<div class="col-md-12">

<p>SQT training in Jaipur SQT NCR provided by <strong>DAAC</strong>. We provide All IT training as per industries standards. In software testing we will learn that how we will going to test any web based application or desktop application in the terms of there specified functionality, performance, security, data base connectivity, comparability not only this there are much more aspects which is checked before launching any application. Here, at <strong>DAAC</strong> laboratory are well-structured for SQT, Software Tesing training in Jaipur where contenders learn the career oriented skills to uphold the career path. If we talked about SQT, best Software Tesing training center in Jaipur, then we provide skills for Software Development Life Cycle : SDLC Methodologies Followed in Industry, Software Testing Life Cycle : The Product Overview, Software Configuration Management (SCM), Quality Standards ( CMM &amp; ISO ) &ndash; Overview, Verification , Validation and Evaluation Techniques, Test Planning , Test Case Design and Execution : With Live Examples, Various Testing Techniques and Types, Testing Estimation and Control: Techniques to Manage in different Environment &amp; Defect/Bug Life Cycle : Tracking and Reporting Methods etc training at live projects training under SQT, Software Tesing Training program. SQT, Software Tesing training course has designed according to the latest technologies which are using in corporation at high level. <strong>DAAC</strong> structured SQT, Software Tesing training course content and syllabus in Jaipur according to student&rsquo;s requirement to be prepare for industries through which candidates can easily get placement in their dreamed companies and corporations. You will start learning from very basic starting from process how a software company works,how the requirement is gathered, who gathered the requirement than what happen with that requirement designing,coding, testing, maintenance this is secondary part of the course.</p>

</div>
	<?php }?>



								<?php if ($this->request->url == "data-science-course-training.html") {?>		<div class="col-md-12">

<p>Data Science is an advanced technology that makes use of robust techniques and methodologies to analyze large volumes of Big Data. The deep data insights that are extracted from Big Data will help the enterprises to make effective business decisions. Data Science has now become a major necessity for businesses irrespective of its size. In the present age of Big Data, to stay competitive, enterprises need to efficiently develop and implement Data Science capabilities or risk being left behind.


</p>
<p><strong>"India has the most number of job vacancies in Data Science. This is the perfect time to step into the job market of Data Science.”</strong></p>

<p>At present, the best institute for Data Course in Jaipur is <strong>DAAC</strong>. As a part of our Data Science training, participants will learn advanced technologies like Artificial Intelligence, Machine Learning, Deep Learning and TensorFlow. Advanced Data Science concepts like Business Analytics, Predictive Analytics, Forecasting, Text Mining, Hadoop and Apache Spark, Big Data Analytics are also covered as a part of this Data Science training program.</p>

<p><strong>"The Learning curve in Data Science, AI & ML is steep & it opens a plethora of exciting opportunities across various industries"</strong></p>

</div>
	<?php }?>



								<?php if ($this->request->url == "linux-training-jaipur.html") {?>		<div class="col-md-12">

<p>Linux powers 94% of the world’s supercomputers, most of the servers powering the Internet, the majority of financial trades worldwide and a billion Android devices. In short, Linux is everywhere. It appears in many different architectures, from mainframes to servers to desktop to mobile and on a staggeringly wide variety of hardware.</p>
<p>
Moreover, 97 % of hiring managers reported that they will prioritize hiring Linux talent relative to other skills areas in the next six months, and 44 percent of hiring managers saying they’re more likely to hire a candidate with Linux certification.


</p>

</div>
	<?php }?>


								<?php if ($this->request->url == "advance-excel-training.html") {?>		<div class="col-md-12">

<p>Are you looking for the best Advance Excel Training in Jaipur ? Get  the most comprehensive, basic and advanced level Excel training in Jaipur. Skill Level. Beginner. We are providing Training to the needs from Beginners level to Experts level,with real-time projects and working models.</p>
<p>
<strong>DAAC</strong> Offering Deep level understanding about the course by Adding Advanced level of concepts to Our ADV EXCEL Course Curriculum  . We just don’t Provide Advance Excel course we help you to understand the Technology from the Scratch level to Advanced level by Ensuring All the Advanced Topics during your Learning Journey with us.</p>
<p>
So if you are the one who is searching for the Best ADVANCE EXCEL  training institute in Jaipur /EXCEL  training near me/EXCEL  training in Mansarovar / Vidhyadhar Nagar / C Scheme . Enroll to Our Advance Excel  course Today itself.


</p>

</div>
	<?php }?>


									<?php if ($this->request->url == "accounting-professional-course-in-jaipur.html") {?>		<div class="col-md-12">

<p>Accounting means keeping the detailed record of a specific resource, asset, liability, revenue or expense. It provides data for decision making in any business by tracking of transactions and estimating the economic activities of an organization. </p>
<p>
<strong>DAAC</strong> offers you one of the best Certification in Accounting  Courses. The expert professionals from industry have well designed the outline of this Certification  in Accounting Professional . We make efforts to make our students highly employable and proficient. Along with the  practical knowledge we focus more on the practical application of the subject as in the real industry.  So, join us and start the beginning of a career having vast scope.


</p>

</div>
	<?php }?>


								<?php if ($this->request->url == "banking-&-finance-training-in-jaipur.html") {?>		<div class="col-md-12">

<p>A course in Banking & Finance prepares students with relevant knowledge and skills to face challenges in the banking industry. The course equips a student with knowledge about financial markets, economics concepts and theories, financial management, accounting, banking operations, derivatives markets as well as credit management and customer relationship management. Students get to learn these theories and work on projects during the duration of the programme. The course readies students for management roles in finance and banking sectors across the globe.


</p>

</div>
	<?php }?>


									<?php if ($this->request->url == "penetration-testing-training-in-jaipur.html") {?>		<div class="col-md-12">

<p>Web applications are an integral part of today's world and web applications are there in nook and cranny of any organization from human resource management to share market. So its imperative that security of these applications plays an important of the business hence our course. In this in-depth, hands-on training course you will learn the art of exploiting and securing the web applications.</p>
<p>
 In <strong>DAAC</strong>  You will learn from basics of web applications to the advanced attacks which range from SQL Injection to web services hacking. This course will also help you learn methodological way of testing complex web applications starting from reconnaissance to the VAPT report creation, and all these with the help of state of the art tools.


</p>

</div>
	<?php }?>





						

			<div class=" align-self-center">
										<section id="carrer_coursdur" <?php if ($iteam['course_group'] != "Designing" && $iteam['course_group'] != "Mobile Development") {?> style="padding:0px" <?php }?>>
<div class="container">
<div class="position_div">
<div class="inner_maindiv" <?php if ($iteam['course_group'] != "Designing" && $iteam['course_group'] != "Mobile Development") {?> style="margin-top:0px" <?php }?>>
<div class="inner_div">
	<h3>Course Feature</h3>
<div class="course_cdiv">

<h6>
<?php

    $fruits = $iteam['cname'];
    $fruits_ar = explode(' ', $fruits);

    if (count($fruits_ar) > 3) {
        $cours = implode(' ', array_slice($fruits_ar, 0, 3));
        echo $cours;
    } else {
        echo $iteam['cname'];
    }

/* $limit=3; if (str_word_count($iteam['cname'], 0) > $limit) {
$words = str_word_count($iteam['cname'], 2);
$pos = array_keys($words);
$text = substr($iteam['cname'], 0, $pos[$limit]);
echo $text;
}else{
echo $iteam['cname']; } */?> </h6>

<h3>
<?php
/* $limit=3;
    if (str_word_count($iteam['sname'], 0) > $limit) {
    $words = str_word_count($iteam['sname'], 2);
    $pos = array_keys($words);
    $text = substr($iteam['sname'],  $pos[$limit]);
    echo $text; */

    if (count($fruits_ar) > 3) {
        echo $iteam['sname'];
    }?>

</h3>

</div><!--course_cdiv-->
<ul class="list-unstyled course_dur_list">
<li>
<i class="fa fa-hourglass-start" aria-hidden="true"></i>
<strong>Duration</strong>
<span><?php echo $iteam['duration']; ?></span>

</li>
<li>
<i class="fa fa-clock-o" aria-hidden="true"></i>

<strong>Class Timings</strong>
<span><?php echo $iteam['timing']; ?></span>

</li>
<li>
<i class="fa fa-check-circle-o" aria-hidden="true"></i>

<strong>Eligibility</strong>
<span><?php echo $iteam['eligible']; ?></span>

</li>
</ul><!--row-->
</div><!--inner_div-->
</div><!--inner_maindiv-->
</div><!--position_div-->

<div class="course_boxes">

</div><!--course_boxes-->
</div><!--container-->
</section><!--carrer_coursdur-->
		</div>
		</div>
</div>
	</div>




<section id="modual_sec">


<?php if ($this->request->url == "student-mobile-app-work.html") {?>
									<div class="student_workdiv  studentappwdiv">
<div class="container-fluid">
<ul>
	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/001.png" />
<a href="https://play.google.com/store/apps/details?id=com.doomshell.maatrishla&hl=en_IN" target="_blank">Visit App</a>
	</div>
	</li>
	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/002.png" />
<a href="javascript:void(0);" target="_blank">Visit App</a>
	</div>
	</li>
	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/003.png" /><a href="javascript:void(0);" target="_blank">Visit App</a></div>
	</li>
	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/004.png" /><a href="javascript:void(0);" target="_blank">Visit App</a></div>
	</li>
	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/005.png" /><a href="javascript:void(0);" target="_blank">Visit App</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/006.png" /><a href="javascript:void(0);" target="_blank">Visit App</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/007.png" /><a href="javascript:void(0);" target="_blank">Visit App</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/008.png" /><a href="javascript:void(0);" target="_blank">Visit App</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/009.png" /><a href="javascript:void(0);" target="_blank">Visit App</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/010.png" /><a href="https://play.google.com/store/apps/details?id=com.doomshell.imaa&hl=en_IN" target="_blank">Visit App</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/011.png" /><a href="javascript:void(0);" target="_blank">Visit App</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/012.png" /><a href="javascript:void(0);" target="_blank">Visit App</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/013.png" /><a href="javascript:void(0);" target="_blank">Visit App</a></div>
	</li>


	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/014.png" /><a href="javascript:void(0);" target="_blank">Visit App</a></div>
	</li>


	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/015.png" /><a href="https://play.google.com/store/apps/details?id=com.doomshell.tossclick&hl=en_IN" target="_blank">Visit App</a></div>
	</li>

		<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/016.png" /><a href=" https://play.google.com/store/apps/details?id=com.coopselfservice.flashticket   " target="_blank">Visit App</a></div>
	</li>

		<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/017.png" /><a href="https://play.google.com/store/apps/details?id=com.doomshell.schoolapp&hl=en   " target="_blank">Visit App</a></div>
	</li>

		<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/018.png" /><a href="https://play.google.com/store/apps/details?id=com.doomshell.lexcarwash&hl=en" target="_blank">Visit App</a></div>
	</li>

		<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/019.png" /><a href="https://play.google.com/store/apps/details?id=com.garment_india.gear&hl=en" target="_blank">Visit App</a></div>
	</li>
</ul>

<div id="background_overlay">&nbsp;</div>
</div>
</div>

	<?php }?>



	
		<?php if ($this->request->url == "student-cake-php-work.html") {?>
									<div class="student_workdiv">
<div class="container-fluid">
<ul>
	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_001.jpg" />
<a href="https://geminipipes.com/" target="_blank">Visit Site</a>
	</div>
	</li>
	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_002.jpg" />
<a href="http://lbnjodhpur.org/" target="_blank">Visit Site</a>
	</div>
	</li>
	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_003.jpg" /><a href="http://flashticket.co-opselfservice.com/" target="_blank">Visit Site</a></div>
	</li>
	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_004.jpg" /><a href="https://www.propertybull.com/" target="_blank">Visit Site</a></div>
	</li>
	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_005.jpg" /><a href="https://www.lexcarwashapp.com/" target="_blank">Visit Site</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_010.jpg" /><a href="https://www.bookanartiste.com/" target="_blank">Visit Site</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_011.jpg" /><a href="http://thelawdesk.org/" target="_blank">Visit Site</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_012.jpg" /><a href="http://www.thevelvetbling.com/" target="_blank">Visit Site</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_013.jpg" /><a href="http://fdmexperience.com/" target="_blank">Visit Site</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_014.jpg" /><a href="http://hamegayeset.com/" target="_blank">Visit Site</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_015.jpg" /><a href="https://fullrangephysio.ca/" target="_blank">Visit Site</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_016.jpg" /><a href="http://neewara.com/" target="_blank">Visit Site</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_017.jpg" /><a href="http://edtc.co-opselfservice.com/" target="_blank">Visit Site</a></div>
	</li>
</ul>

<div id="background_overlay">&nbsp;</div>
</div>
</div>

	<?php }?>


		<?php if ($this->request->url == "student-html-work.html") {?>
									<div class="student_workdiv">
<div class="container-fluid">
<ul>
	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_001.jpg" />
<a href="https://geminipipes.com/" target="_blank">Visit Site</a>
	</div>
	</li>
	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_002.jpg" />
<a href="http://lbnjodhpur.org/" target="_blank">Visit Site</a>
	</div>
	</li>
	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_003.jpg" /><a href="http://flashticket.co-opselfservice.com/" target="_blank">Visit Site</a></div>
	</li>
	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_004.jpg" /><a href="https://www.propertybull.com/" target="_blank">Visit Site</a></div>
	</li>
	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_005.jpg" /><a href="https://www.lexcarwashapp.com/" target="_blank">Visit Site</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_010.jpg" /><a href="https://www.bookanartiste.com/" target="_blank">Visit Site</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_011.jpg" /><a href="http://thelawdesk.org/" target="_blank">Visit Site</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_012.jpg" /><a href="http://www.thevelvetbling.com/" target="_blank">Visit Site</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_013.jpg" /><a href="http://fdmexperience.com/" target="_blank">Visit Site</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_014.jpg" /><a href="http://hamegayeset.com/" target="_blank">Visit Site</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_015.jpg" /><a href="https://fullrangephysio.ca/" target="_blank">Visit Site</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_016.jpg" /><a href="http://neewara.com/" target="_blank">Visit Site</a></div>
	</li>

	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/cakep_017.jpg" /><a href="http://edtc.co-opselfservice.com/" target="_blank">Visit Site</a></div>
	</li>
</ul>

<div id="background_overlay">&nbsp;</div>
</div>
</div>

	<?php }?>



		<?php if ($this->request->url == "student-digital-marketing-work.html") {?>
									<div class="student_workdiv">
<div class="container-fluid">
<ul>
	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/digi_001.jpg" />
<a href="https://www.facebook.com/DAACJAIPUR" target="_blank">Visit Page</a>
	</div>
	</li>
	<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/digi_002.jpg" />
<a href="https://www.youtube.com/c/DAACJaipur" target="_blank">Visit Page</a>
	</div>
	</li>

		<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/digi_003.jpg" />
<a href="https://www.facebook.com/DOOMSHELL" target="_blank">Visit Page</a>
	</div>
	</li>

		<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/digi_004.jpg" />
<a href="https://www.youtube.com/channel/UCjGjEEgmVi_Ah8l-8AAq-YQ" target="_blank">Visit Page</a>
	</div>
	</li>

		<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/digi_005.jpg" />
<a href="https://www.facebook.com/AstroTrishla/" target="_blank">Visit Page</a>
	</div>
	</li>

		<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/digi_006.jpg" />
<a href="http://www.youtube.com/c/sarthiastrotrishla" target="_blank">Visit Page</a>
	</div>
	</li>

		<li>
	<div><img alt="Image Caption" class="thumb_gallery" src="https://daac.in/images/digi_007.jpg" />
<a href="https://www.facebook.com/PropertyBull/" target="_blank">Visit Page</a>
	</div>
	</li>

</ul>

<div id="background_overlay">&nbsp;</div>
</div>
</div>

	<?php }?>


<?php echo $iteam['description']; ?>


<section id="course_catdiv">
<?php echo $iteam['course_category']; ?>
</section><!--course_catdiv-->
</section><!--course_catdiv-->




<?php if ($this->request->url == "web-designing-diploma-2.html") {?>
	<section id="course_mnvidsec">
		<hgroup class="main_grouphead">
<h2>Course Video</h2>
<h5>Course Video</h5>
</hgroup>
		<div class="container">
			<iframe width="100%" height="500" src="https://www.youtube.com/embed/R_t7iY4linw?enablejsapi=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen autoplay  id="autoplay-video"></iframe>
		</div>

	</section>
<?php }?>


<?php //pr($_SERVER);
    if ($_SERVER['REQUEST_URI'] == "/photo-shop.html") {?>

<section id="course_vidsec">
	<h3 class="heading">Advance Photoshop Time Lapse Video</h3>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
<div class="box">
	<iframe width="100%" height="315" src="https://www.youtube.com/embed/LvxLEVZLaz4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
			 </div>	<div class="col-md-4">
<div class="box">
	<iframe width="100%" height="315" src="https://www.youtube.com/embed/TQb4QTlW5e0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
			 </div>

			 		<div class="col-md-4">
<div class="box">
<iframe width="100%" height="315" src="https://www.youtube.com/embed/vzIfRHHyyss" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
			 </div>
		</div>


		</div>
	</div>
</section>

<?php }
    ?>
<?php //pr($_SERVER);
    if ($_SERVER['REQUEST_URI'] == "/web-designing-diploma-2.html") {?>


<section id="course_vidsec">
	<h3 class="heading">Sketching Time Lapse Video</h3>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
<div class="box">
<iframe width="100%" height="315" src="https://www.youtube.com/embed/QN0X4MLNyIA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
			 </div>	<div class="col-md-4">
<div class="box">
	<iframe width="100%" height="315" src="https://www.youtube.com/embed/s5DDQZcGR3A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
			 </div>

			 		<div class="col-md-4">
<div class="box">
<iframe width="100%" height="315" src="https://www.youtube.com/embed/63tlfeugbo0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
			 </div>

			 <div class="col-md-4">
<div class="box">
	<iframe width="100%" height="315" src="https://www.youtube.com/embed/LvxLEVZLaz4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
			 </div>	<div class="col-md-4">
<div class="box">
	<iframe width="100%" height="315" src="https://www.youtube.com/embed/TQb4QTlW5e0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
			 </div>

			 		<div class="col-md-4">
<div class="box">
<iframe width="100%" height="315" src="https://www.youtube.com/embed/vzIfRHHyyss" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
			 </div>
		</div>


		</div>
	</div>
</section>

<?php }
    ?>









<?php }?>
<?php if ($this->request->url == "photo-shop.html") {?>
<section class="course_plan photo_shop_duration">
	<div class="container"><div class="pricing-plans">
					 <div class="wrap">



						<div class="pricing-grids">
						<div class="pricing-grid1">
							<div class="price-value">
									<h2><a href="#"> BASIC</a></h2>
									<h5><span>Duration</span><lable> / 1 month</lable></h5>
									<div class="sale-box">

							</div>

							</div>
							<div class="price-bg">
							<ul>
							<li><a href="#">Basic Tools </a></li>

								<li class="whyt"><a href="#">Brochure Designing </a></li>
								<li><a href="#"> Web Layouts</a></li>


							</ul>

							</div>
						</div>
						<div class="pricing-grid2">
							<div class="price-value two">
								<h3><a href="#">Advance</a></h3>
								<h5><span>Duration</span><lable> / 3 month</lable></h5>
								<div class="sale-box two">

							</div>

							</div>
							<div class="price-bg">
							<ul>
										<li class="whyt"><a href="#">Digital Painting </a></li>
								<li><a href="#">Advance Web Layouts</a></li>
								<li class="whyt"><a href="#">Advance Brochure Designing </a></li>
								<li><a href="#">Logo Designing </a></li>
								<li class="whyt"><a href="#">Adobe Certificate</a></li>
							</ul>

							</div>
						</div>

							<div class="clear"> </div>
							<!--pop-up-grid-->

								<!--pop-up-grid-->
							</div>
						<div class="clear"> </div>

					</div>

				</div></div></section>

			<?php }?>




				<?php if ($this->request->url == "dreamweaver-diploma.html") {?>

<section class="course_plan dreamweaver_diploma_duration"><div class="container"><div class="pricing-plans">
					 <div class="wrap">



						<div class="pricing-grids">
						<div class="pricing-grid1">
							<div class="price-value">
									<h2><a href="#"> BASIC</a></h2>
									<h5><span>Duration</span><lable> / 1 month</lable></h5>
									<div class="sale-box">

							</div>

							</div>
							<div class="price-bg">
							<ul>
								<li class="whyt"><a href="#">Html Introduction </a></li>
								<li><a href="#">HTML5 Tags</a></li>
								<li class="whyt"><a href="#">CSS </a></li>
								<li ><a href="#">CSS3 </a></li>
								<li class="whyt"><a href="#">Layout  </a></li>

							</ul>

							</div>
						</div>
						<div class="pricing-grid2">
							<div class="price-value two">
								<h3><a href="#">Advance</a></h3>
								<h5><span>Duration</span><lable> / 3 month</lable></h5>
								<div class="sale-box two">

							</div>

							</div>
							<div class="price-bg">
							<ul>
										<li class="whyt"><a href="#">Javascript </a></li>
								<li><a href="#">JQuery</a></li>
								<li class="whyt"><a href="#">Responsive </a></li>
								<li><a href="#">Bootstrap </a></li>
								<li class="whyt"><a href="#">Adobe Certificate</a></li>
							</ul>

							</div>
						</div>

							<div class="clear"> </div>
							<!--pop-up-grid-->

								<!--pop-up-grid-->
							</div>
						<div class="clear"> </div>

					</div>

				</div></div></section>
<?php }?>


<?php if ($this->request->url == "web-designing-diploma-2.html") {?>
<section class="course_plan designing_section_duration"><div class="container"><div class="pricing-plans">
					 <div class="wrap">



						<div class="pricing-grids">
						<div class="pricing-grid1">
							<div class="price-value">
									<h2><a href="#"> BASIC</a></h2>
									<h5><span>Duration</span><lable> / 4 month</lable></h5>
									<div class="sale-box">
					<!--		<span class="on_sale title_shop">Not Include</span>-->
							</div>

							</div>
							<div class="price-bg">
							<ul>
								<li class="whyt"><a href="#">Sketching </a></li>
								<li><a href="#">Photoshop</a></li>
								<li class="whyt"><a href="#">Advance Photoshop </a></li>
								<li><a href="#">HTML5 </a></li>
								<li class="whyt"><a href="#">Css</a></li>
							</ul>

							</div>
						</div>
						<div class="pricing-grid2">
							<div class="price-value two">
								<h3><a href="#">Intermediate</a></h3>
								<h5><span>Duration</span><lable> / 6 month</lable></h5>
								<div class="sale-box two">
							<!--<span class="on_sale title_shop">NEW</span>-->
							</div>

							</div>
							<div class="price-bg">
							<ul>
									<li class="whyt"><a href="#">Sketching </a></li>
								<li><a href="#">Photoshop</a></li>
								<li class="whyt"><a href="#">Advance Photoshop </a></li>
								<li><a href="#">HTML5 </a></li>
								<li class="whyt"><a href="#">Css3</a></li>
                                <li><a href="#">Javascript </a></li>
								<li class="whyt"><a href="#">Jquery</a></li>
                                <li><a href="#">Bootstrap </a></li>
								<li class="whyt"><a href="#">Responsive</a></li>
							</ul>

							</div>
						</div>
						<div class="pricing-grid3">
							<div class="price-value three">
								<h4><a href="#">Professional</a></h4>
								<h5><span>Duration</span><lable> / 12 month</lable></h5>
								<div class="sale-box three">
							<!--<span class="on_sale title_shop">NEW</span>-->
							</div>

							</div>
							<div class="price-bg">
							<ul>
								<li class="whyt"><a href="#">Sketching </a></li>
								<li><a href="#">Photoshop</a></li>
								<li class="whyt"><a href="#">Advance Photoshop </a></li>
								<li><a href="#">HTML5 </a></li>
								<li class="whyt"><a href="#">Css3</a></li>
                                <li><a href="#">Javascript </a></li>
								<li class="whyt"><a href="#">Jquery</a></li>
                                <li><a href="#">Bootstrap </a></li>
								<li class="whyt"><a href="#">Responsive</a></li>
								<li><a href="#">Corel Draw</a></li>
								<!-- <li class="whyt"><a href="#">RSCIT </a></li> -->
								<li><a href="#">Adobe Illustrator </a></li>

							</ul>

							</div>
						</div>
							<div style="clear:both;"> </div>
							<!--pop-up-grid-->

								<!--pop-up-grid-->
							</div>
						<div style="clear:both;"> </div>

					</div>

				</div></div></section>

<?php }?>


<?php
if ($iteam['course_group'] != "Students Work") {
    if ($this->request->url == "advance-mobile-development.html" || $this->request->url == "mobile-application-development.html" || $this->request->url == "iphone-training-jaipur.html" || $this->request->url == "react-native.html" || $this->request->url == "ionic-angularjs4-training-jaipur.html" || $this->request->url == "oracle.html") {?>

<section id="our_facltypro">
<div class="container">
<hgroup>
          <h2>Our  Projects</h2>
          <p>Are You Looking For</p>
          <h6>Projects</h6>
        </hgroup>
<div class="owl-carousel owl-theme owl-loaded" id="course_appcrausal">


<div class="owl-item"><a href="https://play.google.com/store/apps/details?id=com.doomshell.maatrishla" target="_blank"><img src="<?php echo SITE_URL; ?>images/app_screen_12.png" alt="">
<span>Astro Trishla</span>
</a></div>

	<div class="owl-item"><a href="https://play.google.com/store/apps/details?id=com.doomshell.euroguard.mainfile" target="_blank"><img src="<?php echo SITE_URL; ?>images/app_screen_11.png" alt="">
<span>Euroguard</span>
	</a></div>

<div class="owl-item"><a href="https://play.google.com/store/apps/details?id=com.hotdealsatwork.ibuysell" target="_blank"><img src="<?php echo SITE_URL; ?>images/app_screen_3.png" alt="">
<span>I Buy Sell</span>
</a></div>

<div class="owl-item"><a href="https://play.google.com/store/apps/details?id=com.iosresonanse.carrentalchoice" target="_blank"><img src="<?php echo SITE_URL; ?>images/app_screen_7.png" alt="">
<span>Car Rental Choice</span>

</a></div>

<div class="owl-item"><a href="https://play.google.com/store/apps/details?id=com.doomshell.tossclick" target="_blank"><img src="<?php echo SITE_URL; ?>images/app_screen_5.png" alt="">
<span>Tossclick</span>

</a></div>

<div class="owl-item"><a href="https://play.google.com/store/apps/details?id=com.flashticket&hl=en" target="_blank"><img src="<?php echo SITE_URL; ?>images/app_screen_9.png" alt="">
<span>Flash Ticket</span>

</a></div>


</div>
</div>
</section>
<?php } else {
        ?>

<section id="our_facltypro">
	<div class="container">
	<hgroup>
          <h2>Our  Projects</h2>
          <p>Are You Looking For</p>
          <h6>Projects</h6>
        </hgroup>


		<div class="row">
			<div class="col-md-4">
				<div class="faclty_probox">
					<a href="https://www.bookanartiste.com/" target="_blank">
						<img src="<?php echo SITE_URL; ?>images/001.jpg" alt="">
						<div class="faclty_probox_overlay">
							<p>Book an Artiste</p>
						</div>	<h6>Book an Artiste</h6>
					</a>

				</div>
			</div>

					<div class="col-md-4">
				<div class="faclty_probox">
					<a href="http://thelawdesk.org/" target="_blank">
						<img src="<?php echo SITE_URL; ?>images/002.jpg" alt="">
						<div class="faclty_probox_overlay">
							<p>The Law Desk</p>
						</div><h6>The Law Desk</h6>
					</a>
				</div>
			</div>

					<div class="col-md-4">
				<div class="faclty_probox">
					<a href="http://www.thevelvetbling.com/" target="_blank">
						<img src="<?php echo SITE_URL; ?>images/003.jpg" alt="">
						<div class="faclty_probox_overlay">
							<p>The Velvet Bling</p>
						</div><h6>The Velvet Bling</h6>
					</a>
				</div>
			</div>

					<div class="col-md-4">
				<div class="faclty_probox">
					<a href="http://fdmexperience.com/" target="_blank">
						<img src="<?php echo SITE_URL; ?>images/004.jpg" alt="">
						<div class="faclty_probox_overlay">
							<p>FDM Experience</p>
						</div><h6>FDM Experience</h6>
					</a>
				</div>
			</div>

					<div class="col-md-4">
				<div class="faclty_probox">
					<a href="http://hamegayeset.com/" target="_blank">
						<img src="<?php echo SITE_URL; ?>images/005.jpg" alt="">
						<div class="faclty_probox_overlay">
							<p>Bnext Solutions</p>
						</div><h6>Bnext Solutions</h6>
					</a>
				</div>
			</div>

							<div class="col-md-4">
				<div class="faclty_probox">
					<a href="https://www.propertybull.com/" target="_blank">
						<img src="<?php echo SITE_URL; ?>images/006.jpg" alt="">
						<div class="faclty_probox_overlay">
							<p>Property Bull</p>
						</div><h6>Property Bull</h6>
					</a>
				</div>
			</div>

							<div class="col-md-4">
				<div class="faclty_probox">
					<a href="http://fullrangephysio.ca/" target="_blank">
						<img src="<?php echo SITE_URL; ?>images/007.jpg" alt="">
						<div class="faclty_probox_overlay">
							<p>Full Range</p>
						</div><h6>Full Range</h6>
					</a>
				</div>
			</div>

									<div class="col-md-4">
				<div class="faclty_probox">
					<a href="http://www.astrotrishla.com/" target="_blank">
						<img src="<?php echo SITE_URL; ?>images/008.jpg" alt="">
						<div class="faclty_probox_overlay">
							<p>Astro Trishla</p>
						</div><h6>Astro Trishla</h6>
					</a>
				</div>
			</div>

											<div class="col-md-4">
				<div class="faclty_probox">
					<a href="http://neewara.com/" target="_blank">
						<img src="<?php echo SITE_URL; ?>images/009.jpg" alt="">
						<div class="faclty_probox_overlay">
							<p>Neewara</p>
						</div><h6>Neewara</h6>
					</a>
				</div>
			</div>

								<div class="col-md-4">
				<div class="faclty_probox">
					<a href="http://edtc.co-opselfservice.com/" target="_blank">
						<img src="<?php echo SITE_URL; ?>images/010.jpg" alt="">
						<div class="faclty_probox_overlay">
							<p>EDTC</p>
						</div><h6>EDTC</h6>
					</a>
				</div>
			</div>

								<div class="col-md-4">
				<div class="faclty_probox">
					<a href="http://www.kbctindia.org/" target="_blank">
						<img src="<?php echo SITE_URL; ?>images/011.jpg" alt="">
						<div class="faclty_probox_overlay">
							<p>kbct india</p>
						</div><h6>kbct india</h6>
					</a>
				</div>
			</div>

									<div class="col-md-4">
				<div class="faclty_probox">
					<a href="https://missbalitropix.com/" target="_blank">
						<img src="<?php echo SITE_URL; ?>images/012.jpg" alt="">
						<div class="faclty_probox_overlay">
							<p>Miss Bali Tropix</p>
						</div><h6>Miss Bali Tropix</h6>
					</a>
				</div>
			</div>


		</div>
	</div>
</section>
<?php }}?>

<section id="related_coursediv">

<div class="container">
<hgroup>
          <h2>Related  Courses</h2>
          <p>Are You Looking For</p>
          <h6>Courses</h6>
        </hgroup>




<div class="owl-carousel owl-theme owl-loaded" id="related_courcrausal">
  <?php foreach ($cou_image as $value) {
    if ($value['image'] != '') {?>
 <div class="owl-item"> <a href="<?php echo SITE_URL; ?><?php echo $value['url']; ?>"><img src="<?php echo SITE_URL; ?>images/course/<?php echo $value['image']; ?>" alt=""></a> </div>

  <?php }}?>
</div><!--owl-carousel-->

</div><!--container-->

</section><!--related_coursediv-->


<!-- <section id="request_callbsec" class="request_callbsec2">
<div class="container">
<ul class="list-inline text-center">
<li class="list-inline-item"><p>We Will Contact You, At a Time which Suits You Best</p></li>
<li class="list-inline-item">
<a href="javascript:void(0);" data-toggle="modal" data-target="#request_modal">
    Request a Call Back
  </a>
  </li>
</ul>
</div><--container--
</section>request_callbsec -->

<section id="call_action" style="margin-bottom:5px;">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-6">
          <p>We Will Contact You, At a Time Which Suits You Best</p>
        </div>
        <div class="col-md-4"><a href="#" data-toggle="modal" data-target="#request_modal"><i class="fas fa-phone-volume"></i> Request a Call Back</a></div>
      </div>
    </div>
  </section>
  <script src="<?php echo SITE_URL; ?>js/owl.carousel.min.js" type="text/javascript"></script>
  <script>
	  $(document).ready(function(){
		$('#course_appcrausal').owlCarousel({
    alert('om shanti');
    loop:true,
    margin:10,
    responsiveClass:true,
	autoplay:true,
	autoplaySpeed:2000,
	autoplayTimeout:2000,
	dots:true,
	nav:false,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:3,
            nav:false,
            loop:true
        }
    }
});
	  })
  </script>







