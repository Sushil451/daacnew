<style type="text/css">
/*  #contacttop_placement{background-size: 100% 100% !important;
    padding: 0px !important;
    height: 300px;
    position: relative;
background:url('http://new.daac.in/daacbanner/Placement--banne.jpg') no-repeat center top; background-size:cover; padding:119px 0px 221px;
#contacttop_placement h4 {
    padding: 10px 0px 10px;
    position: absolute;
    left: 0px;
    right: 0px;
    bottom: 0px;
    background-color: rgba(0,0,0,0.3); 
    margin: 0px;}*/

#contacttop_ba h4{ font-size:27px; color:#fff; text-align:center;}
</style>
<div id="placement_page">
<!--<section id="contacttop_placement">
<img src="http://daac.in/images/Placement_banne.jpg" alt="">
</section>top_banner-->

<!--<section id="carrer_coursdur">
<div class="container">
<div class="position_div">

</div><--inner_maindiv
</div>
</div>
</section>--><!--carrer_coursdur-->

<section id="placement_container">
    <div class="top_banner">
      <div class="container">
      
       <div class="modal" id="myModal">
        <div class="modal-dialog">
         <div class="modal-content">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
         </div>
       </div>
     </div>
      
        <div class="row">
          <div class="col-lg-7">
            <div class="review">
              <h1>Daac Placed</h1>
              <p>10x Students than any other Institute of Rajasthan</p>
              <p><i class="fas fa-quote-left"></i> <span>I feel very happy that I chose DAAC for UX-UI Designing. DAAC trainers are very professional and supportive, Labs are well equipped and Placement record is wonderful.</span></p>
              <div class="student_name"><b>Harmeet Singh</b> - Nobul</div>
            </div>
          </div>
          <div class="col-lg-5">
            <div class="student_video">
              <div class="video_container"> 
            <video id="myVideo" width="100%" height="100%"  controls="controls" autoplay playsinline style="display:block; border-radius:5px;">
            <source src="<?php echo SITE_URL; ?>videos/harmeet_video.mp4" type="video/mp4">
               </video>
               
               
              
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="recents_placements">
      <div class="container">
        <div class="section_heading">
          <h1>Our Recent Placement Stories</h1>
          <p>DAAC is known for its Placements. We have provided 10 Times more Placements than any other Cisco Training Institute of India with 14+ Best Placement Awards, our 90% of business comes from Word of Mouth publicity and Referrals from our old placed students.</p>
        </div>
        <ul class="d-flex flex-wrap justify-content-center">
          <li> <img src="<?php echo SITE_URL; ?>images/rpankit.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Ankit Porwal</h5>
              <p>Future Profilez</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rpdarshna.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Darshna Tiwari</h5>
              <p>Future Profilez</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rpShubham samdani.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Shubham</h5>
              <p>Shubhanvi Busi. Solu.</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rpnisha.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Nisha</h5>
              <p>Future Profilez</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/mahendra.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Mahendra Yadav</h5>
              <p>Webepower</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp18.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Chitra Tambi</h5>
              <p>Elite Info Solution</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp1.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Shubham Joshi</h5>
              <p>Next Big Technology</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp2.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Pooja Sharma</h5>
              <p>Vardhman Infotach</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp3.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Soniya Sharma</h5>
              <p>Vardhman Infotach</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp4.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Prashant Sharma</h5>
              <p>RedSymbol Tech.</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp5.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Sheena Mathur</h5>
              <p>Barefoot Lightning</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp6.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Priyanka</h5>
              <p>Barefoot Lightning</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp7.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Kavita Gupta</h5>
              <p>NYX Ditech</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp8.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Ajay Kumar</h5>
              <p>Future Profilez</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp9.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Prashant</h5>
              <p>SSS Solution</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp10.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Isha Mathur</h5>
              <p>Happy Technoac</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp11.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Malvika Mathur</h5>
              <p>Alcanttor Pvt. Ltd.</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp12.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Rachana</h5>
              <p>Alcanttor Pvt. Ltd.</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp13.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Sunil</h5>
              <p>Efusion Infotech</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp14.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Ankit Shringi</h5>
              <p>Barefoot Lightning</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp15.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Ekta Yadav</h5>
              <p>Happy Technoac</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp16.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Jitendra Shaarma</h5>
              <p>Next Big Technology</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp17.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Ashivna Mathur</h5>
              <p>Pericent BPM</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp19.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Sakshi Goswami</h5>
              <p>Bitace Technology</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp20.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Nirmit</h5>
              <p>Barefoot Lightning</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp21.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Prakesh</h5>
              <p>Blessy Software</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp22.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Prachi Jain</h5>
              <p>HDFC</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp23.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Susheel Sharma</h5>
              <p>Web Master Driver</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp24.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Akash Bhatia</h5>
              <p>Rare Mile Tech.</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp25.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Mayank</h5>
              <p>Konstant</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp26.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Ashish</h5>
              <p>Nexpider</p>
            </div>
          </li>
          <li> <img src="<?php echo SITE_URL; ?>images/rp27.jpg" alt="" class="img-fluid">
            <div class="student_info">
              <h5>Harmeet</h5>
              <p>Doomshell</p>
            </div>
          </li>
        </ul>
        <a href="javascript:void(0)" class="load_more">Load More</a> </div>
    </div>
    <div class="student_speak">
      <div class="container">
        <div class="section_heading">
          <h1>Let our Happily Placed Students Speak for us </h1>
          <p>We are India's only CCIE Training Institute to provide hundreds of Video Testimonials as proof of placements. This page is updated every 15 days with fresh placement records and video testimonials.</p>
        </div>
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <div class="speak_video"> <img src="<?php echo SITE_URL; ?>images/package15lacimg1.jpg" class="img-fluid">
              <div class="student_info"> <a href="javascript:void(0)" class="play_video_btn position-absolute"> <i class="fas fa-play"></i> </a>
                <h5>Bhupender Singh</h5>
                <p>Time Tech</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="speak_video"> <img src="<?php echo SITE_URL; ?>images/package15lacimg.jpg" class="img-fluid">
              <div class="student_info"> <a href="javascript:void(0)" class="play_video_btn position-absolute"> <i class="fas fa-play"></i> </a>
                <h5>Tarun Nagar</h5>
                <p>Dev Technosys</p>
              </div>
            </div>
          </div> 
          <div class="col-md-3 col-sm-6">
            <div class="speak_video"> <img src="<?php echo SITE_URL; ?>images/package15lacimg3.jpg" class="img-fluid">
              <div class="student_info"> <a href="javascript:void(0)" class="play_video_btn position-absolute"> <i class="fas fa-play"></i> </a>
                <h5>Ashok Yadav</h5>
                <p>Co-Founder Webgensis</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="speak_video"> <img src="<?php echo SITE_URL; ?>images/package15lacimg4.jpg" class="img-fluid">
              <div class="student_info"> <a href="javascript:void(0)" class="play_video_btn position-absolute"> <i class="fas fa-play"></i> </a>
                <h5>Arjun Solanki</h5>
                <p>Syon Information</p>
              </div>
            </div>
          </div>
          
        </div>
        <a href="javascript:void(0)" class="load_more">Load More</a> 
        </div>
    </div>
    <div class="package_15_lac">
      <div class="container">
        <div class="section_heading">
          <h1>Meet Students who Crossed 6 Lacs Package Benchmark Within +5 Years</h1>
          <p>Lets meet some shining stars of NB who did CCIE course 5 years ago and are now getting 15 to 25 Lacs packages in India.</p>
        </div>
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <div class="package15lacplus">
              <img src="<?php echo SITE_URL; ?>images/student_video1.jpg" class="img-fluid">
              <div class="package15lacplusdtl">
                <div class="part1">
                  <a href="javascript:void(0)" class="play_video_btn position-absolute"> <i class="fas fa-play"></i> </a>
                  <h5>Ashish Sharna</h5>
                  <p>Nagarro Software</p>
                </div>
                <div class="part2">
                  <img src="<?php echo SITE_URL; ?>images/nagarro-logo.png" class="img-fluid">
                  <p>The best experience I have had so far in DAAC! Every trainer is so friendly and helpful. I recommend...</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="package15lacplus">
              <img src="<?php echo SITE_URL; ?>images/student_video2.jpg" class="img-fluid">
              <div class="package15lacplusdtl">
                <div class="part1">
                  <a href="javascript:void(0)" class="play_video_btn position-absolute"> <i class="fas fa-play"></i> </a>
                  <h5>Arjun Vyas</h5>
                  <p>Appirio</p>
                </div>
                <div class="part2">
                  <img src="<?php echo SITE_URL; ?>images/Appirio-Logo.png" class="img-fluid">
                  <p>The course for php at Doomshell Academy was a very good learning experience. Prior to joining the...</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="package15lacplus">
              <img src="<?php echo SITE_URL; ?>images/student_video3.jpg" class="img-fluid">
              <div class="package15lacplusdtl">
                <div class="part1">
                  <a href="javascript:void(0)" class="play_video_btn position-absolute"> <i class="fas fa-play"></i> </a>
                  <h5>Akash Bhatia</h5>
                  <p>Rare Mile Technologies</p>
                </div>
                <div class="part2">
                  <img src="<?php echo SITE_URL; ?>images/Rare-Mile-Logo.png" class="img-fluid">
                  <p>I came to DAAC Institute to be able to do just basic web designing course. But, i was pleasantly... </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="package15lacplus">
              <img src="<?php echo SITE_URL; ?>images/student_video4.jpg" class="img-fluid">
              <div class="package15lacplusdtl">
                <div class="part1">
                  <a href="javascript:void(0)" class="play_video_btn position-absolute"> <i class="fas fa-play"></i> </a>
                  <h5>Sanjay Sharma</h5>
                  <p>Ernst & Young</p>
                </div>
                <div class="part2">
                  <img src="<?php echo SITE_URL; ?>images/EY-logo.png" class="img-fluid">
                  <p>I am very happy with the services I have received from the DAAC.I have experience on Web Designing...</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <a href="javascript:void(0)" class="load_more">Load More</a>
      </div>
    </div>
    <div class="working_abroad">
      <div class="container">
        <div class="section_heading">
          <h1>Meet Students Working Abroad After IT Training From <span>DAAC</span></h1>
          <p>Lets meet some shining stars of NB who did CCIE course 5 years ago and are now getting 15 to 25 Lacs packages in India.</p>
        </div>
        
        <div class="abroad_container">
          <div class="abroad_student_list">
            <div class="img_part"><img src="<?php echo SITE_URL; ?>images/abroad student.jpg" class="img-fluid"></div>
            <div class="content_part">
              <h2>Harmeet Singh</h2>
              <h4>Front End Developer in <a href="https://nobul.com/" target="_blank" style="color:#19a6db;"><b>Nobul</b></a></h4>
              <h3><b>Country:</b> Toronto, Canada.</h3>
              <p>I feel very happy that I chose DAAC for UX-UI Designing. DAAC trainers are very professional and supportive, Labs are well equipped and Placement record is wonderful.</p>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="abroad_student_list">
            <div class="img_part"><img src="<?php echo SITE_URL; ?>images/abroad student 2.jpg" class="img-fluid"></div>
            <div class="content_part">
              <h2>Piyush Kumar Sonkar</h2>
              <h4>Front End Developer</h4>
              <h3><b>Country:</b> Prague, Czech Republic</h3>
              <p>My name is Piyush, I enrolled myself for Web Designing Training at DAAC. DAAC team constantly motivated me and pushed me to work harder towards my goals.</p>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="abroad_student_list">
            <div class="img_part"><img src="<?php echo SITE_URL; ?>images/abroad student 3.jpg" class="img-fluid"></div>
            <div class="content_part">
              <h2>Pradhuman Singh Udawat</h2>
              <h4>Front End Developer in <a href="https://www.dimensiondata.com/en/locations/canada" target="_blank" style="color:#19a6db;"><b>Dimension Data</b></a></h4>
              <h3><b>Country:</b> Toronto, Canada</h3>
              <p>It was an unforgettable experience. We had the loveliest tutors. There was a friendly atmosphere. Generally, I was extraordinarily lucky to do PHP in DAAC. I highly recommend students to take their guidance to make a mark in industry. I have seen many Institutes, but DAAC is the best!"</p>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <a href="javascript:void(0)" class="load_more">Load More</a>
      </div>
    </div>
    <div class="job_guarantee">
      <div class="container">
        <div class="section_heading">
          <h1>100% Job Guarantee  Courses by <span>Daac</span></h1>
          <p>Courses with 100% Job Placement Guaranteed</p>
        </div>
        
        <div class="job_guarantee_container">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6">
                <div class="guarantee_box">
                <img src="<?php echo SITE_URL; ?>images/web development.png" class="img-fluid guarantee_img">
                  <h4>Web Development </h4>
                  <p>100% Job Placement Guarantee</p>
                  <a href="https://daac.in/web-development-diploma-2.html">Know More <i class="fas fa-angle-double-right"></i></a>
                </div>
              </div>
              <div class="col-md-6">
                <div class="guarantee_box">
                <img src="<?php echo SITE_URL; ?>images/ux_ui design.png" class="img-fluid guarantee_img">
                  <h4>UX-UI Designing</h4>
                  <p>100% Job Placement Guarantee</p>
                  <a href="https://daac.in/web-designing-diploma-2.html">Know More <i class="fas fa-angle-double-right"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </section>

<section id="recruiters">
<div class="container">
<hgroup>
<h2>Top Recruiters</h2>
<p>Our Top Recruiters</p>
<h6>Recruiters</h6>
</hgroup>
<div class="logo_section">
<div class="row">
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:0 0; height:95px; width:175px;"></a> </div>
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:-175px 0; height:95px; width:175px;"></a> </div>
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:-350px 0; height:95px; width:175px;"></a> </div>
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:-525px 0; height:95px; width:175px;"></a> </div>
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:-700px 0; height:95px; width:175px;"></a> </div>
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:-875px 0; height:95px; width:175px;"></a> </div>
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:0px -95px; height:95px; width:175px;"></a> </div>
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:-175px -95px; height:95px; width:175px;"></a> </div>
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:-350px -95px; height:95px; width:175px;"></a> </div>
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:-525px -95px; height:95px; width:175px;"></a> </div>
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:-700px -95px; height:95px; width:175px;"></a> </div>
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:-875px -95px; height:95px; width:175px;"></a> </div>
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:0px -190px; height:95px; width:175px;"></a> </div>
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:-175px -190px; height:95px; width:175px;"></a> </div>
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:-350px -190px; height:95px; width:175px;"></a> </div>
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:-525px -190px; height:95px; width:175px;"></a> </div>
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:-700px -190px; height:95px; width:175px;"></a> </div>
<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6"> <a href="#" style=" background:url(https://daac.in/images/Recruiters-logos.webp) no-repeat; background-position:-875px -190px; height:95px; width:175px;"></a> </div>
</div>
</div>
</div>
</section>

<!-- <section id="request_callbsec" class="request_callbsec2">
<div class="container">
<ul class="list-inline text-center">
<li class="list-inline-item"><p>We Will Contact You, At a Time Which Suits You Best</p></li>
<li class="list-inline-item"><a href="javascript:void(0);" data-toggle="modal" data-target="#request_modal">
    Request a Call Back
  </a></li>
</ul>
</div><--container--
</section>request_callbsec -->

<section id="call_action">
<div class="container">
<div class="row justify-content-center">
<div class="col-md-6">
<p>We Will Contact You, At a Time Which Suits You Best</p>
</div>
<div class="col-md-4"><a href="javascript:void(0);" data-toggle="modal" data-target="#request_modal"><i class="fas fa-phone-volume"></i> Request a Call Back</a></div>
</div>
</div>
</section>

</div>
