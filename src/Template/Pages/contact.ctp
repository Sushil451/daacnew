 <div id="contact_upage">

<section id="carrer_coursdur">
<div class="container">
<div class="position_div">
<div class="inner_maindiv">
<div class="inner_div">
<ul class="list-inline">
<li class="list-inline-item active">Home</li> 
 <li class="list-inline-item">Contact Us</li>
</ul>

</div><!--inner_div-->
</div><!--inner_maindiv-->
</div><!--position_div-->
</div><!--container-->
</section><!--carrer_coursdur-->

<section id="branc_container">
    <div class="container">
      <hgroup>
        <h2>Our Branches</h2>
        <h6>Branches</h6>
      </hgroup>
      <section id="branch_section">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-5">
              <div class="branch_info_container">
                <h2><i class="fas fa-building"></i> Vidhyadhar Nagar</h2>
                <p><i class="fas fa-map-marker-alt"></i> A3 Mall Road Near Radhey Bakers, Vidhyadhar Nagar, Jaipur, 302039</p>
                <ul class="d-flex justify-content-between">
                  <li><i class="fas fa-phone-alt"></i> 9414431944</li>
                  <li><i class="far fa-clock"></i> Mon to Sat 9am to 7pm</li>
                </ul>
                 <a href="javascript:void(0)" class="get_direction"><img src="images/google.svg"> Get GPS Direction</a>
              </div>
             
              <div class="map_container">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3556.474152033363!2d75.7759910144235!3d26.95188018311083!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396db3ba1e504fd3%3A0xc15c9ded5ba79a3b!2sDAAC*21*21%20Vidhyadhar%20Nagar%20I%20Python%20I%20UI%20%26%20UX%20Web-designing%20I%20Advance%20Training!5e0!3m2!1sen!2sin!4v1567664709605!5m2!1sen!2sin" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
              </div>
            </div>
            <div class="col-md-7">
              <div class="branch_images">
                <h4>Outside view from mall street near Radhey Bakers</h4>
                <div class="owl-carousel owl-theme owl-loaded branch-carousel">
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/vb1.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/vb2.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/vb3.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/vb4.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/vb5.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/vb6.jpg" alt="vdn_branch"> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!----VDN Branch----->
      
      <section id="branch_section">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-5">
              <div class="branch_info_container">
                <h2><i class="fas fa-building"></i> Mansarovar</h2>
                <p><i class="fas fa-map-marker-alt"></i> 104 / 75, Second Floor, Near Harishanker Dhaba, Vijay Path, Mansarover, 302020</p>
                <ul class="d-flex justify-content-between">
                  <li><i class="fas fa-phone-alt"></i> 9799322221</li>
                  <li><i class="far fa-clock"></i> Mon to Sat 9am to 7pm</li>
                </ul>
                <a href="javascript:void(0)" class="get_direction"><img src="images/google.svg"> Get GPS Direction</a>
              </div>
              
              <div class="map_container">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3559.759022643191!2d75.7666883145644!3d26.847615869353728!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396db5dc708acf6d%3A0x539c5582951a6645!2sDAAC-Mansarovar%20I%20Python%20I%20PHP%20I%20Web-designing%20Training%20Center!5e0!3m2!1sen!2sin!4v1567666870159!5m2!1sen!2sin" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
              </div>
            </div>
            <div class="col-md-7">
              <div class="branch_images">
                <h4>Outside view from Vijay Path, Sector 122</h4>
                <div class="owl-carousel owl-theme owl-loaded branch-carousel">
                 
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/mb1.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/mb2.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/mb3.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/mb4.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/mb5.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/mb6.jpg" alt="vdn_branch"> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!----Mansarover Branch----->
      
      <section id="branch_section">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-5">
              <div class="branch_info_container">
                <h2><i class="fas fa-building"></i> C-Scheme</h2>
                <p><i class="fas fa-map-marker-alt"></i> 304b 3rd Floor Shyam Anukampa, opposite Yes Bank, C-Scheme, Jaipur, 302001</p>
                <ul class="d-flex justify-content-between">
                  <li><i class="fas fa-phone-alt"></i> 9309332221</li>
                  <li><i class="far fa-clock"></i> Mon to Sat 9am to 7pm</li>
                </ul>
                <a href="javascript:void(0)" class="get_direction"><img src="images/google.svg"> Get GPS Direction</a>
              </div>
              
              <div class="map_container">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3557.636649123253!2d75.80228371456579!3d26.915024366471062!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396db424363157c7%3A0xe881df8bf7443df!2sDAAC%7C%7C%20C-scheme%7C%7C%20Advance%20Certification%20Course%20Center!5e0!3m2!1sen!2sin!4v1567667389394!5m2!1sen!2sin" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
              </div>
            </div>
            <div class="col-md-7">
              <div class="branch_images">
                <h4>Outside view from Shyam Anukampa</h4>
                <div class="owl-carousel owl-theme owl-loaded branch-carousel">
                  
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/cb1.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/cb2.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/cb3.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/cb4.jpg" alt="vdn_branch"> </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!----C-Scheme Branch----->
      
      <section id="branch_section">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-5">
              <div class="branch_info_container">
                <h2><i class="fas fa-building"></i> Pratap Nagar</h2>
                <p><i class="fas fa-map-marker-alt"></i> 1st Floor Khushi, Tower Opposite HDFC Bank, Kumbha Marg, Pratap Nagar, 302033</p>
                <ul class="d-flex justify-content-between">
                  <li><i class="fas fa-phone-alt"></i> 6376052529</li>
                  <li><i class="far fa-clock"></i>  Mon to Sat 9am to 7pm</li>
                </ul>
                <a href="javascript:void(0)" class="get_direction"><img src="images/google.svg"> Get GPS Direction</a>
              </div>
              
              <div class="map_container">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3561.250878215295!2d75.81679511456336!3d26.80013927138009!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396dc991fe0fac33%3A0xd623c823756fb975!2sDAAC!5e0!3m2!1sen!2sin!4v1567667466629!5m2!1sen!2sin" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
              </div>
            </div>
            <div class="col-md-7">
              <div class="branch_images">
                <h4>Outside view from Kumbha Marg</h4>
                <div class="owl-carousel owl-theme owl-loaded branch-carousel">
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/pb1.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/pb2.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/pb3.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/pb4.jpg" alt="vdn_branch"> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!----Pratap-nagar Branch----->
      
      <section id="branch_section">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-5">
              <div class="branch_info_container">
                <h2><i class="fas fa-building"></i> Pune</h2>
                <p><i class="fas fa-map-marker-alt"></i> M-405, 3rd floor, Mega Center, Near Noble Hospital Magarpatta Pune, 411028</p>
                <ul class="d-flex justify-content-between">
                  <li><i class="fas fa-phone-alt"></i> 8856030270</li>
                  <li><i class="far fa-clock"></i> Mon to Sat 9am to 7pm</li>
                </ul>
                <a href="javascript:void(0)" class="get_direction"><img src="images/google.svg"> Get GPS Direction</a>
              </div>
              
              <div class="map_container">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3783.5507968997276!2d73.92368981441656!3d18.50399567451145!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2c1ea7e3d2551%3A0x64d9066398b9630a!2sDAAC%20Magarpatta%20Hadapsar%20%7C%20Blockchain%20%7C%20Hyperledger%20fabric%20%7C%20GO%20Lang%20%7C%20Training%20%7C%20Selenium%20l%20Test%20Automation!5e0!3m2!1sen!2sin!4v1567667596735!5m2!1sen!2sin" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
              </div>
            </div>
            <div class="col-md-7">
              <div class="branch_images">
                <h4>Outside view from Magarpatta</h4>
                <div class="owl-carousel owl-theme owl-loaded branch-carousel">
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/p_b1.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/p_b2.jpg" alt="vdn_branch"> </div>
                      <div class="owl-item"> <img src="<?php echo SITE_URL; ?>images/p_b3.jpg" alt="vdn_branch"> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!----Pune Branch-----> 
    </div>
  </section>

<section id="call_action">
<div class="container">
<div class="row justify-content-center">
<div class="col-md-6">
<p>We Will Contact You, At a Time Which Suits You Best</p>
</div>
<div class="col-md-4"><a href="javascript:void(0);" data-toggle="modal" data-target="#request_modal"><i class="fas fa-phone-volume"></i> Request a Call Back</a></div>
</div>
</div>
</section>
</div>
