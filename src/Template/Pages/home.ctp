<?php if($_SESSION['mysession']!=1){ ?>
<script>
$(document).ready(function() {
    $("#myModal4").modal('show');
});
</script>
<?php } ?>
<script>
$(function() {
    //$( "#dialog" ).dialog();
});
</script>

<section id="slider">
    <div class="owl-carousel owl-theme owl-loaded" id="slider_crausal">
        <?php  foreach($slide as $value){ ?>
        <div class="owl-item">
            <img src="<?php echo SITE_URL; ?>images/slider/<?php echo $value['images'];?>"
                alt="DAAC-all_cources_banners">
        </div>
        <?php } ?>
    </div>
    <div class="slide_content">
        <ul class="list-inline text-center">
            <li class="list-inline-item"><a href="javascript:void(0);">Get Started. It's Free.</a></li>
        </ul>
    </div>
</section>
<!--slider-->
<section id="grow_careers">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="option text-center">
                    <div class="option_iocn position-relative">
                        <img src="<?php echo SITE_URL; ?>images/experts.webp" alt="DAAC-Learn-From-The-Experts"
                            class="img-fluid">
                        <!-- <img src="<?php //echo SITE_URL; ?>images/expert_icon_1.png" alt="DAAC-Learn_From_The_Experts"
                            class="img-fluid option_icon_1">
                        <img src="<?php //echo SITE_URL; ?>images/expert_icon_3.webp" alt="DAAC-Learn_From_The_Experts"
                            class="img-fluid option_icon_3 hvr-hang position-absolute">
                        <img src="<?php //echo SITE_URL; ?>images/expert_icon_2.png" alt="DAAC-Learn_From_The_Experts"
                            class="img-fluid option_icon_2 position-absolute"> -->
                    </div>
                    <h3 class="option_heading">Learn From The Experts</h3>
                    <p>Industry thought-leaders to help
                        you master new skills
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="option_iocn option text-center">
                    <div class="position-relative">
                    <img src="<?php echo SITE_URL; ?>images/growth.webp" alt="DAAC-Guaranteed_Career_Growth"
                            class="img-fluid">
                        <!-- <img src="<?php echo SITE_URL; ?>images/career_icon_1.webp" alt="DAAC-Guaranteed_Career_Growth"
                            class="img-fluid option_icon_1">
                        <img src="<?php echo SITE_URL; ?>images/daac-newnd_06.png" alt="DAAC-Guaranteed_Career_Growth"
                            class="img-fluid option_icon_3 hvr-hang position-absolute">
                        <img src="<?php echo SITE_URL; ?>images/career_icon_2.webp" alt="DAAC-Guaranteed_Career_Growth"
                            class="img-fluid option_icon_2 position-absolute"> -->
                    </div>
                    <h3 class="option_heading">Guaranteed Career Growth</h3>
                    <p>Get a pay-raise, a promotion, or start a new career</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="option text-center">
                    <div class="option_iocn position-relative">
                    <img src="<?php echo SITE_URL; ?>images/curriculum.webp" alt="DAAC-Accredited_Curriculum"
                            class="img-fluid">
                        <!-- <img src="<?php echo SITE_URL; ?>images/curriculum_icon_1.webp" alt="DAAC-Accredited_Curriculum"
                            class="img-fluid option_icon_1">
                        <img src="<?php echo SITE_URL; ?>images/curriculum_icon_3.webp" alt="DAAC-Accredited_Curriculum"
                            class="img-fluid option_icon_3 hvr-hang position-absolute">
                        <img src="<?php echo SITE_URL; ?>images/curriculum_icon_2.webp" alt="DAAC-Accredited_Curriculum"
                            class="img-fluid option_icon_2 position-absolute"> -->
                    </div>
                    <h3 class="option_heading">Accredited Curriculum</h3>
                    <p>Approved by 40+ global accrediting bodies </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="count_loader_sec">
    <div class="container">
        <div id="count_loader">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div id="shiva">
                        <span class="count"><?php echo $stu;?></span><i class="fa fa-plus" aria-hidden="true"></i>
                        <p>Number of Students</p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div id="shiva">
                        <span class="count"><?php //echo $Use['User']['company'];?>510</span><i class="fa fa-plus"
                            aria-hidden="true"></i>
                        <p>Number of Companies</p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div id="shiva">
                        <span class="count"><?php echo $this->element('visits'); ?></span><i class="fa fa-plus"
                            aria-hidden="true"></i>
                        <p>Number of Site Visits</p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div id="shiva">
                        <span class="count">10000</span><i class="fa fa-plus" aria-hidden="true"></i>
                        <p>Facebook Likes</p>
                    </div>
                </div>
            </div>
            <!--row-->
        </div>
    </div>
</section>
<section id="courses" class="course_section popular_csec">
    <div class="course_bg" style="transform: translate(-14.1px, -10px) scale(1.1);"></div>
    <div class="course_contant">
        <div class="container">
            <hgroup>
                <h1 class="headingFirst">Popular Courses</h1>
                <p>Are You Looking For</p>
                <h6>Courses</h6>
            </hgroup>
            <div class="course_tab">
                <div class="row justify-content-center">
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="course_box">
                            <div class="course_cnt">
                                <div class="course_icon"><i class="fas fa-pencil-ruler"></i></div>
                                <h4>UI & UX designing</h4>
                                <p>Duration :
                                    <!-- <strong><?php //echo $course1['1']['Course']['duration'];?> -->
                                    <strong>10 Months</strong>
                                </p>
                                <a href="<?php echo SITE_URL; ?>web-designing-course-in-jaipur.html">Course Detail
                                    <i class="fas fa-angle-double-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="course_box graphic_color">
                            <div class="course_cnt">
                                <div class="course_icon"><i class="fas fa-code"></i></div>
                                <h4>Web development</h4>
                                <p>Duration : <strong><?php echo $course1['2']['Course']['duration'];?></strong></p>
                                <a href="<?php echo SITE_URL; ?>web-development-course-in-jaipur.html">Course Detail <i
                                        class="fas fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </dIV>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="course_box sketching_color">
                            <div class="course_cnt">
                                <div class="course_icon"><i class="fab fa-react"></i></div>
                                <h4>Mobile development react</h4>
                                <p>
                                    Duration :
                                    <strong>
                                        <!-- <?php //echo $course1['4']['Course']['duration'];?> --> 9 Months
                                    </strong>
                                </p>
                                <a href="<?php echo SITE_URL; ?>advance-react-native-training-in-jaipur.html">Course
                                    Detail <i class="fas fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="course_box photoshop_color">
                            <div class="course_cnt">
                                <div class="course_icon">
                                    <img src="<?php echo SITE_URL; ?>images/p-n.png"
                                        alt="full-stack-web-developer-training">
                                </div>
                                <h4>Full Stack Development</h4>
                                <p>Duration : <strong>10 Months</strong></p>
                                <a href="<?php echo SITE_URL; ?>full-stack-web-developer-training.html">Course Detail <i
                                        class="fas fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="course_box bootstrap_color">
                            <div class="course_cnt">
                                <div class="course_icon">
                                    <img src="<?php echo SITE_URL; ?>images/course_img2.png" alt="php-training-jaipur">
                                </div>
                                <h4>CakePHP</h4>
                                <p>Duration : <strong>3 Months</strong></p>
                                <a href="<?php echo SITE_URL; ?>php-training-jaipur.html">Course Detail
                                    <i class="fas fa-angle-double-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="course_box dreamweaver_color">
                            <div class="course_cnt">
                                <div class="course_icon">
                                    <img src="<?php echo SITE_URL; ?>images/tally_ico.png"
                                        alt="tally-institute-in-jaipur">
                                </div>
                                <h4>Tally</h4>
                                <p>Duration : <strong>3 Months</strong></p>
                                <a href="<?php echo SITE_URL; ?>tally-institute-in-jaipur.html">Course Detail <i
                                        class="fas fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6 ">
                        <div class="course_box html_color">
                            <div class="course_cnt">
                                <div class="course_icon"><i class="fas fa-search"></i></div>
                                <h4>Digital Marketing </h4>
                                <p>Duration : <strong>2 Months</strong></p>
                                <a href="<?php echo SITE_URL; ?>digital-marketing-course-in-jaipur.html">Course Detail
                                    <i class="fas fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="course_box css_js_color">
                            <div class="course_cnt">
                                <div class="course_icon"><i class="fas fa-file-excel"></i></div>
                                <h4>Advance Excel</h4>
                                <p>Duration : <strong>3 Months</strong></p>
                                <a href="<?php echo SITE_URL; ?>advance-excel-training.html">Course Detail
                                    <i class="fas fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</section>
<section id="courses" class="course_section trnd_course">
    <div class="course_contant">
        <div class="container">
            <hgroup>
                <h2>Trending Courses</h2>
                <p>Are You Looking For</p>
                <h6>Courses</h6>
            </hgroup>
            <div class="course_tab">
                <div class="row justify-content-center">
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="course_box graphic_color">
                            <div class="course_cnt">
                                <div class="course_icon"><img src="<?php echo SITE_URL; ?>images/ds2.png"
                                        alt="data-science-course-training"></div>
                                <h4>Data Science</h4>
                                <p>Duration : <strong>4 Months</strong></p>
                                <a href="<?php echo SITE_URL; ?>data-science-course-training.html">Course Detail <i
                                        class="fas fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </dIV>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="course_box html_color">
                            <div class="course_cnt">
                                <div class="course_icon"><img src="<?php echo SITE_URL; ?>images/tren_courseico_6.png"
                                        alt="salesforce-training-in-Jaipur">
                                </div>
                                <h4>Salesforce</h4>
                                <p>Duration : <strong>3 Months</strong></p>
                                <a href="<?php echo SITE_URL; ?>salesforce-training-in-Jaipur.html">Course Detail <i
                                        class="fas fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="course_box css_js_color">
                            <div class="course_cnt">
                                <div class="course_icon"><img src="<?php echo SITE_URL; ?>images/tren_courseico_7.png"
                                        alt="block-chain-training-in-jaipur">
                                </div>
                                <h4>Block Chain</h4>
                                <p>Duration : <strong>2 Months</strong></p>
                                <a href="<?php echo SITE_URL; ?>block-chain-training-in-jaipur.html">Course Detail <i
                                        class="fas fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="course_box bootstrap_color">
                            <div class="course_cnt">
                                <div class="course_icon"><img src="<?php echo SITE_URL; ?>images/python_nw.png"
                                        alt="python-training-in-jaipur"></div>
                                <h4>Python Django</h4>
                                <p>Duration : <strong>2 Months</strong></p>
                                <a href="<?php echo SITE_URL; ?>python-training-in-jaipur.html">Course Detail <i
                                        class="fas fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="course_box">
                            <div class="course_cnt">
                                <div class="course_icon"><img src="<?php echo SITE_URL; ?>images/autocad_nw.png"
                                        alt="autocad-training-in-jaipur"></div>
                                <h4>AutoCAD</h4>
                                <p>Duration : <strong>3 Months</strong></p>
                                <a href="<?php echo SITE_URL; ?>autocad-training-in-jaipur.html">Course Detail <i
                                        class="fas fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="course_box sketching_color">
                            <div class="course_cnt">
                                <div class="course_icon"><img src="<?php echo SITE_URL; ?>images/tren_courseico_3.png"
                                        alt="AWS-training-institutes-in-jaipur">
                                </div>
                                <h4>Amazon AWS</h4>
                                <p>Duration : <strong><?php echo $course1['4']['Course']['duration'];?></strong></p>
                                <a href="<?php echo SITE_URL; ?>AWS-training-institutes-in-jaipur.html">Course Detail <i
                                        class="fas fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="course_box photoshop_color">
                            <div class="course_cnt">
                                <div class="course_icon"><img src="<?php echo SITE_URL; ?>images/tren_courseico_4.png"
                                        alt="big-data-training-in-Jaipur">
                                </div>
                                <h4>BigData</h4>
                                <p>Duration : <strong>3 Months</strong></p>
                                <a href="<?php echo SITE_URL; ?>big-data-training-in-Jaipur.html">Course Detail <i
                                        class="fas fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="course_box dreamweaver_color">
                            <div class="course_cnt">
                                <div class="course_icon"><img src="<?php echo SITE_URL; ?>images/oracle_nw.png"
                                        alt="oracle-training-in-jaipur"></div>
                                <h4>Oracle Apps</h4>
                                <p>Duration : <strong>2 Months</strong></p>
                                <a href="<?php echo SITE_URL; ?>oracle-training-in-jaipur.html">Course Detail <i
                                        class="fas fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</section>
<section id="call_action">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <p>We Will Contact You, At a Time Which Suits You Best</p>
            </div>
            <div class="col-md-4"><a href="javascript:void(0);" data-toggle="modal" data-target="#request_modal">
                    <i class="fas fa-phone-volume"></i> Request a Call Back</a>
            </div>
        </div>
    </div>
</section>
<section id="degree_cors_sec">
    <hgroup>
        <h2>Degree Courses</h2>
        <p>Are You Looking For</p>
        <h6>Courses</h6>
    </hgroup>
    </hgroup>
    <div class="container">
        <div class="row ">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                <div class="degree_innerdiv degree_first">
                    <img src="<?php echo SITE_URL; ?>images/degree_img1.png" alt="bsc-animation-course">
                    <h4> <a href="https://daac.in/bsc-animation-course.html">BSC / MSC Animation</a></h4>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                <div class="degree_innerdiv degree_second">
                    <img src="<?php echo SITE_URL; ?>images/degree_img2.png" alt="BCA-course-in-jaipur">
                    <h4><a href="https://daac.in/BCA-course-in-jaipur.html">BCA / MCA</a> </h4>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                <div class="degree_innerdiv degree_third">
                    <img src="<?php echo SITE_URL; ?>images/degree_img3.png" alt="BBA-course-in-jaipur">
                    <h4><a href="https://daac.in/BBA-course-in-jaipur.html">BBA / MBA</a> </h4>
                </div>
            </div>
        </div>
        <!--row-->
    </div>
</section>
<section id="recruiters">
    <div class="container">
        <hgroup>
            <h2>Top Recruiters</h2>
            <p>Our Top Recruiters</p>
            <h6>Recruiters</h6>
        </hgroup>
        <div class="logo_section">
            <div class="owl-carousel owl-theme owl-loaded" id="recruter_crausal">
                <div class="owl-item"><span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:0 0; height:95px; width:175px; display:block;"></span>
                </div>
                <div class="owl-item"> <span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:-175px 0; height:95px; width:175px; display:block;"></span>
                </div>
                <div class="owl-item"> <span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:-350px 0; height:95px; width:175px; display:block;"></span>
                </div>
                <div class="owl-item"> <span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:-525px 0; height:95px; width:175px; display:block;"></span>
                </div>
                <div class="owl-item"> <span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:-700px 0; height:95px; width:175px; display:block;"></span>
                </div>
                <div class="owl-item"> <span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:-875px 0; height:95px; width:175px; display:block;"></span>
                </div>
                <div class="owl-item"> <span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:0px -95px; height:95px; width:175px; display:block;"></span>
                </div>
                <div class="owl-item"> <span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:-175px -95px; height:95px; width:175px; display:block;"></span>
                </div>
                <div class="owl-item"> <span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:-350px -95px; height:95px; width:175px; display:block;"></span>
                </div>
                <div class="owl-item"> <span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:-525px -95px; height:95px; width:175px; display:block;"></span>
                </div>
                <div class="owl-item"> <span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:-700px -95px; height:95px; width:175px; display:block;"></span>
                </div>
                <div class="owl-item"> <span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:-875px -95px; height:95px; width:175px; display:block;"></span>
                </div>
                <div class="owl-item"> <span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:0px -190px; height:95px; width:175px; display:block;"></span>
                </div>
                <div class="owl-item"> <span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:-175px -190px; height:95px; width:175px; display:block;"></span>
                </div>
                <div class="owl-item"> <span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:-350px -190px; height:95px; width:175px; display:block;"></span>
                </div>
                <div class="owl-item"> <span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:-525px -190px; height:95px; width:175px; display:block;"></span>
                </div>
                <div class="owl-item"> <span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:-700px -190px; height:95px; width:175px; display:block;"></span>
                </div>
                <div class="owl-item"> <span
                        style=" background:url(<?php echo SITE_URL; ?>images/Recruiters-logos.webp) no-repeat; background-position:-875px -190px; height:95px; width:175px; display:block;"></span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Student Blog Work-->

<section id="testimonials">
    <div class="container">
        <hgroup>
            <h2>Alumni Speaks</h2>
            <p>Student's Testimonials</p>
            <h6>Alumni Speaks</h6>
        </hgroup>
        <div class="owl-carousel owl-theme owl-loaded" id="speak_crausal">
            <?php foreach($student_details as $key=>$value){ ?>
            <div class="owl-item">
                <p><?php echo $value['des'];?> </p>
                <strong><?php echo $value['name']; ?></strong>
                <?php $catt=$this->Comman->testi($value['enroll']); ?>
                <div class="text-center"> <?php 
               if($catt['image']!=''){
                ?> <img src="<?php echo SITE_URL ?>images/resume/<?php echo  $catt['image']; ?>" width="70" height="71"
                        alt="DAAC-students_testimonials" alt="resume" />
                    <?php  }  ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <!--container-->
</section>
<!--speaker_sec-->
<section id="student_work">
    <div class="container">
        <hgroup>
            <h2>Student's Work Gallery</h2>
            <p>Student's Work Gallery</p>
            <h6>Work Gallery</h6>
        </hgroup>
        <ul class="nav nav-pills mb-3 justify-content-center" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a href="javascript:void(0)" data-cate="1" onclick=" return false;" class="nav-link active"
                    id="pills-home-tab" data-toggle="pill" role="tab" aria-controls="pills-home" aria-expanded="true">
                    Designing</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="javascript:void(0)" role="tab"
                    aria-controls="pills-profile" aria-expanded="true">Programing</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-mobile_app-tab" data-toggle="pill" href="javascript:void(0)" role="tab"
                    aria-controls="pills-mobile_app" aria-expanded="true">Mobile App </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-other_tab-tab" data-toggle="pill" href="javascript:void(0)" role="tab"
                    aria-controls="pills-other_tab" aria-expanded="true">Other</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                <div class="owl-carousel owl-theme owl-loaded" id="designing_crausal">
                    <?php    foreach($destination as $key=>$value) {
                  $filename="images/portfolio/image/".$value['image'];
                  $a=getimagesize(SITE_URL.$filename);
                  if (is_array($a)) { ?>
                    <div class="owl-item">
                        <div class="img-thumbnail">
                            <div class="himg_div">
                                <a class="example-image-link" href="<?php echo SITE_URL; ?>images/portfolio/image/<?php echo $value['image']; ?>" data-lightbox="example-1">
                                    <img class="example-image" src="<?php echo SITE_URL; ?>images/portfolio/image/<?php echo $value['image']; ?>" alt="DAAC-student_designing_work_gallery" />
                                </a>
                            </div>
                            <div class="content">
                                <span><?php echo $value['user']['name']; ?></span>
                                <span><?php echo $value['user']['username']; ?></span>
                            </div>
                            <!--content-->
                        </div>
                        <!--img-thumbnail-->
                    </div>
                    <?php } } ?>
                </div>
            </div>

            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                <div class="owl-carousel owl-theme owl-loaded" id="programing_crausal">
                    <?php    foreach($destination as $key=>$value) {
                        $filename="images/portfolio/image/".$value['image'];
                        $a=getimagesize(SITE_URL.$filename);
                        if (is_array($a)) { ?>
                    <div class="owl-item">
                        <div class="img-thumbnail">
                            <div class="himg_div">
                                <a class="example-image-link" href="<?php echo SITE_URL; ?>images/portfolio/image/<?php echo $value['image']; ?>" data-lightbox="example-1"><img class="example-image" src="<?php echo SITE_URL; ?>images/portfolio/image/<?php echo $value['image']; ?>" alt="DAAC-student_programing_work_gallery" /> </a>
                            </div>
                            <div class="content">
                                <span><?php echo $value['user']['name']; ?></span>
                                <span><?php echo $value['user']['username']; ?></span>
                            </div>
                            <!--content-->
                        </div>
                        <!--img-thumbnail-->
                    </div>
                    <?php } } ?>
                </div>
            </div>

            <div class="tab-pane fade" id="mobile_app" role="tabpanel" aria-labelledby="pills-mobile_app-tab">
                <div class="owl-carousel owl-theme owl-loaded" id="mapp_crausal">
                    <?php    foreach($mobileapp as $key=>$value) {
                  $filename="images/portfolio/image/".$value['image'];
                  $a=getimagesize(SITE_URL.$filename);
                  if (is_array($a)) { ?>
                    <div class="owl-item">
                        <div class="img-thumbnail">
                            <div class="himg_div"><a class="example-image-link"
                                    href="<?php echo SITE_URL; ?>images/portfolio/image/<?php echo $value['image']; ?>"
                                    data-lightbox="example-1"><img class="example-image"
                                        src="<?php echo SITE_URL; ?>images/portfolio/image/<?php echo $value['image']; ?>"
                                        alt="DAAC-student_mobile_app_work_gallery" /></a></div>
                            <div class="content">
                                <span><?php echo $value['name']; ?></span>
                                <span><?php echo $value['username']; ?></span>
                            </div>
                            <!--content-->
                        </div>
                        <!--img-thumbnail-->
                    </div>
                    <?php }} ?>
                </div>
            </div>

            <div class="tab-pane fade" id="other_tab" role="tabpanel" aria-labelledby="pills-other_tab-tab">
                <div class="owl-carousel owl-theme owl-loaded" id="otherc_crausal">
                    <div class="owl-item">
                        <div class="img-thumbnail">
                            <div class="himg_div"><a class="example-image-link"
                                    href="<?php echo SITE_URL;  ?>images/work_img1.jpg" data-lightbox="example-1"><img
                                        class="example-image" src="<?php echo SITE_URL;  ?>images/work_img1.jpg"
                                        alt="DAAC-student_other_work_gallery" /></a></div>
                            <div class="content">
                                <span>Rajesh Jain</span>
                                <span>Daac - 0031</span>
                            </div>
                            <!--content-->
                        </div>
                        <!--img-thumbnail-->
                    </div>
                    <div class="owl-item">
                        <div class="img-thumbnail">
                            <div class="himg_div"><a class="example-image-link"
                                    href="<?php echo SITE_URL;  ?>images/work_img2.jpg" data-lightbox="example-1"><img
                                        class="example-image" src="<?php echo SITE_URL;  ?>images/work_img2.jpg"
                                        alt="DAAC-student_other_work_gallery" /></a></div>
                            <div class="content">
                                <span>Rajesh Jain</span>
                                <span>Daac - 0031</span>
                            </div>
                            <!--content-->
                        </div>
                        <!--img-thumbnail-->
                    </div>
                    <div class="owl-item">
                        <div class="img-thumbnail">
                            <div class="himg_div"><a class="example-image-link"
                                    href="<?php echo SITE_URL;  ?>images/work_img3.jpg" data-lightbox="example-1"><img
                                        class="example-image" src="<?php echo SITE_URL;  ?>images/work_img3.jpg"
                                        alt="DAAC-student_other_work_gallery" /></a></div>
                            <div class="content">
                                <span>Rajesh Jain</span>
                                <span>Daac - 0031</span>
                            </div>
                            <!--content-->
                        </div>
                        <!--img-thumbnail-->
                    </div>
                    <div class="owl-item">
                        <div class="img-thumbnail">
                            <div class="himg_div"><a class="example-image-link"
                                    href="<?php echo SITE_URL;  ?>images/work_img2.jpg" data-lightbox="example-1"><img
                                        class="example-image" src="<?php echo SITE_URL;  ?>images/work_img2.jpg"
                                        alt="DAAC-student_other_work_gallery" /></a></div>
                            <div class="content">
                                <span>Rajesh Jain</span>
                                <span>Daac - 0031</span>
                            </div>
                            <!--content-->
                        </div>
                        <!--img-thumbnail-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--container-->
</section>
<!--student_work_sec-->

<script>
$(document).ready(function() {
    $("#student_work_sec .nav-pills .nav-link").click(function() {
        $("#student_work_sec .nav-pills .nav-link.active").removeClass("active");
        $(this).addClass("active");
    });
});
</script>
<script>
$(document).ready(function() {
    $('#myForm').on('submit', function() {
        var email = $('#subs_email').val();
        $.ajax({
            async: true,
            data: {
                'subs_email': email
            },
            dataType: "html",
            type: "POST",
            url: "<? echo SITE_URL; ?>Home/index",
            success: function(data) {

                $("#support").html(data);
            }

        });
        return false;
    });

});
function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
}
</script>