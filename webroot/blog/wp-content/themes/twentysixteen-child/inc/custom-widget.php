<?php
//========================================Site Links================================//


// Creating the contact widget
class wpb_widgetadd extends WP_Widget {

function __construct() {
parent::__construct(
// Base ID of your widget
'wpb_widgetadd', 

// Widget name will appear in UI
__('Other Links ', 'wpb_widget_domain'), 

// Widget description
array( 'description' => __( ' widget based on Follow on other site', 'wpb_widget_domain' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {

$title = apply_filters( 'widget_title', $instance['title'] );
$title1 = apply_filters( 'widget_title', $instance['title1'] );
$title2 = apply_filters( 'widget_title', $instance['title2'] );
$title3 = apply_filters( 'widget_title', $instance['title3'] );
$title4 = apply_filters( 'widget_title', $instance['title4'] );
$title5 = apply_filters( 'widget_title', $instance['title5'] );
$title6 = apply_filters( 'widget_title', $instance['title6'] );

// before and after widget arguments are defined by themes
echo $args['before_widget'];


/*This is where you run the code and display the output*/

		
?>
<?php echo $title; ?>
<?php if($title1!==''){ ?>
<a href="<?php echo $title1; ?>" target="_blank" class="fa fa-facebook"></a>

<?php }if($title2!==''){ ?>
<a href="<?php echo $title2; ?>" target="_blank" class="fa fa-google-plus"></a>
<?php }if($title3!==''){ ?>
<a href="<?php echo $title3; ?>" target="_blank" class="fa fa-youtube"></a>

<?php }if($title4!==''){ ?>
<a href="<?php echo $title4; ?>" target="_blank" class="fa fa-twitter"></a>


<?php }if($title5!==''){ ?>
<a href="<?php echo $title5; ?>" target="_blank" class="fa fa-linkedin"></a>

<?php }if($title6!==''){ ?>
<a href="<?php echo $title6; ?>" target="_blank" class="fa fa-instagram"></a>
<?php } ?>

</aside>
<?php }
		
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
$title1 = $instance[ 'title1' ];
$title2 = $instance[ 'title2' ];
$title3 = $instance[ 'title3' ];
$title4 = $instance[ 'title4' ];
$title5 = $instance[ 'title5' ];
$title6 = $instance[ 'title6' ];

}
else {
$title = __( '', 'wpb_widget_domain' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Facebook' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title1' ); ?>" type="text" value="<?php echo esc_attr( $title1 ); ?>" />
</p>

<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Google+' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title2' ); ?>" type="text" value="<?php echo esc_attr( $title2 ); ?>" />
</p>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Youtube' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title3' ); ?>" type="text" value="<?php echo esc_attr( $title3 ); ?>" />
</p>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Twitter' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title4' ); ?>" type="text" value="<?php echo esc_attr( $title4 ); ?>" />
</p>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Linkedin' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title5' ); ?>" type="text" value="<?php echo esc_attr( $title5 ); ?>" />
</p>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'instagram' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title6' ); ?>" type="text" value="<?php echo esc_attr( $title6 ); ?>" />
</p>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
$instance['title1'] = ( ! empty( $new_instance['title1'] ) ) ? strip_tags( $new_instance['title1'] ) : '';
$instance['title2'] = ( ! empty( $new_instance['title2'] ) ) ? strip_tags( $new_instance['title2'] ) : '';
$instance['title3'] = ( ! empty( $new_instance['title3'] ) ) ? strip_tags( $new_instance['title3'] ) : '';
$instance['title4'] = ( ! empty( $new_instance['title4'] ) ) ? strip_tags( $new_instance['title4'] ) : '';
$instance['title5'] = ( ! empty( $new_instance['title5'] ) ) ? strip_tags( $new_instance['title5'] ) : '';
$instance['title6'] = ( ! empty( $new_instance['title6'] ) ) ? strip_tags( $new_instance['title6'] ) : '';

return $instance;
}

} // Class wpb_widget ends here

// Register and load the widget
function wpb_load_widgetadd() {
	register_widget( 'wpb_widgetadd' );
}
add_action( 'widgets_init', 'wpb_load_widgetadd' );
