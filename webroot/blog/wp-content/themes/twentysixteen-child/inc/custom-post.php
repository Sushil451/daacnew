<?php
/*------------------------------------------start-astrology-service------------------------------------------*/
function Astrology_funct() {
  $astrologylab = array(
    'name' => 'Quotes',
    'singular_name' => 'Quotes',
    'add_new' => 'Add Quotes',
    'add_new_item' => 'Add New Quotes',
    'edit_item' => 'Edit Quotes',
    'new_item' => 'New Quotes',
    'all_items' => 'All Quotes',
    'view_item' => 'View Quotes',
    'search_items' => 'Search Quotes',
    'not_found' =>  'No Images found',
    'not_found_in_trash' => 'No News found in Trash', 
    'parent_item_colon' => '',
    'menu_name' => 'Quotes'
  );

  $astrology= array(
    'labels' => $astrologylab,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array( 'slug' => 'quotes' ),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'menu_position' => 10,
    'supports' => array( 'title', 'thumbnail', 'editor')
  ); 
  
  register_post_type( 'quotes', $astrology);
}
add_action( 'init', 'Astrology_funct' );


function services_funct() {
  $serviceslab = array(
    'name' => 'Updates',
    'singular_name' => 'Updates',
    'add_new' => 'Add Updates',
    'add_new_item' => 'Add New Updates',
    'edit_item' => 'Edit Updates',
    'new_item' => 'New Updates',
    'all_items' => 'All Updates',
    'view_item' => 'View Updates',
    'search_items' => 'Search Updates',
    'not_found' =>  'No Images found',
    'not_found_in_trash' => 'No News found in Trash', 
    'parent_item_colon' => '',
    'menu_name' => 'Updates'
  );

  $serservice= array(
    'labels' =>    $serviceslab,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array( 'slug' => 'updates' ),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'menu_position' => 10,
    'supports' => array( 'title','tags')
  ); 
  
  register_post_type( 'updates',    $serservice);
}
add_action( 'init', 'services_funct' );


add_action( 'init', 'create_tag_taxonomies', 0 );

//create two taxonomies, genres and tags for the post type "tag"
function create_tag_taxonomies() 
{
  // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    'name' => _x( 'Tags', 'taxonomy general name' ),
    'singular_name' => _x( 'Tag', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Tags' ),
    'popular_items' => __( 'Popular Tags' ),
    'all_items' => __( 'All Tags' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Tag' ), 
    'update_item' => __( 'Update Tag' ),
    'add_new_item' => __( 'Add New Tag' ),
    'new_item_name' => __( 'New Tag Name' ),
    'separate_items_with_commas' => __( 'Separate tags with commas' ),
    'add_or_remove_items' => __( 'Add or remove tags' ),
    'choose_from_most_used' => __( 'Choose from the most used tags' ),
    'menu_name' => __( 'Tags' ),
  ); 

  register_taxonomy('tags',array( 'updates' ),array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'tags' ),
  ));
}

add_filter( 'manage_taxonomies_for_updates_columns', 'activity_type_columns' );
function activity_type_columns( $taxonomies ) {
    $taxonomies[] = 'Tags';
    return $taxonomies;
}
?>