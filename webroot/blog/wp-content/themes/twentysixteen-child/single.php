<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<div id="blog-detail">
    <main id="main" class="site-main" role="main">
    <div class="row">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the single post content template.
	?>
	<?php 

if ( get_post_type( get_the_ID() ) == 'post' ) { ?>
<div class="col-md-8">
<h3><?php echo get_the_title(); ?></h3>
<div class="date-title"><?php  $category_detail=get_the_category($post->ID);//$post->ID
foreach($category_detail as $cd){
echo $cd->cat_name;
} ?>
 <?php echo get_the_date(); ?></div><!--date-title-->
<?php echo get_the_post_thumbnail(); ?>
<p><?php echo get_the_content(); ?></p>



<div class="follow-share">
	<?php /* ?>
	<div class="sub-link">
	<a href="#" class="btn btn-default text-left"><?php  $category_detail=get_the_category($post->ID);//$post->ID
foreach($category_detail as $cd){
//echo $cd->cat_name;
} ?></a>

</div><!--sub-link-->
<?php */ ?>
                <div class="s-link">
         <!--       <ul class="hover-link">
                <li><a href="#" class="fa fa-facebook"></a></li>
                <li><a href="#" class="fa fa-twitter"></a></li>
                <li><a href="#" class="fa fa-linkedin"></a></li>
                <li><a href="#" class="fa fa-google-plus"></a></li>
                </ul>-->
                <a href="" class="btn btn-default">Share &nbsp; <i class="fa fa-share-alt" aria-hidden="true"></i></a><!--s-link-->
                
                <div class="share-button"><?php echo do_shortcode('[simple-social-share]'); ?><?php echo do_shortcode('[whatsapp]'); ?></div></div><!--s-link--><div class="clearfix"></div></div><!--follow-share-->

		<?php	
			//get_template_part( 'template-parts/content', 'single' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				//comments_template();
			}
			
			?></div><!--col-sm-9-->
			
			<div class="col-md-4 ">
			<?php get_sidebar(); ?>
            </div><!--col-sm-3-->
			</div><!--row-->
			<?php

}elseif ( get_post_type( get_the_ID() ) == 'updates' ){
	
	?>
	
<div id="col_about"><div class="update_contnt " ><p><?php the_title(); ?></p>
		 <div class="mn-tag">
             <span class="up-date pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo get_the_date(); ?></span>
                <span class="up-tag"> <i class="fa fa-tag" aria-hidden="true"></i>   <?php
              $sql=("select t.name ,t.slug from wp_terms t, wp_term_taxonomy tt, wp_term_relationships tr
    where t.term_id=tt.term_id AND tt.term_taxonomy_id=tr.term_taxonomy_id and tr.object_id=$post->ID");
    $results = $wpdb->get_results($sql);

    foreach( $results as $result ) {
                
               ?>
               
           
<a href="<?php echo site_url(); ?>/tags/<?php echo $result->slug; ?>"><?php echo $result->name; ?></a> |
               
               <?php
             }
             ?></span></div></div></div>	
	
	<?php
	
	
	
}else{
	
get_template_part( 'template-parts/content', 'single' );	
}
		endwhile;
		?>

	</main><!-- .site-main -->
    
    </div><!--blog-detail-->

	<!--<?php// get_sidebar( 'content-bottom' ); ?>-->

</div><!-- .content-area -->

<!--<?php// get_sidebar(); ?>-->
<?php get_footer(); ?>
