<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
	<div id="category">	<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>
		
			
					<?php
	 $uri =  $_SERVER["REQUEST_URI"];
	$uriArray = explode('/', $uri);
$page_url = $uriArray[1];
$page_url2 = $uriArray[2];

	if($page_url2=='tags'){ ?>	
				<div class="reslt" ><span id="f-res">Found <?php echo $wp_query->found_posts; ?> results for:</span> 
						<span id="search-word"><?php the_archive_title( '<p class="p-tag">', '</p>' );	?></span> 

<section id="col_about">
 <div class="updates-all" >

		
	 <div class="entry-content">
					
					
			
			</div>
		<?php	// Start the Loop.
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				//get_template_part( 'template-parts/content', get_post_format() );

			// End the loop. ?>
			
			
	
	<div class="update_contnt" >
	<p><a href="<?php echo get_the_permalink(); ?>"><?php echo substr(get_the_title(),0,200); ?>......<span style="color:#e74c3c"> Read more</span></a></p>
	
	<?php //substr(the_title( sprintf( '<p><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></p>' ),0,20); ?>
 <div class="mn-tag">
             <span class="up-date pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo get_the_date(); ?></span>
                <span class="up-tag"> <i class="fa fa-tag" aria-hidden="true"></i>   <?php
            $sql=("select t.name ,t.slug from wp_terms t, wp_term_taxonomy tt, wp_term_relationships tr
    where t.term_id=tt.term_id AND tt.term_taxonomy_id=tr.term_taxonomy_id and tr.object_id=$post->ID");
    $results = $wpdb->get_results($sql);

    foreach( $results as $result ) {
                
               ?>
               
           
<a href="<?php echo site_url(); ?>/tags/<?php echo $result->slug; ?>"><?php echo $result->name; ?></a> |
               
               <?php
             }
             ?></span></div>
	</div>
	
	<?php  endwhile; ?>
	 </div>
     </div><!--updates-all-->
</section>
	<!--- blog category --><?php 
	}else{ ?>  <section class="grid-section">
        <ul class="mosaicflow">
		<?php
		
			while ( have_posts() ) : the_post();
		
		?>
		
		 <li class="mosaicflow__item">
            <figure> <a href="<?php the_permalink();?>">
              <?php $a=wp_get_attachment_url(get_post_thumbnail_id($post->ID))?>
              <img width="50%" src="<?php echo $a; ?>" alt="service1"> </a>
             <figcaption> <span class="category"><?php $category_detail=get_the_category($post->ID);//$post->ID
foreach($category_detail as $cd){ ?>
              <a href="<?php echo site_url(); ?>/category/<?php echo $cd->slug; ?>"><?php echo $cd->cat_name; ?></a>
              <?php } ?></span>
              <h4><a href="<?php the_permalink(); ?>"><?php echo get_the_title();?></a></h4>
              <div class="date"><?php echo get_the_date(); ?></div>
              
                <p> <?php echo substr(get_the_content(),0,150); ?> </p>
                <a href="<?php the_permalink(); ?>"class="btn btn-primary pull-right">View</a> 
                
                <div class="follow-share">
                <a href="" class="btn btn-default">Share &nbsp; <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                <div class="share-button"><?php echo do_shortcode('[simple-social-share]'); ?><?php echo do_shortcode('[whatsapp]'); ?></div></div>
              </figcaption>
            </figure>
          </li>
		

	
			
			
			
			
			
			
			
			<?php 
			endwhile;
	} ?>
	 </ul>
      </section>
	<?php
	
	
			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Prev', 'twentysixteen' ),
				'next_text'          => __( 'Next ', 'twentysixteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
			) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
		

			 
		</main><!-- .site-main -->
        </div><!--category-->
	</div><!-- .content-area -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
