<?php
   /**
    * The main template file
    *
    * This is the most generic template file in a WordPress theme
    * and one of the two required files for a theme (the other being style.css).
    * It is used to display a page when nothing more specific matches a query.
    * E.g., it puts together the home page when no home.php file exists.
    *
    * @link http://codex.wordpress.org/Template_Hierarchy
    *
    * @package WordPress
    * @subpackage Twenty_Sixteen
    * @since Twenty Sixteen 1.0
    */
   
   get_header(); ?>
<div id="primary page-wrapper" class="content-area homebg">
   <main id="main" class="site-main" role="main">
      <?php /* ?>
      <section class="slider">
         <?php $cat = get_categories();
            // echo "<pre>"; print_r($cat); 
              
              foreach($cat as $cats){
                  
                  ?>
         <div class="slide-img">
            <div class="cat-<?php echo $cats->term_id; ?> ">
               <div class="back">
                  <hgroup>
                     <a href="<?php echo site_url(); ?>/category/<?php echo $cats->slug; ?>">
                        <img src="<?php echo z_taxonomy_image_url($cats->term_id); ?>" alt="" />
                        <h3><?php echo $cats->name; ?></h3>
                     </a>
                  </hgroup>
               </div>
            </div>
         </div>
         <!--slide-img-->
         <?php 
            }
            ?>
         <div class="clearfix"></div>
      </section>
      <?php */ ?> 
      <!--slider--> 
      <section class="grid-section">
         <ul class="mosaicflow">
            <?php //echo do_shortcode("[post_grid id='58']"); ?>
            <?php
               $arg=array(
               'post_type' => 'quotes',
               'posts_per_page'=>1,
               'post_status' =>'publish',
               'orderby'=>'date',
               'order'=>'desc'
               );
               $service = new WP_Query($arg);
               while (	$service ->have_posts() ) : $service ->the_post();
               ?>
            <li class="mosaicflow__item card mb-3">
               <figure class="grid-comment">
                  <figcaption>
                     <p><?php echo get_the_content(); ?></p>
                     <div class="cmnt-name"><?php echo get_the_title(); ?></div>
                     <?php $a=wp_get_attachment_url(get_post_thumbnail_id($post->ID))?>
                     <img style="width:20%" src="<?php echo $a; ?>" alt="DAAC_blog_quotes" class="img-circle pull-right">
                  </figcaption>
               </figure>
            </li>
            <?php endwhile; ?>
            <?php
               $arg=array(
               'post_type' => 'post',
               'posts_per_page'=>-1,
               'post_status' =>'publish',
               'orderby'=>'date',
               'order'=>'desc'
               );
               $service = new WP_Query($arg);
               while (	$service ->have_posts() ) : $service ->the_post();
               ?>
            <li class="mosaicflow__item card mb-3">
               <figure>
                  <a href="<?php the_permalink();?>">
                  <?php $a=wp_get_attachment_url(get_post_thumbnail_id($post->ID))?>
                  <img width="50%" src="<?php echo $a; ?>" alt="DAAC_blog | courses"> </a>
                  <figcaption>
                     <span class="category"><?php $category_detail=get_the_category($post->ID);//$post->ID
                        foreach($category_detail as $cd){ ?>
                     <a href="<?php echo site_url(); ?>/category/<?php echo $cd->slug; ?>"><?php echo $cd->cat_name; ?></a>
                     <?php } ?></span>
                     <h4><a href="<?php the_permalink(); ?>"><?php echo get_the_title();?></a></h4>
                     <div class="date"><?php echo get_the_date(); ?></div>
                     <p> <?php echo substr(get_the_content(),0,150); ?> </p>
                     <a href="<?php the_permalink(); ?>"class="btn btn-primary pull-right">View</a> 
                     <div class="follow-share">
                        <a href="javascript:void(0)" class="btn btn-default">Share &nbsp; <i class="fa fa-share-alt" aria-hidden="true"></i></a>
                        <div class="share-button"><?php echo do_shortcode('[simple-social-share]'); ?><?php echo do_shortcode('[whatsapp]'); ?></div>
                     </div>
                  </figcaption>
               </figure>
            </li>
            <?php endwhile; ?>
         </ul>
      </section>
      <!-- // grid-wrap --> 
   </main>
   <!-- .site-main --> 
</div>
<!-- .content-area --> 
<?php //get_sidebar(); ?>
<?php get_footer(); ?>