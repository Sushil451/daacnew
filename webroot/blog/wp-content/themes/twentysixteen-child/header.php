<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php endif; ?>
    <?php wp_head(); ?>
    <link href="<?php echo get_stylesheet_directory_uri();?>/css/style.css?14" rel="stylesheet" type="text/css">
    <!--  <link href="<?php// echo get_stylesheet_directory_uri();?>/css/style-1.css" rel="stylesheet" type="text/css">-->
    <!-- <link href="<?php //echo get_stylesheet_directory_uri();?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="<?php echo get_stylesheet_directory_uri();?>/css/animate.css" rel="stylesheet" type="text/css">
    <script src="<?php echo get_stylesheet_directory_uri();?>/js/wow.js"></script>
    <!-------------wow-------------->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri();?>/js/bootstrap.min.js"></script>
    <!-- <link rel="stylesheet" type="text/css" href="<?php //echo get_stylesheet_directory_uri();?>/css/component.css" />
	<script src="<?php //echo get_stylesheet_directory_uri();?>/js/modernizr.custom.js"></script> -->
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/responsive.css" />
    <script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-3229371-11', 'auto');
    ga('send', 'pageview');
    </script>


    <style type="text/css">
    #blog-detail ul li {
        list-style: disc;
        font-size: 16px;
        margin-bottom: 3px;
        font-size: 16px;
        color: #666;
        text-align: left;
        font-family: montserratlight;
    }

    #blog-detail ul {
        padding-left: 15px;
        margin-bottom: 24px;
    }

    #blog-detail b {
        font-size: 20px;
    }

    #contctus p strong {
        font-weight: 500 !important;
    }

    /* header .hder .pull-right{ float: none !important;} */
    /* header .hder #wpb_widgetadd-2 a { color:#fff;
	height: 24px !important;
	width:24px !important;
	position:relative !important;
	display:inline-block !important;
	line-height:24px !important;
	text-align:center !important;
	margin-left:6px !important;
}

header .hder  a { color:#fff;
	height: auto !important;
	width:auto !important;
	position:relative !important;
	display:inline-block !important;
	line-height:24px !important;
	text-align:center !important;
	margin-left:6px !important;
}

header .hder a.nav-link:hover:after {
    transform: inherit !important;
    opacity: 0 !important;
} */
    #blog-detail .follow-share .s-link {
        position: relative;
        margin-bottom: 10px;
    }

    #blog-detail .follow-share .s-link .share-button {
        /*float: left; */
        line-height: 31px;
        display: none;
        position: absolute;
        width: 132px;
        top: -10px;
        right: 100%;
    }

    header p {
        margin: 0px;
        color: #fff;
    }

    @-ms-viewport {
        width: device-width;
    }

    @viewport {
        width: device-width;
    }

    @media (max-width: 992px) {

        #contctus .add-box {
            min-height: 190px;
        }
    }

    @media (max-width: 767px) {
        .slider .slide-img {
            width: 33.333% !important;
            float: left;
        }
    }

    @media (max-width: 579px) {
        .mosaicflow__item figure figcaption h4 a {
            display: block;
            font-size: 16px;
        }

        .grid-section {
            margin: 20px 0px 0px;
        }

        .mosaicflow__item figure {
            padding: 15px 0px;
        }

        .slider .back img {
            height: 30px;
        }

        .slider .back h3 {
            font-size: 16px;
        }

        .mn-tag {
            margin-top: 40px;
        }

        .update_contnt li p,
        .update_contnt p {
            text-align: justify;
        }
    }

    @media (max-width: 480px) {
        #contctus .col-xs-6 {
            width: 100%;
        }

        #contctus .add-box {
            min-height: inherit;
        }

        .slider .back img {
            height: 24px;
        }

        .slider .back h3 {
            font-size: 14px;
        }
    }

    @media (max-width: 359px) {
        footer .newsletter .wysija-paragraph input {
            width: 100%;
        }
    }

    @media print,
    (-o-min-device-pixel-ratio: 5/4),
    (-webkit-min-device-pixel-ratio: 1.25),
    (min-resolution: 120dpi) {}

    @media print {}
    </style>
</head>

<body <?php body_class(); ?>>
    <div id="page" class="site">
        <div class="site-inner">
            <a class="skip-link screen-reader-text"
                href="#content"><?php _e( 'Skip to content', 'twentysixteen' ); ?></a>

            <header id="masthead" class="site-header" role="banner">
                <div class="hder">
                    <div class="container">
                        <div class="d-flex justify-content-between align-items-center">
                            <p class=" text-left"> <span class="d-none d-sm-inline-block">Call :</span> 8764122221</p>
                            <div class=""><?php dynamic_sidebar('header-sidebar'); ?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!--container-->
                </div>
                <!--hder-->

                <div class="hedr-2">
                    <div class="container">
                        <div class="site-header-main">
                            <div class="site-branding pull-left">

                                <?php twentysixteen_the_custom_logo(); ?>

                                <?php if ( is_front_page() && is_home() ) : ?>
                                <h1 class="site-title"><a href="https://www.daac.in/" rel="home"><img
                                            src="<?php echo get_stylesheet_directory_uri();?>/images/daac.png" /></a>
                                </h1>

                                <?php else : ?>
                                <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"
                                        rel="home"><img
                                            src="<?php echo get_stylesheet_directory_uri();?>/images/daac.png" /></a>
                                </p>
                                <?php endif;

					$description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
                                <p class="site-description"><?php //echo $description; ?></p>
                                <?php endif; ?>
                            </div><!-- .site-branding -->

                            <?php if ( has_nav_menu( 'primary' ) || has_nav_menu( 'social' ) ) : ?>
                            <button id="menu-toggle"
                                class="menu-toggle"><?php _e( 'Menu', 'twentysixteen' ); ?></button>


                            <!---menu-start-->
                            <div id="site-header-menu" class="site-header-menu pull-right">
                                <?php if ( has_nav_menu( 'primary' ) ) : ?>
                                <nav id="site-navigation" class="main-navigation" role="navigation"
                                    aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
                                    <?php
									wp_nav_menu( array(
										'theme_location' => 'primary',
										'menu_class'     => 'primary-menu',
									 ) );
								?>
                                </nav><!-- .main-navigation -->
                                <?php endif; ?>

                                <?php if ( has_nav_menu( 'social' ) ) : ?>
                                <nav id="social-navigation" class="social-navigation" role="navigation"
                                    aria-label="<?php esc_attr_e( 'Social Links Menu', 'twentysixteen' ); ?>">
                                    <?php
									wp_nav_menu( array(
										'theme_location' => 'social',
										'menu_class'     => 'social-links-menu',
										'depth'          => 1,
										'link_before'    => '<span class="screen-reader-text">',
										'link_after'     => '</span>',
									) );
								?>
                                </nav><!-- .social-navigation -->
                                <?php endif; ?>
                            </div><!-- .site-header-menu -->
                            <?php endif; ?>
                            <div class="clearfix"></div>
                        </div><!-- .site-header-main -->
                    </div>
                    <!--container-->
                </div>
                <!--hedr-2-->
                <?php if ( get_header_image() ) : ?>
                <?php
					/**
					 * Filter the default twentysixteen custom header sizes attribute.
					 *
					 * @since Twenty Sixteen 1.0
					 *
					 * @param string $custom_header_sizes sizes attribute
					 * for Custom Header. Default '(max-width: 709px) 85vw,
					 * (max-width: 909px) 81vw, (max-width: 1362px) 88vw, 1200px'.
					 */
					$custom_header_sizes = apply_filters( 'twentysixteen_custom_header_sizes', '(max-width: 709px) 85vw, (max-width: 909px) 81vw, (max-width: 1362px) 88vw, 1200px' );
				?>
                <div class="header-image">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                        <img src="<?php header_image(); ?>"
                            srcset="<?php echo esc_attr( wp_get_attachment_image_srcset( get_custom_header()->attachment_id ) ); ?>"
                            sizes="<?php echo esc_attr( $custom_header_sizes ); ?>"
                            width="<?php echo esc_attr( get_custom_header()->width ); ?>"
                            height="<?php echo esc_attr( get_custom_header()->height ); ?>"
                            alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                    </a>
                </div><!-- .header-image -->
                <?php endif; // End header image check. ?>

            </header><!-- .site-header -->

            <div id="content" class="site-content">