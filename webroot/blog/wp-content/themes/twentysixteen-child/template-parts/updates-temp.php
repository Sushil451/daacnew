<?php
/*
Template name: updates-temp

*/

get_header();

?>
<div id="primary" class="content-area ">
	
  <div class="container"> <section class="page_heading"> 
   <header class="entry-header">
		<h1 class="entry-title"><? echo get_the_title(); ?></h1>		
	</header><!-- .entry-header -->

	
   </section>
 <div class="entry-content">
<section id="col_about">
  <main class="container">

        
    <!--<img class="pull-right" src="/images/abt_img.png" alt="">-->
       
     
<ul class="" style="list-style: none;">
  <?php
$custom_query_args = array(
             'post_type'=>'updates',
             'paged' => $custom_query_args['paged'],
	     'posts_per_page' =>10,
             'post_stutas'=>'publish',
             'orderby'=>'date',
             'order'=>'desc'
             );
$custom_query_args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

			// Instantiate custom query
			$custom_query = new WP_Query( $custom_query_args );

			// Pagination fix
			$temp_query = $wp_query;
			$wp_query   = NULL;
			$wp_query   = $custom_query; ?>

         <div class="static-text"> Page <?php echo $custom_query->query['paged']; ?> of <?php
            // print_r($custom_query);
             
             echo $custom_query->max_num_pages; ?>
             <?php previous_posts_link( '<span> < </span>' ); ?>
             <?php next_posts_link( ' >', $custom_query->max_num_pages ); ?>
             </div>
         <div class="updates-all" >
          <?php   
             while ($custom_query ->have_posts() ) : $custom_query ->the_post();
            
             ?> <div class="update_contnt" ><li>
             
             
             
           <p> <a href="<?php echo get_the_permalink(); ?>">
            <?php echo substr(get_the_title(),0,200); ?>......<span style="color:#e74c3c"> Read more</span></a></p>
           <div class="mn-tag">
             <span class="up-date pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo get_the_date(); ?></span>
                <span class="up-tag "> <i class="fa fa-tag" aria-hidden="true"></i>   <?php
            global $post;

        $sql=("select t.name ,t.slug from wp_terms t, wp_term_taxonomy tt, wp_term_relationships tr
    where t.term_id=tt.term_id AND tt.term_taxonomy_id=tr.term_taxonomy_id and tr.object_id=$post->ID");
    $results = $wpdb->get_results($sql);

    foreach( $results as $result ) {

        ?>       
           
<a href="<?php echo site_url(); ?>/tags/<?php echo $result->slug; ?>"><?php echo $result->name; ?></a> |
               
               <?php
             }
             ?></span></div>
             </li></div>
             <?php
//print_r($t);
             endwhile;
wp_reset_query();

   ?>
   
   </div>
     
<?php previous_posts_link( ' <div class="pagination-button">< Prev </div>' ); ?> 
 <?php next_posts_link( '<div class="pagination-button">Next > </div>', $custom_query->max_num_pages ); ?>

<?php 
// Reset main query object
$wp_query = NULL;
$wp_query = $temp_query;
   ?>
</ul>

 </main>
</section>

</div>
</div><!--container-->
</div>
<?php
get_footer();
?>