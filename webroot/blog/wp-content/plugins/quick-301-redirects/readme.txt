﻿=== Quick 301 Redirects ===
Contributors: galdub, tomeraharon, premio
Tags: 301 redirects, 301, redirects, seo redirects, url redirects, domain redirect
Requires at least: 3.1
Tested up to: 5.3
Stable tag: 1.1.3
Plugin URI: https://premio.io/

The fastest and easiest way to do 301 redirects. You can set each redirect individually or bulk upload unlimited number of 301 redirects using a CSV file.

== Description ==

<strong>The fastest and easiest way to do 301 redirects. You can set each redirect individually or bulk upload unlimited number of 301 redirects using a CSV file.</strong>

You can use Quick 301 Redirects for small website with just a few pages and also for sites with thousands of pages. 

= Set 301 Redirects Manually Or In Bulk =

Manage all your website's 301 redirects without any professional knowledge. All you need is to install our plugin, and the leave the rest to our super simple UI (RTL supported).

Our super simple UI will enable you to set your redirects manually one-by-one or using a CSV file for bulk upload.

== Installation ==
1. Install and activate Quick 301 Redirects plugin on your WordPress website
2. Set your 301 redirects manually or upload a CSV file for bulk configuration and click on the "SAVE CHANGES" button
3. You're good to go! All your 301 redirects are now live :)

== Frequently Asked Questions ==

= What can I do with this plugin =
You can set 301 redirects manually or upload a CSV file for bulk configuration

= Is it free? =
Yep, Quick 301 Redirects is completely free

= Are there any restriction? =
No, you can configure as many 301 redirects as you want

= The redirects don't work, what should I do? =
You're probably using a cache plugin, please purge your cache or disable your plugin for a few minutes.

= Where do I find Quick 301 Redirects in my WP admin? =
Click on "Setting" in your WP admin menu, and there in a sub-menu you'll find "Quick 301 Redirects"

== Screenshots ==

1. Quick 301 Redirects, the easiest way to configure 301 redirects
2. You can set your 301 redirects in bulk
3. Or you can set your 301 redirects manually

== Changelog ==

= 1.1.3 =
* another fix for the specific URLs issue

= 1.1.2 =
* specific URLs bug fix

= 1.1.1 =
* added RTL menu support

= 1.1 =
* Launch