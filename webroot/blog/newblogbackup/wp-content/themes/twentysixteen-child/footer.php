<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
</div>
<!-- .site-content -->

<footer id="colophon" class="site-footer">
  <div class="container">
    <div class="row">
      <div class="col-xs-8 col-sm-6 newsletter">
        <?php dynamic_sidebar('newletter-sidebar'); ?>
      </div>
      <div class="col-xs-4 col-sm-6 social">
        <?php dynamic_sidebar('header-sidebar'); ?>
      </div>
    </div>
  </div>
  <div class="site-info">
    <div class="container">
      <div class="pull-left text-left">&copy; <?php echo $archive_year = get_the_time('Y'); ?>  All Rights Reserved. &nbsp; Designed By. <a href="http://doomshell.com/">Doomshell.com</a></div>
      <div class="sitmap pull-right"><a target="_blank" href="<?php echo site_url(); ?>/sitemap-pt-page-2016-12.xml">Site Map</a></div>
    </div>
  </div>
</footer>
<!-- .site-footer -->
</div>
<!-- .site-inner -->
</div>
<!-- .site --> 
<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.mosaicflow.min.js"></script>
<?php wp_footer(); ?>

<script>
    wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      }
    );
    wow.init();
    document.getElementById('moar').onclick = function() {
      var section = document.createElement('section');
      section.className = 'section--purple wow fadeInDown';
      this.parentNode.insertBefore(section, this);
    };
  </script>
  <script>
  $(document).ready(function() {
  $('.contact_form input, .contact_form textarea').each(function() {
    $(this).on('focus', function() {
      $(this).parent('.f-style').addClass('active');
    });

    $(this).on('blur', function() {
      if ($(this).val().length == 0) {
        $(this).parent('.f-style').removeClass('active');
      }
    });

    if ($(this).val() != '') $(this).parent('.f-style').addClass('active');
  });
});
  </script>
</body></html>
