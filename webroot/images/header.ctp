  <!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
<meta name="authoring-tool" content="Adobe_Animate_CC">
 <!-- Chrome, Firefox OS and Opera -->  
    <meta name="theme-color" content="#02abe3" />  
    <!-- Windows Phone -->  
    <meta name="msapplication-navbutton-color" content="#02abe3" />   
    <!-- iOS Safari -->  
    <meta name="apple-mobile-web-app-capable" content="yes">  
    <meta name="apple-mobile-web-app-status-bar-style" content="#02abe3">
    
<?php //$cl=Router::url($this->here, true );
$urlseo = str_replace("/", "", $_SERVER['REQUEST_URI']);
if($urlseo){
$cl = str_replace("/", "", $_SERVER['REQUEST_URI']);
}else{
$cl=Router::url($this->here, true );
}

$met=$this->Common->meta($cl);

if(!empty($met))
{ ?>
<title><?php echo $met['Seo']['title']; ?></title>
<meta name="keywords" content="<?php echo $met['Seo']['keyword'];?>" />
<meta name="description" content="<?php echo $met['Seo']['description'];?>" />

<?php } ?>

<!---------bootstrap-------------------->
<link href="<?php echo $this->webroot; ?>css2/bootstrap.min.css" rel="stylesheet" type="text/css">
<!------------font-awesome------------------>
<link href="<?php echo $this->webroot; ?>css2/prettyPhoto.css" rel="stylesheet" type="text/css">
<link href="<?php echo $this->webroot; ?>css2/styles.css" rel="stylesheet" type="text/css">
<link href="<?php echo $this->webroot; ?>css2/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo $this->webroot; ?>css2/owl.carousel.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo $this->webroot; ?>css2/owl.theme.default.css" rel="stylesheet" type="text/css">
<link href="<?php echo $this->webroot; ?>css2/bootstrap-reboot.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo $this->webroot; ?>css2/toggle.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2Cxux1m3BoMR5fkr9UPq99hClv0gNlp1';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>


<link rel="stylesheet" href="<?php echo $this->webroot; ?>css/admin/bootstrap.offcanvas.min.css"/>
<!---------------genral---------------------->
 <?php //require_once WWW_ROOT . DS . 'css2' . DS . 'style.php';  ?>
 <link href="<?php echo $this->webroot; ?>css2/style.css?1" rel="stylesheet" type="text/css">
 <link href="<?php echo $this->webroot; ?>css2/style_new.css?1" rel="stylesheet" type="text/css">
 <style type="text/css">
#header .head_contact_info li{ line-height:0px;}
#header .head_contact_info .all_courses{ font-size:18px; font-weight:500;}
#header .modal-header{ background:#02abe3!important; border-radius:0px;}
#header .modal-header .close{    color: #fff;
    /* text-shadow: 0 1px 0 #fff; */}
#header .modal-header h5{ color:#fff; font-size: 24px;}
header{ background-color:#fff !important; }
#header .modal-body{ padding:0px;}
#header .modal-body .nav-pills .nav-link.active, #header .modal-body .nav-pills .show>.nav-link{ color: #02abe3;
    background: #f7f7f7; }
#header .modal-body .leftbg{ background:#f1f1f1;min-height:100%;}
#header .modal-body ul li{ padding:0px;}
#header .modal-body ul li a{ display:block; padding:10px;}
#header .modal-body ul li{ padding:10px;}
/*#header .modal-body ul li:hover{ background:#f7f7f7;}*/
#header .modal-body ul li a{ color:#fff; font-size:14px; text-transform:capitalize;}
#header .modal-body .nav-pills .nav-link{ color:#212529;}
#header .modal-body .col-md-4 ul{ border-right:1px solid #eaeaea; min-height: 300px;
    margin-bottom: 0px;}
#header .modal-body .col-md-4:last-child  ul{ border-right:none;}
/*Header Slider End*/

/*============app end==============*/	

header .modal.show .modal-dialog{ max-width:1000px;}
header .modal.show .modal-dialog a, header .modal.show .modal-dialog li{ text-align:left;}
header{ background-color:#02abe3;}
   .onscroll_header.sticked .header_mid .text-center img{ width: 100px !important; }
header .header_bottom_scroll p{ margin-top: 10px !important; font-size: 22px; font-weight: bold; }
#myModal4 .modal-body{ position:relative;}
#myModal4 .modal-body .close{ position:absolute; right:-10px; top:-10px; background-color:#fff; height:30px; width:30px; opacity:1 !important; -webkit-border-radius: 50%;
-moz-border-radius: 50%;
border-radius: 50%; border:1px solid #02abe3; }
.onscroll_header.sticked #address .box{ padding-bottom: 0px; }
header .onscroll_header #address{ margin-top: 0px; }
.price-value h5 span, .price-value .title_shop, .price-value.three h5 lable{ color: #fff !important; }
#course_vidsec{ padding: 60px 0px 30px; background-color: #02abe3; margin-top:20px;}
#course_vidsec h3{ margin-bottom:30px; }
#our_facltypro{ padding: 35px 0px; background-color: #f6f6f6;}
#modual_sec .modual_div ul li{ text-transform: capitalize; }
#our_facltypro .faclty_probox{ margin-bottom: 15px; }
#our_facltypro .faclty_probox a{ display: block; padding: 6px; border:1px solid #bebebe; position: relative; overflow: hidden;}
#our_facltypro .faclty_procount_loaderbox a .faclty_probox_overlay{ display: none; position: absolute; left: 0px; right: 0px; top: 100%; background-color: rgba(0,0,0,0.7); -o-transition:.5s;
-ms-transition:.5s;
-moz-transition:.5s;
-webkit-transition:.5s; height: 100%; padding-top: 65px;}
#our_facltypro .faclty_probox a:hover .faclty_probox_overlay{ top: 0px; }
#our_facltypro .faclty_probox a .faclty_probox_overlay p{ color: #fff; font-size: 24px; text-align: center; }
.faclty_probox_overlay{ display:none;}
#our_facltypro  h3{ margin-bottom: 30px; }
.price-value{    background: #a0c870 !important;
    border-bottom: 2px solid #2b2b33  !important;}
   .pricing-grid1{border: 2px solid #a0c870 !important;}
.pricing-grids{ margin: 0px !important; margin-top: 50px !important; }
.pricing-grid1:hover div.price-bg ul li a, .pricing-grid1:hover div.price-value h3 a{ color: #a0c870 !important; }
.price-value.three{    background: #04dbdd !important;
    border-bottom: 2px solid#028f87 !important;}
    #course_vidsec h3{ color: #fff !important; }
  #frenchisee_sec img{ width: 100%; }
.modal-open{ overflow: inherit !important; padding: 0px !important; }
header .onscroll_header{ z-index: 999 !important; } 
#carrer_coursdur .inner_div{ padding: 50px 0px 60px !important; }
.faclty_probox h6{ color: #fff; font-size: 18px; text-align: center; font-weight: bold; background-color:#000; padding:5px; margin:0px;}
.pricing-grid1, .pricing-grid2, .pricing-grid3{ min-height: 546px !important; }
#speak_crausal{ width:90%; margin:auto; }
#speak_crausal .owl-nav .owl-prev{ left:-60px; }
#speak_crausal .owl-nav .owl-next{ right:-60px; }
.footer_design {
    position: absolute;
    bottom: 0px;
    width: 100%;
    z-index: 9;}
    #speak_crausal .owl-nav .owl-next, #speak_crausal .owl-nav .owl-next:hover, #speak_crausal .owl-nav .owl-next:focus, #speak_crausal .owl-nav .owl-prev, #speak_crausal .owl-nav .owl-prev:hover, #speak_crausal .owl-nav .owl-prev:focus{ background-color:transparent; }

#grow_careers {
	padding:70px 0px;
}
#grow_careers .option .option_iocn {
	min-height: 154px;
}
#grow_careers .option .option_icon_1 {
	margin-top:53px;
}
#grow_careers .option .option_icon_3 {
	left: 0px;
	right: 0px;
	margin: auto;
	bottom: 35px;
}
.position-relative{ position:relative !important;}
.position-absolute{ position:absolute !important;}
#grow_careers .option .option_icon_2 {
	left: 0px;
	right: 0px;
	margin: auto;
	bottom:0
}

/*#grow_careers .experts:hover .experts_icon_3{ bottom:25px;     -webkit-tap-highlight-color: rgba(0,0,0,0);

    -webkit-font-smoothing: antialiased;

    -moz-osx-font-smoothing: grayscale;}*/

	
	#courses_tutorials .video_section .nav{ text-align:left;}
	#courses_tutorials .video_section .nav-link{ color:#fff;}
	
#testimonials.testimonials_sec{ display:none;}
	

	/* Hang */

@-webkit-keyframes hvr-hang {
 0% {
 -webkit-transform: translateY(10px);
 transform: translateY(10px);
}
 50% {
 -webkit-transform: translateY(5px);
 transform: translateY(5px);
}
 100% {
 -webkit-transform: translateY(10px);
 transform: translateY(10px);
}
}
 @keyframes hvr-hang {
 0% {
 -webkit-transform: translateY(10px);
 transform: translateY(10px);
}
 50% {
 -webkit-transform: translateY(6px);
 transform: translateY(6px);
}
 100% {
 -webkit-transform: translateY(12px);
 transform: translateY(12px);
}
}
 @-webkit-keyframes hvr-hang-sink {
 100% {
 -webkit-transform: translateY(12px);
 transform: translateY(12px);
}
}
 @keyframes hvr-hang-sink {
 100% {
 -webkit-transform: translateY(12px);
 transform: translateY(12px);
}
}
.hvr-hang {
	display: inline-block;
	vertical-align: middle;
	-webkit-transform: perspective(1px) translateZ(0);
	transform: perspective(1px) translateZ(0);
	box-shadow: 0 0 1px rgba(0, 0, 0, 0);
}
#grow_careers .option .hvr-hang, #grow_careers .option .hvr-hang, #grow_careers .option .active {
	-webkit-animation-name: hvr-hang-sink, hvr-hang;
	animation-name: hvr-hang-sink, hvr-hang;
 -webkit-animation-duration: .3s, 1.5s;
 animation-duration: .3s, 1.5s;
 -webkit-animation-delay: 0s, .3s;
 animation-delay: 0s, .3s;
	-webkit-animation-timing-function: ease-out, ease-in-out;
	animation-timing-function: ease-out, ease-in-out;
	-webkit-animation-iteration-count: 1, infinite;
	animation-iteration-count: 1, infinite;
	-webkit-animation-fill-mode: forwards;
	animation-fill-mode: forwards;
	-webkit-animation-direction: normal, alternate;
	animation-direction: normal, alternate;
}
#grow_careers .option .option_heading {
	font-size:18px;
	font-weight:600;
	margin-top:15px;
}
/*Career Section End*/



.count {
	color:#02abe3;
	font-size:28px;
	font-weight:600;
}
#count_loader .col-lg-3 {
	border-right:1px solid #cacaca;
}
#count_loader .row .col-lg-3:last-child{ border:none; }
#shiva i {
	color:#fff;
	display:inline-block;
	font-size:20px;
	display:inline-block;
	vertical-align:4px;
}
.linker {
	font-size : 20px;
	font-color: black;
}
#count_loader {
	padding:35px 0px;
	background-color:#fff;
	-webkit-box-shadow: 5px 5px 45px 0px rgba(0, 0, 0, 0.1);
	-moz-box-shadow: 5px 5px 45px 0px rgba(0, 0, 0, 0.1);
	box-shadow: 5px 5px 45px 0px rgba(0, 0, 0, 0.1);
}
#shiva p {
	font-size:16px;
	color:#232323;
	text-align:center;
}
/*counter End*/

#courses {
	padding:70px 0px;
	text-align:center;
}
#courses.course_section {
	height: auto;
	position: relative;
	overflow: hidden;
}
#courses .course_bg {
	width: 100%;
	height: 100%;
	position: absolute;
	top: 0;
	left: 0;
	z-index: 0;
	background: url(<?php echo SITE_URL; ?>images/course_bg.png) no-repeat center center;
	background-size: content !important;
	transform: scale(1.1);
}
#courses .course_contant .course_tab .nav-link {
	color:#414042;
	border:1px solid #6d6e71;
	border-radius:50px;
	margin:0px 15px;
	margin-bottom:20px;
}
#courses .course_contant .course_tab .nav-link.active {
	background:#02abe3;
	border-color:#02abe3;
	color:#fff;
}
#courses .course_contant .course_tab .course_box {
	background: #fff;
	padding:0px;
	border-radius: 15px;
	margin: 0px 0px;
	margin-top: 70px;
	-webkit-box-shadow: 10px 10px 15px 0px rgba(0, 0, 0, 0.12);
	-moz-box-shadow: 10px 10px 15px 0px rgba(0, 0, 0, 0.12);
	box-shadow: 10px 10px 15px 0px rgba(0, 0, 0, 0.12);
}
#courses .course_contant .course_tab .course_box:after {
	content: "";
	width: 80%;
	height: 153px;
	background: #20aaf2;
	display: block;
	position: absolute;
	border-radius: 15px;
	z-index:0;
	top: 90px;
	right: 10px;
}
.course_cnt{ position:relative; z-index:9; padding: 55px 15px 20px 15px; background-color:#fff;     border-radius: 15px;}
#courses .course_contant .course_tab .course_box .course_icon {
	background: #20aaf2;
	width: 60px;
	height: 60px;
	line-height: 60px;
	border-radius: 10px;
	position: absolute;
	left: 0px;
	right: 0px;
	margin: auto;
	top: -30px;
}
#courses .course_contant .course_tab .course_box .course_icon i, #courses .course_contant .course_tab .course_box .course_icon p{ font-size:26px; color:#fff; margin-bottom:0px;     vertical-align: middle;}
#courses .course_contant .course_tab a:hover {
	text-decoration: none;
}
#courses .course_contant .course_tab .course_box h4 {
	font-size:18px;
	font-weight:600; text-transform:capitalize;
}
#courses .course_contant .course_tab .graphic_color .course_icon {
	background:#ffb281;
}
#courses .course_contant .course_tab .graphic_color a {
	color:#ffb281;
}
#courses .course_contant .course_tab .course_box.graphic_color:after {
	background:#ffb281;
}
#courses .course_contant .course_tab .sketching_color .course_icon {
	background:#ca82f1
}
#courses .course_contant .course_tab .sketching_color a {
	color:#ca82f1;
}
#courses .course_contant .course_tab .course_box.sketching_color:after {
	background:#ca82f1;
}
#courses .course_contant .course_tab .photoshop_color .course_icon {
	background:#7865af
}
#courses .course_contant .course_tab .photoshop_color a {
	color:#7865af;
}
#courses .course_contant .course_tab .course_box.photoshop_color:after {
	background:#7865af;
}
#courses .course_contant .course_tab .dreamweaver_color .course_icon {
	background:#90bc66
}
#courses .course_contant .course_tab .dreamweaver_color a {
	color:#90bc66;
}
#courses .course_contant .course_tab .course_box.dreamweaver_color:after {
	background:#90bc66;
}
#courses .course_contant .course_tab .html_color .course_icon {
	background:#e86ac3
}
#courses .course_contant .course_tab .html_color a {
	color:#e86ac3;
}
#courses .course_contant .course_tab .course_box.html_color:after {
	background:#e86ac3;
}
#courses .course_contant .course_tab .css_js_color .course_icon {
	background:#a6a6a6
}
#courses .course_contant .course_tab .css_js_color a {
	color:#a6a6a6;
}
#courses .course_contant .course_tab .course_box.css_js_color:after {
	background:#a6a6a6;
}
#courses .course_contant .course_tab .bootstrap_color .course_icon {
	background:#d73c9c
}
#courses .course_contant .course_tab .bootstrap_color a {
	color:#d73c9c;
}
#courses .course_contant .course_tab .course_box.bootstrap_color:after {
	background:#d73c9c;
}
/*Courses End*/

#call_action {
	background:#02abe3;
	padding:30px 0px;
}
#call_action a {
	color: #fff;
	font-size: 18px;
	padding: 10px 50px;
	border: 2px solid #fff;
	border-radius: 5px;
	display: inline-block;
}
#call_action a i {
	font-size:32px;
	vertical-align: sub;
	margin-right:5px;
}
#call_action p {
	color: #fff;
	font-size: 18px;
	line-height: 56px;
	margin-bottom: 0;
}
/*call Action End*/



#courses_tutorials {
	padding:70px 0;
	background:#f1f2f2;
}
#courses_tutorials hgroup h6 {
	color:#ebebeb;
}
#courses_tutorials .video_section {
	margin-top:50px;
}
#courses_tutorials .video_section .nav {
	display: inherit;
	margin-top:6%
}
#courses_tutorials .video_section a:hover {
	color:#fff;
}
#courses_tutorials .video_section .nav-link {
	display: inline-block;
	margin: 0px 9px 10px 0px;
	padding: 8px 40px;
}
.nav-pills .nav-link.active {
	background-color: inherit;
}
#courses_tutorials .video_section .embed-responsive {
	border-radius:10px;
	position:relative;
	z-index:9;
}
#courses_tutorials .video_section .embed-responsive-4by3::before{ padding-top:56%;}

#myBtn3, #myBtn2{    position: absolute;
   top: 45%;
    z-index: 9;
    left: 56%;
    width: 80px;
    height: 80px;
    border-radius: 50%;
    background: rgba(185, 23, 123, .5);
    color: #fff;
    border: none;}
	#myBtn3{ left:0px; right:0px; margin:auto; top:35%;}
#myBtn3:focus, button:focus, #myBtn2:focus{ outline:none;}	
#myBtn3:after, #myBtn2:after{ font-family: "Font Awesome 5 Free";
   content: "\f04b";
   display: inline-block;
   padding-right: 3px;
   vertical-align: middle;
   font-weight: 900; }
   #myBtn3:before, #myBtn2:before{ font-family: "Font Awesome 5 Free";
   content: "\f04c";
   display: inline-block;
   padding-right: 3px;
   vertical-align: middle;
   font-weight: 900; 
   display:none;}
   
#myBtn3.hide:after, #myBtn2.hide:after{ display:none;} 
#myBtn3.hide:before, #myBtn2.hide:before{ display:block;}
#myBtn3.hide, #myBtn2.hide{ opacity:0; -webkit-transition: .5s ease-out;
  -moz-transition: .5s ease-out;
  -o-transition: .5s ease-out;
  transition: .5s ease-out;}
  #myBtn3.hide:hover, #myBtn2.hide:hover{ opacity:1; -webkit-transition: .5s ease-out;
  -moz-transition: .5s ease-out;
  -o-transition: .5s ease-out;
  transition: .5s ease-out;}
/*=====video End======*/



#recruiters {
	padding:70px 0px;
}
/*#recruiters .logo_section .row .col-md-2 a { background:url(../images/Recruiters-logos.webp) no-repeat; height:95px; width:175px;}
#recruiters .logo_section .row .col-md-2:nth-child(1) a { background-position:0px 0px;}
#recruiters .logo_section .row .col-md-2:nth-child(2) a { background-position:0px 0px;}*/
#recruiters .logo_section a {
	background:#fff;
	display:block;
	margin-bottom:30px;
	padding:10px;
	-webkit-box-shadow: 5px 5px 5px 0px rgba(0, 0, 0, 0.06);
	-moz-box-shadow: 5px 5px 5px 0px rgba(0, 0, 0, 0.06);
	box-shadow: 5px 5px 5px 0px rgba(0, 0, 0, 0.06);
}
#recruiters .logo_section {
	margin-top:30px;
}
/*=====recruiters End======*/	

#testimonials {
	background:url(<?php echo $this->webroot; ?>images/testimonials_bg.jpg) no-repeat center center;
	background-size:cover;
	padding:70px;
	overflow:hidden;
	position:relative
}
#testimonials hgroup h2 {
	color:#fff;
}
#testimonials hgroup p {
	color:#fff
}
#testimonials hgroup h6 {
	color: rgba(255, 255, 255, .1);
}
#testimonials .testimonials_video .nav {
	display: inherit;
}
#testimonials .testimonials_video .nav-link {
	display: inline-block;
	padding: 0px;
	padding:20px 0px 10px 0px;
}
#testimonials .testimonials_video {
	margin-top:40px;
}
#testimonials .testimonials_video a {
	width:31.5%;
	background:#fff;
	text-align:center;
	margin-right:5px;
	margin-bottom:9px
}
#testimonials .testimonials_video a img {
	width:60px;
	margin-bottom:10px;
}
#testimonials .testimonials_video h4 {
	font-size:16px;
	color:#02abe3;
}
#testimonials .testimonials_video p {
	font-size:14px;
	color:#231f20;
	margin-bottom:5px;
}
#testimonials .laptop_img {
	    right: -178px;
    bottom: 24px;
	max-width: 600px;
	z-index: 9;
}
#myVideo2 {
	  border: 14px solid #FFF;
    border-radius: 5px 5px 0px 0px;
    margin-left: 92px;
    border-top-width: 20px;
    border-bottom-width: 20px;
    margin-top: 50px;
    width: 90% !important;

}
.video_img{ position:relative;}
.video_img:after{     content: "";
    background:url(<?php echo $this->webroot; ?>images/laptop.png) no-repeat center top;
    width: 120%;
    height: 50px;
    display: block;
    top: calc(100% - 7px);
    right: 0px;
    left: 0px;
    z-index: 999;
    position: absolute;}

/*============Student Work==============*/	
#student_work {
	background:url(<?php echo SITE_URL; ?>images/student_work_bg.webp) no-repeat center top #f5f5f5;
	background-size:cover;
	padding:70px 0px;
}
#student_work .nav-link {
	color:#414042;
	border:1px solid #6d6e71;
	border-radius:50px;
	margin:0px 15px;
	margin-bottom:20px; font-weight:bold; min-width:130px;
}
#student_work .nav-link.active {
	background:#02abe3;
	border-color:#02abe3;
	color:#fff;
}
#student_work .work_section {
	padding:30px 0px 15px 0px;
}
#student_work .student_project {
	background: #fff;
	padding: 10px;
	border-radius: 5px;
	-webkit-box-shadow: 4px 4px 10px 0px rgba(50, 50, 50, 0.25);
	-moz-box-shadow:    4px 4px 10px 0px rgba(50, 50, 50, 0.25);
	box-shadow:         4px 4px 10px 0px rgba(50, 50, 50, 0.25);
}
#student_work .student_project ul {
	margin-bottom:0px;
	margin-top:5px;
}
#student_work .owl-carousel.owl-drag .owl-item {
	margin-bottom:15px
}
/*============Student Work end==============*/	

#google_app {
	padding-top:70px;
	background:url(<?php echo SITE_URL; ?>images/course_bg.png) no-repeat top center;
	background-size:cover;
	    padding-bottom: 40px;
}
#google_app .app_content {
	margin-top: 100px; text-align:left;
}
#google_app .hulk_img {
	right: 0px;
	bottom: -102px;
	width: 150px; z-index:99;
}
#student_work .content span:first-child{ color:#212529; font-size:16px; display:block; float:left;}
#student_work .content span:last-child{ color:#212529; font-size:16px; display:block; float:right;}
#student_work .content:after{ content:''; clear:both; display:block;}
#footer {
	background-color:#02abe3; padding-top:0px;
	
}
#footer .newsletter {
	background:#fff;
}
#footer .newsletter .form-control {
	border:none;
}
#footer .newsletter .form-control:focus {
	border-color:transparent
}
#footer .newsletter .btn {
	background:#02abe3;
	border-radius:0px;
	color:#fff;
}
#footer .newsletter .btn:focus {
	box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, .0);
}
#footer h3, #footer p {
	color:#fff;
}
#footer h6 {
	color:#fff;
	text-align:right;
	margin-top:30px;
}
#footer a {
	color:#fff;
	padding:0px 5px;
}
#footer .footbottom {
	margin-top:50px;
}
#footer .footbottom h5 {
	color:#fff;
}
#footer .footbottom p {
	font-size:14px;
	text-align:justify;
	margin-top:10px;
}
#footer .footbottom li {
	font-size:14px;
	font-weight:300;
	margin-bottom:10px; margin-right:30px;
}
.footer_copyright {
	border-top:1px solid #088bb6;
	padding:10px 0px;
	text-align:center;
	margin-top:20px;
}
.footer_design {
	position:absolute;
	bottom:0px; left:0px;
	width:100%;
	z-index:9
}
/*============app end==============*/	
#google_app .app_content img{ margin-top:12px;}
/* ==============custom==========
========================================= */
#shiva img{ display:none;}
#count_loader .row .col-xl-3:last-child #shiva{ border:none;}
#speaker_sec{ background-color:#02abe3;}
#speak_crausal .owl-item p, #speak_crausal .owl-item strong{ color:#fff;}
#speak_crausal .owl-nav .owl-next:after, #speak_crausal .owl-nav .owl-prev:before{ margin-top:10px;}
#student_work hgroup{ margin-bottom:60px;}
body{ background-color:transparent !important;}
#modual_sec .modual_div ul li:after{ content:'\f005'; font-weight:900; font-family:"Font Awesome 5 Free";}
#related_courcrausal.owl-carousel .owl-nav .owl-next:after{font-weight:900; font-family:"Font Awesome 5 Free"; content:'\f054';}
#related_courcrausal.owl-carousel .owl-nav .owl-prev:before{font-weight:900; font-family:"Font Awesome 5 Free"; content:'\f053';}
#our_facltypro{ margin-top:20px;}

#our_facltypro hgroup{ margin-bottom:60px;}
hgroup {
	position:relative;
	text-align:center;
}
hgroup h2, hgroup p {
	position:relative;
	z-index:9
}
hgroup h6 {
	font-size: 72px;
	color: #efefef;
	position: absolute;
	left: 0px;
	right: 0px;
	margin: auto;
	top: -11px;
	z-index: 8;
	font-weight: 600;
}
.cyan_color {
	background:#0091dd !important;
	color:#fff;
}
.orange_color {
	background:#f08847 !important;
	color:#fff;
}
.purple_color {
	background:#b956ef !important;
	color:#fff;
}
.green_color {
	background:#7ab541 !important;
	color:#fff;
}
.dark_purple_color {
	background:#7865af !important;
	color:#fff;
}
.dark_cyan_color {
	background:#00aaa4 !important;
	color:#fff;
}
.yellow_d_color {
	background:#d9b34d !important;
	color:#fff;
}
.cyan_light_color {
	background:#20aaf2 !important;
	color:#fff;
}
.magenta_color {
	background:#fc56cb !important;
	color:#fff;
}
.orange_dark_color {
	background:#e8884b !important;
	color:#fff;
}
.yellow_dark_color {
	background:#ca9840 !important;
	color:#fff;
}

#degree_cors_sec .degree_innerdiv img{ display:block; margin:auto; text-align:center;}
#degree_cors_sec .degree_innerdiv h4{ text-align:center;}
header .modal .tab-content ul li{ width:25%; float:left;}
header .modal .tab-content ul:after{ content:''; clear:both; display:block;}
#grow_careers{ padding: 40px 0px !important; }
#recruiters{ padding-top:25px !important; padding-bottom:30px !important; }
#courses{ padding:15px 0px 40px !important;}
#student_work{ padding:40px 0px 10px !important;}
#testimonials{ padding: 25px 25px !important; }
.pricing-grid1, .pricing-grid2, .pricing-grid3{     transition: inherit !important;
    -webkit-transition: inherit !important;
    -moz-transition:inherit !important;
    -o-transition: inherit !important;}
	.pricing-grid1:hover, .pricing-grid2:hover, .pricing-grid3:hover {
    transform: inherit !important;
    -webkit-transform: inherit !important;
    -moz-transform: inherit !important;
    -o-transform: inherit !important;
    -ms-transform:inherit !important;}
	.price-value{ background-color:transparent !important; border-top:5px solid #ff4594;}
	.pricing-grid1:hover, .pricing-grid1{ border:none !important; background-color:#f5f5f5 !important;}
	.price-bg{background-color:#f5f5f5 !important;}
	.pricing-grid1 .price-value h2 a{ color:#ff4594 !important;}
	.price-value, .price-value.two, .price-value.three{ border-bottom:1px solid #e6e6e6 !important;}
	.pricing-grid1 ul li a, .pricing-grid2 ul li a, .pricing-grid3 ul li a, .pricing-grid1:hover div.price-bg ul li a, .pricing-grid1:hover div.price-value h3 a{ color:#000 !important;}


	.pricing-grid2, .pricing-grid2, .pricing-grid3{     transition: inherit !important;
    -webkit-transition: inherit !important;
    -moz-transition:inherit !important;
    -o-transition: inherit !important;}
	.pricing-grid2:hover, .pricing-grid2:hover, .pricing-grid3:hover {
    transform: inherit !important;
    -webkit-transform: inherit !important;
    -moz-transform: inherit !important;
    -o-transform: inherit !important;
    -ms-transform:inherit !important;}
	
	.pricing-grid2:hover, .pricing-grid2{ border:none !important; background-color:#f5f5f5 !important;}
	.price-bg{background-color:#f5f5f5 !important;}
	.pricing-grid2 .price-value.two h3 a{ color:#45aaff !important;}
	.price-value.two, .price-value.two, .price-value.two.three{ border-bottom:1px solid #e6e6e6 !important;}
	.pricing-grid2 ul li a, .pricing-grid2 ul li a, .pricing-grid3 ul li a, .pricing-grid2:hover div.price-bg ul li a{ color:#000 !important;}
	.pricing-grid1, .pricing-grid2, .pricing-grid3{-webkit-box-shadow: 2px 2px 7px 0px rgba(0,0,0,0.33);
-moz-box-shadow: 2px 2px 7px 0px rgba(0,0,0,0.33);
box-shadow: 2px 2px 7px 0px rgba(0,0,0,0.33);}

	.pricing-grid2, .pricing-grid2, .pricing-grid3{     transition: inherit !important;
    -webkit-transition: inherit !important;
    -moz-transition:inherit !important;
    -o-transition: inherit !important;}
	.pricing-grid3:hover, .pricing-grid3:hover, .pricing-grid3:hover {
    transform: inherit !important;
    -webkit-transform: inherit !important;
    -moz-transform: inherit !important;
    -o-transform: inherit !important;
    -ms-transform:inherit !important;}
	.price-value.three{ background-color:transparent !important; background:transparent !important; border-top:5px solid #88b53a;}
	.pricing-grid3:hover, .pricing-grid3{ border:none !important; background-color:#f5f5f5 !important;}
	.price-bg{background-color:#f5f5f5 !important;}
	.pricing-grid3 .price-value.three h4 a{ color:#88b53a !important;}
	.price-value.three, .price-value.three, .price-value.three.three{ border-bottom:1px solid #e6e6e6 !important;}
	.pricing-grid3 ul li a, .pricing-grid3 ul li a, .pricing-grid3 ul li a, .pricing-grid3:hover div.price-bg ul li a, .pricing-grid3:hover div.price-value h3 a{ color:#000 !important;}
	#faq_page #v-pills-tabContent .card-header a.collapsed:before{  font-weight:900; font-family: "Font Awesome 5 Free"; display:inline-block; content:'\f067' !important;}
	#faq_page #v-pills-tabContent .card-header a:before{ content:'\f068' !important; font-weight:900; font-family: "Font Awesome 5 Free"; display:inline-block;}
	#related_courcrausal.owl-theme .owl-nav [class*=owl-], #related_courcrausal.owl-theme .owl-nav [class*=owl-]:hover{ background-color:rgba(256,256,256,0.8) !important;}
	#carrer_coursdur strong, #carrer_coursdur span{ font-weight:normal !important;}

	#contact_upage #contact_usec .inner_box .inner_div ul li:nth-child(1):before{ font-weight:900; font-family: "Font Awesome 5 Free"; content:'\f3c5';}
	#contact_upage #contact_usec .inner_box .inner_div ul li:nth-child(2):before{font-weight:900; font-family: "Font Awesome 5 Free"; content:'\f3c5';}
	#contact_upage #contact_usec .inner_box .inner_div ul li:nth-child(3):before{font-weight:900; font-family: "Font Awesome 5 Free"; content:'\f879';}

	#test_monialsec .test_monialsecinner .box p:before{ font-weight:900; font-family: "Font Awesome 5 Free"; content:'\f10d';}
	#test_monialsec .test_monialsecinner .box p:after{ font-weight:900; font-family: "Font Awesome 5 Free"; content:'\f10e';}
#animation_container{ z-index:99 !important;}
.price-value.two h5 lable, .price-value.two h5 span, .price-value.two .title_shop{ color:#000 !important;}
.price-value h5 span, .price-value .title_shop, .price-value.three h5 lable{ color:#000 !important;}
#top_reqruiter_sec hgroup, #contact_upage #contact_usec hgroup, #test_monialsec .test_monialsecinner hgroup{ margin-bottom:60px !important;}
.dreamweaver_det .pricing-grid1, .dreamweaver_det .pricing-grid2, .dreamweaver_det .pricing-grid3{ min-height:346px !important;}
.photo_det .pricing-grid1, .photo_det .pricing-grid2, .photo_det .pricing-grid3{ min-height:356px !important;}
#degree_cors_sec hgroup, #recruiters hgroup{ margin-bottom: 60px !important; }
#animation_container{ right: 0px !important; }
#student_worksec hgroup{ margin-bottom:60px; }
#contact_upage #contact_usec{ padding: 35px 0px; }
header .c-menu__items li.dropdown1 a.dropdown-toggle1:after, header .c-menu__items li.dropdown2 a.dropdown-toggle2:after, header .c-menu__items li.dropdown3 a.dropdown-toggle3:after, header .c-menu__items li.dropdown4 a.dropdown-toggle4:after{font-weight:900; font-family: "Font Awesome 5 Free"; content:'\f067';}
header .c-menu__items li.dropdown1 a.dropdown-toggle1.active:after, header .c-menu__items li.dropdown2 a.dropdown-toggle2.active:after, header .c-menu__items li.dropdown3 a.dropdown-toggle3.active:after, header .c-menu__items li.dropdown4 a.dropdown-toggle4.active:after{font-weight:900; font-family: "Font Awesome 5 Free"; content:'\f068';}
#google_app .app_content{ margin-top:0px;}
header{ background-color:#fff !important; padding-top:0px !important;}
header .header_top_bar{ background-color:#02abe3; position:relative; }
header .header_bottom_bar{ padding-bottom:7px; padding-top:7px;}
#speak_crausal .owl-item p{ width:95% !important;}
.foot_socialico li a{ height:40px; width:40px; border:1px solid #fff; -webkit-border-radius: 50%;
-moz-border-radius: 50%;
border-radius: 50%; margin-left:7px; line-height:46px; text-align:center;}
.foot_socialico li a i{ font-size:22px;}
header .header_top_bar ul li a{ font-size:16px;}
header .header_top_bar ul li .sub-menu  li{ margin:inherit;}
header .header_top_bar ul li:last-child a{ background-color:#ea7327; padding-left:12px; padding-right:12px !important;}
##courses.popular_csec{ padding-bottom:60px !important;}
.head_contact_info a{ color:#000 !important; font-size:16px;}
.head_contact_info  a.text-light:focus, .head_contact_info  a.text-light:hover{ color:#000 !important;}
/* .degree_innerdiv { padding:40px 20px;} */
/* #degree_cors_sec .degree_first{ background-color:#1d70a3;}
#degree_cors_sec .degree_second{ background-color:#ff8265;}
#degree_cors_sec .degree_third{ background-color:#0db2a8;}
#degree_cors_sec .degree_fourth{ background-color:#e371f1;}
#degree_cors_sec .degree_fifth{ background-color:#6eb970;}
#degree_cors_sec .degree_sixth{ background-color:#efa32a;} */
#courses.trnd_course .course_contant .course_tab .course_box:after{ display:none;}
#courses.trnd_course .course_contant .course_tab  .course_icon{ background-color:transparent !important;}
#courses.trnd_course .course_contant  .graphic_color .course_cnt{ border:2px solid #ffb281;}
#courses.trnd_course .course_contant  .sketching_color .course_cnt{ border:2px solid #ca82f1;} 
#courses.trnd_course .course_contant  .photoshop_color .course_cnt{ border:2px solid #7865af;} 
#courses.trnd_course .course_contant  .dreamweaver_color .course_cnt{ border:2px solid #90bc66;} 
#courses.trnd_course .course_contant  .html_color .course_cnt{ border:2px solid #e86ac3;} 
#courses.trnd_course .course_contant  .css_js_color .course_cnt{ border:2px solid #a6a6a6;} 
#courses.trnd_course .course_contant  .bootstrap_color .course_cnt{ border:2px solid #d73c9c;} 
#courses.trnd_course .course_contant   .course_cnt{ border:2px solid #20aaf2;} 
#courses.trnd_course .course_contant .course_tab .course_icon img{ background-color:#fff;}
#courses.trnd_course .course_contant .course_tab .course_box h4{ min-height:44px;}
#courses .course_contant .course_tab  a{ color:#20aaf2;}
#degree_cors_sec{ padding-top:25px !important; padding-bottom:25px !important; background-color:#f7f7f7;}
#student_work { padding-top:25px !important;}
footer .quick_l li{ margin-right:20px !important;}
footer .quick_l li:last-child{ margin-right:0px !important;}
#footer .footbottom{ margin-top:0px !important;}
footer .quick_l{ margin-top:25px;} 
#courses .course_contant .course_tab .course_box.dreamweaver_color .course_icon{ line-height:48px;}
header .header_top_bar ul li{ margin:0px ;}
.whatsapp i{ color:#fff; font-size:20px;}
.whatsapp{ position:fixed; right:0px; top:158px; z-index:99;}
.whatsapp a{ height:36px; width:36px; background-color:#01E675; line-height:40px; text-align:center; display:inline-block; }
header .nav-link{ padding:.5rem 10px !important;}

html{ overflow-x:hidden;}
.open.oppenned{ top:-13px;}
	.open {
	position:relative;
	top:-7px;
	right:9px;
	width:50px;
	height:50px;
	display:block;
	cursor:pointer;
	transition:opacity 0.2s linear;
	z-index:9999
}
.open:hover {
	opacity:0.8
}
.open span {
	display:block;
	float:left;
	clear:both;
	height:3px;
	width:32px;
	border-radius:40px;
	background-color:#fff;
	position:absolute;
	right:3px;
	top:3px;
	overflow:hidden;
	transition:all 0.4s ease
}
.open span:nth-child(1) {
margin-top:14px;
z-index:9
}
.open span:nth-child(2) {
margin-top:24px
}
.open span:nth-child(3) {
margin-top:34px
}
.open.oppenned span:nth-child(3) {
margin-top:45px
}
.sub-menu {
	transition:all 0.8s cubic-bezier(0.68, -0.55, 0.265, 1.55);
	height:0;
	width:0;
	left:0;
	top:0;
	position:absolute;
	background-color:rgba(7, 153, 201, 0.9);
	border-radius:50%;
	z-index:18;
	overflow:hidden
}
.sub-menu li {
	display:block;
	float:left;
	clear:both;
	margin-bottom:1px;
	height:auto;
	margin-right:-160px;
	transition:all 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55)
}
.sub-menu li:first-child {
	margin-top:180px
}
.sub-menu li:first-child>ul>li{ margin-top:0px;}
.oppenned li>ul>li{ margin-left:15px;}
.sub-menu li:nth-child(1) {
-webkit-transition-delay:0.05s
}
.sub-menu li:nth-child(2) {
-webkit-transition-delay:0.10s
}
.sub-menu li:nth-child(3) {
-webkit-transition-delay:0.15s
}
.sub-menu li:nth-child(4) {
-webkit-transition-delay:0.20s
}
.sub-menu li:nth-child(5) {
-webkit-transition-delay:0.25s
}
.sub-menu li a {
	color:#fff;
	font-family:'Lato', Arial, Helvetica, sans-serif;
	font-size:16px;
	width:100%;
	display:block;
	float:none;
	line-height:inherit;
}
header .header_top_bar ul li ul li:last-child a{ background-color:transparent;}
.oppenned .sub-menu {
	opacity:1;
	height:545px;
	width:400px
}
.oppenned span:nth-child(2) {
overflow:visible
}
.oppenned span:nth-child(1), .oppenned span:nth-child(3) {
z-index:100;
-webkit-transform:rotate(45deg);
transform:rotate(45deg)
}
.oppenned span:nth-child(1) {
-webkit-transform:rotate(45deg) translateY(12px) translateX(12px);
transform:rotate(45deg) translateY(12px) translateX(12px)
}
.toggle_submenu_ul{ display:none;}

.oppenned span:nth-child(2) {
height:530px;
width:400px;
right:-160px;
top:-160px;
border-radius:50%;
background-color:rgba(7, 153, 201, 0.9)
}
.oppenned span:nth-child(3) {
-webkit-transform:rotate(-45deg) translateY(-10px) translateX(10px);
transform:rotate(-45deg) translateY(-10px) translateX(10px)
}
.oppenned li {
	margin-left:202px !important; text-align:left;
}
.oppenned li:first-child{ margin-top:185px !important;}
header .toogle_menu_velvet{ position:absolute; left:0px;  top:0px; z-index:99; background-color:#0799c9; bottom:0px; display:inline-block;}
.header.content{ position: static !important;} 
#social_icons{ top:187px !important;}
#modual_sec>div>div{ text-align:justify;}
#carrer_coursdur span{  vertical-align:top;}

.place_testimonial{
    text-align: center;
    padding: 10px; padding-top:50px;
    margin: 70px 15px 35px;
    background: #f9f9f9;
    box-shadow: 8px 4px 0 0 #5cb6ff;
    position: relative;
}
.place_testimonial .pic{
    width: 200px;
    height: auto;
    border: 3px solid #5cb6ff;
    margin: 0 auto;
    position: absolute; padding:5px; line-height:41px; background-color:#fff;
    top: -40px;
    left: 0;
    right: 0;
}
.place_testimonial .pic img{
    width: 100%;
    height: auto;
}
.place_testimonial .description{
    font-size: 14px; text-align:justify;
    color: #757575;
    line-height: 27px;
    margin-bottom: 20px;
    position: relative;
}
.place_testimonial .description:before{
    content: "\f10d";
    font-family: "FontAwesome";
    font-size: 32px; display:none;
    color: #5cb6ff;
    position: absolute;
    top: -15px;
    left: -35px;
}
.place_testimonial .place_testimonial-profile{
    position: relative;
    margin: 20px 0 10px 0;
}
.place_testimonial .place_testimonial-profile:after{
    content: "";
    width: 50px;
    height: 2px;
    background: #5cb6ff;
    margin: 0 auto;
    position: absolute;
    bottom: -10px;
    left: 0;
    right: 0;
}
.place_testimonial .title{
    display: block;
    font-size: 16px;
    color: #5cb6ff;
    letter-spacing: 1px;
    text-transform: uppercase;
    margin: 0;
}
.place_testimonial .post{
    display: block;
    font-size: 14px;
    color: #757575;
    text-transform: capitalize;
}
#placement_page #latest_plac .table td, #placement_page #latest_plac .table thead th{ font-size:14px !important;}
.table td, .table th{ padding:6px !important;}
#contact_upage #contact_usec .inner_box .inner_div ul li:before{ font-size:18px !important;}
a.g_direction{ background-color:#ea7327 !important;}
#contact_upage #contact_usec .inner_box .inner_div{ position:relative;}
#contact_upage #contact_usec .inner_box .inner_div a.g_direction{ position:absolute; bottom:20px;}
/* .place_testsec{ height:500px;} */
marquee{-moz-animation-play-state: paused;
-webkit-animation-play-state: paused;
animation-play-state: paused;}
 #speak_crausal .owl-item strong{ text-transform:capitalize;}
 #modual_sec .modual_div p{ margin-top:10px;}
 #modual_sec .modual_div ul li a{     text-align: left;
  
    font-size: 15px;
    color: #58595b;
    padding-left: 20px;
    position: relative;
    padding-right: 20px;}
.price-value.two{ background-color:transparent !important; background:transparent !important; border-top:5px solid #45aaff;}
.courses_msecmenu{/*position: absolute; left: 0px; right: 0px; top: 0px; z-index: 999;*/ display:none;}
header .courses_msecmenu{ display:block;}
body.body_hide{ overflow:hidden;}
.courses_msecmenu .courses_msecmenu_inner{  background-color:#02abe3;  }
/*.courses_msecmenu .courses_msecmenu_inner{ overflow-y:scroll;  position: relative; background-color:#02abe3; padding:50px 0px 20px;  height:100vh; }*/
/*.courses_msecmenu .courses_msecmenu_inner .courses_msecmenu_close{ background-color:#0793c2; position: absolute; right: 0px; top: 0px; height: 40px; width: 40px; line-height: 40px; text-align: center;  background-color: ##02ABFB;}*/
/*.courses_msecmenu .courses_msecmenu_inner .courses_msecmenu_close i{ font-size: 30px; color: #fff; line-height:39px;}
*/.courses_msecmenu a{ font-size:16px; color:#fff; text-align:left;}
.courses_msecmenu .col-md-3 a{ padding:10px 6px 10px 6px; display:block; font-weight:bold;}
.courses_msecmenu .col-md-9 ul li{ float:left; width:25%;  padding:0px 14px; margin-bottom:15px;}
.courses_msecmenu .col-md-9 ul:after{ content:''; clear:both; display:block;}
.courses_msecmenu a i{ display: block; text-align: center; font-size: 45px; color: #fff; margin-bottom: 7px; }
.courses_msecmenu .col-md-9 ul li a{ display:block; text-align:center !important;}
.courses_msecmenu .col-md-9 ul li a{    border: 1px solid rgba(255,255,255,0.3);
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    -ms-border-radius: 10px;
    border-radius: 10px;
    padding: 10px;
    -webkit-box-shadow: 0px 0px 20px 0px rgba(177,3,153,0.2);
    -moz-box-shadow: 0px 0px 20px 0px rgba(177,3,153,0.2);
    box-shadow: 0px 0px 20px 0px rgba(177,3,153,0.2); line-height:18px; min-height:136px;
}
.courses_msecmenu .col-md-9 ul li a img{ margin-bottom:12px;}
.courses_msecmenu .col-md-3 a{ position: relative;  font-size: 17px;}
/*.courses_msecmenu .col-md-3 a:after{ content: ''; display: block; position: absolute; left: 0px; top: calc(100% - 8px); height: 2px; width: 50px; background-color: #fff;  -o-transition:.5s;
-ms-transition:.5s;
-moz-transition:.5s;
-webkit-transition:.5s;}
.courses_msecmenu .col-md-3 a:hover:after, .courses_msecmenu .col-md-3 a.active:after{ width: 100%; }*/
.courses_msecmenu .col-md-3 a{ border-bottom:1px solid rgba(255,255,255,0.3);}
.courses_msecmenu .col-md-3 a.active{ background-color:rgba(255,255,255,0.3);}
.modal .close span{ font-size: 40px; display: inline-block; }



    @-ms-viewport {
	width: device-width;
}
@viewport {
	width: device-width;
}
@media(max-width:1500px){
.pricing-grid1, .pricing-grid2, .pricing-grid3{ min-height:590px !important;}
}
@media(max-width:1300px){
header .nav-link{ padding:.5rem 6px !important;}
}
@media(max-width:1200px){
header .header_top_bar ul li a{ font-size:14px;}
}

@media(max-width:1069px){
	/* header .header_top_bar ul li a i{ display:none;} */
	header .nav-link{ padding:.5rem 9px !important;}

}
@media(max-width:992px){
	.place_testimonial .description{  -moz-text-align-last: center; /* For Firefox prior 58.0 */
  text-align-last: center;}
	header .all_courses{ position:absolute; left:0px; right:0px; text-align:center; z-index:9;}
.request_call{ position:relative; z-index:9;}
header .header_top_bar ul li .sub-menu li{ width:100%; display:block;}
header .header_top_bar ul li .sub-menu li i{ display:inline-block;}
	#call_action a{ font-size:16px;}
	#call_action a i{ font-size:22px;}
	#call_action a { padding:10px 20px;}
	header .header_top_bar ul li:last-child a{ margin-right:-15px;}
	#animation_container{ display: none; }
#google_app .app_content{ margin-top: 40px; }
#testimonials{ padding: 70px 30px; }
#speak_crausal{ width: 95%; }
hgroup h6{ font-size: 60px !important; top: 5px !important; }

}

@media(max-width: 767px){
.courses_msecmenu .col-md-9 ul li a img { width: 48px; }
	header .modal .tab-content ul li{ width:33.333%;}
	#footer{ padding-top:20px;}
	#degree_cors_sec .degree_innerdiv{ margin-bottom:0px !important;}
		#courses.trnd_course .course_contant .course_tab .course_box{ border:none !important;}
	body{ padding-top:155px !important;}
#google_app .app_content{ margin-top:0px;}
#degree_cors_sec hgroup, #recruiters hgroup{ margin-bottom:30px !important; }
	#recruiter_logo .r_logos{ margin-top: 30px !important; }

.main_grouphead h5, hgroup h6, hgroup h5{ display: none !important; }
.why_daac_points{ padding-top:0px !important; }
hgroup{ margin-bottom:20px !important; }
hgroup h2{ margin-top: 0px !important; }
.intro_video2{webkit-box-shadow: 10px 10px 0px 0px rgba(2,171,227,0) !important;
    -moz-box-shadow: 10px 10px 0px 0px rgba(2,171,227,0) !important;
    box-shadow: 10px 10px 0px 0px rgba(2,171,227,0) !important; border: 5px solid #02abe3; margin:auto;
}

#grow_careers, #courses, #recruiters, #testimonials, #student_work, #google_app{ padding: 30px 0px; }
footer ul li a i{ font-size: 18px; display: inline-block; }
#google_app .order-2 img{ margin-bottom: -100px; }
footer .text-left, footer p, #footer h6{ text-align: center !important; }
#footer h6{ font-size: 18px; }
footer ul.d-flex{ display: block !important; }
footer ul.d-flex li{ display: inline-block !important; }
footer .footbottom p{ text-align: justify !important; }
	#google_app img.footer_design{ display: none; }

#google_app img{ margin-bottom: -50px; margin-top:35px;}
#google_app .app_content{ text-align: center; margin-bottom: 25px; }
.hulk_img{ display: none; }
hgroup h6 {
    font-size: 50px !important;
    top: 16px !important;}
#courses .course_contant .course_tab .course_box.graphic_color:after {
	display: none;
}
#courses .course_contant .course_tab .course_box.graphic_color {
 border: 5px solid #ffb281;
}

#courses .course_contant .course_tab .course_box.sketching_color:after {
	display: none;
}
#courses .course_contant .course_tab .course_box.sketching_color {
border: 5px solid #ca82f1;
}

#courses .course_contant .course_tab .course_box.photoshop_color:after {
display: none;
}

#courses .course_contant .course_tab .course_box.photoshop_color {
border: 5px solid #7865af;
}


#courses .course_contant .course_tab .course_box.dreamweaver_color:after {
display: none;
}

#courses .course_contant .course_tab .course_box.dreamweaver_color {
border: 5px solid #90bc66;
}


#courses .course_contant .course_tab .course_box.html_color:after {
display: none;
}
#courses .course_contant .course_tab .course_box.html_color {
border: 5px solid #e86ac3;
}


#courses .course_contant .course_tab .course_box.css_js_color:after {
display: none;
}

#courses .course_contant .course_tab .course_box.css_js_color {
border: 5px solid #a6a6a6;
}


#courses .course_contant .course_tab .course_box.bootstrap_color:after {
display: none;
}
#courses .course_contant .course_tab .course_box.bootstrap_color {
border: 5px solid #d73c9c;
}
#courses .course_contant .course_tab .course_box:after{ display: none; }
#courses .course_contant .course_tab .course_box{ border: 5px solid #20aaf2; }
/*Courses End*/
header{ z-index: 999 !important; }
#top_reqruiter_sec ul li:nth-child(1n+11){ display: none; }
header .header_mid .text-center img{ width: 100px !important; }
.lightboxOverlay{ z-index: 9999999 !important; }
	.lightbox{ z-index: 9999999 !important; }
	.lb-outerContainer{ width: 38% !important; height: auto !important;  }
	.lightbox .lb-image{ width: 100% !important; height: auto !important; }
#modual_sec p{ text-align: justify; }
.modal-backdrop.show{ display:none;}
.modal-open .modal{ background-color:rgba(0,0,0,.8);}
}
@media (max-width: 643px) {
#footer .footbottom li{ margin-right:15px;}
}
@media (max-width: 579px) {
.table-responsive .table{ width:580px;}
#placement_page #latest_plac .table td, #placement_page #latest_plac .table thead th{ font-size:12px !important;}
	#placement_page #latest_plac .table-responsive{ margin-top:0px;}
	#modual_sec .modual_div{ padding:10px 0px !important;}
	.main_grouphead h2{ font-size:24px !important;}
	#student_work .nav-link{ min-width:inherit;}
	#footer a { padding:0px 2px; font-size:10px;}
	header .modal .tab-content ul li{ width:50%;} 
	#speak_crausal .owl-item p{ margin-top:0px !important;}
	#degree_cors_sec .degree_innerdiv{ margin-bottom:40px !important;}
	#degree_cors_sec .col-12:last-child .degree_innerdiv{ margin-bottom:0px !important;}
	header .sub-menu  .nav-link { padding:5px 9px !important;}
#recruiter_logo .r_logos li img{ width: 150px !important; height: 43px; }
#recruiter_logo .r_logos li{ padding: 10px; height: 74px !important;}
hgroup h6{ display: none; }
#carrer_coursdur h3{ font-size: 20px; }
#carrer_coursdur span, #carrer_coursdur strong{ width: auto; float: none; display: inline-block;}
#carrer_coursdur strong{ margin-right:0px; width: auto;}
#carrer_coursdur span{ font-weight: normal; vertical-align:top;}
#carrer_coursdur strong:after{ display: none !important; }
.owl-theme .owl-nav{ display: none; }
#student_work .nav-link, #student_work .nav-link.active{ font-size: 10px; margin: 0px; padding: 5px;     border-radius: 0px; }
#student_work .nav-item{ float: left; width: 25%; }
hgroup h2{ font-size: 24px; }
#google_app .app_content, #footer .footbottom{ margin-top: 0px; }
#call_action a{ padding: 10px 15px; }
#speak_crausal{ width: 100%; }
#call_action p{ line-height: inherit; margin-bottom: 15px; }
#call_action{ margin-bottom: 1px; }

.logo_section .col-6 a{ width:188px !important; display: block; margin: auto;} 
hgroup h6 {
    font-size: 46px !important;
    top: 16px !important;}
#courses .row .col-6{     -ms-flex: 0 0 100%;
    flex: 0 0 100%;
    max-width: 100%; }
    #call_action a i{ display: none; }
#slider{ margin-top: 0px !important; }
  .pricing-grid1:hover, .pricing-grid2:hover, .pricing-grid3:hover{    transform: scale(1) !important;
    -webkit-transform: scale(1) !important;
    -moz-transform: scale(1) !important;
    -o-transform: scale(1) !important;
    -ms-transform: scale(1) !important;}
    .pricing-grid1 ul li a, .pricing-grid2 ul li a, .pricing-grid3 ul li a{ padding: 5px 0px !important; }
    .price-value h2 a, .price-value.two h3 a, .price-value.three h4 a { font-size: 22px !important; }
    .pricing-grid1, .pricing-grid2, .pricing-grid3{ min-height: inherit !important; height: auto; }
    .price-value h2, .price-value.two h3, .price-value.three h3{ margin-top: 0px !important; }
     .price-value h5, .price-value.two h5, .price-value.three h5{ margin-bottom: 0px !important; }
    .price-value, .price-value.two, .price-value.three{ padding: 10px 0px !important; }

#related_courcrausal.owl-carousel .owl-nav .owl-prev, #related_courcrausal.owl-carousel .owl-nav .owl-next{ top: 19% !important; }
#our_facltypro, #related_coursediv, #course_vidsec{ padding: 25px 0px !important; }
#related_courcrausal { margin-top: 20px; }
#our_facltypro .row .col-md-4:last-child .faclty_probox{ margin-bottom: 0px; }
#carrer_coursdur i{ display: none; }
#carrer_coursdur .inner_div{ padding: 0px !important; }
#carrer_coursdur span{ font-size: 15px; }
#student_work_sec .nav-pills .nav-item{ width: 25%; float: left; }
#carrer_coursdur .row .col-md-4{ margin-bottom: 15px; }
#carrer_coursdur strong{ font-size: 16px; }
#carrer_coursdur span{ font-size: 15px; }
#student_work_sec .nav-pills .nav-link{font-size: 10px;
    padding: 8px 0px;}
#contacttop_banner{ height: 150px !important; }
.lb-outerContainer { width: 80% !important; }
#speak_crausal{ width: 100%; margin: auto; }
#speak_crausal .owl-item p{ width: 95%; }
#speak_crausal .owl-nav .owl-prev{ left: -35px; }
#speak_crausal .owl-nav .owl-next{ right: -35px; }
#top_reqruiter_sec, #student_work_sec, #speaker_sec{ padding: 20px 0px; }
#frenchisee_sec{ padding-bottom: 0px !important; }
h3.heading{ background: none; padding: 0px; height: auto;}
#carrer_coursdur strong, #carrer_coursdur span{ width:auto !importtant; display:block; text-align:center;}
#carrer_coursdur strong{ margin-bottom:0px !important;}
#carrer_coursdur span{ margin-bottom:10px;}
.intro_video iframe{ height:180px;}
hgroup h2{ margin:0px !important; }
#student_work hgroup{ margin-bottom:30px;}
.owl-theme .owl-nav.disabled+.owl-dots{ display:none;}

}
@media (max-width: 575px){
	#carrer_coursdur h3{ text-align:center;}
	#carrer_coursdur strong{ font-weight:bold !important;}
	body{ padding-top:97px !important;}
	header .head_contact_info .nav-link{ margin:8px 5px 0px !important;}
#recruiter_logo .r_logos li{ height: 74px !important; }
#courses .course_contant .course_tab .course_box{ margin-top:60px;}
#courses hgroup{ margin-top:30px; margin-bottom:0px !important;}
#courses.trnd_course hgroup{ margin-top:0px !important;}
#courses.trnd_course .course_contant .course_tab .course_box h4 {
    min-height: inherit;}
}
@media (max-width: 480px){
	header .all_courses{ left:35px;}
	.all_courses i{ display:none;}
	.open{    top: -4px;
    right: 6px;
    width: 39px;}
	.open span{ width:24px;}
	.open span:nth-child(3){ margin-top:28px;}
	.open span:nth-child(2){ margin-top:21px;}
	header .all_courses{
    right: inherit;}
	.modal-open .modal .col-5, .modal-open .modal .col-7{ width:100% !important; -ms-flex: 0 0 100% !important;
    flex: 0 0 100% !important;
    max-width: 100% !important;
    width: 100% !important;}
	#header .modal-body .leftbg{ height:auto; min-height: inherit;}
	#footer .footbottom li{ margin-right:0px;}
	.footer_copyright{ margin-top:0px;}
header .header_bottom_bar .col-2{ display:none !important;}
header .header_bottom_bar .col-5{     -ms-flex: 0 0 50% !important;
    flex: 0 0 50% !important;
    max-width: 50% !important; width:50% !important;}

.logo_section .col-6 a{-moz-transform: scale(0.8);
-webkit-transform: scale(0.8);
-o-transform: scale(0.8);
-ms-transform: scale(0.8);
transform: scale(0.8);} 
#slider{ margin-top: 0px !important; }
	#test_monialsec .test_monialsecinner .box{ padding: 20px 20px; }

#test_monialsec .test_monialsecinner .box p:before, #test_monialsec .test_monialsecinner .box p:after{ display: none; }
}

@media (max-width: 400px){
	.whatsapp { top:152px;}
	header .header_bottom_bar .col-5{     -ms-flex: 0 0 60% !important;
    flex: 0 0 60% !important;
    max-width: 60% !important; width:60% !important;}
	header .header_bottom_bar .col-5.align-self-center2{     -ms-flex: 0 0 40% !important;
    flex: 0 0 40% !important;
    max-width: 40% !important; width:40% !important;}
#recruiters{ display: none; }
#slider{ margin-top: 0px !important; }
}



/* Retina-specific styles. */
@media print,
	(-o-min-device-pixel-ratio: 5/4),
	(-webkit-min-device-pixel-ratio: 1.25),
	(min-resolution: 120dpi) {

}

@media print {

}
</style>

<link href="<?php echo $this->webroot; ?>css2/component.css" rel="stylesheet" type="text/css">
 
<link href="<?php echo $this->webroot; ?>css2/responsive.css" rel="stylesheet" type="text/css">

<script src="<?php echo $this->webroot; ?>js/jquery-slim.min.js" type="text/javascript"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-3229371-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-3229371-3');
</script>


<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '136734870371777');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=136734870371777&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>



<body onload="init();">


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119963095-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-119963095-1');
</script>

 

<div id="page-wrapper">

<section id="social_icons">
<ul class="list-unstyled">
<li><a href="https://www.facebook.com/DAACJAIPUR" class="face" target="_blank"><i  class="fab fa-facebook-f"></i><span>Facebook</span>
</a></li>
<li><a href="https://twitter.com/#!/DaacAcademy" class="twit" target="_blank"><i class="fab fa-twitter"></i><span> Twitter</span>
</a></li>
<li><a href="https://www.instagram.com/daacjaipur/" class="insta" target="_blank"><i class="fab fa-instagram"></i><span>Instagram</span>
</a></li>
<li><a href="http://www.youtube.com/c/DAACJaipur" class="u_tube" target="_blank"><i  class="fab fa-youtube"></i><span>Youtube</span>
</a></li>
<li><a href="http://www.linkedin.com/company/daac-doomshell-academy-of-advance-computing-" class="linkd" target="_blank"><i class="fab fa-linkedin-in"></i><span>Linkdin</span>
</a></li>
<li><a href="https://plus.google.com/u/0/+DAACJaipur1" class="g_plus" target="_blank"><i class="fab fa-google-plus-g"></i><span>Google plus</span>
</a></li>
<li><a href="https://in.pinterest.com/DaacInstitute/" class="pin_trest" target="_blank"><i class="fab fa-pinterest-p" aria-hidden="true"></i>

</a></li>
</ul>
</section><!--social_icons-->

<!--<section id="address">
<?php $branch=$this->Common->getbranches();?>
<div class="container"><div class="row">




<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-right">

</div>
</div></div>
</section>-->

<?php $pname=$_SERVER['REQUEST_URI']; 
//print_r($this->request->params);
  $control=$this->request->params['controller'];
  $action=$this->request->params['action'];
   ?>

   <header id="header">

      <div class="header_bottom_bar">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-4 col-5 align-self-center2 align-self-center">
              <div class="logo"> <a href="<?php echo SITE_URL; ?>"> <img src="<?php echo $this->webroot; ?>images/logo.png" alt="logo" class="img-fluid"> </a> </div>
            </div>
            <div class="col-sm-4 col-2 align-self-center">
              <div class="glorious_logo text-center"> <img src="<?php echo $this->webroot; ?>images/glorius13.png" alt="glorious_logo" class="img-fluid glorious_logo"> </div>
            </div>
            <div class="col-sm-4 col-5 align-self-center">
              <div class="head_contact_info text-right align-self-center">
                <li class="nav-item request_call"> <a href="#" class="nav-link text-light pr-0 mb-3 mt-3"> <span class="d-none d-sm-inline-block">Call :</span> 8764122221 </a> </li>

			    <li>
				<ul class="list-inline text-right head_top_social">
<li class="list-inline-item"><a href="https://www.facebook.com/DAACJAIPUR" class="face" target="_blank"><i class="fab fa-facebook-f"></i>
</a></li>
<li class="list-inline-item"><a href="https://twitter.com/#!/DaacAcademy" class="twit" target="_blank"><i class="fab fa-twitter"></i>
</a></li>
<li class="list-inline-item"><a href="https://www.instagram.com/daacjaipur/" class="insta" target="_blank"><i class="fab fa-instagram"></i>
</a></li>
<li class="list-inline-item"><a href="http://www.youtube.com/c/DAACJaipur" class="u_tube" target="_blank"><i class="fab fa-youtube"></i>
</a></li>
<li class="list-inline-item"><a href="http://www.linkedin.com/company/daac-doomshell-academy-of-advance-computing-" class="linkd" target="_blank"><i class="fab fa-linkedin-in"></i>
</a></li>
<li class="list-inline-item"><a href="https://plus.google.com/u/0/+DAACJaipur1" class="g_plus" target="_blank"><i class="fab fa-google-plus-g"></i>
</a></li>
<li class="list-inline-item"><a href="https://in.pinterest.com/DaacInstitute/" class="pin_trest" target="_blank"><i class="fab fa-pinterest-p"></i>
</a></li>
</ul>
				</li>
                
                <!-- /scroller --> 
              </div>
            </div>
            <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-xl">
                <div class="modal-content rounded-0">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Our Courses</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                  </div>
                  <div class="modal-body">
                    <div class="courses_msecmenu">
	<div class="courses_msecmenu_inner">
<!-- 	<a href="javascript:void(0);" class="courses_msecmenu_close"><i class="fas fa-times"></i></a>
 --><div>

<div class="row">
<div class="col-md-3 col-sm-4 col-5">
<div class="leftbg">
<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
<a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Designing</a>
<a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Development</a>
<a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Mobile Development</a>
<a class="nav-link" href="https://daac.in/digital-marketing.html">Digital Marketing</a>
<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Accounting </a>
<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-course5" role="tab" aria-controls="v-pills-course5" aria-selected="false">Degree Courses</a>
<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-course6" role="tab" aria-controls="v-pills-course6" aria-selected="false">Advance Courses</a>
<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-course7" role="tab" aria-controls="v-pills-course7" aria-selected="false">Security &amp; Testing </a>
<a class="nav-link" href="https://daac.in/basic-computer-training-jaipur.html">Basic Computer</a>

</div>
</div>
</div>
<div class="col-md-9 col-sm-8 col-7">
<div class="tab-content" id="v-pills-tabContent">
<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
<ul class="list-unstyled">
<li><a href="https://daac.in/photo-shop.html">
<div><img src="<?php echo $this->webroot; ?>images/designing_1.png"></div>
<span>Photoshop</span></a></li>
<li><a href="https://daac.in/graphic-desigining.html">
<div><img src="<?php echo $this->webroot; ?>images/designing_2.png"></div>
<span>Graphic Designing</span> </a></li>
<li><a href="https://daac.in/dreamweaver-diploma.html">
<div><img src="<?php echo $this->webroot; ?>images/designing_3.png"></div>
<span>Dreamweaver</span></a></li>
<li><a href="https://daac.in/web-designing-diploma-2.html">
<div><img src="<?php echo $this->webroot; ?>images/designing_4.png"></div>
UI &amp; UX Designing</a></li>
<li>
<a href="https://daac.in/3d-Animation-jaipur.html">
<div><img src="<?php echo $this->webroot; ?>images/designing_5.png"></div>
3D Animation</a></li>
</ul>
</div>
<div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
<ul class="list-unstyled">
<li><a href="https://daac.in/web-development-diploma-2.html">
<div><img src="<?php echo $this->webroot; ?>images/develop_1.png"></div>
Web Development</a></li>
<li><a href="https://daac.in/c-program.html">
<div><img src="<?php echo $this->webroot; ?>images/develop_2.png"></div>
C</a></li>
<li><a href="https://daac.in/cplus-program.html">
<div><img src="<?php echo $this->webroot; ?>images/develop_3.png"></div>
C++</a></li>
<li><a href="https://daac.in/core-java.html">
<div><img src="<?php echo $this->webroot; ?>images/develop_45.png"></div>
Core Java</a></li>
<li><a href="https://daac.in/java-training-jaipur.html">
<div><img src="<?php echo $this->webroot; ?>images/develop_45.png"></div>
Advance Java</a></li>
<li><a href="https://daac.in/php-training-jaipur.html">
<div><img src="<?php echo $this->webroot; ?>images/develop_6.png"></div>
Php</a></li>
<li><a href="https://daac.in/cake-training-jaipur.html">
<div><img src="<?php echo $this->webroot; ?>images/develop_7.png"></div>
Cakephp</a></li>
<li><a href="https://daac.in/wordpress-training-jaipur.html">
<div><img src="<?php echo $this->webroot; ?>images/develop_8.png"></div>
Wordpress</a></li>
<li><a href="https://daac.in/angular-js.html">
<div><img src="<?php echo $this->webroot; ?>images/develop_9.png"></div>
Angular JS</a></li>
<li><a href="https://daac.in/mysql.html">
<div><img src="<?php echo $this->webroot; ?>images/develop_10.png"></div>
MySQL</a></li>
<li><a href="https://daac.in/drupal.html">
<div><img src="<?php echo $this->webroot; ?>images/develop_11.png"></div>
Drupal</a></li>
<li><a href="https://daac.in/laravel.html">
<div><img src="<?php echo $this->webroot; ?>images/develop_12.png"></div>
Laravel</a></li>
<li><a href="https://daac.in/dotnet-training-jaipur.html">
<div><img src="<?php echo $this->webroot; ?>images/develop_13.png"></div>
.Net</a></li>
</ul>
</div>
<div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
<ul class="list-unstyled">
<li><a href="https://daac.in/advance-mobile-development.html">
<div><img src="<?php echo $this->webroot; ?>images/mobile_1.png"></div>
Mobile Development</a></li>
<li><a href="https://daac.in/mobile-application-development.html">
<div><img src="<?php echo $this->webroot; ?>images/mobile_2.png"></div>
Android </a></li>
<li><a href="https://daac.in/iphone-training-jaipur.html">
<div><img src="<?php echo $this->webroot; ?>images/mobile_3.png"></div>
Iphone</a></li>
<li><a href="https://daac.in/react-native.html">
<div><img src="<?php echo $this->webroot; ?>images/mobile_4.png"></div>
React Native</a></li>
<li><a href="https://daac.in/ionic-angularjs4-training-jaipur.html">
<div><img src="<?php echo $this->webroot; ?>images/mobile_5.png"></div>
Ionic</a></li>
<li><a href="https://daac.in/oracle.html">
<div><img src="<?php echo $this->webroot; ?>images/mobile_6.png"></div>
Oracle Apps</a></li>
</ul>
</div>
<div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
<ul class="list-unstyled">
<li><a href="https://daac.in/tally.html">
<div><img src="<?php echo $this->webroot; ?>images/accounting_1.png"></div>
Tally</a></li>
<li><a href="https://daac.in/bankingandfinance.html">
<div><img src="<?php echo $this->webroot; ?>images/accounting_2.png"></div>
Banking and Finance</a></li>
<li><a href="https://daac.in/accountingprofessional.html">
<div><img src="<?php echo $this->webroot; ?>images/accounting_3.png"></div>
Accounting Professional</a></li>
</ul>
</div>
<div class="tab-pane fade" id="v-pills-course5" role="tabpanel" aria-labelledby="v-pills-settings-tab">
<ul class="list-unstyled">
<li><a href="https://daac.in/bca-course.html">
<div><img src="<?php echo $this->webroot; ?>images/degree_cmenu.png"></div>
BCA</a></li>
<li><a href="https://daac.in/mca-course.html">
<div><img src="<?php echo $this->webroot; ?>images/degree_cmenu.png"></div>
MCA</a></li>
<li><a href="https://daac.in/bba-course.html">
<div><img src="<?php echo $this->webroot; ?>images/degree_cmenu.png"></div>
BBA</a></li>
<li><a href="https://daac.in/mba-course.html">
<div><img src="<?php echo $this->webroot; ?>images/degree_cmenu.png"></div>
MBA</a></li>
<li><a href="https://daac.in/bsc-animation-course.html">
<div><img src="<?php echo $this->webroot; ?>images/degree_cmenu.png"></div>
BSC Animation</a></li>
<li><a href="https://daac.in/msc-animation-course.html">
<div><img src="<?php echo $this->webroot; ?>images/degree_cmenu.png"></div>
MSC Animation</a></li>
</ul>
</div>
<div class="tab-pane fade" id="v-pills-course6" role="tabpanel" aria-labelledby="v-pills-settings-tab">
<ul class="list-unstyled">
<li><a href="https://daac.in/big-data.html">
<div><img src="<?php echo $this->webroot; ?>images/advance_1.png"></div>
Big Data </a></li>
<li><a href="https://daac.in/salesforce.html">
<div><img src="<?php echo $this->webroot; ?>images/advance_2.png"></div>
Salesforce</a></li>
<li><a href="https://daac.in/block-chain.html">
<div><img src="<?php echo $this->webroot; ?>images/advance_3.png"></div>
Block Chain</a></li>
<li><a href="https://daac.in/python-Devlopment.html">
<div><img src="<?php echo $this->webroot; ?>images/advance_4.png"></div>
Python</a></li>
<li><a href="https://daac.in/amazon-aws.html">
<div><img src="<?php echo $this->webroot; ?>images/advance_5.png"></div>
Amazon Aws</a></li>
<li><a href="https://daac.in/linux-training-jaipur.html">
<div><img src="<?php echo $this->webroot; ?>images/advance_6.png"></div>
Linux</a></li>

<li><a href="https://daac.in/autocad.html">
<div><img src="<?php echo $this->webroot; ?>images/advance_7.png"></div>
AutoCAD</a></li>
</ul>
</div>
<div class="tab-pane fade" id="v-pills-course7" role="tabpanel" aria-labelledby="v-pills-settings-tab">
<ul class="list-unstyled">
<li><a href="https://daac.in/ethicalhacking.html">
<div><img src="<?php echo $this->webroot; ?>images/security_1.png"></div>
Ethical Hacking </a></li>
<li><a href="https://daac.in/penetrationtesting.html">
<div><img src="<?php echo $this->webroot; ?>images/security_2.png"></div>
Penetration Testing</a></li>
<li><a href="https://daac.in/software-quality-testing.html">
<div><img src="<?php echo $this->webroot; ?>images/security_3.png"></div>
SQT</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
	  <div class="header_top_bar">
        <div class="container-fluid">

		


          <ul class="nav d-flex ">
		  <li class="toggle_li d-block d-lg-none">
		  <div class="toogle_menu_velvet">
<div class="open">

	<span class="cls"></span>
	<span>      <div class="clos_icomenu"><i class="fas fa-times"></i></div>
		<ul class="sub-menu ">
		<li class="nav-item"> <a class="nav-link active text-light pl-0" href="<?php echo SITE_URL; ?>"><i class="fas fa-home"></i> Home</a> </li>
			<li class=" nav-item list-inline-item"><a class="nav-link active text-light" href="<?php echo SITE_URL; ?>about.html"><i class="fas fa-info-circle"></i> About</a></li>
			<li class="nav-item"> <a class="nav-link text-light" href="<?php echo SITE_URL; ?>static/team"><i class="fas fa-users"></i> Our Team</a> </li>
			<li class="nav-item list-inline-item"><a class="nav-link active text-light" href="<?php echo SITE_URL; ?>students-testimonials.html"><i class="fas fa-bullhorn"></i> Alumni Speaks</a></li>
			<li class="nav-item"> <a class="nav-link text-light" href="<?php echo SITE_URL; ?>placements-daac.html"><i class="fas fa-user-check"></i> Placements</a> </li>
			<li class="nav-item list-inline-item"><a class="nav-link active text-light" href="<?php echo SITE_URL; ?>contact.html"><i class="fas fa-map-marker-alt"></i> Our Branches</a></li>
			<li class="nav-item list-inline-item"><a class="nav-link active text-light" href="<?php echo SITE_URL; ?>faq.html"><i class="far fa-question-circle"></i> FAQ</a></li>
			<li class="nav-item"> <a class="nav-link text-light" href="http://blog.daac.in/"><i class="fab fa-blogger-b"></i> Blog</a> </li>


		</ul>
	</span>
	<span class="cls"></span>
</div>
		</div>
		  </li>
		  <li class="nav-item all_courses d-inline-block d-lg-none"> <a href="#" class="nav-link text-light " data-toggle="modal" data-target=".bd-example-modal-xl"><i class="fas fa-th"></i> Our Courses </a> </li>

		  <li class="d-none d-lg-inline-block">
		  <ul class="nav">
		
		<li class="nav-item"> <a class="nav-link active text-light pl-0" href="<?php echo SITE_URL; ?>"><i class="fas fa-home"></i> Home</a> </li>
			<li class=" nav-item list-inline-item"><a class="nav-link active text-light" href="<?php echo SITE_URL; ?>about.html"><i class="fas fa-info-circle"></i> About</a></li>
			<li class="nav-item all_courses"> <a href="#" class="nav-link text-light " data-toggle="modal" data-target=".bd-example-modal-xl"><i class="fas fa-th"></i> Our Courses </a> </li>

			<li class="nav-item"> <a class="nav-link text-light" href="<?php echo SITE_URL; ?>static/team"><i class="fas fa-users"></i> Our Team</a> </li>

			<li class="nav-item list-inline-item"><a class="nav-link active text-light" href="<?php echo SITE_URL; ?>students-testimonials.html"><i class="fas fa-bullhorn"></i> Alumni Speaks</a></li>
			<li class="nav-item"> <a class="nav-link text-light" href="<?php echo SITE_URL; ?>placements-daac.html"><i class="fas fa-user-check"></i> Placements</a> </li>
			<li class="nav-item list-inline-item"><a class="nav-link active text-light" href="<?php echo SITE_URL; ?>contact.html"><i class="fas fa-map-marker-alt"></i> Our Branches</a></li>
			<li class="nav-item list-inline-item"><a class="nav-link active text-light" href="<?php echo SITE_URL; ?>faq.html"><i class="far fa-question-circle"></i> FAQ</a></li>
			<li class="nav-item"> <a class="nav-link text-light" href="http://blog.daac.in/"><i class="fab fa-blogger-b"></i> Blog</a> </li>


		
			</ul></li>
            
       
            <li class="nav-item ml-auto request_call"> <a href="#" class="nav-link text-light pr-0" data-toggle="modal" data-target="#request_modal"> <i class="fas fa-phone-volume"></i> Request a Call Back </a> </li>
          </ul>
        </div>
      </div>

	  <div class="whatsapp"><a href="https://api.whatsapp.com/send?phone=918764122221" class="pin_trest" target="_blank"><i class="fab fa-whatsapp"></i></a></div>
    </header>


<!--<script>
$(document).ready(function(){
  $("header .header_maindiv ul li").click(function(){
	   $("header .header_maindiv ul li.active").removeClass("active");
    $(this).addClass("active");
  });
});
</script>-->
