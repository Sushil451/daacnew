<?php
  //check login by session id 
  
 $curdate=date("Y-m-d");  
					 
   function getAdminMail()
 {
 	$qry2="select * from ".TABLE_ADMIN ."";
	$temp2=tep_db_query($qry2);
	$res2=tep_db_fetch_array($temp2);
	$FROM=$res2['email'];
	return $FROM;
 }
 
   function getAdminName()
 {
 	$qry2="select fname,lname from ".TABLE_ADMIN ." where group_id=1";
	$temp2=tep_db_query($qry2);
	$res2=tep_db_fetch_array($temp2);
	$FROM=$res2['fname']."&nbsp;".$res2['lname'];
	return $FROM;
 }

  function validate_login($memberid) 
  {
    $sessionid = session_id();
	$query_validate_login = tep_db_query("select * from session where sessionid = '$sessionid' and memberid = '$memberid'");
	if (!tep_db_num_rows($query_validate_login)) 
	{
		  return false;
	}
    else
	{
	      return true;
	}
  }
  
///////////// function for metadetail and page title //////  
  function meta($id) 
  {
   
		$queryformeta=tep_db_query("select * from tblmeta where page=".$id);
		$s=mysql_num_rows($queryformeta); 
		while($resultformeta=mysql_fetch_array($queryformeta))
		{
		$metadetail[]=$resultformeta;
		
		}
		return $metadetail;
  }
   function title($id) 
  {
   
		$queryfortitle=mysql_query("select * from tblclientpage where page_id=".$id);
		while($resultfortitle=mysql_fetch_array($queryfortitle))
		{
		$titledetail=$resultfortitle['page_title'];
		
		}
		return $titledetail;
  }
  
  
  
  
//Check login and file access
 
  function tep_datetime_short($raw_datetime) {
    if ( ($raw_datetime == '0000-00-00 00:00:00') || ($raw_datetime == '') ) return false;

    $year = (int)substr($raw_datetime, 0, 4);
    $month = (int)substr($raw_datetime, 5, 2);
    $day = (int)substr($raw_datetime, 8, 2);
    $hour = (int)substr($raw_datetime, 11, 2);
    $minute = (int)substr($raw_datetime, 14, 2);
    $second = (int)substr($raw_datetime, 17, 2);

    return strftime(DATE_TIME_FORMAT, mktime($hour, $minute, $second, $month, $day, $year));
  }
 function tep_get_system_information() {
    global $HTTP_SERVER_VARS;

    $db_query = tep_db_query("select now() as datetime");
    $db = tep_db_fetch_array($db_query);

    list($system, $host, $kernel) = preg_split('/[\s,]+/', @exec('uname -a'), 5);

    return array('date' => tep_datetime_short(date('Y-m-d H:i:s')),
                 'system' => $system,
                 'kernel' => $kernel,
                 'host' => $host,
                 'ip' => gethostbyname($host),
                 'uptime' => @exec('uptime'),
                 'http_server' => $HTTP_SERVER_VARS['SERVER_SOFTWARE'],
                 'php' => PHP_VERSION,
                 'zend' => (function_exists('zend_version') ? zend_version() : ''),
                 'db_server' => DB_SERVER,
                 'db_ip' => gethostbyname(DB_SERVER),
                 'db_version' => 'MySQL ' . (function_exists('mysql_get_server_info') ? mysql_get_server_info() : ''),
                 'db_date' => tep_datetime_short($db['datetime']));
  }
function tep_admin_check_login() {
  global $PHP_SELF, $login_groups_id;
  if (!isset($_SESSION['Admin_LoginId'])) {
	pt_redirect(FILENAME_LOGIN);
  } else {
  }
}
 function tep_convert_linefeeds($from, $to, $string) {
    if ((PHP_VERSION < "4.0.5") && is_array($from)) {
      return ereg_replace('(' . implode('|', $from) . ')', $to, $string);
    } else {
      return str_replace($from, $to, $string);
    }
  }


////

  function tep_set_time_limit($limit) {
    if (!get_cfg_var('safe_mode')) {
      set_time_limit($limit);
    }
  }

////

 function tep_create_random_value($length, $type = 'mixed') {
    if ( ($type != 'mixed') && ($type != 'chars') && ($type != 'digits')) return false;

    $rand_value = '';
    while (strlen($rand_value) < $length) {
      if ($type == 'digits') {
        $char = tep_rand(0,9);
      } else {
        $char = chr(tep_rand(0,255));
      }
      if ($type == 'mixed') {
        if (eregi('^[a-z0-9]$', $char)) $rand_value .= $char;
      } elseif ($type == 'chars') {
        if (eregi('^[a-z]$', $char)) $rand_value .= $char;
      } elseif ($type == 'digits') {
        if (ereg('^[0-9]$', $char)) $rand_value .= $char;
      }
    }

    return $rand_value;
  }
 
  function tep_mail($to_name, $to_email_address, $email_subject, $email_text, $from_email_name, $from_email_address) {
     // Instantiate a new mail object
    $message = new email(array('X-Mailer: osCommerce'));
   
    $text = strip_tags($email_text);
    if (EMAIL_USE_HTML == 'true') {
      $message->add_html($email_text, $text);
    } else {
      $message->add_text($text);
    }

    // Send message
    $message->build_message();
    $message->send($to_name, $to_email_address, $from_email_name, $from_email_address, $email_subject);
  }

 
// Wrapper function for round() for php3 compatibility
  function tep_round($value, $precision) {
    if (PHP_VERSION < 4) {
      $exp = pow(10, $precision);
      return round($value * $exp) / $exp;
    } else {
      return round($value, $precision);
    }
  }

  function tep_call_function($function, $parameter, $object = '') {
    if ($object == '') {
      return call_user_func($function, $parameter);
    } elseif (PHP_VERSION < 4) {
      return call_user_method($function, $object, $parameter);
    } else {
      return call_user_func(array($object, $function), $parameter);
    }
  }
  
  function getOnlyDate($dateTime)
  {
      echo  strftime("%b %d, %Y",strtotime($dateTime));  
  }
  function getOnlyTime($dateTime)
  {
      echo  strftime("%H:%M:%S",strtotime($dateTime));  
  }
  function getBothDate($dateTime)
  {
      echo  strftime("%b %d,%Y/%H:%M:%S",strtotime($dateTime));  
  }
  function getCurrDate()
  {
      return strftime("%Y-%m-%d %H:%M:%S");  
  }
  function getFeatureName($fID)
  {
    global $lang_value;
    $queryFeature = tep_db_query("select * from " . TABLE_MASTER . " where master_id = $fID");
	$resultFeature = tep_db_fetch_array($queryFeature);
	if($resultFeature)
	  return $resultFeature[$lang_value];
	else
	  return "";  
  }
  function getRandomAdvisor()
  {
    $queryFeature = tep_db_query("select * from " . TABLE_TEAM . " where status = 1");
	$arrStr = "";
	while($resultFeature = tep_db_fetch_array($queryFeature))
	{
	  if($arrStr == "")
	    $arrStr = $resultFeature['id'];
	  else
	    $arrStr .= "," . $resultFeature['id'];	
	}
    $arrM = explode(",",$arrStr); 
	srand();
    $randV = rand(0,count($arrM)-1);
	return $arrM[$randV]; 
  }
  function getFileExtension($str)
{
	$i = strrpos($str,".");
	
	if ($i=="")
	{
		return "" ;
	}
	
	$l  = strlen($str) - $i ;
	$ext = substr($str , $i+1,$l);
	return $ext;
}
////////


////////
 function thumb_jpeg($final_filenamemain,$image_name) 
{ 
          
	global $destination_paththumb; 
    global $destination_pathmain;
    global $source_path;
	global $new_widththumb; 
    global $new_heightthumb; 
	$destimgthumb=ImageCreateTrueColor($new_widththumb,$new_heightthumb) or die("Problem In Creating image"); 

    $srcimg=imagecreatefromjpeg($source_path.$image_name) or die("Problem In opening Source Image"); 

  	ImageCopyResized($destimgthumb,$srcimg,0,0,0,0,$new_widththumb,$new_heightthumb,ImageSX($srcimg),ImageSY($srcimg)) or die("Problem In resizing");

	ImageJPEG($destimgthumb,$destination_paththumb.$final_filenamemain) or die("Problem In saving"); 
} 


//gif

function thumb_gif1($final_filenamemain,$image_name) 
{ 
          
	global $destination_paththumb; 
    global $destination_pathmain;
    global $source_path;
	global $new_widththumb; 
    global $new_heightthumb; 

	$destimgthumb=ImageCreateTrueColor($new_widththumb,$new_heightthumb) or die("Problem In Creating image"); 

    $srcimg=ImageCreateFromGIF($source_path.$image_name) or die("Problem In opening Source Image"); 

  	ImageCopyResized($destimgthumb,$srcimg,0,0,0,0,$new_widththumb,$new_heightthumb,ImageSX($srcimg),ImageSY($srcimg)) or die("Problem In resizing");

	ImageGIF($destimgthumb,$destination_paththumb.$final_filenamemain) or die("Problem In saving"); 
}  
///////////////////////////////////////////
  function memberthumb_jpeg($final_filenamemain,$image_name) 
{ 
    global $source_path; 
    global $destination_pathtmember; 
	global $new_widthmember; 
    global $new_heightmember; 
	$destimgthumb=ImageCreateTrueColor($new_widthmember,$new_heightmember) or die("Problem In Creating image"); 
    $srcimg=ImageCreateFromJPEG($source_path.$image_name) or die("Problem In opening Source Image"); 
  	ImageCopyResized($destimgthumb,$srcimg,0,0,0,0,$new_widthmember,$new_heightmember,ImageSX($srcimg),ImageSY($srcimg)) or die("Problem In resizing"); 
	ImageJPEG($destimgthumb,$destination_pathtmember.$final_filenamemain) or die("Problem In saving"); 
}

////////////////////////////////////////////
 function propertythumb_GIF($final_filenamemain,$image_name,$originalImage) 
   { 
    global $destination_pathsmall; 
   // global $destination_pathmedium; 
	global $destination_pathlarge; 
    global $source_path; 
    list($width, $height) = getimagesize($originalImage);
	$small_width=SMALL_IMAGE_WIDTH;  //Image width Change if needed 
    $small_height=SMALL_IMAGE_HEIGHT;
 
	$large_width=$width;  //Image width Change if needed 
    $large_height=$height;
	$destimgthumb=ImageCreateTrueColor($small_width,$small_height) or die("Problem In Creating image");
	$destimgmain=ImageCreateTrueColor($large_width,$large_height) or die("Problem In Creating image"); 
    $srcimg=ImageCreateFromGIF($source_path.$image_name) or die("Problem In opening Source Image"); 
  	ImageCopyResized($destimgthumb,$srcimg,0,0,0,0,$small_width,$small_height,ImageSX($srcimg),ImageSY($srcimg)) or die("Problem In resizing"); 
	ImageCopyResized($destimgmain,$srcimg,0,0,0,0,$large_width,$large_height,ImageSX($srcimg),ImageSY($srcimg)) or die("Problem In resizing"); 
    ImageGIF($destimgthumb,$destination_pathsmall.$final_filenamemain) or die("Problem In saving");
	ImageGIF($destimgmain,$destination_pathlarge.$final_filenamemain) or die("Problem In saving"); 
}  

 function thumb_png($final_filenamemain,$image_name,$originalImage) 
{ 
    global $destination_pathsmall; 
    //global $destination_pathmedium; 
	global $destination_pathlarge; 
    global $source_path;
	//list($width, $height) = getimagesize($originalImage);
	$small_width=SMALL_IMAGE_WIDTH;  //Image width Change if needed 
    $small_height=SMALL_IMAGE_HEIGHT;
	
	$large_width=LARGE_IMAGE_WIDTH;//Image width Change if needed
	$large_height=LARGE_IMAGE_HEIGHT;
  
	$destimgthumb=ImageCreateTrueColor($small_width,$small_height) or die("Problem In Creating image");
	$destimgmain=ImageCreateTrueColor($large_width,$large_height) or die("Problem In Creating image"); 
    $srcimg=ImageCreateFromPNG($source_path.$image_name) or die("Problem In opening Source Image"); 
  	ImageCopyResized($destimgthumb,$srcimg,0,0,0,0,$small_width,$small_height,ImageSX($srcimg),ImageSY($srcimg)) or die("Problem In resizing"); 
	ImageCopyResized($destimgmain,$srcimg,0,0,0,0,$large_width,$large_height,ImageSX($srcimg),ImageSY($srcimg)) or die("Problem In resizing"); 
    ImagePNG($destimgthumb,$destination_pathsmall.$final_filenamemain) or die("Problem In saving");
	ImagePNG($destimgmain,$destination_pathlarge.$final_filenamemain) or die("Problem In saving"); 
   } 
 function propertythumb_jpeg($final_filenamemain,$image_name,$originalImage) 
   { 
   
	global $destination_pathsmall; 
    global $destination_pathmedium; 
	global $destination_pathlarge; 
    global $source_path;
    list($width, $height) = getimagesize($originalImage);
	$small_width=SMALL_IMAGE_WIDTH;  //Image width Change if needed 
    $small_height=SMALL_IMAGE_HEIGHT;
	
	$large_width=LARGE_IMAGE_WIDTH;//Image width Change if needed
	$large_height=LARGE_IMAGE_HEIGHT;

  
	$destimgthumb=ImageCreateTrueColor($small_width,$small_height) or die("Problem In Creating image");
	$destimgmain=ImageCreateTrueColor($large_width,$large_height) or die("Problem In Creating image"); 
    $srcimg=ImageCreateFromJPEG($source_path.$image_name) or die("Problem In opening Source Image"); 
  	// Resizing our image to fit the canvas 
	ImageCopyResized($destimgthumb,$srcimg,0,0,0,0,$small_width,$small_height,ImageSX($srcimg),ImageSY($srcimg)) or die("Problem In resizing"); 
	ImageCopyResized($destimgmain,$srcimg,0,0,0,0,$large_width,$large_height,ImageSX($srcimg),ImageSY($srcimg)) or die("Problem In resizing"); 
   // Outputs a jpg image
    ImageJPEG($destimgthumb,$destination_pathsmall.$final_filenamemain) or die("Problem In saving");
	ImageJPEG($destimgmain,$destination_pathlarge.$final_filenamemain) or die("Problem In saving"); 
}  

function compare_values($val,$arr)
	{
		$val=trim($val);
		$cmpflag=false;
		foreach($arr as $item)
		{
			$item1=trim($item);
			if(strcasecmp($item1,$val)==0)
			{
				$cmpflag=true;
				break;
			} 
		}
		return $cmpflag;
	}
		//jpeg for small and large banner
function bannerthumb_jpeg($final_filenamemain,$image_name,$w1,$h1) 
  {  
    global $destination_path; 
    global $source_path; 
	$width=$w1;  //Image width Change if needed 
    $height=$h1;
    $destimgthumb=ImageCreateTrueColor($width,$height) or die("Problem In Creating image");
	$srcimg=ImageCreateFromJPEG($source_path.$image_name) or die("Problem In opening Source Image"); 
  	ImageCopyResized($destimgthumb,$srcimg,0,0,0,0,$width,$height,ImageSX($srcimg),ImageSY($srcimg)) or die("Problem In resizing"); 
	ImageJPEG($destimgthumb,$destination_path.$final_filenamemain) or die("Problem In saving");
}  
//gif for small  and large banner
function bannerthumb_gif($final_filenamemain,$image_name,$w1,$h1) 
{ 
 
    global $source_path; 
    global $destination_path; 
    $width=$w1;  //Image width Change if needed 
    $height=$h1;
 	$destimgthumb=ImageCreateTrueColor($width,$height) or die("Problem In Creating image");
	 $srcimg=ImageCreateFromGIF($source_path.$image_name) or die("Problem In opening Source Image"); 
  	ImageCopyResized($destimgthumb,$srcimg,0,0,0,0,$width,$height,ImageSX($srcimg),ImageSY($srcimg)) or die("Problem In resizing"); 
	ImageGIF($destimgthumb,$destination_path.$final_filenamemain) or die("Problem In saving");
}  
//png for small and large banner
 function bannerthumb_png($final_filenamemain,$image_name,$w1,$h1) 
{ 
     global $destination_path; 
     global $source_path; 
   	$width=$w1;  //Image width Change if needed 
    $height=$h1;
  	$destimgthumb=ImageCreateTrueColor($width,$height) or die("Problem In Creating image");
	$srcimg=ImageCreateFromPNG($source_path.$image_name) or die("Problem In opening Source Image"); 
  	ImageCopyResized($destimgthumb,$srcimg,0,0,0,0,$width,$height,ImageSX($srcimg),ImageSY($srcimg)) or die("Problem In resizing"); 
	ImagePNG($destimgthumb,$destination_path.$final_filenamemain) or die("Problem In saving");
  }







  ?>
