<?php /* Smarty version 2.6.24, created on 2012-05-02 10:14:27
         compiled from account/admin_account.tpl */ ?>
<html>
<head>
<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
<link rel="stylesheet" type="text/css" href="css/index.css">
<link rel="stylesheet" type="text/css" href="css/global_style.css">
<?php echo '
<script type="text/javascript" src="show_hide_slide.js"></script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../js/main.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</head>
<body topmargin="0" leftmargin="0" onLoad="init();">
<div id="loading" style="position:absolute; width:100%; text-align:center; top:300px;">
<img src="images/ajax-loader.gif" border=0></div>
<?php echo '
  <script>
 var ld=(document.all);
  var ns4=document.layers;
 var ns6=document.getElementById&&!document.all;
 var ie4=document.all;
  if (ns4)
 	ld=document.loading;
 else if (ns6)
 	ld=document.getElementById("loading").style;
 else if (ie4)
 	ld=document.all.loading.style;
  function init()
 {
 if(ns4){ld.visibility="hidden";}
 else if (ns6||ie4) ld.display="none";
 }
 </script>
'; ?>

<?php echo '
<script type="text/javascript">

String.prototype.trim = function () {
    return this.replace(/^\\s*/, "").replace(/\\s*$/, "");
}

function validatenewpass()
	{ 
	var aa=document.forms["check_pass"];
		if((aa.password_confirmation.value.trim()) == "")
		{
			alert("'; ?>
<?php echo $this->_tpl_vars['ENTERPASSW']; ?>
<?php echo '");
			aa.password_confirmation.focus();
			return false;			
		}
		aa.submit();
	}

</script>
'; ?>



<table border="0" cellpadding="0" cellspacing="0" width="100%" id="AutoNumber1" dir=<?php echo $this->_tpl_vars['DIR']; ?>
>
  <tr>
    <td width="100%" ><table border="0" cellpadding="0" cellspacing="0" width="100%" id="AutoNumber2">
        <tr>
          <td colspan="3"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
        </tr>
        <tr>
          <td width="1%" align="left" valign="top">
		  <table border="0" cellspacing="1" width="100%" id="AutoNumber9" style="vertical-align:top ">
              <tr>
                <td width="30%"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "left.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></td>
              </tr>
          </table></td>
		  
          <td width="99%"  valign="top"
		  ><table width="100%">
              <tr>
                <td valign="top" class="adminhead  tl"><b class="white"> <?php echo $this->_tpl_vars['ADMIN_ACCOUNT_TITLE']; ?>
</b>
				
				</td>
				<td width="26%" valign="top">
				  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tr>
                        <td width="100%" bgcolor="#AAD3F8" class="adminhead">
                        <p class="lighth13 white"><b><?php echo $this->_tpl_vars['ADMIN_ACCOUNT_TITLE']; ?>
</b></p></td>
                      </tr>
                   </table>
			    </td>
				
              </tr>
			 
           
              <tr>
			<?php if ($this->_tpl_vars['var'] == edit): ?>
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "account/edit.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			 <?php elseif ($this->_tpl_vars['var'] == perfom): ?>
			 <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "account/edit.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			 <?php elseif ($this->_tpl_vars['var'] == edit_account): ?>
			 <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "account/edit.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			 <?php elseif ($this->_tpl_vars['var'] == employee_detail): ?>
			 <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "account/employee_detail.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			 
			 <?php elseif ($this->_tpl_vars['var'] == add_member): ?>
			 <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "account/add_member.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			 
			 <?php elseif ($this->_tpl_vars['var'] == no_permission): ?>
		<td width="60%" valign="top" colspan="2"  class="adminhead">
		<font color="#ffffff" size="+2"><b><?php echo $this->_tpl_vars['NOT_PERMISSION']; ?>
 </b></font>
		</td>
			 
			<?php else: ?>
			
                <td width="74%" class="conbg">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" >
                    <tr>
                      <td width="100%">
					  <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding-left:10px;"  >
                          <tr>
                            <td  width="25%"><p class="lighth13"><b class="blue2"> <?php echo $this->_tpl_vars['NAME']; ?>
 :</b>&nbsp;&nbsp;&nbsp;</td>
                            <td   width="75%" align="left"><p class="lighth13"><?php echo $this->_tpl_vars['fname']; ?>
</td>
                          </tr>
                          <tr>
                            <td  width="25%"><p class="lighth13"><b class="blue2"> <?php echo $this->_tpl_vars['EMAILID']; ?>
 :</b>&nbsp;&nbsp;&nbsp;</td>
                            <td  b width="75%" align="left"><p class="lighth13"><?php echo $this->_tpl_vars['email']; ?>
</td>
                          </tr>
                          <tr>
                            <td  width="25%"><p class="lighth13"><b class="blue2"><?php echo $this->_tpl_vars['PASSWORD']; ?>
 :</b>&nbsp;&nbsp;&nbsp;</td>
                            <td   width="75%" align="left"><p class="lighth13">-- Hidden --</td>
                          </tr>
                          <tr>
                            <td  width="25%"><p class="lighth13"><b class="blue2"> <?php echo $this->_tpl_vars['PHONE']; ?>
 :</b>&nbsp;&nbsp;&nbsp;</td>
                            <td   width="75%" align="left"><p class="lighth13"><?php echo $this->_tpl_vars['phone']; ?>
</td>
                          </tr>
						  
						  <tr><td colspan="2" align="right">

						  </td></tr>
                        </table></td>
                    </tr>
                </table></td>  
				
				 <?php if ($this->_tpl_vars['var1'] == check_account): ?>
<td width="26%" valign="top">

<table border="0" cellpadding="0" cellspacing="0" width="100%" id="AutoNumber18">
<tr>
<td width="100%" class="conbg">

<!--rightside password check-->

<form name="check_pass" action="admin_account.php?action=check_password" method="post" onSubmit="return checkRegistration(this);">
<table border="0" cellpadding="0" cellspacing="0" width="95%" id="AutoNumber18">
<tr>
<td width="100%">
<p class="lighth13">
<b><?php echo $this->_tpl_vars['PASSWORD']; ?>
</b></td>
</tr>  
<?php if ($this->_tpl_vars['passerror'] != ""): ?>
<font color="#FF0000"><span class="fnt11"><?php echo $this->_tpl_vars['passerror']; ?>
</span></font>
<?php endif; ?>
<tr>
<td width="100%">
<p class="lighth13">
<input type="password" name="password_confirmation" size="20" class="textbox1 twh"></td>
</tr>
<input type = hidden name = id_info value = <?php echo $this->_tpl_vars['id']; ?>
>
<tr>
<td width="100%">
<p class="lighth13n" align="center"><br/>
<input type="button" value=<?php echo $this->_tpl_vars['SUBMIT']; ?>
 style="cursor:pointer" name="B1" class="blue_but" onClick="validatenewpass();">
<input type="button" value=<?php echo $this->_tpl_vars['BACK']; ?>
 name="B1" style="cursor:pointer" class="blue_but" onClick="location.href='admin_account.php'">
</td>
</tr>
</table>
</form>
</td>
</tr>
</table>
</td>
				
				<?php elseif ($this->_tpl_vars['var1'] == link_edit): ?>
				<td width="26%" valign="top">
           		  
      <table border="0" cellpadding="0" cellspacing="0" width="95%" id="AutoNumber18">
                          <tr>
                            <td width="100%">
							<form name="link" action="admin_account.php?action=link_change" method="post" > 
                            <font color="#034F94"><span style="font-size: 7pt">
                          <input type="text" name="link" value="<?php echo $this->_tpl_vars['link']; ?>
"> </span></font>
						  	
							
							</p>
							<p> <input type="image" src="images/print.jpg"> </p>
							</form>
						  
						  </td>
                          </tr>  
						 
			</table>	 
			
			    </td>
				
				
				
				<?php else: ?>
				   <td width="26%" valign="top">
           		  
<table border="0" class="conbg" cellpadding="0" cellspacing="0" width="100%" id="AutoNumber18">
<tr>
<td width="100%">
<p class="lighth10 blue2"><span style="font-size: 7pt"><?php echo $this->_tpl_vars['MSG6']; ?>
</span></td>
</tr>  

<tr>
<td width="100%"  >
<p class="lighth13n" align="center">
<input type="button" value="<?php echo $this->_tpl_vars['EDIT']; ?>
" name="B1" class="blue_but" style="cursor:pointer" onClick="location.href='admin_account.php?action=check_account&id=<?php echo $this->_tpl_vars['id']; ?>
'"></td>
</tr>

						 
			</table>	 
			
			    </td>
				 <?php endif; ?>
				  
			<?php endif; ?>   
			    
				
				
			  <tr>
                       
              </tr>
              <tr>
                <td width="74%"> </td>
              </tr>
          </table></td>
		 
        </tr>
      </table></td></tr>
</table>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> 
</body>
</html>