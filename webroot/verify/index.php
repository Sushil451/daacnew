<? include("db.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- saved from url=(0065)http://www.certiport.com/Portal/Pages/CredentialVerification.aspx -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>CredentialVerification</title>
	<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	<link href="css/Portal.css" type="text/css" rel="stylesheet">
<script src="js/jquery-3.1.1.min.js" type="text/javascript" language="javascript"></script>
<script language="javascript">
$(document).ready(function()
{
	$("#login_form").submit(function()
	{ 
		//remove all the class add the messagebox classes and start fading
		$("#msgbox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
		//check the username exists or not from ajax
		$.post("verify.php",{ c_Code1:$('#c_Code1').val(),c_Code2:$('#c_Code2').val(),rand:Math.random() } ,function(data)
        {
           // alert(data);
		  if(data=='yes') //if correct login detail
		  {
		  	$("#msgbox").fadeTo(200,0.1,function()  //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html('verifying code.....').addClass('messageboxok').fadeTo(900,1,
              function()
			  { 
			  	 //redirect to secure page
				 document.location='result.php';
			  });
			  
			});
		  }
		  else 
		  {
		  	$("#msgbox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html('Code not verified.').addClass('messageboxerror').fadeTo(900,1);
			});		
          }
				
        });
 		return false; //not to post the  form physically
	});
	//now call the ajax also focus move from 
	$("#c_Code2").blur(function()
	{
		$("#login_form").trigger('submit');
	});
});
</script>	
</head>
<body>
	
<div>


</div>

	<table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
	<tbody><tr>
		<td width="100%" align="center">
					<table id="c_BannerContainer" cellspacing="0" cellpadding="0" width="1000" border="0">
			<tbody><tr>
				<td>
								<table cellspacing="0" cellpadding="0" width="100%" border="0">
									<tbody><tr>
										<td>
<meta content="False" name="vs_showGrid">
<table id="c_Banner_c_BannerTbl" cellspacing="0" cellpadding="0" border="0" width="1000">
					<tbody><tr>
						<td>
			<table id="c_Banner_c_LogoTbl" cellspacing="0" cellpadding="0" width="1000" height="69" border="0" background="images/CertiportHeader.jpg">
							<tbody><tr>
								<td width="305">&nbsp;
						
					</td>
								<td>
						
					</td>
								<td nowrap="nowrap" align="right">
						
					</td>
								<td style="width: 30px;">&nbsp;
						
					</td>
							</tr>
						</tbody></table>
						
		</td>
					</tr>
				</tbody></table>
				
</td>
									</tr>
									<tr style="background-color: White;"><td>&nbsp;</td></tr>
								</tbody></table>
							</td>
			</tr>
		</tbody></table>
		
					<table cellspacing="0" cellpadding="0" width="1000" border="0">
						<tbody><tr>
							<td width="30" style="background-color: White;">&nbsp;</td>
							<td width="*" style="background-color: White;">
	<table id="Table1" width="100%" border="0" cellpadding="0" cellspacing="0">
		<tbody><tr>
			<td colspan="2">
				<span id="c_Header" class="Head">Global Credential Verification</span>
			</td>
		</tr>
		<tr height="10">
			<td>
			</td>
		</tr>
		
		<tr height="15">
			<td>
			</td>
		</tr>
	</tbody></table>
<form name="login_form" method="post" action="" id="login_form">
	
	<table id="Table1" width="100%" border="0" cellpadding="0" cellspacing="0">
		<tbody><tr>
			<td colspan="6">
				<span id="c_VerifyMsg" class="CVContent">Welcome to the DAAC Verification site. This fraud prevention system can be used to verify DAAC credentials held by candidates world-wide. The Credential Identification Code can be found on all certificates issued by DAAC.<br><br>Upon entering the Identification Code, authentication details relating to the holder of the credentials will be presented.</span>
			</td>
		</tr>
		<tr height="20">
			<td colspan="6"> 
			</td>
		</tr>
		<tr>
			<td width="35%">
				<span id="c_CodeLbl" class="CVContent">Credential Identification Code:</span>
			</td>
			<td width="15%">
				<input name="c_Code1" type="text" maxlength="4" id="c_Code1" class="CVProficient" style="width:110px;">
			</td>
			<td width="2%">
				<table cellpadding="0" cellspacing="0" class="CVHyphen">
					<tbody><tr>
						<td>&nbsp;
							
						</td>
					</tr>
				</tbody></table>
			</td>
			<td width="15%">
				<input name="c_Code2" type="text" maxlength="4" id="c_Code2" class="CVProficient" style="width:110px;">
			</td>
			<td align="left">
				<table id="c_ValidateBtn_c_Table" cellspacing="0" cellpadding="0" border="0" style="border-width:0px;border-collapse:collapse;">
			<tbody>
			
			<tr>
				<td id="c_ValidateBtn_c_LeftBtnCell" style="width:5px;padding:0px;"><img id="c_ValidateBtn_c_LeftBtnImg" src="images/leftSObtn.gif"style="height:26px;width:5px;border-width:0px;"></td><td id="c_ValidateBtn_c_TextCell" class="CommandButtonBgLg" align="center" style="height:26px;white-space:nowrap;">
<div class="buttondiv">
<a id="c_ValidateBtn_c_CommandBtn" title="Verify" class="CommandButton" href="javascript: validate()">&nbsp;Verify&nbsp;</a>
</div>
</td><td id="c_ValidateBtn_c_RightBtnCell" style="width:5px;padding:0px;"><img id="c_ValidateBtn_c_RightBtnImg" src="images/rightSObtn.gif" style="height:26px;width:5px;border-width:0px;"></td>
			</tr>
			

		</tbody></table>

			</td>
		</tr>
		<tr height="20">
			<td colspan="6" align="center"><div style="margin-top:10px"> <span id="msgbox" style="display:none"></span></div>
			</td>
		</tr>
	</tbody></table>
	
	</form>	
<?php if(isset($_SESSION['checkcode']) && ($_SESSION['checkcode']!=''))
{
$getcert=mysql_query("select * from tblcertificates where verify_code='".$_SESSION['checkcode']."'"); 
$reccert=mysql_fetch_array($getcert);
extract($reccert);

$getcourse=mysql_query("select cname,duration from tblcourse where id=".$course_id); 
$reccourse=mysql_fetch_array($getcourse);
extract($reccourse);

?>
<table id="c_Results" width="100%" border="1" cellpadding="0" cellspacing="0" class="TableBG">
			<tbody><tr>
				<td>
				<div id="c_ResultInfoDiv">
						</div><table class="TableBody" id="Table2" width="100%" cellpadding="0" cellspacing="0" border="0">
					<tbody><tr>
						<td colspan="3">
							<span id="c_ResultHdr" class="CVContentBold">Verification Results from DAAC Secure Global Database</span>
						</td>
					</tr>
					<tr height="20">
						<td colspan="3">
						</td>
					</tr>
					
					<tr>
							<td width="5%">&nbsp;
								
							</td>
							<td width="35%">
								<span id="c_ResultNameLbl" class="CVContent">Credential belongs to:</span>
							</td>
							<td width="*">
								<span id="c_ResultName" class="CVContent"><?php echo $ctitle; ?></span>
							</td>
						</tr>
						<tr>
							<td>&nbsp;
								
							</td>
							<td>
								<span id="c_ResultIssuedByLbl" class="CVContent">Issued by:</span>
							</td>
							<td>
								<span id="c_ResultIssuedBy" class="CVContent">DAAC</span>
							</td>
						</tr>
						<tr>
							<td>&nbsp;
								
							</td>
							<td>
								<span id="c_ResultDateLbl" class="CVContent">Issued on:</span>
							</td>
							<td>
								<span id="c_ResultDate" class="CVContent"><?php echo $pre_date; ?></span>
							</td>
						</tr>
						<tr>
							<td>&nbsp;
								
							</td>
							<td>
								<span id="c_ResultCertLbl" class="CVContent">Certification:</span>
							</td>
							<td>
								<span id="c_ResultCert" class="CVContent">
								<!--<a target="_blank" class="CVLink" href="#">--><?php echo $cname; ?><!--</a>-->&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<!--<a target="_blank" class="CVLinkSmall" href="#">View Endorsements</a>--></span>
							</td>
						</tr>
						<tr>
							<td>&nbsp;
								
							</td>
							<td>
								<span id="c_ResultCertLbl" class="CVContent">Duration:</span>
							</td>
							<td>
								<span id="c_ResultCert" class="CVContent">
								<!--<a target="_blank" class="CVLink" href="#">--><?php echo $duration; ?><!--</a>-->&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<!--<a target="_blank" class="CVLinkSmall" href="#">View Endorsements</a>--></span>
							</td>
						</tr>
						<?php if(isset($course_includes)){ ?>
						<tr>
							<td>&nbsp;
								
							</td>
							<td>
								<span id="c_ResultCertLbl" class="CVContent">Course Includes:</span>
							</td>
							<td>
								<span id="c_ResultCert" class="CVContent">
								<!--<a target="_blank" class="CVLink" href="#">--><?php echo $course_includes; ?><!--</a>-->&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<!--<a target="_blank" class="CVLinkSmall" href="#">View Endorsements</a>--></span>
							</td>
						</tr>
<?php } ?>
						<tr height="20">
							<td colspan="3">
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<span id="c_ResultInfo" class="CVContent">View the <a target="_blank" class="CVLink" href="pdf.php">online copy</a> of the certificate. Obtain a copy of <a target="_blank" class="CVLink" href="http://www.adobe.com/products/acrobat/readstep.html">Adobe Acrobat Reader</a>.</span>
							</td>
						</tr>
					
				</tbody></table>
			</td>
			</tr>
		</tbody></table>
<?php
}

?>	
	
	
							</td>
							<td width="30" style="background-color: White;">&nbsp;</td>
						</tr>
					</tbody></table>
				</td>
	</tr>
</tbody></table>

		<div align="center">

<table id="c_Footer_c_Footer1" cellspacing="0" cellpadding="0" width="100%" border="0">
	<tbody><tr>
		<td width="100%" align="center"><table cellspacing="0" cellpadding="0" width="1000" border="0">
          <tbody>
            <tr style="width: auto; height: 50px;">
              <td style="border-color: #F4F4F2; background-color: #F4F4F2"></td>
            </tr>
            <!--<tr style="width: auto; height: 35px;">
					<td style="border-color: #767676; background-color: #767676">
					</td>
				</tr>-->
          </tbody>
		  </table></td>
	</tr>
</tbody></table>


</div>



</body></html>