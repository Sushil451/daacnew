<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DropshiperscustomersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DropshiperscustomersTable Test Case
 */
class DropshiperscustomersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DropshiperscustomersTable
     */
    public $Dropshiperscustomers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Dropshiperscustomers',
        'app.Customers',
        'app.Dropshipers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Dropshiperscustomers') ? [] : ['className' => DropshiperscustomersTable::class];
        $this->Dropshiperscustomers = TableRegistry::getTableLocator()->get('Dropshiperscustomers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Dropshiperscustomers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
