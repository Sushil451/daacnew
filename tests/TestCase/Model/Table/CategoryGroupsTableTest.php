<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CategoryGroupsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CategoryGroupsTable Test Case
 */
class CategoryGroupsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CategoryGroupsTable
     */
    public $CategoryGroups;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CategoryGroups',
        'app.Categories',
        'app.Groups',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CategoryGroups') ? [] : ['className' => CategoryGroupsTable::class];
        $this->CategoryGroups = TableRegistry::getTableLocator()->get('CategoryGroups', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CategoryGroups);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
